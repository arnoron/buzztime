#!/bin/bash

tar -cvf /var/archives/cheerios/cleanup-$(date +"%Y-%m-%d-%H").tar.gz /var/log/cleanup/cron.log

#Remove all docker images except latest five
for image_name in $(docker images --format "{{.Repository}}" | sort | uniq); do
	echo $image_name
	image_IDs=$(docker images $image_name -q | sed '1,5d')
	for ID  in $image_IDs; do
		docker rmi $ID
	done;
done;
#Remove dangling docker images
docker rmi $(docker images -f dangling=true -q)

#Remove dangling docker volumes
docker volume rm $(docker volume ls -qf dangling=true)

#Remove stopped & dead docker containers
docker ps --filter status=dead --filter status=exited -aq | xargs docker rm -v

