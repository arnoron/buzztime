#!/bin/bash

# Variables 
SRC=/var/log/haproxy
DEST=/var/archives/cheerios
LOGFILE="$DEST/BackupLog-$(date +%Y-%m-%d-%H:%M).log"
Date=$(date +"%Y-%m-%d-%H:%M")
## Add recipients separated with space 
EMAIL_TO=vivek.kumar@hashedin.com
EMAIL_OPT="-o tls=auto -f btdevops@gmail.com -t $EMAIL_TO -s smtp.gmail.com:587 -xu btdevops@gmail.com -xp 99X.$:sNyA -l $LOGFILE"
HOST=$(cat /etc/hostname)

# Creating Destination if it doesn't exists
[ -d $DEST ] || mkdir -p $DEST


## This will create a log file with all the events performed below by this script 
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>$LOGFILE 2>&1


## Copying log files with appending timestamp to the filename 
echo -e "\n---- Backing up \"$SRC\" ----\n"
cd $SRC
for i in `ls -p | grep -v /`; do cp -npRv $i $DEST/`echo $i | sed "s/.log/-$Date.log/g"`; done

#Original command - find $DEST -type f -mtime +30 \( -iname \*.zip -o -iname \*.log \) -delete
# Add -delete as above to remove zip & log files older than 30 days  \
# Remove -delete to view the list of files to be removed 
echo -e "\n---- Removing Zip/Log files older than 30 days in \"$DEST\" ----\n"
find $DEST -type f -mtime +30 \( -iname \*.zip -o -iname \*.log \) -print -delete

# Nullifying out/ directory after the zip  
##The extglob (Extended Pattern Matching) needs to be enabled in BASH
echo -e "\n---- Cleaning \"$SRC\" contents except logs ----\n"
shopt -s extglob
rm -rvf $SRC/!(haproxy.log)

echo -e "\n ** Nullifying Log files in \"$SRC\" .... \n "
echo -n | tee haproxy.log

##file sizes till now 

echo -e "\n\n ---- Sorting \"$SRC\" by Filesize ---- \n"
du -sch $SRC/* | sort -rh
echo -e "\n\n ---- Sorting \"$DEST\" by Filesize ---- \n" 
du -sch $DEST/* | sort -rh
echo -en "\n"

## Sending logfile as email , see $EMAILOPT for more info 
echo -e "\n\n ** Sending Email to $EMAIL_TO ....... \n"
EMAILCONTENT=$(cat $LOGFILE)

#if [ "$HOST" == "prd-docker-01" ]; then
#  EMAIL_TO=cheerios@buzztime.com
#  EMAIL_OPT="-o tls=auto -f btdevops@gmail.com -t $EMAIL_TO -s smtp.gmail.com:587 -xu btdevops@gmail.com -xp 99X.$:sNyA -l $LOGFILE"
#fi

  /usr/local/bin/sendEmail $EMAIL_OPT  -u "Cheerios Logs@$HOST Backup Status Report" -m "$EMAILCONTENT"

#END###################################################################################################

