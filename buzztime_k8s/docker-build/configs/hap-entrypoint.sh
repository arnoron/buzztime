#!/bin/sh

service rsyslog start

touch /var/log/haproxy/haproxy.log

exec haproxy -f /usr/local/etc/haproxy/haproxy.cfg
