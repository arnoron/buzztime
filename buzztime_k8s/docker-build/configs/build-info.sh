#!/bin/bash

OUT="./templates/build-info.txt"
echo "buildDate=`date`" > $OUT
echo "buildNumber=$BUILD_NUMBER" >> $OUT
echo "gitRevision=`git rev-list HEAD | head -1`" >> $OUT

cat $OUT
