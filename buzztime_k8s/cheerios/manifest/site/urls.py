"""SiteHub URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from cheerios.manifest.site import views, vistar

urlpatterns = [
    url(r'^software/$', views.SoftwareViewSet.as_view()),
    url(r'^content/$', views.ContentViewSet.as_view()),
    url(r'^handshake/$', views.HandshakeViewSet.as_view()),
    url(r'^sitehub/$', views.SiteHubUpdateViewSet.as_view(), name="sitehub"),
    url(r'^vistar-assets/$', vistar.VistarAssets.as_view(), name="vistar"),
    url(r'^build-data/$', views.BuildDataViewSet.as_view(), name="build-data"),
    url(r'^execute_vistar_pop/$', vistar.ProofOfPlayViewset.as_view(), name="proof-of-play"),
    url(r'^player_achievement/$', views.PlayerAchievementViewSet.as_view(), name="player-achievement"),
    url(r'^schedule/$', views.ScheduleViewSet.as_view(), name="schedule"),
    url(r'^schedule_ancillary_data/$', views.ScheduleAncillaryData.as_view(),
        name="schedule-ancillary-data"),
    url(r'^playerplus_points/$', views.PlayerPlusPoints.as_view(), name="playerplus_points"),
    url(r'^player_game_question/$', views.Question.as_view(), name="question"),
    url(r'^site_game_question/$', views.SiteGameQuestion.as_view(), name="site-game-question"),
    url(r'^score/$', views.ScoreViewSet.as_view(), name="score")
]
