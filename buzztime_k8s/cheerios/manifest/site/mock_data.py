"""
Mock data for the site manifest data
"""

def get_content_manifest_data():
    """
    returns the content manifest mock data
    :return:
    """
    return {"contentInfo":
            {"manifestVersion": "1.00.00-1024",
             "device_resolution": "1080p",
            },
            "content":[{"md5": "f1425b726e45ba5053aec4fc9d2ea463",
                        "sha256": "f1425b726e45ba5053aec4fc9d2ea463",
                        "remoteUrl": "http://cdn.dev.buzztime.com/0007AC/"
                                     "ads/TestAd_15sec.mp4?ec_rate=20&disp_res=1080",
                        "filepath": "/ads/NetworkAds.xml",
                        "reportOnDownload": "false",
                        "manifestInfo": {"fileId": 10750,
                                         "fileVersionId": 175832,
                                         "source": "global",
                                         "updateDate": "2012-01-25T20:16:24.707",
                                        },
                        "resolutions": [480, 600, 720, 1080, 2160],
                        "mimetype": "text/xml",
                        "contentTag": "ads",
                        "notify": "onArrival",
                       },
                      ]
           }
