"""
Serializers for site manifest data
"""
import re

from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class SoftwareViewSetSerializer(SerializerValidator):
    """
    Validate the params for the software viewset
    """
    deviceId = serializers.CharField(required=True)


class ContentViewSetSerializer(SerializerValidator):
    """
    Validate the params for the software viewset
    """
    deviceId = serializers.CharField(required=True)
    lastDate = serializers.DateTimeField(required=False)


class HandshakeViewSetSerializer(SerializerValidator):
    """
    Validate the params for the handshake viewset
    """
    serialno = serializers.CharField(required=True)


class SiteHubUpdateViewSetSerializer(SerializerValidator):
    """
    Validate the request body for the sitehub viewset
    """
    SerialNumber = serializers.CharField(required=False)
    DeviceIdentifier = serializers.CharField(required=True)
    SoftwareVersion = serializers.CharField(required=False)
    FirmwareManufacturer = serializers.CharField(required=False)
    FirmwareModel = serializers.CharField(required=False)
    FirmwareDisplay = serializers.CharField(required=False)
    FirmwareSDKVersion = serializers.CharField(required=False)
    FirmwareOSVersion = serializers.CharField(required=False)
    DataFreeBytes = serializers.CharField(required=False)
    DataTotalBytes = serializers.CharField(required=False)
    SiteIPAddress = serializers.CharField(required=False)
    WAPModelFirmware = serializers.CharField(required=False)


class VistarAssetsSerializer(SerializerValidator):
    """
    Validate the request body for the vistar assets post API
    """
    fileName = serializers.CharField(required=True)
    filePath = serializers.CharField(required=True)
    siteID = serializers.IntegerField(required=True)


class BuildDataViewSetSerializer(SerializerValidator):
    """
    Validate the request body for the build data viewset
    """
    sitehubSoftwareFileName = serializers.CharField(required=True)
    featureGroups = serializers.CharField(required=True, allow_blank=True)
    manifestFileId = serializers.IntegerField(required=True)
    versionName = serializers.CharField(required=True)
    buildType = serializers.CharField(required=True)
    versionCode = serializers.IntegerField(required=True)
    branch = serializers.CharField(required=True)
    minimumSDKVersion = serializers.IntegerField(required=True)
    maximumSDKVersion = serializers.IntegerField(required=True)
    runDependencies = serializers.CharField(required=True, allow_blank=True)
    gitCommit = serializers.CharField(required=True)
    gitURL = serializers.CharField(required=True)
    mavenCoordinate = serializers.CharField(required=False)
    isDeprecated = serializers.IntegerField(required=True)


class ProofOfPlaySerializer(SerializerValidator):
    """
    Validate the request paramters for PoP Execution API
    """
    pop_url = serializers.CharField(required=True)


class ProofOfPlayDBPostSerializer(SerializerValidator):
    """
    Validates the request params for DB save for vistar APIs
    """
    site_id = serializers.IntegerField(required=True)
    vistar_ad_id = serializers.CharField(required=True)
    vistar_campaign_id = serializers.CharField(required=True)
    ad_type = serializers.CharField(required=True)
    run_date = serializers.CharField(required=True)
    channel_number = serializers.IntegerField(required=True)
    player_count = serializers.IntegerField(required=True)
    tablet_count = serializers.IntegerField(required=True)

    @staticmethod
    def validate_run_date(run_date):
        """
        Validates the time to be in 2017-08-01T14:33:00 format,
        else raises validation error
        :return:
        """
        if not re.fullmatch(r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}", run_date):
            raise serializers.ValidationError('Incorrect time format passed')


class PlayerAchievementGETSerializer(SerializerValidator):
    """
    Validate the request parameters for the GET player achievement API call
    """
    player_id = serializers.IntegerField(required=True)
    achievement_id = serializers.IntegerField(required=True)


class PlayerAchievementPOSTSerializer(SerializerValidator):
    """
    Validate the request body for the inserting player achievement API
    """
    achievement_id = serializers.IntegerField(required=True)
    client_date = serializers.DateTimeField(required=True)
    game_name_id = serializers.IntegerField(required=True)
    player_id = serializers.IntegerField(required=True)
    question_id = serializers.CharField(required=True)
    site_id = serializers.IntegerField(required=True)


class SupervisorScheduleSerializer(SerializerValidator):
    """
    Serializer for the supervisor schedule API
    """
    site_id = serializers.IntegerField(required=True)
    schedule_id = serializers.IntegerField(required=True)
    schedule_date = serializers.DateTimeField(required=False)


class ScheduleAncillaryDataSerializer(SerializerValidator):
    """
    Serializer for the schedule Ancillary API
    """
    schedule_id = serializers.IntegerField(required=True)
    db_name = serializers.CharField(required=True)


class PlayerPlusPointsSerializer(SerializerValidator):
    """
    Validate the request body for the awarding player plus points
    """
    player_id = serializers.IntegerField(required=True)
    award_date = serializers.CharField(required=True)
    points_awarded = serializers.IntegerField(required=True)
    points_award_type_id = serializers.IntegerField(required=True)
    reference_id = serializers.IntegerField(required=True)


class PlayerGameQuestionSerializer(SerializerValidator):
    """
    Serializer for Player Game Question
    """
    player_id = serializers.IntegerField(required=True)
    site_id = serializers.IntegerField(required=True)
    game_name_id = serializers.IntegerField(required=True)
    game_id = serializers.IntegerField(required=True)
    game_start_time = serializers.DateTimeField(required=False)
    question_id = serializers.IntegerField(required=True)
    answer_index = serializers.IntegerField(required=True)
    handle = serializers.CharField(required=True)
    pm_num = serializers.IntegerField(required=True)
    score = serializers.IntegerField(required=True)
    interactive_ad_skin_id = serializers.IntegerField(required=False)
    answer_date = serializers.DateTimeField(required=False)
    pin = serializers.CharField(required=True, allow_blank=True)
    question_number = serializers.IntegerField(required=False)


class QuestionSerializer(SerializerValidator):
    """
    Serializer for a question
    """
    question_id = serializers.IntegerField(required=True)
    total_points = serializers.IntegerField(required=True)
    total_responses = serializers.IntegerField(required=True)


class SiteGameQuestionSerializer(SerializerValidator):
    """
    Serializer for Site Game Question API
    """
    site_id = serializers.IntegerField(required=True)
    game_name_id = serializers.IntegerField(required=True)
    game_start = serializers.DateTimeField(input_formats=["%Y-%m-%dT%H:%M:%S"], required=True)
    network_id = serializers.IntegerField(required=True)
    question_list = QuestionSerializer(required=True, many=True)


class NetworkAiringSerializer(SerializerValidator):
    """

    """
    game_name_id = serializers.IntegerField(required=True)
    game_start_time = serializers.DateTimeField(required=True)
    game_id = serializers.IntegerField(required=True)


class PlayerSerializer(SerializerValidator):
    """

    """
    player_id = serializers.IntegerField(required=True)
    handle = serializers.CharField(required=True)
    pm_num = serializers.IntegerField(required=True)


class PlayerScoreSerializer(SerializerValidator):
    """

    """
    player = PlayerSerializer(required=True)
    score = serializers.IntegerField(required=True)
    response_count = serializers.IntegerField(required=True)
    channel = serializers.IntegerField(required=True)
    duration = serializers.IntegerField(required=True)
    repeat_game = serializers.IntegerField(required=True)
    playmaker_type_id = serializers.IntegerField(required=True)
    session_guid = serializers.UUIDField(required=False)


class ScoreSerializer(SerializerValidator):
    """

    """
    ranking_type = serializers.CharField(required=True)
    site_id = serializers.IntegerField(required=True)
    site_name = serializers.CharField(required=True)
    network_airing = NetworkAiringSerializer(required=True)
    player_scores = PlayerScoreSerializer(required=True, many=True)
