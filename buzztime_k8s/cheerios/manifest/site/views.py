"""
Viewset methods for Qbert APIs
"""
import datetime

from multiprocessing.pool import ThreadPool
import pymssql

from rest_framework import status

from cheerios.interface.api_view_interface import ApiViewInterface
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.manifest.constant import URL, ALLOWED_DB_MAPPER
from cheerios.manifest.exceptions import NotFoundError
from cheerios.manifest.site.constants import QBERT_STORED_PROCEDURE_MAPPER
from cheerios.manifest.site.serializers import SoftwareViewSetSerializer, \
    ContentViewSetSerializer, \
    HandshakeViewSetSerializer, SiteHubUpdateViewSetSerializer, BuildDataViewSetSerializer, \
    PlayerAchievementGETSerializer, PlayerAchievementPOSTSerializer, ScheduleAncillaryDataSerializer, \
    PlayerPlusPointsSerializer, SiteGameQuestionSerializer, \
    PlayerGameQuestionSerializer, SupervisorScheduleSerializer, ScoreSerializer
from cheerios.players.decorators import close_connection
from cheerios.players.views.login import LoginViewSet
from cheerios.services.jwt_utils import jwt_decorator
from cheerios.settings import MSSQLDBPROG, MSSQLBROADCAST, MSSQLUSERDB, MSSQLUSER, MSSQLHOST, MSSQLPASSWORD, \
    MSSQLDBGAMEMGMT, MSSQLDBGAMEPLAY, MSSQLPLAYERPLUSDB

from cheerios.utils import CheeriosUtils


PST_OFFSET = -800

MULTIPLICATION_FACTOR = 100

CHEERIOS_UTILS = CheeriosUtils()

from cheerios.services.log_classifier import Logger

class ManifestSiteViewSet(MSSQLApiView):
    """
    General Viewset for the Manifest Site APIs
    """

    @close_connection
    def execute_stored_procedure(self, data_parameter, stored_procedure, database=MSSQLDBPROG, transaction=False):
        """
        takes a stored procedure and data parameters and
        calls the stored procedure with the data param
        :param data_parameter:
        :param stored_procedure:
        :return:
        """
        cursor = self.get_cursor(database, transaction=transaction)
        self.call_storeprocedure(stored_procedure, cursor, data_parameter)
        if any(x in stored_procedure for x in ["upd", "sav"]):
            return True
        return self.fetchall_from_cursor(cursor)


MANIFEST_OBJ = ManifestSiteViewSet()


class SoftwareViewSet(ApiViewInterface):
    """
    Viewset for manifest software data
    """

    @jwt_decorator(URL['software'], 'GET')
    def get(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().get(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return SoftwareViewSetSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """
        data = "@SerialNumber={}".format(data.get('deviceId'))
        return MANIFEST_OBJ.execute_stored_procedure(
            data, QBERT_STORED_PROCEDURE_MAPPER['get_manifest_software']), True

    def format_response(self, data):
        """

        :param data:
        :return:
        """
        softwareInfo = ""
        if data:
            softwareInfo = dict(releaseVersion=data[0].get('SiteHubSoftwareRelease'),
                                environs=data[0].get('Environments').split(','),
                                branches=data[0].get('Branches').split(','),
                                features=data[0].get('Features').split(',')
                                )
        software = map(self.process_data, data)
        return dict(softwareInfo=softwareInfo, software=software)

    def process_data(self, software):
        """

        :return:
        """
        return dict(package=software.get('SiteHubSoftwareFileName'),
                    branch=software.get('Branch'),
                    versionName=software.get('VersionName'),
                    versionCode=software.get('VersionCode'),
                    md5=software.get('Md5'),
                    remoteUrl="{0}{1}{2}{3}".format(software.get('EdgecastWebServer'),
                                                    software.get('FilePath'),
                                                    software.get('FileName'),
                                                    software.get('EdgecastDownloadRate')
                                                    ),
                    filepath="{0}{1}".format(software.get('FilePath'),
                                             software.get('FileName')),
                    rundeps=software.get('RunDependencies').split(),
                    featureGroups=software.get('FeatureGroups').split(),
                    coordinate=software.get('MavenCoordinate'),
                    manifestFileStatusId=software.get('ManifestFileStatusID')
                    )


class ContentViewSet(ApiViewInterface):
    """
    Viewset for manifest software data
    """

    @jwt_decorator(URL['content'], 'GET')
    def get(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().get(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return ContentViewSetSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """
        sp_param_string = "@SerialNumber={}".format(data.get('deviceId'))

        if data.get('lastDate'):
            sp_param_string += ", @LastDate='{}'".format(data.get('lastDate'))
        else:
            sp_param_string += ", @LastDate=NULL"

        return MANIFEST_OBJ.execute_stored_procedure(
            sp_param_string, QBERT_STORED_PROCEDURE_MAPPER['get_manifest_content']), True

    def format_response(self, data):
        """
        Formats response
        :param data:
        :return:
        """
        if data:
            response_json = dict(contentInfo=dict(manifestDate=data[0].get('ManifestDate')),
                                 content=list())
            for data_instance in data:
                formatted_data_instance = dict()
                formatted_data_instance['md5'] = data_instance.get('Md5')
                formatted_data_instance['remoteUrl'] = data_instance.get(
                    'EdgecastWebServer').replace("\\", "/") + \
                                                       data_instance.get('FilePath').replace("\\",
                                                                                             "/") + \
                                                       data_instance.get('CDNFileName').replace(
                                                           "\\", "/") + \
                                                       data_instance.get(
                                                           'EdgecastDownloadRate').replace("\\",
                                                                                           "/")
                formatted_data_instance['filepath'] = data_instance.get('FilePath').replace("\\",
                                                                                            "/") + \
                                                      data_instance.get('FileName').replace("\\",
                                                                                            "/")
                formatted_data_instance['reportOnDownload'] = True if data_instance.get(
                    'ReportOnDownload') else False
                formatted_data_instance['publishable'] = True if data_instance.get(
                    'SiteHubPublishable') else False
                formatted_data_instance['manifestInfo'] = dict(
                    fileId=data_instance.get('ManifestFileID'),
                    fileVersionId=data_instance.get('ManifestFileVersionID'),
                    source=data_instance.get('ManifestSource'),
                    updateDate=data_instance.get('UpdateDate'))
                response_json['content'].append(formatted_data_instance)

            return response_json

        return []


class HandshakeViewSet(ApiViewInterface):
    """
    Viewset for manifest software data
    """

    @jwt_decorator(URL['site_handshake'], 'GET')
    def get(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().get(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return HandshakeViewSetSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """

        data = "@SerialNumber='{}'".format(data.get('serialno'))
        site_by_serial_info = MANIFEST_OBJ.execute_stored_procedure(data,
                                                                    QBERT_STORED_PROCEDURE_MAPPER
                                                                    ['device_by_serial'])

        if not site_by_serial_info:
            return {}, True

        site_by_serial_info = site_by_serial_info[0]

        data = "@SiteID={}".format(site_by_serial_info['SiteID'])
        site_info = MANIFEST_OBJ.execute_stored_procedure(
            data, QBERT_STORED_PROCEDURE_MAPPER['site_info'])

        if not site_info:
            site_info = {}
        else:
            site_info = site_info[0]

        data = "@NamePattern='{}'".format('SiteHub.%')
        platform_services_data = MANIFEST_OBJ.execute_stored_procedure(data,
                                                                QBERT_STORED_PROCEDURE_MAPPER
                                                                ['platform_services_conf'])

        site_settings = []
        site_secure = []

        for data in platform_services_data:
            platform_conf_name = data.get('Name')
            if platform_conf_name:
                if "SiteHub.Settings" in platform_conf_name:
                    site_settings.append(data)
                elif "SiteHub.Secure" in platform_conf_name:
                    site_secure.append(data)

        data = "@SiteID={}".format(site_by_serial_info['SiteID'])
        site_certificates = MANIFEST_OBJ.execute_stored_procedure(
            data, QBERT_STORED_PROCEDURE_MAPPER['site_cert_list'])

        site_confidentials_response = MANIFEST_OBJ.execute_stored_procedure(
            data, QBERT_STORED_PROCEDURE_MAPPER['site_confidentials_list'])

        site_confidentials = map(self.process_confidentials_response, site_confidentials_response)

        handshake_response = dict(SiteName=site_info['SiteName'],
                                  SiteID=site_by_serial_info['SiteID'],
                                  DeviceRoleID=site_by_serial_info.get('DeviceRoleID'),
                                  StatusID=site_by_serial_info['StatusID'],
                                  RouterSSID=site_info['RouterSSID'],
                                  GMTOffset=PST_OFFSET + (
                                          int(site_info['TimeZone']) * MULTIPLICATION_FACTOR),
                                  Latitude=site_info['latitude'],
                                  Longitude=site_info['longitude'],
                                  IsTabletEnabled=site_info['IsTabletEnabled'],
                                  SiteIPAddress=site_info['SiteIPAddress'],
                                  Certificates=site_certificates,
                                  Confidentials=site_confidentials,
                                  Secure=site_secure,
                                  Settings=site_settings
                                  )

        return handshake_response, True

    def check_empty_response(self, response, check_reponse=False):
        """

        :param response:
        :param check_reponse:
        :return:
        """
        if response == {} and check_reponse:
            CHEERIOS_UTILS.log_error(404, "Value not found")
            raise NotFoundError("Value not found")
        return response

    def process_confidentials_response(self, confidential):
        """
        Transforms the confidentials response from the database
        to the expected response
        :param confidential: response list from the db
        :return: transformed list of confidentials
        """
        confidential_mapped = dict()
        confidential_mapped['alias'] = confidential.get('Name')
        confidential_mapped[confidential.get('HashMethod')] = confidential.get('ConfigHash')
        return confidential_mapped


class SiteHubUpdateViewSet(ApiViewInterface):
    """
    Updates the sitehub device details
    """

    @jwt_decorator(URL['sitehub_update'], 'POST')
    def post(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().post(request)

    def get_serializer(self):
        """
        """
        return SiteHubUpdateViewSetSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """

        firmware_release_id = "NULL"
        software_release_id = "NULL"  # Dummy s/w release id since database returning empty response
        site_id = "NULL"

        data_string = "@SiteHubSoftwareRelease='{}'".format(data.get('SoftwareVersion'))
        software_release_response = MANIFEST_OBJ.execute_stored_procedure(
            data_string, QBERT_STORED_PROCEDURE_MAPPER['sitehub_software_release'])

        if software_release_response:
            software_release_id = software_release_response[0].get('SiteHubSoftwareReleaseID')

        data_string = "@Manufacturer='{0}', @Model='{1}', @Display='{2}', " \
                      "@SDKVersion='{3}', @OSVersion='{4}'".format(data.get('FirmwareManufacturer'),
                                                                   data.get('FirmwareModel'),
                                                                   data.get('FirmwareDisplay'),
                                                                   data.get('FirmwareSDKVersion'),
                                                                   data.get('FirmwareOSVersion'))

        firmware_release_response = MANIFEST_OBJ.execute_stored_procedure(
            data_string, QBERT_STORED_PROCEDURE_MAPPER[ 'sitehub_firmware_release'])

        if firmware_release_response:
            firmware_release_id = firmware_release_response[0].get('SiteHubFirmwareReleaseID')

        data_string = "@SerialNumber='{}'".format(data.get('SerialNumber'))

        device_data_response = MANIFEST_OBJ.execute_stored_procedure(data_string,
                                                                     QBERT_STORED_PROCEDURE_MAPPER
                                                                     ['get_sitehub_device'])

        if not device_data_response:
            return dict(error=1, content=("No Device found for the passed serial number", 400))

        current_date = str(datetime.datetime.now()).replace(" ", "T")

        data_string = "@SerialNumber='{0}', " \
                      "@CurrentSiteHubSoftwareReleaseID={1}, " \
                      "@CurrentSiteHubFirmwareReleaseID={2}, " \
                      "@DeviceIdentifier='{3}', " \
                      "@HandshakeDate='{4}', " \
                      "@DataFreeBytes={5}, " \
                      "@DataTotalBytes={6}".format(data.get('SerialNumber'),
                                                   software_release_id,
                                                   firmware_release_id,
                                                   data.get('DeviceIdentifier'),
                                                   current_date[:-3],
                                                   data.get('DataFreeBytes'),
                                                   data.get('DataTotalBytes'))

        sitehub_device_update_response = MANIFEST_OBJ.execute_stored_procedure(
            data_string, QBERT_STORED_PROCEDURE_MAPPER['update_sitehub_device'])

        site_id = device_data_response[0].get('SiteID')

        data_string = "@SiteID={0}, @InternalIP='{1}', @WAPModelFirmware='{2}'".format(site_id,data.get('SiteIPAddress'),data.get('WAPModelFirmware'))

        site_hardware_save_response = MANIFEST_OBJ.execute_stored_procedure(
            data_string, QBERT_STORED_PROCEDURE_MAPPER['save_site_hardware'],database=MSSQLBROADCAST)

        if site_hardware_save_response and sitehub_device_update_response:
            return "SiteHub Device Successfully Updated"


class BuildDataViewSet(ApiViewInterface):
    """
    Posts the build data for the Sitehub APK builds to the database
    """

    @jwt_decorator(URL['build_data'], 'POST')
    def post(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().post(request)

    def get_serializer(self):
        """
        """
        return BuildDataViewSetSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """

        data_string = "@SiteHubSoftwareFileName='{0}', " \
                      "@FeatureGroups='{1}', " \
                      "@ManifestFileID={2}, " \
                      "@VersionName='{3}', " \
                      "@BuildType='{4}', " \
                      "@VersionCode='{5}', " \
                      "@Branch='{6}', " \
                      "@MinimumSDKVersion={7}, " \
                      "@MaximumSDKVersion={8}, " \
                      "@RunDependencies='{9}', " \
                      "@GitCommit='{10}', " \
                      "@GitURL='{11}', " \
                      "@MavenCoordinate='{12}', "\
                      "@IsDeprecated={13}".format(data.get('sitehubSoftwareFileName'),
                                                       data.get('featureGroups'),
                                                       data.get('manifestFileId'),
                                                       data.get('versionName'),
                                                       data.get('buildType'),
                                                       data.get('versionCode'),
                                                       data.get('branch'),
                                                       data.get('minimumSDKVersion'),
                                                       data.get('maximumSDKVersion'),
                                                       data.get('runDependencies'),
                                                       data.get('gitCommit'),
                                                       data.get('gitURL'),
                                                       data.get('mavenCoordinate'),
                                                       data.get('isDeprecated'))

        result = MANIFEST_OBJ.execute_stored_procedure(
            data_parameter=data_string,
            stored_procedure=QBERT_STORED_PROCEDURE_MAPPER['save_build_data'])

        if result == True:
            return "JSON build data successfully stored"


class PlayerAchievementViewSet(ApiViewInterface):
    """
    Gets the player achievement count when the player id and the achievement id is passed
    """

    @jwt_decorator(URL['sitehub_player_achievement'], 'GET')
    def get(self, request):
        """
        Get API to fetch handshake data.
        """
        self.request_method = request.method
        return super().get(request)

    @jwt_decorator(URL['sitehub_player_achievement'], 'POST')
    def post(self, request):
        """

        :param request:
        :return:
        """
        self.request_method = request.method
        return super().post(request)

    def get_serializer(self):
        """
        Return serializer
        """
        if self.request_method == 'GET':
            return PlayerAchievementGETSerializer
        return PlayerAchievementPOSTSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """

        NO_PLAYER_ERROR = ("Player Does not exist", 400)
        NO_ACHIEVEMENT_ERROR = ({"EarnedCount": 0}, 200)

        if self.request_method == 'GET':
            login_interface = LoginViewSet()
            player_exists = login_interface.get_player_profile(data.get('player_id'))

            if not player_exists:
                return dict(error=1, content=NO_PLAYER_ERROR), True

            param_string = "@NamespaceID=1," \
                           "@NamespaceUserID={0}," \
                           "@AchievementID={1}".format(data.get('player_id'),
                                                      data.get('achievement_id'))
            response = MANIFEST_OBJ.execute_stored_procedure(param_string,
                                                         QBERT_STORED_PROCEDURE_MAPPER['get_player_achievement_count'],
                                                         database=MSSQLUSERDB)

            if not response:
                return dict(error=1, content=NO_ACHIEVEMENT_ERROR), True

            return response, True

        reference_data = "QuestionID={}".format(data.get('question_id'))
        param_dict = dict(
            NamespaceID=1,
            AchievementPlatformID=1,
            ReferenceData=reference_data,
            SiteID=data.get('site_id'),
            ClientDate=data.get('client_date'),
            GameNameID=data.get('game_name_id'),
            NamespaceUserID=data.get('player_id'),
            AchievementDate=data.get('client_date'),
            AchievementID=data.get('achievement_id'),
        )

        result = MANIFEST_OBJ.execute_stored_procedure(data_parameter=param_dict,
                                                       stored_procedure=QBERT_STORED_PROCEDURE_MAPPER['save_player_achievement_count'],
                                                       database=MSSQLUSERDB)
        if result:
            return "Achievement Saved."

    def format_response(self, data):
        """
        Format Response to remove the outer list block
        :param data:
        :return:
        """
        if data:
            return data[0]
        return data


class SupervisorViewSet(ApiViewInterface):
    """
    The Base class for the supervisor schedule APIs
    """

    def prepare_response_dict(self, table_list, schedule_id, schedule_ancillary=False):
        """
        Method which is directly called from the child functions
        and creates the thread pool to
        call the SPs
        :return:
        """
        self.schedule_id = schedule_id
        self.schedule_ancillary = schedule_ancillary

        schedule_response_dict = dict(TAG="DbModel", _tables=[])

        # Creating thread pool of 10 threads to execute 10 SPs simultaneously
        pool = ThreadPool(10)
        schedule_response_dict['_tables'] = pool.map(self.loop_table_dict, table_list)

        return schedule_response_dict

    def loop_table_dict(self, table):
        """
        Loops over the table dictionary and creates the base JSON
        :return:
        """
        db = MSSQLDBPROG

        if self.schedule_ancillary:
            db = MSSQLDBGAMEMGMT

        conn = pymssql.connect(user=MSSQLUSER, server=MSSQLHOST,
                               database=db, password=MSSQLPASSWORD,
                               timeout=10, as_dict=False)

        cursor = conn.cursor()

        table_dict = dict(_deleteRows=[],
                          _insertRows=self.get_insert_rows_data(self.get_table_data(table['name'], cursor), table['keys']),
                          _tableName=table['name'],
                          _updateRows=[])
        cursor.close()
        conn.close()
        return table_dict

    def get_table_data(self, table_name, cursor):
        """
        Makes the actual SP call
        :param table:
        :return:
        """
        sp_dict_key = "{}_table_data".format(table_name)
        stored_procedure = QBERT_STORED_PROCEDURE_MAPPER[sp_dict_key]

        self.call_storeprocedure(stored_procedure, cursor, "@SiteHubScheduleID={}".format(self.schedule_id))

        return self.fetchall_from_cursor(cursor)

    def get_insert_rows_data(self, table_data, keys):
        """
        Creates the internal JSON as specified
        :return:
        """
        insert_rows_list = []

        id_value=1
        for row in table_data:
            if self.schedule_ancillary:
                game_rec_val = row.get('GameRecID')
                value_type = None
                if isinstance(game_rec_val, int):
                    value_type = "INTEGER"
                elif isinstance(game_rec_val, str):
                    value_type = "TEXT"
                game_rec_id = dict(_name='gameRecId', _operator="", _type=value_type, _value=str(game_rec_val))
                for key, value in row.items():
                    row_dict = dict(_keys=[], _properties=[])
                    if key != 'GameRecID' and value != None:
                        self.create_dict_in_list("_id", str(id_value), '_keys', row_dict, "INTEGER", "=")
                        id_value = id_value + 1
                        row_dict['_properties'].append(game_rec_id)

                        #parameterName
                        self.create_dict_in_list("parameterName", key, '_properties', row_dict)

                        #parameterValue
                        self.create_dict_in_list("parameterValue", str(value), '_properties', row_dict)

                        #parameterType
                        if isinstance(value, int):
                            parameter_type_value="Int"
                        if isinstance(value, str):
                            parameter_type_value = "String"
                        self.create_dict_in_list("parameterType", parameter_type_value, '_properties', row_dict)

                        insert_rows_list.append(row_dict)
            else:
                row_dict = dict(_keys=[], _properties=[])
                for key, value in row.items():
                    value_type = None
                    if isinstance(value, int):
                        value_type = "INTEGER"
                    if isinstance(value, str):
                        value_type = "TEXT"
                    item_dict = dict(_name=key, _operator="", _type=value_type, _value=value)
                    if keys and key == '_id':
                        row_dict['_keys'].append(item_dict)
                        continue
                    row_dict['_properties'].append(item_dict)

                insert_rows_list.append(row_dict)


        return insert_rows_list

    def create_dict_in_list(self, name, value, key, row_dict, value_type="TEXT", operator=""):
        item_dict = dict(_name=name, _operator=operator, _type=value_type, _value=value)
        row_dict[key].append(item_dict)

    def get_site_schedule(self, site_id):
        """
        Gets the site schedule for the passed site id
        :param site_id:
        :return:
        """
        stored_procedure = QBERT_STORED_PROCEDURE_MAPPER['get_site_schedule']

        return MANIFEST_OBJ.execute_stored_procedure(data_parameter="@SiteID={}".format(site_id),
                                              stored_procedure=stored_procedure)


class ScheduleViewSet(SupervisorViewSet):
    """
    Supervisor Schedule Viewset
    """

    @jwt_decorator(URL['supervisor_schedule'], 'GET')
    def get(self, request):
        """
        Gets the params
        :param request:
        :return:
        """
        return super().get(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return SupervisorScheduleSerializer

    def get_from_downstream(self, data):
        """
        This is the actual business logic
        :param data:
        :return:
        """
        TABLE_LIST = [dict(name='Segments', keys=False),
                      dict(name='BlockSegments', keys=True),
                      dict(name='Blocks', keys=False),
                      dict(name='ChannelTypes', keys=False),
                      dict(name='Channels', keys=False),
                      dict(name='RollBlocks', keys=True),
                      dict(name='Rolls', keys=False),
                      dict(name='SegmentPackages', keys=False),
                      dict(name='SegmentParameters', keys=True),
                      dict(name='SegmentTypes', keys=False)]

        serial_number = data.get("site_id")
        schedule_id = data.get("schedule_id")
        schedule_date = data.get("schedule_date")
        db_schedule_change_date = None

        site_schedule = self.get_site_schedule(serial_number)

        if site_schedule:

            # Checking if passed site id is the same as in db, returning 304 in this case
            if site_schedule[0]['ScheduleChangeDate']:
                db_schedule_change_date = site_schedule[0]['ScheduleChangeDate'].strftime('%Y-%m-%dT%H:%M:%S')

            if site_schedule[0]['SiteHubScheduleID'] == int(schedule_id) and db_schedule_change_date == schedule_date:
                return dict(error=1, content=("", status.HTTP_304_NOT_MODIFIED)), True
        else:
            return dict(error=1, content=("Schedule not found with the passed ID", status.HTTP_400_BAD_REQUEST)), True

        return self.prepare_response_dict(TABLE_LIST, schedule_id), True



class ScheduleAncillaryData(SupervisorViewSet):
    """
    Schedule Ancillary Data Viewset
    """

    @jwt_decorator(URL['schedule_ancillary_data'], 'GET')
    def get(self, request):
        """
        Gets the params
        :param request:
        :return:
        """
        return super().get(request)

    def get_serializer(self):
        """
        Returns the corresponding serializer
        :return:
        """
        return ScheduleAncillaryDataSerializer

    def get_from_downstream(self, data):
        """
        This is the actual business logic
        :param data:
        :return:
        """
        db_name = data.get('db_name')
        if db_name not in ALLOWED_DB_MAPPER.keys():
            return dict(error=1, content=("DB Not Found", status.HTTP_400_BAD_REQUEST)), True

        TABLE_LIST = ALLOWED_DB_MAPPER[db_name]
        schedule_id = data.get("schedule_id")

        return self.prepare_response_dict(TABLE_LIST, schedule_id, schedule_ancillary=True), True


class PlayerPlusPoints(ApiViewInterface):
    """
    Player Plus Points Viewset
    """

    @jwt_decorator(URL['playerplus_points'], 'POST')
    def post(self, request):
        """
        Posts the data
        """
        return super().post(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return PlayerPlusPointsSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """
        param_string = "@PlayerID={0}," \
                       "@PointsAwarded={1}," \
                       "@PointsAwardTypeID={2}," \
                       "@PointsAwardReferenceID={3}," \
                       "@PointsAwardDate='{4}'".format(data.get('player_id'),
                                                       data.get('points_awarded'),
                                                       data.get('points_award_type_id'),
                                                       data.get('reference_id'),
                                                       data.get('award_date')
                                                       )

        MANIFEST_OBJ.execute_stored_procedure(data_parameter=param_string,
                                              stored_procedure=QBERT_STORED_PROCEDURE_MAPPER['save_player_plus_points'],
                                              database=MSSQLPLAYERPLUSDB)

        # Return success message if the code flow reached to this point
        return "PlayerPlus points awarded successfully."


class ScoreViewSet(ApiViewInterface):
    """
    Viewset for scores of an airing
    """

    @jwt_decorator(URL['score'], 'POST')
    def post(self, request):
        """
        To POST the scores into the database
        :param request:
        :return:
        """
        return super().post(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return ScoreSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and save data.
        """
        game_start_time = data['network_airing']['game_start_time']
        site_id = data['site_id']
        game_id = data['network_airing']['game_id']
        game_name_id = data['network_airing']['game_name_id']

        for player_score in data.get('player_scores'):
            player_id = player_score['player']['player_id']
            handle = player_score['player']['handle']
            pm_num = player_score['player']['pm_num']
            score = player_score['score']
            response_count = player_score['response_count']
            channel = player_score['channel']
            duration = player_score['duration']
            repeat_game = player_score['repeat_game']
            playmaker_type_id = player_score['playmaker_type_id']
            session_guid = player_score.get('session_guid')

            param_string = "@PlayerID={0}, " \
                           "@Handle={1}, " \
                           "@GameStartTime='{2}', " \
                           "@SiteID={3}, " \
                           "@GameID={4}, " \
                           "@GameNameID={5}, " \
                           "@PMNum={6}, " \
                           "@Score={7}, " \
                           "@ResponseCount={8}, " \
                           "@Channel={9}, " \
                           "@Duration={10}, " \
                           "@RepeatGame={11}, " \
                           "@PlaymakerTypeID={12}".format(player_id,
                                                          handle,
                                                          game_start_time,
                                                          site_id,
                                                          game_id,
                                                          game_name_id,
                                                          pm_num,
                                                          score,
                                                          response_count,
                                                          channel,
                                                          duration,
                                                          repeat_game,
                                                          playmaker_type_id)

            if session_guid:
                param_string = param_string + ", @SessionGUID='{}'".format(session_guid)
            else:
                param_string = param_string + ", @SessionGUID=NULL".format(session_guid)

            try:
                MANIFEST_OBJ.execute_stored_procedure(data_parameter=param_string,
                                                      stored_procedure=QBERT_STORED_PROCEDURE_MAPPER['insert_gameplay'],
                                                      database = MSSQLDBGAMEPLAY,
                                                      transaction = True)
            except pymssql.IntegrityError as ex:
                Logger.log.error(str(ex))
                return dict(error=1, content=("Error: Duplicate Values not allowed.", status.HTTP_400_BAD_REQUEST))

        return "GamePlay data stored successfully."


class SiteGameQuestion(ApiViewInterface):
    """
    Site Game Question Viewset
    """

    @jwt_decorator(URL['site_game_question'], 'POST')
    def post(self, request):
        """
        POST API to save the Site Game Question
        """
        return super().post(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return SiteGameQuestionSerializer

    def get_from_downstream(self, data):
        """
        Call
        stored
        procedure and saves
        data.
        """

        for question in data.get('question_list'):
            data_string = "@SiteID={0}, " \
                          "@GameNameID={1}, " \
                          "@GameStartTime='{2}', " \
                          "@BuzztimeNetworkID={3}, " \
                          "@QuestionID={4}, " \
                          "@ResponseCount={5}, " \
                          "@TotalPoints={6} " .format(data.get('site_id'),
                                                      data.get('game_name_id'),
                                                      data.get('game_start'),
                                                      data.get('network_id'),
                                                      question.get('question_id'),
                                                      question.get('total_responses'),
                                                      question.get('total_points'))

            try:
                MANIFEST_OBJ.execute_stored_procedure(data_parameter=data_string,
                                                      stored_procedure=QBERT_STORED_PROCEDURE_MAPPER['save_site_game_question'],
                                                      database=MSSQLDBGAMEPLAY,
                                                      transaction=True)
            except pymssql.IntegrityError as ex:
                Logger.log.error(str(ex))
                return dict(error=1, content=("Error: Duplicate Values not allowed.", status.HTTP_400_BAD_REQUEST))

        return "Question Data Successfully Saved."


class Question(ApiViewInterface):
    """
    Viewset for Questions CRUD operations
    """

    @jwt_decorator(URL['question'], 'POST')
    def post(self, request):
        """
        Method to POST the Questions data
        Question Serializer
        """
        return super().post(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return PlayerGameQuestionSerializer

    def get_from_downstream(self, data):
        """
        Business logic to call the SP and save the data
        :param data:
        :return:
        """
        data_string = "@PlayerID={0}, " \
                      "@SiteID={1}, " \
                      "@GameNameID={2}, " \
                      "@GameID={3}, " \
                      "@QuestionID={4}, " \
                      "@AnswerID={5}, " \
                      "@Handle={6}, " \
                      "@PMNum={7}, " \
                      "@Score={8}, " \
                      "@InteractiveAdSkinID={9}".format(data.get('player_id'),
                                                        data.get('site_id'),
                                                        data.get('game_name_id'),
                                                        data.get('game_id'),
                                                        data.get('question_id'),
                                                        data.get('answer_index'),
                                                        data.get('handle'),
                                                        data.get('pm_num'),
                                                        data.get('score'),
                                                        data.get('interactive_ad_skin_id'))

        if data.get('answer_date'):
            data_string = data_string + ", @AnswerDate='{}'".format(data.get('answer_date'))
        else:
            data_string = data_string + ", @AnswerDate=NULL"

        if data.get('game_start_time'):
            data_string = data_string + ", @GameDate='{}'".format(data.get('game_start_time'))
        else:
            data_string = data_string + ", @GameDate=NULL"

        if data.get('question_number'):
            data_string = data_string + ", @QuestionNumber='{}'".format(data.get('question_number'))



        result = MANIFEST_OBJ.execute_stored_procedure(data_parameter=data_string,
                                                       stored_procedure=QBERT_STORED_PROCEDURE_MAPPER[
                                                           'insert_player_game_question'], database=MSSQLDBGAMEPLAY)

        if not result:
            return "Question data successfully saved"
