"""
Common functions used in test cases
"""
def response_data(response):
    """
    Common function which send response content from response.
    :param response:
    :return:
    """
    return response.content

def response_status(response):
    """
    Common function which send response status code from response.
    """
    return response.status_code
