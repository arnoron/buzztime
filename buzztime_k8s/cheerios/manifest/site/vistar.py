import requests
import time
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.manifest.constant import URL
from cheerios.manifest.site.constants import QBERT_STORED_PROCEDURE_MAPPER
from cheerios.manifest.site.serializers import VistarAssetsSerializer, ProofOfPlaySerializer, \
    ProofOfPlayDBPostSerializer
from cheerios.manifest.site.views import MANIFEST_OBJ
from cheerios.players.decorators import close_connection
from cheerios.services.jwt_utils import jwt_decorator
from cheerios.services.log_classifier import Logger
from cheerios.settings import MSSQLDBPROG
from requests.exceptions import ConnectionError

error_messages = ['Invalid Parameters Passed',
                  "Vistar Assets could not be saved.",
                  "Could not retrieve vistar Asset.",
                  "Error While Executing Stored Procedure",
                  "Internal Server Error. Please try after some time."]


class VistarAssets(MSSQLApiView):
    """
    Viewset to manipulate Vistar Assets sent from SiteHub to the DB
    """
    def init_vistar_assets(self):
        """

        :return:
        """

        serializer = VistarAssetsSerializer(data=self.request_data)

        # Check if Input Parameters are incorrect or missing
        if not serializer.is_valid():
            Logger.log.error("Data Passed: {}".format(self.request_data))
            raise ValueError(error_messages[0])

        save_vistar_response, success = self.save_vistar_assets()

        if not success:
            Logger.log.error("Data Passed: {}".format(self.request_data))
            raise Exception(save_vistar_response)

        # If success, then save vistar response will be the vistar asset id
        vistar_asset_id = save_vistar_response

        self.parameters = "@SiteID={}, @VistarAssetID={}".format(self.request_data.get('siteID'),
                                                                 vistar_asset_id)

    @jwt_decorator(URL['vistar_assets'], 'POST')
    @close_connection
    def post(self, request):
        """
        Method which posts vistar data to the database
        :param request:
        :return:
        """
        self.request_data = request.data

        self.init_vistar_assets()
        self.execute_stored_procedure('insert_site_vistar_assets', MSSQLDBPROG, self.parameters)

        # Successful Insertion of Vistar Assets
        return Response("Vistar Assets Added", status.HTTP_200_OK)

    @jwt_decorator(URL['vistar_assets'], 'DELETE')
    @close_connection
    def delete(self, request):
        """

        :param request:
        :return:
        """

        self.request_data = request.data

        self.init_vistar_assets()
        self.execute_stored_procedure('delete_site_vistar_assets', MSSQLDBPROG, self.parameters)

        # Successful Deletion of Vistar Assets
        return Response("Vistar Assets Successfully Deleted", status.HTTP_200_OK)

    def save_vistar_assets(self):
        """
        Saves the vistar assets in the dbo.VistarAssets table by calling the SP
        :return:
        """
        parameters = "@FileName='{}', @FilePath='{}'".format(self.request_data.get('fileName').replace("'", "''"),
                                                             self.request_data.get('filePath').replace("'", "''"))
        result = self.execute_stored_procedure('save_vistar_assets', MSSQLDBPROG, parameters)

        # The Stored Procedure returns a blank response
        if not result:
            return error_messages[1], False

        vistar_asset_id = result[0].get('VistarAssetID')

        # The Stored Procedure response does not have a vistar asset ID
        if not vistar_asset_id:
            return error_messages[2], False

        return vistar_asset_id, True

    def execute_stored_procedure(self, stored_procedure, database, parameters):
        """
        Method which executes a stored procedure and returns the result
        :param stored_procedure: The stored procedure to be executed
        :param database: The database to which the stored procedure belongs
        :param parameters: The parameters to pass with the Stored Procedure
        :return: The query set response from the Stored Procedure
        """
        sp_name = QBERT_STORED_PROCEDURE_MAPPER[stored_procedure]
        cursor = self.get_cursor(database, transaction=True)

        self.call_storeprocedure(sp_name, cursor, parameters)

        result = self.fetchall_from_cursor(cursor)
        return result


class ProofOfPlayViewset(APIView):
    """

    """

    def __init__(self):
        """
        Initializing constants for the class
        """
        self.SLEEP_TIME = 1
        self.MAX_RETRIES = 5
        self.FORCE_RETRY_STATUS_CODES = [408, 500, 503]
        self.SUCCESS_CODE = 200

    @jwt_decorator(URL['pop_validation'], 'POST')
    def post(self, request):
        """
        Executing Proof Of Play for Vistar Assets
        :param request:
        :return:
        """
        retries = 0
        request_data = request.data
        pop_url = request_data.get('pop_url')

        serializer = ProofOfPlaySerializer(data=request_data)

        if not serializer.is_valid():
            Logger.log.error("Data Passed: {}".format(request))
            raise ValueError(error_messages[0])

        # Save PoP Data to the DB
        if ProofOfPlayDBPostSerializer(data=request_data).is_valid():
            parameters = "@SiteID='{}', @VistarAdID='{}', @VistarCampaignID='{}', @AdType='{}'," \
                         "@RunDate='{}', @ChannelNumber='{}', @PlayerCount='{}'".format(request_data.get('site_id'),
                                                                                    request_data.get('vistar_ad_id'),
                                                                                    request_data.get('vistar_campaign_id'),
                                                                                    request_data.get('ad_type'),
                                                                                    request_data.get('run_date'),
                                                                                    request_data.get('channel_number'),
                                                                                    request_data.get('player_count'))
            stored_procedure = QBERT_STORED_PROCEDURE_MAPPER['save_pop_data']
            MANIFEST_OBJ.execute_stored_procedure(parameters, stored_procedure)

        try:
            pop_response = requests.get(pop_url)
        except ConnectionError:
            # Return Error if invalid URL is passed
            Logger.log.error("URL={}".format(pop_url))
            Logger.log.error("Invalid URL passed. Error Code=400")
            return Response("Invalid URL passed", status=400)
        except Exception as ex:
            Logger.log.error(ex)
            return Response("Connection Error: {}".format(str(ex)))

        # Retry logic for the proof of play API call
        while pop_response.status_code in self.FORCE_RETRY_STATUS_CODES and retries < self.MAX_RETRIES:
            time.sleep(self.SLEEP_TIME)
            retries += 1
            pop_response = requests.get(pop_url)

        # Return Bad request for errors which do not need retry
        if pop_response.status_code is not self.SUCCESS_CODE and pop_response.status_code not in self.FORCE_RETRY_STATUS_CODES:
            Logger.log.error("URL={}".format(pop_url))
            Logger.log.error("Error Message={}".format(pop_response.content))
            return Response("Bad Request", status=400)

        # Return Internal Server Error if max retry is exceeded
        if retries == self.MAX_RETRIES:
            Logger.log.error("URL={}".format(pop_url))
            Logger.log.error("Max Retries Exceeded. Response: {}".format(pop_response.content))
            return Response("Internal Server Error. Please try after some time.", 500)

        # Return success if code flow reached this point
        return Response("Proof of Play successfully Executed", 200)
