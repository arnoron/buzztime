import json
import requests
import mock

from pymssql import DatabaseError
from rest_framework import status
from rest_framework.test import APITestCase
from mock import patch, Mock

from cheerios.analytics.test_data import response_content, get_jwt_token, get_jwt_token_delete
from cheerios.test_utils import mock_test_utils

MOCK_TEST_RESPONSES = dict(save_vistar_assets_no_response=[],
                           save_vistar_assets_no_id=[{'VistarAssetID': None}],
                           save_vistar_assets_correct_id=[{'VistarAssetID': 4}],
                           insert_delete_site_vistar_assets=[])


class VistarAssetsTest(APITestCase):
    """
    The test cases for the Vistar Assets Viewset
    """
    def setUp(self):
        """
        An initialization method to initialize the API Endpoint for these tests
        :return: None
        """
        self.api_endpoint = "/manifest/site/vistar-assets/"

    def post_request(self, data):
        """
        Method which makes a post API call
        :param data: The data with which to make a post call
        :return: The response data and the response status code of the response
        """
        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:vistar"), format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        return response_data, response.status_code

    def delete_request(self, data):
        """
        Method which makes a delete API call
        :param data: The data with which to make a delete call
        :return: The response data and the response status code of the response
        """
        response = self.client.delete(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token_delete("manifest:qbert:vistar"), format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        return response_data, response.status_code

    def test_serializer_not_valid(self):
        """
        Tests the case when the serializer is invalid
        :return:
        """
        data=dict(fileName="test_file_name_3",
	              filePath="test_file_path"
                  )

        response_data, response_status = self.post_request(data)

        self.assertEqual(response_data, "ValueError Invalid Parameters Passed")
        self.assertEqual(response_status, status.HTTP_400_BAD_REQUEST)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_TEST_RESPONSES['save_vistar_assets_no_response'])))
    def test_save_vistar_assets_no_response(self):
        """
        Tests when save vistar assets return no response
        :return:
        """
        data = dict(fileName="test_file_name_3",
                    filePath="test_file_path",
                    siteID=1235
                    )

        response_data, response_status = self.post_request(data)

        self.assertEqual(response_data, "Error: Vistar Assets could not be saved.")
        self.assertEqual(response_status, status.HTTP_500_INTERNAL_SERVER_ERROR)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(
               side_effect=mock_test_utils.mock_fetch_responses(MOCK_TEST_RESPONSES['save_vistar_assets_no_id'])))
    def test_no_vistar_assets_id(self):
        """
        Tests when no vistar assets id is returned
        :return:
        """
        data = dict(fileName="test_file_name_3",
                    filePath="test_file_path",
                    siteID=1235
                    )

        response_data, response_status = self.post_request(data)

        self.assertEqual(response_data, "Error: Could not retrieve vistar Asset.")
        self.assertEqual(response_status, status.HTTP_500_INTERNAL_SERVER_ERROR)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_TEST_RESPONSES['save_vistar_assets_correct_id'],
                                                                MOCK_TEST_RESPONSES['insert_delete_site_vistar_assets'])))
    def test_successful_insertion(self):
        """
        Tests when the insertion is successful
        :return:
        """
        data = dict(fileName="test_file_name_3",
                    filePath="test_file_path",
                    siteID=1235
                    )

        response_data, response_status = self.post_request(data)

        self.assertEqual(response_data, "Vistar Assets Added")
        self.assertEqual(response_status, status.HTTP_200_OK)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=DatabaseError('Adaptive Server Timeout')))
    def test_sp_internal_server_error(self):
        """
        Tests when the stored procedure execution raises an internal server error
        :return:
        """
        data = dict(fileName="test_file_name_3",
                    filePath="test_file_path",
                    siteID=1235
                    )

        response_data, response_status = self.post_request(data)
        self.assertEqual(response_data, "Error: Internal Server Error")
        self.assertEqual(response_status, status.HTTP_500_INTERNAL_SERVER_ERROR)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_TEST_RESPONSES['save_vistar_assets_correct_id'],
                                                                 MOCK_TEST_RESPONSES[
                                                                     'insert_delete_site_vistar_assets'])))
    def test_successful_deletion(self):
        """
        Tests when the insertion is successful
        :return:
        """
        data = dict(fileName="test_file_name_3",
                    filePath="test_file_path",
                    siteID=1235
                    )

        response_data, response_status = self.delete_request(data)

        self.assertEqual(response_data, "Vistar Assets Successfully Deleted")
        self.assertEqual(response_status, status.HTTP_200_OK)


class ProofOfPlayTestCase(APITestCase):
    """
    Test cases for the Proof Of Play API
    """


    STATUS_CODES = dict(internal_server_error=500,
                        bad_request=400,
                        success=200)

    def send_vistar_request(self, send_data=True, save_to_db=False, invalid_date=False):
        """
        Method to send vistar request with a dummy URL
        :return:
        """
        request_data = dict(pop_url='test_url') if send_data else dict()

        if save_to_db:
            request_data = dict(pop_url='test_url',
                                site_id=1,
                                vistar_ad_id='sample_Ad_id',
                                vistar_campaign_id='sample_campaign_id',
                                ad_type='sameple_ad_type',
                                run_date='2018-05-15T01:01:04',
                                channel_number=15,
                                player_count=10,
                                tablet_count=24)
            if invalid_date:
                request_data['run_date'] = 'invalid'

        return self.client.post("/manifest/site/execute_vistar_pop/",
                                request_data,
                                HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:proof-of-play"),
                                format="json")

    def mock_vistar_response(self, response_status_code=500):
        """
        Mock for the requests get method
        :return:
        """
        request_object = requests.models.Response
        request_object.status_code = response_status_code
        return request_object

    def test_no_url_passed(self):
        """
        Case when no url is passed in the post request body
        :return:
        """
        response = self.send_vistar_request(send_data=False)
        self.assertEqual("ValueError Invalid Parameters Passed",
                         json.loads(response_content(response).decode("utf-8")))

    @mock.patch('requests.get')
    def test_first_time_success(self, mock_vistar):
        """
        Case when first time is the success response
        :return:
        """
        mock_vistar.side_effect = [self.mock_vistar_response(response_status_code=self.STATUS_CODES['success'])]
        response = self.send_vistar_request()
        self.assertEqual("Proof of Play successfully Executed",
                         json.loads(response_content(response).decode("utf-8")))

    @mock.patch('requests.get', side_effect=mock_vistar_response)
    def test_retries_exhausted(self, mock_vistar):
        """
        Case when all the retries are done with
        :return:
        """
        response = self.send_vistar_request()
        self.assertEqual("Internal Server Error. Please try after some time.",
                         json.loads(response_content(response).decode("utf-8")))

    @mock.patch('requests.get')
    def test_bad_request_from_vistar(self, mock_vistar):
        """
        Case when vistar execution returns a bad request, no retry should happen
        :return:
        """
        mock_vistar.side_effect = [self.mock_vistar_response(response_status_code=self.STATUS_CODES['bad_request'])]
        response = self.send_vistar_request()
        self.assertEqual("Bad Request",
                         json.loads(response_content(response).decode("utf-8")))

    @mock.patch('requests.get')
    def test_second_retry_responds_success(self, mock_vistar):
        """
        Case when first try fails but second try succeeds
        :return:
        """
        mock_vistar.side_effect = [self.mock_vistar_response(response_status_code=self.STATUS_CODES['internal_server_error']),
                                   self.mock_vistar_response(response_status_code=self.STATUS_CODES['success'])]
        response = self.send_vistar_request()
        self.assertEqual("Proof of Play successfully Executed",
                         json.loads(response_content(response).decode("utf-8")))

    @mock.patch('requests.get')
    def test_second_retry_responds_failure(self, mock_vistar):
        """
        Case when first try fails with retry code and the second try responds with a bad request
        :return:
        """
        mock_vistar.side_effect = [self.mock_vistar_response(response_status_code=self.STATUS_CODES['internal_server_error']),
                                   self.mock_vistar_response(response_status_code=self.STATUS_CODES['bad_request'])]
        response = self.send_vistar_request()
        self.assertEqual("Bad Request",
                         json.loads(response_content(response).decode("utf-8")))


    @mock.patch('cheerios.manifest.site.views.ManifestSiteViewSet.execute_stored_procedure')
    @mock.patch('requests.get')
    def test_save_to_db(self, mock_vistar, mock_execute_stored_procedure):
        """

        :param mock_vistar:
        :return:
        """
        mock_vistar.side_effect = [self.mock_vistar_response(response_status_code=self.STATUS_CODES['success'])]
        mock_execute_stored_procedure.side_effect = [True]
        response = self.send_vistar_request(save_to_db=True)
        self.assertEqual("Proof of Play successfully Executed",
                         json.loads(response_content(response).decode("utf-8")))

    @mock.patch('requests.get')
    def test_url_connection_error(self, mock_vistar):
        """

        :return:
        """
        mock_vistar.side_effect = requests.exceptions.ConnectionError()
        response = self.send_vistar_request()
        self.assertEqual("Invalid URL passed",
                         json.loads(response_content(response).decode("utf-8")))
