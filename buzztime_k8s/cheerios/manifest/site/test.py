"""
Test cases for site manifest
"""
import json
import pymssql

import mock
from mock import patch, Mock
from rest_framework import status
from rest_framework.test import APITestCase
from cheerios.analytics.test_data import response_content, get_jwt_token
from cheerios.manifest.site.test_data import response_data, response_status
from cheerios.manifest.test_data import get_jwt_with_expired_date, get_jwt_valid_data
from cheerios.settings import MSSQLDBPROG
from cheerios.test_utils import mock_test_utils
from .response_data import MOCK_RESPONSE
EXECUTE_STORED_PROCEDURE = 'cheerios.manifest.site.views.' \
                           'ManifestSiteViewSet.execute_stored_procedure'


def mocked_execute_stored_procedure(data_parameter, stored_procedure, database=MSSQLDBPROG):
    """
    Mocks the call stored procedure method
    :return:
    """
    if any(x in stored_procedure for x in ["upd", "sav"]):
        return True
    return []


class SoftwareViewSetTest(APITestCase):
    """
    Tests for software viewset
    """

    def setUp(self):
        """
        Initializing method
        :return:
        """
        self.api_endpoint = "/manifest/site/software/"

    def test_no_auth_header(self):
        """
        Tests when no auth header
        :return:
        """
        request_url = self.api_endpoint
        response = self.client.get("{0}?deviceId=1001".format(request_url))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "Error: JWT Token not found in request parameters nor headers")
        self.assertEqual(response_status(response), 400)

    def test_incorrent_auth_header(self):
        """
        Tests when incorrect auth header passed
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_with_expired_date(request_url)
        response = self.client.get("{0}?deviceId=1001".format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "Error: Invalid JWT token. JWT token expired")
        self.assertEqual(response_status(response), 400)

    def test_no_device_id(self):
        """
        Tests when no device id
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}".format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")
        self.assertEqual(response_status(response), 400)

    @mock.patch(EXECUTE_STORED_PROCEDURE,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['software'])))
    def test_successful_request(self):
        """
        Tests for successful request
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?deviceId=1001".format(request_url),
                                   HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue("softwareInfo" in json_response_data)
        self.assertTrue("software" in json_response_data)
        self.assertEqual(response_status(response), 200)


class ContentViewSetTest(APITestCase):
    """
    Tests for content viewset
    """

    def setUp(self):
        """
        Initializing method
        :return:
        """
        self.api_endpoint = "/manifest/site/content/"

    def test_no_auth_header(self):
        """
        Tests when no auth header
        :return:
        """
        request_url = self.api_endpoint
        response = self.client.get("{0}?deviceId=1001".format(request_url))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "Error: JWT Token not found in request parameters nor headers")
        self.assertEqual(response_status(response), 400)

    def test_incorrent_auth_header(self):
        """
        Tests when incorrect auth header passed
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_with_expired_date(request_url)
        response = self.client.get("{0}?deviceId=1001".format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "Error: Invalid JWT token. JWT token expired")
        self.assertEqual(response_status(response), 400)

    def test_no_device_id(self):
        """
        Tests when no device id
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}".format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")
        self.assertEqual(response_status(response), 400)

    @mock.patch(EXECUTE_STORED_PROCEDURE,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['content'])))
    def test_successful_request(self):
        """
        Tests for successful request
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?deviceId=123".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue("contentInfo" in json_response_data)
        self.assertTrue("content" in json_response_data)
        self.assertEqual(response_status(response), 200)

    @mock.patch(EXECUTE_STORED_PROCEDURE,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['content'])))
    def test_success_with_resolution(self):
        """
        Tests for successful request with resolution
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?deviceId=123".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue("contentInfo" in json_response_data)
        self.assertTrue("content" in json_response_data)
        self.assertEqual(response_status(response), 200)


class HandshakeViewSetTest(APITestCase):
    """
    Tests for handshake viewset
    """

    def setUp(self):
        """
        Initializing method
        :return:
        """
        self.api_endpoint = "/manifest/site/handshake/"

    def test_no_auth_header(self):
        """
        Tests when no auth header
        :return:
        """
        request_url = self.api_endpoint
        response = self.client.get("{0}?serialno=9999".
                                   format(request_url))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "Error: JWT Token not found in request parameters nor headers")
        self.assertEqual(response_status(response), 400)

    def test_incorrent_auth_header(self):
        """
        Tests when incorrect auth header passed
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_with_expired_date(request_url)
        response = self.client.get("{0}?serialno=9999".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "Error: Invalid JWT token. JWT token expired")
        self.assertEqual(response_status(response), 400)

    def test_no_serialno(self):
        """
        Tests when serial number is not passed
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")
        self.assertEqual(response_status(response), 400)

    def test_successful_request(self):
        """
        Tests for successful request
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?serialno=170501418600548".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue("SiteName" in json_response_data)
        self.assertTrue("Confidentials" in json_response_data)
        self.assertEqual(response_status(response), 200)

    @mock.patch(EXECUTE_STORED_PROCEDURE,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['empty_response'])))
    def test_serialno_not_found(self):
        """
        Tests when serialno is not found
        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?serialno=1000001".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data, "ValueError Value not found")
        self.assertEqual(response_status(response), 400)


class SiteHubUpdateViewSetTest(APITestCase):
    """
    Tests for handshake viewset
    """
    def setUp(self):
        """
        Initializing
        method
        :return:
        """
        self.api_endpoint = "/manifest/site/sitehub/"
        self.data = dict(SerialNumber="1234356677",
                          DeviceIdentifier="c2bad75745199482",
                          SoftwareVersion="1.00",
                          FirmwareManufacturer="Buzztime",
                          FirmwareModel="BZT-SH01",
                          FirmwareDisplay="1.2.0.1021 release-keys",
                          FirmwareSDKVersion="23",
                          FirmwareOSVersion="6.0.1",
                          DataFreeBytes="10101",
                          DataTotalBytes="1010101",
                          SiteIPAddress="10.10.10.1",
                          WAPModelFirmware="Ruckus H500 Multimedia Hotzone"
                                           " Wireless AP Version: 100.1.0.0.432"
                    )

    @mock.patch('cheerios.manifest.site.views.ManifestSiteViewSet.execute_stored_procedure',
                side_effect=mocked_execute_stored_procedure)
    def test_no_device(self, mocked_stored_procedure):
        """
        Tests the scenario when the sitehub data is successfully updated
        :return:
        """
        request_url = self.api_endpoint

        data = self.data

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:sitehub"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, 'No Device found for the passed serial number')

    def test_no_auth_header(self):
        """
        Tests when no auth header
        :return:
        """
        request_url = self.api_endpoint

        data = self.data

        response = self.client.post(request_url, data,
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, 'Error: JWT Token not found in request '
                                        'parameters nor headers')

    def test_incorrent_auth_header(self):
        """
        Tests when incorrect auth header passed
        :return:
        """
        request_url = self.api_endpoint

        data = self.data

        jwt = get_jwt_with_expired_date(request_url)
        response = self.client.post(request_url, data, HTTP_AUTHORIZATION=jwt,
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, 'Error: Invalid JWT token. JWT token expired')

    def test_no_device_id(self):
        """
        Tests when no device id is passed in the request
        :return:
        """
        request_url = self.api_endpoint

        data = self.data

        del data['DeviceIdentifier']

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:sitehub"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, 'ValueError Invalid Parameter passed')


class BuildDataViewSet(APITestCase):
    """
    Tests for build data viewset
    """

    def setUp(self):
        """
        Initializing
        method
        :return:
        """
        self.api_endpoint = "/manifest/site/build-data/"
        self.data = dict(sitehubSoftwareFileName="test_file_name_2",
                         featureGroups="test_feature_group",
                         manifestFileId=100,
                         versionName="test_version_name",
                         buildType="debug",
                         versionCode=23,
                         branch="test_branch",
                         minimumSDKVersion=200,
                         maximumSDKVersion=300,
                         runDependencies="test_dependencies",
                         gitCommit="test_commit",
                         gitURL="test_git_url",
                         mavenCoordinate="test_coordinates",
                         isDeprecated=0
                         )

    @mock.patch('cheerios.manifest.site.views.ManifestSiteViewSet.execute_stored_procedure',
                side_effect=mocked_execute_stored_procedure)
    def test_successful_updation(self, mocked_stored_procedure):
        """
        Tests the scenario when the sitehub data is successfully updated
        :return:
        """
        request_url = self.api_endpoint

        data = self.data

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:build-data"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertEqual(response_data, 'JSON build data successfully stored')


class PlayerAchievementTest(APITestCase):
    """
    Tests for build data viewset
    """

    def setUp(self):
        """
        Initializing
        method
        :return:
        """
        self.api_endpoint = "/manifest/site/player_achievement/"


    def test_player_id_not_passed_get(self):
        """

        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?achievement_id=123".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(json_response_data, "ValueError Invalid Parameter passed")

    def test_achievement_id_not_passed_get(self):
        """

        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?player_id=123".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(json_response_data, "ValueError Invalid Parameter passed")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['empty_response'])))
    def test_no_record_for_passed_input_get(self):
        """

        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?player_id=123&achievement_id=456".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(json_response_data, "Player Does not exist")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['player_details'],
                                                                 MOCK_RESPONSE['player_achievement_response'])))
    def test_record_exists_for_passed_input_get(self):
        """

        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?player_id=17594961&achievement_id=456".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertEqual(json_response_data, {'EarnedCount': 2})

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['player_achievement_response'])))
    def test_achievement_inserted(self):
        """

        :return:
        """
        request_url = self.api_endpoint

        data = dict(achievement_id=1,
                    client_date="2017-08-01T14:33:00",
                    game_name_id=1,
                    player_id=123,
                    question_id=45,
                    site_id=678)

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:player-achievement"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertEqual(response_data, "Achievement Saved.")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['player_achievement_response'])))
    def test_achievement_inserted_char_question_id(self):
        """

        :return:
        """
        request_url = self.api_endpoint

        data = dict(achievement_id=1,
                    client_date="2017-08-01T14:33:00",
                    game_name_id=1,
                    player_id=123,
                    question_id="45",
                    site_id=678)

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:player-achievement"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertEqual(response_data, "Achievement Saved.")

    def test_incorrect_date_time_format(self):
        """

        :return:
        """
        request_url = self.api_endpoint

        data = dict(achievement_id=1,
                    client_date="incorrect",
                    game_name_id=1,
                    player_id=123,
                    question_id="45",
                    site_id=678)

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:player-achievement"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "ValueError Invalid Parameter passed")

    def test_incorrect_parameter_type(self):
        """

        :return:
        """
        request_url = self.api_endpoint

        data = dict(achievement_id=1,
                    client_date="2017-08-01T14:33:00",
                    game_name_id=1,
                    player_id="abcd",
                    question_id="45",
                    site_id=678)

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:player-achievement"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "ValueError Invalid Parameter passed")

    def test_missing_fields(self):
        """

        :return:
        """
        request_url = self.api_endpoint

        data = dict(achievement_id=1,
                    client_date="2017-08-01T14:33:00",
                    player_id="abcd",
                    question_id="45",
                    site_id=678)

        response = self.client.post(request_url, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:player-achievement"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "ValueError Invalid Parameter passed")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['player_details'], [])))
    def test_no_achievement(self):
        """

        :return:
        """
        request_url = self.api_endpoint
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{0}?player_id=17594961&achievement_id=456".
                                   format(request_url), HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertEqual(json_response_data, {'EarnedCount': 0})


class SupervisorScheduleTest(APITestCase):
    """
    Test Cases for the Supervisor Schedule Tests
    """

    @mock.patch("cheerios.manifest.site.views.SupervisorViewSet.get_site_schedule")
    @mock.patch("cheerios.manifest.site.views.SupervisorViewSet.get_table_data")
    def test_supervisor_schedule_successful(self, mock_table_data, mock_site_schedule):
        """
        Tests the case when supervisor schedule get is successful
        :return:
        """
        request_url = "/manifest/site/schedule/"
        jwt = get_jwt_valid_data(request_url)
        supervisor_schedule_mock_dict = MOCK_RESPONSE['supervisor_schedule']
        mock_site_schedule.side_effect = [supervisor_schedule_mock_dict['get_site_schedule']]
        mock_table_data.side_effect = [supervisor_schedule_mock_dict['json_block'],
                                       supervisor_schedule_mock_dict['json_segment_parameter'],
                                       supervisor_schedule_mock_dict['json_segment_package'],
                                       supervisor_schedule_mock_dict['json_roll'],
                                       supervisor_schedule_mock_dict['json_segment'],
                                       supervisor_schedule_mock_dict['json_channel_type'],
                                       supervisor_schedule_mock_dict['json_roll_block'],
                                       supervisor_schedule_mock_dict['json_channel'],
                                       supervisor_schedule_mock_dict['json_block_segment'],
                                       supervisor_schedule_mock_dict['json_segment_type']]
        response = self.client.get("{}?serial_number=100&schedule_id=1001&site_id=1".format(request_url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response.status_code, 200)

    @mock.patch("cheerios.manifest.site.views.SupervisorViewSet.get_table_data")
    def test_schedule_ancillary_successful(self, mock_table_data):
        """
        Tests the case when schedule ancillary data get is successful
        :param mock_table_data:
        :return:
        """
        request_url = "/manifest/site/schedule_ancillary_data/"
        jwt = get_jwt_valid_data(request_url)
        mock_table_data.side_effect = [MOCK_RESPONSE['supervisor_schedule']['triviaweb_schedule']]
        response = self.client.get("{}?db_name=triviagames&schedule_id=1001".format(request_url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response.status_code, 200)

    @mock.patch("cheerios.manifest.site.views.SupervisorViewSet.get_site_schedule")
    def test_schedule_not_modified(self, mock_site_schedule):
        """
        Tests the case when schedule is not modified
        :param mock_site_schedule:
        :return:
        """
        request_url = "/manifest/site/schedule/"
        jwt = get_jwt_valid_data(request_url)
        supervisor_schedule_mock_dict = MOCK_RESPONSE['supervisor_schedule']
        mock_site_schedule.side_effect = [supervisor_schedule_mock_dict['get_site_schedule']]

        response = self.client.get("{}?serial_number=100&schedule_id=1&"
                                   "schedule_date=2018-09-11T10:50:37&site_id=1".format(request_url), HTTP_AUTHORIZATION=jwt)

        self.assertEqual(response.status_code, 304)

    @mock.patch("cheerios.manifest.site.views.SupervisorViewSet.get_site_schedule")
    def test_schedule_not_found(self, mock_site_schedule):
        """
        Tests the case when schedule is not found
        :param mock_site_schedule:
        :return:
        """
        request_url = "/manifest/site/schedule/"
        jwt = get_jwt_valid_data(request_url)
        mock_site_schedule.side_effect = [[]]

        response = self.client.get("{}?serial_number=100&schedule_id=1&schedule_date=2018-09-11T10:50:37&site_id=1".format(request_url),
                                   HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))

        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response_data, "Schedule not found with the passed ID")

    def test_input_parameters_missing(self):
        """
        Tests the case when the input parameters to the API is missing
        :return:
        """
        request_url = "/manifest/site/schedule/"
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get(request_url, HTTP_AUTHORIZATION=jwt)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data, "ValueError Invalid Parameter passed")
        self.assertEqual(response.status_code, 400)

    def test_schedule_ancillary_parameter_missing(self):
        """
        Tests the case when schedule ancillary get is given with no db name
        :param mock_table_data:
        :return:
        """
        request_url = "/manifest/site/schedule_ancillary_data/"
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{}?schedule_id=1001".format(request_url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response.status_code, 400)

    def test_schedule_ancillary_wrong_db(self):
        """
        Tests the case when schedule ancillary get is given with wrong db name
        :param mock_table_data:
        :return:
        """
        request_url = "/manifest/site/schedule_ancillary_data/"
        jwt = get_jwt_valid_data(request_url)
        response = self.client.get("{}?db_name=improper_db&schedule_id=1001".format(request_url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response.status_code, 400)


class QuestionTest(APITestCase):
    """
    Tests for question viewset
    """

    def setUp(self):
        """
        Initializing
        method
        :return:
        """
        self.api_endpoint = "/manifest/site/player_game_question/"

    def test_player_id_string(self):
        """

        :return:
        """
        request_body = dict(player_id="test",
                            site_id=1,
                            game_name_id=165,
                            game_id=1,
                            question_id=1,
                            answer_index=1,
                            game_start_time="2012-03-26T12:00:00",
                            handle="test",
                            pm_num=1,
                            score=1,
                            interactive_ad_skin_id=1,
                            answer_date="2012-03-26T12:00:00",
                            pin="test_pin")

        response = self.client.post(path=self.api_endpoint,
                                    data=request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:question"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")

    def test_date_incorrect_format(self):
        """

        :return:
        """
        request_body = dict(player_id=1,
                            site_id=1,
                            game_name_id=165,
                            game_id=1,
                            question_id=1,
                            game_start_time="2012-03-26T12:00:00",
                            answer_index=1,
                            handle="test",
                            pm_num=1,
                            score=1,
                            interactive_ad_skin_id=1,
                            answer_date="test",
                            pin="test_pin")

        response = self.client.post(path=self.api_endpoint,
                                    data=request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:question"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([])))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_answer_date_null(self):
        """

        :return:
        """
        request_body = dict(player_id=1,
                            site_id=1,
                            game_name_id=165,
                            game_id=1,
                            game_start_time="2012-03-26T12:00:00",
                            question_id=1,
                            answer_index=1,
                            handle="test",
                            pm_num=1,
                            score=1,
                            interactive_ad_skin_id=1,
                            pin="test_pin")

        response = self.client.post(path=self.api_endpoint,
                                    data=request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:question"),
                                    format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, "Question data successfully saved")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([])))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_game_start_time_null(self):
        """

        :return:
        """
        request_body = dict(player_id=1,
                            site_id=1,
                            game_name_id=165,
                            game_id=1,
                            question_id=1,
                            answer_index=1,
                            handle="test",
                            pm_num=1,
                            score=1,
                            interactive_ad_skin_id=1,
                            answer_date="2012-03-26T12:00:00",
                            pin="test_pin")

        response = self.client.post(path=self.api_endpoint,
                                    data=request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:question"),
                                    format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, "Question data successfully saved")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([])))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_non_player_plus_post(self):
        """

        :return:
        """
        request_body = dict(player_id=0,
                            site_id=1,
                            game_name_id=165,
                            game_id=1,
                            question_id=1,
                            answer_index=1,
                            game_start_time="2012-03-26T12:00:00",
                            handle="test",
                            pm_num=1,
                            score=1,
                            interactive_ad_skin_id=1,
                            answer_date="2012-03-26T12:00:00",
                            pin="")

        response = self.client.post(path=self.api_endpoint,
                                    data=request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:question"),
                                    format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, "Question data successfully saved")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([])))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_successful_post(self):
        """

        :return:
        """
        request_body = dict(player_id=1230,
                            site_id=1,
                            game_name_id=165,
                            game_id=1,
                            question_id=1,
                            answer_index=1,
                            handle="test",
                            game_start_time="2012-03-26T12:00:00",
                            pm_num=1,
                            score=1,
                            interactive_ad_skin_id=1,
                            answer_date="2012-03-26T12:00:00",
                            pin="test_pin")

        response = self.client.post(path=self.api_endpoint,
                                    data=request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:question"),
                                    format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, "Question data successfully saved")


class SiteGameQuestionTest(APITestCase):
    """
    Tests for build data viewset
    """

    def setUp(self):
        """
        Initializing
        method
        :return:
        """
        self.api_endpoint = "/manifest/site/site_game_question/"

    def test_siteid_string(self):
        """

        :return:
        """
        request_body = dict(site_id="test",
                            game_name_id=101,
                            game_start="2012-03-26T12:00:00",
                            network_id=104,
                            question_list=[{"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1},
                                           {"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1}
                                           ]
                            )

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:site-game-question"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")


    def test_game_start_invalid_format(self):
        """

        :return:
        """
        request_body = dict(site_id=100,
                            game_name_id=101,
                            game_start="test",
                            network_id=104,
                            question_list=[{"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1},
                                           {"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1}
                                           ]
                            )

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:site-game-question"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")

    def test_site_id_missing(self):
        """

        :return:
        """
        request_body = dict(game_name_id=101,
                            game_start="2012-03-26T12:00:00",
                            network_id=104,
                            question_list=[{"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1},
                                           {"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1}
                                           ]
                            )

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:site-game-question"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")

    def test_total_points_missing(self):
        """

        :return:
        """
        request_body = dict(site_id="test",
                            game_name_id=101,
                            game_start="2012-03-26T12:00:00",
                            network_id=104,
                            question_list=[{"question_id": 600,
                                            "total_responses": 1},
                                           {"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1}
                                           ]
                            )

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:site-game-question"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")


    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock(side_effect=pymssql.IntegrityError))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_unique_key_error(self):
        """

        :return:
        """
        request_body = dict(site_id="100",
                            game_name_id=101,
                            game_start="2012-03-26T12:00:00",
                            network_id=104,
                            question_list=[{"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1},
                                           {"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1}
                                           ]
                            )

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:site-game-question"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Error: Duplicate Values not allowed.")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([], [])))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_successful_save(self):
        """

        :return:
        """
        request_body = dict(site_id="100",
                            game_name_id=101,
                            game_start="2012-03-26T12:00:00",
                            network_id=104,
                            question_list=[{"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1},
                                           {"question_id": 600,
                                            "total_points": 1,
                                            "total_responses": 1}
                                           ]
                            )

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:site-game-question"),
                                    format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, "Question Data Successfully Saved.")


class ScoreViewSetTest(APITestCase):
    """
    Tests for score data viewset
    """

    def setUp(self):
        """
        Initializing
        method
        :return:
        """
        self.api_endpoint = "/manifest/site/score/"

    def test_siteid_string(self):
        """
        Tests case when site id is a string
        :return:
        """
        request_body = {"site_id": "test",
                        "site_name": "The NOT Buzztime Lounge",
                        "ranking_type": "NetworkRankings",
                        "network_airing": {
                           "game_name_id": 123,
                           "game_start_time": "2018-10-10T17:30:00",
                           "game_id": 999
                        },
                        "player_scores": [{
                          "player": {
                            "player_id": 12345678,
                            "handle": "PHRANC",
                            "pm_num": 199
                          },
                          "score": 15000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 9
                        },
                         {
                          "player": {
                            "player_id": 12345679,
                            "handle": "PHRANC12",
                            "pm_num": 200
                          },
                          "score": 16000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 10,
                          "session_guid": "B1B6AE10-9A90-4C13-AF18-144AFB8DA508"
                         }
                        ]
                        }

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:score"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")


    def test_game_start_invalid_format(self):
        """
        Tests case when game start time is in invalid format
        :return:
        """
        request_body = {"site_id": 1234,
                        "site_name": "The NOT Buzztime Lounge",
                        "ranking_type": "NetworkRankings",
                        "network_airing": {
                           "game_name_id": 123,
                           "game_start_time": "test",
                           "game_id": 999
                        },
                        "player_scores": [{
                          "player": {
                            "player_id": 12345678,
                            "handle": "PHRANC",
                            "pm_num": 199
                          },
                          "score": 15000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 9
                        },
                         {
                          "player": {
                            "player_id": 12345679,
                            "handle": "PHRANC12",
                            "pm_num": 200
                          },
                          "score": 16000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 10,
                          "session_guid": "B1B6AE10-9A90-4C13-AF18-144AFB8DA508"
                         }
                        ]
                        }

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:score"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")

    def test_site_id_missing(self):
        """
        Tests case when site id is missing
        :return:
        """
        request_body = {"site_name": "The NOT Buzztime Lounge",
                        "ranking_type": "NetworkRankings",
                        "network_airing": {
                           "game_name_id": 123,
                           "game_start_time": "2018-10-10T17:30:00",
                           "game_id": 999
                        },
                        "player_scores": [{
                          "player": {
                            "player_id": 12345678,
                            "handle": "PHRANC",
                            "pm_num": 199
                          },
                          "score": 15000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 9
                        },
                         {
                          "player": {
                            "player_id": 12345679,
                            "handle": "PHRANC12",
                            "pm_num": 200
                          },
                          "score": 16000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 10,
                          "session_guid": "B1B6AE10-9A90-4C13-AF18-144AFB8DA508"
                         }
                        ]
                        }

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:score"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")

    def test_player_id_missing(self):
        """
        Tests case when player id is missing
        :return:
        """
        request_body = {"site_id": 1234,
                        "site_name": "The NOT Buzztime Lounge",
                        "ranking_type": "NetworkRankings",
                        "network_airing": {
                           "game_name_id": 123,
                           "game_start_time": "2018-10-10T17:30:00",
                           "game_id": 999
                        },
                        "player_scores": [{
                          "player": {
                            "handle": "PHRANC",
                            "pm_num": 199
                          },
                          "score": 15000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 9
                        },
                         {
                          "player": {
                            "player_id": 12345679,
                            "handle": "PHRANC12",
                            "pm_num": 200
                          },
                          "score": 16000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 10,
                          "session_guid": "B1B6AE10-9A90-4C13-AF18-144AFB8DA508"
                         }
                        ]
                        }

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:score"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "ValueError Invalid Parameter passed")


    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock(side_effect=pymssql.IntegrityError))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_unique_key_error(self):
        """
        Tests the unique constrain error
        :return:
        """
        request_body = {"site_id": 1234,
                        "site_name": "The NOT Buzztime Lounge",
                        "ranking_type": "NetworkRankings",
                        "network_airing": {
                           "game_name_id": 123,
                           "game_start_time": "2018-10-10T17:30:00",
                           "game_id": 999
                        },
                        "player_scores": [{
                          "player": {
                            "player_id": 12345678,
                            "handle": "PHRANC",
                            "pm_num": 199
                          },
                          "score": 15000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 9
                        },
                         {
                          "player": {
                            "player_id": 12345679,
                            "handle": "PHRANC12",
                            "pm_num": 200
                          },
                          "score": 16000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 10,
                          "session_guid": "B1B6AE10-9A90-4C13-AF18-144AFB8DA508"
                         }
                        ]
                        }

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:score"),
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Error: Duplicate Values not allowed.")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([], [])))
    @patch(mock_test_utils.GET_CURSOR, mock.Mock())
    def test_successful_save(self):
        """
        Tests successful save
        :return:
        """
        request_body = {"site_id": 1234,
                        "site_name": "The NOT Buzztime Lounge",
                        "ranking_type": "NetworkRankings",
                        "network_airing": {
                           "game_name_id": 123,
                           "game_start_time": "2018-10-10T17:30:00",
                           "game_id": 999
                        },
                        "player_scores": [{
                          "player": {
                            "player_id": 12345678,
                            "handle": "PHRANC",
                            "pm_num": 199
                          },
                          "score": 15000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 9
                        },
                         {
                          "player": {
                            "player_id": 12345679,
                            "handle": "PHRANC12",
                            "pm_num": 200
                          },
                          "score": 16000,
                          "response_count": 15,
                          "channel": 1,
                          "duration": 1800,
                          "repeat_game": 0,
                          "playmaker_type_id": 10,
                          "session_guid": "B1B6AE10-9A90-4C13-AF18-144AFB8DA508"
                         }
                        ]
                        }

        response = self.client.post(self.api_endpoint, request_body,
                                    HTTP_AUTHORIZATION=get_jwt_token("manifest:qbert:score"),
                                    format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, "GamePlay data stored successfully.")
