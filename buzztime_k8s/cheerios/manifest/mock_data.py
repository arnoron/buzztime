def handshake_data():
    return {
        "Certificates": [
            "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
            "37ed3bb8365735d72db3a1e75c387e382867d0c7249308ced04a3ca798ec53ee"
            ],
        "Confidentials": [
        {
          "alias": "platform-secret",
          "sha256": "b37e50cedcd3e3f1ff64f4afc0422084ae694253cf399326868e07a35f4a45fb"
        }
      ]
    }


def certificates_data():
    return {
        "name": "BWWAloha",
        "alias": "BWWAloha-site1097",
        "digest": "b37e50cedcd3e3f1ff64f4afc0422084 ae694253cf399326868e07a35f4a45fb",
        "alg": "sha256",
        "form": "der",
        "Certificate": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tC"
    }


def confidentials_data():
    return {
        "alias": "platform-secret",
        "digest": "b37e50cedcd3e3f1ff64f4afc0422084ae694253cf399326868e07a35f4a45fb",
        "alg": "sha256",
        "secret": "{1234567-1234-1234-1231234094592857}"
    }
