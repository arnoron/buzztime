"""ucars URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include

from cheerios.manifest import views

urlpatterns = [
    url(r'^handshake/$', views.HandshakeView.as_view()),
    url(r'^certificates/$', views.CertificatesView.as_view()),
    url(r'^confidentials/$', views.ConfidentialsView.as_view()),
    url(r'^site/', include('cheerios.manifest.site.urls', namespace='qbert'))
]
