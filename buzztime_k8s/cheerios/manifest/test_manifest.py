"""
Testcase files for testing manifest API's
"""
from mock import patch, Mock
from rest_framework.test import APITestCase
from rest_framework import status
from cheerios.manifest.test_data import get_jwt_with_future_date, response_status,\
    get_invalid_jwt, get_jwt_with_trailing_space, get_jwt_valid_data, \
    valid_serial_number, invalid_serial_number, valid_certificate_sha256,\
    invalid_certificate_sha256, get_jwt_with_expired_date
from cheerios.manifest.constant import URL
from cheerios.test_utils import mock_test_utils
from .response_data import MOCK_RESPONSE

class TestManifestJWTModal(APITestCase):
    """
    Testcases to test JWT decoder.
    """
    def setUp(self):
        self.url = {
            'handshake': '/manifest/handshake/',
            'confidentials': '/manifest/confidentials/',
            'certificates': '/manifest/certificates/'
        }

    def test_jwt_future_date(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        handshake = URL.get('handshake')
        jwt = get_jwt_with_future_date(handshake)
        serial_number = valid_serial_number()
        response = self.client.get('{0}?serialnumber={1}'.format(handshake, serial_number),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    def test_jwt_expired_date(self):
        """
        Forwarding JWT with expired date should not be acceptable
        """
        handshake = URL.get('handshake')
        jwt = get_jwt_with_expired_date(handshake)
        serial_number = valid_serial_number()
        response = self.client.get('{0}?serialnumber={1}'.format(handshake, serial_number),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    def test_with_invalid_jwt(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        handshake = URL.get('handshake')
        jwt = get_invalid_jwt()
        serial_number = valid_serial_number()
        response = self.client.get('{0}?serialnumber={1}'.format(handshake, serial_number),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock(return_value=None))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['site_data'],
                                                                 MOCK_RESPONSE['certificates'],
                                                                 MOCK_RESPONSE['confidentials'])))
    def test_jwt_with_trailing_space(self):
        """
        Forwarding JWT with path with trailing spaces.
        """
        handshake = URL.get('handshake')
        jwt = get_jwt_with_trailing_space(handshake)
        serial_number = valid_serial_number()
        response = self.client.get('{0}?serialnumber={1}'.format(handshake, serial_number),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock(return_value=None))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['site_data'],
                                                                 MOCK_RESPONSE['certificates'],
                                                                 MOCK_RESPONSE['confidentials'])))
    def test_get_operation(self):
        """
        When Proper input is passed
        """
        handshake = URL.get('handshake')
        jwt = get_jwt_valid_data(handshake)
        serial_number = valid_serial_number()
        response = self.client.get('{0}?serialnumber={1}'.format(handshake, serial_number),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_200_OK)


class TestHandshakeAPI(APITestCase):
    """
    Testcases of manifest API's
    """
    def test_without_parameter(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        handshake = URL.get('handshake')
        response = self.client.get('{0}'.format(handshake),
                                   HTTP_AUTHORIZATION=get_jwt_valid_data(handshake))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    def test_invalid_serial(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        handshake = URL.get('handshake')
        jwt = get_jwt_valid_data(handshake)
        serial_number = invalid_serial_number()
        response = self.client.get('{0}?serialnumber={1}'.format(handshake, serial_number),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock(return_value=None))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['site_data_valid'],
               MOCK_RESPONSE['certificates_valid'],
               MOCK_RESPONSE['confidentials_valid'])))
    def test_valid_serial(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        handshake = URL.get('handshake')
        jwt = get_jwt_valid_data(handshake)
        serial_number = valid_serial_number()
        response = self.client.get('{0}?serialnumber={1}'.format(handshake, serial_number),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_200_OK)


class TestConfidentialsAPI(APITestCase):
    """
    Testcases of manifest API's
    """
    def test_without_parameter(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        url = URL.get('confidentials')
        jwt = get_jwt_valid_data(url)
        response = self.client.get('{0}'.format(url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    def test_without_sha256(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        url = URL.get('confidentials')
        jwt = get_jwt_valid_data(url)
        response = self.client.get('{0}?name=Test.Group'.format(url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    def test_without_name(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        url = URL.get('confidentials')
        jwt = get_jwt_valid_data(url)
        response = self.client.get('{0}?sha256=12345'.format(url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    def test_invalid_sha256(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        url = URL.get('confidentials')
        jwt = get_jwt_valid_data(url)
        response = self.client.get('{0}?name=abc&sha256=1'.format(url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['valid_serial'])))
    def test_valid_serial(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        handshake = URL.get('confidentials')
        jwt = get_jwt_valid_data(handshake)
        response = self.client.get(
            '{0}?name=SiteHub.platform.secret&sha256=e55c313b5b988049a59aac89ed98dc53'
                .format(handshake),HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_200_OK)


class TestCertificatesAPI(APITestCase):
    """
    Testcases of manifest API's
    """
    def test_without_parameter(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        url = URL.get('certificates')
        jwt = get_jwt_valid_data(url)
        response = self.client.get('{0}'.format(url),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    def test_invalid_sha256(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        url = URL.get('certificates')
        jwt = get_jwt_valid_data(url)
        parameter = invalid_certificate_sha256()
        response = self.client.get('{0}?sha256={1}'.format(url, parameter),
                                   HTTP_AUTHORIZATION=jwt)

        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock(return_value=None))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['certificates_test'])))
    def test_valid_serial(self):
        """
        Forwarding JWT with future date should not be acceptable
        """
        url = URL.get('certificates')
        jwt = get_jwt_valid_data(url)
        parameter = valid_certificate_sha256()
        response = self.client.get('{0}?sha256={1}'.format(url, parameter),
                                   HTTP_AUTHORIZATION=jwt)
        self.assertEqual(response_status(response), status.HTTP_200_OK)
