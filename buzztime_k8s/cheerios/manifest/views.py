"""
Manifest API.
"""
import base64

from cheerios.manifest.db_operations import ManifestDBOperation
from cheerios.manifest.serializers import HandShakeSerializer,\
    CertificatesSerializer, ConfidentialsSerializer
from cheerios.services.jwt_utils import jwt_decorator
from cheerios.utils import CheeriosUtils
from cheerios.manifest.constant import URL
from cheerios.interface.api_view_interface import ApiViewInterface

CHEERIOS_UTILS = CheeriosUtils()

MANIFEST_OBJ = ManifestDBOperation()


class HandshakeView(ApiViewInterface):
    """
    Handshake API:
    Will be called from Tablet passing serial number as Query Parameter
    Output will be list of certificates from DB
    """

    @jwt_decorator(URL['handshake'], 'GET')
    def get(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().get(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return HandShakeSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """
        return MANIFEST_OBJ.get_handshake_data(data.get('serialnumber')), True

    def format_response(self, data):
        response = []
        certificate = data.get("Certificates")
        if certificate:
            for item in certificate:
                response.append(item.get("CertificateHash"))
            data["Certificates"] = response
        return data

class CertificatesView(ApiViewInterface):
    """
    Certificates API:
    It will return the actual certificate from DB
    for sha256 value passed
    """

    @jwt_decorator(URL['certificates'], 'GET')
    def get(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().get(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return CertificatesSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """
        response = MANIFEST_OBJ.get_certificates_data(data.get('sha256'))
        return response, True

    def format_response(self, data):
        """
        Formating response.
        Covert bytes data to base64.
        """
        if not data or not data[0]:
            raise ValueError("Invalid parameter passed.")
        certificate = data[0]
        decode_value = base64.b64encode(certificate["CertificateData"])
        certificate["CertificateData"] = decode_value.decode("utf-8")
        return certificate

class ConfidentialsView(ApiViewInterface):
    """
    Confidentials API:
    It will return the actual certificate from DB
    for sha256 value passed
    """
    @jwt_decorator(URL['confidentials'], 'GET')
    def get(self, request):
        """
        Get API to fetch handshake data.
        """
        return super().get(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return ConfidentialsSerializer

    def get_from_downstream(self, data):
        """
        Call stored procedure and fetch data.
        """
        return MANIFEST_OBJ.get_confidentials_data(data.get('name'), data.get('sha256')), True

    def format_response(self, data):
        """
        Formating response.
        Covert bytes data to base64.
        """
        if not data or not data[0]:
            raise ValueError("Invalid parameter passed.")
        return data[0]
