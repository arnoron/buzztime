"""
Constant value for manifest API.
"""
ALLOWED_DB_MAPPER = {
    'triviagames': [dict(name='GameParameters', keys=False)]
}

URL = {
    'handshake': '/manifest/handshake/',
    'confidentials': '/manifest/confidentials/',
    'certificates': '/manifest/certificates/',
    'software': '/manifest/site/software/',
    'content': '/manifest/site/content/',
    'site_handshake': '/manifest/site/handshake/',
    'sitehub_update': '/manifest/site/sitehub/',
    'vistar_assets': '/manifest/site/vistar-assets/',
    'build_data': '/manifest/site/build-data/',
    'pop_validation': '/manifest/site/execute_vistar_pop/',
    'sitehub_player_achievement': '/manifest/site/player_achievement/',
    'supervisor_schedule': '/manifest/site/schedule/',
    'schedule_ancillary_data': '/manifest/site/schedule_ancillary_data/',
    'playerplus_points': '/manifest/site/playerplus_points/',
    'question': '/manifest/site/player_game_question/',
    'site_game_question': '/manifest/site/site_game_question/',
    'score': '/manifest/site/score/'
}

STORE_PROC = {
    "sel_device": "dbo.usp_sel_DeviceBySerialNumber",
    "sel_secure_conf_list": "dbo.usp_sel_SecureConfigurationList",
    "sel_site_certificate_list": "dbo.usp_sel_SiteCertificateList",
    "sel_site_certificate": "dbo.usp_sel_SiteCertificate",
    "sel_site_configuration": "dbo.usp_sel_SecureConfiguration",
    'pop_validation': '/manifest/site/execute_vistar_pop/'
}
