"""
Class which provide function to get/update value of manifest value in DB.
"""

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.decorators import close_connection
from cheerios.settings import MSSQLDBPROG
from cheerios.manifest.constant import STORE_PROC


class ManifestDBOperation(MSSQLApiView):
    """
    generic class for all the Stored procedure
    calls and validations for Manifest API.
    """

    @close_connection
    def get_handshake_data(self, serial_number):
        """
        This function will create a connection
        get the Data from DB for passed serialnumber from tablet
        return data
        :param serial_number:
        :return:
        """
        result = {}
        cursor = self.get_cursor(MSSQLDBPROG)
        store_procedure = STORE_PROC['sel_device']
        data = "@SerialNumber='{}'".format(serial_number)
        self.call_storeprocedure(store_procedure, cursor, data)
        site_data = self.fetchall_from_cursor(cursor)
        if not site_data or not site_data[0]:
            raise ValueError("Invalid parameter passed.")
        site_id = site_data[0].get('SiteID')
        store_procedure = STORE_PROC['sel_site_certificate_list']
        data = "@SiteID={}".format(site_id)
        self.call_storeprocedure(store_procedure, cursor, data)
        result['Certificates'] = self.fetchall_from_cursor(cursor)
        store_procedure = STORE_PROC['sel_secure_conf_list']
        self.call_storeprocedure(store_procedure, cursor, data)
        result['Confidentials'] = self.fetchall_from_cursor(cursor)
        return result

    @close_connection
    def get_certificates_data(self, sha256):
        """
        This function will create connection
        get data from DB for passed sha256 value from tablet
        return certificates.
        :param sha256:
        :return:
        """
        cursor = self.get_cursor(MSSQLDBPROG)
        store_procedure = STORE_PROC['sel_site_certificate']
        data = "@CertificateHash='{}'".format(sha256)
        self.call_storeprocedure(store_procedure, cursor, data)
        certificate_data = self.fetchall_from_cursor(cursor)
        return certificate_data

    @close_connection
    def get_confidentials_data(self, name, sha256):
        """
        This function will create connection
        get data from DB for passed sha256 value from tablet
        return confidential certificates.
        :param sha256:
        :return:
        """
        cursor = self.get_cursor(MSSQLDBPROG)
        store_procedure = STORE_PROC['sel_site_configuration']
        data = "@Name='{0}', @ConfigHash='{1}'".format(name, sha256)
        self.call_storeprocedure(store_procedure, cursor, data)
        configuration_data = self.fetchall_from_cursor(cursor)
        return configuration_data
