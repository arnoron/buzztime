"""
Custom exceptions being used
"""
class NotFoundError(ValueError):
    """
    Custom exception for Not found values
    """
    def __init__(self, arg):
        self.strerror = arg
        self.args = {arg}
