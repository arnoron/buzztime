"""
Serializer to validates parameters.
"""
from rest_framework import serializers
from cheerios.serializers import SerializerValidator

class HandShakeSerializer(SerializerValidator):
    """
    Serializer for Handshake Model
    Check all mandatory fields of database are available.
    """
    serialnumber = serializers.CharField(allow_null=False, required=True)


class CertificatesSerializer(SerializerValidator):
    """
    Serializer for Certificates Model
    Check all mandatory fields of database are available.
    """
    sha256 = serializers.CharField(allow_null=False, required=True)


class ConfidentialsSerializer(SerializerValidator):
    """
    Serializer for Certificates Model
    Check all mandatory fields of database are available.
    """
    name = serializers.CharField(allow_null=False, required=True)
    sha256 = serializers.CharField(allow_null=False, required=True)
