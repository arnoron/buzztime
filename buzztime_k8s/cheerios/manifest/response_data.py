"""
Responses for Stored Procedure calls
"""
import datetime
MOCK_RESPONSE= dict(
    site_data=[
        {
            'StatusID': 0,
            'UpdateDate': datetime.datetime(2013, 11, 5, 9, 50, 21, 270000),
            'SerialNumber': '0112TT999931',
            'SiteID': 40000,
            'DeviceID': 6
        }
    ],
    certificates=[],
    confidentials=[
        {
            'HashMethod': 'md5',
            'ConfigHash': 'e55c313b5b988049a59aac89ed98dc53',
            'Name': 'SiteHub.platform.secret'
        },
        {
            'HashMethod': 'md5',
            'ConfigHash': '704571a993a2b36a69490672024feadd',
            'Name': 'SiteHub.shared.secret'
        },
        {
            'HashMethod': 'md5',
            'ConfigHash': 'bc5a014a36d69b68c7881f64098a6347',
            'Name': 'SiteHub.SiteManifest.token'
        }
    ],
    site_data_valid=[
        {
            'SiteID': 40000,
            'DeviceID': 6,
            'StatusID': 0,
            'SerialNumber': '0112TT999931',
            'UpdateDate': datetime.datetime(2013, 11, 5, 9, 50, 21, 270000)
        }
    ],
    certificates_valid=[],
    confidentials_valid=[
        {
            'HashMethod': 'md5',
            'Name': 'SiteHub.platform.secret',
            'ConfigHash': 'e55c313b5b988049a59aac89ed98dc53'
        },
        {
            'HashMethod': 'md5',
            'Name': 'SiteHub.shared.secret',
            'ConfigHash': '704571a993a2b36a69490672024feadd'
        },
        {
            'HashMethod': 'md5',
            'Name': 'SiteHub.SiteManifest.token',
            'ConfigHash': 'bc5a014a36d69b68c7881f64098a6347'
        }
    ],
    certificates_test=[
        {
            'CertificateFormat': 'DER',
            'HashMethod': 'SHA256',
            'CertificateData': b'0\x82\x04,0\x82\x03\x14\xa0\x03\x02\x01\x02\x02\n '
                               b'\x17\x10\x19\x19\x16\x03" \x890\r\x06\t*\x86H\x86\xf7\r'
                               b'\x01\x01\x0b\x05\x00071\x130\x11\x06\x03U\x04\x03\x0c'
                               b'\nBWWTERM5001 0\x1e\x06\x03U\x04\n\x0c\x17'
                               b'Buffalo Wild Wings Inc.0\x1e\x17\r171019191501Z\x17'
                               b'\r221019191501Z071\x130\x11\x06\x03U\x04\x03\x0c'
                               b'\nBWWTERM5001 0\x1e\x06\x03U\x04\n\x0c\x17'
                               b'Buffalo Wild Wings Inc.0\x82\x01"0\r\x06\t*\x86H\x86'
                               b'\xf7\r\x01\x01\x01\x05\x00\x03\x82\x01\x0f\x000\x82\x01'
                               b'\n\x02\x82\x01\x01\x00\xbf,\xe5\xd9\xcd(\xcd\xeepP\x9ff'
                               b'\xb9O\x9c\x13i\xfb\x9c\xd3$\xd1\xdah\x15\xe9 6\xa5\xf9'
                               b'\x03\'0\xdd\xaa8\x17\xea\x92oLx\xa3\xa7?J\x0e\xcc\xff&'
                               b'\x9b\xd8\xe9\x1d\rf\xe0\xda\x06@\xbehn\x15\x0b\xa18\x18'
                               b'\x08\xa0) \x1e\xbaxOLI\xef\x05aV\xac;)\n\xc1\xfe\xd1Y\x9e'
                               b'\x90s\x80[$\xdbe\x1e\x18g \x99\xce\xe6\xd3o\x05E\xb7Z\xf7'
                               b'\xb6B\x00\x93;N\xb1\xa3\x1f\x82\xab\x82\xfbo\xeef\x8c]I\xd3'
                               b'\xf7\x19\xb8{~\x02\xda"\xbc\xaf\xf2\x94\xe97\xbf56\x96/\x05'
                               b'\xc8\x94#<"\x04\x11^\x8b\x81\xbd^\xbfj\x08\xc1\\\xecFq?\x10%Z'
                               b'\x94\xc2gQ\x9b\xfc\xb6\x18\x14\x14\xcf\x85\x96\x8dv\xd8\xd0\xfcf'
                               b'\x8ehW&<\x1e\xaa\xab\xf0\xef\xcdU \x9f\xc81\xed\xfc\xfe\xf0\xd8'
                               b'\xa3\xc3\x90\x19\xe1yr\xdf\xf2\xefq\x8b\x99\r\xbc\xaa\xbc\xd1'
                               b'\xddd\xc0u$\xd4\x9cN\xd1F2k\xc3b\xf4\xf4\xb2\xe6Q\x1c\xa3c\x02'
                               b'\x03\x01\x00\x01\xa3\x82\x0180\x82\x0140\x0e\x06\x03U\x1d\x0f'
                               b'\x01\x01\xff\x04\x04\x03\x02\x03\xa80\x0c\x06\x03U\x1d\x13\x01'
                               b'\x01\xff\x04\x020\x000\x1d\x06\x03U\x1d\x0e\x04\x16\x04\x14Y\rB'
                               b'\x11~\xb0\xc1\x15\xb2\xa72&$k\xca\x92\xb0\xf8\xf0\xad0h\x06\x03U'
                               b'\x1d#\x04a0_\x80\x14Y\rB\x11~\xb0\xc1\x15\xb2\xa72&$k\xca\x92'
                               b'\xb0\xf8\xf0\xad\xa1;\xa49071\x130\x11\x06\x03U\x04\x03\x0c'
                               b'\nBWWTERM5001 0\x1e\x06\x03U\x04\n\x0c\x17'
                               b'Buffalo Wild Wings Inc.\x82\n \x17\x10\x19\x19\x16\x03" '
                               b'\x890 \x06\x03U\x1d%\x01\x01\xff\x04\x160\x14\x06\x08+\x06\x01'
                               b'\x05\x05\x07\x03\x01\x06\x08+\x06\x01\x05\x05\x07\x03\x020i\x06'
                               b'\x03U\x1d\x11\x04b0`\x82\nBWWTERM500\x82\x0bBWWTERM500.\x87\x10'
                               b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01'
                               b'\x82\x03::1\x87\x04\n@A\xaf\x82\x0c10.64.65.175\x87\x04\x7f'
                               b'\x00\x00\x01\x82\t127.0.0.1\x82\tlocalhost0\r\x06\t*\x86H\x86'
                               b'\xf7\r\x01\x01\x0b\x05\x00\x03\x82\x01\x01\x00#Uo\xd2\x98\xbaGfk'
                               b'\xa3\xe3\x1b\xa0<\xc3\xbe\xed\x8bF\xeb.\xc5\xf2\xd30H\xc3@i$\xee'
                               b'\x1d\xec\xfd\x05\x0c$\x9c\xa1\xcd\xc2\xdc\xd5\xbcK\x1b\x0c\xc9'
                               b'\xa2\x89}Q\xbc\xc4\xec\x95u/\xb1\x05\x1d@p\xf4\x8b\x8a\xf1\xa7'
                               b'\xce\x7f)\x18\x88\xe9\xab\xa4\x96jy.Xz\xe0\xe1\x08p%\xdd|'
                               b'\n^w;\'\x94.\xfa\xd8\x08\x84@\xbc\xf0a\x9e\xe8\xce;\x9b\x06\x02W'
                               b'\x8d\xbc\xa7\x93\x06d/\x90O\x0f\xca\x11\xb2?\xa3r\x14\x85\x8d"'
                               b'\x83,\xf4\xed5~\xbaK\x1c\x19\xf27/d\xe3\xa5\xad-T\xfa\xd6\x81'
                               b'\xfb\xee\xefi\'d\xf4<\xa1\x04\xb2[\x97#wh\xe3<x\xddZ\xacA\xb1&'
                               b'\x8d@\x8e\x91A\x8d#\xe1?\x1f\x04\xfa\x032\x92\x84\x02\x01\x0f'
                               b'\x82\xfb\xa2\xab\x83dSA8H\xa1\t\xf2\xbc\r\x9b\xf7\x91`\xaa!['
                               b'\xc9\x17\x88D\xfb\xac\xef\xd4\xc8\x0b/\xc5\xdc\x18\xa8\xb6'
                               b'\x186\xb3U9\x8e\xaf\x97\xfe\xfc.\x14\xf8\xce\\\x10\x05\x03<x',
            'CertificateCommonName': 'BWWTERM500',
            'CertificateHash': '35a7bcd104d7f1749234011780767ebde63baa0a70739d12e7e6761acb883de3',
            'CertificateName': 'BWW FCC'
        }
    ],
    valid_serial=[
        {
            'HashMethod': 'md5',
            'Value': '{340A98F5-F71E-4625-81BC-0A38371213A7}',
            'Name': 'SiteHub.platform.secret',
            'ConfigHash': 'e55c313b5b988049a59aac89ed98dc53'
        }
    ],

)
