"""
Mock data for testing manifest API's
"""
from datetime import timedelta
import datetime
import jwt

from cheerios.services.jwt_utils import get_jwt_secret_from_url
from cheerios.analytics.utils.utils import UtilFunction
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


def response_status(response):
    """
    We need to remove this once we create common function.
    Common function which send response status code from response.
    """
    return response.status_code


def valid_serial_number():
    """
    Return valid serial number.
    """
    return "0112TT999931"


def valid_certificate_sha256():
    """
    Return valid certificate sha256.
    """
    return "35a7bcd104d7f1749234011780767ebde63baa0a70739d12e7e6761acb883de3"


def invalid_certificate_sha256():
    """
    Return valid certificate sha256.
    """
    return "35a7bcd104d7f174923401178079d12e7e6761acb883de3"


def invalid_serial_number():
    """
    Return in valid serial number.
    """
    return -1


def get_jwt_with_future_date(url):
    """
    Return Json Web Token for post Event operation
    """
    date = datetime.datetime.utcnow() + timedelta(minutes=6)
    return get_jwt(url, date)

def get_jwt_with_expired_date(url):
    """
    Return Json Web Token for post Event operation
    """
    date = datetime.datetime.utcnow() - timedelta(minutes=6)
    return get_jwt(url, date)


def get_invalid_jwt():
    """
    Return invalid jwt token
    """
    return 'fdtyghjbmn,l234r'


def get_jwt(url, date_time):
    """
    Returns jwt token of date and url 
    """
    secret = get_jwt_secret_from_url(url)
    date = CHEERIOS_UTILS.convert_datetime_to_str(date_time)
    encoded = jwt.encode({"REQUEST_METHOD": "GET",\
                          "PATH_INFO": url + ' ',\
                          "time": date\
                          },\
                          secret, algorithm='HS256',\
                          headers={\
                              "alg": "HS256",\
                              "typ": "JWT"\
                         })
    return encoded.decode("utf-8")

def get_jwt_with_trailing_space(url):
    """
    Return Json Web Token for get API operation
    """
    secret = get_jwt_secret_from_url(url)
    date = CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.utcnow())
    encoded = jwt.encode({"REQUEST_METHOD": "GET",\
                          "PATH_INFO": url + ' ',\
                          "time": date\
                          },\
                          secret, algorithm='HS256',\
                          headers={\
                              "alg": "HS256",\
                              "typ": "JWT"\
                         })
    return encoded.decode("utf-8")


def get_jwt_valid_data(url):
    """
    Return Json Web Token for get API operation
    """
    return get_jwt(url, datetime.datetime.utcnow())
