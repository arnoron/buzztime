"""
Serializer for authentication and common functions.
"""
from rest_framework import serializers

class SerializerValidator(serializers.Serializer):
    """
    Serializer to validate values.
    """
    def create(self, validated_data):
        """
        Overridden function create.
        """
        pass

    def update(self, instance, validated_data):
        """
        Overridden function update.
        """
        pass

class AuthenticationSerializer(SerializerValidator):
    """
    Serializer for authentication class
    """
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=True)
    player_id = serializers.CharField(required=False)


class AuthorizationSerializer(SerializerValidator):
    """
    Serializer for authentication class
    """
    player_id = serializers.IntegerField(required=True)
