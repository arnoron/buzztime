"""
This file has been written to incorporate the session management in redis,
everytime a request comes in it should automatically go to this middleware
where it would check whether the session in present inside redis or not.
"""
from importlib import import_module

from django.core.urlresolvers import reverse
from django.http.response import HttpResponseForbidden, HttpResponse
from django.conf import settings
from rest_framework.response import Response

from cheerios.constant.constant import SUCESS_STATUS, NOT_MODIFY_STATUS, PLAYMAKER_AUTH_APIS

from cheerios.analytics.constants import URL
from cheerios.constant.constant import SUCESS_STATUS, NOT_MODIFY_STATUS
from cheerios.players.constants import PLAYERS_UTILS_CONSTANTS, PLAYERS_AUTH_REQUIRED
from cheerios.services.redis import RedisSession
from cheerios.triviaweb.constants import TRIVIA_WEB_PREFIX, \
    IGNORE_URL, SESSION_EXPIRY, LOGIN_URL, POST_AUTH_URL
from cheerios.triviaweb.constants import POST_API, \
    DEFAULT_THEME_BRAND, USER_ROLES, ALL_ENTERPRISES
from cheerios.constant.constant import WRONG_CREDENTIALS_TITLES
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()

class SessionManagement(object):
    """
    Middleware class to manage session authentication
    """
    @staticmethod
    def process_request(request):
        """
        filter on all APIs which are not part of trivia, all those requests which do not
        require authentication and all preflight requests
        """
        path = request.path

        Logger.assign_logfile(path)

        engine = import_module(settings.SESSION_ENGINE)
        stump_auth = path.find(TRIVIA_WEB_PREFIX) != -1 \
            and path not in IGNORE_URL

        post_auth = path in POST_AUTH_URL \
            and request.method == POST_API

        if stump_auth or post_auth:
            # Add logic to check for session in redis and if it doesn't then return 403 error

            session_key = request.META.get('HTTP_AUTHORIZATION',\
                                           request.COOKIES.get(settings.SESSION_COOKIE_NAME, None))

            if path == PLAYERS_UTILS_CONSTANTS['AVATAR_URL']:
                session_key = request.META.get('HTTP_AUTHORIZATION', \
                                               request.COOKIES.get('session_id'))

            redis_obj = RedisSession()
            Logger.log.info('Check for credentials in redis for %s', session_key)

            try:
                if not redis_obj.get_key(key=session_key) and request.method != 'OPTIONS':
                    return HttpResponseForbidden()
            except Exception as ex:
                message = "Internal Server Error. Please retry after some time."
                Logger.log.error(message)
                return HttpResponse(message, status=500)

            request.session = engine.SessionStore(session_key)

    @staticmethod
    def auth_for_post_call():
        """
        Function for adding auth for POST calls only.
        There are few apis which are open for GET where as
        need auth for POST calls
        :return:
        """
        pass

    def process_response(self, request, response):
        """
        After the user view is processed, but still in the middleware stack,
        the session engine calls its save method to save any changes to the data store.
        Now we need to call the save to our redis object. So we would need to initialize it
        and then call it.
        """


        path = request.path
        valid_path = path in [LOGIN_URL, \
        PLAYERS_UTILS_CONSTANTS['PLAYERS_LOGIN_URL'],\
        PLAYERS_UTILS_CONSTANTS['PLAYERS_HASHED_LOGIN_URL'], \
        PLAYERS_UTILS_CONSTANTS['PLAYERS_TABLET_LOGIN_URL']]

        valid_response = CHERRIOS_UTILS.attr_has_value(response, 'data')\
        and response.status_code in [SUCESS_STATUS, NOT_MODIFY_STATUS]

        valid_url = path.find(TRIVIA_WEB_PREFIX) != -1 \
        or path.find(PLAYERS_UTILS_CONSTANTS['PLAYERS_PREFIX']) != -1
        if valid_path and request.method != 'OPTIONS' and  valid_response and valid_url and \
        response.data not in WRONG_CREDENTIALS_TITLES:
            request.session.save()
            response.data['sessionId'] = request.session.session_key
            response.set_cookie(settings.SESSION_COOKIE_NAME,
                                request.session.session_key,
                                domain=settings.SESSION_COOKIE_DOMAIN,
                                expires=SESSION_EXPIRY,
                                path=settings.SESSION_COOKIE_PATH,
                                secure=settings.SESSION_COOKIE_SECURE or None,
                                httponly=settings.SESSION_COOKIE_HTTPONLY or None)
            redis_obj = RedisSession()
            _roles = response.data.get('roles', [])
            _brand_id = response.data.get('brandID', DEFAULT_THEME_BRAND)
            enterprise = _brand_id if USER_ROLES['SUPER_ADMIN'] not in _roles\
                                                    else ALL_ENTERPRISES
            try:

               redis_obj.save_data(key=response._headers['session'][1],value={'roles': _roles,\
                                           'enterprise': enterprise})
            except Exception:
                message = "Internal Server Error. Please retry after some time."
                Logger.log.error(message)
                return HttpResponse(message, status=500)

        return response
