"""Cheerios URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from cheerios import robots, build_info

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^robots.txt', robots.robots_file_content),
    url(r'^version', build_info.build_file_content),
    url(r'^drf_docs/', include('rest_framework_docs.urls')),
    url(r'^analytics/', include('cheerios.analytics.urls', namespace="analytics")),
    url(r'^feeds/', include('cheerios.feeds.urls', namespace="feeds")),
    url(r'^foodmenu/', include('cheerios.foodmenu.urls', namespace="foodmenu")),
    url(r'^triviaweb/', include('cheerios.triviaweb.urls', namespace="triviaweb")),
    url(r'^players/', include('cheerios.players.urls', namespace="players")),
    url(r'^playmaker/', include('cheerios.playmaker.urls', namespace="playmaker")),
    url(r'^manifest/', include('cheerios.manifest.urls', namespace="manifest")),
    url(r'^mysterymachine/', include('cheerios.mysterymachine.urls', namespace="mysterymachine"))
]
urlpatterns += staticfiles_urlpatterns()
