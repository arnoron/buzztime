"""
classes for MongoDB
"""

import datetime
from mongoengine.document import DynamicDocument
from mongoengine.fields import StringField, DateTimeField


class Event(DynamicDocument):
    """
    Event class for MongoDB
    """
    en = StringField()
    es = StringField()
    _c = DateTimeField()
    edt = DateTimeField()

    def save(self, *args, **kwargs):
        if not self._c:
            self._c = datetime.datetime.now()

        return super(Event, self).save(*args, **kwargs)
