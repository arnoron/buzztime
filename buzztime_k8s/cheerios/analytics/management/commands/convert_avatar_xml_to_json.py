import csv
import json
from random import randint

import untangle
from django.core.management.base import BaseCommand
from django.http import HttpResponse

from cheerios.players.constants import AVATAR_XML_CSV_PATH, DEFAULT_LOWER_LIMIT_FOR_COORDS, \
    DEFAULT_UPPER_LIMIT_FOR_COORDS, AVATAR_JSON_DEFAULT_VALUES
from cheerios.players.views.avatar import AvatarViewSet
import datetime


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        try:
            rows = []
            migrated_avatars = 0
            with open(AVATAR_XML_CSV_PATH, 'r') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                before_for_loop_time = datetime.datetime.now()
                total_sp_call_time = datetime.timedelta(0)
                for row in csv_reader:
                    inside_for_loop_time = datetime.datetime.now()
                    if row[3] != "migrated" and migrated_avatars < 10:
                        obj = untangle.parse(row[1])
                        json_item = {}
                        for features in obj.avatar.features:
                            for feature in features.feature:
                                json_item[feature["name"] + "_type"] = feature["name"] + "_" + feature["selected"]

                        for colors in obj.avatar.colors:
                            for color in colors.color:
                                try:
                                    if len(color["color"]) > 6:
                                        rgb_color = tuple(
                                            int(format(int(color["color"]), 'x')[i:i + 2], 16) for i in (0, 2, 4))
                                    else:
                                        rgb_color = tuple(int(color["color"][i:i + 2], 16) for i in (0, 2, 4))
                                    json_item[color["name"]] = {"r": rgb_color[0], "g": rgb_color[1],
                                                                "b": rgb_color[2]}
                                    json_item[color["name"] + "Coords"] = {"x": color["xpos"], "y": color["ypos"]}
                                except ValueError as e:
                                    # print('Invalid Hex data.',e)
                                    json_item[color["name"]] = {"r": 0, "g": 0, "b": 0}
                                    json_item[color["name"] + "Coords"] = {
                                        "x": randint(DEFAULT_LOWER_LIMIT_FOR_COORDS,
                                                     DEFAULT_UPPER_LIMIT_FOR_COORDS),
                                        "y": randint(DEFAULT_LOWER_LIMIT_FOR_COORDS,
                                                     DEFAULT_UPPER_LIMIT_FOR_COORDS)}

                        if obj.avatar['gender'] == 'm':
                            json_item['gender'] = 'male'
                        else:
                            json_item['gender'] = 'female'

                        json_item.update(AVATAR_JSON_DEFAULT_VALUES)

                        json_string = json.dumps(json_item)

                        avatar_interface = AvatarViewSet()
                        player_id = row[0]
                        file_crc_32 = row[2]
                        before_sp_call_time = datetime.datetime.now()
                        print("before_SP_call", before_sp_call_time-inside_for_loop_time)
                        try:
                            migration_status = avatar_interface.save_xml(player_id, player_id + ".jpg", file_crc_32,
                                                                        json_string)
                        except Exception as ex:
                            print(str(ex))
                        sp_call_time = datetime.datetime.now() - before_sp_call_time
                        total_sp_call_time += sp_call_time
                        print("SP call time", sp_call_time)
                        if migration_status:
                            migrated_avatars += 1
                            # print("avatar saved for {}".format(player_id))
                            row[3] = "migrated"
                            rows.append(row)
                        else:
                            # print("avatar not saved for {}".format(player_id))
                            row[3] = "not migrated"
                            rows.append(row)
                        # self.json_list.append(json.dumps(json_item))
                    else:
                        rows.append(row)
                print("for loop time", datetime.datetime.now() - before_for_loop_time)
                print("total SP call time", total_sp_call_time)

            with open(AVATAR_XML_CSV_PATH, 'w') as csv_file_out:
                csv_writer = csv.writer(csv_file_out)
                csv_writer.writerows(rows)
            if migrated_avatars == 0:
                print(0)
            else:
                print(1)
            # TODO : Should take csv data one by one and convert and call Avatar save_xml API to save and should
            # proceed to next data when saving is complete.
        except FileNotFoundError as e:
            print("File not found in {} location".format(AVATAR_XML_CSV_PATH))