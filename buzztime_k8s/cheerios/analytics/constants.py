"""
constant variable of site
"""
URL = {
    'post-event': '/analytics/event/',
    'ping': '/analytics/ping/',
    'health-check': '/analytics/healthcheck',
    'post_pci': '/analytics/pci/',
    'rabbitmq_creds': '/analytics/rmq_creds/'
}

DEFAULTEVENTCATEGORY = "EventService"
DEFAULTEVENTCOLLECTION = "Event"

JWT_VALID_TIME_INTERVAL = 60 * 5

CHEERIOS_CONFIG_SP = 'usp_sel_PlatformServicesConfiguration'

MONGO_NAME_PATTERN = 'Cheerios.Mongo%'

PLAYMAKER_RMQ_PORT = 443
