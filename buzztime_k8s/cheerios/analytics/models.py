"""
classes for print fields in DRF doc.
"""
from __future__ import unicode_literals
from django.db import models


class Event(models.Model):
    """
    Event class for print fields in DRF doc.
    """
    name = models.TextField(blank=True)
    source = models.TextField(blank=True)
    time = models.DateTimeField(blank=True)
    keyValuePairs = models.TextField(blank=True)
