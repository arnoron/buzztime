"""
API view For Document like post, get method for any document.
"""
import base64
import json

from Crypto.Cipher import AES
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from cheerios.analytics.constants import URL, PLAYMAKER_RMQ_PORT
from cheerios.analytics.db_operations import EventOperation
from cheerios.analytics.serializers import EventSerializer
from cheerios.analytics.serializers import EventSerializerforInternelModel
from cheerios.services.is_authenticated import is_authenticated
from cheerios.services.jwt_utils import jwt_decorator
from cheerios.analytics.utils.utils import UtilFunction
from cheerios.services.log_classifier import Logger
from cheerios.settings import RMQ_HOST, RMQ_PASSWORD, RMQ_USERNAME, RMQ_CREDS_SYM_KEY, SITE_PC_ID
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = UtilFunction()


class EventViewSet(APIView):
    """
    API For Event Document :
    1. Post API for Event service:-
        It gets data from Tablet events. That data is processed
        and saved mongodb's Event collection.
        i) This method accepts name, source, time and KeyPairValue (all are optional).
        ii) Passed data can be a single Event or a list of Events.
        iii) Data will be processed to create a dictionary
            and saved in mongodb's Event Collection.
    """
    permission_classes = (permissions.BasePermission,)
    serializer_class = EventSerializerforInternelModel

    @jwt_decorator(URL['post-event'], 'POST')
    def post(self, request):
        """
        Post call for Event
        Check that request is valid according to jwt.
        Validate passed data as per Serializer defined.
        If data is valid then save into mongo database.
        else give bad request as a error.
        If any run time error occur then it will handle by try and give appropriate message.
        """
        try:
            if request.data:
                new_data_list = UTIL_FUNCTION.map_data(request.data)
                for new_data in new_data_list:
                    serializer = EventSerializer(data=new_data)
                if serializer.is_valid():
                    events_obj = EventOperation()
                    events_obj.save_data(new_data_list)
                else:
                    result = "Serializer Error: {0}".format(serializer.errors)
                    message = "Error status code 400, {0}".format(result)
                    Logger.log.error(message)
                    data = request.data
                    message = "Event API data :- {0}".format(data)
                    Logger.log.info(message)
                    return Response(result, status=400)
            else:
                result = "Empty data passed Error"
                message = "Error status code 400, {0}".format(result)
                Logger.log.error(message)
                return Response(result, status=400)
        except ValueError as inst:
            data = request.data
            message = "Event API data :- {0}".format(data)
            Logger.log.error(message)
            result = "Data Format Error: {0}".format(inst.args[0])
            message = "Error status code 400,  {0}".format(result)
            Logger.log.error(message)
            return Response(result, status=400)
        except Exception as inst:
            error = ""
            if "Errno 111" in inst.args[0]:
                error = "Mongodb connection refused."
            else:
                data = request.data
                message = "Event API data :- {0}".format(data)
                Logger.log.info(message)
                error = inst.args[0]
            error = "Error: {0}".format(error)
            message = "Error status code 500, {0}".format(error)
            Logger.log.error(message)
            return Response(error, status=500)
        return Response(status=200)


class PingView(APIView):
    """
    Get api created to test response time
    This is a test api.
    To test the response time.
    It will just return status 200
    """

    @staticmethod
    def get(request):
        """
        Get API created for Response time check.
        :param request:
        :return: 200 status
        """
        return Response("", status=200)


class HealthCheckView(APIView):
    """
    Its a health check api.
    It will connect to mongo db
    If connection is established it will return 200
    else 500
    """

    @staticmethod
    def get(request):
        """
        Get API created for health check.
        This api will create connection with DB.
        If connection is established it will return 200
        else 500 status code
        :param request:
        :return: status code 200/500
        """
        # Test comment to check dead containers removal. To be reverted later.
        try:
            events_obj = EventOperation()
            status = events_obj.check_mongo_connection()
            return Response(status=status)
        except Exception as inst:
            error = ""
            if "Errno 111" in inst.args[0]:
                error = "Mongodb connection refused."
            else:
                error = inst.args[0]
            error = "Error: {0}".format(error)
            message = "Error status code 500, {0}".format(error)
            Logger.log.error(message)
            return Response(error, status=500)


class RabbitMQCredsViewSet(APIView):
    """
    Returns the rabbit mq connection parameters in an encrypted manner
    """

    @is_authenticated()
    def get(self, request):
        """

        :return:
        """
        response_dict = dict(username=RMQ_USERNAME,
                             password=RMQ_PASSWORD,
                             url=RMQ_HOST,
                             port=PLAYMAKER_RMQ_PORT,
                             site_pc_id=SITE_PC_ID
                             )

        utils_obj = CheeriosUtils

        encrypted_text = utils_obj.encrypt_text_aes(json.dumps(response_dict), RMQ_CREDS_SYM_KEY)

        return Response(encrypted_text.decode("utf-8"), 200)
