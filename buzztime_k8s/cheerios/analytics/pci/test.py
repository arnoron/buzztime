"""
Test cases for the PCI APIs
"""
import json
import mock

from rest_framework import status
from rest_framework.response import Response
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import get_jwt_token, \
    response_content, response_status, get_expired_jwt_post_event

from cheerios.analytics.pci.test_data import pci_valid_data, pci_empty_field


def mocked_save_data(parameter):
    """
    Mocks the call stored procedure method
    :return:
    """
    return Response("PCI data saved successfully", status.HTTP_200_OK)


class PCIViewTest(APITestCase):
    """
    Test cases for the PCI API
    """

    def setUp(self):
        """
        Initializing method
        :return:
        """
        self.api_endpoint = "/analytics/pci/"

    def test_no_enterprise_id(self):
        """
        Tests when enterprise id is not present
        :return:
        """
        data = pci_empty_field("EnterpriseId")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "Serializer Error: "
                                        "{'EnterpriseId': ['This field is required.']}")

    def test_no_device_id(self):
        """
        Tests when device id is not present
        :return:
        """
        data = pci_empty_field("DeviceID")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'DeviceID': ['This field is required.']}")


    def test_no_serial_number(self):
        """
        Tests when serial number is not present
        :return:
        """
        data = pci_empty_field("SerialNumber")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'SerialNumber': ['This field is required.']}")

    def test_no_condition_name(self):
        """
        Tests when condition name is not present
        :return:
        """
        data = pci_empty_field("ConditionName")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'ConditionName': ['This field is required.']}")

    def test_no_seal_condition_name(self):
        """
        Tests when seal condition name is not present
        :return:
        """
        data = pci_empty_field("SealConditionName")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'SealConditionName': ['This field is required.']}")

    def test_no_question_1(self):
        """
        Tests when question 1 is not present
        :return:
        """
        data = pci_empty_field("Question1")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'Question1': ['This field is required.']}")

    def test_no_answer_1(self):
        """
        Tests when answer 1 is not present
        :return:
        """
        data = pci_empty_field("Answer1")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'Answer1': ['This field is required.']}")

    def test_no_audit_type_name(self):
        """
        Tests when audit type name is not present
        :return:
        """
        data = pci_empty_field("AuditTypeName")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'AuditTypeName': ['This field is required.']}")

    def test_no_inspected_by(self):
        """
        Tests when inspected by is not present
        :return:
        """
        data = pci_empty_field("InspectedBy")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'InspectedBy': ['This field is required.']}")

    def test_no_http_response_code(self):
        """
        Tests when http response code is not present
        :return:
        """
        data = pci_empty_field("HttpResponseCode")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'HttpResponseCode': ['This field is required.']}")

    def test_no_client_date(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("ClientDate")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'ClientDate': ['This field is required.']}")

    def test_no_device_make_id(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("DeviceMakeID")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'DeviceMakeID': ['This field is required.']}")

    def test_no_device_make_name(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("DeviceMakeName")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'DeviceMakeName': ['This field is required.']}")

    def test_no_device_model_id(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("DeviceModelID")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'DeviceModelID': ['This field is required.']}")

    def test_no_device_model_name(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("DeviceModelName")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'DeviceModelName': ['This field is required.']}")

    def test_no_condition_id(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("ConditionID")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'ConditionID': ['This field is required.']}")

    def test_no_setcondition_id(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("SealConditionID")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'SealConditionID': ['This field is required.']}")

    def test_no_location_id(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("LocationID")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'LocationID': ['This field is required.']}")

    def test_no_location_name(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("LocationName")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'LocationName': ['This field is required.']}")

    def test_no_audit_type_id(self):
        """
        Tests when client date is not present
        :return:
        """
        data = pci_empty_field("AuditTypeID")

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Serializer Error: {'AuditTypeID': ['This field is required.']}")

    def test_incorrect_auth_header(self):
        """
        Tests when incorrect auth header passed
        :return:
        """
        data = pci_valid_data()

        response = self.client.post(self.api_endpoint, data, format="json",
                                    HTTP_AUTHORIZATION=get_expired_jwt_post_event())
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Error: Invalid JWT header. Authorization header must be "
                         "JWT token. Got: incomplete format"
                        )

    def test_no_auth_header(self):
        """
        Tests when auth header is not passed
        :return:
        """
        data = pci_valid_data()

        response = self.client.post(self.api_endpoint, data, format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         'Error: JWT Token not found in request parameters nor headers'
                        )


    @mock.patch('cheerios.analytics.pci.pci.PCIView.save_data',
                side_effect=mocked_save_data)
    def test_success_post(self, parameter):
        """
        Tests when successful post is done
        :return:
        """
        data = pci_valid_data()

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:pci"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertEqual(response_data, 'PCI data saved successfully')
