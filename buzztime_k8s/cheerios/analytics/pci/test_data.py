"""
static Data for PCI API
"""
def pci_valid_data():
    """
    Returns valid pci data.
    """
    return {
        "DeviceID": 2,
        "EnterpriseId": 2,
        "SerialNumber": "0112TT999934",
        "ConditionName": "testCondition",
        "SealConditionName": "testSealCondition",
        "Question1": "Test Question",
        "Answer1": "Test Answer",
        "AuditTypeName": "Test Audit Type Name",
        "InspectedBy": "Jaydeep",
        "HttpResponseCode": 200,
        "ClientDate":"2018-02-26 10:30:38.003",
        "DeviceMakeID": 2,
        "DeviceMakeName": "testDeviceMakeName",
        "DeviceModelID": 2,
        "DeviceModelName": "testDeviceModelName",
        "ConditionID": 2,
        "SealConditionID":2,
        "LocationID": 2,
        "LocationName": "Ahemedabad",
        "AuditTypeID": 2
    }

def pci_empty_field(field):
    """
    PCI data with removing one field value.
    """
    data = pci_valid_data()
    del data[field]
    return data
