"""
PCI API
"""


from rest_framework import status
from rest_framework.response import Response

from cheerios.analytics.pci.serializer import PCISerializer
from cheerios.services.jwt_utils import jwt_decorator
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.decorators import close_connection
from cheerios.settings import MSSQLDBPROG

from cheerios.services.log_classifier import Logger

STORED_PROCEDURE_MAPPER = dict(insert_device_pci_check='dbo.usp_ins_DevicePCICheck')
URL = {'post_pci': '/analytics/pci/'}


class PCIView(MSSQLApiView):
    """
    POST api to save FreedomPay DMP sent from Tablet into DB
    """

    @jwt_decorator(URL['post_pci'], 'POST')
    def post(self, request):
        """
        POST API created for saving FreedomPay DMP
        sent from Tablet in DevicePCICheck table.
        :param request:
        :return: 200 status
        """
        request_data = request.data
        serializer = PCISerializer(data=request_data)
        if serializer.is_valid():
            parameter = "@DeviceID='{0}', @EnterpriseId='{1}', @SerialNumber='{2}', @ConditionName='{3}'," \
                        "@SealConditionName='{4}', @Question1='{5}', @Answer1='{6}', @AuditTypeName='{7}'," \
                        "@InspectedBy='{8}', @HttpResponseCode='{9}', @ClientDate='{10}', @DeviceMakeID='{11}'," \
                        "@DeviceMakeName='{12}', @DeviceModelID='{13}', @DeviceModelName='{14}', @ConditionID='{15}'," \
                        "@SealConditionID='{16}', @AuditTypeID='{17}', @LocationID='{18}', @LocationName='{19}'"\
                         .format(request_data.get('DeviceID'),
                                 request_data.get('EnterpriseId'),
                                 request_data.get('SerialNumber'),
                                 request_data.get('ConditionName'),
                                 request_data.get('SealConditionName'),
                                 request_data.get('Question1'),
                                 request_data.get('Answer1'),
                                 request_data.get('AuditTypeName'),
                                 request_data.get('InspectedBy'),
                                 request_data.get('HttpResponseCode'),
                                 request_data.get('ClientDate'),
                                 request_data.get('DeviceMakeID'),
                                 request_data.get('DeviceMakeName'),
                                 request_data.get('DeviceModelID'),
                                 request_data.get('DeviceModelName'),
                                 request_data.get('ConditionID'),
                                 request_data.get('SealConditionID'),
                                 request_data.get('AuditTypeID'),
                                 request_data.get('LocationID'),
                                 request_data.get('LocationName')
                                )
            return self.save_data(parameter)
        else:
            result = "Serializer Error: {0}".format(serializer.errors)
            message = "Error status code 400, {0}".format(result)
            Logger.log.error(message)
            data = request.data
            message = "Event API data :- {0}".format(data)
            Logger.log.info(message)
            return Response(result, status=400)

    @close_connection
    def save_data(self, parameter):
        """
        Saves PCI data into the Database
        :param parameter: The data to be saved into DB
        :return: Response
        """
        cursor = self.get_cursor(MSSQLDBPROG)
        store_procedure = STORED_PROCEDURE_MAPPER['insert_device_pci_check']
        self.call_storeprocedure(store_procedure, cursor, parameter)
        return Response("PCI data saved successfully", status.HTTP_200_OK)
