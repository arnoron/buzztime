"""
Serializer for PCI API
"""

from rest_framework import serializers


class PCISerializer(serializers.Serializer):
    """
    Serializer for PCI
    Check all mandatory fields of database are available.
    """
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    EnterpriseId = serializers.IntegerField(required=True)
    DeviceID = serializers.IntegerField(required=True)
    SerialNumber = serializers.CharField(required=True)
    ConditionName = serializers.CharField(required=True)
    SealConditionName = serializers.CharField(required=True)
    Question1 = serializers.CharField(required=True)
    Answer1 = serializers.CharField(required=True)
    AuditTypeName = serializers.CharField(required=True)
    InspectedBy = serializers.CharField(required=True)
    HttpResponseCode = serializers.IntegerField(required=True)
    ClientDate = serializers.DateTimeField(required=True)
    DeviceMakeID = serializers.IntegerField(required=True)
    DeviceMakeName = serializers.CharField(required=True)
    DeviceModelID = serializers.IntegerField(required=True)
    DeviceModelName = serializers.CharField(required=True)
    ConditionID = serializers.IntegerField(required=True)
    SealConditionID = serializers.IntegerField(required=True)
    AuditTypeID = serializers.IntegerField(required=True)
    LocationID = serializers.IntegerField(required=True)
    LocationName = serializers.CharField(required=True)
