# from mock import patch, Mock
# from rest_framework.test import APITestCase
#
# from cheerios.analytics.test_data import response_content, response_status
#
#
# class TestRMQCreds(APITestCase):
#     """
#     Test Cases for the RabbitMQ get credentials API
#     """
#
#     def test_missing_player_id(self):
#         response = self.client.get("/analytics/rmq_creds/")
#         self.assertEqual(response_status(response), 400)
#         self.assertEqual(response_content(response).decode("utf-8"), "\"PlayerID missing from the request\"")
#
#     @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=['1234'.encode("utf-8"),
#                                                                                '1234'.encode("utf-8")]))
#     def test_incorrect_auth_token(self):
#         response = self.client.get("/analytics/rmq_creds/?player_id=1234")
#         self.assertEqual(response_status(response), 403)
#         self.assertEqual(response_content(response).decode("utf-8"), "\"Auth Token Error\"")
#
#     @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[None]))
#     def test_auth_token_does_not_exist(self):
#         response = self.client.get("/analytics/rmq_creds/?player_id=1234")
#         self.assertEqual(response_status(response), 403)
#         self.assertEqual(response_content(response).decode("utf-8"), "\"Auth Token Error\"")
#
#     @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=['1234'.encode("utf-8"),
#                                                                                '1234'.encode("utf-8")]))
#     def test_successful_transaction(self):
#         response = self.client.get("/analytics/rmq_creds/?player_id=1234", HTTP_AUTHORIZATION="1234")
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(response.content.decode("utf-8"), "\"44ztRlQ2nTkNRuI7c1IQy2bZJ8EGmG98wiQbsBgy"
#                                                            "iqu9OYZ52cu4Zln+lk6krjyFvE5eAZWvKvZPniTKFD"
#                                                            "gIH+Et3EIBMFydlfiouomJq28iUF6K/dN5WPIzfESA"
#                                                            "bI7i1syyVF3HxAklsMAlm9bm4Q==\"")
