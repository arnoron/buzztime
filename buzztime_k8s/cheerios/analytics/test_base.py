"""
Base class for test cases.
"""
# from mongoengine.connection import connect, disconnect
from rest_framework.test import APITestCase

# from cheerios.test_settings import MONGODB, MONGOPORT, MONGOHOST


class TestBase(APITestCase):
    """
    Connect mongodb for test case database when test case starts.
    Disconnect when test case complete.
    """
    def _pre_setup(self):
        """
        Basic function to reate connection with database for test cases.
        It will pick database name and port from test_settings file
        """
#         disconnect()
#         connect(MONGODB, host=MONGOHOST, port=MONGOPORT)
#         super(TestBase, self)._pre_setup()
        pass
