"""
Test cases for the RKI Update API
"""
import json

import mock
from rest_framework import status
from rest_framework.response import Response
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import get_jwt_token, response_content, \
                                         response_status, get_expired_jwt_post_event

def mocked_execute_stored_procedure(self, execute_stored_procedure):
    """
    Mocks the call stored procedure method
    :return:
    """
    return Response("RKI entry successfully updated", status.HTTP_200_OK)


class TestRKIUpdate(APITestCase):
    """
    Test cases for the RKI Update API
    """


    def setUp(self):
        """
        Initializing method
        :return:
        """
        self.api_endpoint = "/analytics/rki/"

    def test_no_auth_header(self):
        """
        Tests the case when no authorization header is passed
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                    RKIKeyID='37216',
                    ClientDate='2017-08-01T14:33:00',
                    RKIKeyStatusID=1)

        response = self.client.post(self.api_endpoint, data, format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         'Error: JWT Token not found in request parameters nor headers'
                        )

    def test_incorrect_auth_header(self):
        """
        Tests the case when incorrect authorization header is passed
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                    RKIKeyID='37216',
                    ClientDate='2017-08-01T14:33:00',
                    RKIKeyStatusID=1)

        response = self.client.post(self.api_endpoint, data, format="json",
                                    HTTP_AUTHORIZATION=get_expired_jwt_post_event())
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data,
                         "Error: Invalid JWT header. Authorization header must be "
                         "JWT token. Got: incomplete format"
                        )

    def test_incorrect_time_format(self):
        """
        Tests the case when no authorization header is passed
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                    RKIKeyID='37216',
                    ClientDate='XYZ@#$',
                    RKIKeyStatusID=1)

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:rki"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "Serializer Error: {'ClientDate': ['Incorrect time format passed']}")

    def test_incorrect_status_value(self):
        """
        Tests the case when incorrect status value is passed
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                    RKIKeyID='37216',
                    ClientDate='2017-08-01T14:33:00',
                    RKIKeyStatusID='Invalid')

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:rki"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "Serializer Error: {'RKIKeyStatusID': "
                                        "['\"Invalid\" is not a valid choice.']}")

    def test_no_tablet_sno(self):
        """
        Tests the case when no tablet serial number is passed
        :return:
        """
        data = dict(RKIKeyID='37216',
                    ClientDate='2017-08-01T14:33:00',
                    RKIKeyStatusID=1)

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:rki"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "Serializer Error: {'SerialNumber': ['This field is required.']}")

    def test_no_site_id(self):
        """
        Tests the case when no site id is passed
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                    ClientDate='2017-08-01T14:33:00',
                    RKIKeyStatusID=1)

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:rki"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "Serializer Error: {'RKIKeyID': ['This field is required.']}")

    def test_no_time(self):
        """
        Tests the case when time is not passed
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                    RKIKeyID='37216',
                    RKIKeyStatusID=1)

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:rki"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "Serializer Error: {'ClientDate': ['This field is required.']}")

    def test_no_status_value(self):
        """
        Tests the case when no status value is passed
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                    RKIKeyID='37216',
                    ClientDate='2017-08-01T14:33:00'
                   )

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:rki"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_data, "Serializer Error: {'RKIKeyStatusID': ['This field is required.']}")

    @mock.patch('cheerios.analytics.rki.RKIUpdateViewSet.execute_stored_procedure',
                side_effect=mocked_execute_stored_procedure)
    def test_successful_updation_rki(self, mocked_stored_procedure):
        """
        Tests the case when successful updation of RKI takes place
        :return:
        """
        data = dict(SerialNumber='R52G417WGXH',
                   RKIKeyID='37216',
                   ClientDate='2017-08-01T14:33:00',
                   RKIKeyStatusID=1)

        response = self.client.post(self.api_endpoint, data,
                                    HTTP_AUTHORIZATION=get_jwt_token("analytics:rki"),
                                    format="json")
        response_data = json.loads(response_content(response).decode("utf-8"))
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertEqual(response_data, 'RKI entry successfully updated')
