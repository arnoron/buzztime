"""ucars URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from cheerios.analytics import views
from cheerios.analytics.rki import RKIUpdateViewSet

from cheerios.analytics.rki import RKIUpdateViewSet
from cheerios.analytics.pci.pci import PCIView
from cheerios.analytics.views import RabbitMQCredsViewSet

urlpatterns = [
    # url(r'^event/$', views.EventViewSet.as_view(), name="event"),
    url(r'^ping/$', views.PingView.as_view()),
    # url(r'^healthcheck$', views.HealthCheckView.as_view()),
    url(r'^rki/$', RKIUpdateViewSet.as_view(), name="rki"),
    url(r'^pci/$', PCIView.as_view(), name="pci"),
    url(r'^rmq_creds/$', RabbitMQCredsViewSet.as_view(), name="rabbitmq_credentials_api")
]
