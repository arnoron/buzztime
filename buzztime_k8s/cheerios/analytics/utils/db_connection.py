"""
Function for mongodb database connections.
"""

from mongoengine.connection import register_connection
from cheerios.analytics.constants import DEFAULTEVENTCATEGORY, \
    DEFAULTEVENTCOLLECTION, MONGO_NAME_PATTERN, CHEERIOS_CONFIG_SP
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.settings import MSSQLDBPROG, MONGOHOST


class MongoConnection(object):
    """
    Class containing momgo connetion functions
    """
    @staticmethod
    def get_db_connection_name(new_data):
        """
        This method gets db name and collection name from
        'en'
        """
        db_details = {}
        db_details['db_name'] = "default"
        db_details['collection_name'] = DEFAULTEVENTCOLLECTION
        if new_data.get("en"):
            event_name = new_data.get("en").split('.')
            if len(event_name) > 1:
                if event_name[0].strip() != "":
                    db_details['db_name'] = event_name[0]
                if event_name[1].strip() != "":
                    db_details['collection_name'] = event_name[1]
            if len(event_name) == 1:
                db_details['collection_name'] = event_name[0]
        return db_details

    def register_db(self, new_data_list):
        """
        Register database to mongoengine connection list
        """
        username, password = self.get_mongodb_credentials()
        register_connection('default', DEFAULTEVENTCATEGORY, username=username, host=MONGOHOST,
                            password=password, authentication_source='EventService',
                            authentication_mechanism='MONGODB-CR')
        for new_data in new_data_list:
            if new_data.get("en") and new_data.get("en").split('.')[0]:
                event = new_data.get("en").split('.')[0]
                register_connection(event, DEFAULTEVENTCATEGORY, username=username, password=password,
                                    host=MONGOHOST, authentication_mechanism='MONGODB-CR')

    @staticmethod
    def get_mongodb_credentials():
        """
        This function will connect to the database and execute the SP
        'dbo.usp_sel_PlatformServicesConfiguration' to get the credentials by which
        to authenticate the mongodb calls
        :return:
        """
        creds = dict()

        data = (MONGO_NAME_PATTERN,)
        api_view = MSSQLApiView()

        cursor = api_view.get_cursor(MSSQLDBPROG)
        api_view.call_storeprocedure(CHEERIOS_CONFIG_SP, cursor, data)
        response = api_view.fetchall()

        for auth_item in ['username', 'password']:
            creds[auth_item] = list(filter(lambda record:
                               record.get('Name') == 'Cheerios.Mongo.{}'.format(auth_item.title()), response))

        return creds['username'][0]['Value'], creds['password'][0]['Value']
