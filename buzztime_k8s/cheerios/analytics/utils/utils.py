"""
Function to help Record to convert for MongoDb.
modify_data :- Modify data with fields in db.
map_data :- Mapping passed data with fields in db.
convert_str_to_datetime :- converting string data to date and time object.
create_key_value_pairs :- converting string or list data to dictionary.
"""
import collections
import datetime
import json
import time

from cheerios.utils import CheeriosUtils

CHEERIOS_UTILS = CheeriosUtils()


class UtilFunction(object):
    """
    Class contains various util functions
    like convert string to datetime object,
    convert datetime object to string and etc
    """
    @classmethod
    def modify_data(cls, data):
        """
        Modify data with fields in db.
        Added en, es, edt data which are same as name, source and time.
        created key value pair as a dictionary from keyValuePairs json object.
        """
        new_data = {}
        new_data['en'] = data.get("name")
        new_data['es'] = data.get("source")
        new_data['edt'] = CHEERIOS_UTILS.convert_str_to_datetime(data.get("time"))
        new_data = cls.create_key_value_pairs(data.get("keyValuePairs"), new_data)
        return new_data

    @classmethod
    def map_data(cls, data):
        """
        Mapping passed data with fields in db.
        Add fileds en, es, edt and key value pair for database.
        If multiple record is passes then modify each record
        else modify only one record.
        """
        new_data_list = []
        if isinstance(data, list):
            for each_data in data:
                new_data_list.append(cls.modify_data(each_data))
        else:
            new_data_list.append(cls.modify_data(data))
        return new_data_list

    @staticmethod
    def create_key_value_pairs(key_value_pairs, new_data):
        """
        converting string or list data to dictionary.
        If data is available in string then convert that into dictionary.
        If String format is not match with json format then give error.
        Take each key and add it in to object with respective value.
        """
        decoder = json.JSONDecoder(object_pairs_hook=collections.OrderedDict)
        word_list = ['en', 'edt', 'es', '_c', '_id']
        if key_value_pairs:
            if isinstance(key_value_pairs, str):
                try:
                    key_value_pairs = decoder.decode(key_value_pairs)
                except ValueError:
                    raise ValueError("Incorrect keyValuePairs  format, "
                                     "should be {\"key\":\"value\"}")
            for key, value in key_value_pairs.items():
                if key not in word_list:
                    new_data[key] = value
        return new_data
