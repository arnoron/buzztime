"""
Function to validate JWT. Default JWT secret keys are saved in settings.py
Below function will decode passed payload using the list of saved secret keys.
Once its decoded loop will break and that data will be used.
If it doesn't decode respective Errors will be raised.
"""

from datetime import datetime
from functools import wraps
from rest_framework.response import Response
import jwt

from cheerios.analytics.constants import JWT_VALID_TIME_INTERVAL
from cheerios.analytics.utils.utils import UtilFunction
from cheerios.settings import JWT_SECRET_KEYS
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


def jwt_decorator(end_point=None, method=None):
    """
    Parameterized Decorator to validate token.
    """
    def require_jwt(func):
        """
        Decorator to validate token.
        func argument have definition of function which calls this Decorator
        """
        def inner(request, *args, **kwargs):
            """
            This function validate jwt token.
            If token is valid then it will call view function.
            otherwise it will throw exception.
            """
            try:
                validate_jwt(args[0], end_point, method)
                response = func(request, *args, **kwargs)
            except jwt.exceptions.InvalidTokenError as inst:
                result = "Error: {0}".format(inst.args[0])
                message = "Error status code 400, {0}".format(result)
                Logger.log.error(message)
                return Response(result, status=400)
            except ValueError as inst:
                result = "Error: {0}".format(inst.args[0])
                message = "Error status code 400, {0}".format(result)
                Logger.log.error(message)
                return Response(result, status=400)
            except Exception as inst:
                result = "Error: {0}".format(inst.args[0])
                message = "Error status code 500, {0}".format(result)
                Logger.log.error(message)
                return Response(result, status=500)
            return response
        return wraps(func)(inner)
    return require_jwt


def validate_jwt(request, end_point, method):
    """
    This function first checks if 'HTTP_AUTHORIZATION' is passed in Header
    or 'jwt' is passed in 'request.data'.
    Followed by several validation including:
    1. Check if HTTP_AUTHORIZATION is passed
    2. Check if HTTP_AUTHORIZATION has value associated with it
    3. If there is only one JSON token forwarded
    4. Try to decode payload using list of Secret keys,
        If fails to decode using all the keys then raise error.
    """
    try:
        jwt_data = None
        if request.META and 'HTTP_AUTHORIZATION' in request.META or 'jwt' in request.data:
            jwt_decrypted_data = {}
            message = ""
            jwt_data = request.META.get('HTTP_AUTHORIZATION', None)
            if jwt_data:
                parts = jwt_data.split()
                if len(parts) == 0:
                    result = "Invalid JWT header. Token not found. Got: {0} ".format(jwt_data)
                    message = "Error status code 400,  {0}".format(result)
                    Logger.log.error(message)
                    raise jwt.exceptions.InvalidTokenError(result)
                elif len(parts) > 1:
                    result = "Invalid JWT header. Authorization header must be " \
                             "JWT token. Got: incomplete format"
                    message = "Error status code 400,  {0}".format(result)
                    Logger.log.error(message)
                    raise jwt.exceptions.InvalidTokenError(result)
                jwt_data = parts[0]

            # if JWT token was not found in the header then check the request parameters
            if not jwt_data:
                jwt_data = request.data.get('jwt', None)

            secret_key = get_jwt_secret_from_url(end_point)

            try:
                jwt_decrypted_data = jwt.decode(jwt_data, secret_key, \
                    algorithms=['HS256'], leeway=10)
                result = "Extracted data to post from JWT token:  {0}"\
                    .format(jwt_decrypted_data)
                Logger.log.info(result)
            except jwt.exceptions.DecodeError as excp:
                error_message = "Data extraction using secret key failed. Error: {0}"\
                    .format(excp.args[0])

                message = "Error status code 400,  {0}".format(error_message)
                Logger.log.error(message)

            # Check if jwt token is older than 5 minutes
            if jwt_decrypted_data.get('time'):
                datetime_object = abs((datetime.utcnow() - CHEERIOS_UTILS.convert_str_to_datetime(\
                    jwt_decrypted_data['time'])).\
                    total_seconds())
                if datetime_object > JWT_VALID_TIME_INTERVAL:
                    result = "Invalid JWT token. JWT token expired"
                    message = "Error status code 400,  {0}".format(result)
                    Logger.log.error(message)
                    raise jwt.exceptions.InvalidTokenError(result)

                if jwt_decrypted_data['REQUEST_METHOD'] == method and \
                    jwt_decrypted_data['PATH_INFO'] == end_point:
                    return True
                else:
                    raise jwt.exceptions.InvalidTokenError("Invalid JWT token")

            else:
                Logger.log.error(error_message)
                raise jwt.exceptions.InvalidTokenError(error_message)
        else:
            result = "JWT Token not found in request parameters nor headers"
            message = "Error status code 400,  {0}".format(result)
            Logger.log.error(message)
            raise jwt.exceptions.InvalidTokenError(result)

    except jwt.exceptions.InvalidTokenError as excp:
        if request.data:
            data = request.data
            message = "Event API data :- {0}".format(data)
            Logger.log.info(message)
        raise jwt.exceptions.InvalidAlgorithmError(excp.args[0])
    except ValueError as excp:
        if request.data:
            data = request.data
            message = "Event API data :- {0}".format(data)
            Logger.log.info(message)
        raise ValueError("Jwt Token :-{0}".format(excp.args[0]))
    except Exception as excp:
        if request.data:
            data = request.data
            message = "Event API data :- {0}".format(data)
            Logger.log.info(message)
        raise jwt.exceptions.InvalidAlgorithmError(excp.args[0])


def get_jwt_secret_from_url(url):
    """

    :return:
    """

    for key, value in JWT_SECRET_KEYS.items():
        if key in url:
            return value
