"""
Test case for Event Model
"""
# 
# from rest_framework import status
# 
# from cheerios.analytics.constants import URL
# from cheerios.analytics.test_base import TestBase
# from cheerios.analytics.test_data import get_invalid_event_data,\
#     get_valid_single_event_data, get_valid_multiple_event_data,\
#     get_valid_invalid_date, get_invalid_key_value_pair,\
#     get_event_without_key_value, get_jwt_with_future_date,\
#     get_single_event_without_jwt, get_single_event_invalid_jwt,\
#     get_jwt_token, get_valid_event_jwt_object, get_expired_jwt_post_event,\
#     get_jwt_get_event, get_sql_injection_jwt, get_multiple_event_mix_category,\
#     get_multiple_event_category, get_single_event_category_data,\
#     get_without_event_category_name, get_jwt_incorrect_date_format,\
#     response_status
# from cheerios.feeds.test.test_data import create_log_file, remove_test_log_file

# class TestEventModal(TestBase):
#     """
#     Testcase for post operation.
#     """
#     def setUp(self):
#         self.url = URL['post-event']
#         create_log_file()
#
#     # def test_post_operation(self):
#     #     """
#     #     When Proper input is passed
#     #     """
#     #     data = get_valid_single_event_data()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     # def test_post_incomplete_data(self):
#     #     """
#     #     When Proper input is not passed
#     #     """
#     #     data = get_invalid_event_data()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     # def test_post_multiple_events(self):
#     #     """
#     #     When A List of events is passed
#     #     """
#     #     data = get_valid_multiple_event_data()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_post_invalid_date(self):
#         """
#         When Date format is invalid
#         """
#         data = get_valid_invalid_date()
#         response = self.client.post(self.url, data, format='json',
#                                     HTTP_AUTHORIZATION=get_jwt_post_event())
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_post_invalid_key_value(self):
#         """
#         When keyValuePair format is invalid
#         """
#         data = get_invalid_key_value_pair()
#         response = self.client.post(self.url, data, format='json',
#                                     HTTP_AUTHORIZATION=get_jwt_post_event())
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     # def test_post_without_key_value(self):
#     #     """
#     #     When event data without keyValuePair
#     #     """
#     #     data = get_event_without_key_value()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     # def test_post_jwt(self):
#     #     """
#     #     Passing valid JWT as part of data
#     #     """
#     #     data = get_valid_event_jwt_object()
#     #     response = self.client.post(self.url, data, format='json')
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_post_without_jwt(self):
#         """
#         Not Passing JWT as part of data
#         """
#         data = get_single_event_without_jwt()
#         response = self.client.post(self.url, data, format='json')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_post_with_invalid_jwt(self):
#         """
#         Passing invalid JWT as part of data
#         """
#         data = get_single_event_invalid_jwt()
#         response = self.client.post(self.url, data, format='json')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_post_sql_injection_jwt(self):
#         """
#         Passing expired JWT with sql injection.
#         """
#         data = get_sql_injection_jwt()
#         response = self.client.post(self.url, data, format='json')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     # def test_post_with_jwt_header(self):
#     #     """
#     #     Passing valid JWT in Header
#     #     """
#     #     data = get_single_event_without_jwt()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_post_invalid_jwt_header(self):
#         """
#         Passing invalid JWT in Header
#         """
#         data = get_single_event_without_jwt()
#         response = self.client.post(self.url, data, format='json',
#                                     HTTP_AUTHORIZATION=get_jwt_post_event() +
#                                     ' ' + get_jwt_post_event())
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_post_with_jwt_invalid_time(self):
#         """
#         Passing expired JWT in Header
#         """
#         data = get_single_event_without_jwt()
#         response = self.client.post(self.url, data, format='json',
#                                     HTTP_AUTHORIZATION=get_expired_jwt_post_event())
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_post_with_jwt_for_get(self):
#         """
#         Passing 'GET' JWT for 'POST' call in Header
#         """
#         data = get_single_event_without_jwt()
#         response = self.client.post(self.url, data, format='json',
#                                     HTTP_AUTHORIZATION=get_jwt_get_event())
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_post_without_data(self):
#         """
#         Pass empty data.
#         """
#         data = {}
#         response = self.client.post(self.url, data, format='json',
#                                     HTTP_AUTHORIZATION=get_jwt_post_event())
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     # def test_post_single_event_category(self):
#     #     """
#     #     When Proper input is passed
#     #     We also pass the category name in name attribute. pattern should be category1.event1
#     #     """
#     #     data = get_single_event_category_data()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     # def test_multiple_event_category(self):
#     #     """
#     #     When Proper input is passed
#     #     We also pass the category name in name attribute. pattern should be category1.event1
#     #     all object has category name.
#     #     """
#     #     data = get_multiple_event_category()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     # def test_post_multiple_event_mix(self):
#     #     """
#     #     When Proper input is passed
#     #     We also pass the category name in name attribute. pattern should be category1.event1
#     #     Not each event object have category value.
#     #     Some object contains category name and some object do't have category name.
#     #     """
#     #     data = get_multiple_event_mix_category()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     # def test_post_with_spaces_dots(self):
#     #     """
#     #     When single Event data which contains only dot and spaces in name.
#     #     It will save data in default db and default collection
#     #     """
#     #     data = get_without_event_category_name()
#     #     response = self.client.post(self.url, data, format='json',
#     #                                 HTTP_AUTHORIZATION=get_jwt_post_event())
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_post_jwt_with_future_date(self):
#         """
#         Forwarding JWT with future date should not be acceptable
#         """
#         data = get_valid_multiple_event_data()
#         response = self.client.post(self.url, data, format='json',
#                                     HTTP_AUTHORIZATION=get_jwt_with_future_date())
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_jwt_incorrect_date_format(self):
#         """
#         Forwarding JWT with incorrect format date should not be acceptable
#         """
#         data = get_jwt_incorrect_date_format()
#         response = self.client.post(self.url, data, format='json')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#

#     def tearDown(self):
#         remove_test_log_file()
