"""
This module contains the RKI Updation event from tablet
"""
import logging

from rest_framework import status
from rest_framework.response import Response

from cheerios.analytics.serializers import RKIUpdateSerializer
from cheerios.services.jwt_utils import jwt_decorator
from cheerios.players.decorators import get_store_procedure_api

from cheerios.players.views.players import PlayersViewSet
from cheerios.settings import MSSQLDBPROG

from cheerios.services.log_classifier import Logger

URL = {'post_rki': '/analytics/rki/'}
STORED_PROCEDURE_MAPPER = dict(rki_update='usp_upd_DeviceRKI')


class RKIUpdateViewSet(PlayersViewSet):
    """
    This method takes the data passed. It validates the
    data through a serializer and if valid, executes the stored
    procedure to save the updated data in the database
    """

    @jwt_decorator(URL['post_rki'], 'POST')
    def post(self, request):
        """
        Posts the data to the database
        :param request:
        :return:
        """
        request_data = request.data

        serializer = RKIUpdateSerializer(data=request_data)

        stored_procedure = STORED_PROCEDURE_MAPPER['rki_update']

        if not serializer.is_valid():
            result = "Serializer Error: {0}".format(serializer.errors)
            message = "Error status code 400, {0}".format(result)
            Logger.log.error(message)
            data = request.data
            message = "Confidential API data :- {0}".format(data)
            Logger.log.error(message)
            return Response(result, status=400)

        data_to_post = "@SerialNumber='{0}', @RKIKeyID='{1}', " \
                       "@RKIKeyStatusID='{2}', @ClientDate='{3}'".format(
                           request_data.get('SerialNumber'),
                           request_data.get('RKIKeyID'),
                           request_data.get('RKIKeyStatusID'),
                           request_data.get('ClientDate'))
        return self.execute_stored_procedure(stored_procedure, data_to_post)

    @get_store_procedure_api
    def execute_stored_procedure(self, store_procedure, data_to_post):
        """
        Function which calls stored procedure to update the data
        :param store_procedure:
        :param data_to_post:
        :return:
        """
        cursor = self.get_cursor(MSSQLDBPROG)
        self.call_storeprocedure(store_procedure, cursor, data_to_post)
        return Response("RKI entry successfully updated", status.HTTP_200_OK)
