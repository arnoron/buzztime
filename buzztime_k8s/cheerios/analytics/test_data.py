"""
static Data for all Collections
"""
from datetime import timedelta
import datetime
import jwt
from django.core.urlresolvers import reverse

from cheerios.constant.constant import REGISTERED_CLAIMS_VALID_VALS
from cheerios.services.jwt_utils import get_jwt_secret_from_url
from cheerios.analytics.utils.utils import UtilFunction
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


def response_status(response):
    """
    Common function which send response status code from response.
    """
    return response.status_code


def response_data(response):
    """
    Common function which sends response data
    """
    return response.data


def response_content(response):
    """
    Common function which send response content from response.
    """
    return response.content


def get_invalid_event_data():
    """
    Send invalid Event data
    """
    return [{
        'source':'tablet2',
        'time': '2012-08-24T14:33:00',
        'keyValuePairs': {
            'ac': 'Appearance of view',
            'sid': '37216',
            'sc': 'Premium Arcade',
            'sn': 'R52G417WGXH',
            's': 'deb559ea-6014-46ed-b49f-126c874bd30a',
            'et': 'Navigation',
            'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'
        }
    }]


def get_valid_single_event_data():
    """
    Send single Event data which is store in list.
    """
    return [{\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }]


def get_valid_invalid_date():
    """
    Send single Event data which is store in list and date format is invalid.
    """
    return [{\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }]


def get_invalid_key_value_pair():
    """
    Send single Event data which is store in list and key value pair format is invalid.
    """
    return [{\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': '''{
                'ac': 'Appearance of view',
                'sid': '37216',
                'sc': 'Premium Arcade',
                'sn': 'R52G417WGXH',
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',
                'et': 'Navigation',
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'
            }'''\
        }]


def get_valid_single_event_object():
    """
    Send single Event data which is store in object.
    """
    return {\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }


def get_valid_multiple_event_data():
    """
    Send multiple Event data which is store in list.
    """
    return [{\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        },\
        {\
            'name':'event3',\
            'source':'tablet3',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }\
    ]


def get_key_value_pair():
    """
    Send KeyValuePair data which is store in object.
    """
    return {\
            "ac": "Appearance of view",\
            "sid": "37216",\
            "sc": "Premium Arcade",\
            "sn": "R52G417WGXH",\
            "s": "deb559ea-6014-46ed-b49f-126c874bd30a",\
            "et": "Navigation",\
            "md": "gamelobby.activities.GameBrowseActivity.appPanel_onStart"\
        }


def get_key_value_pair_string():
    """
    Send KeyValuePair data which is store in string.
    """
    return '''{
            "ac": "Appearance of view",
            "sid": "37216",
            "sc": "Premium Arcade",
            "sn": "R52G417WGXH",
            "s": "deb559ea-6014-46ed-b49f-126c874bd30a",
            "et": "Navigation",
            "md": "gamelobby.activities.GameBrowseActivity.appPanel_onStart"
        }'''


def get_key_value_incorrect_format():
    """
    Send KeyValuePair data which is store in string but json format is incorrect.
    """
    return '''{
            'ac': 'Appearance of view',
            'sid': '37216',
            'sc': 'Premium Arcade',
            'sn': 'R52G417WGXH',
            's': 'deb559ea-6014-46ed-b49f-126c874bd30a',
            'et': 'Navigation',
            'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'
        }'''


def get_event_for_key_value():
    """
    Send Event data with out suppling KeyValuePair data"""

    return {\
            "en": "event2",\
            "es": "tablet2",\
            "edt": "2012-08-24T14:33:00"\
            }


def get_event_without_key_value():
    """Send Event data with out suppling KeyValuePair data
    """
    return {\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00'\
            }


def get_jwt_token(url_name):
    """
    Return Json Web Token for post Event operation
    """
    secret = get_jwt_secret_from_url(url_name)
    date = CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.utcnow())
    encoded = jwt.encode({"REQUEST_METHOD": "POST",\
                          "PATH_INFO": reverse(url_name),\
                          "time": date,
                          "iss": REGISTERED_CLAIMS_VALID_VALS['iss'],
                          "aud": REGISTERED_CLAIMS_VALID_VALS['aud']
                          },\
                          secret, algorithm='HS256',\
                          headers={\
                              "alg": "HS256",\
                              "typ": "JWT"\
                         })
    return encoded.decode("utf-8")


def get_jwt_with_future_date():
    """
    Return Json Web Token for post Event operation
    """
    date = CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.utcnow() +
                                                 timedelta(hours=-5, minutes=-30))
    encoded = jwt.encode({"REQUEST_METHOD": "POST",\
                          "PATH_INFO": "/analytics/event/",\
                          "time": date\
                          },\
                          'secret', algorithm='HS256',\
                          headers={\
                              "alg": "HS256",\
                              "typ": "JWT"\
                         })
    return encoded.decode("utf-8")


def get_expired_jwt_post_event():
    """
    Returns old Expired JWT for /analytics/event/
    """
    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJSRVFVRVNUX01FVEhPRCI6IlBPU1Q\
        iLCJQQVRIX0lORk8iOiIvYW5hbHl0aWNzL2V2ZW50LyIsInRpbWUiOiIyMDEyLTA4LTI0VDE0OjMzOjA\
        wIn0.SBe1QyK-u8JCaWGrrOaVG1dwDNmcOdglcZgiPoxhh4w"


def get_jwt_get_event():
    """
    Return JWT with Method 'GET' for /analytics/event/
    """
    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJSRVFVRVNUX01FVEhPRCI6IkdFVCIs\
        IlBBVEhfSU5GTyI6Ii9hbmFseXRpY3MvZXZlbnQvIiwidGltZSI6IjIwMTYtMDYtMjBUMTM6MTU6MD\
        AifQ.QeLfC-Ehq5Bbp443_YDDyMQUOFyadx1qXiKwirD4sM8"


def get_valid_event_jwt_object():
    """
    Send single Event data which is store in object.
    """
    return {\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            },\
            'jwt': get_jwt_token("analytics:event")\
        }


def get_single_event_without_jwt():
    """
    Send single Event data which is store in object without JWT.
    """
    return {\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }


def get_single_event_invalid_jwt():
    """
    Send single Event data which is store in object without invaild JWT.
    """
    return {\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            },\
            'jwt': get_jwt_token("analytics:event") + ' ' + get_jwt_token("analytics:event")\
        }


def get_sql_injection_jwt():
    """
    Send single Event data which is store in object and expired JWT with sql injection.
    """
    return {\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            },\
            'jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJSRVFVRVNUX01FVEhPRCI6IlBPU1QiL\
                CJQQVRIX0lORk8iOiIvYW5hbHl0aWNzL2V2ZW50LyIsInRpbWUiOiIyMDEyLTA4LTI0VDE0OjMzOjAwIn0.SBe1QyK-\
                u8JCaWGrrOaVG1dwDNmcOdglcZgiPoxhh4w or 1=1'\
        }


def get_single_event_category_data():
    """
    Send single Event data which is store in list.
    We also pass the category name in name attribute. pattern should be category1.event1
    """
    return [{\
            'name':'category1.event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }]


def get_jwt_incorrect_date_format():
    """
    Send single Event data which is store in object and expired JWT with incorrect date format.
    """
    return {\
            'name':'event2',\
            'source':'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            },\
            'jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJSRVFVRVNUX01FVEhPRCI6IlBPU1QiL\
                CJQQVRIX0lORk8iOiIvYW5hbHl0aWNzL2V2ZW50LyIsInRpbWUiOiIyMDE2LTA3LTIxVDEwOjU0In0.7sFGGk3xVKCR\
                3Wsq_gCsfbPdsf4sRp7jVlvQY7nYjQQ'\
        }


def get_multiple_event_category():
    """
    Send multiple Event data which is store in list.
    We also pass the category name in name attribute. pattern should be category1.event1
    all object has category name.
    """
    return [{\
           'name':'category1.event2',\
           'source':'tablet2',\
           'time': '2012-08-24T14:33:00',\
           'keyValuePairs': {\
               'ac': 'Appearance of view',\
               'sid': '37216',\
               'sc': 'Premium Arcade',\
               'sn': 'R52G417WGXH',\
               's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
               'et': 'Navigation',\
               'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
           }\
       },\
       {\
           'name':'category2.event3',\
           'source':'tablet3',\
           'time': '2012-08-24T14:33:00',\
           'keyValuePairs': {\
               'ac': 'Appearance of view',\
               'sid': '37216',\
               'sc': 'Premium Arcade',\
               'sn': 'R52G417WGXH',\
               's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
               'et': 'Navigation',\
               'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }\
    ]


def get_multiple_event_mix_category():
    """
    Send multiple Event data which is store in list.
    We also pass the category name in name attribute. pattern should be category1.event1
    Not each event object have category value.
    Some object contains category name and some object do't have category name.
    """
    return [{\
           'name':'category1.event2',\
           'source':'tablet2',\
           'time': '2012-08-24T14:33:00',\
           'keyValuePairs': {\
               'ac': 'Appearance of view',\
               'sid': '37216',\
               'sc': 'Premium Arcade',\
               'sn': 'R52G417WGXH',\
               's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
               'et': 'Navigation',\
               'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
           }\
       },\
       {\
           'name':'event3',\
           'source':'tablet3',\
           'time': '2012-08-24T14:33:00',\
           'keyValuePairs': {\
               'ac': 'Appearance of view',\
               'sid': '37216',\
               'sc': 'Premium Arcade',\
               'sn': 'R52G417WGXH',\
               's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
               'et': 'Navigation',\
               'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }\
    ]


def get_category_name():
    """
    Send single Event data which stores the category name only.
    """
    return {\
            'name': 'category1.',\
            }



def get_event_name():
    """
    Send single Event data which stores the event name only.
    """
    return {\
            'name': '.event1',\
            }


def get_without_event_category_name():
    """
    Send single Event data which contains only dot and spaces in name.
    """
    return {\
            'name': ' . ',\
            }


def get_event_category_extra_data():
    """
    Send single Event data which is store in list.
    We also pass the category name in name attribute. pattern should be category1.event1
    """
    return [{\
            'name': 'category1.event2',\
            'en': 'name',
            'source': 'tablet2',\
            'time': '2012-08-24T14:33:00',\
            'keyValuePairs': {\
                'es': 'source test name2',\
                'ac': 'Appearance of view',\
                'sid': '37216',\
                'sc': 'Premium Arcade',\
                'sn': 'R52G417WGXH',\
                's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
                'et': 'Navigation',\
                'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart'\
            }\
        }]


def get_db_collection_mapped_data():
    """
    Send single Event mapped data with es=temp.test
    """
    return {\
            'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart',\
            'sc': 'Premium Arcade',\
            'et': 'Navigation',\
            's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
            'ac': 'Appearance of view',\
            'edt': datetime.datetime(2012, 8, 24, 14, 33),\
            'sn': 'R52G417WGXH',\
            'sid': '37216',\
            'es': 'tablet29',\
            'en': 'temp.test'\
    }


def get_db_mapped_data():
    """
    Send single Event mapped data with es=temp
    """
    return {\
            'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart',\
            'sc': 'Premium Arcade',\
            'et': 'Navigation',\
            's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
            'ac': 'Appearance of view',\
            'edt': datetime.datetime(2012, 8, 24, 14, 33),\
            'sn': 'R52G417WGXH',\
            'sid': '37216',\
            'es': 'tablet29',\
            'en': 'temp'\
    }


def get_collection_mapped_data():
    """
    Send single Event mapped data with es=temp
    """
    return {\
            'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart',\
            'sc': 'Premium Arcade',\
            'et': 'Navigation',\
            's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
            'ac': 'Appearance of view',\
            'edt': datetime.datetime(2012, 8, 24, 14, 33),\
            'sn': 'R52G417WGXH',\
            'sid': '37216',\
            'es': 'tablet29',\
            'en': '.test'\
    }


def get_without_name_mapped_data():
    """
    Send single Event mapped data with es=temp
    """
    return {\
            'md': 'gamelobby.activities.GameBrowseActivity.appPanel_onStart',\
            'sc': 'Premium Arcade',\
            'et': 'Navigation',\
            's': 'deb559ea-6014-46ed-b49f-126c874bd30a',\
            'ac': 'Appearance of view',\
            'edt': datetime.datetime(2012, 8, 24, 14, 33),\
            'sn': 'R52G417WGXH',\
            'sid': '37216',\
            'es': 'tablet29'\
    }

def get_jwt_token_delete(url_name):
    """

    :return:
    """
    secret = get_jwt_secret_from_url(url_name)
    date = CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.utcnow())
    encoded = jwt.encode({"REQUEST_METHOD": "DELETE", \
                          "PATH_INFO": reverse(url_name), \
                          "time": date \
                          }, \
                         secret, algorithm='HS256', \
                         headers={ \
                             "alg": "HS256", \
                             "typ": "JWT" \
                             })
    return encoded.decode("utf-8")
