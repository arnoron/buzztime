"""
Serializer classes
"""
import re
from rest_framework import serializers
from cheerios.analytics.models import Event


class EventSerializer(serializers.Serializer):
    """
    Serializer for Event Model
    Check all mandatory fields of database are available.
    """
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    en = serializers.CharField(allow_null=True, required=False)
    es = serializers.CharField(allow_null=True, required=False)
    edt = serializers.DateTimeField(allow_null=True, required=False)


class EventSerializerforInternelModel(serializers.ModelSerializer):
    """
    Serializer for Event Model to show field in doc.
    """
    class Meta:
        """
        Meta class for print fields in DRF doc.
        """
        fields = ('name', 'source', 'time', 'keyValuePairs')
        model = Event


class RKIUpdateSerializer(serializers.Serializer):
    """
    Serializer to validate requests for RKI API calls
    """

    RKIKeyID = serializers.CharField(required=True, allow_blank=True)
    RKIKeyStatusID = serializers.ChoiceField([1,0])
    ClientDate = serializers.CharField(required=True)
    SerialNumber = serializers.CharField(required=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    @staticmethod
    def validate_RKIKeyID(RKIKeyID):
        """
        Method to validate if the RKI Key ID is in hex format or not
        :return:
        """
        try:
            _ = int(RKIKeyID, 16)
        except ValueError:
            raise serializers.ValidationError('RKIKeyID is not in hexadecimal format. '
                                              'Should contain only digits and letters A to F')

    @staticmethod
    def validate_SerialNumber(SerialNumber):
        """
        Method to validate if the serial number is alphanumeric or not
        :return:
        """
        if not SerialNumber.isalnum():
            raise serializers.ValidationError('SerialNumber passed is not alphanumeric')

    @staticmethod
    def validate_ClientDate(ClientDate):
        """
        Validates the time to be in 2017-08-01T14:33:00 format,
        else raises validation error
        :return:
        """
        if not re.fullmatch(r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}", ClientDate):
            raise serializers.ValidationError('Incorrect time format passed')
