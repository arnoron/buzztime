"""
Test for Ping Api and Health check Api
"""
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.constants import URL
from cheerios.analytics.test_data import response_status
from cheerios.feeds.test.test_data import create_log_file, remove_test_log_file


class TestPingCheck(APITestCase):
    """
    Testcase for get operation. Hit ping api and check if sie is up
    """

    def setUp(self):
        """
        Pre-setup of the ping Testcase.
        """
        self.url_ping = URL['ping']
        create_log_file()

    def test_ping_get(self):
        """
        Test if Ping Api works
        """
        response = self.client.get(self.url_ping)
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    def tearDown(self):
        remove_test_log_file()
