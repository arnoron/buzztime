"""
Test cases for Util function
"""
import datetime

from rest_framework.test import APITestCase

from cheerios.analytics.constants import DEFAULTEVENTCOLLECTION
from cheerios.analytics.test_data import get_valid_single_event_data, \
    get_valid_multiple_event_data, get_key_value_pair, get_key_value_pair_string, \
    get_key_value_incorrect_format, get_event_without_key_value, \
    get_valid_single_event_object, get_event_for_key_value, \
    get_multiple_event_mix_category, get_multiple_event_category, \
    get_single_event_category_data, get_event_name, get_category_name, \
    get_event_category_extra_data, \
    get_db_collection_mapped_data, get_db_mapped_data, \
    get_collection_mapped_data, get_without_name_mapped_data
from cheerios.analytics.utils.db_connection import MongoConnection
from cheerios.analytics.utils.utils import UtilFunction
from cheerios.feeds.test.test_data import create_log_file, remove_test_log_file
from cheerios.utils import CheeriosUtils

MONGO_CONNECTION = MongoConnection()
UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


class TestUtils(APITestCase):
    """
    Test cases for Util function
    """
    def setUp(self):
        create_log_file()

    def test_convert_str_to_datetime(self):
        """
        On Passing valid Date ,
        On Passing null Date ,
        On Passing date time in any format other then ISO
        """
        date_time = "2012-08-24T14:33:00"
        new_date_time = CHEERIOS_UTILS.convert_str_to_datetime(date_time)
        self.assertEqual(str(new_date_time), '2012-08-24 14:33:00')

        date_time = ""
        new_date_time = CHEERIOS_UTILS.convert_str_to_datetime(date_time)
        self.assertEqual(new_date_time, None)

        date_time = "2012-08-24 14:33:00"
        try:
            new_date_time = CHEERIOS_UTILS.convert_str_to_datetime(date_time)
        except ValueError as inst:
            self.assertEqual(inst.args[0],
                             "Incorrect date format, should be %Y-%m-%dT%H:%M:%S")

    def test_map_data_single_event(self):
        """
        On Passing single event data which is store in list
        """
        data = get_valid_single_event_data()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'event2')
        self.assertEqual(mapped_data[0]['es'], 'tablet2')
        self.assertEqual(str(mapped_data[0]['edt']), '2012-08-24 14:33:00')

    def test_map_data_without_keyvalue(self):
        """
        On Passing single event data which is store in list without key value pairs
        """
        data = get_event_without_key_value()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'event2')
        self.assertEqual(mapped_data[0]['es'], 'tablet2')
        self.assertEqual(str(mapped_data[0]['edt']), '2012-08-24 14:33:00')

    def test_map_data_single_object(self):
        """
        On Passing single event data which is store in object
        """
        data = get_valid_single_event_object()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'event2')
        self.assertEqual(mapped_data[0]['es'], 'tablet2')
        self.assertEqual(str(mapped_data[0]['edt']), '2012-08-24 14:33:00')

    def test_map_data_multiple_event(self):
        """
        On Passing multiple event data which is store in list
        """
        data = get_valid_multiple_event_data()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'event2')
        self.assertEqual(mapped_data[0]['es'], 'tablet2')
        self.assertEqual(str(mapped_data[0]['edt']), '2012-08-24 14:33:00')
        self.assertEqual(mapped_data[1]['en'], 'event3')
        self.assertEqual(mapped_data[1]['es'], 'tablet3')
        self.assertEqual(str(mapped_data[1]['edt']), '2012-08-24 14:33:00')

    def test_create_key_value_pairs(self):
        """
        On key value pair is store in dictionary
        """
        key_value_pair = get_key_value_pair()
        new_data = get_event_for_key_value()
        new_data = UTIL_FUNCTION.create_key_value_pairs(key_value_pair, new_data)
        self.assertEqual(new_data['en'], 'event2')
        self.assertEqual(new_data['es'], 'tablet2')
        self.assertEqual(new_data['edt'], '2012-08-24T14:33:00')
        self.assertEqual(new_data['ac'], 'Appearance of view')
        self.assertEqual(new_data['sid'], '37216')
        self.assertEqual(new_data['sc'], 'Premium Arcade')
        self.assertEqual(new_data['sn'], 'R52G417WGXH')
        self.assertEqual(new_data['s'], 'deb559ea-6014-46ed-b49f-126c874bd30a')
        self.assertEqual(new_data['et'], 'Navigation')
        self.assertEqual(new_data['md'],
                         'gamelobby.activities.GameBrowseActivity.appPanel_onStart')

    def test_create_key_value_string(self):
        """
        On key value pair which
        store in string
        """
        key_value_pair = get_key_value_pair_string()
        new_data = get_event_for_key_value()
        new_data = UTIL_FUNCTION.create_key_value_pairs(key_value_pair, new_data)
        self.assertEqual(new_data['en'], 'event2')
        self.assertEqual(new_data['es'], 'tablet2')
        self.assertEqual(new_data['edt'], '2012-08-24T14:33:00')
        self.assertEqual(new_data['ac'], 'Appearance of view')
        self.assertEqual(new_data['sid'], '37216')
        self.assertEqual(new_data['sc'], 'Premium Arcade')
        self.assertEqual(new_data['sn'], 'R52G417WGXH')
        self.assertEqual(new_data['s'], 'deb559ea-6014-46ed-b49f-126c874bd30a')
        self.assertEqual(new_data['et'], 'Navigation')
        self.assertEqual(new_data['md'],
                         'gamelobby.activities.GameBrowseActivity.appPanel_onStart')

    def test_key_value_wrong_format(self):
        """
        On key value pair is store in
        string but json format is incorrect
        """
        key_value_pair = get_key_value_incorrect_format()
        new_data = get_event_for_key_value()
        try:
            new_data = UTIL_FUNCTION.create_key_value_pairs(key_value_pair, new_data)
        except ValueError as inst:
            self.assertEqual(inst.args[0],
                             "Incorrect keyValuePairs  format, should be {\"key\":\"value\"}")
        self.assertEqual(new_data['en'], 'event2')
        self.assertEqual(new_data['es'], 'tablet2')
        self.assertEqual(new_data['edt'], '2012-08-24T14:33:00')
        self.assertEqual(hasattr(new_data, 'ac'), False)
        self.assertEqual(hasattr(new_data, 'sid'), False)
        self.assertEqual(hasattr(new_data, 'sc'), False)
        self.assertEqual(hasattr(new_data, 'sn'), False)
        self.assertEqual(hasattr(new_data, 's'), False)
        self.assertEqual(hasattr(new_data, 'et'), False)
        self.assertEqual(hasattr(new_data, 'md'), False)

    def test_key_value_pairs_empty(self):
        """
        On key value pair is none
        """
        key_value_pair = None
        new_data = get_event_for_key_value()
        new_data = UTIL_FUNCTION.create_key_value_pairs(key_value_pair, new_data)
        self.assertEqual(new_data['en'], 'event2')
        self.assertEqual(new_data['es'], 'tablet2')
        self.assertEqual(new_data['edt'], '2012-08-24T14:33:00')
        self.assertEqual(hasattr(new_data, 'ac'), False)
        self.assertEqual(hasattr(new_data, 'sid'), False)
        self.assertEqual(hasattr(new_data, 'sc'), False)
        self.assertEqual(hasattr(new_data, 'sn'), False)
        self.assertEqual(hasattr(new_data, 's'), False)
        self.assertEqual(hasattr(new_data, 'et'), False)
        self.assertEqual(hasattr(new_data, 'md'), False)

    def test_convert_datetime_to_str(self):
        """
        Test method which converts datetime object to string
        On Passing valid Date ,
        On Passing null Date ,
        On Passing date time in any format other then datetime object
        """
        date_time = datetime.datetime(2016, 6, 25, 20, 23, 11, 158601)
        converted_datetime = CHEERIOS_UTILS.convert_datetime_to_str(date_time)
        self.assertEqual(converted_datetime, "2016-06-25T20:23:11")

        date_time_empty = ""
        converted_datetime = CHEERIOS_UTILS.convert_datetime_to_str(date_time_empty)
        self.assertEqual(converted_datetime, None)

        date_time_str = "2012-08-24 14:33:00"
        try:
            CHEERIOS_UTILS.convert_datetime_to_str(date_time_str)
        except AttributeError as inst:
            self.assertEqual(inst.args[0],
                             "Incorrect date format, should be datetime object")

    def test_map_data_with_category(self):
        """
        On Passing single event data which is store in object
        We also pass the category name in name attribute. pattern should be category1.event1
        """
        data = get_single_event_category_data()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'category1.event2')
        self.assertEqual(mapped_data[0]['es'], 'tablet2')
        self.assertEqual(str(mapped_data[0]['edt']), '2012-08-24 14:33:00')

    def test_mapdata_multiple_category(self):
        """
        On Passing multiple event data which is store in list
        We also pass the category name in name attribute. pattern should be category1.event1
        all object has category name.
        """
        data = get_multiple_event_category()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'category1.event2')
        self.assertEqual(mapped_data[0]['es'], 'tablet2')
        self.assertEqual(str(mapped_data[0]['edt']), '2012-08-24 14:33:00')
        self.assertEqual(mapped_data[1]['en'], 'category2.event3')
        self.assertEqual(mapped_data[1]['es'], 'tablet3')
        self.assertEqual(str(mapped_data[1]['edt']), '2012-08-24 14:33:00')

    def test_map_data_multiple_mix(self):
        """
        On Passing multiple event data which is store in list
        We also pass the category name in name attribute. pattern should be category1.event1
        Not each event object have category value.
        Some object contains category name and some object do't have category name.
        """
        data = get_multiple_event_mix_category()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'category1.event2')
        self.assertEqual(mapped_data[0]['es'], 'tablet2')
        self.assertEqual(str(mapped_data[0]['edt']), '2012-08-24 14:33:00')
        self.assertEqual(mapped_data[1]['en'], 'event3')
        self.assertEqual(mapped_data[1]['es'], 'tablet3')
        self.assertEqual(str(mapped_data[1]['edt']), '2012-08-24 14:33:00')

    def test_event_name(self):
        """
        On Passing single Event data which stores the event name only.
        """
        data = get_event_name()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], '.event1')

    def test_category_name(self):
        """
        On Passing single Event data which stores the category name only.
        """
        data = get_category_name()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertEqual(mapped_data[0]['en'], 'category1.')

    def test_extra_data_passed(self):
        """
        Pass 'es' and 'en' in data to post which should not be added to db.
        """
        data = get_event_category_extra_data()
        mapped_data = UTIL_FUNCTION.map_data(data)
        self.assertNotEqual(mapped_data[0]['en'], 'name')
        self.assertNotEqual(mapped_data[0]['es'], 'source test name2')

    def test_get_db_connection_name(self):
        """
        Validate if connection details returned matches with data passed
        Data passed contains name=temp.test
        Returned details should have db_name as 'temp' and
        collection_name as 'test'
        """
        data = get_db_collection_mapped_data()
        connection_details = MONGO_CONNECTION.get_db_connection_name(data)
        self.assertEqual(connection_details['db_name'], 'temp')
        self.assertEqual(connection_details['collection_name'], 'test')

    def test_db_without_collection_name(self):
        """
        Validate if connection details returned matches with data passed
        Data passed contains name=temp (only category name is passed)
        Returned details should have db_name as 'temp' and
        collection_name as 'Event'(default value)
        """
        data = get_db_mapped_data()
        connection_details = MONGO_CONNECTION.get_db_connection_name(data)
        self.assertEqual(connection_details['db_name'], 'default')
        self.assertEqual(connection_details['collection_name'], 'temp')

    def test_db_without_dbname(self):
        """
        Validate if connection details returned matches with data passed
        Data passed contains name=.test (only collection name is passed)
        Returned details should have db_name as 'default' (default alias name) and
        collection_name as 'test'
        """
        data = get_collection_mapped_data()
        connection_details = MONGO_CONNECTION.get_db_connection_name(data)
        self.assertEqual(connection_details['db_name'], 'default')
        self.assertEqual(connection_details['collection_name'], 'test')

    def test_db_without_name(self):
        """
        Validate if connection details returned matches with data passed
        Data passed doecn't contains name
        Returned details should have db_name as 'default' (default alias name) and
        collection_name as 'Event'(default value)
        """
        data = get_without_name_mapped_data()
        connection_details = MONGO_CONNECTION.get_db_connection_name(data)
        self.assertEqual(connection_details['db_name'], 'default')
        self.assertEqual(connection_details['collection_name'], DEFAULTEVENTCOLLECTION)

    def tearDown(self):
        remove_test_log_file()
