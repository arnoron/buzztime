"""
Model Operations like add update or delete data on database
"""

from django.http.response import HttpResponse
from mongoengine.context_managers import switch_collection, switch_db
from rest_framework.exceptions import APIException

from cheerios.analytics.mongo_models import Event
from cheerios.analytics.utils.db_connection import MongoConnection
from cheerios.settings import MONGOHOST, MONGOPORT
from pymongo.mongo_client import MongoClient
from pymongo.read_preferences import Primary
from cheerios.services.log_classifier import Logger

MONGO_CONNECTION = MongoConnection()


class EventOperation(object):# pragma: no cover
    """
    Event Model Operations
    Save_data :- this method save data into database.
    """
    @staticmethod
    def save_data(new_data_list):
        """
        Event Model save data
        Taking each object data from list
        Create Event object
        Taking each key from object and save it into event object.
        Save event object into database.
        """
        try:
            MONGO_CONNECTION.register_db(new_data_list)
            for new_data in new_data_list:
                db_connection_details = MONGO_CONNECTION.get_db_connection_name(new_data)
                with switch_db(Event, db_connection_details['db_name']) as db_name:
                    with switch_collection(Event, db_connection_details['collection_name'])\
                     as event_name:
                        event_obj = Event()
                        for key, value in new_data.items():
                            event_obj[key] = value
                        event_obj.save(write_concern={"w": 1})
                        message = "Analytics Data posted with id: {0}".format(event_obj.id)
                        Logger.log.info(message)
            return HttpResponse(status=200)
        except APIException as exception:
            message = "Exception Raised: {0}".format(exception)
            Logger.log.error(message)
            return HttpResponse(status=400)

    @staticmethod
    def check_mongo_connection():
        """
        This function will check for mongodb connection.
        If connection is established it will return 200
        Else it will raise 500 error automatically.
        """
        expected_details = MongoClient(host=[MONGOHOST + ":" + str(MONGOPORT)],\
            document_class=dict, tz_aware=False, connect=True, read_preference=Primary())
        expected_details.database_names()
        return 200
