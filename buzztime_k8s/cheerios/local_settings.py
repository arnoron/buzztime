from .settings import *
import redis
REDISHOST = "127.0.0.1"
REDISPORT = "6379"

REDISDB = redis.StrictRedis(host=REDISHOST, port=REDISPORT, db=0,\
          decode_responses=False, socket_connect_timeout=2, socket_timeout=2)