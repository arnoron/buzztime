"""
interface for third party Caching service.
"""
from rest_framework.views import APIView


class CacheDataView(APIView):
    """
    Class for caching.
    """
    # def __init__(self):
    #     self.create_cache_connection()

    def create_cache_connection(self):
        """
        This function will be over ridden in view to create cache connection
        :return:
        """
        pass

    def cache_authentication(self, secret, email):
        """
        This function will be over ridden to
        check for authentication
        """
        pass

    def get_cache_data(self, url, node):
        """
        This function will be overridden to
        get dat from the cache used
        """
        pass

    def remove_cache_data(self, url, node):
        """
        This is over ridden to remove dat from
        cache
        """
        pass

    def update_cache_data(self, url, key, data):
        """
        Override to update data in cache
        """
        pass
