"""
Singleton class to return the instance of an objects
"""


class Singleton(type):
    """
    Singleton class
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Overriding __call__ method to create an instance if it's not available
        and if it's already available return the instance.
        :param args:
        :param kwargs:
        :return:
        """
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]
