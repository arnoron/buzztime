"""
interface for third party API.
"""
from rest_framework.views import APIView
from rest_framework.response import Response
from cheerios.services.log_classifier import Logger


class CachedApiView(APIView):
    """
    Third party API class which call third party API.
    Identify cache time for that API.
    Set Response with appropriate cache time.
    """
    def get(self, request, *args):
        """
        Get method for third party API.
        """
        try:
            request_object = self.get_data_from_request(request)
            data = self.get_from_downstream(request_object, *args)
            ttl = self.get_ttl(data, request_object)
            response = data
            response['Cache-Control'] = "max-age={0}, public ".format(ttl)
            response['Vary'] = "X-Api-CompanyCode"
            return response
        except ValueError as inst:
            result = "Error while hitting url : {0}".format(inst)
            message = "Error status code 400, {0}".format(result)
            Logger.log.error(message)
            return Response(result, status=400)
        except Exception as inst:
            error = str(inst)
            if "HTTPConnectionPool" in error:
                error = "Read timed out."
            error = "Error while hitting url : {0}".format(error)
            message = "Error status code 500, {0}".format(error)
            Logger.log.error(message)
            return Response(error, status=500)

    def get_data_from_request(self, request):
        """
        Request object passed to function
        As per requirement data will be returned to interface
        :param request:
        :return:
        """
        pass

    def get_from_downstream(self, request_url, *args):
        """
        This interface get response from third party API.
        """
        pass

    def get_ttl(self, data, url=None):
        """
        Identify cache time from API and return it.
        """
        pass
