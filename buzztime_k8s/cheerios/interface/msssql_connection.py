"""
Interface which allow user to called store procedures.
This interface contains mssql database related function.
"""
import pymssql
from contextlib import contextmanager
from rest_framework.views import APIView
from cheerios.utils import CheeriosUtils
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.settings import MYPOOL, MSSQLUSER, MSSQLHOST, MSSQLPASSWORD
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()

class MSSQLApiView(APIView):
    """
    Interface for players revamp.
    This class with include basic functionalities
    like: open connect, close connection
    """
    def __init__(self):
        """
        Define interface variable.
        """
        super().__init__()
        # We created conn and cursor_list as a object.
        # Because we might need to use multiple databse in one transaction.
        self.conn = {}
        self.cursor = None
        self.cursor_list = {}
        # Need to change value of conn_flag to 1
        # If API is using common cursor but multiple mssql class.
        self.conn_flag = 0
        self.transaction = False


    def get_cursor(self, database, transaction=False, store_procedure=None):
        """
        Function which creates connection with db using pymssql
        If transaction value is set to true then Db transaction will start
        We need to commit/rollback if we are using transaction
        :param self:, db:
        :return: cursor object
        """
        if not CHERRIOS_UTILS.attr_exist(self, 'conn'):
            self.__init__()
        # Check if connection of database is exist.
        if CHERRIOS_UTILS.attr_exist(self, 'conn') and \
            CHERRIOS_UTILS.attr_has_value(self.conn, database):
            # If connection has cursor then we closed that connection and create new.
            # Otherwise return existing cursor.
            if CHERRIOS_UTILS.attr_exist(self, 'cursor_list') and \
                CHERRIOS_UTILS.attr_exist(self.cursor_list, database):
                return self.cursor_list[database]
            else:
                self.close_single_cursor(self.conn[database])

        self.database = database
        if store_procedure == STORED_PROCEDURE_MAPPER['achievement_leaderboard']:
            conn = pymssql.connect(user=MSSQLUSER, server=MSSQLHOST,
                                   database=self.database, password=MSSQLPASSWORD,
                                   timeout=20)
        else:
            conn = MYPOOL[database].connect()

        cursor = conn.cursor()
        self.conn[database] = conn
        if transaction:
            self.transaction = True
            self.conn[database].autocommit(False)
        self.cursor_list[database] = cursor
        return cursor

    def close_single_cursor(self, conn, transaction=False):
        """
        Closing connection
        """
        if self.operation_required('commit', conn):
            conn.commit()
        if self.operation_required('autocommit', conn) and \
                CHERRIOS_UTILS.attr_has_value(self, "transaction") and transaction:
            conn.autocommit(True)
        conn.close()

    def close_cursor(self):
        """
        This function will close passed conn object connection
        :param conn_obj:
        :return:
        """
        if not CHERRIOS_UTILS.attr_exist(self, 'conn'):
            return
        for database in self.conn:
            self.close_single_cursor(self.conn[database], True)
        self.conn_flag = 0

    def call_storeprocedure(self, store_procedure, cursor, parameters, required_parameters=True):
        """
        # This function calls with database name, store procedure name
        And parameter values of store procedures.
        # Calls store procedures with parameters.
        # Return store procedure response.
        """
        message = "Store procedure :- {0} called with {1}".\
            format(store_procedure, parameters)
        Logger.log.info(message)
        self.conn_flag = 1
        if required_parameters is True:
            utils_obj = CheeriosUtils
            if isinstance(parameters, dict):
                # create parameter and remove stray quotation marks, before the actual call
                parameters = utils_obj.create_sp_parameters(parameters)
            elif isinstance(parameters, str):
                # clean the parameters to escape the stray quotation marks, before the actual call
                parameters = utils_obj.escape_quotes_sp_params(parameters)
            result = cursor.execute("exec {0} {1}".format(store_procedure, parameters))
        else:
            result = cursor.callproc(store_procedure, parameters)
        self.cursor = cursor
        return result

    def fetchall(self):
        """
        Function fetch all data from cursor and return as response.
        """
        next_value = self.cursor.nextset()
        if CHERRIOS_UTILS.is_not_empty(next_value):
            result = self.cursor.fetchall()
        else:
            result = None
        # message = str(result)
        #         message = "Store procedure :- {0} response :-  {1}".\
        #             format(store_procedure, message)
        #         Logger.log.info(message)
        return result

    @staticmethod
    def fetchall_from_cursor(cursor):
        """
        Fetch response from cursor
        """
        result = []
        headers = cursor.description
        if not headers:
            return result
        header_list = [index[0] for index in headers]
        for row in cursor:
            row_dict = dict(zip(header_list, row))
            result.append(row_dict)
        return result

    def operation_required(self, operation, conn):
        """
        Checking if operation like commit and rollback is required or not.
        Commit/Rollback only happens once and if transaction is on.
        """
        check_conn = conn and CHERRIOS_UTILS.attr_exist(conn, operation)
        check_tran = CHERRIOS_UTILS.attr_has_value(self, "transaction")
        return check_conn and self.conn_flag == 1 and  check_tran

    def tras_operation(self, operation):
        """
        Commit / rollback transaction when required.
        """
        for database in self.conn:
            conn = self.conn[database]
            if self.operation_required(operation, conn):
                getattr(conn, operation)()
        self.conn_flag = 0

    def connection_commit(self):
        """
        Commit connection to save data in MSSQL.
        If there is transaction then change back autocommit value to True.
        """
        self.tras_operation('commit')

    def connection_rollback(self):
        """
        Rollback to previous state connection to save data in MSSQL.
        If there is transaction then change back autocommit value to True.
        """
        self.tras_operation('rollback')

    @contextmanager
    def connection_manager(self, store_procedure, parameters, database, transaction_flag=False,
                           *, cursor=None):
        """
        This method is to handle all connection issues in a store procedure.
        Things like creating a connection and removing one.
        """
        if not cursor:
            cursor = self.get_cursor(database, transaction_flag)
        self.call_storeprocedure(store_procedure, cursor, parameters)
        yield cursor
        if not transaction_flag:
            self.close_cursor()
