"""
Interface to help GET and POST API.
"""
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.utils import CheeriosUtils
from rest_framework.response import Response

CHEERIOS_UTILS = CheeriosUtils()


class ApiViewInterface(MSSQLApiView):
    """
    Interface to help GET and POST API.
    """

    def get(self, request):
        """
        Get API helper.
        """
        data = request.GET
        self.validate_serializer(data)
        response, check_response = self.get_from_downstream(data)
        if type(response) is dict and response.get('error'):
            return Response(response.get('content')[0], status=response.get('content')[1])
        result = self.check_empty_response(response, check_response)
        return Response(result, status=200)

    def post(self, request):
        """
        Post API helper.
        """
        data = request.data
        self.validate_serializer(data)
        response = self.get_from_downstream(data)
        if type(response) is dict and response.get('error'):
            return Response(response.get('content')[0], status=response.get('content')[1])
        return Response(response, status=200)

    def validate_serializer(self, data):
        """
        Validate serializer.
        """
        serializer = self.get_serializer()
        if CHEERIOS_UTILS.is_not_empty(serializer):
            serializer_validate = serializer(data=data)
            CHEERIOS_UTILS.validate_serializer(serializer_validate)

    def check_empty_response(self, response, check_reponse=False):
        """
        Throw error when response is empty.
        """
        if check_reponse and not CHEERIOS_UTILS.is_not_empty(response):
            CHEERIOS_UTILS.log_error(400, "Empty response.")
            raise ValueError("Invalid Parameter passed")
        return self.format_response(response)

    def get_serializer(self):
        """
        Fetch serializer class
        """
        pass

    def get_from_downstream(self, data):
        """
        Perform API operation and return response of operation.
        This function should return response and Boolean(Default False)
        Boolean should be True if we need to throw an error when response is empty
        """
        pass

    def format_response(self, data):
        """
        Formating response.
        Convert bytes data to base64.
        """
        return data
