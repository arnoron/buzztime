"""
Defining all common constants
"""
SUCESS_STATUS = 200
NOT_MODIFY_STATUS = 304

add_delivery_url = 'cheerios.services.Email.email.Email.add_deliveries'

WRONG_CREDENTIALS_TITLES =['Wrong password', 'Wrong email id', 'Invalid credentials']

REGISTERED_CLAIMS_VALID_VALS = dict(iss="android",
                                    aud="cheerios"
                                   )

TRUE_VALUES = {'t', 'T', 'true', 'True', 'TRUE', '1', 1, True}

DEFAULT_SENDER_EMAIL = "PlayTrivia@buzztime.com"

WELCOME_EMAIL_TEMPLATE_ID = "8a3c935a-3886-4b78-be11-be9f9d9bbd7b"

NCR_ALLOWED_STATUS = [200, 304]

ONE_WEEK = 7*24*60*60

PLAYMAKER_AUTH_APIS = ['/analytics/rmq_creds/', '/players/mobile_login/']

TRIVIAWEB_APIS = ['/triviaweb/forgot-password/', '/triviaweb/change_password/']
