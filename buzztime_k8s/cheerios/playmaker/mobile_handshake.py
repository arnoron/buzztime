from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.decorators import close_connection
from cheerios.playmaker.serializers import MobileHandshakeSerializer
from cheerios.services.is_authenticated import is_authenticated
from cheerios.services.log_classifier import Logger
from cheerios.settings import MSSQLDBPROG


class MobileHandshakeView(MSSQLApiView):
    """
    API view for mobile Handshake
    """

    @is_authenticated()
    @close_connection
    def post(self, request):
        """
        Takes the request body and validates it using the serializer
        if the serializer is valid:
        :return:
        """
        request_body = request.data
        mobile_handshake_serializer = MobileHandshakeSerializer(data=request_body)

        if not mobile_handshake_serializer.is_valid():
            Logger.log.error(mobile_handshake_serializer.errors)
            return Response(mobile_handshake_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        cursor = self.get_cursor(MSSQLDBPROG)
        store_procedure = "dbo.usp_sav_MobileDeviceFromHandshake"

        parameters = dict(DeviceIdentifier=request_body.get('device_id'),
                          MobileAppName=request_body.get('mobile_app_name'),
                          MobileAppVersion=request_body.get('mobile_app_version'),
                          BrandName=request_body.get('brand_name'),
                          DeviceModel=request_body.get('device_model'),
                          TimeZone=request_body.get('timezone'),
                          OperatingSystem=request_body.get('operating_system'),
                          OperatingSystemVersion=request_body.get('operating_system_version'),
                          HasNotch=request_body.get('has_notch'),
                          IsTablet=request_body.get('is_tablet'),
                          ClientDate=request_body.get('client_date'),
                          PlayerID=request_body.get('player_id'))

        self.call_storeprocedure(store_procedure, cursor, parameters)
        self.fetchall_from_cursor(cursor)

        return Response("success", status=status.HTTP_200_OK)
