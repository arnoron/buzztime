from rest_framework import serializers


class MobileHandshakeSerializer(serializers.Serializer):
    """
    Serializer to validate requests for Mobile Handshake API calls
    """
    device_id = serializers.CharField(required=True),
    mobile_app_name = serializers.CharField(required=True)
    mobile_app_version = serializers.CharField(required=True)
    brand_name = serializers.CharField(required=True)
    device_model = serializers.CharField(required=True)
    timezone = serializers.CharField(required=True)
    operating_system = serializers.CharField(required=True)
    operating_system_version = serializers.CharField(required=True)
    has_notch = serializers.BooleanField(required=True)
    is_tablet = serializers.BooleanField(required=True)
    client_date = serializers.DateTimeField(required=True)
    player_id = serializers.IntegerField(required=True)
