"""
Serializer classes
"""
from rest_framework import serializers


class NcrSerializer(serializers.Serializer):
    """
    Serializer for NCR API
    Check all mandatory fields and there type.
    """
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    site_id = serializers.IntegerField(allow_null=False, required=True)
    menu_id = serializers.IntegerField(allow_null=False, required=True)
    promise_time = serializers.DateField(allow_null=True, required=False)
    company_code = serializers.CharField(allow_null=False, required=True)
    authorization = serializers.CharField(allow_null=False, required=True)
