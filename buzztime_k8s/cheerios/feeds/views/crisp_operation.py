"""
Contains Crisp operartions class
which includes Functions which support crisp API
"""
from pymssql import OperationalError

from sqlalchemy import exc
from rest_framework.response import Response

from cheerios.feeds.serializers import CrispSerializerforInternalModel
from cheerios.settings import MSSQLDBGAMEPLAY, CRISPAPIKEY
from cheerios.players.utils.utils  import UtilFunction
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.services.log_classifier import Logger

UTIL_FUNCTION = UtilFunction()

SWITCHER = {
    "ok": 1,
    "true": 1,
    "approved": 1
}


class CrispViewSet(MSSQLApiView): # pragma: no cover
    """
    API To post data in mssql
    # It will create connection with MS SQL server
    # using mypool object which is sqlalchemy object used for
    # connection pooling
    # It will take two parameter GUID and status both are mandatory
    # It will call store procedure to store both value in MSSQL.
    # It will return response of Store procedure.
    """

    serializer_class = CrispSerializerforInternalModel

    def post(self, request):
        """
        Get API to connect mssql.
        Take two parameter GUID and Status.
        It will called to store procedure and save data into MSSQL.
        """
        message = "Crisp API called"
        Logger.log.info(message)
        response = self.post_crisp_data(request)
        return response

    def post_crisp_data(self, request):
        """
        it is a webhook call back from Crisp
        when the image's status is changed.
        It connects to MS SQL.
        Take parameter value from request.
        Validate ContentId and Action values.
        Next validate ApiKey from request with Environment variable.
        If keys don't match return Error message.
        Once Validated
        call store procedure with parameter and update the state in MS SQL.
        """
        try:
            content_id = self.get_request_parameter(request, "ContentId")
            action = self.get_request_parameter(request, "Action")
            api_key = self.get_request_parameter(request, "ApiKey")

            message = "Request parameter Value :- ContentId = {0} and Action = {1} " \
                .format(content_id, action)
            Logger.log.info(message)
            required_value = [content_id, action, api_key]
            UTIL_FUNCTION.required_parameters(required_value)
            if api_key != CRISPAPIKEY:
                message = "ApiKey mismatch"
                Logger.log.error(message)
                return Response(message, status=400)
            action = self.change_status_value(action)
            data = [content_id, action]
            store_procedure = 'dbo.usp_upd_LetsGoImageStatus'
            cursor = self.get_cursor(MSSQLDBGAMEPLAY)
            result = self.call_storeprocedure(store_procedure, cursor, data)
            return Response(result, status=200)

        except ValueError as inst:
            return UTIL_FUNCTION.value_error(inst)
        except OperationalError as inst:
            return UTIL_FUNCTION.operation_error(inst)
        except exc.DBAPIError as excep:
            return UTIL_FUNCTION.dbapi_error(excep)
        except Exception as inst:
            return UTIL_FUNCTION.exception(inst)
        finally:
            self.close_cursor()

    @staticmethod
    def get_request_parameter(request, attribute):
        """
        Get value of parameter from request.
        """
        return request.data.get(attribute)

    @staticmethod
    def change_status_value(action):
        """
        Action is must in accepted status list.
        """
        value = SWITCHER.get(action.lower())
        if value is None:
            value = 0
        return value
