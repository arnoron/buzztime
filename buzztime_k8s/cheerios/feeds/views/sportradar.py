"""
This file contain class and function to call Sportradar API.
"""
import math
import re
from datetime import datetime
import requests

from rest_framework.response import Response
from cheerios.interface.baseclass import CachedApiView
from cheerios.settings import SPORTRADAR_API_KEY
from cheerios.feeds.utils.utils import UtilFunction
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

MAX_CACHE_INTERVAL = 60 * 60 * 24

API_TIMEOUT = 10
TTL = {
    re.compile("^.*/boxscore.*$"): 60 * 5,
    re.compile("^.*/changes.*$"): 60 * 60 * 24,
    re.compile("^.*/schedule.*$"): 60 * 5,
    re.compile("^.*/standings.*$"): 30,
    re.compile("^.*/pbp.*$"): 30,
    re.compile("^.*/statistics.*$"): 60 * 60 * 24
    }

ACCESS_LEVEL_REGEX = dict(trial=re.compile("^nfl-ot\\d"),
                          prod=re.compile("^nfl-o\\d"))

ACCESS_LEVEL = dict(trial='-ot\\d', prod='-o\\d')
SPORTRADAR_URL = 'http://api.sportradar.us/'
PROD_ENV_VALUE = '-o'
UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


class SportradarViewSet(CachedApiView): # pragma: no cover
    """
    API For sportradar API
    1. GET Schedules from sportradar.
        It call Sportradr API and get data from Sportradr API.
        Set TTL for that api and return Response object
        with data and cache expiry to nginx
    """

    def get_data_from_request(self, request):
        """
        Overriding Interface CachedApiView function.
        This function accepts request as input
        It will process request object and return data as per our requirement.
        :param request:
        :return:
        """
        if 'url' not in request.GET:
            raise ValueError("Query parameter is missing")
        url = request.GET['url']
        if not url.endswith(".json"):
            raise ValueError("Invalid URL passed.")
        return url

    def get_from_downstream(self, request_url, *args):
        """
        Set Sportradar url.
        Call Sportradar URL and return response of Sportradar
        """
        url = SPORTRADAR_URL + self.create_environment_specific_url(request_url)
        message = "URL created: {0}".format(url)
        Logger.log.info(message)
        response = self.get_data_from_sportradar_api(url)
        return response

    def get_ttl(self, data, request_object=None):
        """
        find cache time for ngnix.
        return cache time for api.
        """
        ttl = 60 * 5
        if data.status_code == 200:
            ttl = self.find_api_ttl(request_object, data.data)
        return ttl

    @staticmethod
    def get_data_from_sportradar_api(url):
        """
        Set url to call Sportradar API.
        Call Sportradar API.
        If status is 200 cache data and return data with 200 status.
        Else return Status with Error message.
        Api will timeout after specified seconds.
        If by that time data api got data will return data with 200 status
        Else an Error message with 500 status
        """
        payload = {'api_key': SPORTRADAR_API_KEY}
        sportradar_data = requests.get(url, params=payload, timeout=API_TIMEOUT)
        message = "URL called: {0}".format(sportradar_data.url)
        Logger.log.info(message)
        if sportradar_data.status_code == 200:
            result = UTIL_FUNCTION.get_json_data_from_response(sportradar_data)
            if result:
                return Response(result, status=200)
            message = "Data capture :- {0}".format(sportradar_data.text)
            Logger.log.info(message)
            message = "Invalid Response capture from URL :- {0}".format(url)
        elif sportradar_data.status_code == 403 and \
                sportradar_data.text == '<h1>Developer Inactive</h1>':
            message = "License Key Invalid"
        else:
            message = "Error while hitting url Error: Invalid URL :- {0} ".format(url)
        result = message
        message = "Error status code {0}, {1}".format(sportradar_data.status_code, message)
        Logger.log.error(message)
        return Response(result, status=sportradar_data.status_code)

    @classmethod
    def find_api_ttl(cls, sport_radar_api, result):
        """
        Check pattern of url and set expired time for nginx cache.
        return nginx cache with appropriate expired time.
        """
        expiry_time = MAX_CACHE_INTERVAL
        for key, ttl in TTL.items():
            if key.match(sport_radar_api):
                if 'boxscore' in sport_radar_api or 'pbp' in sport_radar_api:
                    expiry_time = cls.check_schedule(ttl, result)
                else:
                    expiry_time = ttl
                break
        return expiry_time

    @staticmethod
    def check_schedule(ttl, result):
        """
        Depending on game id passed. It will be cached for
        specified amount of time.
        """
        expiry_time = MAX_CACHE_INTERVAL
        status = result.get('status')
        if status == 'inprogress' or status == 'halftime' or \
                status == 'complete' or status == 'flex-schedule':
            expiry_time = ttl
        elif status == 'closed' or status == 'cancelled':
            expiry_time = MAX_CACHE_INTERVAL
        else:
            datetime_object = ((CHEERIOS_UTILS.convert_str_to_datetime(
                result.get('scheduled').split('+')[0])) - datetime.utcnow()).\
                total_seconds()
            ttl = abs(math.floor(datetime_object))
            if ttl < MAX_CACHE_INTERVAL:
                expiry_time = ttl
        return expiry_time

    @staticmethod
    def create_environment_specific_url(sport_radar_api):
        """
        Whatever value is passed from Tablet,
        We need to call production api.
        This function will create
        specific url
        :param sport_radar_api:
        :return:
        """
        if ACCESS_LEVEL_REGEX.get('trial').match(sport_radar_api):
            query = re.search(ACCESS_LEVEL.get('trial'), sport_radar_api)
        else:
            query = re.search(ACCESS_LEVEL.get('prod'), sport_radar_api)
        return sport_radar_api.replace(
            sport_radar_api[query.span()[0]: (query.span()[1] - 1)], PROD_ENV_VALUE, 1)
