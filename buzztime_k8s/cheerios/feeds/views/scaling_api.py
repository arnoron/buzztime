"""
This function contains view for hostname api.
This api is used to check different hostname
running while scaling WSGI container.
"""
import logging
import socket
import requests
from rest_framework.response import Response
from rest_framework.views import APIView

API_TIMEOUT = 1

HEALTHCHECKURL = {
    'players': 'wsgi-players:8002',
    'tablet': 'wsgi-tablet:8002',
    'triviaweb': 'wsgi-triviaweb:8002',
    'foodmenu': 'wsgi-foodmenu:8002'
}


def log_healthcheck_error(log_name, status_value, error):
    LOGGER = logging.getLogger(log_name)
    message = "Error status code {0}, Error : {1}". \
        format(status_value, error)
    LOGGER.error(message)


class HostName(APIView):
    """
    Get api created to check scale container host name
    This is a test api.
    It will just return status 200
    """

    @staticmethod
    def get(request):
        """
        Get API created for check scale container host name
        :param request:
        :return: 200 status
        """
        host = socket.gethostname()
        return Response(host, status=200)


class HealthCheckView(APIView):
    """
    This API will check healthcheck of Docker wsgi conatiners.
    We need to update list of HEALTHCHECKURL any modification happen.
    """

    def get(self, request):
        """
        GET API to healthcheck of Docker wsgi conatiners.
        """
        status_flag = False
        for log_file, url in HEALTHCHECKURL.items():
            try:
                api = "http://{0}/feeds/hostname/".format(url)
                response = requests.get(api, timeout=API_TIMEOUT)
                if response.status_code != 200:
                    message = "{0} Container is down".format(url)
                    log_healthcheck_error(log_file, 500, message)
                else:
                    status_flag = True
            except:
                message = "{0} Container is down".format(url)
                log_healthcheck_error(log_file, 500, message)
        if status_flag:
            return Response(status=200)
        return Response(status=500)

