"""
classes for print fields in DRF doc.
"""
from __future__ import unicode_literals
from django.db import models


class Crisp(models.Model):
    """
    Crisp class for print fields in DRF doc.
    """
    ContentId = models.TextField()
    Action = models.TextField()
    ApiKey = models.TextField()
