"""ucars URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from cheerios.feeds.views import scaling_api # , sportradar, crisp_operation,

urlpatterns = [
    # url(r'^sportradar/', sportradar.SportradarViewSet.as_view()),
    url(r'^hostname/$', scaling_api.HostName.as_view()),
    # url(r'^crisp/$', crisp_operation.CrispViewSet.as_view()),
    url(r'^healthcheck/$', scaling_api.HealthCheckView.as_view()),
]
