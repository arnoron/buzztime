"""
Functions which gives data to test cases.
Some function might help to execute some repeated code instead of provide data.
"""
import os
import datetime
from datetime import timedelta
from unittest.mock import Mock
from rest_framework import status
from requests.models import Response
from cheerios.analytics.utils.utils import UtilFunction
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


def create_log_file():
    """
    Create Test log file.
    If it is deleted by other testcase.
    """
    source = "out/buzztime_test_app.log"
    file = open(source, mode='w')
    file.close()


def remove_test_log_file():
    """
    Remove Test file after test case complete.
    """
    source = "out/buzztime_test_app.log*"
    if os.path.isfile(source):
        os.remove(source)


def response_status(response):
    """
    Common function which send response status code from response.
    """
    return response.status_code


def get_game_closed():
    """
    Function will return mock data for boxscore api with status closed
    :return:
    """
    return {
        "id": "c8ca977e-77ad-42fc-a80b-0d4a78e73b87",
        "status": "closed",
        "reference": "56516",
        "number": 14,
        "scheduled": "2015-09-14T00:31:03+00:00",
        "attendance": 93579,
        "utc_offset": -6,
        "entry_mode": "INGEST",
        "weather": "Temp:  F, Wind:   mph",
        "clock": "00:00",
        "quarter": 4
    }


def get_game_scheduled_next_month():
    """
    Function will return mock data for boxscore api with status scheduled
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "scheduled",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(26) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_scheduled_after_hour():
    """
    Function will return mock data for boxscore api with status scheduled
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "scheduled",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(1) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_in_progress():
    """
    Function will return mock data for boxscore api with status inprogress
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "inprogress",
        "reference": "56905",
        "number": 5,
        "scheduled": CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.utcnow()) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_halftime():
    """
    Function will return mock data for boxscore api with status halftime
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "halftime",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(1) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_complete():
    """
    Function will return mock data for boxscore api with status complete
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "halftime",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(-3) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_cancelled():
    """
    Function will return mock data for boxscore api with status cancelled
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "cancelled",
        "reference": "56905",
        "number": 5,
        "scheduled": CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.utcnow()) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_postponed_one_day():
    """
    Function will return mock data for boxscore api with status postponed
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "postponed",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(24) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_postponed_two_days():
    """
    Function will return mock data for boxscore api with status postponed
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "postponed",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(48) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_delayed_one_day():
    """
    Function will return mock data for boxscore api with status delayed
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "delayed",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(24) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_delayed_two_days():
    """
    Function will return mock data for boxscore api with status delayed
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "delayed",
        "reference": "56905",
        "number": 5,
        "scheduled": create_future_date(48) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_flex_schedule():
    """
    Function will return mock data for boxscore api with status flex-schedule
    Assumption: Game is currently scheduled to occur on a specific date and time,
    however it is more likely to be rescheduled, as time is not predefined,
    we will cache it for 5 mins for boxscore api
    and 30 seconds for play by play api
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "flex-schedule",
        "reference": "56905",
        "number": 5,
        "scheduled": CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.utcnow()) + "+00:00",
        "entry_mode": "INGEST"
    }


def get_game_time_tbd():
    """
    Function will return mock data for boxscore api with status delayed
    Assumption: Time-tbd - It will return scheduled time as <Date>T00:00:00+00:00
    :return:
    """
    return {
        "id": "28290722-4ceb-4a4c-a4e5-1f9bec7283b3",
        "status": "time-tbd",
        "reference": "56905",
        "number": 5,
        "scheduled": CHEERIOS_UTILS.convert_datetime_to_str(datetime.datetime.combine(
            datetime.datetime.utcnow().date(), datetime.time()) + timedelta(hours=48)) + "+00:00",
        "entry_mode": "INGEST"
    }


def create_future_date(hour):
    """
    Function will take hour parameter and
    add those hours to current datetime and
    return future date
    :param hour:
    :return:
    """
    future_date = datetime.datetime.utcnow() + timedelta(hours=hour)
    return CHEERIOS_UTILS.convert_datetime_to_str(future_date)


def get_play_by_play_closed():
    """
    Function will return mock date for play-by-play api with status closed
    :return:
    """
    return {
        "id": "c8ca977e-77ad-42fc-a80b-0d4a78e73b87",
        "status": "closed",
        "reference": "56516",
        "number": 14,
        "scheduled": "2015-09-14T00:31:03+00:00",
        "attendance": 93579,
        "utc_offset": -6,
        "entry_mode": "INGEST",
        "weather": "Temp:  F, Wind:   mph",
        "clock": "00:00",
        "quarter": 4
    }


def invalid_crisp_content_id():
    """
    Return invalid content_id to validate crisp API
    """
    return "82729195-82729195"


def valid_action():
    """
    Return Valid action of crisp
    """
    return "ok"


def valid_content_id():
    """
    Return valid content_id to validate crisp API
    """
    return "4CDAE812-DB34-4B47-B02B-953200965640"


def random_action():
    """
    Return random action to validate crisp API
    """
    return "string"


def valid_action_capital():
    """
    Return valid action in capital to validate crisp API
    """
    return "OK"


def get_response_valid_data():
    """
    Return Response object with valid data to validate crisp API
    """
    response = Response()
    response.json = Mock(return_value='{"test": "Test"}')
    response.status_code = status.HTTP_200_OK
    return response


def get_response_invalid_data():
    """
    Return Response object with invalid data to validate crisp API
    """
    response = Response()
    response._content = ""
    response.status_code = status.HTTP_200_OK
    return response
