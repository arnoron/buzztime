"""
Unit Test case for crisp API
"""
import json

from rest_framework.test import APITestCase

from cheerios.feeds.test.test_data import valid_content_id, \
    get_response_invalid_data, get_response_valid_data
from cheerios.feeds.utils.utils import UtilFunction
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils


class TestUtilsUnitTestcase(APITestCase):
    """
    Unit Test case for Utils  functions
    """

    def test_empty_parameter(self):
        """
        Passing empty data to is_not_empty function.
        """
        result = CHEERIOS_UTILS.is_not_empty(None)
        self.assertEqual(result, False)

    def test_not_empty_with_data(self):
        """
        Passing value to is_not_empty function.
        """
        guid = valid_content_id()
        result = CHEERIOS_UTILS.is_not_empty(guid)
        self.assertEqual(result, True)

    def test_invalid_response(self):
        """
        Passing empty data with 200 status as a response.
        """
        response_obj = get_response_invalid_data()
        result = UTIL_FUNCTION.get_json_data_from_response(response_obj)
        self.assertEqual(result, False)

    def test_valid_response(self):
        """
        Passing valid data with 200 status as a response.
        """
        response_obj = get_response_valid_data()
        result = UTIL_FUNCTION.get_json_data_from_response(response_obj)
        data = {}
        data['test'] = 'Test'
        json_data = json.dumps(data)
        self.assertEqual(result, json_data)
