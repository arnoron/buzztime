# """
# Integrated Test case for crisp API
# """
# from rest_framework import status
# from rest_framework.test import APITestCase
#
# from cheerios.feeds.test.test_data import valid_action,\
#     valid_content_id, invalid_crisp_content_id,\
#     response_status, valid_action_capital,\
#     random_action
# from cheerios.settings import CRISPAPIKEY
# from cheerios.feeds.views.crisp_operation import CrispViewSet
#
# CRISPVIEWSET = CrispViewSet()
#
#
# class TestCrispIntegratedTestcase(APITestCase):
#     """
#     Integrated Test case for crisp API functions
#     """
#
#     def test_empty_parameter(self):
#         """
#         Content id and action is empty.
#         """
#         data = {}
#         data["Action"] = None
#         data["ContentId"] = None
#         url = "/feeds/crisp/"
#         response = self.client.post(url, data)
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_empty_action(self):
#         """
#         Action is empty and valid Content id pass.
#         """
#         data = {}
#         data["Action"] = None
#         data["ContentId"] = valid_content_id()
#         url = "/feeds/crisp/"
#         response = self.client.post(url, data)
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_empty_content_id(self):
#         """
#         Action is correct but empty Content id
#         """
#         data = {}
#         data["Action"] = valid_action()
#         data["ContentId"] = None
#         url = "/feeds/crisp/"
#         response = self.client.post(url, data)
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_invalid_content_id(self):
#         """
#         Pass wrong content id
#         """
#         data = {}
#         data["Action"] = valid_action()
#         data["ContentId"] = invalid_crisp_content_id()
#         url = "/feeds/crisp/"
#         response = self.client.post(url, data)
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     # def test_valid_data(self):
#     #     """
#     #     Pass valid content id and Action
#     #     """
#     #     data = {}
#     #     data["Action"] = valid_action()
#     #     data["ContentId"] = valid_content_id()
#     #     data["ApiKey"] = CRISPAPIKEY
#     #     url = "/feeds/crisp/"
#     #     response = self.client.post(url, data)
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_invalid_apikey(self):
#         """
#         Pass valid content id and Action
#         """
#         data = {}
#         data["Action"] = valid_action()
#         data["ContentId"] = valid_content_id()
#         data["ApiKey"] = CRISPAPIKEY + "123e"
#         url = "/feeds/crisp/"
#         response = self.client.post(url, data)
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_empty_apikey(self):
#         """
#         Pass valid content id and Action
#         """
#         data = {}
#         data["Action"] = valid_action()
#         data["ContentId"] = valid_content_id()
#         url = "/feeds/crisp/"
#         response = self.client.post(url, data)
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#
# class TestCrispUnitCases(APITestCase):
#     """
#     Unit test case for Crisp operation.
#     """
#     def test_valid_action(self):
#         """
#         Action is correct.
#         change Action to integer.
#         """
#         status_value = valid_action()
#         result = CRISPVIEWSET.change_status_value(status_value)
#         self.assertEqual(result, 1)
#
#     def test_valid_action_capital(self):
#         """
#         Action is correct.
#         change Action to integer.
#         """
#         status_value = valid_action_capital()
#         result = CRISPVIEWSET.change_status_value(status_value)
#         self.assertEqual(result, 1)
#
#     def test_valid_action_random(self):
#         """
#         Action is correct.
#         change Action to integer.
#         """
#         status_value = random_action()
#         result = CRISPVIEWSET.change_status_value(status_value)
#         self.assertEqual(result, 0)
