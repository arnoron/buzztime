"""
Testcases for ttl is right according to game status.
"""

from rest_framework.test import APITestCase

from cheerios.feeds.test.test_data import get_game_closed, get_game_scheduled_next_month, \
    get_game_scheduled_after_hour, get_game_in_progress, get_game_halftime, \
    get_game_complete, get_game_cancelled, get_game_postponed_one_day, \
    get_game_postponed_two_days, get_game_flex_schedule, get_game_time_tbd, \
    get_play_by_play_closed, remove_test_log_file, create_log_file
from cheerios.feeds.views.sportradar import SportradarViewSet

SPORTRADAR_VIEW_SET = SportradarViewSet()
MAX_CACHE_INTERVAL = 60 * 60 * 24
TTL = {
    "boxscore" : 60 * 5,
    "play-by-play" : 30,
    }


class TestUtils(APITestCase):
    """
    Unit Test cases for game status ttl
    """
    def setUp(self):
        create_log_file()

    def test_game_closed(self):
        """
        Test case to validate expiry time is set to 24 hours
        if game id passed has status closed
        :return:
        """
        result = get_game_closed()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_game_scheduled_next_month(self):
        """
        Test case to validate expiry time is set to 24 hours
        if scheduled time - current time is greater than 24hrs
        and game id passed has status scheduled
        :return:
        """
        result = get_game_scheduled_next_month()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_game_scheduled_after_hour(self):
        """
        Test case to validate expiry time is set to
        scheduled time - current time if difference is less than 24hrs
        and game id passed has status scheduled
        :return:0
        """
        result = get_game_scheduled_after_hour()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(3599, expiry_time)

    def test_game_inprogress(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status inprogress
        :return:0
        """
        result = get_game_in_progress()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['boxscore'], expiry_time)

    def test_game_halftime(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status halftime
        :return:0
        """
        result = get_game_halftime()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['boxscore'], expiry_time)

    def test_game_complete(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status closed
        :return:0
        """
        result = get_game_complete()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['boxscore'], expiry_time)

    def test_game_cancelled(self):
        """
        Test case to validate expiry time is set to 24hrs
        for game id passed has status cancelled
        :return:0
        """
        result = get_game_cancelled()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_game_postponed_oneday(self):
        """
        Test case to validate expiry time is set to
        scheduled time - current time if difference is less than 24 hours
        for game id passed has status postponed
        :return:0
        """
        result = get_game_postponed_one_day()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(86399, expiry_time)

    def test_game_postponed_twodays(self):
        """
        Test case to validate expiry time is set to 24hrs
        if scheduled time - current time difference is more than 24hrs
        for game id passed has status postponed
        :return:0
        """
        result = get_game_postponed_two_days()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_game_delayed_oneday(self):
        """
        Test case to validate expiry time is set to
        scheduled time - current time if difference is less than 24 hours
        for game id passed has status delayed
        :return:0
        """
        result = get_game_postponed_one_day()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(86399, expiry_time)

    def test_game_delayed_twodays(self):
        """
        Test case to validate expiry time is set to 24hrs
        if scheduled time - current time difference is more than 24hrs
        for game id passed has status delayed
        :return:0
        """
        result = get_game_postponed_two_days()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_game_flex_scheduled(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status flex-scheduled
        :return:0
        """
        result = get_game_flex_schedule()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['boxscore'], expiry_time)

    def test_game_time_tbd(self):
        """
        Test case to validate expiry time is set to 24
        for game id passed has status flex-scheduled
        :return:0
        """
        result = get_game_time_tbd()
        sport_radar_api = 'nfl-ot1/games/c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_pbp_closed(self):
        """
        Test case to validate expiry time is set to 24
        for game id passed has status flex-scheduled
        :return:0
        """
        result = get_play_by_play_closed()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_pbp_scheduled_next_month(self):
        """
        Test case to validate expiry time is set to 24 hours
        if scheduled time - current time is greater than 24hrs
        and game id passed has status scheduled
        :return:
        """
        result = get_game_scheduled_next_month()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_pbp_scheduled_after_hour(self):
        """
        Test case to validate expiry time is set to
        scheduled time - current time if difference is less than 24hrs
        and game id passed has status scheduled
        :return:0
        """
        result = get_game_scheduled_after_hour()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(3599, expiry_time)

    def test_pbp_inprogress(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status inprogress
        :return:0
        """
        result = get_game_in_progress()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['play-by-play'], expiry_time)

    def test_pbp_halftime(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status halftime
        :return:0
        """
        result = get_game_halftime()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['play-by-play'], expiry_time)

    def test_pbp_complete(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status closed
        :return:0
        """
        result = get_game_complete()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['play-by-play'], expiry_time)

    def test_pbp_cancelled(self):
        """
        Test case to validate expiry time is set to 24hrs
        for game id passed has status cancelled
        :return:0
        """
        result = get_game_cancelled()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_pbp_postponed_oneday(self):
        """
        Test case to validate expiry time is set to
        scheduled time - current time if difference is less than 24 hours
        for game id passed has status postponed
        :return:0
        """
        result = get_game_postponed_one_day()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(86399, expiry_time)

    def test_pbp_postponed_twodays(self):
        """
        Test case to validate expiry time is set to 24hrs
        if scheduled time - current time difference is more than 24hrs
        for game id passed has status postponed
        :return:0
        """
        result = get_game_postponed_two_days()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_pbp_delayed_one_day(self):
        """
        Test case to validate expiry time is set to
        scheduled time - current time if difference is less than 24 hours
        for game id passed has status delayed
        :return:0
        """
        result = get_game_postponed_one_day()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(86399, expiry_time)

    def test_pbp_delayed_two_days(self):
        """
        Test case to validate expiry time is set to 24hrs
        if scheduled time - current time difference is more than 24hrs
        for game id passed has status delayed
        :return:0
        """
        result = get_game_postponed_two_days()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def test_pbp_flex_scheduled(self):
        """
        Test case to validate expiry time is set to 5 mins
        for game id passed has status flex-scheduled
        :return:0
        """
        result = get_game_flex_schedule()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(TTL['play-by-play'], expiry_time)

    def test_pbp_time_tbd(self):
        """
        Test case to validate expiry time is set to 24
        for game id passed has status flex-scheduled
        :return:0
        """
        result = get_game_time_tbd()
        sport_radar_api = 'nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json'
        expiry_time = SPORTRADAR_VIEW_SET.find_api_ttl(sport_radar_api, result)
        self.assertAlmostEqual(MAX_CACHE_INTERVAL, expiry_time)

    def tearDown(self):
        remove_test_log_file()
