"""
Testcase for sportradar interface
"""
from rest_framework import status
from rest_framework.test import APITestCase
from cheerios.feeds.test.test_data import response_status
#
# from cheerios.feeds.test.test_data import create_log_file,\
#     response_status, remove_test_log_file
# from cheerios.feeds.views.sportradar import SportradarViewSet
# from cheerios.settings import SPORTRADAR_API_KEY
#
# SPORTRADARVIEWSET = SportradarViewSet()
# PROD_ENV_VALUE = '-o'
#
#
class TestSportRadar(APITestCase):
    """
    Testcase for get operation.
    """

    def setUp(self):
        pass
#         create_log_file()

#     def test_get_api(self):
#         """
#         Pass valid url
#         It will create sportradar api and call it.
#         As url is correct it will return 200 status
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1%2Fgames%2F2016%2FREG%2Fschedule.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_api_invalid_game(self):
#         """
#         Pass invalid url containing invalid game
#         It will create sportradar api and call it.
#         As url is correct it will return 596 status
#         """
#         url = "/feeds/sportradar/?url=nfl-ot4%2Fgames%2F2016%2FREG%2Fschedule.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), 596)
#
#     def test_get_invalid_league(self):
#         """
#         Pass invalid url containing invalid league
#         It will create sportradar api and call it.
#         As url is correct it will return 596 status
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1%2Fgame%2F2016%2FREG%2Fschedule.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), 596)
#
#     def test_get_invalid_parameter(self):
#         """
#         Pass invalid url containing invalid parameter
#         It will create sportradar api and call it.
#         As url is correct it will return 404 status
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1%2Fgames%2F2018%2FREG%2Fschedule.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_503_SERVICE_UNAVAILABLE)
#
#     def test_get_invalid_url(self):
#         """
#         Pass invalid url containing invalid sportradar api
#         It will create sportradar api and call it.
#         As url is correct it will return 404 status
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1%2Fgames%2F2016%2FREG%2Fschedules.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_503_SERVICE_UNAVAILABLE)
#
#     def test_get_api_without_query(self):
#         """
#         Pass url with out query parameter
#         It will create sportradar api and call it.
#         As url is correct it will return 400 status
#         """
#         url = "/feeds/sportradar/"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_get_game_api(self):
#         """
#         Pass boxscore url. Pass existing game id.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1/games/" \
#               "28290722-4ceb-4a4c-a4e5-1f9bec7283b3/boxscore.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_id_notin_schedule(self):
#         """
#         Pass boxscore url.
#         Pass game id which is not present in schedules api data.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1/games/" \
#               "c8ca977e-77ad-42fc-a80b-0d4a78e73b87/boxscore.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_get_with_invalid_id(self):
#         """
#         Pass boxscore url.
#         Pass invalid game id.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1/games/" \
#               "c8ca977e-77ada78e73b87/boxscore.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_503_SERVICE_UNAVAILABLE)
#
#     def test_api_key_url(self):
#         """
#         Call api with invalid url.
#         Include api_key as parameter to url passed from tablet
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1%2Fgames%2F2016%2FREG%2Fschedule.json&api_key=" \
#               + SPORTRADAR_API_KEY
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_api_key_in_url(self):
#         """
#         Call api with api_key as part of url.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1%2Fgames%2F2016%2FREG%2Fschedule.json/api_key=" \
#               + SPORTRADAR_API_KEY
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_invalid_format(self):
#         """
#         Call api with format other then json.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1%2Fgames%2F2016%2FREG%2Fschedule.xml"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_playbyplay_api(self):
#         """
#         Call play-by-play api.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1/games/28290722-4ceb-4a4c-a4e5-1f9bec7283b3/pbp.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_playbyplay_with_invalid_id(self):
#         """
#         Call play-by-play api.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1/games/28290722-4ceb-1f9bec7283b3/pbp.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_503_SERVICE_UNAVAILABLE)
#
#     def test_seasonal_statistics_api(self):
#         """
#         Call seasonal statistics api.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1/seasontd/2015/REG/teams/" \
#               "33405046-04ee-4058-a950-d606f8c30852/statistics.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_seasonal_statistics_id(self):
#         """
#         Call seasonal statistics api with wrong id.
#         :return:
#         """
#         url = "/feeds/sportradar/?url=nfl-ot1/seasontd/2015/REG/teams/" \
#               "33405046-04ee-4058-2/statistics.json"
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_503_SERVICE_UNAVAILABLE)
#
#     def test_create_env_specific_url(self):
#         """
#         Call create_environment_specific_url function
#         and validate if function is working as expected
#         :return:
#         """
#         sport_radar_api_trail = 'nfl-ot1/games/2016/REG/schedule.json'
#         new_trail_url = SPORTRADARVIEWSET.create_environment_specific_url(sport_radar_api_trail)
#         self.assertEqual(new_trail_url, 'nfl' + PROD_ENV_VALUE + '1/games/2016/REG/schedule.json')
#         sport_radar_api_prod = 'nfl-o1/games/2016/REG/schedule.json'
#         new_prod_url = SPORTRADARVIEWSET.create_environment_specific_url(sport_radar_api_prod)
#         self.assertEqual(new_prod_url, 'nfl' +  PROD_ENV_VALUE + '1/games/2016/REG/schedule.json')
#
#     def tearDown(self):
#         """
#         Teardown function to remove test log files
#         :return:
#         """
#         remove_test_log_file()



class TestHostName(APITestCase):
    """
    Testcase for checking Hostname API is working.
    """
    def test_health_check(self):
        """
        Test if Hostname API is working properly.
        """
        url = "/feeds/hostname/"
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
