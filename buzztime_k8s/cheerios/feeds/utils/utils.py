"""
Contains various classes for redis connection,
string operations and generic util functions
"""
import datetime
import time


class UtilFunction(object):
    """
    Class with generic util functions
    """

    @staticmethod
    def get_json_data_from_response(sportradar_data):
        """
        Check if response data is valid json
        If yes then returning json data
        Otherwise returning false
        """
        try:
            json_object = sportradar_data.json()
        except ValueError:
            return False
        return json_object
