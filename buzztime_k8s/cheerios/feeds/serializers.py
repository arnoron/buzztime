"""
Serializer classes
"""
from rest_framework import serializers
from cheerios.feeds.models import Crisp


class CrispSerializerforInternalModel(serializers.ModelSerializer):
    """
    Serializer for Crisp Model to show field in doc.
    """
    class Meta:
        """
        Meta class for print fields in DRF doc.
        """
        fields = ('ContentId', 'Action', 'ApiKey')
        model = Crisp
