import multiprocessing

from cheerios.mysterymachine.command_service_layer.command_response import CommandResponse
from cheerios.mysterymachine.pub_sub_service_layer.publisher import publisher
from cheerios.mysterymachine.pub_sub_service_layer.subscriber import Subscriber
from cheerios.services.log_classifier import Logger


class CommandRequest:
    """
    The command request class
    """

    def __init__(self, payload, target_site, method, command_name, token=None):
        """
        Initializes the class parameters
        :param payload: The command payload to be executed
        :param target_site: The site to which the command is targeted to
        """
        self.command_payload = payload
        self.target_site = target_site

        # Get this from request param as per the user
        self.request_source = "agent/001"

        self.method = method
        self.command_name = command_name
        self.token = str(token)

    def execute_command(self):
        """
        Executes the command passed in the command request
        :return:
        """

        # Create a subscriber instance to the target site topic
        subscriber = Subscriber(self.request_source)

        # A command response object to handle the response of the command
        command_response = CommandResponse(subscriber, self.command_name, self.request_source,
                                           token=self.token)

        self.command_payload = self.get_command_payload()

        # Start the subscriber in the background
        subscriber.on_message = command_response.push_to_storage
        subscriber.start_in_background()

        # Publish the command to the topic for the target site
        try:
            publisher.publish(payload=self.command_payload, topic=self.target_site)
        except Exception as ex:
            Logger.log.error(str(ex))
            subscriber.stop()
            raise ex


    def get_command_payload(self):
        """
        Create the structure of the command payload and return
        :return:
        """
        cmd = 'replyto {}\n{} {}'.format(self.request_source, self.method, self.command_name)
        if self.command_payload:
            cmd = '{}\n{}'.format(cmd, self.command_payload)
        return cmd
