import hashlib
import json
from time import time

from cheerios.services.firebase import Firebase
from cheerios.services.redis import RedisSession
from cheerios.settings import MM_FIREBASE_API_KEY, MM_FIREBASE_AUTHDOMAIN, MM_FIREBASE_KEY, \
    MM_FIREBASE_STORAGE_BUCKET
from cheerios.services.log_classifier import Logger


class CommandResponse:
    """

    """

    def __init__(self, subscriber, command, site, token=None):
        """

        """
        self.source_site = site
        self.command = command
        self.subscriber = subscriber
        self.token = str(token)

    def push_to_storage(self, client, userdata, msg):
        """

        :param client:
        :param userdata:
        :param msg:
        :return:
        """

        # Connect to Firebase and push the response payload to it, then close the subscriber process
        response_key = self.get_key()
        response_value = msg.payload.decode("utf-8")

        # For Push Notifications
        obj = dict(command=self.command,
                   site="BWW California",
                   read=False,
                   key=response_key,
                   success='"success":true' in response_value,
                   time=time())

        # Storing in Redis
        redis = RedisSession()
        redis.save_data(key=response_key, value=response_value, persist=True)
        redis.set_object(name=self.token, key=response_key, value=json.dumps(obj))

        # Storing in Firebase
        try:
            firebase_obj = Firebase(MM_FIREBASE_API_KEY,
                                    MM_FIREBASE_AUTHDOMAIN,
                                    MM_FIREBASE_KEY,
                                    MM_FIREBASE_STORAGE_BUCKET)
            firebase_obj.create_connection()
            firebase_obj.push(url="/mystery_machine/", key=str(self.token), data=obj)
        except Exception as err:
            Logger.log.error(str(err))

        self.subscriber.stop(client)

    def get_key(self):
        """
        Returns the key to be saved in the firebase
        :return:
        """
        order = hash((self.token, time()))
        key = "{}@{}@{}".format(self.command, self.source_site, order)

        # MD5ing the key to avoid the unacceptable characters
        md5_obj = hashlib.md5()
        md5_obj.update(key.encode("utf-8"))
        hashed_key = md5_obj.hexdigest()

        return hashed_key
