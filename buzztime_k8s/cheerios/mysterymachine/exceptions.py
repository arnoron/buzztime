"""
Exception Module
"""


class MysteryMachineException(Exception):
    """
    Exception Class For MysteryMachine Request
    """
    pass
