"""
This Module contains the class for executing
the stored procedures
"""

from cheerios.interface.api_view_interface import ApiViewInterface

from cheerios.mysterymachine.constants import URL
from cheerios.mysterymachine.serializers import StoredProcedureSerializer
from cheerios.mysterymachine.services.configurations import stored_procedures
from cheerios.mysterymachine.services.utils import catch_error
from cheerios.services.jwt_utils import jwt_decorator


class ExecuteStoredProcedure(ApiViewInterface):
    """
    This class will execute the stored procedure
    based on the parameters passed.
    """

    @catch_error
    @jwt_decorator(end_point=URL['stored_procedure'], method="POST")
    def post(self, request):
        """
        Overriding the post method of the base class
        :param request:
        :return:
        """
        return super().post(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return StoredProcedureSerializer

    def get_from_downstream(self, data):
        """
        This method will fetch the entities
        passed in the parameters after calling the stored procedure
        :param data:
        :return:
        """
        procedure_name = data['procedure_name']
        parameters_dict = data['parameters']
        config = stored_procedures[procedure_name]
        transaction = data.get('transaction', False)
        procedure = config["store_procedure"]
        db = procedure.split('.')[0]
        params = config['params'].format(**parameters_dict)
        with self.connection_manager(procedure, params, db, transaction) as cursor:
            result = self.fetchall_from_cursor(cursor)

        return result
