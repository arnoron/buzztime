"""
This Module contains the class for executing the stored procedures
"""

from urllib.parse import quote

from rest_framework.response import Response
from rest_framework.views import APIView

from cheerios.mysterymachine.constants import URL
from cheerios.mysterymachine.serializers import ExecuteCommandSerializer
from cheerios.mysterymachine.services.command_configurations import command_config
from cheerios.mysterymachine.services.rabbit import RABBITMQ
from cheerios.mysterymachine.services.utils import catch_error
from cheerios.services.jwt_utils import jwt_decorator
from cheerios.services.redis import RedisSession
from cheerios.settings import LISTENER_PREFIX
from cheerios.utils import CheeriosUtils

CHEERIOS_UTILS = CheeriosUtils()
REDIS = RedisSession()


class ExecuteCommand(APIView):
    """
    This class will execute the stored procedure based on the parameters passed.
    """

    @catch_error
    @jwt_decorator(end_point=URL['command'], method="POST")
    def post(self, request):
        """
        Validating Data and Processing the Data
        :param request:
        :return:
        """
        data = request.data
        serializer = ExecuteCommandSerializer(data=data)
        CHEERIOS_UTILS.validate_serializer(serializer)
        return self.process_command(serializer.validated_data)

    def process_command(self, data):
        """
        Publish Command and returns response
        :param data:
        :return:
        """
        topic = data['topic']
        payload = data['payload']
        call_back = data['call_back']
        command_registry = data['command']

        configuration = command_config[command_registry]

        method = configuration['method']
        command = configuration['command']

        route = self.get_route(call_back)

        RABBITMQ.execute_command(
            topic=topic,
            route=route,
            method=method,
            command=command,
            payload=payload
        )

        command_name = configuration['name']
        response = dict(
            command=command_name,
            call_back=call_back
        )

        return Response(response)

    def get_route(self, call_back):
        """
        Generates Route and message_id
        :return:
        """
        encoded_url = quote(call_back, safe='')
        route = '.'.join([LISTENER_PREFIX, encoded_url])
        return route
