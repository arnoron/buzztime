from cheerios.interface.api_view_interface import ApiViewInterface
from cheerios.mysterymachine.command_service_layer.command_request import CommandRequest
from cheerios.mysterymachine.decorators import validate_permission
from cheerios.mysterymachine.permission_codes import EXECUTE_COMMAND
from cheerios.mysterymachine.serializers import ExecutePOSTCommandSerializer
from cheerios.services.log_classifier import Logger


class ExecuteCommand(ApiViewInterface):
    """
    View set for Execute Command API
    """
    token = None

    @validate_permission([EXECUTE_COMMAND])
    def post(self, request):
        """
        Get API.
        This API will be called for any GET command.
        It will make an call to RabbitMQ from
        there to Sitehub and execute the command and return the response.
        :param request:
        :return: status code 200/500
        """
        return super().post(request)

    def get_serializer(self):
        """
        Return serializer
        """
        return ExecutePOSTCommandSerializer

    def get_from_downstream(self, data):
        """

        :param data:
        :return:
        """
        command_request = CommandRequest(payload=data.get('payload'),
                                         method=data.get('method'),
                                         target_site=data.get('reply_to_site'),
                                         command_name=data.get('command_name'),
                                         token=self.token)

        try:
            command_request.execute_command()
        except Exception as ex:
            Logger.log.error(str(ex))
            return dict(error=1, content=("Cannot execute the command currently. Please try after sometime", 500))

        return "Command Submitted"
