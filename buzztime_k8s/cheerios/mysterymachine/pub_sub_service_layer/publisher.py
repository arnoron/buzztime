import paho.mqtt.client as mqtt
from paho.mqtt import publish
import os
from cheerios.settings import RMQ_USERNAME, RMQ_PASSWORD, RMQ_HOST, RMQ_PORT


class Publisher:
    """
    The Publisher module which publishes a payload to a messaging queue service
    """

    def __init__(self):
        """
        Initializes the connection parameters
        """
        # TODO Get these values from the settings file
        self.username = RMQ_USERNAME
        self.password = RMQ_PASSWORD
        self.host = RMQ_HOST
        # Added in condition it is throwing error while deploying code.
        if RMQ_PORT:
            self.port = int(RMQ_PORT)

    def publish(self, payload, topic):
        """
        Creates a connection to the messaging queue and publishes a payload to it
        :param payload: The payload which needs to be published to the queue
        :param topic: The topic to which the payload needs to be published
        :return: None
        """
        # Initialize the Connection to the client to publish the body
        client = mqtt.Client(userdata={'username': self.username, 'password': self.password})
        client.connect(self.host, port=self.port, keepalive=60, bind_address="")

        # Publish the Payload to the client
        publish.single(topic, payload, hostname=self.host, port=self.port,
                       auth={'username': self.username,
                             'password': self.password})


publisher = Publisher()
