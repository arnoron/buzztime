import multiprocessing
import paho.mqtt.client as mqtt

from cheerios.settings import RMQ_USERNAME, RMQ_PASSWORD, RMQ_HOST, RMQ_PORT
from cheerios.services.log_classifier import Logger


class Subscriber:
    """
    The Subscriber Module subscribes to a topic in a
    messaging queue and executes the callback function
    in case of a new message
    """

    def __init__(self, topic):
        """
        Initializes the connection parameters
        """
        self.username = RMQ_USERNAME
        self.password = RMQ_PASSWORD
        self.host = RMQ_HOST
        self.port = int(RMQ_PORT)
        self.topic = topic
        self.background_process = None

    def start_in_background(self):
        """
        Starts a subscriber process to a specified topic as a daemon
        :return:
        """
        # BG Process Creating Error while Stoping the Process
        # self.background_process = multiprocessing.Process(name="subscriber",
        #                                                 target=self.subscribe,
        #                                                 args=(self.topic,))
        # self.background_process.daemon = True
        # self.background_process.start()
        self.subscribe(self.topic)

    def stop(self, client):
        """
        Stops the subscriber running in the background
        :return:
        """
        # BG Process Creating Error while Stoping the Process
        # if self.background_process:
        #     self.background_process.terminate()
        client.loop_stop(force=True)


    @staticmethod
    def on_message(client, userdata, msg):
        """
        The callback method. This method is dynamically initialized by the lower layer as per the implementation
        """
        pass

    def subscribe(self, topic):
        """
        Subscribes to a passed topic and loops forever. On arrival
        of a new message, calls the on_message callback function
        :param topic: The topic to subscribe to.
        :return: None
        """

        def on_connect(client, userdata, flags, rc):
            client.subscribe(topic)

        # Initialize the Connection to the client to publish the body
        client = mqtt.Client(userdata={'username': self.username, 'password': self.password})

        client.on_connect = on_connect
        client.on_message = self.on_message

        client.username_pw_set(self.username, password=self.password)
        client.connect(self.host, self.port, 60)
        # Commented due to daemon error
        # client.loop_forever()
        # Logger.log.error('Looping Forever')
        client.loop_start()
