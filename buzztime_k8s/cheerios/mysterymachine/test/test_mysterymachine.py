"""
Test Cases for SitesHub Stored Procedure Calls
"""
from rest_framework.test import APITestCase
from mock import patch, Mock

from cheerios.mysterymachine.test.response_data import MOCK_RESPONSE
from cheerios.mysterymachine.test.test_data import site_list_data, \
    site_info_data, invalid_site_info_data
from cheerios.test_utils import mock_test_utils, generate_jwt_token


class TestSites(APITestCase):
    """
    This is the Test class for sitehub
    """

    def setUp(self):
        """
        Setup Method for TestSites
        :return:
        """
        self.url = '/mysterymachine/execute_stored_procedure/'
        self.token = generate_jwt_token('mysterymachine', self.url)

    def call_api(self, url, data):
        """
        method to call the url
        :param token:
        :param url:
        :return:
        """
        data["jwt"] = self.token
        resp = self.client.post(url, format='json', data=data)
        return resp.status_code, resp.json()

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['site_list'])))
    def test_get_sites(self):
        """
        test get all organisations
        :return:
        """
        status_code, data = self.call_api(self.url, site_list_data)
        self.assertEqual(status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['site_info'])))
    def test_get_info(self):
        """
        Test case to get all organisations
        :return:
        """
        status_code, data = self.call_api(self.url, site_info_data)
        self.assertEqual(status_code, 200)
        self.assertEqual(len(data[0]), 15)

    def test_negative_info(self):
        """
        Wrong dataTest Case
        :return:
        """
        status_code, data = self.call_api(self.url, {})
        self.assertEqual(status_code, 400)

    def test_invalid_data(self):
        """
        Test Case for Invalid data
        :return:
        """
        status_code, data = self.call_api(self.url, invalid_site_info_data)
        self.assertEqual(status_code, 400)
