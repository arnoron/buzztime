"""
This module contains the test data required
for the test cases for mystery machine
"""

site_list_data = {
            "procedure_name": "site_list",
            "parameters": {}
        }

site_info_data = {
            "procedure_name": "site_info",
            "parameters": {'site_id': '40974'}
        }
invalid_site_info_data = {
            "procedure_name": "site_info"
        }