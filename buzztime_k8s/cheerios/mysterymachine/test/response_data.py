"""
Mock Responses
"""

MOCK_RESPONSE = dict(
    site_info=[{
        "longitude": -117.278771,
        "Zip": "92011",
        "SiteID": 40902,
        "State": "CA",
        "PriPhone": None,
        "SiteIPAddress": "172.23.59.109",
        "Address1": "2231 rutherford rd",
        "RouterSSID": None,
        "SiteName": "NTN Site Hub - Test Site #5",
        "TimeZone": 0,
        "IsTabletEnabled": "false",
        "Country": "US",
        "Address2": None,
        "City": "carlsbad",
        "latitude": 33.131288
    }],
    site_list=[
        {
            "latitude": 33.131288,
            "Country": "US",
            "SiteID": 40974,
            "OrgName": "NTN Site Hub - Test Site #14",
            "City": "Carlsbad",
            "longitude": -117.278771,
            "Zip": "92008",
            "Address1": "2231 Rutherford Road",
            "Address2": "Suite 200",
            "State": "CA"
        },
        {
            "latitude": 33.131288,
            "Country": "US",
            "SiteID": 40882,
            "OrgName": "NTN Site Hub - Test Site #3",
            "City": "Carlsbad",
            "longitude": -117.278771,
            "Zip": "92008",
            "Address1": "2231 Rutherford Rd",
            "Address2": None,
            "State": "CA"
        }]
)
