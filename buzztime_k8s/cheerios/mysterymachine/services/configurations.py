"""
This file contains the configurations for Stored Procedure Calls.

"""

stored_procedures = {
    "site_list": {
        "store_procedure": "Programming.dbo.usp_sel_SiteHubSiteList",
        "params": ""
    },
    "site_info": {
        "store_procedure": "Programming.dbo.usp_sel_SiteHubSiteInfo",
        "params": "@SiteID={site_id}"
    },
    "device_list": {
        "store_procedure": "Programming.dbo.usp_sel_SiteHubDeviceBySiteID",
        "params": "@SiteID={site_id}"
    },
    "get_sites": {
        "store_procedure": "Programming.dbo.usp_sel_AssignmentEntity",
        "params": "@AssignmentEntityTypeID={entity_type}, @AssignmentEntityID={entity_id}"
    },
    "search_entity": {
        "store_procedure": "Programming.dbo.usp_sel_FindOrgAssignmentEntity",
        "params": "@assignmentEntityTypeID={entity_type}, @SearchString='%{search}%'"
    },
    "get_entity": {
        "store_procedure": "Programming.dbo.usp_sel_FindOrgAssignmentEntity",
        "params": "@assignmentEntityTypeID={entity_type}, @SearchInt={entity_id}"
    }


}
