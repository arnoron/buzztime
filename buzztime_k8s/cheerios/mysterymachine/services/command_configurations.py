command_config = {
    'mediacontent': {
        'name': 'Media Content',
        'method': 'GET',
        'command': 'mediacontent',
    },

    # System Status

    'initlog': {
        # Also in Diagnostic, Profiling
        'name': 'Init Log',
        'method': 'POST',
        'command': 'server/init_logs',
    },

    'handshake': {
        'name': 'Handshake',
        'method': 'POST',
        'command': 'server/handshake',
    },

    'confidential': {
        'name': 'Confidential',
        'method': 'POST',
        'command': 'server/confidential_keys',
    },

    'site_ops': {
        'name': 'Site Control',
        'method': 'POST',
        'command': 'server/site_ops',
    },

    'site_info': {
        'name': 'site_info',
        'method': 'POST',
        'command': 'server/site_info',
    },

    # Content

    'installed_pkgs': {
        'name': 'Installed Packages',
        'method': 'POST',
        'command': 'server/installed_apks',
    },

    'content_manifest': {
        'name': 'Content Manifest',
        'method': 'POST',
        'command': 'server/content_files',
    },

    # Log Editor

    'pkg_log': {
        'name': 'Package Log',
        'method': 'POST',
        'command': 'server/pkg_loglevel',
    },

    'logtag': {
        'name': 'Set Log Level',
        'method': 'POST',
        'command': 'server/logtag',
    },

    # System Logs

    'site_logs': {
        'name': 'Site Logs',
        'method': 'POST',
        'command': 'server/site_logs',
    },

    'sync_logs': {
        'name': 'Sync Logs',
        'method': 'POST',
        'command': 'server/sync_logs',
    },

    'screen_cap': {
        'name': 'Screenshot',
        'method': 'POST',
        'command': 'server/screen_cap',
    },

    'logcat': {
        'name': 'Sync Logs',
        'method': 'POST',
        'command': 'server/logcat',
    },

    # Diagnostics

    'cpu_thermal_zones': {
        'name': 'CPU Thermal Zone',
        'method': 'POST',
        'command': 'server/cpu_thermal_zones',
    },

    'load_avg': {
        'name': 'Load Avg',
        'method': 'POST',
        'command': 'server/load_avg',
    },

    'io_stats': {
        'name': 'IO Stats',
        'method': 'POST',
        'command': 'server/io_stats',
    },

    'ping_test': {
        'name': 'Ping Test',
        'method': 'POST',
        'command': 'server/ping_hosts',
    },

    'disk_partitions': {
        'name': 'Disk Partition',
        'method': 'POST',
        'command': 'server/disk_partitions',
    },

    'ram': {
        'name': 'RAM Info',
        'method': 'POST',
        'command': 'server/current_ram',
    },

    'network_stats': {
        'name': 'Network Stats',
        'method': 'POST',
        'command': 'server/network_stats',
    },

    # Device Commands

    'reboot': {
        'name': 'Reboot',
        'method': 'POST',
        'command': 'server/reboot_sitehub'
    },

    'system_update': {
        'name': 'System Update',
        'method': 'POST',
        'command': 'dash_op/system_update'
    },

    'system_install': {
        'name': 'System Install',
        'method': 'POST',
        'command': 'dash_op/system_install'
    }

}
