"""
This File contains the utilities for the Mystery Machine
Services
"""

from functools import wraps
from rest_framework import status
from rest_framework.response import Response

from cheerios.services.log_classifier import Logger


def catch_error(api_function):
    """
    A decorator function which captures errors for Django API.
    :param api_function:
    :return: decorated function.
    """

    @wraps(api_function)
    def inner_fn(*args, **kwargs):
        """
        A Function which wraps the provided function with exception blocks and it's status.
        :param args:
        :param kwargs:
        :return:
        """
        try:
            return api_function(*args, **kwargs)
        except (ValueError, KeyError) as err:
            error_message = "Invalid data for {0}".format(err.args[0])
            Logger.log.error(error_message)
            return Response(data="Invalid data", status=status.HTTP_400_BAD_REQUEST)
        except Exception as err:
            Logger.log.error("Error {}".format(str(err)))
            return Response(data="Internal Error", status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return inner_fn
