from paho.mqtt import publish

from cheerios.settings import MM_RMQ_USERNAME, MM_RMQ_PASSWORD, MM_RMQ_PORT, MM_RMQ_HOST


class RabbitMQ:

    def execute_command(self, topic, route, method, command, payload=None):
        """
        gets command and executes it on topic
        :param topic:
        :param route:
        :param method:
        :param command:
        :param payload:
        :return:
        """
        cmd = self.get_command(route, method, command, payload)
        self.publish_command(topic, cmd)

    def publish_command(self, topic, command):
        """
        Publisher command to the topic
        :param topic:
        :param command:
        :return:
        """
        publish.single(
            topic=topic,
            auth=dict(
                username=MM_RMQ_USERNAME,
                password=MM_RMQ_PASSWORD
            ),
            port=MM_RMQ_PORT,
            payload=command,
            hostname=MM_RMQ_HOST
        )

    def get_command(self, route, method, command, payload=None):
        """
        Generates command from the args
        :param route:
        :param method:
        :param command:
        :param payload:
        :return:
        """
        cmd = 'replyto {0}\n{1} {2}'.format(route, method, command)
        if payload:
            cmd = '{0}\n\n{1}'.format(cmd, payload)
        return cmd


RABBITMQ = RabbitMQ()
