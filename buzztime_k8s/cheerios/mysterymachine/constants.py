"""
This is the module which contains constants for mystery machine APIs
"""

URL = {
    'stored_procedure': '/mysterymachine/execute_stored_procedure/',
    'command': '/mysterymachine/execute_command/'
}
