"""
Serializer for MysteryMachine
"""

from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class ExecutePOSTCommandSerializer(SerializerValidator):
    """
    Serializer for Execute Command
    """
    payload = serializers.CharField(required=False)
    command_name = serializers.CharField(allow_null=False, required=True)
    reply_to_site = serializers.CharField(allow_null=False, required=True)
    method = serializers.CharField(required=True)


class StoredProcedureSerializer(SerializerValidator):
    """
    Serializer for Stored Procedure Calls
    """
    procedure_name = serializers.CharField(required=True)
    parameters = serializers.DictField(required=True)
    transaction = serializers.BooleanField(default=False)


class ExecuteCommandSerializer(SerializerValidator):
    """
    Serializer for Execute Command
    """
    topic = serializers.CharField(required=True)
    command = serializers.CharField(required=True)
    call_back = serializers.URLField(required=True)
    payload = serializers.DictField(required=False, default=dict())
