"""
Decorators Module for MysteryMachine
"""
from functools import wraps

import requests
from rest_framework import status
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from cheerios.mysterymachine.exceptions import MysteryMachineException
from cheerios.settings import MM_CHECK_PERMISSION_API
from cheerios.services.log_classifier import Logger


def call_api(request, code):
    """
    function to call Permissions API and Validate those. returns token of user on successful
    validation
    :param request:
    :param code:
    :return:
    """
    data = dict(permissions=code)
    headers = dict(authorization=request.META.get('HTTP_AUTHORIZATION'))
    response = requests.post(MM_CHECK_PERMISSION_API, headers=headers, json=data)
    data = response.json()

    if response.status_code != status.HTTP_200_OK:
        raise MysteryMachineException(data, response.status_code)

    token = data.pop('token', None)
    result = response.json().values()
    if not all(result):
        raise PermissionDenied
    return token


def validate_permission(code=None):
    """
    Wrapper for Decorator
    :param code:
    :return:
    """
    def actual_decorator(func):
        """
        Actual Decorator
        :param func:
        :return:
        """
        @wraps(func)
        def check_permission(self, request, *args, **kwargs):
            """
            wraping function to authenticate permissions
            :param self:
            :param request:
            :param args:
            :param kwargs:
            :return:
            """
            try:
                self.token = call_api(request, code)
                return func(self, request, *args, **kwargs)
            except PermissionDenied as err:
                raise err
            except MysteryMachineException as err:
                Logger.log.error(str(err))
                return Response(data=err.args[0], status=err.args[1])
            except ValueError as err:
                Logger.log.error(str(err))
                return Response(data=err.args[0], status=status.HTTP_400_BAD_REQUEST)
            except Exception as err:
                Logger.log.error(str(err))
                data = dict(detail="Internal Server Error. Please try after sometime.")
                return Response(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return check_permission

    return actual_decorator
