from datetime import datetime
from itertools import cycle

import jwt
from mock import MagicMock

from cheerios.settings import JWT_SECRET_KEYS


class MockTestUtils:
    def __init__(self):
        self.EXE_STORED_PROCEDURE = ''
        self.CONNECTION_MANAGER = 'cheerios.interface.msssql_connection.MSSQLApiView.' \
                                  'connection_manager'
        self.CALL_STORED_PROCEDURE = 'cheerios.interface.msssql_connection.MSSQLApiView.' \
                                     'call_storeprocedure'
        self.FETCH_ALL_FROM_CURSOR = 'cheerios.interface.msssql_connection.MSSQLApiView.' \
                                     'fetchall_from_cursor'
        self.GET_CURSOR = 'cheerios.interface.msssql_connection.MSSQLApiView.get_cursor'
        self.FETCHALL = 'cheerios.interface.msssql_connection.MSSQLApiView.fetchall'

    def set_cursor_fetch_data(self, cursor, data):
        cursor.return_value.__enter__.return_value.__iter__.return_value = data
        return cursor

    def mock_connection_manager(self):
        mock = MagicMock()
        mock.__enter__.return_value.__iter__.return_value = None
        mock.__exit__.return_value = False
        return cycle([mock])

    def mock_cursor_responses(self, *args):
        mocked = []
        for response in args:
            mock = MagicMock()
            mock.__enter__.return_value.__iter__.return_value = response
            mock.__exit__.return_value = False
            mocked.append(mock)
        return cycle(mocked)

    def mock_fetch_responses(self, *args):
        mocked = []
        for response in args:
            mocked.append(response)
        return cycle(mocked)


def generate_jwt_token(service, path_info):
    """
    generate jwt token for authentication with cheerios
    :return:
    """
    time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
    payload = dict(REQUEST_METHOD="POST",
                   PATH_INFO=path_info, time=time,
                   iss="android", aud="cheerios")
    procedure_key = JWT_SECRET_KEYS[service]
    encoded = jwt.encode(payload, procedure_key, algorithm='HS256')
    token = encoded.decode('utf-8')
    return token


mock_test_utils = MockTestUtils()
