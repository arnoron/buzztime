"""
This file holds the constants used in players cheerios
"""
from cheerios.settings import CHEERIOS_DIR

STORED_PROCEDURE_MAPPER = {'achievement_leaderboard':
                               'UserDB.dbo.usp_sel_AchievementPlayerSummaryList',
                           'achievement_classification':
                               'UserDB.dbo.usp_sel_AchievementClassificationNameValuePairList',
                           'achievement_rarity':
                               'UserDB.dbo.usp_sel_AchievementRarityNameValuePairList',
                           'achievement_type':
                               'UserDB.dbo.usp_sel_AchievementTypeNameValuePairList',
                           'achievement_name': 'UserDB.dbo.usp_sel_AchievementNameValuePairList',
                           'yesterday_games': 'dbGamePlay.dbo.usp_sel_HOFYesterdayGames',
                           'trivia_games': 'dbGamePlay.dbo.usp_sel_HOFTriviaGames',
                           'game_winners': 'dbGamePlay.dbo.usp_sel_HOFGameWinners',
                           'game_winners_site': 'dbGamePlay.dbo.usp_sel_HOFGameWinnersSite',
                           'player_averages': 'dbGamePlay.dbo.usp_sel_GamePlayerAverages',
                           'game_forum': 'dbGamePlay.dbo.usp_sel_GamesForumPlayerStats',
                           'player_plus_leaders': 'dbGamePlay.dbo.usp_sel_PlayersPlusLeaders',
                           'top_trivia_sites': 'dbGamePlay.dbo.usp_sel_TopTriviaSites',
                           'game_schedule': 'dbGamePlay.dbo.usp_sel_PrimetimeSchedule',
                           'events': 'dbGamePlay.dbo.usp_sel_MultiSiteFinder',
                           'player_search': 'UserDB.dbo.usp_sel_PlayerSearch',
                           'site_scores': 'dbGamePlay.dbo.usp_sel_HOFSiteScores',
                           'player_scores': 'dbGamePlay.dbo.usp_sel_HOFPlayerScores',
                           'site_player_scores': 'dbGamePlay.dbo.usp_sel_HOFSitePlayerScores',
                           'site_competitions': 'dbGamePlay.dbo.usp_sel_Competitions',
                           'site_info': 'dbGamePlay.dbo.usp_sel_SiteInfo',
                           'member_info': 'UserDB.dbo.usp_sel_tbMembers',
                           'location_search_site_name':
                               'dbGamePlay.dbo.usp_sel_SiteFinderForNameSearch',
                           'location_search_site_near':
                               'dbGamePlay.dbo.usp_sel_SiteFinderByGeoLocationSiteService',
                           'customer_competitions': 'dbGamePlay.dbo.usp_sel_CompetitionXML',
                           'site_competitions_ranking': 'dbGamePlay.dbo.usp_sel_CompetitionRanking',
                           'competition_games': 'dbGamePlay.dbo.usp_sel_CompetitionGames',
                           'site_players': 'dbGamePlay.dbo.usp_sel_SitePlayers',
                           'site_games': 'dbGamePlay.dbo.usp_sel_SiteGames',
                           'site_game_players': 'dbGamePlay.dbo.usp_sel_SiteGamePlayers',
                           'site_game_history': 'dbGamePlay.dbo.usp_sel_SiteGameHistory',
                           'site_game_player_history':
                               'dbGamePlay.dbo.usp_sel_SiteGamePlayerHistory',
                           'player_group': 'dbo.usp_sel_PlayerGroup',
                           'player_group_member_full': 'dbo.usp_sel_PlayerGroupMemberFull',
                           'achievement_earned_count':
                               'UserDB.dbo.usp_sel_AchievementTotalEarnedCountForPlayer',
                           'game_type_averages': 'dbGamePlay.dbo.usp_sel_GameTypeAverages',
                           'game_casino_stats': 'dbGamePlay.dbo.usp_sel_GameCasinoStats',
                           'player_recent_stats': 'dbGamePlay.dbo.usp_sel_PlayerRecent',
                           'achievement_event_history':
                               'UserDB.dbo.usp_sel_NamespaceUserAchievementEventHistory',
                           'game_type_scores': 'dbGamePlay.dbo.usp_sel_GameTypeScores',
                           'game_casino_scores': 'dbGamePlay.dbo.usp_sel_GameCasinoScores',
                           'achievement_list':
                               'UserDB.dbo.usp_sel_NamespaceUserAchievementList',
                           'achievement_info': 'UserDB.dbo.usp_sel_NamespaceUserAchievementInfo',
                           'achievement_event_list':
                               'UserDB.dbo.usp_sel_NamespaceUserAchievementHistory',
                           'registration': 'UserDB.dbo.usp_ins_Registration',
                           'namespace_identity': 'UserDB.dbo.usp_sel_EntityNamespaceUserName',
                           'email_exists_status': 'UserDB.dbo.usp_sel_EmailExists',
                           'inappropriate_word_status': 'dbPlayerPlus.dbo.usp_sel_DirtyWord',
                           'username_exists_status': 'UserDB.dbo.usp_sel_UserNameExists',
                           'points_award_queue': 'dbPlayerPlus.dbo.usp_ins_PointsAwardQueue',
                           'get_avatar': 'dbPlayerPlus.dbo.usp_sel_Avatar',
                           'save_avatar': 'dbPlayerPlus.dbo.usp_set_Avatar',
                           'delete_avatar': 'dbPlayerPlus.dbo.usp_del_Avatar',
                           'update_member_contact': 'dbPlayerPlus.dbo.usp_upd_MemberContact',
                           'update_display_name': 'UserDB.dbo.usp_upd_DisplayName',
                           'update_entity_namespace': 'UserDB.dbo.usp_upd_EntityNamespace',
                           'update_entity_password': 'UserDB.dbo.usp_upd_NamespacePassword',
                           'sports_stats': 'dbGamePlay.dbo.usp_sel_TriviaGameSportsProfile',
                           'authentication': 'UserDB.dbo.usp_sel_PlayerAuthentication',
                           'forgot_password': 'UserDB.dbo.usp_sel_ForgotPassword',
                           'hash_authentication': 'UserDB.dbo.usp_sel_AuthenticatePlayer',
                           'platform_services_config': \
                               'Programming.dbo.usp_sel_PlatformServicesConfiguration',
                           'power_page': 'dbGamePlay.dbo.usp_sel_PowerPage',
                           'smartest_bar': 'dbGamePlay.dbo.usp_sel_CompetitionRanking_withMVP',
                           'save_player_opt_in': 'dbPlayerPlus.dbo.usp_sav_PlayerOptin',
                           'insert_sweepstake_entry':
                               'Programming.dbo.usp_ins_SweepstakesAlternateEntry',
                           'platform_api_key': 'UserDB.dbo.usp_sel_PlatformAPIKey',
                           'site_name': 'dbGamePlay.dbo.usp_sel_tbOrg',
                           'competition_player_ranking': 'dbGamePlay.dbo.usp_sel_CompetitionPlayerRanking'
                          }

GAME_TYPE_ID_MAPPER = {17: 'StumpLiveTrivia',
                       18: 'StumpLiveQuestionnairey',
                       19: 'StumpPrivateTrivia',
                       20: 'StumpPrivateQuestionnairey',
                       23: 'StumpTriviaInABox',
                       45: 'BuzztimeLiveTrivia',
                       46: 'BuzztimeLivePoker',
                       47: 'BuzztimeLiveQuestionnairey',
                       48: 'BuzztimePrivateTrivia',
                       49: 'BzztimePrivatePoker',
                       50: 'BuzztimePrivateQuestionnairey',
                       52: 'StumpQuestionnaireyInABox' \
                       }

PLAYER_GROUP_TYPE = {'Buddy': 1,
                     'Denied': 2,
                     'Pending': 3,
                     'PlayerGroup': 4,
                     'Block': 5,
                     'BlockCommunities': 6,
                     'LeagueTeam': 7 \
                     }

PLAYER_GROUP_MEMBER_STATUS = {'All': 0,
                              'Active': 1,
                              'Requested': 2,
                              'Invited': 3 \
                              }

# Enrollment constraint types designate whether players may join a
# given group directly, or if they are required to go through an
# approval process by the group's owner
PLAYER_GROUP_ENROLLMENT_TYPE = ['Private',
                                'Public' \
                                ]

NAME_SPACE_TYPE = {'BuzztimeCom': 1,
                   'Facebook': 2,
                   'BuzztimePin': 3,
                   'Twitter': 4,
                   'MySpace': 5,
                   'EmailAddress': 6 \
                   }

ACHIEVEMENT_PLATFORM = {'BuzztimeNetwork': 1,
                        'Facebook': 2 \
                        }

BADGE_ICON_PATH = '/shared/images/badgeicons/'

GAME_ICON_PATH = '/shared/images/gameicons/'

PROFILE_API_ROOT = '/players/profile'

ENTITY_TYPE = dict(RegisteredPlayer=1,
                   Site=2,
                   AnonymousPlayer=3,
                   SiteEmployee=4,
                   UnregisteredUser=5,
                   EmailAddress=6 \
                   )

POINTS_AWARD_TYPE = dict(Game=1,
                         PlayerRewardCard=2,
                         ReferAFriend=3,
                         PPlusRegistration=4,
                         ThePulse=5,
                         Administrative=99 \
                         )

CONTACT_FORM_ADMIN_TEMPLATE = "A new entry has been added to {0} " \
                              "and contains the following data {1}"

BUZZTIME_TRIVIAWEB_NAME = 'Buzztime, Inc.'

CASINO_GAME_MAPPER = dict(holdem=dict(game_id=4, game_derivative_id=0),
                          blackjack=dict(game_id=2, game_derivative_id=0),
                          freezeout=dict(game_id=4, game_derivative_id=1) \
                          )

PLAYER_FIELDS_MAPPER = dict(NamespaceIdentity='Username',
                            EarnedCount='BadgeCount',
                            Address1='AddressLine1',
                            Address2='AddressLine2',
                            EntityID='PlayerID',
                            OrgName='HomeSiteName',
                            SiteID='HomeSiteID',
                            SiteState='HomeSiteState',
                            SiteCity='HomeSiteCity',
                            Points='PlayerPlusPoints',
                            NationalTop100='NationalTop100Count',
                            LocalWins='LocalWinsCount',
                            NationalBronze='NationalBronzeCount',
                            NationalGold='NationalGoldCount',
                            NationalSilver='NationalSilverCount',
                            TotalPoints='PlayerPlusPoints' \
                            )

PLAYERS_UTILS_CONSTANTS = dict(PLAYERS_LOGIN_URL='/players/login/',
                               PLAYERS_HASHED_LOGIN_URL='/players/hashed_login/',
                               PLAYERS_TABLET_LOGIN_URL='/players/tablet_login/',
                               SESSION_EXPIRY=9 * 60 * 60,
                               PLAYERS_PREFIX='/players/',
                               PLAYERS_EDIT_URL='/players/edit_user_info/',
                               AVATAR_URL='/players/avatar/index.php',
                               AVATAR_DELETE_URL='/players/avatar/',
                               GAMENAMEID_BUZZTIME_SPORTS=241 \
                               )

PLAYERS_AUTH_REQUIRED = [PLAYERS_UTILS_CONSTANTS['PLAYERS_EDIT_URL'],
                         PLAYERS_UTILS_CONSTANTS['AVATAR_URL'],
                         PLAYERS_UTILS_CONSTANTS['AVATAR_DELETE_URL']
                         ]

PLAYERS_LOGIN_URL = [PLAYERS_UTILS_CONSTANTS['PLAYERS_LOGIN_URL'],
                     PLAYERS_UTILS_CONSTANTS['PLAYERS_HASHED_LOGIN_URL']
                     ]

FORM_NAME_MAPPER = dict(site_issues="Site (Bar/Restaurant) Issues",
                        reg_login="Registration and Login",
                        my_playerplus_acc="My PlayerPlus Account",
                        website_issue="Web Site Issues",
                        give_feedback="Give Feedback",
                        promo_contests="Promotions & Contests",
                        mobile_playmaker="Mobile Playmaker",
                        fb_trivia_game="Facebook Trivia Game",
                        incorrect_trivia="Incorrect Trivia Content",
                        suggest_location="Suggest a location", \
                        )

BRONTO_WSDL = 'https://api.bronto.com/v4?wsdl'

EXCLUDE_DATA_FROM_EMAIL = ["admin_email", "sender_email", "user_template_id", "admin_template_id"]

SMARTESTBAR_URL = '/players/smartestbar/'

PREMIUMGAMEID_QB1 = '14'

DEFAULT_UPPER_LIMIT_FOR_COORDS = 305
DEFAULT_LOWER_LIMIT_FOR_COORDS = 32

AVATAR_XML_CSV_PATH = CHEERIOS_DIR+'/players/avatar_csv/test-2-05-avatars.csv'

AVATAR_JSON_DEFAULT_VALUES = dict(randomizeSlider='true',
                                  loading='false',
                                  skin='true',
                                  hair='true',
                                  eye='true',
                                  marker='true',
                                  key=2,
                                  imageLoading='false',
                                  disableTab='false',
                                  tab=1, section='',
                                  selectedTab='SEX'
                                  )
