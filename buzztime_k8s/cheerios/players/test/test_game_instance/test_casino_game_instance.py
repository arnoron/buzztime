"""
Holds the test cases for the casino game instance class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.game_instance.casino_game_instance import CasinoGameInstance
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestCasinoGameInstance(APITestCase):
    """
    Tests the casino game instance module
    """

    def setUp(self):
        """
        Initializes the casino game instance object
        :return:
        """
        self.casino_game_instance_interface = CasinoGameInstance()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_casino_valid(self, cursor):
        """
        Tests the casino game instance with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['casino_data'])
        player_id = 220404
        game = 'holdem'

        response = self.casino_game_instance_interface.get_casino_game_instances(player_id, game)
        self.assertEqual(response.status_code, 200)

    def test_casino_invalid_game(self):
        """
        Tests the casino game instance with invalid game
        :return:
        """
        player_id = 220404
        game = 'xyz'

        response = self.casino_game_instance_interface.get_casino_game_instances(player_id, game)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid game parameter passed. Can be either 'holdem',"
                                        " 'blackjack' or 'freezeout'")

    def test_casino_player_null(self):
        """
        Tests the casino game instance with null player id
        :return:
        """
        player_id = None
        game = 'holdem'

        response = self.casino_game_instance_interface.get_casino_game_instances(player_id, game)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.get('Error'), 'Empty parameters passed')

    def test_casino_invalid_player(self):
        """
        Tests the casino game instance with invalid player id
        :return:
        """
        player_id = '%$&'
        game = 'holdem'

        response = self.casino_game_instance_interface.get_casino_game_instances(player_id, game)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.get('Error'), 'Empty parameters passed')
