"""
Mock Responses
"""
import datetime

MOCK_RESPONSE = dict(
    test_recent_game_valid=[
        {
            'State': 'TX',
            'GameFileName': 'trendalicious-trivia',
            'GameNameId': 152,
            'LocalRank': 2,
            'PlayerId': 221641,
            'Game': 'Trendalicious Trivia',
            'NationalRank': 1808,
            'OrgName': 'Coaches Pub',
            'SiteId': 35510,
            'GameStartTime': datetime.datetime(2018, 4, 6, 18, 30),
            'NationalCount': 2174,
            'City': 'Houston',
            'ResponseCount': 10,
            'Score': 2567,
            'LocalCount': 2
        }
    ],
    test_site_game_valid=[
        {
            'GameFileName': 'topix',
            'TotalScore': 802902,
            'PlayerGamesAverage': 6921,
            'SiteID': 1110,
            'GameStartTime': datetime.datetime(2018, 4, 6, 17, 0),
            'Game': 'Topix',
            'PlayerGames': 116,
            'GameNameID': 137
        }
    ],
    casino_data=[],
    game_instance=[],

)
