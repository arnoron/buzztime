"""
Holds the test cases for the site game instance class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.game_instance.site_game_instance import SiteGameInstance
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestSiteGameInstance(APITestCase):
    """
    Tests the site game instance module
    """

    def setUp(self):
        """
        Initializes the site game instance interface
        :return:
        """
        self.site_game_instance_interface = SiteGameInstance()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_site_game_valid(self, cursor):
        """
        Tests the site game instance with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_site_game_valid'])
        site_id = 1110
        game_id = 137

        response = self.site_game_instance_interface.get_site_game_instance(site_id, game_id)
        self.assertEqual(response.status_code, 200)

    def test_site_game_invalid_site(self):
        """
        Tests the site game instance with invalid site id
        :return:
        """
        site_id = '$%^'
        game_id = 25

        response = self.site_game_instance_interface.get_site_game_instance(site_id, game_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed for game_id, site_id")

    def test_site_game_invalid_game(self):
        """
        Tests the site game instance with invalid game id
        :return:
        """
        site_id = 10
        game_id = '*&^'

        response = self.site_game_instance_interface.get_site_game_instance(site_id, game_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed for game_id, site_id")
