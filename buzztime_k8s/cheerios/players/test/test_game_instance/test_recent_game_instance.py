"""
Holds the test cases for the recent game instance class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.game_instance.recent_game_instance import RecentGameInstance
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestRecentGameInstance(APITestCase):
    """
    Tests the recent game instance module
    """

    def setUp(self):
        """
        Initializes the recent game instance object
        :return:
        """
        self.recent_game_instance_interface = RecentGameInstance()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_recent_game_valid(self, cursor):
        """
        Tests the recent game instance with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_recent_game_valid'])
        player_id = 221641

        response = self.recent_game_instance_interface.get_recent_game_instances(player_id)
        self.assertEqual(response.status_code, 200)

    def test_recent_game_player_null(self):
        """
        Tests the recent game instance with null player id
        :return:
        """
        player_id = None

        response = self.recent_game_instance_interface.get_recent_game_instances(player_id)
        self.assertEqual(response.data, 'Invalid parameters passed')
        self.assertEqual(response.status_code, 400)

    def test_recent_game_invalid_player(self):
        """
        Tests the recent game instance with invalid player id
        :return:
        """
        player_id = '%$*'

        response = self.recent_game_instance_interface.get_recent_game_instances(player_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, 'Invalid parameters passed')

