"""
Holds the test cases for the game instance details class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.game_instance.game_instance_details import GameInstanceDetails
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestGameInstanceDetails(APITestCase):
    """
    Tests the game instance details module
    """

    def setUp(self):
        """
        Initializes the game instance details object
        :return:
        """
        self.game_instance_details_interface = GameInstanceDetails()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_game_instance_valid(self, cursor):
        """
        Tests the game instance details with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['game_instance'])
        player_id = 39089
        game_id = 135
        mode = 'average'

        response = self.game_instance_details_interface.\
            get_game_instance_details(player_id, game_id, mode)
        self.assertEqual(response.status_code, 200)

    def test_game_instance_player_null(self):
        """
        Tests the game instance details with null player id
        :return:
        """
        player_id = None
        game_id = 50
        mode = 'average'

        response = self.game_instance_details_interface.\
            get_game_instance_details(player_id, game_id, mode)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Parameters passed for player_id,game_id,mode")

    def test_game_instance_game_id_null(self):
        """
        Tests the game instance details with null game id
        :return:
        """
        player_id = 25
        game_id = None
        mode = 'average'

        response = self.game_instance_details_interface.\
            get_game_instance_details(player_id, game_id, mode)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Parameters passed for player_id,game_id,mode")


    def test_game_invalid_player(self):
        """
        Tests the game instance details with invalid player id
        :return:
        """
        player_id = '$%*'
        game_id = 50
        mode = 'average'

        response = self.game_instance_details_interface. \
            get_game_instance_details(player_id, game_id, mode)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Parameters passed for player_id,game_id,mode")

    def test_game_invalid_game(self):
        """
        Tests the game instance details with invalid game id
        :return:
        """
        player_id = 25
        game_id = '%$&'
        mode = 'average'

        response = self.game_instance_details_interface. \
            get_game_instance_details(player_id, game_id, mode)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Parameters passed for player_id,game_id,mode")

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_game_instance_mode_null(self, cursor):
        """
        Tests the game instance details with mode as null
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['game_instance'])
        player_id = 39089
        game_id = 135
        mode = None

        response = self.game_instance_details_interface.\
            get_game_instance_details(player_id, game_id, mode)
        self.assertEqual(response.status_code, 200)
