"""
Tests the avatar module
"""
from mock import patch, Mock
from .response_data import MOCK_RESPONSE
from rest_framework.test import APITestCase

from cheerios.players.test.base import login
from cheerios.players.test.test_data import response_headers, response_status
from django.http.cookie import SimpleCookie
from cheerios.test_utils import mock_test_utils

API_ENDPOINT = '/players/avatar/'


class TestAvatarViewSet(APITestCase):
    """
    Test cases for Avatar viewset
    """

    def setUp(self):
        """
        Logs the user in
        :return:
        """
        self.session_id = "test-session-id"

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['fetch'])))
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch('cheerios.services.redis.RedisSession.get_key', Mock())
    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test-session-id", b"test-session-id"]))
    def test_get_avatar(self):
        """
        Tests the get avatar module
        :return:
        """
        self.client.cookies = SimpleCookie({'player_id': '207344'})
        response = self.client.get(API_ENDPOINT + "?player_id=207344", HTTP_AUTHORIZATION=self.session_id)
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['fetch'])))
    @patch('cheerios.services.redis.RedisSession.get_key', Mock())
    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test-session-id", b"test-session-id"]))
    def test_get_avatar_empty_avatar(self):
        """
        Tests the get avatar when there is no avatar
        :return:
        """
        self.client.cookies = SimpleCookie({'player_id': '207344'})
        response = self.client.get(API_ENDPOINT + "?player_id=207344", HTTP_AUTHORIZATION=self.session_id)
        self.assertEqual(response_status(response), 200)
