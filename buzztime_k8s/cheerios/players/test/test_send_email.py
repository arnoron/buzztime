"""
Tests for the send mail viewset
"""
import mock
from mock import patch, Mock
from rest_framework.test import APITestCase
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils
from cheerios.constant.constant import add_delivery_url
from cheerios.services.Email.email import Email


def mocked_add_delivery(add_delivery):
    """

    :return:
    """
    delivery = { \
                'results': [{'isError': False}] \
               }

    return delivery

def mocked_unsuccessful_delivery(add_delivery):
    """

    :return:
    """
    delivery = { \
                'results': [{'isError': True, 'errorString': 'Email not sent'}] \
               }

    return delivery


class TestSendEmailViewSet(APITestCase):
    """
    Tests for send email viewset
    """

    def setUp(self):
        """
        Function to initialize class scope variables
        """
        self.send_mail_endpoint = '/players/email/'

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    @mock.patch(add_delivery_url, side_effect=mocked_add_delivery)
    def test_send_email(self, mocked_add_delivery):
        """
        Tests the successful email sending
        :return:
        """

        recipient_details = dict(username='test_username',
                                 email='test_email@gmail.com',
                                 display_name='test_display_name',
                                )

        sender_details = dict(name='test_sender_name',
                              email='test_sender_email@gmail.com')

        email_interface = Email(recipient_details, sender_details, message_id='12345')
        self.assertEqual(email_interface.send(), True)

    def get_response_data(self, data):
        """
        Does a post API call and returns the data
        :param credentials:
        :return:
        """
        return self.client.post(self.send_mail_endpoint, data, format='json')

    def test_contact_with_empty_data(self):
        """
        Tests contact form mail with no data
        :return:
        """
        request_data = dict(form='site_issues')

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.data, "Incorrect response data passed. Please check.")
        self.assertEqual(response_data.status_code, 400)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    def test_forgot_pw_mail_empty_data(self):
        """
        Tests forgot password mail with no data
        :return:
        """
        request_data = dict()

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.data, "Incorrect response data passed. Please check.")
        self.assertEqual(response_data.status_code, 400)

    def test_contact_with_empty_email(self):
        """
        Tests contact mail with no email
        :return:
        """
        request_data = dict(form='site_issues',
                            data_to_post=dict(username="John")
                           )

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.data, "Please add the user template id in the body")
        self.assertEqual(response_data.status_code, 400)

    def test_forgot_pw_mail_empty_domain(self):
        """
        Tests forgot password mail with no domain
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", user_email="test-user@test.com",
                                              sender_email="test-sender@test.com"))

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add domain name in the body")

    def test_mail_empty_user_email(self):
        """
        Tests forgot password mail with no user email
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", \
            domain="test-domain.com", sender_email="test-sender@test.com"))

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the user email in the body")

    def test_mail_empty_sender_email(self):
        """
        Tests forgot password mail with no admin email
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", \
            user_email="test-user@test.com", domain="test-domain.com"))

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the sender email in the body")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(None)))
    def test_email_id_not_found(self):
        """
        Tests when the passed email id is not in the db
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", user_email="test-user@test.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com",
                                              user_template_id="12345"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Email ID not found in the database")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    @mock.patch(add_delivery_url, side_effect=mocked_add_delivery)
    def test_forgot_password_mail(self, mocked_add_delivery):
        """
        Tests forgot password mail success
        :return:
        """
        request_data = dict(data_to_post=dict(username="Masroor",
                                              user_email="masroor.h@hashedin.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com",
                                              user_template_id="12345"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 200)
        self.assertEqual(response_data.data, "Email sent successfully")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    @mock.patch(add_delivery_url, side_effect=mocked_add_delivery)
    def test_contact_us_mail_success(self, mocked_add_delivery):
        """
        Tests forgot password mail success
        :return:
        """
        request_data = dict(form="promo_contests",
                            data_to_post=
                            dict(user_email="reet.shrivastava@hashedin.com",
                                 username="hasher",
                                 phone_no="6757483",
                                 admin_email="shrivastavareet@gmail.com",
                                 sender_email="publish@buzztime.com",
                                 user_template_id="12345",
                                 admin_template_id="54321"
                                )
                           )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 200)
        self.assertEqual(response_data.data, "Email sent successfully")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    @mock.patch(add_delivery_url, side_effect=mocked_unsuccessful_delivery)
    def test_forgot_password_failed(self, mocked_add_unsuccessful_delivery):
        """
        Tests forgot password mail success
        :return:
        """
        request_data = dict(data_to_post=dict(username="Masroor",
                                              user_email="masroor.h@hashedin.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com",
                                              user_template_id="12345"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 500)
        self.assertEqual(response_data.data, "Email to user not sent successfully")

    def test_invalid_form_type(self):
        """
        Tests invalid form type
        :return:
        """
        request_data = dict(form="xyz",
                            data_to_post=
                            dict(user_email="reet.shrivastava@hashedin.com",
                                 username="hasher",
                                 phone_no="6757483",
                                 admin_email="shrivastavareet@gmail.com",
                                 sender_email="publish@buzztime.com",
                                 user_template_id="12345",
                                 admin_template_id="54321"
                                ) \
                            )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Invalid form type passed")

    def test_contact_empty_admin_email(self):
        """
        Tests the contact form with no admin email
        :return:
        """
        request_data = dict(form="promo_contests",
                            data_to_post=
                            dict(user_email="reet.shrivastava@hashedin.com",
                                 username="hasher",
                                 phone_no="6757483",
                                 sender_email="publish@buzztime.com",
                                 user_template_id="12345",
                                 admin_template_id="54321"
                                ) \
                            )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the admin email in the body")

    def test_contact_empty_sender_email(self):
        """
        Tests the contact form with no sender email
        :return:
        """
        request_data = dict(form="promo_contests",
                            data_to_post=
                            dict(user_email="reet.shrivastava@hashedin.com",
                                 username="hasher",
                                 phone_no="6757483",
                                 admin_email="shrivastavareet@gmail.com",
                                 user_template_id="12345",
                                 admin_template_id="54321"
                                ) \
                            )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the sender email in the body")

    def test_contact_empty_user_email(self):
        """
        Tests the contact form with no user email
        :return:
        """
        request_data = dict(form="promo_contests",
                            data_to_post=
                            dict(username="hasher",
                                 phone_no="6757483",
                                 admin_email="shrivastavareet@gmail.com",
                                 user_template_id="12345",
                                 admin_template_id="54321",
                                 sender_email="publish@buzztime.com",
                                ) \
                            )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the user email in the body")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    @mock.patch(add_delivery_url, side_effect=mocked_unsuccessful_delivery)
    def test_contact_us_mail_failed(self, mocked_add_unsuccessful_delivery):
        """
        Tests forgot password mail success
        :return:
        """
        request_data = dict(form="promo_contests",
                            data_to_post=
                            dict(user_email="reet.shrivastava@hashedin.com",
                                 username="hasher",
                                 phone_no="6757483",
                                 admin_email="shrivastavareet@gmail.com",
                                 sender_email="publish@buzztime.com",
                                 user_template_id="12345",
                                 admin_template_id="54321"
                                ) \
                            )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 500)
        self.assertEqual(response_data.data, "Email to admin not sent successfully")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    def test_forgot_pw_empty_template_id(self):
        """
        Tests forgot password mail with no template id
        :return:
        """
        request_data = dict(data_to_post=dict(username="Masroor",
                                              user_email="masroor.h@hashedin.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the template id in the body")

    def test_contact_mail_empty_user(self):
        """
        Tests contact us mail with no user template id
        :return:
        """
        request_data = dict(form="promo_contests",
                            data_to_post=
                            dict(user_email="reet.shrivastava@hashedin.com",
                                 username="hasher",
                                 phone_no="6757483",
                                 admin_email="shrivastavareet@gmail.com",
                                 sender_email="publish@buzztime.com",
                                 admin_template_id="54321"
                                ) \
                            )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the user template id in the body")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['test_forgot_password_mail'])))
    def test_contact_mail_empty_admin(self):
        """
        Tests contact us mail with no admin template id
        :return:
        """
        request_data = dict(form="promo_contests",
                            data_to_post=
                            dict(user_email="reet.shrivastava@hashedin.com",
                                 username="hasher",
                                 phone_no="6757483",
                                 admin_email="shrivastavareet@gmail.com",
                                 sender_email="publish@buzztime.com",
                                 user_template_id="12345"
                                ) \
                            )
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the admin template id in the body")
