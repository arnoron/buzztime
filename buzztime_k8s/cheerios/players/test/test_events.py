"""
Test cases for events page api
"""
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import response_status, \
    get_sites_data, get_events_unprocessed_data
from cheerios.players.views.events import EventsViewSet
from mock import patch, Mock
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils

URL = dict(events='/players/events/sitefinder/')


class TestEventsViewSet(APITestCase):
    """
    Tests the Trivia Games Viewset APIs
    """
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['test_events'])))
    def test_events(self):
        """
        Tests the events api without any query params
        :return:
        """
        response = self.client.get(URL['events'])
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['test_events'])))
    def test_events_with_lat_and_lng(self):
        """
        Tests the events api with latitude and longitude params
        :return:
        """
        response = self.client.get('{0}{1}'.format(URL['events'], '?latitude=33.15&'
                                                                  'longitude=-117.35'))
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['test_events'])))
    def test_events_count_index(self):
        """
        Tests the events api with latitude and longitude params
        :return:
        """
        response = self.client.get('{0}{1}'.format(URL['events'], '?count=10&start_index=21'))
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    def test_format_response_data(self):
        """
        Tests the format_response_data method
        :return:
        """
        events_object = EventsViewSet()
        formatted_response = events_object.format_response_data(get_sites_data())
        self.assertEqual(formatted_response[0]['Location']['Name'],
                         get_sites_data()[0]['OrgName'])

    def test_post_process_data_tablet(self):
        """
        Tests the post process data method
        :return:
        """
        events_object = EventsViewSet()
        events_object.count = 2
        events_object.return_type = 'tablet'
        events_object.start_index = 1
        processed_data = events_object.post_process_data(get_events_unprocessed_data())
        self.assertEqual(len(processed_data), 1)
        self.assertEqual(processed_data[0]['Distance'], 100)

    def test_post_process_data_classic(self):
        """
        Tests the post process data method
        :return:
        """
        events_object = EventsViewSet()
        events_object.count = 2
        events_object.return_type = 'classic'
        events_object.start_index = 1
        processed_data = events_object.post_process_data(get_events_unprocessed_data())
        self.assertEqual(len(processed_data), 0)

    def test_post_process_data_jackpot(self):
        """
        Tests the post process data method
        :return:
        """
        events_object = EventsViewSet()
        events_object.count = 2
        events_object.return_type = 'jackpot'
        events_object.start_index = 1
        processed_data = events_object.post_process_data(get_events_unprocessed_data())
        self.assertEqual(len(processed_data), 1)
        self.assertEqual(processed_data[0]['Distance'], 100)

    def test_post_process_data_all(self):
        """
        Tests the post process data method
        :return:
        """
        events_object = EventsViewSet()
        events_object.count = 2
        events_object.return_type = 'all'
        events_object.start_index = 1
        processed_data = events_object.post_process_data(get_events_unprocessed_data())
        self.assertEqual(len(processed_data), 1)
        self.assertEqual(processed_data[0]['Distance'], 100)

    def test_get_events_type(self):
        """
        Tests the get events type method
        :return:
        """
        events_object = EventsViewSet()
        self.assertEqual(events_object.get_event_type(45), 'BuzztimeLiveTrivia')
        self.assertEqual(events_object.get_event_type(100), 'Special Event')
