"""
Holds the integration test cases for the sites API
"""
import json

from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import response_data, response_status
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import  mock_test_utils


API_ENDPOINT = '/players/sites/'

class TestSite(APITestCase):
    """
    Tests the site API
    """
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['site_name_data'],
               MOCK_RESPONSE['geo_site_data'])))
    def test_site_search(self):
        """
        Tests site search
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT,
                                   '?search=mexico&latitude=23.634501&'
                                   'longitude=-102.55278399999997'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('SiteListName' in json_response_data)
        self.assertTrue('SiteGeoCode' in json_response_data['SiteListName'])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['test_site_num_gold_sort']]))
    def test_site_num_gold_sort(self):
        """
        Tests the site with num_gold sort param
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?sort=num-gold-desc&state=FL'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('SiteID' in json_response_data[0])
        self.assertTrue('OrgName' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    def test_site_invalid_sort_criteria(self):
        """
        Tests the site with invalid sort param
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?sort=xyz'))
        self.assertEqual(response_status(response), 400)
        self.assertEqual(response.data, "Invalid Sorting Criteria passed")

    def test_site_invalid_query_param(self):
        """
        Tests the site with invalid query param
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?abc=xyz'))
        self.assertEqual(response.data, "Invalid query parameter passed")
        self.assertEqual(response_status(response), 400)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['participating_loc'],
               MOCK_RESPONSE['leader'],
               MOCK_RESPONSE['gameplay'])))
    def test_site_competition_details(self):
        """
        Tests the site competition details
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '1892/competitions/9410'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('competition_gameplay' in json_response_data)
        self.assertTrue('participating_locations' in json_response_data)
        self.assertTrue('competition_leaders' in json_response_data)
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['test_site_competition_list']]))
    def test_site_competition_list(self):
        """
        Tests the site competition list
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '1892/competitions/'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('GameFileName' in json_response_data[0])
        self.assertTrue('OrgName' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['test_site_game_instances']]))
    def test_site_game_instances(self):
        """
        Tests the site game instances
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '9881/games/136/instances/'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('Game' in json_response_data[0])
        self.assertTrue('GameStartTime' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['test_site_game_players']]))
    def test_site_game_players(self):
        """
        Tests the site game players
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '9881/games/136/players'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('Game' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    def test_site_incorrect_subtype(self):
        """
        Tests the site with incorrect game subtype
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '9881/games/136/xyz'))
        self.assertEqual(response.data, "Incorrect games subtype passed")
        self.assertEqual(response_status(response), 400)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['test_site_games']]))
    def test_site_games(self):
        """
        Tests the site games
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '9881/games/'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('Game' in json_response_data[0])
        self.assertTrue('SiteID' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['test_site_game_instance_players']]))
    def test_site_game_instance_players(self):
        """
        Tests the site game instance players
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '1110/'
                                                  'games/133/players?start-date=07-04-2018'
                                                  '&start-time=17:30'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Game' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['test_site_players']]))
    def test_site_players(self):
        """
        Tests the site players
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '13972/players'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('OrgName' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    def test_site_invalid_subtype(self):
        """
        Tests the site with invalid player info subtype
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, 'xyz'))
        self.assertEqual(response.data, "Invalid query parameter passed")
        self.assertEqual(response_status(response), 400)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['site_details']]))
    def test_site_details(self):
        """
        Tests the site details
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '13972'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('SiteName' in json_response_data)
        self.assertTrue('Id' in json_response_data)
        self.assertEqual(response_status(response), 200)
