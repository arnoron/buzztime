"""
Mock Responses
"""
import datetime
from decimal import Decimal
from uuid import UUID

MOCK_RESPONSE = dict(
    test_site_scores_game_id=[
        {
            'RowID': 1,
            'SiteName': 'BWW QA STG #1110',
            'GameDerivativeID': 58,
            'City': 'CARLSBAD',
            'State': 'CA',
            'Rank': 1,
            'SiteID': 1110,
            'Score': 29211,
            'RecCount': 978
        }
    ],
    test_site_scores_loc_params=[
        {
            'GameDerivativeID': 58,
            'RecCount': 5,
            'Score': 4518,
            'RowID': 1,
            'City': 'Dothan',
            'SiteName': "Uncle Bob's LLC",
            'SiteID': 38954,
            'Rank': 379,
            'State': 'AL'
        }
    ],
    handle_site_search={
        'SiteListName':
            {
                'SiteGeoCode': [
                    {
                        'Latitude': Decimal('44.994369'),
                        'TabletSite': True,
                        'ReviewCount': 5,
                        'Location':
                            {
                                'Address':
                                    {
                                        'State':
                                            {
                                                'Name': 'MN'
                                            },
                                        'StreetAddress1': '1754 N Lexington',
                                        'Country':
                                            {
                                                'Name': None
                                            },
                                        'City':
                                            {
                                                'Name': 'Roseville'
                                            },
                                        'Zipcode':
                                            {
                                                'ZipCodeVAlue': '55113'
                                            },
                                        'StreetAddress2': None
                                    },
                                'Id': 1488,
                                'Name': "Ol' Mexico"
                            },
                        'Phone': '(651) 487-2847',
                        'PowerPage':
                            {
                                'OptFullBar': True,
                                'PowerPageID': 3989,
                                'OptFullMenu': True,
                                'Image3': None,
                                'Image2': None,
                                'OptLiveMusic': False,
                                'OptFreeWifi': True,
                                'Description': "Welcome to Ol' Mexico   Welcome to Ol' Mexico! "
                                               "Come discover the many foods of Ol' Mexico and "
                                               "enjoy the BEST MARGARITAS in town! Celebrating "
                                               "Over 30 Years of fine Authentic Mexican Cuisine "
                                               "Happy hour specials from 3pm and 6pm with 2 for 1"
                                               " on drinks and $2.00 off appetizers. Late night "
                                               "happy hour 9pm-midnight. Ol' Mexico offers free "
                                               "Wi-Fi and Comedy Improv on Saturday nights at "
                                               "7:30.  Join us!",
                                'Logo': '1488_logo.png',
                                'Image1': None,
                                'URL': 'http://www.olmexico.com/ordereze/1000/Page.aspx',
                                'OptKidFriendly': True,
                                'Opt21AndOver': False
                            },
                        'RowNum': 1,
                        'Longitude': Decimal('-93.146205'),
                        'Id': 1488,
                        'Chain': 'NONE',
                        'distance': 0.0
                    }
                ]
            },
        'SiteListNear':
            {
                'SiteGeoCode': [
                    {
                        'PowerPage':
                            {
                                'OptFullMenu': True,
                                'Description': 'The Buffalo Wild Wings restaurants'
                                               ' features a variety of boldly flavored,'
                                               ' made-to-order menu items including its '
                                               'namesake Buffalo, New York-style chicken wings. '
                                               'The Buffalo Wild Wings menu specializes in '
                                               'mouth-watering signature sauces and seasonings '
                                               'with flavors ranging from Sweet BBQ to Blazin\'. '
                                               'Guests enjoy a welcoming neighborhood atmosphere '
                                               'that includes an extensive multi-media system for '
                                               'watching their favorite sporting events. Buffalo '
                                               'Wild Wings is the recipient of hundreds of "Best '
                                               'Wings" and "Best Sports Bar" awards across '
                                               'the country.',
                                'Logo': '39733_logo.png',
                                'Image1': None,
                                'OptFreeWifi': True,
                                'PowerPageID': 6778,
                                'Opt21AndOver': False,
                                'OptLiveMusic': False,
                                'Image3': None,
                                'Image2': None,
                                'OptKidFriendly': True,
                                'OptFullBar': True,
                                'URL': 'http://www.buffalowildwings.com'
                            },
                        'Location':
                            {
                                'Address':
                                    {
                                        'City':
                                            {
                                                'Name': 'Saltillo'
                                            },
                                        'Country':
                                            {
                                                'Name': None
                                            },
                                        'StreetAddress1': 'Boulevard Galerias Saltillo # 375-2',
                                        'Zipcode': {
                                            'ZipCodeVAlue': '25240'
                                        },
                                        'StreetAddress2': None,
                                        'State':
                                            {
                                                'Name': 'Coahuila'
                                            }
                                    },
                                'Name': 'Buffalo Wild Wings Saltillo',
                                'Id': 39733
                            },
                        'ReviewCount': None, 'Id': 39733,
                        'Longitude': Decimal('-100.982107'),
                        'Phone': '01152 -844-3050-992',
                        'RowNum': 1,
                        'Latitude': Decimal('25.455461'),
                        'distance': 160.09476057019856,
                        'Chain': 'Bufwldwngs',
                        'TabletSite': True
                    }
                ]
            }
    },
    test_site_num_gold_sort=[
        {
            'State': 'FL',
            'TriviaRank': 1,
            'SiteID': 9881,
            'TotalScore': 189099362,
            'Top100': 8728,
            'Gold': 1715,
            'City': 'Pompano Beach',
            'OrgName': "Bru's Room Pompano Beach",
            'Bronze': 22,
            'Silver': 23
        }
    ],
    participating_loc=[
        {
            'CompetitionXML': "<competitions><competition><CompetitionID>9410</CompetitionID>"
                              "<Name>Tuesday Winter Trivia League</Name><CompetitionTypeID>2"
                              "</CompetitionTypeID><StartDate>2016-12-05T00:00:00</StartDate>"
                              "<EndDate>2016-12-28T00:00:00</EndDate><GameNameID>29</GameNameID>"
                              "<Game>Showdown</Game><GameFileName>showdown</GameFileName>"
                              "<CompetitionStatus>Deleted</CompetitionStatus><competitiongames>"
                              "<competitiongame><PrimetimeScheduleID>509</PrimetimeScheduleID>"
                              "<GameDate>2016-12-06T17:30:00</GameDate><GameNameID>29</GameNameID>"
                              "<Game>Showdown</Game><TimePacific>1899-12-30T17:30:00</TimePacific>"
                              "<TimeMountain>1899-12-30T18:30:00</TimeMountain><TimeCentral>"
                              "1899-12-30T19:30:00</TimeCentral><TimeEastern>1899-12-30T20:30:00"
                              "</TimeEastern></competitiongame><competitiongame>"
                              "<PrimetimeScheduleID>509</PrimetimeScheduleID><GameDate>2016-12-13T"
                              "17:30:00</GameDate><GameNameID>29</GameNameID><Game>Showdown</Game>"
                              "<TimePacific>1899-12-30T17:30:00</TimePacific><TimeMountain>1899-12"
                              "-30T18:30:00</TimeMountain><TimeCentral>1899-12-30T19:30:00</TimeCe"
                              "ntral><TimeEastern>1899-12-30T20:30:00</TimeEastern></competitionga"
                              "me><competitiongame><PrimetimeScheduleID>509</PrimetimeScheduleID>"
                              "<GameDate>2016-12-20T17:30:00</GameDate><GameNameID>29</GameNameID>"
                              "<Game>Showdown</Game><TimePacific>1899-12-30T17:30:00</TimePacific>"
                              "<TimeMountain>1899-12-30T18:30:00</TimeMountain><TimeCentral>1899-"
                              "12-30T19:30:00</TimeCentral><TimeEastern>1899-12-30T20:30:00</Time"
                              "Eastern></competitiongame><competitiongame><PrimetimeScheduleID>509"
                              "</PrimetimeScheduleID><GameDate>2016-12-27T17:30:00</GameDate><Game"
                              "NameID>29</GameNameID><Game>Showdown</Game><TimePacific>1899-12-30T"
                              "17:30:00</TimePacific><TimeMountain>1899-12-30T18:30:00</TimeMounta"
                              "in><TimeCentral>1899-12-30T19:30:00</TimeCentral><TimeEastern>1899-"
                              "12-30T20:30:00</TimeEastern></competitiongame></competitiongames>"
                              "<competitionsites><competitionsite><SiteID>288</SiteID><OrgName>Za"
                              "ck's Place</OrgName><Chain>NONE</Chain><City>Little Rock</City>"
                              "<State>AR</State><TimeZone>2</TimeZone><AllowSignage>0</AllowSignage"
                              "></competitionsite><competitionsite><SiteID>17490</SiteID>"
                              "<OrgName>Guido's Nickel</OrgName><Chain>NONE</Chain><City>Lakewood"
                              "</City><State>CO</State><TimeZone>1</TimeZone><AllowSignage>0"
                              "</AllowSignage></competitionsite><competitionsite><SiteID>689"
                              "</SiteID><OrgName>Teasers</OrgName><Chain>NONE</Chain><City>Chicago"
                              "</City><State>IL</State><TimeZone>2</TimeZone><AllowSignage>0"
                              "</AllowSignage></competitionsite><competitionsite><SiteID>1892"
                              "</SiteID><OrgName>Heroes Sports Bar &amp; Grill</OrgName><Chain/>"
                              "<City>Wichita</City><State>KS</State><TimeZone>2</TimeZone>"
                              "<AllowSignage>0</AllowSignage></competitionsite><competitionsite>"
                              "<SiteID>39454</SiteID><OrgName>T's Rockin Roadhouse</OrgName>"
                              "<Chain/><City>Columbus</City><State>MI</State><TimeZone>3"
                              "</TimeZone><AllowSignage>1</AllowSignage></competitionsite>"
                              "<competitionsite><SiteID>14829</SiteID><OrgName>Tailgate Sports "
                              "Cafe</OrgName><Chain>NONE</Chain><City>Minneapolis</City><State>"
                              "MN</State><TimeZone>2</TimeZone><AllowSignage>0</AllowSignage>"
                              "</competitionsite><competitionsite><SiteID>40124</SiteID>"
                              "<OrgName>Granby Station Pub</OrgName><Chain/><City>Granby</City>"
                              "<State>MO</State><TimeZone>2</TimeZone><AllowSignage>1"
                              "</AllowSignage></competitionsite><competitionsite><SiteID>"
                              "39769</SiteID><OrgName>Dog Prairie Tavern</OrgName><Chain/>"
                              "<City>St Paul</City><State>MO</State><TimeZone>2</TimeZone>"
                              "<AllowSignage>1</AllowSignage></competitionsite><competitionsite>"
                              "<SiteID>40185</SiteID><OrgName>The Huff Sports Bar &amp; Grill"
                              "</OrgName><Chain/><City>Waynesboro</City><State>MS</State>"
                              "<TimeZone>2</TimeZone><AllowSignage>1</AllowSignage></competiti"
                              "onsite><competitionsite><SiteID>36870</SiteID><OrgName>Rudino's "
                              "Sports Bar</OrgName><Chain/><City>Cincinnati</City><State>OH"
                              "</State><TimeZone>3</TimeZone><AllowSignage>1</AllowSignage>"
                              "</competitionsite><competitionsite><SiteID>33651</SiteID><OrgName>"
                              "Elsa's On the Border</OrgName><Chain>NONE</Chain><City>Dayton"
                              "</City><State>OH</State><TimeZone>3</TimeZone><AllowSignage>0"
                              "</AllowSignage></competitionsite></competitionsites></competit"
                              "ion></competitions>"
        }
    ],
    leader=[],
    gameplay=[
        {
            'CompetitionID': 9410,
            'GameNameID': 29,
            'Game': 'Showdown',
            'NetworkGameID': 0,
            'CategoryID': 1,
            'GameDate': datetime.datetime(2016, 12, 6, 17, 30),
            'Name': 'Tuesday Winter Trivia League',
            'CompetitionGameID': 53798,
            'Channel': 0,
            'TotalPlayers': 66
        }
    ],
    test_site_competition_list=[
        {
            'OrgName': 'Heroes Sports Bar & Grill',
            'GameEngineID': None,
            'GameTypeID': 1,
            'CreatedDate': datetime.datetime(2017, 4, 3, 12, 26, 39, 207000),
            'QualificationMessage': None,
            'RequiredAge': 0,
            'State': 'KS',
            'BestOfNum': 0,
            'GSTMultiplier': Decimal('1.50'),
            'GroupID': None,
            'Day2': None,
            'MinPoints': 0,
            'CategoryID': 1,
            'EndDate': datetime.datetime(2017, 4, 27, 0, 0),
            'Name': 'TuesdayTrivia League - B Division',
            'GameStatusID': 1,
            'RankingTop': 0,
            'GTMultiplier': Decimal('0.50'),
            'CompetitionStatusID': 4,
            'PromotionName': 'Customer Competitions',
            'GameNameID': 29,
            'AutoProcess': True,
            'ProductionDate': datetime.datetime(2007, 4, 22, 0, 0),
            'GameSubType': 'Showdown',
            'QualifyingResponseCount': 9,
            'DaysToOfficial': 3,
            'GameStyleID': None,
            'CompetitionTypeID': 2,
            'MaximumScore': 63750,
            'GameType': 'Trivia',
            'GSTDurationMultiplier': Decimal('38.46'),
            'CompetitionClassID': 1,
            'UpdateDate': datetime.datetime(2017, 4, 3, 12, 26, 39, 207000),
            'GameScript': '',
            'Category': 'Trivia',
            'RankingRandom': False,
            'ContentPath': None,
            'ScoresPerSite': 5,
            'day1': datetime.datetime(2017, 4, 4, 17, 30),
            'AccountID': UUID('b0821785-bc49-4aea-a790-6d03433f14f7'),
            'PromotionID': 407,
            'ReportQuestionSummary': -1,
            'GameSubTypeID': 2,
            'MyZoneAdID': None,
            'FileName': 'customer-competitions',
            'WinnersTop': 0,
            'Game': 'Showdown',
            'MaxPlayerGameID': 0,
            'Passive': 0,
            'CompetitionID': 9570,
            'DisplayStatsOnWeb': 1,
            'Country': 'US',
            'ResultsOfficial': True,
            'GameZoneAdID': None,
            'MatchGameID': False,
            'StartDate': datetime.datetime(2017, 4, 4, 0, 0),
            'CompetitionType': 'Site Level Only',
            'SiteID': 1892,
            'Sequence': 4,
            'DerivativeID': 11,
            'Derivative': 'Showdown',
            'GameFileName': 'showdown',
            'GameCommandline': '',
            'CompetitionGameTypeID': 0,
            'PlayersPlusOnly': 0,
            'CompetitionStatus': 'Archived',
            'Locations': 10,
            'ScoringGridID': None,
            'TotalPlayers': 0,
            'CreateDate': datetime.datetime(2008, 4, 22, 0, 38, 46, 440000),
            'LastUpdated': datetime.datetime(2015, 8, 26, 10, 10, 10, 233000),
            'TabletGameNameID': 194, 'City': 'Wichita', 'RandomizeAnswers': 1,
            'BUZZTIME_GAME_TYPE_ID': 141
        }
    ],
    test_site_game_instances=[
        {
            'SiteID': 9881,
            'PlayerGames': 4,
            'TotalScore': 23898,
            'Game': 'Lunchtime Trivia',
            'PlayerGamesAverage': 5974,
            'GameStartTime': datetime.datetime(2017, 11, 28, 10, 0),
            'GameNameID': 136,
            'GameFileName': 'lunch-time-trivia'
        }
    ],
    test_site_game_players=[
        {
            'Username': 'COUGAR',
            'SiteState': 'FL',
            'PlayerGames': 620,
            'SiteID': 9881,
            'LocalWins': 266,
            'Game': 'Lunchtime Trivia',
            'GameNameID': 136,
            'Handle': 'BIGCAT',
            'OrgName': "Bru's Room Pompano Beach",
            'TotalScore': 3746018,
            'PlayerGamesAverage': 6041,
            'GameFileName': 'lunch-time-trivia',
            'PlayerRank': 1,
            'PlayerID': 219966,
            'SiteCity': 'Pompano Beach'
        }
    ],
    test_site_games=[
        {
            'Game': 'Showdown',
            'GameFileName': 'showdown',
            'TotalScore': 27771,
            'PlayerGamesAverage': 27771,
            'SiteID': 9881,
            'GameNameID': 29,
            'PlayerGames': 1
        }
    ],
    test_site_game_instance_players=[
        {
            "SiteState": "CA",
            "Game": "Playback",
            "Username": None,
            "PlayerID": 8778965,
            "OrgName": "BWW QA STG #1110",
            "GameStartTime": "2018-04-07T17:30:00",
            "LocalRank": 116,
            "Score": 678,
            "GameFileName": "playback",
            "GameNameID": 133,
            "PatSite": 1110,
            "SiteCity": "CARLSBAD",
            "SiteID": 1110,
            "Handle": "TED"
        }
    ],
    test_site_players=[
        {
            'LocalWins': 0,
            'Top100': 0,
            'GamesPlayed': 1,
            'OrgName': 'Buffalo Wild Wings Bloomington IL',
            'SiteID': 13972,
            'SiteState': 'IL',
            'Duration': Decimal('1000'),
            'Handle': 'AVADOM',
            'SiteCity': 'Bloomington',
            'Silver': 0,
            'PlayerRank': 23,
            'Rank': 23,
            'Gold': 0,
            'Bronze': 0,
            'Points': 5000,
            'PlayerID': 23285154,
            'Username': 'AVADOM10'
        }
    ],
    test_site_details=
    {
        'Accuracy': None,
        'SalesType': 'DITV',
        'Longitude': Decimal('-88.926691'),
        'ReviewCount': 2,
        'Id': 13972,
        'Location':
            {
                'Address':
                    {
                        'StreetAddress1': '3220 East Empire',
                        'ZipCode':
                            {
                                'ZipCodeValue': '61704'
                            },
                        'City':
                            {
                                'Name': 'Bloomington'
                            },
                        'State':
                            {
                                'Name': 'IL'
                            }
                    },
                'Name': 'Buffalo Wild Wings Bloomington IL',
                'Id': 13972
            },
        'IsTabletSite': True,
        'Distance': 0.0,
        'Chain': 'Bufwldwngs',
        'PowerPage':
            {
                'Description': 'The Buffalo Wild Wings&#174; restaurants features a variety of '
                               'boldly flavored, made-to-order menu items including its namesake '
                               'Buffalo, New York-style chicken wings. The Buffalo Wild Wings '
                               'menu specializes in mouth-watering signature sauces and'
                               ' seasonings '
                               'with flavors ranging from Sweet BBQ&#174; to Blazin\'&#174;.'
                               ' Guests '
                               'enjoy a welcoming neighborhood atmosphere that includes an '
                               'extensive'
                               ' multi-media system for watching their favorite sporting events. '
                               'Buffalo Wild Wings is the recipient of hundreds of "Best Wings"'
                               ' and '
                               '"Best Sports Bar" awards across the country.',
                'PowerPage_First_Image': 'bww_image1.jpg',
                'OptFullBar': 1,
                'OptFreeWifi': 1,
                'PowerPageID': 3271,
                'PowerPage_Second_Image': 'bww_image2.jpg',
                'Logo': 'bww_logo.png',
                'Opt21AndOver': 0,
                'URLTwitter': 'http://www.twitter.com/BWWings',
                'OptLiveMusic': 0,
                'PowerPage_Third_Image': 'bww_image3.jpg',
                'URLFacebook': 'http://www.facebook.com/BuffaloWildWings',
                'URLMyspace': 'http://www.myspace.com/buffalowildwings',
                'URL': 'http://www.buffalowildwings.com',
                'OptKidFriendly': 1,
                'OptFullMenu': 1
            },
        'Phone': '(309) 661-8027',
        'SiteName': 'Buffalo Wild Wings Bloomington IL',
        'SiteEvents':
            {
                'SiteEvent': [
                    {
                        'EventId': None,
                        'EventTypeId': None,
                        'EventDayId': None,
                        'EventStartTime': None,
                        'EventStatusId': None,
                        'EventEndTime': None,
                        'EventType': 'Special Event',
                        'SiteId': 13972
                    }
                ]
            },
        'Latitude': Decimal('40.488637')
    },
    test_top_trivia_location=[
        {
            'Gold': 0,
            'TriviaRank': 2670,
            'Bronze': 0,
            'Top100': 0,
            'Silver': 0,
            'TotalScore': 0,
            'OrgName': 'BWW QA STG #1110',
            'State': 'CA',
            'City': 'CARLSBAD',
            'SiteID': 1110
        }
    ],
    test_top_trivia_location_state=[
        {
            'OrgName': 'ICEBAR Orlando',
            'State': 'FL',
            'TotalScore': 1749,
            'TriviaRank': 226,
            'SiteID': 40762,
            'Top100': 0,
            'Gold': 0,
            'Bronze': 0,
            'City': 'Orlando',
            'Silver': 0
        }
    ],
    site_name_data=[
        {
            'OptLiveMusic': 0,
            'Address1': '1754 N Lexington',
            'URL': 'http://www.olmexico.com/ordereze/1000/Page.aspx',
            'PriPhone': '(651) 487-2847',
            'longitude': Decimal('-93.146205'),
            'rownum': 1,
            'Description': "Welcome to Ol' Mexico   Welcome to Ol' Mexico! Come discover the "
                           "many foods of Ol' Mexico and enjoy the BEST MARGARITAS in town! "
                           "Celebrating Over 30 Years of fine Authentic Mexican Cuisine Happy "
                           "hour specials from 3pm and 6pm with 2 for 1 on drinks and $2.00 off "
                           "appetizers. Late night happy hour 9pm-midnight. Ol' Mexico offers "
                           "free Wi-Fi and Comedy Improv on Saturday nights at 7:30.  Join us!",
            'City': 'Roseville',
            'latitude': Decimal('44.994369'),
            'distance': 0.0,
            'State': 'MN',
            'OptFreeWifi': 1,
            'isTabletSite': 1,
            'Opt21AndOver': 0,
            'TotalRows': 1,
            'OptFullMenu': 1,
            'OptKidFriendly': 1,
            'Logo': '1488_logo.png',
            'PowerPageID': 3989,
            'Chain': 'NONE',
            'siteid': 1488,
            'OptFullBar': 1,
            'ReviewCount': 5,
            'Zip': '55113',
            'OrgName': "Ol' Mexico"
        }
    ],
    geo_site_data=[
        {
            'OptLiveMusic': 0,
            'Address1': 'Boulevard Galerias Saltillo # 375-2',
            'URL': 'http://www.buffalowildwings.com',
            'PriPhone': '01152 -844-3050-992',
            'longitude': Decimal('-100.982107'),
            'rownum': 1,
            'Description': 'The Buffalo Wild Wings restaurants '
                           'features a variety of boldly flavored, '
                           'made-to-order menu items including its namesake Buffalo,'
                           ' New York-style chicken wings. The Buffalo Wild Wings'
                           ' menu specializes in mouth-watering signature sauces and seasonings'
                           ' with flavors ranging from Sweet BBQ to Blazin\'. '
                           'Guests enjoy a welcoming neighborhood atmosphere that includes an '
                           'extensive multi-media system for watching their favorite sporting '
                           'events. Buffalo Wild Wings is the recipient of hundreds of "Best'
                           ' Wings" and "Best Sports Bar" awards across the country.',
            'City': 'Saltillo',
            'latitude': Decimal('25.455461'),
            'distance': 160.09476057019856,
            'State': 'Coahuila',
            'OptFreeWifi': 1,
            'isTabletSite': 1,
            'Opt21AndOver': 0,
            'OptFullMenu': 1,
            'OptKidFriendly': 1,
            'Logo': '39733_logo.png',
            'PowerPageID': 6778,
            'Chain': 'Bufwldwngs',
            'siteid': 39733,
            'OptFullBar': 1,
            'ReviewCount': None,
            'Zip': '25240',
            'OrgName': 'Buffalo Wild Wings Saltillo'
        }
    ],
    site_details=[
        {
            'siteid': 13972,
            'PowerPageURLFacebook': 'http://www.facebook.com/BuffaloWildWings',
            'SalesType': 'DITV',
            'pDisplayAddress': True,
            'OptKidFriendly': 1,
            'EventStatusID': None,
            'City': 'Bloomington',
            'distance': 0.0,
            'Zip': '61704',
            'PowerPageLogo': 'bww_logo.png',
            'longitude': Decimal('-88.926691'),
            'PowerPageURLTwitter': 'http://www.twitter.com/BWWings',
            'Opt21AndOver': 0,
            'PowerPageImage2': 'bww_image2.jpg',
            'TriviaRank': 141,
            'accuracy': 100,
            'OptLiveMusic': 0,
            'EventEndTime': None,
            'ReviewCount': 2,
            'EventDayID': None,
            'PowerPageImage1': 'bww_image1.jpg',
            'EventStartTime': None,
            'TimeZone': 2,
            'latitude': Decimal('40.488637'),
            'PowerPageURLMyspace': 'http://www.myspace.com/buffalowildwings',
            'State': 'IL',
            'OptFreeWifi': 1,
            'PowerPageURL': 'http://www.buffalowildwings.com',
            'isTabletSite': 1,
            'PowerPageImage3': 'bww_image3.jpg',
            'PowerPageDescription': 'The Buffalo Wild Wings&#174; restaurants '
                                    'features a variety of boldly flavored, made-to-order'
                                    ' menu items including its namesake Buffalo,'
                                    ' New York-style chicken wings. The Buffalo Wild'
                                    ' Wings menu specializes in mouth-watering signature '
                                    'sauces and seasonings with flavors ranging from Sweet '
                                    'BBQ&#174; to Blazin\'&#174;. Guests enjoy a welcoming'
                                    ' neighborhood atmosphere that includes an extensive '
                                    'multi-media system for watching their favorite sporting '
                                    'events. Buffalo Wild Wings is the recipient of hundreds'
                                    ' of "Best Wings" and "Best Sports Bar" awards'
                                    ' across the country.',
            'OptFullMenu': 1,
            'EventID': None,
            'Chain': 'Bufwldwngs',
            'EventTypeID': None,
            'OptFullBar': 1,
            'PowerPageID': 3271,
            'PriPhone': '(309) 661-8027',
            'OrgName': 'Buffalo Wild Wings Bloomington IL',
            'Address1': '3220 East Empire',
            'ActivityRank': 371
        }
    ],
    site_details_site=[
        {
            'City': 'Tonawanda',
            'OrgName': 'Buffalo Wild Wings Tonawanda',
            'State': 'NY',
            'TotalScore': 69386984,
            'Top100': 583,
            'Silver': 2,
            'TriviaRank': 199,
            'Gold': 55,
            'Bronze': 1,
            'SiteID': 10339
        },
        {
            'City': 'Plano',
            'OrgName': 'Austin Avenue Plano',
            'State': 'TX',
            'TotalScore': 11849216,
            'Top100': 244,
            'Silver': 1,
            'TriviaRank': 200,
            'Gold': 55,
            'Bronze': 1,
            'SiteID': 40116
        }
    ],
    site_details_id=[
        {
            'City': 'CARLSBAD',
            'OrgName': 'BWW QA STG #1110',
            'State': 'CA',
            'TotalScore': 0,
            'Top100': 0,
            'Silver': 0,
            'TriviaRank': 2670,
            'Gold': 0,
            'Bronze': 0,
            'SiteID': 1110
        }
    ],

)
