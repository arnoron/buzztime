import ast
import json

from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.test.response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestSearchSiteByLocation(APITestCase):
    """

    """

    def test_longitude_not_passed(self):
        """

        :return:
        """
        response = self.client.get("/players/game_sites/nearby/?latitude=12.912431")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'"ValueError Invalid Parameter passed"')

    def test_latitude_not_passed(self):
        """

        :return:
        """
        response = self.client.get("/players/game_sites/nearby/?longitude=12.912431")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'"ValueError Invalid Parameter passed"')

    def test_latitude_is_a_string(self):
        """

        :return:
        """
        response = self.client.get("/players/game_sites/nearby/?latitude=abcd&longitude=12.91")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'"ValueError Invalid Parameter passed"')

    def test_longitude_is_a_string(self):
        """

        :return:
        """
        response = self.client.get("/players/game_sites/nearby/?longitude=abcd&latitude=12.91")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'"ValueError Invalid Parameter passed"')

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_blank_response(self, fetch_all_mock, connection_manager_mock):
        """

        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        fetch_all_mock.side_effect = mock_test_utils.\
            mock_fetch_responses(MOCK_RESPONSE['site_search_by_location_no_match'])

        response = self.client.get("/players/game_sites/nearby/?longitude=12.3&latitude=12.4")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[]')

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_correct_response(self, fetch_all_mock, connection_manager_mock):
        """

        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        fetch_all_mock.side_effect = mock_test_utils. \
            mock_fetch_responses(MOCK_RESPONSE['site_search_by_location_match'])

        response = self.client.get("/players/game_sites/nearby/?longitude=12.3&latitude=12.4")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8"))[0]['Id'], 40978)
