"""
Holds the test cases for the top trivia location class
"""
from rest_framework.test import APITestCase

from cheerios.players.views.site.top_trivia_location import TopTriviaLocation
from mock import patch, Mock
from rest_framework.response import Response
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils

MOCK_STORED_PROCEDURE = 'cheerios.players.views.players.PlayersViewSet.execute_stored_procedure'


class TestSiteSearch(APITestCase):
    """
    Tests the site search module
    """

    def setUp(self):
        """
        Initializes the top trivia location object
        :return:
        """
        self.top_trivia_location_interface = TopTriviaLocation()

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['site_details_site'],
               MOCK_RESPONSE['site_details_id'])))
    def test_top_trivia_location(self):
        """
        Tests the top trivia location
        :return:
        """
        state = None

        response = self.top_trivia_location_interface.get_top_sites_response(state, site_id=1110)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['site_details_site'],
               MOCK_RESPONSE['site_details_id'])))
    def test_top_trivia_location_state(self):
        """
        Tests the top trivia location with state
        :return:
        """
        state = 'FL'

        response = self.top_trivia_location_interface.get_top_sites_response(state, site_id=None)
        self.assertEqual(response.status_code, 200)
