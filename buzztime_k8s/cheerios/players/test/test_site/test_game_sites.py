"""
Holds the test cases for the game sites class
"""
from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.views.site.game_sites import GameSites
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestGameSites(APITestCase):
    """
    Tests the game sites module
    """

    def setUp(self):
        """
        Initializes the game sites object
        :return:
        """
        self.game_sites_interface = GameSites()

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['test_site_scores_game_id'])))
    def test_site_scores_game_id(self):
        """
        Tests the site scores api with premium game id param
        :return:
        """
        premium_game_id = 119
        site = ''
        city = ''
        state = ''
        lower_limit = 1
        upper_limit = 200

        response = self.game_sites_interface.get_game_sites(premium_game_id,
                                                            site,
                                                            city,
                                                            state,
                                                            lower_limit,
                                                            upper_limit
                                                           )
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['test_site_scores_loc_params'])))
    def test_site_scores_loc_params(self):
        """
        Tests the site scores api with location params
        :return:
        """
        premium_game_id = 119
        site = ''
        city = 'Dothan'
        state = 'AL'
        lower_limit = 1
        upper_limit = 200

        response = self.game_sites_interface.get_game_sites(premium_game_id,
                                                            site,
                                                            city,
                                                            state,
                                                            lower_limit,
                                                            upper_limit
                                                           )
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['test_site_scores_loc_params'])))
    def test_site_scores_limits(self):
        """
        Tests the site scores api with limits params
        :return:
        """
        premium_game_id = 119
        site = ''
        city = ''
        state = ''
        lower_limit = '4'
        upper_limit = '10'

        response = self.game_sites_interface.get_game_sites(premium_game_id,
                                                            site,
                                                            city,
                                                            state,
                                                            lower_limit,
                                                            upper_limit
                                                           )
        self.assertEqual(response.status_code, 200)
