"""
Holds the test cases for the site details class
"""
import datetime

from mock import patch, Mock
from rest_framework.response import Response
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import get_events_unprocessed_data
from cheerios.players.views.site.site_details import SiteDetails
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestSiteDetails(APITestCase):
    """
    Tests the site details module
    """

    def setUp(self):
        """
        Initializes the site details object
        :return:
        """
        self.site_details_interface = SiteDetails()

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['site_details'])))
    def test_site_details_valid_site_id(self):
        """
        Tests the site details with valid site id
        :return:
        """
        site_id = 13972

        response = self.site_details_interface.get_site_details(site_id)
        self.assertEqual(response.status_code, 200)

    def test_site_invalid_site_id(self):
        """
        Tests the site details with invalid site id
        :return:
        """
        site_id = '%$&'
        response = self.site_details_interface.get_site_details(site_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Site ID is mandatory")

    def test_site_details_empty_site_id(self):
        """
        Tests the site details with no site id
        :return:
        """
        site_id = None

        response = self.site_details_interface.get_site_details(site_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Site ID is mandatory")

    def test_post_process_data(self):
        """
        Tests  the post process data method
        :return:
        """
        site_info_object = SiteDetails()
        processed_data = site_info_object.post_process_data(get_events_unprocessed_data()[0])

        now = datetime.datetime.now()
        if int(now.strftime("%w")) in (1, 2):
            self.assertEqual(processed_data['SiteEvents']['SiteEvent'][0]['EventDayId'], 2)
        else:
            self.assertEqual(processed_data['SiteEvents']['SiteEvent'][0]['EventDayId'], 0)
