"""
Holds the test cases for the site search class
"""
from mock import patch, Mock
from .response_data import MOCK_RESPONSE

from rest_framework.test import APITestCase

from cheerios.players.views.site.site_search import SiteSearch
from cheerios.test_utils import mock_test_utils


class TestSiteSearch(APITestCase):
    """
    Tests the site search module
    """

    def setUp(self):
        """
        Initializes the site search object
        :return:
        """
        self.site_search_interface = SiteSearch()

    def test_search_empty_string(self):
        """
        Tests the site search with no search string
        :return:
        """
        latitude = '23.45'
        longitude = '32.56'
        search_string = None
        miles = '10'

        response = self.site_search_interface.\
            get_site_search_response(latitude, longitude, search_string, miles)
        self.assertEqual(response.data, "search is a mandatory field")
        self.assertEqual(response.status_code, 400)

    def test_site_search_lat_not_float(self):
        """
        Tests the site search with invalid type latitude
        :return:
        """
        latitude = '$&^'
        longitude = '32.56'
        search_string = "Mexico"
        miles = '10'

        response = self.site_search_interface.\
            get_site_search_response(latitude, longitude, search_string, miles)
        self.assertEqual(response.data, "Bad Request: Latitude and Longitude should be float")
        self.assertEqual(response.status_code, 400)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, Mock(side_effect=[MOCK_RESPONSE['handle_site_search']]))
    def test_site_search_empty_lat_data(self):
        """
        Tests the site search with no latitude
        :return:
        """
        latitude = 'None'
        longitude = '32.56'
        search_string = "Mexico"
        miles = '10'

        response = self.site_search_interface.\
            get_site_search_response(latitude, longitude, search_string, miles)
        self.assertEqual(response.status_code, 400)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['site_name_data'],
               MOCK_RESPONSE['geo_site_data'])))
    def test_site_search_valid_values(self):
        """
        Tests the site search with valid values
        :return:
        """
        latitude = '23.45'
        longitude = '32.56'
        search_string = 'mexico'
        miles = '10'

        response = self.site_search_interface.\
            get_site_search_response(latitude, longitude, search_string, miles)
        self.assertEqual(response.status_code, 200)


    def test_site_search_invalid_miles(self):
        """
        Tests the site search with invalid value for miles arguement
        :return:
        """
        latitude = '23.45'
        longitude = '32.56'
        search_string = 'mexico'
        miles = 'abc'

        response = self.site_search_interface.\
            get_site_search_response(latitude,longitude,search_string,miles)
        self.assertEqual(response.data, "Bad Request: Miles can only be positive integer.")
        self.assertEqual(response.status_code, 400)