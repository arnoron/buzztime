"""
Test Module for the AMOE Data API
"""
import json

from mock import patch
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_content, response_status
from cheerios.test_utils import mock_test_utils

#commented because we are not passing the captcha in the test cases.
# class TestAMOEData(APITestCase):
#     """
#     Test class for the AMOEData API Class
#     """
#
#     AMOE_URL = "/players/amoe_data/"
#
#     def test_serializer_not_valid(self):
#         """
#         Tests the case when serializer is not valid
#         :return:
#         """
#         response = self.client.post(self.AMOE_URL,
#                                     data=dict(first_name="Test_first_name",
#                                               last_name="Test_last_name",
#                                               email_address="test_email_address",
#                                               zip="12345"
#                                               )
#                                     )
#         response_data = json.loads(response_content(response).decode("utf-8"))
#         self.assertEqual(response_data, "ValueError Invalid Data Passed")
#         self.assertEqual(response_status(response), 400)
#
#     @patch(mock_test_utils.CONNECTION_MANAGER)
#     def test_successful_insertion(self, cursor):
#         """
#         Tests the successful insertion
#         :return:
#         """
#         cursor.side_effect = mock_test_utils.mock_cursor_responses([])
#         response = self.client.post(self.AMOE_URL, data=dict(first_name="Test_first_name",
#                                                              last_name="Test_last_name",
#                                                              email_address="test_email_address",
#                                                              zip="test_zip",
#                                                              favorite_trivia_game="test_trivia_game"
#                                                              )
#                                     )
#         response_data = json.loads(response_content(response).decode("utf-8"))
#         self.assertEqual(response_data, "AMOE Data Inserted")
#         self.assertEqual(response_status(response), 200)

    # TODO Add more negative test cases
