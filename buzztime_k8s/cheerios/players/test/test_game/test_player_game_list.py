"""
Holds the test cases for the player game list class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.game.player_game_list import PlayerGameList
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestPlayerGameList(APITestCase):
    """
    Tests the Player Game List Module
    """

    def setUp(self):
        """
        Initializes the game list object
        :return:
        """
        self.game_list_interface = PlayerGameList()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_game_list_valid_values(self, cursor):
        """
        Tests the game list with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_game_list_valid_values'])
        player_id = 233432
        response = self.game_list_interface.get_game_list(player_id)
        self.assertEqual(response.status_code, 200)

    def test_game_list_player_id_null(self):
        """
        Tests the game list with null player id
        :return:
        """
        player_id = None

        response = self.game_list_interface.get_game_list(player_id)
        self.assertEqual(response.data.get('Error'), 'Empty parameters passed')
        self.assertEqual(response.status_code, 400)

    def test_game_invalid_player(self):
        """
        Tests the game list with invalid player id
        :return:
        """
        player_id = '%$*'
        response = self.game_list_interface.get_game_list(player_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.get('Error'), 'Empty parameters passed')
