"""
Mock Responses
"""
import datetime

MOCK_RESPONSE = dict(
    test_game=[
        {
            'Game': 'History Trivia',
            'GameFileName': 'history-trivia',
            'TotalPlayers': 1529,
            'GameDerivativeID': 188,
            'GameID': 683,
            'GameDate': datetime.datetime(2018, 4, 4, 0, 0),
            'GameTypeID': 1,
            'PremiumGameID': 54
        }
    ],
    winner_site_data=[
        {
            'State': 'CA',
            'Score': 9702,
            'SiteID': 1110,
            'PremiumGameSiteScoreID': 960,
            'Rank': 1,
            'City': 'CARLSBAD',
            'GameID': None,
            'GameDate': datetime.datetime(2018, 4, 4, 0, 0),
            'PremiumGameID': 54,
            'SiteName': 'BWW QA STG #1110'
        }
    ],
    winner_data=[
        {
            'State': 'AZ',
            'GameTypeID': 1,
            'SiteID': 15104,
            'City': 'Tucson',
            'PremiumGameScoreID': 21300,
            'GameDate': datetime.datetime(2018, 4, 4, 0, 0),
            'Score': 9997,
            'SiteName': 'Famous Sams 17 Tucson',
            'PIN': '6202WIY',
            'GameName': 'History Trivia',
            'GameID': 683,
            'GameSubTypeID': 4,
            'Handle': 'DEOHGE',
            'Rank': 1,
            'PremiumGameID': 54,
            'GameDerivativeID': 188,
            'PlayerID': 233432,
            'RepeatGame': 0
        }
    ],
    test_game_premium=[
        {
            'GameFileName': 'history-trivia',
            'GameID': 54,
            'TotalPlayers': 1529,
            'Description': 'History Trivia',
            'GameDate': datetime.datetime(2018, 4, 4, 0, 0),
            'TopSiteName': 'BWW QA STG #1110',
            'TopPlayer': 'DEOHGE'
        }
    ],
    game_list=[
        {
            'GameNameID': 243,
            'ProductionDate': datetime.datetime(2015, 7, 1, 0, 0),
            'GameTypeID': 1,
            'Game': 'Academy of Football 30',
            'DerivativeID': 243,
            'GameSubTypeID': 4
        }
    ],
    game_site_players=[
        {
            'GameDerivativeID': 188,
            'SiteID': 1110,
            'PIN': '0005BQS',
            'HomeState': 'CA',
            'Handle': 'FIVE  ',
            'RowID': 1,
            'HomeSiteName': 'BWW QA STG #1110',
            'ForumUserID': 3002058,
            'HomeSiteID': '1110',
            'State': 'CA',
            'City': 'CARLSBAD',
            'SiteScore': 9702,
            'RecCount': 116,
            'SiteName': 'BWW QA STG #1110',
            'Score': 9923,
            'HomeCity': 'CARLSBAD',
            'PlayerID': 3002058
        }
    ],
    test_game_sites=[

        {
            'State': 'CA',
            'RowID': 1,
            'Rank': 1,
            'SiteName': 'BWW QA STG #1110',
            'Score': 82296,
            'RecCount': 990,
            'SiteID': 1110,
            'City': 'CARLSBAD',
            'GameDerivativeID': 85
        }
    ],
    test_game_players=[
        {
            'Score': 84307,
            'Handle': 'TESST',
            'SiteID': 1110,
            'State': 'CA',
            'RepeatGame': 0,
            'RecCount': 1509,
            'GameDerivativeID': 85,
            'SiteName': 'BWW QA STG #1110',
            'ForumUserID': 8672314,
            'PlayerID': 8672314,
            'RowID': 1,
            'City': 'CARLSBAD',
            'Rank': 1
        }
    ],
    test_game_list=[
        {
            'DerivativeID': 243,
            'GameTypeID': 1,
            'GameSubTypeID': 4,
            'ProductionDate': datetime.datetime(2015, 7, 1, 0, 0),
            'GameNameID': 243,
            'Game': 'Academy of Football 30'
        }
    ],
    test_game_list_valid_values=[
        {
            'NationalBronze': 0,
            'GameSubTypeDerivativeID': 188,
            'AverageScoreRank': 0,
            'PlayerCount': 7116,
            'NationalTop100Rank': 335,
            'NationalTop100': 1,
            'NationalGold': 1,
            'GamesPlayed': 2,
            'LocalWinsRank': 1875,
            'LocalWins': 0,
            'GameSubTypeID': 4,
            'NationalSilver': 0,
            'AverageScore': 8352,
            'GameTypeID': 1,
            'Description': 'History Trivia',
            'GamesPlayedForAverage': 2
        }
    ],
    test_premium_game_list=[
        {
            'GameDate': datetime.datetime(2018, 4, 4, 0, 0),
            'Description': 'History Trivia',
            'TopPlayer': 'DEOHGE',
            'TotalPlayers': 1529,
            'TopSiteName': 'BWW QA STG #1110',
            'GameID': 54,
            'GameFileName': 'history-trivia'
        }
    ],
    test_site_game_valid_site=[
        {
            'PlayerGamesAverage': 7442,
            'SiteID': 1110,
            'PlayerGames': 696,
            'GameFileName': 'topix',
            'GameNameID': 137,
            'Game': 'Topix',
            'TotalScore': 5180313
        }
    ]

)
