"""
Holds the test cases for the site game list class
"""
from mock import patch
from rest_framework.test import APITestCase

from .response_data import MOCK_RESPONSE
from cheerios.players.views.game.site_game_list import SiteGameList
from cheerios.test_utils import mock_test_utils


class TestSiteGameList(APITestCase):
    """
    Tests the site game list module
    """

    def setUp(self):
        """
        Initializes the site game list module
        :return:
        """
        self.site_game_list_interface = SiteGameList()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_site_game_valid_site(self, cursor):
        """
        Tests the site game list with valid site id
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_site_game_valid_site'])
        site_id = 1110

        response = self.site_game_list_interface.get_games_for_site(site_id)
        self.assertEqual(response.status_code, 200)

    def test_site_game_invalid_site(self):
        """
        Tests the site game list with invalid site id
        :return:
        """
        site_id = 'xyz'

        response = self.site_game_list_interface.get_games_for_site(site_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.get('Error'), 'Empty parameters passed')

