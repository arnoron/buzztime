"""
Holds the test cases for the premium game list class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.game.premium_games_list import PremiumGamesList
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestPremiumGameList(APITestCase):
    """
    Tests the premium game list module
    """

    def setUp(self):
        """
        Initializes the premium game list object
        :return:
        """
        self.premium_game_list_interface = PremiumGamesList()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_premium_game_list(self, cursor):
        """
        Tests the premium game list method
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_premium_game_list'])
        response = self.premium_game_list_interface.get_premium_games()
        self.assertEqual(response.status_code, 200)
