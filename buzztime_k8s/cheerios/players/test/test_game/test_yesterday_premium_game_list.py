"""
Holds the test cases for the yesterday premium game list class
"""
from mock import patch
from .response_data import MOCK_RESPONSE
from rest_framework.test import APITestCase

from cheerios.players.views.game.yesterday_premium_games_list import YesterdayPremiumGamesList
from cheerios.test_utils import mock_test_utils


class TestYesterdayPremiumGameList(APITestCase):
    """
    Tests the yesterday premium game list
    """

    def setUp(self):
        """
        Initializes the yesterday premium game list object
        :return:
        """
        self.premium_game_list_interface = YesterdayPremiumGamesList()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_yesterday_premium_game(self, cursor):
        """
        Tests the yesterday premium game list method
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['test_game'],
                                                   MOCK_RESPONSE['winner_data'],
                                                   MOCK_RESPONSE['winner_site_data'])
        response = self.premium_game_list_interface.get_yesterday_premium_games()
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_game_winner_data(self, cursor):
        """
        Tests the game winner data
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['winner_data'])
        game_id = 652
        premium_game_id = 70

        self.assertIsNotNone(
            self.premium_game_list_interface.get_game_winner_data(
                premium_game_id, game_id
            ))

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_game_winner_site_data(self, cursor):
        """
        Tests the game winners site data
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['winner_site_data'])
        game_id = 10
        premium_game_id = 25

        self.assertIsNotNone(
            self.premium_game_list_interface.get_game_winner_site_data(
                premium_game_id, game_id
            ))
