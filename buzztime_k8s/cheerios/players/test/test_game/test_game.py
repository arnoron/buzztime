"""
Holds the integration test cases for the game API
"""
import json

from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import response_status, response_data

from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils

API_ENDPOINT = '/players/games/'


class TestGame(APITestCase):
    """
    Tests the Game API
    """
    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_game_premium_yesterday(self, cursor, connection_manager_mock):
        """
        Tests the game API with premium type and yesterday
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [
            MOCK_RESPONSE['test_game'],
            MOCK_RESPONSE['winner_data'],
            MOCK_RESPONSE['winner_site_data']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?type=premium&day=yesterday'))
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_game_premium(self, cursor, connection_manager_mock):
        """
        Tests the game API with only premium type
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_game_premium']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?type=premium'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('GameFileName' in json_response_data[0])
        self.assertTrue('TopPlayer' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_games_list(self, cursor, connection_manager_mock):
        """
        Tests the game API to return games list
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['game_list']]
        response = self.client.get(API_ENDPOINT)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('Game' in json_response_data[0])
        self.assertTrue('GameNameID' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_game_site_players(self, cursor, connection_manager_mock):
        """
        Tests the game API to return games site players
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['game_site_players']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '54/sites/1110/players'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('SiteID' in json_response_data[0])
        self.assertTrue('SiteName' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_game_sites(self, cursor, connection_manager_mock):
        """
        Tests the game API to return games sites
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_game_sites']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '82/sites'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('SiteID' in json_response_data[0])
        self.assertTrue('SiteName' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_game_players(self, cursor, connection_manager_mock):
        """
        Tests the game API to return game players
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_game_players']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '82/players'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Rank' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    def test_game_invalid_game_subtype(self):
        """
        Tests the game API with invalid game_subtype_id
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '82/xyz'))
        self.assertEqual(response.data, 'Invalid game info subtype')
        self.assertEqual(response_status(response), 400)
