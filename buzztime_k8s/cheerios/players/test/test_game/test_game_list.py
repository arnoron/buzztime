"""
Holds the test cases for the game list class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.game.game_list import GameList

from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestGameList(APITestCase):
    """
    Tests the game list module
    """

    def setUp(self):
        """
        Initializes the game list object
        :return:
        """
        self.game_list_interface = GameList()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_game_list(self, cursor):
        """
        Tests the game list method
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['test_game_list'])
        response = self.game_list_interface.get_game_list()
        self.assertEqual(response.status_code, 200)
