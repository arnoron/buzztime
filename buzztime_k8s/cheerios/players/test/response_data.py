"""
MOCK RESPONSES
"""
from decimal import Decimal

import datetime


MOCK_RESPONSE = dict(
    competition_player_ranking=[
        {'CompetitionID': 10568,
         'GroupID': 0,
         'RowNum': 1,
         'Rank': 1,
         'Score': 14999,
         'PlayerID': 2357940,
         'Handle': 'KHAN  ',
         'Username': None,
         'Email': None,
         'SiteID': 2897,
         'OrgName': 'Fox and Hound College Station',
         'SiteCity': 'College Station',
         'SiteState': 'TX',
         'RecCount': 1201,
         'SiteNamePlayedAt': 'Fox and Hound College Station',
         'SiteCityPlayedAt': 'College Station',
         'SiteStatePlayedAt': 'TX',
         'SiteIDPlayedAt': 2897},
        {'CompetitionID': 10568,
         'GroupID': 0,
         'RowNum': 2,
         'Rank': 2,
         'Score': 14987,
         'PlayerID': 232078,
         'Handle': 'BO    ',
         'Username': 'bo_edge',
         'Email': '232078@ntn.com',
         'SiteID': 23361,
         'OrgName': 'Celtic Crown',
         'SiteCity': 'Chicago',
         'SiteState': 'IL',
         'RecCount': 1201,
         'SiteNamePlayedAt': 'Celtic Crown',
         'SiteCityPlayedAt': 'Chicago',
         'SiteStatePlayedAt': 'IL',
         'SiteIDPlayedAt': 23361}
        ],
    site_search_by_location_match=[
        {
            'siteid': 40978,
            'OrgName': 'NTN Site Hub - Test Site #18',
            'Address1': '2231 Rutherford Road',
            'City': 'Carlsbad',
            'State': 'CA',
            'Zip': '92008',
            'PriPhone': None,
            'Chain': '',
            'latitude': Decimal('12.912409'),
            'longitude': Decimal('77.640437'),
            'distance': 0.0019333604187330433,
            'rownum': 1,
            'PowerPageID': None,
            'Description': None,
            'URL': None,
            'Logo': None,
            'OptFreeWifi': None,
            'Opt21AndOver': None,
            'OptKidFriendly': None,
            'OptLiveMusic': None,
            'OptFullBar': None,
            'OptFullMenu': None,
            'ReviewCount': None,
            'isTabletSite': 1
        },
        {
            'siteid': 39670,
            'OrgName': 'Buffalo Wild Wings Estancia Mall Capital Commons Philippines',
            'Address1': 'Estancia Mall - Unit L1-139, Capitol Commons, Bgy.',
            'City': 'Pasig City',
            'State': 'Manila',
            'Zip': '9720',
            'PriPhone': '(632) 632-7489',
            'Chain': 'Bufwldwngs',
            'latitude': Decimal('14.576376'),
            'longitude': Decimal('121.085109'),
            'distance': 2917.0105382616985,
            'rownum': 2,
            'PowerPageID': 6744,
            'Description': 'The Buffalo Wild Wings&#174; '
                           'restaurants features a variety of boldly flavored, '
                           'made-to-order menu items including its namesake Buffalo,'
                           ' New York-style chicken wings. '
                           'The Buffalo Wild Wings menu specializes in mouth-watering signature sauces '
                           'and seasonings with flavors ranging from Sweet BBQ&#174; '
                           'to Blazin\'&#174;. Guests enjoy a welcoming neighborhood atmosphere that i'
                           'ncludes an extensive multi-media system for watching their favorite sporting events. '
                           'Buffalo Wild Wings is the recipient of hundreds of "Best Wings" and "'
                           'Best Sports Bar" awards across the country.', 'URL': 'http://www.buffalowildwings.com',
            'Logo': 'bww_logo.png',
            'OptFreeWifi': 1,
            'Opt21AndOver': 0,
            'OptKidFriendly': 1,
            'OptLiveMusic': 0,
            'OptFullBar': 1,
            'OptFullMenu': 1,
            'ReviewCount': None,
            'isTabletSite': 1
        }
    ],
    site_search_by_location_no_match=[
        {
            'siteid': 857,
            'OrgName': "Smitty's Restaurant and Lounge Dauphin",
            'Address1': '1601 Main Street S',
            'City': 'Dauphin',
            'State': 'MB',
            'Zip': 'R7N 3J5',
            'PriPhone': '(204) 638-8022',
            'Chain': 'NONE',
            'latitude': Decimal('51.142000'),
            'longitude': Decimal('-100.058000'),
            'distance': 161.51620727207248,
            'rownum': 1,
            'PowerPageID': None,
            'Description': None,
            'URL': None,
            'Logo': None,
            'OptFreeWifi': None,
            'Opt21AndOver': None,
            'OptKidFriendly': None,
            'OptLiveMusic': None,
            'OptFullBar': None,
            'OptFullMenu': None,
            'ReviewCount': None,
            'isTabletSite': 1
        },
        {
            'siteid': 40419,
            'OrgName': 'The Owl',
            'Address1': '3737 Wascana Parkway',
            'City': 'Regina',
            'State': 'SK',
            'Zip': 'S4S 0A2',
            'PriPhone': '(306) 586-8811 Ext 202',
            'Chain': '',
            'latitude': Decimal('50.418222'),
            'longitude': Decimal('-104.586002'),
            'distance': 202.45628839082804,
            'rownum': 2,
            'PowerPageID': 7302,
            'Description': None,
            'URL': 'http://theowl.ursu.ca/',
            'Logo': '40419_logo.png',
            'OptFreeWifi': 1,
            'Opt21AndOver': 0,
            'OptKidFriendly': 0,
            'OptLiveMusic': 1,
            'OptFullBar': 1,
            'OptFullMenu': 1,
            'ReviewCount': None,
            'isTabletSite': 1
        }
    ],
    auth_api=[
        {
            'PlayerID': 22631264,
            'Password': '871089a00add2203c254c526e8d00cc7',
            'Salt': 'bh3'
        }
    ],
    bad_words=b"{'dirty_words':['SATAN'], 'exceptions':['test']}",
    player_prof=
    {
        'avatar_crc': 397504235,
        'username': 'Hasher01',
        'player_id': 22631264,
        'pin': None,
        'email': '22631264@ntn.com',
        'birth_date': datetime.datetime(1987, 10, 10, 0, 0),
        'player_plus_points': 5000,
        'postal_code': '560100',
        'display_name': 'HASHER',
        'site_id': 33433
    },
    fetch=[
        {
            'PlayerID': 207344,
            'FileSent': None,
            'AvatarID': 11808,
            'FileModified': datetime.datetime(2016, 11, 8, 13, 34, 28),
            'AvatarFile': '207344.jpg',
            'AvatarXML': '<avatar gender="m"><features><feature selected="2" name="body"/>'
                         '<feature selected="0" name="chest"/><feature selected="2" name="head"/>'
                         '<feature selected="3" name="shirt"/><feature selected="3" '
                         'name="shirtFront"/><feature selected="3" name="shirtChest"/>'
                         '<feature selected="5" name="hair"/><feature selected="5"'
                         ' name="hairFront"/><feature selected="5" name="hairBack"/>'
                         '<feature selected="5" name="hairMiddle"/><feature selected="0" '
                         'name="hat"/><feature selected="7" name="faceHair"/><feature'
                         ' selected="7" name="facialHairFront"/><feature selected="7" '
                         'name="facialHairBack"/><feature selected="2" name="ears"/>'
                         '<feature selected="2" name="earFront"/><feature selected="2" '
                         'name="earBack"/><feature selected="3" name="eyes"/><feature '
                         'selected="4" name="eyeBrows"/><feature selected="5" name="mouth"/>'
                         '<feature selected="4" name="nose"/></features><colors><color '
                         'ypos="36" xpos="189" color="14652762" name="skinColor"/><color '
                         'ypos="32" xpos="47" color="6052864" name="hairColor"/><color ypos="0" '
                         'xpos="0" color="0" name="lipColor"/><color ypos="27" xpos="52" '
                         'color="10001408" name="eyeColor"/><color ypos="6" xpos="189" '
                         'color="11579646" name="shirtColor"/><color ypos="1" xpos="43" '
                         'color="16711156" name="backdropColor"/></colors></avatar>',
            'FileCRC32': -1273262692,
            'created': datetime.datetime(2009, 2, 9, 11, 20, 57, 890000),
            'Expires': None,
            'updated': datetime.datetime(2016, 11, 8, 13, 34, 28, 183000)
        }
    ],
    test_events=[
        {
            'ReviewCount': 0,
            'Id': 40356,
            'PowerPage':
                {
                    'PowerPage_First_Image': '',
                    'PowerPage_Second_Image': '',
                    'URLMyspace': '',
                    'URLTwitter': '',
                    'OptFreeWifi': 0,
                    'Logo': '',
                    'OptLiveMusic': 0,
                    'PowerPage_Third_Image': '',
                    'Description': '',
                    'OptKidFriendly': 0,
                    'PowerPageID': None,
                    'OptFullMenu': 0,
                    'OptFullBar': 0,
                    'URL': '',
                    'Opt21AndOver': 0,
                    'URLFacebook': ''
                },
            'Longitude': Decimal('-71.436790'),
            'SiteEvents':
                {
                    'SiteEvent': [
                        {
                            'EventStatusId': 0,
                            'EventDayId': 3,
                            'EventTypeId': 45,
                            'EventId': 3976766,
                            'EventEndTime': '12:30 pm',
                            'EventStartTime': '7:00 PM',
                            'SiteId': 40356,
                            'EventType': 'BuzztimeLiveTrivia'
                        }
                    ]
                },
            'Phone': '(603) 888-9464',
            'Chain': 'Bufwldwngs',
            'Accuracy': None,
            'Distance': 5289.454443992287,
            'Location':
                {
                    'Address':
                        {
                            'City':
                                {
                                    'Name': 'Nashua'
                                },
                            'StreetAddress1': '310 Daniel Webster HWY',
                            'ZipCode':
                                {
                                    'ZipCodeValue': '03060'
                                },
                            'State':
                                {
                                    'Name': 'NH'
                                }
                        },
                    'Id': 40356,
                    'Name': 'Buffalo Wild Wings Nashua'
                },
            'Latitude': Decimal('42.701605'),
            'SalesType': 'DITV',
            'SiteName': 'Buffalo Wild Wings Nashua',
            'IsTabletSite': True
        }
    ],
    test_schedule_for_weekly=[
        {
            'RepeatGame': 0,
            'PrimetimeScheduleID': 580,
            'GameNameID': 21,
            'GameNameIDEast': 21,
            'Country': 'US',
            'GameFileName': 'cinema-trivia',
            'DayID': 1,
            'GameNameIDWest': 0,
            'Game': 'Cinema Trivia',
            'TimeEastern': datetime.datetime(1899, 12, 30, 20, 0),
            'TimeCentral': datetime.datetime(1899, 12, 30, 19, 0),
            'TimeMountain': datetime.datetime(1899, 12, 30, 18, 0),
            'GameName': 'Cinema Trivia',
            'TimePacific': datetime.datetime(1899, 12, 30, 17, 0)
        }
    ],
    test_today_schedule=[
        {
            'TimeEastern': datetime.datetime(1899, 12, 30, 20, 0),
            'TimeCentral': datetime.datetime(1899, 12, 30, 19, 0),
            'GameNameID': 140,
            'TimePacific': datetime.datetime(1899, 12, 30, 17, 0),
            'Country': 'US',
            'GameNameIDEast': 140,
            'GameNameIDWest': 0,
            'GameName': 'SciFiles',
            'TimeMountain': datetime.datetime(1899, 12, 30, 18, 0),
            'Game': 'SciFiles',
            'PrimetimeScheduleID': 511,
            'DayID': 3,
            'GameFileName': 'scifiles',
            'RepeatGame': 0
        }
    ],
    test_specific_day_schedule=[
        {
            'game_icon_name': 'brainbuster',
            'central_time': datetime.datetime(1899, 12, 30, 19, 0),
            'country': 'US',
            'eastern_time': datetime.datetime(1899, 12, 30, 20, 0),
            'game_id': 510,
            'pacific_time': datetime.datetime(1899, 12, 30, 17, 0),
            'game': 'Brainbuster',
            'mountain_time': datetime.datetime(1899, 12, 30, 18, 0),
            'day': 'Tuesday'
        },
        {
            'game_icon_name': 'showdown',
            'central_time': datetime.datetime(1899, 12, 30, 19, 30),
            'country': 'US',
            'eastern_time': datetime.datetime(1899, 12, 30, 20, 30),
            'game_id': 509,
            'pacific_time': datetime.datetime(1899, 12, 30, 17, 30),
            'game': 'Showdown',
            'mountain_time': datetime.datetime(1899, 12, 30, 18, 30),
            'day': 'Tuesday'
        },
        {
            'game_icon_name': 'glory-daze',
            'central_time': datetime.datetime(1899, 12, 30, 20, 30),
            'country': 'US',
            'eastern_time': datetime.datetime(1899, 12, 30, 21, 30),
            'game_id': 508,
            'pacific_time': datetime.datetime(1899, 12, 30, 18, 30),
            'game': 'Glory Daze',
            'mountain_time': datetime.datetime(1899, 12, 30, 19, 30),
            'day': 'Tuesday'
        }
    ],
    test_forgot_password_mail=[
        {
            'Password': 1234,
            'Handle': 'xyz'
        }
    ],
    player_info={
        'State': None,
        'Address1': None,
        'TotalPlays': 1,
        'Handle': 'HASHER',
        'BirthDate': datetime.datetime(1987, 10, 10, 0, 0),
        'City': None,
        'SiteState': 'CA',
        'MobilePhoneNo': None,
        'Country': None,
        'LastPlayDate': datetime.datetime(2018, 4, 10, 0, 0),
        'PostalCode': '560100',
        'FirstName': None,
        'Email': '22631264@ntn.com',
        'PhoneNo': None,
        'Address2': None,
        'LastName': None,
        'TotalPoints': 5000,
        'PlayerID': 22631264,
        'Username': 'Hasher01',
        'FrequentGames': None,
        'MemberSince': datetime.datetime(2017, 5, 24, 4, 57, 5, 473000),
        'SiteCity': 'Carlsbad',
        'OrgName': 'NTN BUZZTIME #33433',
        'SiteID': '33433',
        'Sex': None,
        'PIN': '464014A',
        'SiteCountry': 'US'
    },
    login_player_prof=[
        {
            'Username': 'Hasher01',
            'PostalCode': '560100',
            'Pin': '464014A',
            'TotalPoints': 5000,
            'Email': '22631264@ntn.com',
            'BirthDate': datetime.datetime(1987, 10, 10, 0, 0),
            'AvatarCrc': 397504235,
            'Handle': 'HASHER',
            'SiteID': '33433',
            'MemberSince': datetime.datetime(2017, 5, 24, 4, 57, 5, 473000)
        }
    ],
    get_profile=[
        {
            'Handle': 'HASHER',
            'TotalPoints': 5000,
            'PostalCode': '560100',
            'SiteID': '33433',
            'Pin': '464014A',
            'AvatarCrc': 397504235,
            'Username': 'Hasher01',
            'BirthDate': datetime.datetime(1987, 10, 10, 0, 0),
            'MemberSince': datetime.datetime(2017, 5, 24, 4, 57, 5, 473000),
            'Email': '22631264@ntn.com'
        }
    ],
    get_mem_info=[
        {
            'TotalPlays': 1,
            'FirstName': None,
            'OrgName': 'NTN BUZZTIME #33433',
            'LastName': None,
            'SiteID': '33433',
            'MobilePhoneNo': None,
            'Country': None,
            'PostalCode': '560100',
            'SiteCountry': 'US',
            'FrequentGames': None,
            'PhoneNo': None,
            'TotalPoints': 5000,
            'City': None,
            'PlayerID': 22631264,
            'Handle': 'HASHER',
            'PIN': '464014A',
            'Email': '22631264@ntn.com',
            'MemberSince': datetime.datetime(2017, 5, 24, 4, 57, 5, 473000),
            'BirthDate': datetime.datetime(1987, 10, 10, 0, 0),
            'Address1': None,
            'Username': 'Hasher01',
            'Sex': None,
            'LastPlayDate': datetime.datetime(2018, 4, 14, 0, 0),
            'State': None,
            'SiteState': 'CA',
            'SiteCity': 'Carlsbad',
            'Address2': None
        }
    ],
    fetch_player_prof=[
        {
            'Username': 'Hasher01',
            'PostalCode': '560100',
            'Pin': '464014A',
            'TotalPoints': 5000,
            'Email': '22631264@ntn.com',
            'BirthDate': datetime.datetime(1987, 10, 10, 0, 0),
            'AvatarCrc': 397504235,
            'Handle': 'HASHER',
            'SiteID': '33433',
            'MemberSince': datetime.datetime(2017, 5, 24, 4, 57, 5, 473000)
        }
    ]
)
