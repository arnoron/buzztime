"""
Test cases for Players Base Class
"""
from django.test import TestCase
from rest_framework import status

from cheerios.players.test.test_data import response_status
from cheerios.players.views.players import PlayersViewSet


class TestPlayers(TestCase):
    """
    Test cases for the PlayerViewSet base class
    """

    def test_get_response_correct(self):
        """
        Test the case when the arguments are correct, should return 200
        :return:
        """
        players_obj = PlayersViewSet()
        response = players_obj. \
            execute_stored_procedure( \
            'UserDB.dbo.usp_sel_AchievementClassificationNameValuePairList', 1)
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    def test_get_resp_gen_exception(self):
        """
        Tests the case when database name is not valid, throws 500
        :return:
        """
        with self.assertRaises(Exception) as error:
            players_obj = PlayersViewSet()
            players_obj. \
                execute_stored_procedure( \
                '1.dbo.usp_sel_AchievementClassificationNameValuePairList', \
                (1,))
        self.assertTrue("1" in str(error.exception))

    def test_get_resp_op_error(self):
        """
        Tests the case when the database does not exists, throws Operation Error
        :return:
        """
        with self.assertRaises(Exception) as error:
            players_obj = PlayersViewSet()
            players_obj. \
                execute_stored_procedure( \
                'nodb.dbo.usp_sel_AchievementClassificationNameValuePairList', \
                (1,))
        self.assertTrue("nodb" in str(error.exception))

    def test_get_response_value_error(self):
        """
        Tests the case when an argument does not exists, throws 400 bad request
        :return:
        """
        players_obj = PlayersViewSet()
        response = players_obj.execute_stored_procedure('', (1,))
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
