"""
Common Methods for the basic test cases
"""

from cheerios.players.test.test_data import get_login_data


def login(self):
    """
    Logs in for a test case
    :return:
    """
    login_api_endpoint = '/players/login/'
    credentials = get_login_data().get('correct_creds')
    response_data = self.client.post(login_api_endpoint, credentials)
    return response_data
