"""
Test cases for Schedule api
"""
from mock import patch, Mock
from rest_framework import status
from rest_framework.response import Response
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import response_status, get_schedule_data
from cheerios.players.views.schedule import ScheduleBaseClass
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils

MOCK_STORED_PROCEDURE = 'cheerios.players.views.players.PlayersViewSet.execute_stored_procedure'
URL = dict(weekly='/players/schedule/weekly/',
           game='/players/schedule/game/',
           today='/players/schedule/today/',
           day='/players/schedule/day/')


class TestScheduleWeeklyViewSet(APITestCase):
    """
    Testcase for get operation.
    Test if data coming from api is in correct or not
    Test weekly schedule data
    """
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['test_schedule_for_weekly'])))
    def test_schedule_for_weekly(self):
        """
        Test If data from scheduled api is in correct format or not
        """
        response = self.client.get(URL['weekly'])
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    def test_get_weekday(self):
        """
        Test if passed integer gets Day value
        :return:
        """
        schedule_obj = ScheduleBaseClass()
        week_day_name = schedule_obj.get_weekday(2)
        self.assertEqual(week_day_name, 'Tuesday')

    def test_get_weekday_invalid_value(self):
        """
        Test if passed integer gets Day value
        :return:
        """
        schedule_obj = ScheduleBaseClass()
        week_day_name = schedule_obj.get_weekday(10)
        self.assertEqual(week_day_name, 'Invalid day')

    def test_get_weekday_empty_value(self):
        """
        Test if passed empty string what value will be returned.
        :return:
        """
        schedule_obj = ScheduleBaseClass()
        week_day_name = schedule_obj.get_weekday('')
        self.assertEqual(week_day_name, 'Invalid day')

    def test_format_schedule_data(self):
        """
        Test if data is mapped properly
        :return:
        """
        schedule_obj = ScheduleBaseClass()
        raw_schedule_obj = get_schedule_data()
        list_data = schedule_obj.format_response_data(raw_schedule_obj)
        self.assertEqual(list_data[0]['game_id'], raw_schedule_obj[0]['PrimetimeScheduleID'])

    def test_format_schedule_empty_data(self):
        """
        Test if data is mapped properly
        :return:
        """
        schedule_obj = ScheduleBaseClass()
        list_data = schedule_obj.format_response_data('')
        self.assertEqual(list_data, [])

    def test_format_schedule_str_data(self):
        """
        Test if data is mapped properly
        :return:
        """
        schedule_obj = ScheduleBaseClass()
        list_data = schedule_obj.format_response_data('hello test')
        self.assertEqual(list_data, [])


class TestScheduleTodayViewSet(APITestCase):
    """
    Testcase for get operation.
    Test if data coming from api is in correct or not
    Test today's schedule data
    """
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['test_today_schedule'])))
    def test_today_schedule(self):
        """
        Test If data from scheduled api is in correct format or not
        :return:
        """
        response = self.client.get(URL['today'])
        self.assertEqual(response_status(response), status.HTTP_200_OK)


class TestScheduleSpecificGameViewSet(APITestCase):
    """
    Testcase for get operation.
    Test if data coming from api is in correct or not
    Test today's schedule data
    """
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    def test_specific_game_schedule(self):
        """
        Test If data from scheduled api is in correct format or not
        :return:
        """
        response = self.client.get(URL['game'])
        self.assertEqual(response_status(response), status.HTTP_200_OK)


class TestScheduleDayViewSet(APITestCase):
    """
    Testcase for get operation.
    Test if data coming from api is in correct or not
    Test today's schedule data
    """
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['test_specific_day_schedule'])))
    def test_specific_day_schedule(self):
        """
        Test If data from scheduled api is in correct format or not
        :return:
        """
        response = self.client.get(URL['day'])
        self.assertEqual(response_status(response), status.HTTP_200_OK)
