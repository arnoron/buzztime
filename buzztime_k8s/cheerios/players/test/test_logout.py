from mock import patch, Mock
from redis.sentinel import SlaveNotFoundError
from rest_framework import status
from rest_framework.test import APITestCase


class TestLogout(APITestCase):
    """

    """

    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test_api_token", b"test_api_token"]))
    def test_auth_no_passed(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id=100))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, "Auth Token Error")

    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test_api_token", b"test_api_token"]))
    def test_incorrect_auth(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id=100),
                                    HTTP_AUTHORIZATION="incorrect_api_token")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, "Auth Token Error")

    def test_player_id_not_passed(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(),
                                    HTTP_AUTHORIZATION="correct_api_token")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'player_id': ['This field may not be null.']})

    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[None]))
    def test_player_id_incorrect(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id=100),
                                    HTTP_AUTHORIZATION="correct_api_token")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, "Auth Token Error")


    def test_player_id_string(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id="incorrect_player_id"),
                                    HTTP_AUTHORIZATION="correct_api_token")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'player_id': ['A valid integer is required.']})


    def test_player_id_special_char(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id="$#$%%$^%"),
                                    HTTP_AUTHORIZATION="correct_api_token")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'player_id': ['A valid integer is required.']})

    def test_player_id_with_quotes(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id="1234'"),
                                    HTTP_AUTHORIZATION="test_api_token")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'player_id': ['A valid integer is required.']})

    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=SlaveNotFoundError))
    def test_redis_inaccessible(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id=1234),
                                    HTTP_AUTHORIZATION="correct_api_token")
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.assertEqual(response.data, "Internal Server Error. Please try after some time")

    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test_api_token", b"test_api_token"]))
    @patch('cheerios.services.redis.RedisSession.remove_keys', Mock())
    def test_successful_logout(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id=1234),
                                    HTTP_AUTHORIZATION="test_api_token")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, "Player has been successfully logged out.")

    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test_api_token", b"test_api_token"]))
    @patch('cheerios.services.redis.RedisSession.remove_keys', Mock(side_effect=SlaveNotFoundError))
    def test_redis_down_while_removing_key(self):
        """

        :return:
        """
        response = self.client.post(path="/players/logout/", data=dict(player_id=1234),
                                    HTTP_AUTHORIZATION="test_api_token")
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.assertEqual(response.data, "Internal Server Error. Please retry after some time.")

