"""
Mock data for the unit tests
"""
import datetime
from contextlib import contextmanager
from cheerios.players.views.player.tablet_login import TabletLoginViewSet
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.utils import CheeriosUtils
UTIL_FUNCTION = CheeriosUtils()
MSSQLVIEW = MSSQLApiView()

@contextmanager
def db_connection(db_list):
    """
    Contex manager to create connection for given DB and close connection.
    """
    if isinstance(db_list, tuple):
        cursor_list = []
        for database in db_list:
            cursor_list.append(MSSQLVIEW.get_cursor(database))
    else:
        cursor_list = MSSQLVIEW.get_cursor(db_list)
    yield cursor_list
    MSSQLVIEW.close_cursor()


def get_current_utc_timestamp():
    """
    To return the current unix timestamp in utc zone
    :return:
    """
    return int(datetime.datetime.utcnow().timestamp())


def get_prepared_hash(player_id):
    """
    To return the hash value as sent by tablet
    :return:
    """
    tablet_login_interface = TabletLoginViewSet()
    web_secret = tablet_login_interface._get_web_secret()
    ts = get_current_utc_timestamp()
    tssalted = ts + (ts % (173 * 89))

    return UTIL_FUNCTION.create_md5_hash(str(player_id) +
                                         str(ts) + str(tssalted) +
                                         web_secret
                                        )


def response_headers(response):
    """
    To return the headers of a response
    :param response:
    :return:
    """
    return UTIL_FUNCTION.response_headers(response)


def response_data(response):
    """
    Common function which send response content from response.
    :param response:
    :return:
    """
    return response.content


def response_status(response):
    """
    Common function which send response status code from response.
    """
    return response.status_code


def get_schedule_data():
    """
    Function will return hardcoded schedule data
    :return:
    """
    return [{'TimeMountain': datetime.datetime(1899, 12, 30, 18, 30),
             'GameFileName': 'showdown',
             'PrimetimeScheduleID': 509,
             'GameNameID': 29,
             'GameNameIDEast': 29,
             'TimeCentral': datetime.datetime(1899, 12, 30, 19, 30),
             'Country': 'US',
             'Game': 'Showdown',
             'TimeEastern': datetime.datetime(1899, 12, 30, 20, 30),
             'DayID': 2,
             'GameName': 'Showdown',
             'RepeatGame': 0,
             'TimePacific': datetime.datetime(1899, 12, 30, 17, 30),
             'GameNameIDWest': 0
            },
            {'TimeMountain': datetime.datetime(1899, 12, 30, 19, 30),
             'GameFileName': 'glory-daze',
             'PrimetimeScheduleID': 508,
             'GameNameID': 139,
             'GameNameIDEast': 139,
             'TimeCentral': datetime.datetime(1899, 12, 30, 20, 30),
             'Country': 'US',
             'Game': 'Glory Daze',
             'TimeEastern': datetime.datetime(1899, 12, 30, 21, 30),
             'DayID': 2,
             'GameName': 'Glory Daze',
             'RepeatGame': 0,
             'TimePacific': datetime.datetime(1899, 12, 30, 18, 30),
             'GameNameIDWest': 0
            }
           ]


def get_achievement_mode_data():
    """
    Returns hardcoded data for the achievement mode classification API
    :return: Hardcoded data
    """
    return [{'AchievementClassificationName': 'Animals',
             'AchievementClassificationID': 1},
            {'AchievementClassificationName': 'Arts',
             'AchievementClassificationID': 2},
            {'AchievementClassificationName': 'Countdown',
             'AchievementClassificationID': 11},
            {'AchievementClassificationName': 'Food & Bev',
             'AchievementClassificationID': 3},
            {'AchievementClassificationName': 'Language',
             'AchievementClassificationID': 4},
            {'AchievementClassificationName': 'Lunchtime Trivia',
             'AchievementClassificationID': 12},
            {'AchievementClassificationName': 'Mixed',
             'AchievementClassificationID': 5},
            {'AchievementClassificationName': 'Religion',
             'AchievementClassificationID': 6},
            {'AchievementClassificationName': 'Science',
             'AchievementClassificationID': 7},
            {'AchievementClassificationName': 'Sports',
             'AchievementClassificationID': 8},
            {'AchievementClassificationName': 'The Late Shift',
             'AchievementClassificationID': 13},
            {'AchievementClassificationName': 'USA',
             'AchievementClassificationID': 9},
            {'AchievementClassificationName': 'World',
             'AchievementClassificationID': 10}]


def get_sites_data():
    """
    Returns hardcoded data for the events API
    :return:
    """
    return [
        {
            'accuracy': 100,
            'OrgName': "Silver Buckle Inn",
            'EventEndTime': None,
            'OptLiveMusic': 0,
            'State': "AB",
            'PowerPageImage1': "",
            'SalesType': "DITV",
            'isClassicSite': 0,
            'EventStartTime': None,
            'Address1': "687 South Railway",
            'Distance': 1216.6558743251503,
            'PriPhone': "(403) 526-1303",
            'latitude': 50.0326,
            'OptFullMenu': 0,
            'PowerPageDescription': "",
            'OptKidFriendly': 0,
            'PowerPageURLTwitter': "",
            'OptFullBar': 0,
            'FeatureEnabled': None,
            'EventID': None,
            'siteid': 67,
            'isTabletSite': 1,
            'PowerPageImage3': "",
            'PowerPageURLMyspace': "",
            'PowerPageLogo': "67_logo.png",
            'EventTypeID': None,
            'EventStatusID': None,
            'PowerPageURL': "http://thebuckle.ca/",
            'OptFreeWifi': 0,
            'PowerPageID': 5453,
            'pDisplayAddress': True,
            'Chain': "",
            'City': "Medicine Hat",
            'Zip': "T1A 2V8",
            'PowerPageImage2': "",
            'ReviewCount': 0,
            'PowerPageURLFacebook': "",
            'Opt21AndOver': 0,
            'EventDayID': None,
            'longitude': -110.6667
        }
    ]


def get_events_unprocessed_data():
    """
    Returns unprocessed events data to pass to the post process method
    :return:
    """
    return [{'Distance': 100, \
            'SiteName': "Silver Buckle Inn", \
            'Latitude': 50.0326, \
            'PowerPage': {'Image1': "",
                          'URL': "http://thebuckle.ca/",
                          'Image3': "",
                          'URLTwitter': "",
                          'OptLiveMusic': 0,
                          'PowerPageID': 5453,
                          'OptFullMenu': 0,
                          'OptFullBar': 0,
                          'OptKidFriendly': 0,
                          'OptFreeWifi': 0,
                          'Description': "",
                          'Image2': "",
                          'URLMyspace': "",
                          'Opt21AndOver': 0,
                          'URLFacebook': "",
                          'Logo': "67_logo.png"
                         }, \
            'Longitude': -110.6667, \
            'Phone': "(403) 526-1303", \
            'SalesType': "DITV", \
            'IsTabletSite': True, \
            'SiteEvents': {'SiteEvent': [{"EventEndTime": "9:30 am",
                                          "EventStartTime": "6:00 PM",
                                          "EventTypeId": 45,
                                          "EventId": 2671335,
                                          "EventDayId": 2,
                                          "EventType": "BuzztimeLiveTrivia",
                                          "EventStatusId": 0,
                                          "SiteId": 40105
                                         },
                                         {"EventEndTime": "9:30 am",
                                          "EventStartTime": "6:00 PM",
                                          "EventTypeId": 45,
                                          "EventId": 2671336,
                                          "EventDayId": 0,
                                          "EventType": "BuzztimeLiveTrivia",
                                          "EventStatusId": 0,
                                          "SiteId": 40105
                                         }
                                        ]
                          } \
           }]

def get_login_data():
    """
    Returns the mock data for the login API
    :return:
    """
    return dict(correct_creds=dict(username='hasher01',
                                   password='6f8be3fe9ecb2feffb23536b22b9866f'
                                  ),
                incorrect_password=dict(username='Hasher',
                                        password='6c6101f1098dcd55fb23d28cd32f7e77'
                                       ),
                incorrect_username=dict(username='Hasher_incorrect',
                                        password='6c6101f1098dcd55fb23d28cd32f7e66'
                                       ),
                blank_creds=dict(username='',
                                 password=''
                                ),
                special_char_creds=dict(username='!@#$%^&*()',
                                        password='!@#$%^&*()'
                                       ),
                player_id=22631264,
                username='Hasher01'
               )

def get_smartestbar_json():
    """
        Returns the mock data for the smartestbar dropdown API
        :return:
        """
    return { \
              "playerCompetitions": [ \
                { \
                  "weekId": 0, \
                  "desc": "Overall", \
                  "competitionId": 8894 \
                }, { \
                  "weekId": 1, \
                  "desc": "Week 1 10/4", \
                  "competitionId": 8890, \
                  "startDate": "2015-10-04" \
                }, { \
                  "weekId": 2, \
                  "desc": "Week 2 10/11", \
                  "competitionId": 8891, \
                  "startDate": "2015-10-11" \
                }, { \
                  "weekId": 3, \
                  "desc": "Week 3 10/18", \
                  "competitionId": 8892, \
                  "startDate": "2015-10-18" \
                }, { \
                  "weekId": 4, \
                  "desc": "Week 4 10/25", \
                  "competitionId": 8893, \
                  "startDate": "2015-10-25" \
                }], \
              "siteCompetitions": [ \
                { \
                  "weekId": 0, \
                  "desc": "Overall", \
                  "competitionId": 8899 \
                }, { \
                  "weekId": 1, \
                  "desc": "Week 1 10/4", \
                  "competitionId": 8895, \
                  "startDate": "2015-10-04" \
                }, { \
                  "weekId": 2, \
                  "desc": "Week 2 10/11", \
                  "competitionId": 8896, \
                  "startDate": "2015-10-11" \
                }, { \
                  "weekId": 3, \
                  "desc": "Week 3 10/18", \
                  "competitionId": 8897, \
                  "startDate": "2015-10-18" \
                }, { \
                  "weekId": 4, \
                  "desc": "Week 4 10/25", \
                  "competitionId": 8898, \
                  "startDate": "2015-10-25" \
                } \
              ] \
            }
