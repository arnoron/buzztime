"""
mocked Data
"""
from decimal import Decimal

MOCK_RESPONSE = dict(
    test_badge_with_name=[
        {
            "value": 123,
            "name": "#@%$!"
        },
        {
            "value": 117,
            "name": "X to Z"
        },
        {
            "value": 151,
            "name": "Aha!"
        },
        {
            "value": 127,
            "name": "Ahoy Matey"
        },
        {
            "value": 98,
            "name": "Animation Nation"
        }
    ],
    test_badge_with_rarity=[
        {
            "value": 1,
            "name": "Common"
        },
        {
            "value": 2,
            "name": "Not So Common"
        },
        {
            "value": 4,
            "name": "Very Scarce"
        },
        {
            "value": 8,
            "name": "Unique"
        }
    ],
    test_badge_with_type=[
        {
            "name": "Game Play",
            "value": 6
        },
        {
            "name": "Perfect Score",
            "value": 5
        },
        {
            "name": "PlayersPlus Bonus",
            "value": 7
        }
    ],
    test_badge_with_category=[
        {
            "name": "Animals",
            "value": 1
        },
        {
            "name": "Arts",
            "value": 2
        },
        {
            "name": "Countdown",
            "value": 11
        }
    ],
    test_badge_valid_class=[
        {
            'AchievementName': 'WWJD',
            'AchievementID': 141
        },
        {
            'AchievementName': 'Zen Master',
            'AchievementID': 139
        },
        {
            'AchievementName': 'Zoo Keeper',
            'AchievementID': 96
        }
    ],
    test_info=[
        {
            'AchievementRarityID': 8,
            'LastAchievementDate': None,
            'AchievementLevelName': '',
            'AchievementName': 'Cocktail',
            'AchievementTypeID': 6,
            'GameName': 'The Late Shift',
            'AchievementDescription': "Let's get this party started!",
            'AchievementRarityPercent': Decimal('0.000000'),
            'AchievementClassificationName': 'The Late Shift',
            'ImageFileName': 'cocktail',
            'AchievementTypeName': 'Game Play',
            'AchievementRarityName': 'Unique',
            'AchievementLevelArtName': '',
            'EarnedCount': 0,
            'AchievementClassificationID': 13,
            'AchievementID': 189
        }
    ],
    test_event=[],
    test_badge_list=[
        {
            'AchievementRarityID': 8,
            'LastAchievementDate': None,
            'AchievementTypeName': 'PlayersPlus Bonus',
            'AchievementLevelArtName': '',
            'AchievementDescription': "You're obviously nocturnal"
                                      " to get a bonus like that!",
            'ImageFileName': 'night-owl',
            'AchievementRarityEndPercentile': 100,
            'EarnedCount': 0,
            'AchievementID': 192,
            'AchievementLevelName': '',
            'AchievementName': 'Night Owl',
            'AchievementClassificationID': 13,
            'AchievementClassificationName': 'The Late Shift',
            'AchievementRarityName': 'Unique',
            'AchievementRarityPercent': Decimal('0.000000')
        }
    ],

)
