"""
Holds the test cases for the badgeclassification class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import get_achievement_mode_data
from cheerios.players.views.badge.badge_classification import BadgeClassification
from cheerios.test_utils import mock_test_utils
from .mock_response import MOCK_RESPONSE


class TestBadgeClassification(APITestCase):
    """
    Tests the badge classification module
    """

    def setUp(self):
        """
        Initializes the badge classification object
        :return:
        """
        self.badge_classification_interface = BadgeClassification()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_badge_valid_class(self, cursor):
        """
        Tests the badge classification with valid class
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_badge_valid_class'])
        classification_type = 'name'
        response = self.badge_classification_interface.\
            get_badge_on_classification(classification_type)
        self.assertEqual(response.status_code, 200)

    def test_badge_invalid_class(self):
        """
        Tests the badge classification with invalid class
        :return:
        """
        classification_type = 'xyz'
        response = self.badge_classification_interface.\
            get_badge_on_classification(classification_type)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, 'Incorrect stored procedure passed')

    def test_format_response(self):
        """
        Tests the format response data
        :return:
        """
        raw_schedule_obj = get_achievement_mode_data()
        achievement_obj = BadgeClassification()
        achievement_obj.selected_mode = 'category'
        list_data = achievement_obj.format_response_data(raw_schedule_obj)
        self.assertEqual(list_data[0]['name'], raw_schedule_obj[0]['AchievementClassificationName'])
