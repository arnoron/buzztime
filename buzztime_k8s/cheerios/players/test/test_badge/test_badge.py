"""
Holds the test cases for the badge API
"""
import json
from mock import patch

from rest_framework.test import APITestCase

from cheerios.players.test.test_data import response_status, response_data

from .mock_response import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils 

API_ENDPOINT = '/players/badges/'


class TestBadge(APITestCase):
    """
    Test cases for the badge class
    """
    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_badge_with_name(self, cursor):
        """
        Test badge with class parameter
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
                             MOCK_RESPONSE['test_badge_with_name'])
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?classification-type=name'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('value' in json_response_data[0])
        self.assertTrue('name' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_badge_with_rarity(self, cursor):
        """
        Test badge with class parameter
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
                             MOCK_RESPONSE['test_badge_with_rarity'])
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?classification-type=rarity'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('value' in json_response_data[0])
        self.assertTrue('name' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_badge_with_type(self, cursor):
        """
        Test badge with class parameter
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
                             MOCK_RESPONSE['test_badge_with_rarity'])
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?classification-type=type'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('value' in json_response_data[0])
        self.assertTrue('name' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_badge_with_category(self, cursor):
        """
        Test badge with class parameter
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
                             MOCK_RESPONSE['test_badge_with_rarity'])
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?classification-type=category'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('value' in json_response_data[0])
        self.assertTrue('name' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    def test_badge_with_invalid_class(self):
        """
        Test badge with invalid class passed
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?classification-type=xyz'))
        self.assertEqual(response_status(response), 400)
        self.assertEqual(response.data,"Incorrect stored procedure passed")

    def test_badge_with_empty_class(self):
        """
        Test badge with no class passed
        :return:
        """
        response = self.client.get(API_ENDPOINT)
        self.assertEqual(response_status(response), 400)
        self.assertEqual(response.data,"ValueError Invalid Parameter passed")
