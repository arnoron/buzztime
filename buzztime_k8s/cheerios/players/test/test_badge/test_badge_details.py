"""
Holds the test cases for the badgedetails class
"""

from mock import patch
from rest_framework.test import APITestCase
from cheerios.players.views.badge.badge_details import BadgeDetail
from cheerios.test_utils import mock_test_utils
from .mock_response import MOCK_RESPONSE


class TestBadgeDetails(APITestCase):
    """
    Tests the badge details module
    """

    def setUp(self):
        """
        Initialize the badge details object
        :return:
        """
        self.badge_detail_interface = BadgeDetail()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_badge_details_valid(self, fetch_all_mock, connection_manager_mock):
        """
        Tests the get badge details with valid values
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        fetch_all_mock.side_effect = [MOCK_RESPONSE['test_info'], MOCK_RESPONSE['test_event']]

        player_id = 220404
        badge_id = 189

        response = self.badge_detail_interface.get_badge_details(player_id, badge_id)
        self.assertEqual(response.status_code, 200)

    def test_badge_details_player_null(self):
        """
         Tests the get badge details with null player id
        :return:
        """
        player_id = None
        badge_id = 189

        response = self.badge_detail_interface.get_badge_details(player_id, badge_id)
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.data, 'Invalid parameters passed')

    def test_badge_details_badge_null(self):
        """
         Tests the get badge details with null badge id
        :return:
        """
        player_id = 220404
        badge_id = None

        response = self.badge_detail_interface.get_badge_details(player_id, badge_id)
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.data, 'Invalid parameters passed')

    def test_badge_player_invalid(self):
        """
         Tests the get badge details with invalid player id
        :return:
        """
        player_id = '?%*'
        badge_id = 189

        response = self.badge_detail_interface.get_badge_details(player_id, badge_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, 'Invalid parameters passed')

    def test_badge_badge_invalid(self):
        """
         Tests the get badge details with invalid badge id
        :return:
        """
        player_id = 220404
        badge_id = '?%*'

        response = self.badge_detail_interface.get_badge_details(player_id, badge_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, 'Invalid parameters passed')
