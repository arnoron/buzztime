"""
Holds the test cases for the badgelist class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.badge.badge_list import BadgeList
from .mock_response import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestBadgeList(APITestCase):
    """
    Tests the badge list module
    """

    def setUp(self):
        """
        Initializes the badge list object
        :return:
        """
        self.badge_list_interface = BadgeList()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_badge_list_all_badges(self, fetch_all_mock, connection_manager_mock):
        """
        Tests the badge list with all badges option
        :return:
        """

        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        fetch_all_mock.side_effect = [MOCK_RESPONSE['test_badge_list']]
        player_id = 220404
        badge_type = 'all'

        response = self.badge_list_interface.get_badge_list(player_id, badge_type)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_badge_list_recent_badges(self, cursor):
        """
        Tests the badge list with recent badges option
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['test_event'])
        player_id = 220404
        badge_type = 'recent'

        response = self.badge_list_interface.get_badge_list(player_id, badge_type)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_badge_list_rare_badges(self, cursor, connection_manager_mock):
        """
        Tests the badge list with rare badges option
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_badge_list']]
        player_id = 220404
        badge_type = 'rare'

        response = self.badge_list_interface.get_badge_list(player_id, badge_type)
        self.assertEqual(response.status_code, 200)

    def test_badge_invalid_badge(self):
        """
        Tests the badge list with invalid badge type
        :return:
        """
        player_id = 220404
        badge_type = 'xyz'

        response = self.badge_list_interface.get_badge_list(player_id, badge_type)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Incorrect or no badge type parameter passed."
                                        " Badge type will be either 'all', 'rare' or 'recent'")


    def test_badge_list_player_id_null(self):
        """
        Tests the badge list with null player id
        :return:
        """
        player_id = None
        badge_type = 'all'

        response = self.badge_list_interface.get_badge_list(player_id, badge_type)
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.data, 'Incorrect player_id parameter passed None')

    def test_badge_invalid_player(self):
        """
        Tests the badge list with invalid player id
        :return:
        """
        player_id = '%$&'
        badge_type = 'all'

        response = self.badge_list_interface.get_badge_list(player_id, badge_type)
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.data, 'Incorrect player_id parameter passed %$&')

