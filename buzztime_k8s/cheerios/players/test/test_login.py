"""
Test cases for login API
"""
import json

from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import get_login_data
from cheerios.players.views.login import LoginViewSet
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestLoginViewSet(APITestCase):
    """
    Test cases for login viewset
    """

    def setUp(self):
        """
        Function to initialize class scope variables
        """
        self.login_api_endpoint = '/players/login/'
        self.login_interface = LoginViewSet()
        self.data = get_login_data()

    def get_response_data(self, credentials):
        """
        Does a post API call and returns the data
        :param credentials:
        :return:
        """
        return self.client.post(self.login_api_endpoint, credentials)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['auth_api'], MOCK_RESPONSE['fetch_player_prof'])))
    @patch('cheerios.services.redis.RedisSession.save_data', Mock())
    @patch('cheerios.services.redis.RedisSession.get_value', Mock())
    def test_login_player(self):
        """
        Test for valid login player case
        :return:
        """
        credentials = self.data.get('correct_creds')
        response_data = self.get_response_data(credentials)
        self.assertEqual(response_data.status_code, 200)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data['player_id'], self.data.get('player_id'))
        self.assertEqual(json_response_data['username'], 'Hasher01')

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(None)))
    @patch('cheerios.services.redis.RedisSession.save_data', Mock())
    def test_login_incorrect_password(self):
        """
        Test for login player with incorrect password
        :return:
        """
        credentials = self.data.get('incorrect_password')
        response_data = self.get_response_data(credentials)
        self.assertEqual(response_data.status_code, 200)

        json_response_data = json.loads(response_data.content.decode("utf-8"))

        self.assertEqual(json_response_data, "Invalid credentials")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(None)))
    @patch('cheerios.services.redis.RedisSession.save_data', Mock())
    def test_login_incorrect_username(self):
        """
        Test for login player with incorrect username
        :return:
        """
        credentials = self.data.get('incorrect_username')
        response_data = self.get_response_data(credentials)

        self.assertEqual(response_data.status_code, 200)

        json_response_data = json.loads(response_data.content.decode("utf-8"))

        self.assertEqual(json_response_data, "Invalid credentials")

    def test_login_player_blank_fields(self):
        """
        Test for login player with blank username and password
        :return:
        """
        credentials = self.data.get('blank_creds')
        response_data = self.get_response_data(credentials)
        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data, "ValueError Invalid Parameter passed")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(None)))
    @patch('cheerios.services.redis.RedisSession.save_data', Mock())
    def test_login_special_characters(self):
        """
        Test for login player with some absurd creds
        :return:
        """
        credentials = self.data.get('special_char_creds')
        response_data = self.get_response_data(credentials)

        self.assertEqual(response_data.status_code, 200)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data, "Invalid credentials")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['login_player_prof'])))
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch('cheerios.services.redis.RedisSession.save_data', Mock())
    def test_get_player_profile(self):
        """
        Test for function which takes in a player id and returns its profile
        :return:
        """
        player_profile = self.login_interface.get_player_profile(self.data.get('player_id'))
        self.assertEqual(player_profile['player_id'], self.data.get('player_id'))
        self.assertEqual(player_profile['username'], self.data.get('username'))
