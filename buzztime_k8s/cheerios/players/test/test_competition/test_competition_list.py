"""
Holds the test cases for the competition list class
"""

from mock import patch
from rest_framework.test import APITestCase
from cheerios.players.views.competition.competition_list import CompetitionList
from cheerios.test_utils import mock_test_utils
from .response_data import MOCK_RESPONSE


class TestSitePlayerList(APITestCase):
    """
    Tests the site player module
    """

    def setUp(self):
        """
        Initializes the competition list object
        :return:
        """
        self.competition_list_interface = CompetitionList()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_competition_valid_site(self, cursor):
        """
        Tests the competition list with valid site id
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['site_data'])
        site_id = 39089

        response = self.competition_list_interface.get_competition_list(site_id)
        self.assertEqual(response.status_code, 200)

    def test_competition_invalid_site(self):
        """
        Tests the competition list with invalid site id
        :return:
        """
        site_id = 'xyz'
        response = self.competition_list_interface.get_competition_list(site_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Site ID is mandatory")

    def test_competition_empty_site(self):
        """
        Tests the competition list with no site id
        :return:
        """
        site_id = None

        response = self.competition_list_interface.get_competition_list(site_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Site ID is mandatory")
