"""
Holds the test cases for the competition details class
"""

from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.views.competition.competition_details import CompetitionDetails
from cheerios.test_utils import mock_test_utils
from .response_data import MOCK_RESPONSE


class TestCompetitionDetails(APITestCase):
    """
    Tests the competition details module
    """

    def setUp(self):
        """
        Initializes the competition details object
        :return:
        """
        self.competition_details_interface = CompetitionDetails()

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_competition_valid(self, fetch):
        """
        Tests competition details with valid values
        :return:
        """
        fetch.side_effect = mock_test_utils.mock_fetch_responses(
            MOCK_RESPONSE['location'],
            MOCK_RESPONSE['leader'],
            MOCK_RESPONSE['game']
            )
        competition_id = 10555

        response = self.competition_details_interface.get_competition_details(competition_id)
        self.assertEqual(response.status_code, 200)

    def test_competition_invalid(self):
        """
        Tests competition details with invalid competition id
        :return:
        """
        competition_id = 'xyz'

        response = self.competition_details_interface.get_competition_details(competition_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Parameters passed")

    def test_competition_empty_competition(self):
        """
        Tests competition details with no competition id
        :return:
        """
        competition_id = None

        response = self.competition_details_interface.get_competition_details(competition_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Parameters passed")