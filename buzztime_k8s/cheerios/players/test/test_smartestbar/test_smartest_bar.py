"""
Holds the test cases for the smartestbar class
"""
import json

from rest_framework.test import APITestCase
from cheerios.players.constants import SMARTESTBAR_URL
from cheerios.players.test.test_data import response_status, response_data
from mock import patch, Mock
from rest_framework.response import Response
MOCK_STORED_PROCEDURE = 'cheerios.players.views.players.PlayersViewSet.execute_stored_procedure'


class TestSmartestBar(APITestCase):
    """
    Tests the site search module
    """
    @patch(MOCK_STORED_PROCEDURE, Mock(return_value=Response([])))
    def test_successful_transaction(self):
        """
        Tests the smartest bar with successful transaction
        :return:
        """
        url = "{0}?competition_id=10275".format(SMARTESTBAR_URL)
        response = self.client.get(url)
        self.assertEqual(response_status(response), 200)

    def test_empty_competition_id(self):
        """
        Tests the smartest bar with no competition id
        :return:
        """
        url = SMARTESTBAR_URL
        response = self.client.get(url)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), 400)
        self.assertEqual(json_response_data, {'competition_id': ['This field is required.']})

    def test_string_competition_id(self):
        """
        Tests the smartest bar with competition id as string
        :return:
        """
        url = "{0}?competition_id=exynos".format(SMARTESTBAR_URL)
        response = self.client.get(url)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), 400)
        self.assertEqual(json_response_data, {'competition_id': ['A valid integer is required.']})

    def test_negative_competition_id(self):
        """
        Tests the smartest bar with negative competition id
        :return:
        """
        url = "{0}?competition_id=-34332".format(SMARTESTBAR_URL)
        response = self.client.get(url)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), 400)
        self.assertEqual(json_response_data, {'competition_id':
                                              ['Ensure this value is greater '
                                               'than or equal to 0.'
                                              ]
                                             }
                        )

    def test_string_count(self):
        """
        Tests the smartest bar with count as string
        :return:
        """
        url = "{0}?competition_id=10275&count=exynos".format(SMARTESTBAR_URL)
        response = self.client.get(url)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), 400)
        self.assertEqual(json_response_data, {'count': ['A valid integer is required.']})

    def test_negative_count(self):
        """
        Tests the smartest bar with no competition id
        :return:
        """
        url = "{0}?competition_id=10275&count=-10".format(SMARTESTBAR_URL)
        response = self.client.get(url)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(response_status(response), 400)
        self.assertEqual(json_response_data, {'count': ['Ensure this value '
                                                        'is greater than or equal to 0.']})
