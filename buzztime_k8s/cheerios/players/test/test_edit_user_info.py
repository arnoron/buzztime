"""
Test cases for Edit User Info API
"""
import json

from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.test.base import login
from cheerios.players.test.test_data import response_headers
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestEditUserViewSet(APITestCase):
    """
    Tests the Edit User Info API
    """

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['auth_api'], MOCK_RESPONSE['fetch_player_prof'],
               MOCK_RESPONSE['auth_api'], MOCK_RESPONSE['fetch_player_prof'],
               MOCK_RESPONSE['auth_api'], MOCK_RESPONSE['fetch_player_prof'],
               MOCK_RESPONSE['auth_api'], MOCK_RESPONSE['fetch_player_prof'])))
    @patch('cheerios.services.redis.RedisSession.save_data', Mock())
    def setUp(self):
        """
        Function to initialize class scope variables
        """
        self.edit_user_api_endpoint = '/players/edit_user_info/'
        self.session_id = "test-session-id"

    def get_response_data(self, data):
        """
        Does a post API call and returns the data
        :param credentials:
        :return:
        """
        return self.client.post\
            (self.edit_user_api_endpoint, data, HTTP_AUTHORIZATION=self.session_id)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['get_profile'],
               MOCK_RESPONSE['get_mem_info'],
               [{'UserNameExists': 0}],
               [{'EmailExists': 0}],
           )))
    @patch('cheerios.services.redis.RedisSession.get_key', Mock())
    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test-session-id",
                                                                               b"test-session-id",
                                                                               MOCK_RESPONSE['bad_words']]))
    def test_username_bad_word(self):
        """
        Tests username containing inappropriate word
        :return:
        """
        request_data = dict(player_id="22631264",
                            display_name="Test Name",
                            username="SATAN",
                            password="password",
                            postal_code="000000",
                            email="testemail@test.com",
                           )

        response_data = self.get_response_data(request_data)
        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(self.get_username_error_message(json_response_data),
                         "SATAN contains inappropriate text")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['get_profile'],
               MOCK_RESPONSE['get_mem_info'],
               [{'UserNameExists': 0}],
               [{'EmailExists': 0}])))
    @patch('cheerios.services.redis.RedisSession.get_key', Mock())
    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test-session-id",
                                                                               b"test-session-id",
                                                                               MOCK_RESPONSE['bad_words']]))
    def test_display_name_bad_word(self):
        """
        Tests display name containing inappropriate word
        :return:
        """
        request_data = dict(player_id="22631264",
                            display_name="Idiot",
                            username="SATAN",
                            password="password",
                            postal_code="000000",
                            email="testemail@test.com",
                           )

        response_data = self.get_response_data(request_data)
        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(self.get_username_error_message(json_response_data),
                         "SATAN contains inappropriate text")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['get_profile'],
               MOCK_RESPONSE['get_mem_info'],
               [{'UserNameExists': 1}],
               [{'EmailExists': 0}])))
    @patch('cheerios.services.redis.RedisSession.get_key', Mock())
    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test-session-id",
                                                                               b"test-session-id",
                                                                               MOCK_RESPONSE['bad_words']]))
    def test_duplicate_username(self):
        """
        Tests when the posted username has a duplicate in the db
        :return:
        """
        request_data = dict(player_id="22631264",
                            display_name="Hasher",
                            username="Hasher",
                            password="password",
                            postal_code="000000",
                            email="testemail@test.com",
                           )

        response_data = self.get_response_data(request_data)
        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(self.get_username_error_message(json_response_data),
                         "Username is already taken")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['get_profile'],
               MOCK_RESPONSE['get_mem_info'],
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_key', Mock())
    @patch('cheerios.services.redis.RedisSession.get_value', Mock(side_effect=[b"test-session-id",
                                                                               b"test-session-id",
                                                                               MOCK_RESPONSE['bad_words']]))
    def test_duplicate_email(self):
        """
        Tests when the posted email has a duplicate in the db
        :return:
        """
        request_data = dict(player_id="22631264",
                            display_name="Test name",
                            username="Testname1",
                            password="password",
                            postal_code="000000",
                            email="masroor.h@hashedin.com",
                            dob="1-1-1990"
                           )

        response_data = self.get_response_data(request_data)
        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data['email_errors']['message'],
                         "Email is already taken")

    def get_username_error_message(self, data):
        """
        Returns the username error message
        :param self:
        :return:
        """
        return data['username_errors']['message']
