"""
Holds the test cases for the top trivia player class
"""
from mock import patch, Mock
from rest_framework.test import APITestCase
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils
from cheerios.players.views.player.top_trivia_player import TopTriviaPlayer


class TestTopTriviaPlayer(APITestCase):
    """
    Tests the top trivia player module
    """

    def setUp(self):
        """
        Initializes the top trivia player module
        :return:
        """
        self.top_trivia_player_interface = TopTriviaPlayer()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_trivia_players_valid(self, fetch_all_mock, connection_manager_mock):
        """
        Tests the top trivia players with valid values
        :return:
        """
        fetch_all_mock.side_effect = [MOCK_RESPONSE['trivia_player']]
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()

        state = 'AL'
        handle = 'Dave'
        game = 87

        response = self.top_trivia_player_interface.get_top_trivia_players(game, state, handle)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_trivia_players_empty_state(self, fetch_all_mock, connection_manager_mock):
        """
        Tests the top trivia players with no state param
        :return:
        """

        fetch_all_mock.side_effect = [MOCK_RESPONSE['trivia_player']]
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()

        state = None
        handle = 'Dave'
        game = 87

        response = self.top_trivia_player_interface.get_top_trivia_players(game, state, handle)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_trivia_players_empty_handle(self, fetch_all_mock, connection_manager_mock):
        """
        Tests the top trivia players with no handle param
        :return:
        """

        fetch_all_mock.side_effect = [MOCK_RESPONSE['trivia_player']]
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()

        state = 'AL'
        handle = None
        game = 87

        response = self.top_trivia_player_interface.get_top_trivia_players(game, state, handle)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    def test_players_invalid_state(self):
        """

        :return:
        """
        state = 107
        handle = 'john'
        game = 187

        response = self.top_trivia_player_interface.get_top_trivia_players(game, state, handle)
        self.assertEqual(response.status_code, 200)

    def test_players_invalid_handle(self):
        """
        Tests the top trivia players with invalid handle
        :return:
        """
        state = 'AR'
        handle = 107
        game = 187

        response = self.top_trivia_player_interface.get_top_trivia_players(game, state, handle)
        self.assertEqual(response.data, "Invalid parameter passed for handle")
        self.assertEqual(response.status_code, 400)

    def test_players_invalid_game(self):
        """
        Tests the top trivia players with invalid game
        :return:
        """
        state = 'AR'
        handle = 'john'
        game = '%$&'

        response = self.top_trivia_player_interface.get_top_trivia_players(game, state, handle)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed")
