"""
Holds the test cases for the player details class
"""
from mock import patch
from .response_data import MOCK_RESPONSE
from rest_framework.test import APITestCase

from cheerios.players.views.player.player_details import PlayerDetails
from cheerios.test_utils import mock_test_utils


class TestPlayerDetails(APITestCase):
    """
    Tests the player details module
    """

    def setUp(self):
        """
        Initializes the player details object
        :return:
        """
        self.player_details_interface = PlayerDetails()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_details_valid_player(self, cursor, connection_manager_mock):
        """
        Tests the player details with valid player id
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['get_member_info'],
                              MOCK_RESPONSE['total_count'],
                              MOCK_RESPONSE['sport_stats'],
                              MOCK_RESPONSE['game_averages'],
                              MOCK_RESPONSE['casino_stats'],
                              MOCK_RESPONSE['casino_stats']]

        player_id = 206276

        response = self.player_details_interface.get_player_details(player_id)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_details_valid_email(self, cursor, connection_manager_mock):
        """
        Tests the player details with valid email id
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['get_member_info'],
                              MOCK_RESPONSE['total_count'],
                              MOCK_RESPONSE['sport_stats'],
                              MOCK_RESPONSE['game_averages'],
                              MOCK_RESPONSE['casino_stats'],
                              MOCK_RESPONSE['casino_stats']]

        player_id = None
        email_id = 'email_check@devbuzz.com'

        response = self.player_details_interface.get_player_details(player_id, email_id)
        self.assertEqual(response.status_code, 200)

    def test_details_empty_values(self):
        """
        Tests the player details with without values
        :return:
        """
        player_id = None
        email_id = None

        response = self.player_details_interface.get_player_details(player_id, email_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "No parameters passed for player_id and email")

    def test_details_invalid_player(self):
        """
        Tests the player details with invalid player id
        :return:
        """
        player_id = '$%^'

        response = self.player_details_interface.get_player_details(player_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed")

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_details_empty_averages(self, cursor, connection_manager_mock):
        """
        Tests the player details when all the averages are 0
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['get_member_info'],
                              MOCK_RESPONSE['total_count'],
                              MOCK_RESPONSE['sport_stats'],
                              MOCK_RESPONSE['game_averages_empty'],
                              MOCK_RESPONSE['casino_stats'],
                              MOCK_RESPONSE['casino_stats']]

        player_id = 30302809

        response = self.player_details_interface.get_player_details(player_id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0].get('BadgeCount'), 0)
        self.assertEqual(response.data[0].get('NationalTop100Count'), 0)
        self.assertEqual(response.data[0].get('LocalWinsCount'), 0)
        self.assertEqual(response.data[0].get('NationalGoldCount'), 0)
        self.assertEqual(response.data[0].get('NationalSilverCount'), 0)
        self.assertEqual(response.data[0].get('NationalBronzeCount'), 0)
