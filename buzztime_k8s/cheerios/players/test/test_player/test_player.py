"""
Holds the integration test cases for the player API
"""
import json

from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import response_data, response_status
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import  mock_test_utils

API_ENDPOINT = '/players/players/'


class TestPlayer(APITestCase):
    """
    Tests the players API
    """
    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_search(self, cursor, connection_manager_mock):
        """
        Tests the player search
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_player_search']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?search=john'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_num_badges_sort(self, fetch):
        """
        Tests players with num_badges sort params
        :return:
        """
        fetch.side_effect = mock_test_utils.mock_fetch_responses(
            MOCK_RESPONSE['test_player_num_badges_sort'])
        response = self.client.get(API_ENDPOINT + '?sort=num-badges-desc')
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_playerplus_sort(self, cursor, connection_manager_mock):
        """
        Tests players with playerplus points sort params
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_player_playerplus_sort']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?sort=playerplus-points-desc'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_trivia_average_sort(self, cursor, connection_manager_mock):
        """
        Tests players with trivia average sort params
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_player_trivia_average_sort']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?sort=trivia-average-desc'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    def test_player_invalid_sort(self):
        """
        Tests players with invalid sort params
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?sort=xyz'))
        self.assertEqual(response.data, "Invalid Sorting Criteria passed")
        self.assertEqual(response_status(response), 400)

    def test_player_invalid_query_param(self):
        """
        Tests players with invalid query params
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?page=2'))
        self.assertEqual(response.data, "Invalid query parameter passed")
        self.assertEqual(response_status(response), 400)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_badge_id(self, cursor, connection_manager_mock):
        """
        Tests players with badge id
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_info'], MOCK_RESPONSE['test_event']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '206276/badges/187'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('AchievementID' in json_response_data['badge_info'][0])
        self.assertTrue('AchievementName' in json_response_data['badge_info'][0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_badge_type(self, cursor, connection_manager_mock):
        """
        Tests players badges with badge type
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_player_badge_type']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '206276/badges/?type=rare'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('AchievementID' in json_response_data[0])
        self.assertTrue('AchievementName' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_games(self, cursor, connection_manager_mock):
        """
        Tests players games
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_player_games']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '206276/games'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerCount' in json_response_data[0])
        self.assertTrue('Description' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_instance_id(self, cursor, connection_manager_mock):
        """
        Tests the player game instance with id
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_player_instance_id']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '206276/gameinstances/0/'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('Game' in json_response_data[0])
        self.assertTrue('Score' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_recent_instance(self, cursor, connection_manager_mock):
        """
        Tests players recent game instance
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['test_player_recent_instance']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '206276/gameinstances/'
                                                                 '?type=recent'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('Game' in json_response_data[0])
        self.assertTrue('Score' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_casino_instance(self, cursor, connection_manager_mock):
        """
        Tests players casino game instance
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [[]]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT,
                                   '206276/gameinstances/?type=casino&game=holdem'))
        self.assertEqual(response_status(response), 200)

    def test_player_invalid_instance(self):
        """
        Tests players with invalid game instance type
        :return:
        """
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '206276/gameinstances/?type=xyz'))
        self.assertEqual(response.data, "Invalid game instance type passed")
        self.assertEqual(response_status(response), 400)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_details(self, cursor, connection_manager_mock):
        """
        Tests player details
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['get_member_info'],
                              MOCK_RESPONSE['total_count'],
                              MOCK_RESPONSE['sport_stats'],
                              MOCK_RESPONSE['game_averages'],
                              MOCK_RESPONSE['casino_stats'],
                              MOCK_RESPONSE['casino_stats']]
        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '206276/'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_player_details_email(self, cursor, connection_manager_mock):
        """
        Tests player details when email passed
        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        cursor.side_effect = [MOCK_RESPONSE['get_member_info'],
                              MOCK_RESPONSE['total_count'],
                              MOCK_RESPONSE['sport_stats'],
                              MOCK_RESPONSE['game_averages'],
                              MOCK_RESPONSE['casino_stats'],
                              MOCK_RESPONSE['casino_stats']]

        response = self.client.get('{0}{1}'.format(API_ENDPOINT, '?email=masroor.h@hashedin.com'))
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertTrue('PlayerID' in json_response_data[0])
        self.assertTrue('Handle' in json_response_data[0])
        self.assertEqual(response_status(response), 200)
