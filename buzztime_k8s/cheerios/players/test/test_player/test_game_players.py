"""
Holds the test cases for the game players class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.player.game_players import GamePlayers
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestGamePlayers(APITestCase):
    """
    Tests the game players module
    """

    def setUp(self):
        """
        Initializes the game players object
        :return:
        """
        self.game_players_interface = GamePlayers()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_player_scores_game_id(self, cursor):
        """
        Tests the player scores api with premium game id param
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_player_scores_game_id'])
        premium_game_id = 119
        handle = ''
        site = ''
        city = ''
        state = ''
        lower_limit = ''
        upper_limit = ''

        response = self.game_players_interface.get_game_players(premium_game_id, handle, site, city,
                                                                state, lower_limit, upper_limit)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_player_scores_loc_params(self, cursor):
        """
        Tests the player scores api with location params
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_player_scores_loc_params'])
        premium_game_id = 119
        handle = ''
        site = ''
        city = 'Dothan'
        state = 'AL'
        lower_limit = ''
        upper_limit = ''

        response = self.game_players_interface.get_game_players(premium_game_id, handle, site, city,
                                                                state, lower_limit, upper_limit)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_player_scores_with_limits(self, cursor):
        """
        Tests the player scores api with limits params
        :return:
        """

        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_player_scores_with_limits'])
        premium_game_id = 119
        handle = ''
        site = ''
        city = ''
        state = ''
        lower_limit = '4'
        upper_limit = '10'

        response = self.game_players_interface.get_game_players(premium_game_id, handle, site, city,
                                                                state, lower_limit, upper_limit)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_player_scores_with_handle(self, cursor):
        """
        Tests the player scores api with game id param
        :return:
        """

        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_player_scores_with_handle'])
        premium_game_id = 119
        handle = 'John'
        site = ''
        city = ''
        state = ''
        lower_limit = ''
        upper_limit = ''

        response = self.game_players_interface.get_game_players(premium_game_id, handle, site, city,
                                                                state, lower_limit, upper_limit)
        self.assertEqual(response.status_code, 200)
