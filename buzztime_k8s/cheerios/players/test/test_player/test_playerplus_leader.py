"""
Holds the test cases for the playerplus leader class
"""
from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.views.player.playerplus_leader import PlayerPlusLeader
from cheerios.test_utils import mock_test_utils
from .response_data import MOCK_RESPONSE


class TestPlayerplusLeader(APITestCase):
    """
    Tests the playerplus leader module
    """

    def setUp(self):
        """
        Initializes the player plus leader object
        :return:
        """
        self.playerplus_leader_interface = PlayerPlusLeader()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_leaders_valid(self, cursor):
        """
        Tests the get playerplus leaders with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['player_plus'])
        state = 'AB'
        handle = 'eda'
        response = self.playerplus_leader_interface.get_playerplus_leaders(state, handle)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['plus_empty_state'])))
    def test_leaders_empty_state(self):
        """
        Tests the get playerplus leaders with no state param
        :return:
        """
        state = None
        handle = 'LESTER'

        response = self.playerplus_leader_interface.get_playerplus_leaders(state, handle)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses(
               MOCK_RESPONSE['plus_empty_handle'])))
    def test_leaders_empty_handle(self):
        """
        Tests the get playerplus leaders with no handle
        :return:
        """
        state = 'AB'
        handle = None

        response = self.playerplus_leader_interface.get_playerplus_leaders(state, handle)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    def test_leaders_invalid_state(self):
        """
        Tests the get playerplus leaders with invalid state param
        :return:
        """
        state = 107
        handle = 'john'

        response = self.playerplus_leader_interface.get_playerplus_leaders(state, handle)
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    def test_leaders_invalid_handle(self):
        """
        Tests the get playerplus leaders with invalid handle
        :return:
        """
        state = 'AR'
        handle = 107

        response = self.playerplus_leader_interface.get_playerplus_leaders(state, handle)
        self.assertEqual(response.status_code, 200)

    @patch('cheerios.players.views.players.PlayersViewSet.execute_stored_procedure',
           Mock(side_effect=Exception))
    def test_leaders_sc_handle(self):
        """
        Tests the get playerplus leaders with invalid handle
        :return:
        """
        state = 'AR'
        handle = '$*^'

        response = self.playerplus_leader_interface.get_playerplus_leaders(state, handle)
        self.assertEqual(response.data, 'Invalid parameters passed state and handle')
        self.assertEqual(response.status_code, 400)
