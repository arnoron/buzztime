"""
Holds the test cases for the site game instance players class
"""
from mock import patch
from rest_framework.test import APITestCase
from cheerios.players.views.player.site_game_instance_players import SiteGameInstancePlayers
from cheerios.test_utils import mock_test_utils


class TestSiteGameInstancePlayers(APITestCase):
    """
    Tests the site game instance players
    """

    def setUp(self):
        """
        Initializes the site game instance player interface
        :return:
        """
        self.players_interface = SiteGameInstancePlayers()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_players_valid(self, cursor):
        """
        Tests the site game instance player with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses([])
        site_id = 1110
        game_id = 137
        start_date = '30-03-2017'
        start_time = '18:30'

        response = self.players_interface.\
            get_site_gameinstance_players(site_id,
                                          game_id,
                                          start_date,
                                          start_time
                                         )
        self.assertEqual(response.status_code, 200)

    def test_players_invalid_site(self):
        """
        Tests the site game instance player with invalid site id
        :return:
        """
        site_id = '$%^'
        game_id = 25
        start_date = '30-03-2017'
        start_time = '18:30'

        response = self.players_interface. \
            get_site_gameinstance_players(site_id,
                                          game_id,
                                          start_date,
                                          start_time
                                          )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid or no parameters passed")

    def test_players_invalid_game(self):
        """
        Tests the site game instance player with invalid game id
        :return:
        """
        site_id = 10
        game_id = '@$%'
        start_date = '30-03-2017'
        start_time = '18:30'
        response = self.players_interface. \
            get_site_gameinstance_players(site_id,
                                          game_id,
                                          start_date,
                                          start_time
                                          )
        self.assertEqual(response.data, "Invalid or no parameters passed")
        self.assertEqual(response.status_code, 400)

    def test_players_invalid_strtdate(self):
        """
        Tests the site game instance player with invalid start date
        :return:
        """
        site_id = 10
        game_id = 25
        start_date = 'xyz'
        start_time = '18:30'

        response = self.players_interface.\
            get_site_gameinstance_players(site_id,
                                          game_id,
                                          start_date,
                                          start_time
                                         )
        self.assertEqual(response.data, "Date or Time does not match the intended format. "
                                        "Date format is dd-mm-yyyy and time format is hh:mm")
        self.assertEqual(response.status_code, 400)

    def test_players_invalid_strttime(self):
        """
        Tests the site game instance player with invalid start time
        :return:
        """
        site_id = 10
        game_id = 25
        start_date = '30-03-2017'
        start_time = 'xyz'

        response = self.players_interface.\
            get_site_gameinstance_players(site_id,
                                          game_id,
                                          start_date,
                                          start_time
                                         )
        self.assertEqual(response.data, "Date or Time does not match the intended format."
                                        " Date format is dd-mm-yyyy and time format is hh:mm")
        self.assertEqual(response.status_code, 400)

    def test_players_empty_site(self):
        """
        Tests the site game instance player with no site id
        :return:
        """
        site_id = None
        game_id = 25
        start_date = '30-03-2017'
        start_time = '18:30'

        response = self.players_interface.\
            get_site_gameinstance_players(site_id,
                                          game_id,
                                          start_date,
                                          start_time
                                         )
        self.assertEqual(response.data, "Invalid or no parameters passed")
        self.assertEqual(response.status_code, 400)

    def test_players_empty_game(self):
        """
        Tests the site game instance player with no game id
        :return:
        """
        site_id = 10
        game_id = None
        start_date = '30-03-2017'
        start_time = '18:30'

        response = self.players_interface.\
            get_site_gameinstance_players(site_id,
                                          game_id,
                                          start_date,
                                          start_time
                                         )
        self.assertEqual(response.data, "Invalid or no parameters passed")
        self.assertEqual(response.status_code, 400)
