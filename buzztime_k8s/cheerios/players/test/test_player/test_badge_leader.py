# To be uncommented when sleep is removed from the Dev DB for the SP
#
# """
# Holds the test cases for the bsdge leader class
# """
from mock import Mock, patch
from rest_framework.test import APITestCase
from cheerios.players.views.player.badge_leader import BadgeLeader
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestBadgeLeader(APITestCase):
    """
    Tests the badge leader module
    """

    def setUp(self):
        """
        Initializes the badge leader object
        :return:
        """
        self.badge_leader_interface = BadgeLeader()

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_badge_leaders_all_values(self, fetch):
        """
        Tests the badge leaders with valid values
        :return:
        """
        fetch.side_effect = mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['badge_leaders'])
        start_date = '01-04-2018'
        end_date = '13-04-2018'
        badge_classifier = 'name'
        badge_type = '15'
        response = self.badge_leader_interface.get_badge_leaders_response(start_date,
                                                                          end_date,
                                                                          badge_classifier,
                                                                          badge_type,
                                                                         )
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_badge_leaders_no_dates(self, fetch):
        """
        Tests the badge leaders without dates
        :return:
        """
        fetch.side_effect = mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['badge_leaders'])
        start_date = None
        end_date = None
        badge_classifier = 'name'
        badge_type = '15'

        response = self.badge_leader_interface.get_badge_leaders_response(start_date,
                                                                          end_date,
                                                                          badge_classifier,
                                                                          badge_type,
                                                                         )
        self.assertEqual(response.status_code, 200)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_badge_leaders_no_values(self, fetch):
        """
        Tests the badge leaders without values
        :return:
        """
        fetch.side_effect = mock_test_utils.mock_fetch_responses([])
        start_date = None
        end_date = None
        badge_classifier = None
        badge_type = None

        response = self.badge_leader_interface.get_badge_leaders_response(start_date,
                                                                          end_date,
                                                                          badge_classifier,
                                                                          badge_type,
                                                                         )
        self.assertEqual(response.status_code, 200)

    def test_badge_leaders_invalid_dates(self):
        """
        Tests the badge leaders with invalid dates
        :return:
        """
        start_date = '%$^'
        end_date = 'xyz'
        badge_classifier = None
        badge_type = None

        response = self.badge_leader_interface.get_badge_leaders_response(start_date,
                                                                          end_date,
                                                                          badge_classifier,
                                                                          badge_type,
                                                                         )
        self.assertEqual(response.data, "Date is not in the correct format. "
                                        "Should in format dd-mm-yyyy")
        self.assertEqual(response.status_code, 400)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_badge_leaders_invalid_class(self, fetch):
        """
        Tests the badge leaders with invalid classifier
        :return:
        """
        fetch.side_effect = mock_test_utils.mock_fetch_responses([])
        start_date = None
        end_date = None
        badge_classifier = 'xyz'
        badge_type = None

        response = self.badge_leader_interface.get_badge_leaders_response(start_date,
                                                                          end_date,
                                                                          badge_classifier,
                                                                          badge_type,
                                                                         )
        self.assertEqual(response.status_code, 200)
