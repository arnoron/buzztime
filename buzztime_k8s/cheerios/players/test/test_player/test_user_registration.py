"""
Test cases for Register API
"""
import json
import datetime

from mock import patch, Mock
from requests import Response
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.services.check_inappropriate_word import InAppropriateWordViewSet

from cheerios.players.test.test_data import response_data
from cheerios.players.views.player.user_registration import UserRegistrationViewSet
from cheerios.services.log_classifier import Logger
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.utils import CheeriosUtils
from cheerios.test_utils import mock_test_utils
CHERRIOS_UTILS = CheeriosUtils()

MOCK_POST = 'requests.post'
MOCK_JSON = 'requests.models.Response.json'


class TestUserRegistrationViewSet(APITestCase):
    """
    Tests the Register API
    """

    def setUp(self):
        """
        Function to initialize class scope variables
        """
        self.registration_api_endpoint = '/players/players/'
        self.registration_interface = UserRegistrationViewSet()
        self.bypass_captcha = Response()
        self.bypass_captcha._content = b"{'success':True}"

    def get_response_data(self, data):
        """
        Does a post API call and returns the data
        :param credentials:
        :return:
        """
        return self.client.post(self.registration_api_endpoint, data)

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 0}],)))
    @patch('cheerios.services.redis.RedisSession.get_value',
                side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_username_invalid_word(self, mock):
        """
        Tests username containing inappropriate word
        :return:
        """
        request_data = dict(display_name="Test Name",
                            username="SATAN",
                            password="password",
                            postal_code="000000",
                            email="testemail@test.com",
                            dob="1-1-1990",
                            opt_in=False
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data['username_errors']['message'], \
                         "SATAN contains inappropriate text")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 0}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_name_inappropriate_word(self, mock):
        """
        Tests display name containing inappropriate word
        :return:
        """
        request_data = dict(display_name="Idiot",
                            username="SATAN",
                            password="password",
                            postal_code="000000",
                            email="testemail169@test.com",
                            dob="1-1-1990",
                            site_id=123,
                            opt_in=False
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data['username_errors']['message'], \
                         "SATAN contains inappropriate text")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 1}],
               [{'EmailExists': 0}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_duplicate_username(self, mock):
        """
        Tests when the posted username has a duplicate in the db
        :return:
        """
        request_data = dict(display_name="Hasher",
                            username="Hasher",
                            password="password",
                            postal_code="000000",
                            email="testemail@test.com",
                            dob="1-1-1990",
                            site_id=123,
                            opt_in=False
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data['username_errors']['message'], \
                         "Username is already taken")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_duplicate_email(self, mock):
        """
        Tests when the posted email has a duplicate in the db
        :return:
        """
        request_data = dict(display_name="Test name",
                            username="Testname165",
                            password="password",
                            postal_code="000000",
                            email="mazz1@hashedin.com",
                            dob="1-1-1990",
                            site_id=123,
                            opt_in=False
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data['email_errors']['message'], "Email is already taken")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_without_opt_in(self, mock):
        """
        Tests API without passing Opt_in
        :return:
        """
        request_data = dict(display_name="Test name",
                            username="Testname165",
                            password="password",
                            postal_code="000000",
                            email="mazz1@hashedin.com",
                            dob="1-1-1990",
                            site_id=123
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_without_display_name(self, mock):
        """
        Tests API without passing display name
        :return:
        """
        request_data = dict(username="Testname165",
                            password="password",
                            postal_code="000000",
                            email="mazz1@hashedin.com",
                            dob="1-1-1990",
                            opt_in=False,
                            site_id=123
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_without_password(self, mock):
        """
        Tests API without passing password
        :return:
        """
        request_data = dict(display_name="Test name",
                            username="Testname165",
                            postal_code="000000",
                            email="mazz1@hashedin.com",
                            dob="1-1-1990",
                            opt_in=False,
                            site_id=123
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_without_postal_code(self, mock):
        """
        Tests API without passing postal code
        :return:
        """
        request_data = dict(display_name="Test name",
                            username="Testname165",
                            password="password",
                            email="mazz1@hashedin.com",
                            dob="1-1-1990",
                            opt_in=False,
                            site_id=123
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_without_email(self, mock):
        """
        Tests API without passing email
        :return:
        """
        request_data = dict(display_name="Test name",
                            username="Testname165",
                            password="password",
                            postal_code="000000",
                            dob="1-1-1990",
                            opt_in=False,
                            site_id=123
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_without_dob(self, mock):
        """
        Tests API without passing date of birth
        :return:
        """
        request_data = dict(display_name="Test name",
                            username="Testname165",
                            password="password",
                            postal_code="000000",
                            email="mazz1@hashedin.com",
                            opt_in=False,
                            site_id=123
                           )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data,
                         "ValueError Invalid Parameter passed")

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_load_inappropriate_words(self, mock):
        """
        Tests the load_inappropriate_words module
        :return:
        """
        inappropriate_word_interface = InAppropriateWordViewSet()
        dirty_words_dict = inappropriate_word_interface.load_inappropriate_words()
        self.assertTrue('dirty_words' in dirty_words_dict)
        self.assertTrue('exceptions' in dirty_words_dict)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 1}])))
    def test_username_exists(self):
        """
        Tests the username_exists module
        :return:
        """
        username_exists = self.client.get('/players/check_username_availability/?username=Hasher')
        json_response_data = json.loads(response_data(username_exists).decode("utf-8"))
        self.assertEqual(json_response_data, True)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}])))
    def test_username_not_exists(self):
        """
        Tests the username_not_exists module
        :return:
        """
        username_not_exists = self.client. \
            get('/players/check_username_availability/?username=lkhfgr')
        json_response_data = json.loads(response_data(username_not_exists).decode("utf-8"))
        self.assertEqual(json_response_data, False)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'NamespaceIdentity': 'rahul'}])))
    def test_generate_user_name(self):
        """
        Tests the generate_user_name module
        :return:
        """
        cursor = ''
        user_name = self.registration_interface.generate_user_name('RAHUL', cursor)
        self.assertEqual(user_name, "RAHUL10")

    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'NamespaceIdentity': 'john1120'}])))
    def test_get_user_names(self):
        """
        Tests the get_user_names module, Dummy cursor object
        :return:
        """
        cursor = ''
        usernames = self.registration_interface.get_user_names('John', cursor)
        self.assertEqual(usernames[0], "JOHN1120")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'NamespaceIdentity': 'john1120'}],
               [{'UserNameExists': 0}],
               [{'EmailExists': 1}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_empty_username(self, mock):
        """
        No username passed
        :return:
        """
        request_data = dict(display_name="Test name",
                            password="password",
                            postal_code="000000",
                            email="masroor.h@hashedin.com",
                            dob="1-1-1990",
                            opt_in=False,
                            site_id=123 \
                            )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data['email_errors']['message'], "Email is already taken")

    @patch(MOCK_POST, Mock(return_value=Response()))
    @patch(MOCK_JSON, Mock(return_value={'success': True}))
    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               [{'UserNameExists': 0}],
               [{'EmailExists': 0}])))
    @patch('cheerios.services.redis.RedisSession.get_value',
           side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_inappropriate_display_name(self, mock):
        """
        No username passed
        :return:
        """
        request_data = dict(display_name="SATAN",
                            username="Testname169",
                            password="password",
                            postal_code="000000",
                            email="masroor.h12@hashedin.com",
                            dob="1-1-1990",
                            opt_in=False,
                            site_id=123 \
                            )

        response = self.get_response_data(request_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data['display_name_errors']['message'], \
                         "SATAN contains inappropriate text")

    def test_create_md5_hash(self):
        """
        Test create md5 hash
        :return:
        """
        hashed_key = CHERRIOS_UTILS.create_md5_hash('abcde')
        self.assertEqual(hashed_key, 'ab56b4d92b40713acc5af89985d4b786')

    def test_get_random_salt(self):
        """
        Tests the get random salt method
        :return:
        """
        salt = self.registration_interface.get_random_salt_value()
        self.assertTrue(salt)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([{'EmailExists': 0}])))
    def test_email_availability_false(self):
        """
        Tests when the email is available
        :return:
        """
        email_check_api = '/players/check_email_availability/?email=ghjre@fc.com'
        response = self.client.get(email_check_api)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data, False)

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses([{'EmailExists': 1}])))
    def test_email_availability_true(self):
        """
        Tests when the email is not available
        :return:
        """
        email_check_api = '/players/check_email_availability/?email=masroor.h@hashedin.com'
        response = self.client.get(email_check_api)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data, True)

    def test_newsletter_subscription(self):
        """
        Testing newsletter_subscription.
        As user already subscribed mail it will return SUBPEND with some id.
        :return:
        """
        user_data = dict(display_name="Test name",
                         username="Testname165",
                         password="password",
                         postal_code="000000",
                         email="mazz{0}@hashedin.com".format(datetime.datetime.now()),
                         opt_in=False,
                         site_id=123,
                         dob="1-1-1990"
                        )

        self.registration_interface.newsletter_subscription(
            user_data,
            123456
            )

    def test_old_date_of_birth(self):
        """

        :return:
        """
        user_data = dict(display_name="Test name",
                         username="Testname165",
                         password="password",
                         postal_code="000000",
                         email="mazz{0}@hashedin.com".format(datetime.datetime.now()),
                         opt_in=False,
                         site_id=123,
                         dob="1-1-1875",
                         mobile=True
                         )
        response = self.get_response_data(user_data)
        json_response_data = json.loads(response_data(response).decode("utf-8"))
        self.assertEqual(json_response_data, "Date of Birth year should be greater than 1900")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # @patch(mock_test_utils.GET_CURSOR, Mock())
    # @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    # @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
    #        Mock(side_effect=mock_test_utils.mock_fetch_responses(
    #            [{'UserNameExists': 0}],
    #            [{'EmailExists': 0}], [{'PlayerID': '1', 'PIN': '1', 'username': 'user_name'}],
    #            [{'OrgName': 'test_org_name'}])))
    # def test_user_registration_md5_password(self):
    #     """
    #     test user registration with passing md5 password with version 1.0
    #     :return:
    #     """
    #     user_data = dict(display_name="Test name",
    #                      username="Testname165",
    #                      password="5f4dcc3b5aa765d61d8327deb882cf99",
    #                      postal_code="000000",
    #                      email="mazz{0}@hashedin.com".format(datetime.datetime.now()),
    #                      opt_in=False,
    #                      site_id=123,
    #                      dob="1-1-1990",
    #                      mobile=True,
    #                      api_version="1.0"
    #                      )
    #     response = self.get_response_data(user_data)
    #     json_response_data = json.loads(response_data(response).decode("utf-8"))
    #     Logger.log.error("below")
    #     Logger.log.error(json_response_data)
    #     self.assertEqual(json_response_data['player_id'], "1")
    #     self.assertEqual(json_response_data['username'], "Testname165")
    #     self.assertEqual(response.status_code, 200)
    #
    # @patch(mock_test_utils.GET_CURSOR, Mock())
    # @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    # @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
    #        Mock(side_effect=mock_test_utils.mock_fetch_responses(
    #            [{'UserNameExists': 0}],
    #            [{'EmailExists': 0}], [{'PlayerID': '1', 'PIN': '1', 'username': 'user_name'}],
    #            [{'OrgName': 'test_org_name'}])))
    # def test_user_registration(self):
    #     """
    #     test user registration with passing password ( without md5 ) with version 1.1
    #     :return:
    #     """
    #     user_data = dict(display_name="Test name",
    #                      username="Testname116",
    #                      password="password",
    #                      postal_code="000000",
    #                      email="mazz{0}@hashedin.com".format(datetime.datetime.now()),
    #                      opt_in=False,
    #                      site_id=123,
    #                      dob="1-1-1990",
    #                      mobile=True,
    #                      api_version="1.1"
    #                      )
    #     response = self.get_response_data(user_data)
    #     json_response_data = json.loads(response_data(response).decode("utf-8"))
    #     self.assertEqual(json_response_data['player_id'], "1")
    #     self.assertEqual(json_response_data['username'], "Testname116")
    #     self.assertEqual(response.status_code, 200)
    #
    # @patch(mock_test_utils.GET_CURSOR, Mock())
    # @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    # @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
    #        Mock(side_effect=mock_test_utils.mock_fetch_responses(
    #            [{'UserNameExists': 0}],
    #            [{'EmailExists': 0}], [{'PlayerID': '1', 'PIN': '1', 'username': 'user_name'}],
    #            [{'OrgName': 'test_org_name'}])))
    # def test_user_registration_without_version(self):
    #     """
    #     test user registration with passing password  without version ( default 1.0 )
    #     :return:
    #     """
    #     user_data = dict(display_name="Test name",
    #                      username="Testname116",
    #                      password="5f4dcc3b5aa765d61d8327deb882cf99",
    #                      postal_code="000000",
    #                      email="mazz{0}@hashedin.com".format(datetime.datetime.now()),
    #                      opt_in=False,
    #                      site_id=123,
    #                      dob="1-1-1990",
    #                      mobile=True
    #                      )
    #     response = self.get_response_data(user_data)
    #     json_response_data = json.loads(response_data(response).decode("utf-8"))
    #     self.assertEqual(json_response_data['player_id'], "1")
    #     self.assertEqual(json_response_data['username'], "Testname116")
    #     self.assertEqual(response.status_code, 200)
    #
    # @patch(mock_test_utils.GET_CURSOR, Mock())
    # @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    # @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
    #        Mock(side_effect=mock_test_utils.mock_fetch_responses(
    #            [{'UserNameExists': 0}],
    #            [{'EmailExists': 0}], [{'PlayerID': '1', 'PIN': '1', 'username': 'user_name'}],
    #            [{'OrgName': 'test_org_name'}])))
    # def test_user_registration_invalid_version(self):
    #     """
    #     test user registration with passing password  with invalid version
    #     :return:
    #     """
    #     user_data = dict(display_name="Test name",
    #                      username="Testname116",
    #                      password="5f4dcc3b5aa765d61d8327deb882cf99",
    #                      postal_code="000000",
    #                      email="mazz{0}@hashedin.com".format(datetime.datetime.now()),
    #                      opt_in=False,
    #                      site_id=123,
    #                      dob="1-1-1990",
    #                      mobile=True,
    #                      api_version="5.7"
    #                      )
    #     response = self.get_response_data(user_data)
    #     json_response_data = json.loads(response_data(response).decode("utf-8"))
    #     self.assertEqual(json_response_data, "ValueError Invalid Parameter passed")
    #     self.assertEqual(response.status_code, 400)
