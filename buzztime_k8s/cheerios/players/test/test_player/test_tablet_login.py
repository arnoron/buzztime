"""
This module tests the tablet login functionality
"""
import json

from mock import patch, Mock
from rest_framework.test import APITestCase

from cheerios.players.test.test_data import get_current_utc_timestamp, get_prepared_hash
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestTabletLogin(APITestCase):
    """
    This class contains the tests for the tablet login
    """

    def setUp(self):
        """
        Function to initialize class scope variables
        """
        self.login_api_endpoint = '/players/tablet_login/'

    def get_response_data(self, data):
        """
        Does a post API call and returns the data
        :param credentials:
        :return:
        """
        return self.client.post(self.login_api_endpoint, data)

    def test_pid_string(self):
        """
        Tests the case when pid is a string
        :return:
        """
        data = dict(player_id='abcd',
                    prepared_hash='c20c310e4ad6dfe5800c4478e29a33ce',
                    timestamp=get_current_utc_timestamp())

        response_data = self.get_response_data(data)

        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data, {"player_id": ["A valid integer is required."]})

    def test_timestamp_string(self):
        """
        Tests the case when timestamp is a string
        :return:
        """
        data = dict(player_id='30325601',
                    prepared_hash='c20c310e4ad6dfe5800c4478e29a33ce',
                    timestamp='abcd')

        response_data = self.get_response_data(data)

        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data, {"timestamp": ["A valid integer is required."]})

    def test_pid_missing(self):
        """
        Tests the case when pid is missing
        :return:
        """
        data = dict(prepared_hash='c20c310e4ad6dfe5800c4478e29a33ce',
                    timestamp=get_current_utc_timestamp())

        response_data = self.get_response_data(data)

        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data, {"player_id": ["This field is required."]})

    def test_timestamp_missing(self):
        """
        Tests the case when timestamp is missing
        :return:
        """
        data = dict(player_id='30325601',
                    prepared_hash='c20c310e4ad6dfe5800c4478e29a33ce')

        response_data = self.get_response_data(data)

        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data, {"timestamp": ["This field is required."]})

    def test_hash_missing(self):
        """
        Tests the case when prepared hash is missing
        :return:
        """
        data = dict(player_id='30325601',
                    timestamp=get_current_utc_timestamp())

        response_data = self.get_response_data(data)

        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data, {"prepared_hash": ["This field is required."]})

    def test_timeout(self):
        """
        Tests the case when timestamp is expired
        :return:
        """
        data = dict(player_id='30325601',
                    prepared_hash='c20c310e4ad6dfe5800c4478e29a33ce',
                    timestamp='1499942519')

        response_data = self.get_response_data(data)

        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))
        self.assertEqual(json_response_data,
                         {"timestamp": ["Timeout error. Timestamp is expired."]}
                        )

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(
               MOCK_RESPONSE['web_secret'],
               MOCK_RESPONSE['web_secret'],
               MOCK_RESPONSE['test_successful_login'])))
    @patch('cheerios.services.redis.RedisSession.save_data', Mock())
    def test_successful_login(self):
        """
        Tests the case when the login is successful
        :return:
        """

        data = dict(player_id='206276',
                    prepared_hash=get_prepared_hash(206276),
                    timestamp=get_current_utc_timestamp())

        response_data = self.get_response_data(data)
        self.assertEqual(response_data.status_code, 200)
        json_response_data = response_data.json()
        self.assertEqual(json_response_data['player_id'], '206276')

    @patch(mock_test_utils.GET_CURSOR, Mock())
    @patch(mock_test_utils.CALL_STORED_PROCEDURE, Mock())
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
           Mock(side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['web_secret'])))
    def test_hash_not_matching(self):
        """
        Tests the case when the hash does not match
        :return:
        """
        data = dict(player_id='30325601',
                    prepared_hash='xyz',
                    timestamp=get_current_utc_timestamp())

        response_data = self.get_response_data(data)

        self.assertEqual(response_data.status_code, 400)

        json_response_data = json.loads(response_data.content.decode("utf-8"))

        self.assertEqual(json_response_data, 'Hash does not match')
