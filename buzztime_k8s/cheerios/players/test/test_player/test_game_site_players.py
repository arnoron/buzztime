"""
Holds the test cases for the game site players class
"""
from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.views.player.game_site_players import GameSitePlayers
from .response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestGameSitePlayers(APITestCase):
    """
    Tests the game site players
    """

    def setUp(self):
        """
        Initializes the game site players object
        :return:
        """
        self.game_site_players_interface = GameSitePlayers()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_gamesite_players_valid(self, cursor):
        """
        Tests the game site players with valid values
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_gamesite_players_valid'])
        game_id = 54
        site_id = 1110

        response = self.game_site_players_interface.get_game_site_players(game_id, site_id)
        self.assertEqual(response.status_code, 200)

    def test_gamesite_invalid_game_id(self):
        """
        Tests the game site players with invalid game id
        :return:
        """
        game_id = '%$*'
        site_id = 25
        response = self.game_site_players_interface.get_game_site_players(game_id, site_id)
        self.assertEqual(response.data, "Invalid Parameters Passed")
        self.assertEqual(response.status_code, 400)

    def test_gamesite_invalid_site_id(self):
        """
        Tests the game site players with invalid site id
        :return:
        """
        game_id = 10
        site_id = '&#$'

        response = self.game_site_players_interface.get_game_site_players(game_id, site_id)
        self.assertEqual(response.data, "Invalid Parameters Passed")
        self.assertEqual(response.status_code, 400)

    def test_gamesite_empty_game_id(self):
        """
        Tests the game site players with no game id
        :return:
        """
        game_id = None
        site_id = 25

        response = self.game_site_players_interface.get_game_site_players(game_id, site_id)
        self.assertEqual(response.data, "Invalid Parameters Passed")
        self.assertEqual(response.status_code, 400)

    def test_gamesite_empty_site_id(self):
        """
        Tests the game site players with no site id
        :return:
        """
        game_id = 10
        site_id = None

        response = self.game_site_players_interface.get_game_site_players(game_id, site_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Parameters Passed")
