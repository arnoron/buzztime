"""
Holds the test cases for the site game players list class
"""
from mock import patch, Mock
from rest_framework.test import APITestCase
from cheerios.players.views.player.site_game_players_list import SiteGamePlayersList
from cheerios.test_utils import mock_test_utils


class TestSiteGamePlayersList(APITestCase):
    """
    Tests the site game players module
    """

    def setUp(self):
        """
        Initializes the site game players list object
        :return:
        """
        self.players_list_interface = SiteGamePlayersList()

    @patch(mock_test_utils.CONNECTION_MANAGER,
           Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    def test_players_list_valid(self):
        """
        Tests the site game players list with valid values
        :return:
        """
        site_id = 1110
        game_id = 137

        response = self.players_list_interface.get_site_game_players(site_id, game_id)
        self.assertEqual(response.status_code, 200)

    def test_players_list_invalid_site(self):
        """
        Tests the site game players list invalid site id
        :return:
        """
        site_id = '$%^'
        game_id = 25

        response = self.players_list_interface.get_site_game_players(site_id, game_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed")

    def test_players_list_invalid_game(self):
        """
        Tests the site game players list with invalid game id
        :return:
        """
        site_id = 10
        game_id = '*&^'
        response = self.players_list_interface.get_site_game_players(site_id, game_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed")

    def test_players_list_empty_site(self):
        """
        Tests the site game players list with no site id
        :return:
        """
        site_id = None
        game_id = 25

        response = self.players_list_interface.get_site_game_players(site_id, game_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed")

    def test_players_list_empty_game(self):
        """
        Tests the site game players list with no game id
        :return:
        """
        site_id = 10
        game_id = None

        response = self.players_list_interface.get_site_game_players(site_id, game_id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid parameters passed")
