"""
Holds the test cases for the player search class
"""
from mock import patch
from .response_data import MOCK_RESPONSE
from rest_framework.test import APITestCase

from cheerios.players.views.player.player_search import PlayerSearch
from cheerios.test_utils import mock_test_utils


class TestPlayerSearch(APITestCase):
    """
    Tests the player search module
    """

    def setUp(self):
        """
        Initializes the player search object
        :return:
        """
        self.player_search_interface = PlayerSearch()

    @patch(mock_test_utils.CONNECTION_MANAGER)
    def test_player_search_valid_value(self, cursor):
        """
        Tests the player search with valid value
        :return:
        """
        cursor.side_effect = mock_test_utils.mock_cursor_responses(
            MOCK_RESPONSE['test_player_search_valid_value'])
        search_param = 'john'

        response = self.player_search_interface.get_player_search_response(search_param)
        self.assertEqual(response.status_code, 200)

    def test_player_search_no_value(self):
        """
        Tests the player search with no value
        :return:
        """
        search_param = ''

        response = self.player_search_interface.get_player_search_response(search_param)
        self.assertEqual(response.data, 'Invalid search_params passed')
        self.assertEqual(response.status_code, 400)

    def test_player_search_null_value(self):
        """
        Tests the player search with null value
        :return:
        """
        search_param = None

        response = self.player_search_interface.get_player_search_response(search_param)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, 'Invalid search_params passed')
