from pymssql import OperationalError

from mock import patch
from rest_framework.test import APITestCase

from cheerios.players.test.response_data import MOCK_RESPONSE
from cheerios.test_utils import mock_test_utils


class TestCompetitionPlayerRanking(APITestCase):
    """

    """

    def test_competition_id_string(self):
        """

        :return:
        """
        response = self.client.get("/players/competition/test/ranking/")
        self.assertEqual(response.status_code, 404)

    def test_competition_id_absent(self):
        """

        :return:
        """
        response = self.client.get("/players/competition/ranking/")
        self.assertEqual(response.status_code, 404)

    def test_competition_id_special_chars(self):
        """

        :return:
        """
        response = self.client.get("/players/competition/#$%^/ranking/")
        self.assertEqual(response.status_code, 404)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_no_player_id_associated(self, fetch_all_mock, connection_manager_mock):
        """

        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()

        error_instance = OperationalError()
        error_instance.args = (50000, b'Passed in CompetitionID is not associated with a Player Level competition')

        fetch_all_mock.side_effect = error_instance

        response = self.client.get("/players/competition/711/ranking/")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'"Invalid parameter passed"')

    def test_competition_id_contains_quotes(self):
        """

        :return:
        """
        response = self.client.get("/players/competition/71'1/ranking/")
        self.assertEqual(response.status_code, 404)

        response = self.client.get("/players/competition/711'/ranking/")
        self.assertEqual(response.status_code, 404)

        response = self.client.get("/players/competition/'711/ranking/")
        self.assertEqual(response.status_code, 404)

    @patch(mock_test_utils.CONNECTION_MANAGER)
    @patch(mock_test_utils.FETCH_ALL_FROM_CURSOR)
    def test_correct_competition_id(self, fetch_all_mock ,connection_manager_mock):
        """

        :return:
        """
        connection_manager_mock.side_effect = mock_test_utils.mock_connection_manager()
        fetch_all_mock.side_effect = MOCK_RESPONSE['competition_player_ranking']

        response = self.client.get("/players/competition/10568/ranking/")
        self.assertEqual(response.status_code, 200)
