"""
Serializer classes
"""
from datetime import datetime, timedelta
from rest_framework import serializers
from cheerios.utils import CheeriosUtils
from cheerios.serializers import SerializerValidator
UTIL_FUNCTION = CheeriosUtils()

class RegistrationSerializer(SerializerValidator):
    """
    Serializer for the Registration Form
    """
    username = serializers.CharField(allow_null=True, required=False)
    password = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    display_name = serializers.CharField(required=True)
    postal_code = serializers.CharField(required=True)
    dob = serializers.DateField(input_formats=["%d-%m-%Y"], required=True)
    site_id = serializers.IntegerField(allow_null=True, required=False)
    opt_in = serializers.BooleanField(required=True)
    platform_api_key = serializers.UUIDField(allow_null=True, required=False)


class UserNameSerializer(SerializerValidator):
    """
    Serializer for Avatar Mode Model
    Check all mandatory fields of database are available.
    """
    username = serializers.CharField(required=True)

class EmailSerializer(SerializerValidator):
    """
    Serializer for Avatar Mode Model
    Check all mandatory fields of database are available.
    """
    email = serializers.CharField(required=True)

class DeleteAvatarSerializer(SerializerValidator):
    """
    Serializer for Avatar Mode Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(required=True)

class AvatarSerializer(SerializerValidator):
    """
    Serializer for Avatar Mode Model
    Check all mandatory fields of database are available.
    """
    xml = serializers.CharField(required=True)
    pixels = serializers.CharField(required=True)
    backdrop = serializers.CharField(required=True)

class PlayerAuthentication(SerializerValidator):
    """
    Serializer for Player Authentication Mode Model
    Check all mandatory fields of database are available.
    """
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

class UserSerializer(SerializerValidator):
    """
    Serializer for User Mode Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(required=True)

class BadgeSerializer(SerializerValidator):
    """
    Serializer for Badge Mode Model
    Check all mandatory fields of database are available.
    """
    classification_type = serializers.CharField(required=True)

class CompetitionSerializer(SerializerValidator):
    """
    Serializer for Competition Mode Model
    Check all mandatory fields of database are available.
    """
    promotion_id = serializers.IntegerField(required=True)


class AchievementModeSerializer(SerializerValidator):
    """
    Serializer for Achievement Mode Model
    Check all mandatory fields of database are available.
    """
    mode = serializers.CharField(allow_null=True, required=True)


class SiteScoresSerializer(SerializerValidator):
    """
    Serializer for Site Scores Model
    Check all mandatory fields of database are available.
    """
    premium_game_id = serializers.CharField(allow_null=True, required=True)
    game_id = serializers.CharField(allow_null=True, required=False)
    lower_limit = serializers.CharField(allow_null=True, required=False)
    upper_limit = serializers.CharField(allow_null=True, required=False)
    site = serializers.CharField(allow_null=True, required=False)
    city = serializers.CharField(allow_null=True, required=False)
    state = serializers.CharField(allow_null=True, required=False)


class PlayerScoresSerializer(SerializerValidator):
    """
    Serializer for Player Scores Model
    Check all mandatory fields of database are available.
    """
    premium_game_id = serializers.CharField(allow_null=True, required=True)
    game_id = serializers.CharField(allow_null=True, required=False)
    lower_limit = serializers.CharField(allow_null=True, required=False)
    handle = serializers.CharField(allow_null=True, required=False)
    site = serializers.CharField(allow_null=True, required=False)
    city = serializers.CharField(allow_null=True, required=False)
    state = serializers.CharField(allow_null=True, required=False)


class SitePlayerScoresSerializer(SerializerValidator):
    """
    Serializer for Site Player Scores Model
    Check all mandatory fields of database are available.
    """
    premium_game_id = serializers.CharField(allow_null=True, required=True)
    site_id = serializers.CharField(allow_null=True, required=True)
    game_id = serializers.CharField(allow_null=True, required=False)


class SiteInfoSerializer(SerializerValidator):
    """
    Serializer for Site Info Scores Model
    Check all mandatory fields of database are available.
    """
    site_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)


class CustomerCompetitionSerializer(SerializerValidator):
    """
    Serializer for Customer Competition Model
    Check all mandatory fields of database are available.
    """
    competition_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)


class SiteCompetitionsRankingSerializer(SerializerValidator):
    """
    Serializer for Site Competitions Scores Model
    Check all mandatory fields of database are available.
    """
    competition_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    upper_limit = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class SitePlayersSerializer(SerializerValidator):
    """
    Serializer for Site Player Model
    Check all mandatory fields of database are available.
    """
    site_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)


class SiteGamesSerializer(SerializerValidator):
    """
    Serializer for Site Games Model
    Check all mandatory fields of database are available.
    """
    site_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)


class SiteGamePlayersSerializer(SerializerValidator):
    """
    Serializer for Site Game Player Scores Model
    Check all mandatory fields of database are available.
    """
    site_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    game_name_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)


class SiteGameHistorySerializer(SerializerValidator):
    """
    Serializer for Site Game History Scores Model
    Check all mandatory fields of database are available.
    """
    site_id = serializers.CharField(allow_null=True, required=True)
    game_name_id = serializers.CharField(allow_null=True, required=True)


class SiteGamePlayerHistorySerializer(SerializerValidator):
    """
    Serializer for Site Game Player History Scores Model
    Check all mandatory fields of database are available.
    """
    site_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    game_name_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    start_date = serializers.CharField(allow_null=False, required=True)
    start_time = serializers.CharField(allow_null=False, required=True)


class PlayerSerializer(SerializerValidator):
    """
    Serializer for Player Model
    Check all mandatory fields of database are available.
    """
    stored_procedure = serializers.CharField(allow_null=True, required=True)
    database = serializers.CharField(allow_null=True, required=True)


class LocationSearchDatabaseSerializer(SerializerValidator):
    """
    Serializer for Location Search Model
    Check all mandatory fields for database are available
    """
    store_procedure_args = serializers.CharField(allow_null=True, required=True)
    databases = serializers.CharField(allow_null=True, required=True)


class LocationSearchSerializer(SerializerValidator):
    """
    Serializer for Location Search Model
    Check all mandatory fields for database are available
    """
    latitude = serializers.CharField(allow_null=True, required=False)
    longitude = serializers.CharField(allow_null=True, required=False)
    search_string = serializers.CharField(allow_null=False, required=True)
    startRowNumber = serializers.CharField(allow_null=True, required=False)
    numberOfRows = serializers.CharField(allow_null=True, required=False)


class PlayerAchievementTotalEarnedCountSerializer(SerializerValidator):
    """
    Serializer for Player Achievement Total Earned Count Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)


class GameTypeAveragesSerializer(SerializerValidator):
    """
    Serializer for Game Type Averages Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    game_derivative_id = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class GameCasinoStatsSerializer(SerializerValidator):
    """
    Serializer for Game Casino Stats Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    game_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    game_derivative_id = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class PlayerRecentStatsSerializer(SerializerValidator):
    """
    Serializer for Player Recent Stats Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    count = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class GameTypeScoresSerializer(SerializerValidator):
    """
    Serializer for Game Type Scores Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    stat_filter = serializers.CharField(allow_null=False, required=True)
    game_id = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class GameCasinoScoresSerializer(SerializerValidator):
    """
    Serializer for Game Casino Scores Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    game_subtype_derivative_id = serializers.IntegerField(allow_null=True,
                                                          required=True, min_value=0)
    game_subtype = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    stat_filter = serializers.CharField(allow_null=True, required=False, allow_blank=True)


class PlayerRecentAchievementListSerializer(SerializerValidator):
    """
    Serializer for Player Recent Achievement List Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    start_index = serializers.IntegerField(allow_null=True, required=False, min_value=0)
    count = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class PlayerAchievementListSerializer(SerializerValidator):
    """
    Serializer for Player Achievement List Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    start_index = serializers.IntegerField(allow_null=True, required=False, min_value=0)
    count = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class AchievementDetailSerializer(SerializerValidator):
    """
    Serializer for Achievement Detail Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    achievement_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)


class AchievementEventListSerializer(SerializerValidator):
    """
    Serializer for Achievement Event List Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    achievement_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    start_index = serializers.IntegerField(allow_null=True, required=False, min_value=0)
    count = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class MembersInfoSerializer(SerializerValidator):
    """
    Serializer for Members info List Model
    Check all mandatory fields of database are available.
    """
    player_id = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class BuddyMembersSerializer(SerializerValidator):
    """
    Serializer for Buddy Members List Model
    Check all mandatory fields of database are available.
    """
    owner_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    start_index = serializers.IntegerField(allow_null=True, required=False, min_value=0)
    count = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class SiteCompetitionsSerializer(SerializerValidator):
    """
    Serializer for Site Competitions Model
    Check all mandatory fields of database are available.
    """
    competition_id = serializers.IntegerField(allow_null=True, required=True, min_value=0)
    site_id = serializers.IntegerField(allow_null=True, required=False, min_value=0)
    hide_sequence = serializers.IntegerField(allow_null=True, required=False, min_value=0)


class CompetitionGamesSerializer(SerializerValidator):
    """
    Serializer for the Competition Games Model
    Check all mandatory fields of database are available.
    """
    competition_id = serializers.IntegerField(allow_null=True, required=False, min_value=0)

class TabletLoginSerializer(SerializerValidator):
    """
    Serializer for the tablet login
    """
    player_id = serializers.IntegerField(required=True)
    prepared_hash = serializers.CharField(required=True)
    timestamp = serializers.IntegerField(required=True)

    @staticmethod
    def validate_timestamp(timestamp):
        """
        Validates that the timestamp is between the 12 hours
        range of the current time, else raise error
        :return:
        """
        timestamp_utc = datetime.fromtimestamp(timestamp)

        current_time = datetime.utcnow()
        max_time_limit = current_time + timedelta(hours=12)
        min_time_limit = current_time - timedelta(hours=12)

        if UTIL_FUNCTION.time_in_range(min_time_limit, max_time_limit, timestamp_utc):
            return timestamp
        else:
            raise serializers.ValidationError("Timeout error. Timestamp is expired.")

class SmartestBarSerializer(SerializerValidator):
    """
    Serializer for smartest bar leaderboard
    """
    competition_id = serializers.IntegerField(required=True, min_value=0)
    count = serializers.IntegerField(required=False, min_value=0, default=50, allow_null=True)


class SiteSearchByLocationSerializer(SerializerValidator):
    """
    Serializer for searching sites nearby a passed location
    """
    latitude = serializers.FloatField(required=True)
    longitude = serializers.FloatField(required=True)
