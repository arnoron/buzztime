"""
Common functions for gameplay.
"""
from rest_framework.exceptions import ValidationError
from cheerios.services.log_classifier import Logger


class UtilFunction:
    """
    Util function which allow to other function to do specific task.
    This function is common for all classes.
    """
    @staticmethod
    def response_headers(response):
        """
        To return the headers of a response
        :param response:
        :return:
        """
        return response._headers

    @staticmethod
    def convert_int_or_raise_error(value):
        """

        :param value:
        :return:
        """
        try:
            int_value = int(value)
            return int_value
        except Exception:
            message = "Error status code 400"
            Logger.log.error(message)
            raise ValidationError('Parameter Should be Integer')
