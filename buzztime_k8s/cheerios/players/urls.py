"""ucars URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from cheerios.players.views import avatar
from cheerios.players.views import check_email_availability
from cheerios.players.views import check_username_availability
from cheerios.players.views import events, schedule
from cheerios.players.views import login
from cheerios.players.views import send_email
from cheerios.players.views.amoe import AMOEData
from cheerios.players.views.badge import badge
from cheerios.players.views.bulk_avatar import BulkConvertViewSet
from cheerios.players.views.competition import competition
from cheerios.players.views.competition_player_ranking import CompetitionPlayerRanking
from cheerios.players.views.game import game
from cheerios.players.views.logout import Logout
from cheerios.players.views.player import player, edit_user_info, hash_login, tablet_login
from cheerios.players.views.site import site
from cheerios.players.views.site.search_site_by_location import Site
from cheerios.players.views.smartest_bar import smartest_bar

urlpatterns = [
    url(r'^schedule/(?P<schedule_option>game|weekly|today|day)', \
        schedule.ScheduleViewSet.as_view()),

    url(r'^players/((?P<player_id>[0-9]*)/?)'
        r'((?P<player_info_subtype>[gameplayinstances|badges|games]*)/?)'
        r'((?P<player_info_subtype_id>[0-9]*)/?)', player.PlayerViewSet.as_view()),

    url(r'^badges/', badge.BadgeViewSet.as_view()),

    url(r'^sites/((?P<site_id>[0-9]*)/?)'
        r'((?P<site_info_subtype>[players|games|competitions]*)/?)'
        r'((?P<site_info_subtype_id>[0-9]*)/?)'
        r'((?P<site_info_games_subtype>[instances|players]*)/?)', site.SiteViewSet.as_view()),

    url(r'^games/((?P<game_id>[0-9]*)/?)'
        r'((?P<game_info_subtype>[sites|players]*)/?)'
        r'((?P<game_info_subtype_id>[0-9]*)/?)', game.GameViewSet.as_view()),

    url(r'^events/sitefinder/', events.EventsViewSet.as_view()),

    url(r'^login/', login.LoginViewSet.as_view()),
    url(r'^check_email_availability/', \
        check_email_availability.EmailAvailabilityViewSet.as_view()),
    url(r'^check_username_availability', \
        check_username_availability.UsernameAvailabilityViewSet.as_view()),
    url(r'^avatar/', avatar.AvatarViewSet.as_view()),
    url(r'^edit_user_info/', edit_user_info.EditUserViewSet.as_view()),
    url(r'^email/', send_email.EmailViewSet.as_view()),
    url(r'^hashed_login', hash_login.HashLoginViewset.as_view()),
    url(r'^smartestbar', smartest_bar.SmartestBar.as_view()),
    url(r'^tablet_login/', tablet_login.TabletLoginViewSet.as_view()),
    url(r'^competitions/', competition.CompetitionViewSet.as_view()),
    url(r'^amoe_data/', AMOEData.as_view()),
    url(r'^mobile_login/', login.LoginViewSet.as_view()),
    url(r'^competition/(?P<competition_id>[0-9]*)/ranking/', CompetitionPlayerRanking.as_view()),
    url(r'^logout/', Logout.as_view()),
    url(r'^game_sites/nearby/', Site.as_view())
]
