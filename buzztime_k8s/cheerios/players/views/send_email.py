
"""
Module to send mail
"""
from rest_framework.response import Response

from cheerios.players.constants import BUZZTIME_TRIVIAWEB_NAME, \
    STORED_PROCEDURE_MAPPER, FORM_NAME_MAPPER, EXCLUDE_DATA_FROM_EMAIL
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.services.Email.email import Email
from cheerios.settings import MSSQLUSERDB

class EmailViewSet(PlayersViewSet):
    """
    Email API.
    Forms the email data and initiates the send email method
    """
    @get_store_procedure_api
    def post(self, request):
        """
        Gets the data and send email.
        :return:
        """
        request_data = request.data
        form_type = request_data.get('form', None)
        form_data = request_data.get('data_to_post', None)

        if form_type:
            return self.send_contact_form_email(form_data, form_type)
        else:
            return self.send_forgot_password_mail(form_data)

    def send_forgot_password_mail(self, form_data):
        """
        This implies a forgot password email
        :return:
        """

        database_name = MSSQLUSERDB

        sender_details = {}

        if form_data:
            sender_details['email'] = form_data.get('sender_email', None)
            sender_details['name'] = BUZZTIME_TRIVIAWEB_NAME
            display_name = form_data.get('display_name', None)

            user_recipient_details = dict(email=form_data.get('user_email', None),
                                          username=form_data.get('username', 'User'), \
                                          display_name=display_name
                                        )

            domain_name = form_data.get('domain', None)

            if not domain_name:
                return UTIL_FUNCTION.log_error(400, "Please add domain name in the body")

            if not user_recipient_details['email']:
                return UTIL_FUNCTION.log_error(400, "Please add the user email in the body")

            if not sender_details['email']:
                return UTIL_FUNCTION.log_error(400, "Please add the sender email in the body")
            recipient_details = self.get_user_password_guid(\
                user_recipient_details['email'], database_name)

            if not recipient_details:
                return UTIL_FUNCTION.log_error(400, "Email ID not found in the database")

            user_recipient_details.update({'password': recipient_details['Password'],
                                           'display_name': recipient_details['Handle']})


            template_id = form_data.get('user_template_id')

            if not template_id:
                return UTIL_FUNCTION.log_error(400, "Please add the template id in the body")
            forgot_password_email = Email(user_recipient_details, sender_details,
                                          template_id, domain=domain_name)
            user_email_status = forgot_password_email.send()

            if user_email_status:
                return Response("Email sent successfully", status=200)

            return UTIL_FUNCTION.log_error(500, "Email to user not sent successfully")

        else:
            return UTIL_FUNCTION.log_error(400, "Incorrect response data passed. Please check.")

    def send_contact_form_email(self, form_data, form_type):
        """
        This implies the request came from the contact us forms
        :return:
        """

        admin_recipient_details = {}
        sender_details = {}


        if form_data:

            admin_recipient_details['email'] = form_data.get('admin_email')
            sender_details['email'] = form_data.get('sender_email')
            sender_details['name'] = 'Buzztime Player Portal'

            user_template_id = form_data.get('user_template_id')

            if not user_template_id:
                return UTIL_FUNCTION.log_error(400, "Please add the user template id in the body")

            admin_template_id = form_data.get('admin_template_id')

            if not admin_template_id:
                return UTIL_FUNCTION.log_error(400, "Please add the admin template id in the body")

            user_recipient_details = dict(email=form_data.get('user_email', None),
                                          name=form_data.get('username', 'User') \
                                        )

            if not user_recipient_details['email']:
                return UTIL_FUNCTION.log_error(400, "Please add the user email in the body")

            if not admin_recipient_details['email']:
                return UTIL_FUNCTION.log_error(400, "Please add the admin email in the body")

            if not sender_details['email']:
                return UTIL_FUNCTION.log_error(400, "Please add the sender email in the body")

            email_fields = dict(form_name=FORM_NAME_MAPPER.get(form_type),
                                form_data=self.format_form_data(form_data))

            if not email_fields['form_name']:
                return UTIL_FUNCTION.log_error(400, "Invalid form type passed")

            email_to_admin = Email(admin_recipient_details,
                                   sender_details, admin_template_id, email_fields)
            admin_email_status = email_to_admin.send()

            if not admin_email_status:
                return UTIL_FUNCTION.log_error(500, "Email to admin not sent successfully")

            email_to_user = Email(user_recipient_details,
                                  sender_details, user_template_id)
            user_email_status = email_to_user.send()

            if not user_email_status:
                return UTIL_FUNCTION.log_error(500, "Email to user not sent successfully")

            return Response("Email sent successfully", status=200)

        else:
            return UTIL_FUNCTION.log_error(400, "Incorrect response data passed. Please check.")

    def get_user_password_guid(self, email, database_name):
        """
        Returns the encrypted password of the email passes
        :param email:
        :return:
        """

        request_data = "@email='{}'".format(email)

        stored_procedure = STORED_PROCEDURE_MAPPER['forgot_password']
        cursor = self.get_cursor(database_name)
        self.call_storeprocedure(stored_procedure, cursor, request_data)
        result = self.fetchall_from_cursor(cursor)
        if not result:
            return False
        return result[0]

    def format_form_data(self, form_data):
        """
        Formats the form data into an html format to be injected in the email template
        :param form_data:
        :return:
        """
        formatted_data = ""
        for key in form_data:
            if not key in EXCLUDE_DATA_FROM_EMAIL:
                formatted_data += "<br/><b> {0} </b>: {1}".format(key, form_data[key])
        formatted_data += "<br/>"

        return formatted_data


