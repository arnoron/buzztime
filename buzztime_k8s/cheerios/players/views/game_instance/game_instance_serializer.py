from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class GameInstanceDetailsSerializer(SerializerValidator):

    player_id = serializers.IntegerField(required=True, allow_null=False)
    game_id = serializers.IntegerField(required=True, allow_null=False)
    mode = serializers.CharField(required=True, allow_null=True)


