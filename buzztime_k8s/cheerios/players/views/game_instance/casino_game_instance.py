"""
Module which holds the CasinoGameInstance class
"""
from cheerios.players.constants import CASINO_GAME_MAPPER, STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer
from cheerios.services.log_classifier import Logger
from rest_framework.response import Response


class CasinoGameInstance(PlayersViewSet):
    """
    Returns the casino game instance response
    """
    def get_casino_game_instances(self, player_id, game):
        """
        Returns the casino game instances for a player and a game
        :return:
        """
        serializer = CheckIntegerSerializer(data=dict(integer_value=player_id))
        if serializer.is_valid():
            game_dict = CASINO_GAME_MAPPER.get(game, None)

            if game_dict is None:
                return UTIL_FUNCTION.log_error(400, "Invalid game parameter passed. Can be either " \
                                                    "'holdem', 'blackjack' or 'freezeout'")

            request_data = dict(PlayerID=player_id,
                                GameSubType=game_dict['game_id'],
                                GameSubTypeDerivativeID=game_dict['game_derivative_id'],
                                )

            stored_procedure = STORED_PROCEDURE_MAPPER['game_casino_scores']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            error = {
                "Error": "Empty parameters passed",
                "ErrorCode": 400
            }
            result = "Serializer Error: {0}".format(serializer.errors)
            message = "Error status code 400, {0}".format(result)
            Logger.log.error(message)
            return Response(error, status=400)

