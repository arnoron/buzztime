"""
Module which holds the SiteGameInstance class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer

class SiteGameInstance(PlayersViewSet):
    """
    Returns the site game instance response
    """

    def get_site_game_instance(self, site_id, game_id):
        """
        Returns the site game instance for a site and a game
        :param site_id:
        :param game_id:
        :return:
        """
        site_id_serializer = CheckIntegerSerializer(data=dict(integer_value=site_id))
        game_id_serializer = CheckIntegerSerializer(data=dict(integer_value=game_id))
        if site_id_serializer.is_valid() and game_id_serializer.is_valid():
            request_data = dict(SiteID=site_id,
                                GameNameID=game_id)

            stored_procedure = STORED_PROCEDURE_MAPPER['site_game_history']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            return UTIL_FUNCTION.log_error(400,'Invalid parameters passed for game_id, site_id')