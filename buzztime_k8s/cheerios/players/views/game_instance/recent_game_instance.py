"""
Module which holds the RecentGameInstance class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet,UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer



class RecentGameInstance(PlayersViewSet):
    """
    Returns the recent game instance response
    """
    def get_recent_game_instances(self, player_id):
        """
        Returns the recent game instances for a player
        :param player_id:
        :return:
        """
        serializer = CheckIntegerSerializer(data=dict(integer_value=player_id))
        if serializer.is_valid():
            count = 10

            request_data = dict(PlayerId=player_id,
                                count=count)

            stored_procedure = STORED_PROCEDURE_MAPPER['player_recent_stats']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, 'Invalid parameters passed')
