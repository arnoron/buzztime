"""
Module which holds the GameInstanceDetails class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from .game_instance_serializer import GameInstanceDetailsSerializer


class GameInstanceDetails(PlayersViewSet):
    """
    Returns the game instance details response
    """
    def get_game_instance_details(self, player_id, game_id, mode):
        """
        Returns the game instance details for a player, a game and a mode
        :param player_id:
        :param game_id:
        :param mode:
        :return:
        """
        serializer = GameInstanceDetailsSerializer(data=dict(player_id=player_id,game_id=game_id,mode=mode))

        if serializer.is_valid():

            request_data = dict(PlayerID=player_id,
                                GameSubTypeDerivativeID=game_id,
                                StatFilter=mode)

            stored_procedure = STORED_PROCEDURE_MAPPER['game_type_scores']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, "Invalid Parameters passed for player_id,game_id,mode")
