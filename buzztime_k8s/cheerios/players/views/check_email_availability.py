"""
Checks id the email already exists or not
"""
from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.settings import MSSQLUSERDB
from cheerios.players.serializers import EmailSerializer
from cheerios.utils import CheeriosUtils, escape_quotes
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()


class EmailAvailabilityViewSet(MSSQLApiView):
    """
    Checks if the email is already used or not
    """
    @get_store_procedure_api
    def get(self, request):
        """
        User registration API
        :return:
        """
        request_params = request.GET

        serializer = EmailSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)

        result = True if self.email_exists(request_params.get('email')) else False
        return Response(result, status=200)

    @escape_quotes
    def email_exists(self, email):
        """
        Checks if email exists in the db
        :param email:
        :return:
        """
        data_string = "@Email='{0}', @NamespaceID={1}".format(email, 1)

        cursor = self.get_cursor(MSSQLUSERDB)
        store_procedure = STORED_PROCEDURE_MAPPER['email_exists_status']

        self.call_storeprocedure(store_procedure, cursor, data_string)
        result = self.fetchall_from_cursor(cursor)[0]
        return result['EmailExists']
