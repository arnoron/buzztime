"""
Player Base Class
"""
from rest_framework.response import Response
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.serializers import PlayerSerializer
from cheerios.players.utils.utils import UtilFunction
from cheerios.players.decorators import close_connection
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

UTIL_FUNCTION = CheeriosUtils()
PLAYERS_UTILS = UtilFunction()



class PlayersViewSet(MSSQLApiView):
    """
    Generic class for the Players viewset
    To be inherited by the specific viewsets
    """
    @close_connection
    def execute_stored_procedure(self, store_procedure, data):
        """
        Calls the stored procedure and returns the response
        :param store_procedure: Name of the stored procedure to be called
        :param data: The data to be bound to the Stored Procedure call
        :param database: database to query to
        :return: Tuple Response from the stored procedure
        """
        database = store_procedure.split(".")[0]
        request_params = {'stored_procedure': store_procedure,
                          'data': data,
                          'database': database
                         }
        serializer = PlayerSerializer(data=request_params)
        data_string = data
        if isinstance(data, dict):
            data_string = self.get_data_string(data)
        if serializer.is_valid():
            with self.connection_manager(store_procedure, data_string, database) as cursor:
                result = self.fetchall_from_cursor(cursor)
            result = self.format_response_data(result)
            result = self.post_process_data(result)
            response = Response(result, status=200)
            return response
        else:
            result = "Serializer Error: {0}".format(serializer.errors)
            message = "Error status code 400, {0}".format(result)
            Logger.log.error(message)
            return UTIL_FUNCTION.log_error(400, result)

    def get_data_string(self, data):
        """
        Converts the data in dictionary format to a string format
        :return:
        """

        return ",".join(["@{0}='{1}'".format(data_key, data_value) for data_key, data_value in data.items()])

    @staticmethod
    def format_response_data(result):
        """
        To be inherited for formatting response data
        :return: result
        """
        return result

    @staticmethod
    def format_response(response_data, data_entity_mapper):
        """
        Formats response to a particular format
        """

        clean_response_data = []

        if response_data:
            for player in response_data:
                row = {data_entity_mapper.get(name, name): val for name, val in player.items()}
                clean_response_data.append(row)

        return Response(clean_response_data, status=200)


    @staticmethod
    def post_process_data(data):
        """
        Process data after getting the response
        :param data: response data
        :return: Processed data
        """
        return data
