"""
Checks if a username is available or not
"""
from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.settings import MSSQLUSERDB
from cheerios.players.serializers import UserNameSerializer
from cheerios.utils import CheeriosUtils, escape_quotes

CHERRIOS_UTILS = CheeriosUtils()

class UsernameAvailabilityViewSet(MSSQLApiView):
    """
    Checks if the username is already used or not
    """
    @get_store_procedure_api
    def get(self, request):
        """
        User registration API
        :return:
        """
        request_params = request.GET

        serializer = UserNameSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)

        result = True if self.username_exists(request_params.get('username')) else False

        return Response(result, status=200)

    @escape_quotes
    def username_exists(self, username):
        """
        Checks if username exists in the db
        :param username:
        :return:
        """
        data_string = "@Username='{0}', @NamespaceID={1}".format(username, 1)

        cursor = self.get_cursor(MSSQLUSERDB)
        store_procedure = STORED_PROCEDURE_MAPPER['username_exists_status']

        self.call_storeprocedure(store_procedure, cursor, data_string)
        result = self.fetchall_from_cursor(cursor)[0]
        return result['UserNameExists']
