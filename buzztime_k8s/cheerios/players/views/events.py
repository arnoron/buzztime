"""
API View for the Events Page APIs
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.decorators import get_store_procedure_api
from cheerios.services.log_classifier import Logger


class EventsViewSet(PlayersViewSet):
    """
    API which returns the list of trivia games and their details
    """

    start_index = 1
    count = 10
    def __init__(self):
        """
        Constructor initializes the global vars
        """
        super().__init__()
        self.return_type = ""
        self.start_index = ""
        self.count = ""

    @get_store_procedure_api
    def get(self, request):
        """
        Calls the stored procedure
        Gets the API data and returns the response
        :param request: Http Request Object
        :return: Http Response Object
        """
        request_params = request.GET
        latitude = float(request_params.get("latitude", 0))
        longitude = float(request_params.get("longitude", 0))
        self.return_type = request_params.get('return_type')
        self.start_index = request_params.get("start_index", 1)
        self.count = request_params.get("count", 10)
        data = dict(Latitude=latitude, Longitude=longitude, MaximumDistance=0)
        stored_procedure = STORED_PROCEDURE_MAPPER['events']
        result = self.execute_stored_procedure(stored_procedure, data)
        message = "Events: {0}".format(result)
        Logger.log.info(message)
        return result

    def format_response_data(self, result):
        """
        Formats the response data returned by the stored procedure into a JSON readable format
        :param result: un-formatted result data in the form of tuple
        :return: formatted JSON data
        """

        list_data = []

        if isinstance(result, list):
            for result_obj in result:
                list_data.append(
                    dict(Id=result_obj.get('siteid'),
                         Chain=result_obj.get('Chain'),
                         Phone=result_obj.get('PriPhone'),
                         Latitude=result_obj.get('latitude'),
                         Longitude=result_obj.get('longitude'),
                         Distance=result_obj.get('distance'),
                         ReviewCount=result_obj.get('ReviewCount'),
                         SalesType=result_obj.get('SalesType'),
                         SiteName=result_obj.get('OrgName'),
                         Accuracy=result_obj.get('Accuracy'),
                         IsTabletSite=True if result_obj.get('isTabletSite') else False,
                         Location=dict(Id=result_obj.get('siteid'),
                                       Name=result_obj.get('OrgName'),
                                       Address=dict(City=dict(Name=result_obj.get('City')),
                                                    State=dict(Name=result_obj.get('State')),
                                                    ZipCode=dict(ZipCodeValue=result_obj.
                                                                 get('Zip')),
                                                    StreetAddress1=result_obj.get('Address1')
                                                   )
                                      ),
                         PowerPage=dict(PowerPageID=result_obj.get('PowerPageID'),
                                        Description=result_obj.get('PowerPageDescription'),
                                        URL=result_obj.get('PowerPageURL'),
                                        Logo=result_obj.get('PowerPageLogo'),
                                        PowerPage_First_Image=result_obj.get('PowerPageImage1'),
                                        PowerPage_Second_Image=result_obj.get('PowerPageImage2'),
                                        PowerPage_Third_Image=result_obj.get('PowerPageImage3'),
                                        URLFacebook=result_obj.get('PowerPageURLFacebook'),
                                        URLMyspace=result_obj.get('PowerPageURLMyspace'),
                                        URLTwitter=result_obj.get('PowerPageURLTwitter'),
                                        Opt21AndOver=result_obj.get("Opt21AndOver"),
                                        OptFreeWifi=result_obj.get("OptFreeWifi"),
                                        OptKidFriendly=result_obj.get("OptKidFriendly"),
                                        OptLiveMusic=result_obj.get("OptLiveMusic"),
                                        OptFullBar=result_obj.get("OptFullBar"),
                                        OptFullMenu=result_obj.get("OptFullMenu")
                                       ),
                         SiteEvents=dict(SiteEvent=[dict(EventDayId=result_obj.get('EventDayID'),
                                                         EventEndTime=result_obj.
                                                         get('EventEndTime'),
                                                         EventId=result_obj.get('EventID'),
                                                         EventStartTime=result_obj.
                                                         get('EventStartTime'),
                                                         EventStatusId=result_obj.
                                                         get('EventStatusID'),
                                                         EventType=self.get_event_type
                                                         (result_obj.get('EventTypeID')),
                                                         EventTypeId=result_obj.get('EventTypeID'),
                                                         SiteId=result_obj.get('siteid')
                                                        )
                                                   ]
                                        )
                        )
                )

        return list_data

    def post_process_data(self, data):
        """
        Filters out the event data to exclude
        the ones not in the return_type_list
        Sorts the data based on the distance
        :param data:
        :return:
        """


        return_type_list = ['BuzztimeLiveTrivia', 'BuzztimeLiveQuestionnairey']

        trivia_return_list = ['StumpLiveTrivia', 'StumpPrivateTrivia', 'StumpTriviaInABox',
                              'BuzztimeLiveTrivia', 'BuzztimePrivateTrivia']


        if self.return_type == 'tablet':
            data[:] = [event for event in data
                       if event['IsTabletSite'] is True]

        elif self.return_type == 'classic':
            data[:] = [event for event in data
                       if event['IsTabletSite'] is False]

        elif self.return_type == 'jackpot':
            data[:] = [event for event in data
                       if event['SiteEvents']['SiteEvent'][0]['EventType'] in trivia_return_list]

        elif self.return_type == 'all':
            pass

        else:
            data[:] = [event for event in data
                       if event['SiteEvents']['SiteEvent'][0]['EventType'] in return_type_list]

        data[:] = [location for location in data
                   if location['SalesType'] != "Stump"]

        # Sorts the data in the ascending order of distance
        sorted_data = sorted(data, key=lambda k: k['Distance'])
        return sorted_data[int(self.start_index) - 1:int(self.start_index) - 1 + int(self.count)]

    @staticmethod
    def get_event_type(event_type_id):
        """
    Function will take event_type_id and return the event type
    if found else return Special Event
    :param event_type_id:
    :return:
    """
        switcher = {
            17: 'StumpLiveTrivia',
            18: 'StumpLiveQuestionnairey',
            19: 'StumpPrivateTrivia',
            20: 'StumpPrivateQuestionnairey',
            23: 'StumpTriviaInABox',
            45: 'BuzztimeLiveTrivia',
            46: 'BuzztimeLivePoker',
            47: 'BuzztimeLiveQuestionnairey',
            48: 'BuzztimePrivateTrivia',
            49: 'BzztimePrivatePoker',
            50: 'BuzztimePrivateQuestionnairey',
            52: 'StumpQuestionnaireyInABox'
        }
        return switcher.get(event_type_id, 'Special Event')
