"""
API for Player authentication.
"""
from rest_framework.response import Response
from cheerios.players.utils.utils import UtilFunction
from cheerios.players.decorators import get_store_procedure_api
from cheerios.services.Login.login import LoginViewInterface
from cheerios.settings import MSSQLUSERDB
from cheerios.utils import CheeriosUtils
from cheerios.players.serializers import PlayerAuthentication
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()
UTIL_FUNCTION = UtilFunction()


class LoginViewSet(LoginViewInterface):
    """
    Viewset for the login API. It takes username and password
    as parameters and returns the response as the player details
    if the authentication is successful else returns False
    """
    @get_store_procedure_api
    def post(self, request):
        """
        Gets the post params and calls the authenticate method to authenticate the user
        :param request:
        :return:
        """
        response = self.login_user(request)

        if UTIL_FUNCTION.response_headers(response).get('session') and response.data:

            session_id = UTIL_FUNCTION.response_headers(response)['session'][1]
            player_id = response.data['player_id']
            player_profile = self.get_player_profile(player_id)
            response = Response(player_profile, 200)
            response['Access-Control-Expose-Headers'] = 'Session'
            response["Session"] = session_id
        return response

    def login_user(self, request):
        """

        :return:
        """
        mobile_login = False

        if 'mobile_login' in request.path:
            mobile_login = True

        message = "Login API called"
        Logger.log.info(message)
        request_params = request.data

        serializer = PlayerAuthentication(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)
        password = request_params.get('password')
        username = request_params.get('username')
        version = "1.0"


        # If the value sent is an email then put it as 6 else as 1

        mobile_auth = False
        if mobile_login:
            mobile_auth = True
            version = request_params.get('api_version')

        namespace_id = 6 if CHERRIOS_UTILS.validate_email(username) else 1
        authenticated_player = self.authenticate_user(username, password, namespace_id, version, mobile_auth)
        if authenticated_player:
            response = self.get_response(authenticated_player, username)
            player_id = authenticated_player.get('PlayerID')
            persist = False
            mobile = False
            if mobile_login:
                persist = True
                mobile = True
            if CHERRIOS_UTILS.does_auth_token_exist_for_user(player_id, mobile):
                auth_token = CHERRIOS_UTILS.get_auth_token_for_user(player_id, mobile)
            else:
                auth_token = CHERRIOS_UTILS.create_auth_token_for_user(authenticated_player.get('PlayerID'), persist, mobile)
            response["Session"] = auth_token
            return response
        else:
            return_status = 200
            if mobile_login and version == "1.1":
                return_status = 401
            return Response('Invalid credentials', status=return_status)


    def get_player_profile(self, player_id, cursor=None):
        """

        :param player_id:
        :return:
        """
        player_profile = None

        parameters = "@PlayerID={}".format(player_id)
        if not cursor:
            cursor = self.get_cursor(MSSQLUSERDB)
        store_procedure = "UserDB.dbo.usp_sel_PlayerProfile"
        self.call_storeprocedure(store_procedure, cursor, parameters)
        result = self.fetchall_from_cursor(cursor)
        if result:
            result = result[0]
            player_profile = dict(player_id=player_id,
                                  username=result.get('Username'),
                                  display_name=result.get('Handle'),
                                  pin=result.get('Pin'),
                                  email=result.get('Email'),
                                  postal_code=result.get('PostalCode'),
                                  site_id=int(result.get('SiteID')),
                                  avatar_crc=result.get('AvatarCrc'),
                                  player_plus_points=int(result.get('TotalPoints')),
                                  birth_date=result.get('BirthDate')
                                 )

        return player_profile
