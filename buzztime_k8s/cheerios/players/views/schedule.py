"""
File contains various apis for schedules.
"""
from datetime import datetime

from rest_framework.response import Response

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.decorators import get_store_procedure_api
from cheerios.services.log_classifier import Logger


class ScheduleBaseClass(PlayersViewSet):
    """
    Contains the common methods for all the schedule APIs
    """
    def format_response_data(self, result):
        """
        This function will intake result obtained fron stored procedure
        and format as per the format required on UI
        :param result:
        :return:
        """
        list_data = []
        if isinstance(result, list):
            for result_obj in result:
                list_data.append(
                    dict(game_id=result_obj.get('PrimetimeScheduleID'),
                         country=result_obj.get('Country'),
                         day=self.get_weekday(result_obj.get('DayID')),
                         game=result_obj.get('GameName'),
                         pacific_time=result_obj.get('TimePacific'),
                         mountain_time=result_obj.get('TimeMountain'),
                         central_time=result_obj.get('TimeCentral'),
                         eastern_time=result_obj.get('TimeEastern'),
                         game_icon_name=result_obj.get('GameFileName'),
                         classic_locations_only=result_obj.get('ClassicLocationsOnly'),
                         game_length_minutes=result_obj.get('GameLengthMinutes')
                        )
                    )
        return list_data


    @staticmethod
    def get_weekday(week_day):
        """
        Function will dat integer as week day value and
        map integer values with string.
        For any value apart form 1-7 it will return Invalid day
        :param week_day:
        :return:
        """
        switcher = {
            1: "Monday",
            2: "Tuesday",
            3: "Wednesday",
            4: "Thursday",
            5: "Friday",
            6: "Saturday",
            7: "Sunday"
        }
        return switcher.get(week_day, 'Invalid day')


class ScheduleViewSet(ScheduleBaseClass):
    """
    Schedule API main controller
    Gets the schedule option value and routes the control to the
    appropriate method on the basis of its value
    """

    @get_store_procedure_api
    def get(self, request, schedule_option):
        """

        :param request: HTTP request object
        :param schedule_option: the schedule option values,
        can be either game, day, today or weekly
        :return: An HTTP response object representing the schedule
        """

        schedule_method_mapper = dict(game=self.specific_game,
                                      day=self.day_schedule,
                                      weekly=self.weekly_schedule,
                                      today=self.todays_schedule
                                     )
        parameters = schedule_method_mapper[schedule_option]()
        result = self.execute_stored_procedure(STORED_PROCEDURE_MAPPER['game_schedule'],
                                               parameters
                                              )
        result = self.pre_process(result, schedule_option)
        message = "Schedule for {0}: {1}".format(schedule_option, result)
        Logger.log.info(message)
        return result

    @staticmethod
    def specific_game():
        """
        Get schedule parameter value for game.
        Need to pass country value. For now country is hardcoded for 'US'
        """
        week_day = datetime.today().weekday() + 1
        country = 'US'
        game_id = 507
        return dict(Country=country, DayID=week_day, GameNameID=game_id)

    @staticmethod
    def weekly_schedule():
        """
        Get schedule parameter value for weekly game.
        Need to pass country value. For now country is hardcoded for 'US'
        """
        week_day = -1
        country = 'US'
        return dict(Country=country, DayID=week_day)

    @staticmethod
    def todays_schedule():
        """
        Get schedule parameter value for today.
        Need to pass country value. For now country is hardcoded for 'US'
        """
        week_day = datetime.today().weekday() + 1
        country = 'US'
        return dict(Country=country, DayID=week_day)

    @staticmethod
    def day_schedule():
        """
        Get schedule parameter value for day.
        Need to pass country value. For now country is hardcoded for 'US'
        """
        # This will be passed from UI
        date_passed = '02 07 2017'
        datetime_object = datetime.strptime(date_passed, '%m %d %Y')
        week_day = datetime_object.weekday()+1
        country = 'US'
        return dict(Country=country, DayID=week_day)

    def pre_process(self, result, schedule_option):
        if schedule_option == 'weekly':
            processed_result = {
                'weekly': [],
                'classic_locations_only': []
            }
            for block in result.data:
                key = 'weekly'
                if block['classic_locations_only'] == 1:
                    key = 'classic_locations_only'
                del block['classic_locations_only']
                processed_result[key].append(block)
            return Response(processed_result, status=200)
        return result
