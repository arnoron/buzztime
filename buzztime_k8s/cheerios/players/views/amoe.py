"""
Python Module to handle AMOE data requests
"""

import pytz
from datetime import datetime

from rest_framework.response import Response

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.views.serializer import AMOESerializer
from cheerios.services.log_classifier import Logger
from cheerios.services.validate_captcha import validate_captcha


class AMOEData(PlayersViewSet):
    """
    The AMOE Data class. This has the following attributes:
        first_name
        last_name
        email_address
        zip
        favorite_trivia_game
    """

    @get_store_procedure_api
    @validate_captcha()
    def post(self, request):
        """
        Method to insert the form posted data to the database
        :param request:
        :return:
        """
        request_data = request.data

        serializer = AMOESerializer(data=request_data)

        if not serializer.is_valid():
            Logger.log.error(serializer.errors)
            raise ValueError("Invalid Data Passed")

        current_date_time = datetime.now(tz=pytz.timezone('US/Pacific')).strftime("%Y-%m-%dT%X")

        sp_data = dict(SweepstakesID=request_data.get('sweepstake_id', 1),
                       AcceptedTermsFlag=request_data.get('accepted_terms_flag', 1),
                       FirstName=request_data.get('first_name'),
                       LastName=request_data.get('last_name'),
                       EmailAddress=request_data.get('email_address'),
                       Zip=request_data.get('zip'),
                       FavoriteTriviaGame=request_data.get('favorite_trivia_game'),
                       EntryDateTime=current_date_time
                      )

        response = self.execute_stored_procedure(STORED_PROCEDURE_MAPPER['insert_sweepstake_entry'],
                                                 sp_data
                                                )

        if response.status_code == 200:
            return Response("AMOE Data Inserted", response.status_code)
