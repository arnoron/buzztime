"""
Module For Bulk Avatar Conversion API.
"""
import csv
import datetime
import json

import untangle
from random import randint
from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import AVATAR_XML_CSV_PATH, DEFAULT_LOWER_LIMIT_FOR_COORDS, \
    DEFAULT_UPPER_LIMIT_FOR_COORDS, AVATAR_JSON_DEFAULT_VALUES
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.views.avatar import AvatarViewSet


class BulkConvertViewSet(MSSQLApiView):
    """
    API for converting avatars from XML to JSON
    """

    @get_store_procedure_api
    def get(self, request, count):
        """
        Should take csv data one by one and convert and call Avatar save_xml API to save and should
        proceed to next data when saving is complete.
        :param request:
        :param count: number of avatars to be converted from XML to JSON
        :return:
        """
        status = {}
        limit = int(count)
        try:
            rows = []
            migrated_avatars = 0
            failed_migration_avatars = 0
            with open(AVATAR_XML_CSV_PATH, 'r') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                total_sp_call_time = datetime.timedelta(0)
                for row in csv_reader:
                    if row[3] != "migrated" and migrated_avatars < limit:
                        obj = untangle.parse(row[1])
                        json_item = {}
                        for features in obj.avatar.features:
                            for feature in features.feature:
                                json_item[feature["name"] + "_type"] = feature["name"] + "_" + feature["selected"]

                        for colors in obj.avatar.colors:
                            for color in colors.color:
                                try:
                                    if len(color["color"]) > 6:
                                        rgb_color = tuple(
                                            int(format(int(color["color"]), 'x')[i:i + 2], 16) for i in (0, 2, 4))
                                    else:
                                        rgb_color = tuple(int(color["color"][i:i + 2], 16) for i in (0, 2, 4))
                                    json_item[color["name"]] = {"r": rgb_color[0], "g": rgb_color[1],
                                                                "b": rgb_color[2]}
                                    json_item[color["name"] + "Coords"] = {"x": color["xpos"], "y": color["ypos"]}
                                except ValueError as e:
                                    # print('Invalid Hex data.',e)
                                    json_item[color["name"]] = {"r": 0, "g": 0, "b": 0}
                                    json_item[color["name"] + "Coords"] = {
                                        "x": randint(DEFAULT_LOWER_LIMIT_FOR_COORDS,
                                                     DEFAULT_UPPER_LIMIT_FOR_COORDS),
                                        "y": randint(DEFAULT_LOWER_LIMIT_FOR_COORDS,
                                                     DEFAULT_UPPER_LIMIT_FOR_COORDS)}

                        if obj.avatar['gender'] == 'm':
                            json_item['gender'] = 'male'
                        else:
                            json_item['gender'] = 'female'

                        json_item.update(AVATAR_JSON_DEFAULT_VALUES)

                        json_string = json.dumps(json_item)

                        avatar_interface = AvatarViewSet()
                        player_id = row[0]
                        file_crc_32 = row[2]
                        before_sp_call_time = datetime.datetime.now()
                        migration_status = False;
                        try:
                            migration_status = avatar_interface.save_xml(player_id, player_id + ".jpg", file_crc_32,
                                                                         json_string)
                        except Exception as ex:
                            status["error"] = str(ex)
                        sp_call_time = datetime.datetime.now() - before_sp_call_time
                        total_sp_call_time += sp_call_time
                        if migration_status:
                            migrated_avatars += 1
                            row[3] = "migrated"
                            rows.append(row)
                        else:
                            failed_migration_avatars += 1
                            row[3] = "not migrated"
                            rows.append(row)
                    else:
                        rows.append(row)

            with open(AVATAR_XML_CSV_PATH, 'w') as csv_file_out:
                csv_writer = csv.writer(csv_file_out)
                csv_writer.writerows(rows)
            if migrated_avatars == 0:
                status = 0
            else:
                status["total_time_taken"] = total_sp_call_time
                status["no_of_avatars_for_which_migration_was_attempted"] = migrated_avatars
                status["no_of_avatars_migrated_successfully"] = migrated_avatars - failed_migration_avatars
                status["no_of_avatars_migration_failed"] = failed_migration_avatars

        except FileNotFoundError as e:
            status = "File not found in {} location.".format(AVATAR_XML_CSV_PATH)

        return Response(status)
