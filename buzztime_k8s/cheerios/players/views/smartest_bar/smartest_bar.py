"""
Module which holds the SmartestBar class
"""

from cheerios.players.serializers import SmartestBarSerializer
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = CheeriosUtils()


class SmartestBar(PlayersViewSet):
    """
    Returns the smartest bar promotion rankings
    """
    @get_store_procedure_api
    def get(self, request):
        """
        Returns the smartest bar leader board
        :return:
        """
        request_params = request.GET

        serializer = SmartestBarSerializer(data=request_params)

        if not serializer.is_valid():
            return UTIL_FUNCTION.log_error(400, serializer.errors)

        validated_data = serializer.validated_data

        competition_id = validated_data.get('competition_id')
        count = validated_data.get('count')
        lower_limit = 1

        data = dict(CompetitionID=competition_id,
                    LowerLimit=lower_limit,
                    UpperLimit=count)

        response = self.execute_stored_procedure(STORED_PROCEDURE_MAPPER['smartest_bar'], data)
        return response
