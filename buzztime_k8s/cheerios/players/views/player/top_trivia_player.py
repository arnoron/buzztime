"""
Module which holds the TopTriviaPlayer class
"""

from operator import itemgetter

from cheerios.players.constants import STORED_PROCEDURE_MAPPER, PLAYER_FIELDS_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer



class TopTriviaPlayer(PlayersViewSet):
    """
    Returns the top trivia players response
    """
    def get_top_trivia_players(self, game, state, handle):
        """
        Returns the top trivia players for the passed parameters
        :param game:
        :param state:
        :param handle:
        :return:
        """
        row_count = 100

        if handle:
            try:
                handle = "%" + handle + "%"
            except TypeError as ex:
                return UTIL_FUNCTION.log_error(400, 'Invalid parameter passed for handle')
        serializer = CheckIntegerSerializer(data=dict(integer_value=game))
        if serializer.is_valid():

            data = dict(recordcount=row_count,
                        GameSubTypeDerivativeID=game)

            if state: data['State'] = state
            if handle: data['HandleLike'] = handle

            stored_procedure = STORED_PROCEDURE_MAPPER['player_averages']

            result = self.execute_stored_procedure(stored_procedure, data)
            if result.status_code is not 200:
                return UTIL_FUNCTION.log_error(result.status_code, result.data)

            result = self.format_response(result.data, PLAYER_FIELDS_MAPPER)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, "Invalid parameters passed")

    def post_process_data(self, data):
        """
        Sorts the result in descending order of average
        :param data:
        :return:
        """
        data = sorted(data, key=itemgetter('AverageScore'), reverse=True)
        return data
