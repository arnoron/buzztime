"""
Module which contains the BadgeLeader class
"""
import datetime
from cheerios.players.constants import ACHIEVEMENT_PLATFORM, \
    STORED_PROCEDURE_MAPPER, PLAYER_FIELDS_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION, PLAYERS_UTILS
from cheerios.settings import MSSQLUSERDB


class BadgeLeader(PlayersViewSet):
    """
    Returns the list of badge leaders players
    """
    def get_badge_leaders_response(self, start_date_str, end_date_str,
                                   badge_classifier, badge_type):
        """
        Returns the badge leaders for a start-end date, a badge classifier and a badge type
        :param start_date:
        :param end_date:
        :param badge_classifier:
        :param badge_type:
        :return:
        """

        first_row = 1
        row_count = 100

        start_date = datetime.datetime(2000, 1, 1)
        end_date = datetime.datetime.combine(datetime.date.today(), datetime.time(0, 0))

        try:
            if start_date_str:
                start_date = UTIL_FUNCTION.format_date(start_date_str)
            if end_date_str:
                end_date = UTIL_FUNCTION.format_date(end_date_str)

        except IndexError:
            return UTIL_FUNCTION.log_error(400, "Date is not in the correct format. " \
                            "Should in format dd-mm-yyyy")

        data = "@FirstRow={0},@EndDate='{1}',@StartDate='{2}'," \
               "@AchievementTypeID={3},@RowCount={4}," \
               "@AchievementID={5}, @AchievementClassificationID={6}," \
               "@AchievementPlatformID={7}, " \
               "@AchievementRarityID={8}".format(first_row,
                                                 end_date,
                                                 start_date,
                                                 badge_type if badge_classifier == 'type' else 'NULL',
                                                 row_count,
                                                 badge_type if badge_classifier == 'name' else 'NULL',
                                                 badge_type if badge_classifier == 'category' else 'NULL',
                                                 ACHIEVEMENT_PLATFORM['BuzztimeNetwork'],
                                                 badge_type if badge_classifier == 'rarity' else 'NULL',
                                                )

        store_procedure = STORED_PROCEDURE_MAPPER['achievement_leaderboard']
        cursor = self.get_cursor(database=MSSQLUSERDB, store_procedure=store_procedure)
        self.call_storeprocedure(store_procedure, cursor, data)
        result = self.fetchall_from_cursor(cursor)
        result = self.format_response(result, PLAYER_FIELDS_MAPPER)
        return result
