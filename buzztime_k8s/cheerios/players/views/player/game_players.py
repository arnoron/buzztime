"""
Module which holds the GamePlayers class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.services.log_classifier import Logger


class GamePlayers(PlayersViewSet):
    """
    Returns the game players response
    """

    def get_game_players(self, premium_game_id, handle,
                         site, city, state, lower_limit, upper_limit):
        """
        Returns the game players response based on the passed parameters
        :return:
        """

        data = dict(PremiumGameID=premium_game_id)

        if handle: data['Handle'] = handle
        if site: data['SiteName'] = site
        if city: data['City'] = city
        if state: data['State'] = state
        if lower_limit: data['LowerLimit'] = lower_limit
        if upper_limit: data['UpperLimit'] = upper_limit

        stored_procedure = STORED_PROCEDURE_MAPPER['player_scores']
        result = self.execute_stored_procedure(stored_procedure, data)
        return result
