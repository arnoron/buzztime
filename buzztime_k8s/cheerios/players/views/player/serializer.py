from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class GetSiteGamePlayersSerializer(SerializerValidator):

    site_id = serializers.IntegerField(required=True, allow_null=False)
    game_id = serializers.IntegerField(required=True, allow_null=False)

class GetSiteGameInstancePlayersSerializer(SerializerValidator):
    site_id = serializers.IntegerField(required=True, allow_null=False)
    game_id = serializers.IntegerField(required=True, allow_null=False)
