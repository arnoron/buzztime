"""
This module is the implementation of login from /m page as
sent from the tablet
"""
from rest_framework import status
from rest_framework.response import Response
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.serializers import TabletLoginSerializer
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.login import LoginViewSet
from cheerios.services.log_classifier import Logger


class TabletLoginViewSet(PlayersViewSet):
    """
    This class takes in the pid, ph and timestamp
    It validates the parameters
    It then creates an md5 hash and compares with the ph received
    If hash is the same as ph, the login is successful
    """
    @get_store_procedure_api
    def post(self, request):
        """
        Logins the user
        :param request:
        :return:
        """
        request_params = request.data

        serializer = TabletLoginSerializer(data=request_params)

        if not serializer.is_valid():
            return UTIL_FUNCTION.log_error(400, serializer.errors)
        web_secret = self._get_web_secret()
        ts = request_params.get('timestamp')
        player_id = request_params.get('player_id')
        tssalted = int(ts) + (int(ts) % (173 * 89))
        hash_value = UTIL_FUNCTION.create_md5_hash(player_id + \
                                                   ts + str(tssalted) + \
                                                   web_secret \
                                                  )

        if hash_value != request_params.get('prepared_hash'):
            return UTIL_FUNCTION.log_error(400, "Hash does not match")

        login_interface = LoginViewSet()

        player_profile = login_interface.get_player_profile(request_params.get('player_id'))
        if not player_profile:
            return UTIL_FUNCTION.log_error(400, "User not registered")

        response = Response(player_profile, status.HTTP_200_OK)
        session_id = login_interface.get_session_key(player_profile['username'])
        response['Access-Control-Expose-Headers'] = 'Session'
        response["Session"] = session_id
        return response

    def _get_web_secret(self):
        """
        Retrieves the websecret from the database
        :return:
        """
        store_procedure = STORED_PROCEDURE_MAPPER['platform_services_config']
        database = store_procedure.split(".")[0]
        data = "@NamePattern='Playmaker.Tablet.WebSecret'"

        cursor = self.get_cursor(database)
        self.call_storeprocedure(store_procedure, cursor, data)
        result = self.fetchall_from_cursor(cursor)
        return result[0].get('Value')
