"""
Module which holds the PlayerPlusLeader class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER, PLAYER_FIELDS_MAPPER
from cheerios.players.views.players import PlayersViewSet,UTIL_FUNCTION



class PlayerPlusLeader(PlayersViewSet):
    """
    Returns the player plus leader response
    """
    def get_playerplus_leaders(self, state, handle):
        """
        Returns the player plus leaders for a state and a handle
        :param state:
        :param handle:
        :return:
        """

        first_row = 1
        row_count = 200

        data = dict(RowStart=first_row, RowEnd=row_count)

        if state: data['State'] = state
        if handle: data['HandleLike'] = handle
        stored_procedure = STORED_PROCEDURE_MAPPER['player_plus_leaders']
        try:
            result = self.execute_stored_procedure(stored_procedure, data)
        except Exception:
            return UTIL_FUNCTION.log_error(400,'Invalid parameters passed state and handle')
        result = self.format_response(result.data, PLAYER_FIELDS_MAPPER)
        return result
