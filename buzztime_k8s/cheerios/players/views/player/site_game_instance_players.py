"""
Module which holds the SiteGameInstancePlayers class
"""

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from .serializer import GetSiteGameInstancePlayersSerializer



class SiteGameInstancePlayers(PlayersViewSet):
    """
    Returns the site game instance players response
    """

    def get_site_gameinstance_players(self, site_id, game_id, start_date, start_time):
        """
        Returns the site game instance players for passed params
        :param site_id:
        :param game_id:
        :param game_time:
        :return:
        """
        serializer =GetSiteGameInstancePlayersSerializer(data=dict(site_id=site_id,game_id=game_id))
        if serializer.is_valid():
            try:
                start_date_time = UTIL_FUNCTION.format_date_time(start_date, start_time)
            except ValueError:
                return UTIL_FUNCTION.log_error(400, "Date or Time does not match the intended "
                                                    "format. Date format is dd-mm-yyyy and time "
                                                    "format is hh:mm")

            data_string = "@GameStartTime='{0}',@SiteID={1},@GameNameID={2}".format(start_date_time,
                                                                                    site_id,
                                                                                    game_id)

            stored_procedure = STORED_PROCEDURE_MAPPER['site_game_player_history']
            result = self.execute_stored_procedure(stored_procedure, data_string)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, "Invalid or no parameters passed")