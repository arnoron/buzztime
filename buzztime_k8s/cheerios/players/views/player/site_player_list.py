"""
Module which holds the SitePlayerList class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.services.log_classifier import Logger


class SitePlayerList(PlayersViewSet):
    """
    Returns the site player list response
    """

    def get_players_for_site(self, site_id):
        """
        Returns the players for a site
        :param site_id:
        :return:
        """

        row_limit = 1000
        request_data = dict(SiteID=site_id, RowLimit=row_limit)

        stored_procedure = STORED_PROCEDURE_MAPPER['site_players']
        result = self.execute_stored_procedure(stored_procedure, request_data)
        return result
