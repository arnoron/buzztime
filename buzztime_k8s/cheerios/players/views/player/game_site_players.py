"""
Module which holds the GameSitePlayers class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from .player_serializer import GetGameSitePlayersSerializer




class GameSitePlayers(PlayersViewSet):
    """
    Returns the game site players response
    """

    def get_game_site_players(self, premium_game_id, site_id):
        """
        Returns the game site players based on the game id and the site id
        :return:
        """
        serializer = GetGameSitePlayersSerializer(data=dict(premium_game_id=premium_game_id,site_id=site_id))

        if serializer.is_valid():
            request_data = dict(PremiumGameID=premium_game_id,
                                SiteID=site_id)
            stored_procedure = STORED_PROCEDURE_MAPPER['site_player_scores']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, "Invalid Parameters Passed")