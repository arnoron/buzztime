"""
API for hash login
"""
from rest_framework.response import Response
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.login import LoginViewSet
from cheerios.players.serializers import PlayerAuthentication
from cheerios.utils import CheeriosUtils

CHERRIOS_UTILS = CheeriosUtils()



class HashLoginViewset(PlayersViewSet):
    """
    Logs in the user with the hashed password
    :param LoginViewSet:
    :return:
    """

    @get_store_procedure_api
    def post(self, request):
        """
        Logs in the user with the hashed password
        :return:
        """

        request_param = request.POST

        serializer = PlayerAuthentication(data=request_param)
        CHERRIOS_UTILS.validate_serializer(serializer)

        hashed_password = request_param.get('password')
        username = request_param.get('username')
        data = dict(Username=username, NamespaceID=1)

        store_procedure = STORED_PROCEDURE_MAPPER['hash_authentication']

        response = self.execute_stored_procedure(store_procedure, data)

        if response.__dict__.get('data'):
            response_dict = response.__dict__.get('data')[0]
        else:
            return UTIL_FUNCTION.log_error(400, "User not registered")

        db_hashed_password = response_dict.get('NamespacePassword')
        player_id = response_dict.get('PlayerID')
        login_interface = LoginViewSet()

        if db_hashed_password == hashed_password:
            player_profile = login_interface.get_player_profile(player_id)
        else:
            return Response(False, status=200)

        response = Response(player_profile, 200)

        if CHERRIOS_UTILS.does_auth_token_exist_for_user(player_id, mobile=False):
            auth_token = CHERRIOS_UTILS.get_auth_token_for_user(player_id, mobile=False)
        else:
            auth_token = CHERRIOS_UTILS.create_auth_token_for_user(player_id, persist=False,
                                                                   mobile=False)
        response["Session"] = auth_token
        response['Access-Control-Expose-Headers'] = 'Session'

        return response
