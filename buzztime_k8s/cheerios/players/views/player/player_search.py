"""
Module which holds the PlayerSearch class
"""

from cheerios.players.constants import STORED_PROCEDURE_MAPPER, PLAYER_FIELDS_MAPPER
from cheerios.players.views.players import PlayersViewSet,UTIL_FUNCTION
from cheerios.players.views.serializer import CheckCharSerializer


class PlayerSearch(PlayersViewSet):
    """
    Retuns the player search response
    """
    def get_player_search_response(self, search_param):
        """
        Returns the player search response for a search parameter
        :param search_param:
        :return:
        """
        serializer = CheckCharSerializer(data=dict(char_value=search_param))
        if serializer.is_valid():
            data = dict(Handle=search_param)
            stored_procedure = STORED_PROCEDURE_MAPPER['player_search']
            result = self.execute_stored_procedure(stored_procedure, data)
            result = self.format_response(result.data, PLAYER_FIELDS_MAPPER)
            return result
        else:
            return UTIL_FUNCTION.log_error(400,'Invalid search_params passed')
