from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class GetGameSitePlayersSerializer(SerializerValidator):

    premium_game_id = serializers.IntegerField(required=True, allow_null=False)
    site_id = serializers.IntegerField(required=True, allow_null=False)


