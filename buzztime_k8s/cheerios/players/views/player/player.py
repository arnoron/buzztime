"""
Module contains the base routing class for the players API
"""

from rest_framework.response import Response
from cheerios.settings import MSSQLUSERDB, MSSQLDBGAMEPLAY, MSSQLPLAYERPLUSDB

from cheerios.players.views.badge.badge_details import BadgeDetail
from cheerios.players.views.badge.badge_list import BadgeList
from cheerios.players.views.game.player_game_list import PlayerGameList
from cheerios.players.views.game_instance.casino_game_instance import CasinoGameInstance
from cheerios.players.views.game_instance.game_instance_details import GameInstanceDetails
from cheerios.players.views.game_instance.recent_game_instance import RecentGameInstance
from cheerios.players.views.player.badge_leader import BadgeLeader
from cheerios.players.views.player.player_details import PlayerDetails
from cheerios.players.views.player.player_search import PlayerSearch
from cheerios.players.views.player.playerplus_leader import PlayerPlusLeader
from cheerios.players.views.player.top_trivia_player import TopTriviaPlayer
from cheerios.players.views.player.user_registration import UserRegistrationViewSet
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.decorators import get_store_procedure_api
from cheerios.services.log_classifier import Logger

class PlayerViewSet(PlayersViewSet):
    """
    Player API main class. This class is used to route the request
    into different functions.
    The routing algorithm is as follows:
    if player id is not passed, it means that the user needs
    information about a group of players and then the sorting or
    searching params will be handled. Else the information
    related to the passed player id will be returned.
    """
    @get_store_procedure_api
    def get(self, request, player_id, player_info_subtype, player_info_subtype_id):
        """
        Returns the players response
        :return:
        """

        request_params = request.GET

        if not player_id:

            if request_params.get('email'):
                email = request_params.get('email')
                player_interface = PlayerDetails()
                response = player_interface.get_player_details(None, email)
            else:
                response = self.handle_search_and_sort(request_params)

        else:
            if player_info_subtype:
                response = self.handle_player_info_subtype(request_params,
                                                           player_id,
                                                           player_info_subtype,
                                                           player_info_subtype_id
                                                          )

            else:
                player_interface = PlayerDetails()
                response = player_interface.get_player_details(player_id)

        return response

    @get_store_procedure_api
    def post(self, request, player_id, player_info_subtype, player_info_subtype_id):
        """

        :param request:
        :return:
        """
        user_reg_interface = UserRegistrationViewSet()
        user_cursor = self.get_cursor(MSSQLUSERDB, True)
        player_cursor = self.get_cursor(MSSQLPLAYERPLUSDB, True)
        self.conn_flag = 1
        return user_reg_interface.register_user(request, player_cursor, user_cursor)

    def handle_player_info_subtype(self, request_params,
                                   player_id,
                                   player_info_subtype,
                                   player_info_subtype_id):
        """
        Returns the players response when the player info subtype is passed
        :return:
        """
        if player_info_subtype == 'badges':
            response = self.handle_player_badges(request_params, player_id, player_info_subtype_id)

        elif player_info_subtype == 'games':
            game_interface = PlayerGameList()
            response = game_interface.get_game_list(player_id)

        elif player_info_subtype == 'gameinstances':
            response = self.handle_player_game_instances(request_params,
                                                         player_id,
                                                         player_info_subtype_id)

        return response

    @staticmethod
    def handle_player_game_instances(request_params, player_id, player_info_subtype_id):
        """
        Returns the players response when the gameinstances subtype is passed
        :return:
        """
        if player_info_subtype_id:
            mode = request_params.get('mode')
            game_instance_interface = GameInstanceDetails()
            response = game_instance_interface.\
                get_game_instance_details(player_id, player_info_subtype_id, mode)

        else:
            game_instance_type = request_params.get('type')
            if game_instance_type == 'recent':
                recent_game_instance_interface = RecentGameInstance()
                response = recent_game_instance_interface.get_recent_game_instances(player_id)

            elif game_instance_type == 'casino':
                game = request_params.get('game')
                casino_game_instance_interface = CasinoGameInstance()
                response = casino_game_instance_interface.get_casino_game_instances(player_id, game)

            else:
                Logger.log.error("Invalid game instance type "
                             "parameter passed %s", game_instance_type)
                return UTIL_FUNCTION.log_error(400, "Invalid game instance type passed")

        return response

    @staticmethod
    def handle_player_badges(request_params, player_id, player_info_subtype_id):
        """
        Returns the players response when the badges subtype is passed
        :param request_params:
        :param player_id:
        :param player_info_subtype_id:
        :return:
        """
        if player_info_subtype_id:
            badge_detail_interface = BadgeDetail()
            response = badge_detail_interface.get_badge_details(player_id, player_info_subtype_id)

        else:
            badge_list_interface = BadgeList()
            badge_type = request_params.get('type')
            response = badge_list_interface.get_badge_list(player_id, badge_type)

        return response

    def handle_search_and_sort(self, request_params):
        """
        Returns the players response when the search or sort params are passed
        :param request_params:
        :return:
        """
        if request_params.get('search'):
            response = self.handle_player_search(request_params)

        elif request_params.get('sort'):
            response = self.handle_sort(request_params)

        else:
            return UTIL_FUNCTION.log_error(400, "Invalid query parameter passed")

        return response

    @staticmethod
    def handle_player_search(request_params):
        """
        Returns the players response when the search param is passed
        :param request_params:
        :return:
        """
        player_interface = PlayerSearch()
        search_param = request_params.get('search')
        response = player_interface.get_player_search_response(search_param)
        return response

    def handle_sort(self, request_params):
        """
        Returns the players response when the sort param is passed
        :return:
        """
        sort_param = request_params.get('sort')

        if sort_param == 'num-badges-desc':
            response = self.handle_num_badges_sort(request_params)

        elif sort_param == 'playerplus-points-desc':
            response = self.handle_playerplus_points_sort(request_params)

        elif sort_param == 'trivia-average-desc':
            response = self.handle_trivia_average_sort(request_params)

        else:
            UTIL_FUNCTION.log_error(400, "Invalid Sorting Criteria passed {0}".format(sort_param))
            return Response("Invalid Sorting Criteria passed", status=400)

        return response


    @staticmethod
    def handle_num_badges_sort(request_params):
        """
        Returns the players response when the num_badges sort param is passed
        :param request_params:
        :return:
        """
        player_interface = BadgeLeader()

        start_date = request_params.get('start-date')
        end_date = request_params.get('end-date')

        badge_classifier = request_params.get('badge-classifier')
        badge_type = request_params.get('badge-type-id')
        response = player_interface.\
            get_badge_leaders_response(start_date, end_date, badge_classifier,
                                       badge_type
                                      )
        return response

    @staticmethod
    def handle_playerplus_points_sort(request_params):
        """
        Returns the players response when the playerplus_points sort params are passed
        :param request_params:
        :return:
        """
        player_interface = PlayerPlusLeader()
        state = request_params.get('state')
        handle = request_params.get('handle')
        response = player_interface.get_playerplus_leaders(state, handle)
        return response

    @staticmethod
    def handle_trivia_average_sort(request_params):
        """
        Returns the players response when the trivia_average sort params are passed
        :param request_params:
        :return:
        """
        player_interface = TopTriviaPlayer()
        game = request_params.get('game-id', 87)
        state = request_params.get('state')
        handle = request_params.get('handle')
        response = player_interface.get_top_trivia_players(game, state, handle)
        return response
