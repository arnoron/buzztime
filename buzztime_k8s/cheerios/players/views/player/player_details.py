"""
Module which contains the PlayerDetails class
"""
import json

from collections import Counter

from pymssql import DatabaseError
from cheerios.players.constants import STORED_PROCEDURE_MAPPER, \
    NAME_SPACE_TYPE, ACHIEVEMENT_PLATFORM, PLAYER_FIELDS_MAPPER, \
    PLAYERS_UTILS_CONSTANTS
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.settings import MSSQLUSERDB, MSSQLDBGAMEPLAY
from cheerios.players.decorators import close_connection
from cheerios.players.views.serializer import CheckIntegerOptionalSerializer



class PlayerDetails(PlayersViewSet):
    """
    Interface to access the player details for a player
    """
    def get_player_details(self, player_id=None, email=None):
        """
        Returns the player details for a player
        :param player_id:
        :return:
        """
        if not player_id and not email:
            return UTIL_FUNCTION.log_error(400, 'No parameters passed for player_id and email')

        serializer = CheckIntegerOptionalSerializer(data=dict(integer_value = player_id))
        if serializer.is_valid():
            response_list = []
            try:
                response_data = self.get_member_info(player_id, email)
                if response_data:
                    player_id = response_data['PlayerID']
                    response_data['BadgeCount'] = self.get_total_badge_count(player_id)
                    response_data['BuzztimeSportsCount'] = self.get_sports_stats(player_id)
                    response_data.update(self.get_game_averages(player_id))
                    response_data.update(self.get_casino_stats(player_id))
                else:
                    return UTIL_FUNCTION.log_error(400, "Email or Player id does not exists")

            except DatabaseError as ex:
                return UTIL_FUNCTION.log_error(400, str(ex))
            response_list.append(response_data)
            result = self.format_response(response_list, PLAYER_FIELDS_MAPPER)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, "Invalid parameters passed")

    @close_connection
    def get_member_info(self, player_id, email, cursor=None):
        """
        Returns the player details
        :param player_id:
        :return:
        """

        database_name = MSSQLUSERDB
        forum_user_id = 0

        data_string = "@PlayerID={0}, @ForumUserID={1}".format(player_id, forum_user_id)
        if not player_id:
            data_string = "@Email='{0}',@ForumUserID={1}".format(email, forum_user_id)
        stored_procedure = STORED_PROCEDURE_MAPPER['member_info']
        with self.connection_manager(stored_procedure, data_string, database_name) as cursor:
            result = self.fetchall_from_cursor(cursor)
        if result:
            return result[0]
        return {}

    @close_connection
    def get_total_badge_count(self, player_id):
        """
        Returns the player's total badge count
        :param player_id:
        :return:
        """
        if not player_id:
            player_id = ''
        database_name = MSSQLUSERDB
        stored_procedure = STORED_PROCEDURE_MAPPER['achievement_earned_count']
        data = dict(NamespaceID=NAME_SPACE_TYPE['BuzztimeCom'],
                    NamespaceUserID=player_id,
                    AchievementPlatformID=ACHIEVEMENT_PLATFORM['BuzztimeNetwork'])

        data_string = self.get_data_string(data)

        with self.connection_manager(stored_procedure, data_string, database_name) as cursor:
            result = self.fetchall_from_cursor(cursor)[0]
        if not result['TotalEarnedCount']:
            result['TotalEarnedCount'] = 0

        return result['TotalEarnedCount']

    @close_connection
    def get_sports_stats(self, player_id):
        """
        Returns the sports stats
        :param player_id:
        :return:
        """
        database_name = MSSQLDBGAMEPLAY

        request_data = dict(GameNameID=PLAYERS_UTILS_CONSTANTS['GAMENAMEID_BUZZTIME_SPORTS'],
                            PlayerID=player_id
                       )

        stored_procedure = STORED_PROCEDURE_MAPPER['sports_stats']

        data_string = self.get_data_string(request_data)

        with self.connection_manager(stored_procedure, data_string, database_name) as cursor:
            result = self.fetchall_from_cursor(cursor)
        list_data = self.format_response_data(result)

        if not list_data:
            list_data = 0

        return list_data

    @close_connection
    def get_game_averages(self, player_id):
        """
        Returns the players game averages
        :param player_id:
        :return:
        """
        game_derivative_id = 0
        database_name = MSSQLDBGAMEPLAY

        request_data = dict(PlayerID=player_id, GameSubTypeDerivativeID=game_derivative_id)

        stored_procedure = STORED_PROCEDURE_MAPPER['game_type_averages']

        data_string = self.get_data_string(request_data)

        with self.connection_manager(stored_procedure, data_string, database_name) as cursor:
            result = self.fetchall_from_cursor(cursor)
        counter = Counter()

        dict_str = json.dumps(result)
        list_data = json.loads(dict_str, object_pairs_hook=UTIL_FUNCTION.dict_clean)

        for game in list_data:
            counter.update(game)

        averages_dict = dict(counter)

        if averages_dict:
            response_data = dict(NationalTop100Count=averages_dict['NationalTop100'],
                                 LocalWinsCount=averages_dict['LocalWins'],
                                 NationalGoldCount=averages_dict['NationalGold'],
                                 NationalSilverCount=averages_dict['NationalSilver'],
                                 NationalBronzeCount=averages_dict['NationalBronze'])
        else:
            response_data = dict(NationalTop100Count=0,
                                 LocalWinsCount=0,
                                 NationalGoldCount=0,
                                 NationalSilverCount=0,
                                 NationalBronzeCount=0)

        return response_data

    def get_casino_stats(self, player_id):
        """
        Returns the casino stats for a player
        :param player_id:
        :return:
        """

        holdem_game_id = 4
        blackjack_game_id = 2

        casino_stats = dict(HoldemChipCount=self.get_casino_chip_count(player_id, holdem_game_id),
                            BlackjackChipCount=self.get_casino_chip_count
                            (player_id, blackjack_game_id))
        return casino_stats

    @close_connection
    def get_casino_chip_count(self, player_id, game_id):
        """
        Returns the casino chip count
        :param player_id:
        :return:
        """
        game_derivative_id = 0
        database_name = MSSQLDBGAMEPLAY

        request_data=dict(PlayerID=player_id, GameSubType=game_id,
                          GameSubTypeDerivativeID=game_derivative_id)

        stored_procedure = STORED_PROCEDURE_MAPPER['game_casino_stats']

        data_string = self.get_data_string(request_data)

        with self.connection_manager(stored_procedure, data_string, database_name) as cursor:
            result = self.fetchall_from_cursor(cursor)
        return result[0]['TotalScore'] if result else 0
