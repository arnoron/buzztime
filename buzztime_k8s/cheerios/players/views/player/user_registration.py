"""
APIs for user registration
"""
import datetime
import random

import time
from threading import Thread

import requests
from pytz import timezone
from rest_framework.response import Response

from cheerios.constant.constant import TRUE_VALUES

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import ENTITY_TYPE, POINTS_AWARD_TYPE, STORED_PROCEDURE_MAPPER
from cheerios.players.serializers import RegistrationSerializer
from cheerios.players.views.check_email_availability import EmailAvailabilityViewSet
from cheerios.players.views.check_username_availability import UsernameAvailabilityViewSet
from cheerios.services.Email.email import Email
from cheerios.services.check_inappropriate_word import InAppropriateWordViewSet
from cheerios.services.log_classifier import Logger
from cheerios.services.validate_captcha import validate_captcha
from cheerios.settings import ARCAMAX_URL
from cheerios.utils import CheeriosUtils

CHERRIOS_UTILS = CheeriosUtils()

API_TIMEOUT = 10
SLEEP_TIME = 5
MAX_RETRIES = 5


class UserRegistrationViewSet(MSSQLApiView):
    """
    API viewset for user registration
    """

    def __init__(self):
        """
        Defines constants
        """

        super().__init__()

        self.forum_user_id = 0
        self.home_site_id = 0
        self.referrer_id = 0
        self.registration_player_plus_points = 5000

        self.inappropriate_word_check = InAppropriateWordViewSet()
        self.duplicate_username_check = UsernameAvailabilityViewSet()
        self.duplicate_email_check = EmailAvailabilityViewSet()
        self.error_messages = dict()
        self.listids = "listid.1=3&listid.2=52"
        self.source = "source=4811"
        self.minimum_dob_year_allowed = 1900

    @validate_captcha()
    def register_user(self, request, player_cursor, user_cursor):
        """
        User registration API
        :return:
        """

        error_occurred = False

        request_params = request.data

        version = request_params.get('api_version', "1.0")

        user_data = dict(display_name=request_params.get('display_name'),
                         username=request_params.get('username'),
                         password=request_params.get('password'),
                         postal_code=request_params.get('postal_code'),
                         email=request_params.get('email'),
                         dob=request_params.get('dob'),
                         site_id=request_params.get('site_id', 0),
                         opt_in=request_params.get('opt_in'),
                         platform_api_key=request_params.get('platform_api_key'),
                         api_version=version
                        )

        sign_up_obj = RegistrationSerializer(data=user_data)
        CHERRIOS_UTILS.validate_serializer(sign_up_obj)

        if int(user_data['dob'].split("-")[2]) < self.minimum_dob_year_allowed:
            return Response("Date of Birth year should be greater than 1900", 400)

        platform_api_key = user_data['platform_api_key']
        user_data['reg_source'] = self.get_registration_source_name(platform_api_key, user_cursor)

        if user_data.get('display_name'):
            user_data['display_name'] = user_data.get('display_name').upper()

        if not user_data.get('username'):
            user_data['username'] = self.generate_user_name(user_data.get('display_name'),
                                                            user_cursor)

        inappropriate_words = self.inappropriate_word_check.load_inappropriate_words()

        if self.inappropriate_word_check.contains_inappropriate_word(user_data['username'],
                                                                     inappropriate_words):
            status_value = 400
            error = "{0} contains inappropriate text".format(user_data["username"])
            CHERRIOS_UTILS.log_error(status_value, error)
            self.error_messages['username_errors'] = dict(message=error, status=status_value)
            error_occurred = True

        if self.inappropriate_word_check.contains_inappropriate_word(user_data['display_name'],
                                                                     inappropriate_words):
            status_value = 400
            error = "{0} contains inappropriate text".format(user_data['display_name'])
            CHERRIOS_UTILS.log_error(status_value, error)
            self.error_messages['display_name_errors'] = dict(message=error, status=status_value)
            error_occurred = True

        if self.duplicate_username_check.username_exists(user_data['username']):
            status_value = 400
            error = "Username is already taken"
            CHERRIOS_UTILS.log_error(status_value, error)
            self.error_messages['username_errors'] = dict(message=error, status=status_value)
            error_occurred = True

        if self.duplicate_email_check.email_exists(user_data['email']):
            status_value = 400
            error = "Email is already taken"
            CHERRIOS_UTILS.log_error(status_value, error)
            self.error_messages['email_errors'] = dict(message=error, status=status_value)
            error_occurred = True

        if error_occurred:
            return Response(self.error_messages, 400)

        add_player_result = self.register_player(user_data, user_cursor)
        if add_player_result and add_player_result.get('player_id'):
            self.award_player_plus_points(add_player_result['player_id'],
                                          self.registration_player_plus_points, player_cursor)
            add_player_result['player_plus_points'] = self.registration_player_plus_points
            CHERRIOS_UTILS.create_auth_token_for_user(add_player_result['player_id'], persist=True,
                                                      mobile=True)

        player_id = add_player_result.get('player_id')
        user_data["player_id"] = player_id

        site_id = user_data['site_id']
        user_data['Reg_site_id'] = site_id
        user_data['Reg_site_name'] = self.get_site_name(site_id, user_cursor)

        email = Email()
        email.register_user_to_bronto(user_data)

        opt_in = user_data.get('opt_in')
        if opt_in in TRUE_VALUES:
            self.newsletter_subscription(user_data, player_id)
        self.save_player_opt_in(player_id, opt_in, player_cursor)

        return Response(add_player_result, status=200)

    def get_registration_source_name(self, platform_api_key, cursor):
        """
        Resolves platform_api_key into source name if not found returns buzztime.com by default
        :param platform_api_key:
        :param cursor:
        :return: registration source name
        """
        if platform_api_key:
            parameters = "@PlatformAPIKey='{0}'".format(platform_api_key)
            store_procedure = STORED_PROCEDURE_MAPPER['platform_api_key']
            self.call_storeprocedure(store_procedure, cursor, parameters)
            result = self.fetchall_from_cursor(cursor)
            if result:
                return result[0]['PlatformName']

            status_value = 400
            error = "Invalid platform API Key {0}".format(platform_api_key)
            CHERRIOS_UTILS.log_error(status_value, error)

            raise ValueError("Invalid Parameters Passed")

        return "Buzztime.com"

    def get_site_name(self, site_id, cursor):
        """
        Resolves site_id into name
        :param site_id:
        :param cursor:
        :return: site_name
        """

        parameters = "@SiteID={}".format(site_id)
        store_procedure = STORED_PROCEDURE_MAPPER['site_name']

        self.call_storeprocedure(store_procedure, cursor, parameters)
        result = self.fetchall_from_cursor(cursor)
        if result:
            return result[0]['OrgName']

        return ""

    def newsletter_subscription(self, user_data, player_id):
        """
        Call third party URL to subscribe Email id.
        :param user_data: user object
        :param player_id:player id of player object
        :return:
        """
        email_id = user_data.get('email')
        email = "email={0}".format(email_id)
        fname = "fname={0}".format(user_data.get('display_name'))

        date_format = '%m/%d/%Y %H:%M:%S'
        date_time = datetime.datetime.now(timezone('US/Pacific'))
        date_time = date_time.strftime(date_format)

        time = "ts={0}".format(date_time.replace(" ", "%20"))

        player_id = "leadid={0}".format(player_id)
        arcamaxurl = "{0}?{1}&{2}&{3}&{4}&{5}&{6}".format(ARCAMAX_URL,
                                                          email,
                                                          fname,
                                                          self.listids,
                                                          self.source,
                                                          time,
                                                          player_id)
        site_id = user_data.get("site_id")
        if site_id:
            arcamaxurl = "{0}&scextcode={1}".format(arcamaxurl, site_id)
        thread = Thread(target=self.subscription, args=(arcamaxurl, email_id))
        thread.start()

    def subscription(self, arcamaxurl, email_id):
        """
        Retrying Subscription in case it fails due to internal error.
        We can retry 5 times.
        """
        for retries in range(0, MAX_RETRIES):
            try:
                response = requests.get(arcamaxurl, timeout=API_TIMEOUT)
                response_message = response.content.decode("utf-8").upper()
                if 'FAIL' in response_message:
                    self.log_max_retries_error(email_id, retries)
                    time.sleep(SLEEP_TIME)
                    continue
                elif not 'SUBPEND' in response_message:
                    error = "{0} failed to subscribed  arcamax mail due to {1} response". \
                        format(email_id, response_message)
                    CHERRIOS_UTILS.log_error(404, error)

                break
            except Exception:
                self.log_max_retries_error(email_id, retries)

    @staticmethod
    def log_max_retries_error(email_id, retries):
        if retries == MAX_RETRIES - 1:
            error = "{0} failed to subscribed  Arcamax mail due to Max Retries Exceeded". \
                format(email_id)
            CHERRIOS_UTILS.log_error(500, error)

    def save_player_opt_in(self, player_id, status, player_cursor):
        """
        save email is subscribe or not to Db.
        :param player_id: player id of user
        :param status: status if user want to subscribe email.
        :param player_cursor: mssql cursor.
        :return:
        """
        status_id = 0
        if status in TRUE_VALUES:
            status_id = 1
        parameters = "@PlayerID={0}, @OptinID=6, @StatusID={1}".format(player_id, status_id)
        store_procedure = STORED_PROCEDURE_MAPPER['save_player_opt_in']
        self.call_storeprocedure(store_procedure, player_cursor, parameters)

    def award_player_plus_points(self, player_id, points, cursor):
        """
        Awards the registered player plus points after registration
        :param player_id:
        :param points:
        :return:
        """
        current_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
        parameters = "@PlayerID={0}, @PointsAwarded={1}, @PointsAwardTypeID={2}," \
                     "@PointsAwardReferenceID={3}, @PointsAwardDate='{4}'".format(
            player_id,
            points,
            POINTS_AWARD_TYPE['PPlusRegistration'],
            player_id,
            current_date[:-3]
        )

        store_procedure = STORED_PROCEDURE_MAPPER['points_award_queue']

        self.call_storeprocedure(store_procedure, cursor, parameters)

    def generate_user_name(self, base_name, cursor):
        """
        Generates random username based on the display name
        :param display_name:
        :return:
        """
        name_prefix = ""
        suffix = 10

        if not base_name:
            name_prefix = "USER"
        else:
            name_prefix = base_name

        usernames = self.get_user_names(name_prefix, cursor)

        if not usernames:
            temp_name = name_prefix + "10"
        else:
            while name_prefix + str(suffix) in usernames:
                suffix += 1
            temp_name = name_prefix + str(suffix)

        return temp_name

    def get_user_names(self, username_base, cursor):
        """
        Gets all the similar usernames to the username_base
        :param name_prefix:
        :return:
        """
        usernames = []
        parameters = "@UserNamePattern={}".format(username_base)
        store_procedure = STORED_PROCEDURE_MAPPER['namespace_identity']

        self.call_storeprocedure(store_procedure, cursor, parameters)
        result = self.fetchall_from_cursor(cursor)
        if result:
            for username in result:
                usernames.append(username.get("NamespaceIdentity").upper())

        return usernames

    def register_player(self, user_data, cursor):
        """
        Registers the user in the db
        :return:
        """
        registration_source_name = user_data['reg_source']
        version = user_data['api_version']
        player_profile = {}
        salt = self.get_random_salt_value()

        site_id = 0
        if user_data.get('site_id'):
            site_id = user_data.get('site_id')

        if version == "1.1":
            hashed_password = CHERRIOS_UTILS.create_md5_hash(\
            "{0}{1}".format(CHERRIOS_UTILS.create_md5_hash(user_data.get('password')), salt))
        elif version == "1.0":
            hashed_password = CHERRIOS_UTILS.create_md5_hash( \
                "{0}{1}".format(user_data.get('password'), salt))
        else:
            CHERRIOS_UTILS.log_error(400, "Invalid api_version passed")
            raise ValueError("Invalid Parameter passed")

        parameters = "@UserName='{0}', @Password='{1}', @Salt='{2}', " \
                     "@Handle='{3}', @PostalCode='{4}', @Email='{5}'," \
                     "@ForumUserID={6}," \
                     "@RegistrationSourceName='{7}', @NamespaceID={8}," \
                     "@SiteID={9}, @ReferringID={10}, @BirthDate='{11}'".format(
            user_data['username'],
            hashed_password,
            salt,
            user_data['display_name'],
            user_data['postal_code'],
            user_data['email'],
            self.forum_user_id,
            registration_source_name,
            ENTITY_TYPE.get('RegisteredPlayer'),
            user_data['site_id'],
            self.referrer_id,
            datetime.datetime.strptime(user_data['dob'], "%d-%m-%Y")
        )

        utils_obj = CheeriosUtils
        parameters = utils_obj.escape_quotes_sp_params(parameters)

        store_procedure = STORED_PROCEDURE_MAPPER["registration"]

        self.call_storeprocedure(store_procedure, cursor, parameters)
        result = self.fetchall_from_cursor(cursor)[0]

        if result:
            player_profile = {'player_id': result.get('PlayerID'),
                              'pin': result.get('PIN'),
                              'username': user_data.get('username')
                              }

            if not result['PlayerID']:
                message = "Registration failed. Database returned PlayerID=0"
                Logger.log.error(message)
        else:
            message = "Registration failed. Store procedure didn't return any data"
            Logger.log.error(message)

        return player_profile

    @staticmethod
    def get_random_salt_value():
        """
        Gets random salt value
        :return:
        """
        return ''.join(random.choice
                       ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
                       for i in range(3))
