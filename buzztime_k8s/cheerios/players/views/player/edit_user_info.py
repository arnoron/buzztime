"""
Updates the user info
"""
from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import STORED_PROCEDURE_MAPPER, ENTITY_TYPE
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.serializers import UserSerializer
from cheerios.players.views.check_email_availability import EmailAvailabilityViewSet
from cheerios.players.views.check_username_availability import UsernameAvailabilityViewSet
from cheerios.players.views.login import LoginViewSet
from cheerios.players.views.player.player_details import PlayerDetails
from cheerios.services.check_inappropriate_word import InAppropriateWordViewSet
from cheerios.services.is_authenticated import is_authenticated
from cheerios.settings import MSSQLPLAYERPLUSDB, MSSQLUSERDB
from cheerios.utils import CheeriosUtils

CHERRIOS_UTILS = CheeriosUtils()


class EditUserViewSet(MSSQLApiView):
    """
    Viewset for the edit user API. It takes player id as mandatory
    field and updates the player with the other details passed.
    """

    def __init__(self):
        """
        Defines Class scope constants
        """
        super().__init__()
        self.inappropriate_word_check = InAppropriateWordViewSet()
        self.duplicate_username_check = UsernameAvailabilityViewSet()
        self.duplicate_email_check = EmailAvailabilityViewSet()
        self.login_viewset_interface = LoginViewSet()
        self.player_details_viewset = PlayerDetails()
        self.error_messages = dict()
        self.password_and_salt = {}

    @is_authenticated()
    @get_store_procedure_api
    def post(self, request):
        """
        Gets the post params and calls the update methods to update the db
        after validation
        :param request:
        :return:
        """

        error_occurred = False
        request_params = request.POST

        serializer = UserSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)

        player_id = request_params.get('player_id')
        user_cursor = self.get_cursor(MSSQLUSERDB, True)
        player_info = self.login_viewset_interface.get_player_profile(player_id, user_cursor)
        player_details = self.player_details_viewset.get_member_info(player_id, None, user_cursor)
        player_info.update(player_details)

        data = dict(display_name=request_params.get('display_name'),
                    username=request_params.get('username'),
                    password=request_params.get('password'),
                    site_id=request_params.get('site_id', player_info.get('site_id')),
                    player_id=player_id,
                    postal_code=request_params.get('postal_code', player_info.get('postal_code')),
                    email=request_params.get('email', player_info.get('Email')),
                    first_name=request_params.get('first_name', player_info.get('FirstName')),
                    last_name=request_params.get('last_name', player_info.get('LastName')),
                    address1=request_params.get('address1', player_info.get('Address1')),
                    address2=request_params.get('address2', player_info.get('Address2')),
                    city=request_params.get('city', player_info.get('City')),
                    state=request_params.get('state', player_info.get('State')),
                    phone_no=request_params.get('phone_no', player_info.get('PhoneNo')),
                    mobile_phone_no=request_params.get
                    ('mobile_phone_no', player_info.get('MobilePhoneNo'))
                   )

        if data.get('display_name'):
            data['display_name'] = data.get('display_name').upper()
        else:
            data['display_name'] = player_info['display_name'].upper()

        inappropriate_words = self.inappropriate_word_check.load_inappropriate_words()

        if data['username']:
            if self.inappropriate_word_check.contains_inappropriate_word(data['username'],
                                                                         inappropriate_words):
                status_value = 400
                error = "{0} contains inappropriate text".format(data["username"])
                CHERRIOS_UTILS.log_error(status_value, error)
                self.error_messages['username_errors'] = dict(message=error, status=status_value)
                error_occurred = True
        else:
            data['username'] = player_info['username']

        if data['display_name'] and self.inappropriate_word_check.contains_inappropriate_word(
                data['display_name'], inappropriate_words):
            status_value = 400
            error = "{0} contains inappropriate text".format(data['display_name'])
            CHERRIOS_UTILS.log_error(status_value, error)
            self.error_messages['display_name_errors'] = dict(message=error,
                                                              status=status_value)
            error_occurred = True

        if self.duplicate_username_check.username_exists(data['username']) and \
                player_info.get('username') != data.get('username'):
            status_value = 400
            error = "Username is already taken"
            CHERRIOS_UTILS.log_error(status_value, error)
            self.error_messages['username_errors'] = dict(message=error, status=status_value)
            error_occurred = True

        if self.duplicate_email_check.email_exists(data['email']) and \
                player_info.get('email') != data.get('email'):
            status_value = 400
            error = "Email is already taken"
            CHERRIOS_UTILS.log_error(status_value, error)
            self.error_messages['email_errors'] = dict(message=error, status=status_value)
            error_occurred = True
        if error_occurred:
            return Response(self.error_messages, 400)

        player_cursor = self.get_cursor(MSSQLPLAYERPLUSDB, True)
        self.update_member_contact(data, player_cursor)

        # commit is required because two stored procedure are using same table for update.
        self.connection_commit()
        if data.get('display_name'):
            self.update_display_name(data, user_cursor)

        self.update_entity_namespace(data, player_info, user_cursor)

        if data.get('password'):
            self.update_namespace_password(data, user_cursor)

        updated_data = self.login_viewset_interface \
            .get_player_profile(data.get('player_id'), user_cursor)
        return Response(updated_data, status=200)

    def update_member_contact(self, data, cursor):
        """
        Updates the member contact database
        :return:
        """

        parameters = "@PlayerID={0}, @FirstName='{1}', @LastName='{2}', @Address1='{3}', " \
                     "@Address2='{4}', @City='{5}', @State='{6}', @PostalCode='{7}'," \
                     "@PhoneNo='{8}', @MobilePhoneNo='{9}', @Email='{10}', " \
                     "@SiteID='{11}'".format(data.get('player_id'), data.get('first_name'),
                                             data.get('last_name'),
                                             data.get('address1'),
                                             data.get('address2'),
                                             data.get('city'),
                                             data.get('state'),
                                             data.get('postal_code'),
                                             data.get('phone_no'),
                                             data.get('mobile_phone_no'),
                                             data.get('email'),
                                             data.get('site_id'))

        store_procedure = STORED_PROCEDURE_MAPPER['update_member_contact']
        self.call_storeprocedure(store_procedure, cursor, parameters)

    def update_display_name(self, data, cursor):
        """
        Updates the display name of the player
        :return:
        """

        display_name_uppercase = None

        if data.get('display_name'):
            display_name_uppercase = data.get('display_name').upper()

        parameters = "@EntityID={0}, @DisplayName='{1}'".format(data.get('player_id'),
                                                                display_name_uppercase)

        store_procedure = STORED_PROCEDURE_MAPPER['update_display_name']
        self.call_storeprocedure(store_procedure, cursor, parameters)

    def update_entity_namespace(self, data, player_info, cursor):
        """
        Updates the username of the user
        :return:
        """

        namespace_id = 1

        username_upper_case = None

        if data.get('username'):
            username_upper_case = data.get('username').upper()

        self.password_and_salt = self.get_password_and_salt(player_info.get('username'), cursor)

        parameters = "@EntityID={0}, @NamespaceID={1}, @NamespaceIdentity='{2}'," \
                     "@Salt='{3}', @Password='{4}'".format(data.get('player_id'), namespace_id,
                                                           username_upper_case,
                                                           self.password_and_salt.get('Salt'),
                                                           self.password_and_salt.get('Password')
                                                           )

        store_procedure = STORED_PROCEDURE_MAPPER['update_entity_namespace']
        self.call_storeprocedure(store_procedure, cursor, parameters)

    def update_namespace_password(self, data, cursor):
        """
        Updates the password of the user
        :return:
        """

        namespace_id = 1

        hashed_password = CHERRIOS_UTILS. \
            create_md5_hash(data.get('password', self.password_and_salt['Password']) +
                            self.password_and_salt['Salt'])

        parameters = "@EntityID={0}, @NamespaceID={1}," \
                     " @Password='{2}'".format(data.get('player_id'),
                                               namespace_id,
                                               hashed_password)
        store_procedure = STORED_PROCEDURE_MAPPER['update_entity_password']
        self.call_storeprocedure(store_procedure, cursor, parameters)

    def get_password_and_salt(self, username, cursor):
        """

        :param username:
        :return:
        """
        parameters = "@Username='{0}', @NamespaceID={1}".format(username,
                                                                ENTITY_TYPE.get('RegisteredPlayer'))
        store_procedure = STORED_PROCEDURE_MAPPER['authentication']

        self.call_storeprocedure(store_procedure, cursor, parameters)
        result = self.fetchall_from_cursor(cursor)

        if result:
            result = result[0]

        return result
