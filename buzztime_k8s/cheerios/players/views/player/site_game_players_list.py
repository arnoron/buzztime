"""
Module which holds the SiteGamePlayersList class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from .serializer import GetSiteGamePlayersSerializer


class SiteGamePlayersList(PlayersViewSet):
    """
    Returns the site game players list response
    """

    def get_site_game_players(self, site_id, game_id):
        """
        Returns the site game players for a site and a game
        :param site_id:
        :param player_id:
        :return:
        """
        serializer = GetSiteGamePlayersSerializer(data=dict(site_id=site_id, game_id=game_id))
        if serializer.is_valid():
            request_data = dict(SiteID=site_id,
                                GameNameID=game_id)

            stored_procedure = STORED_PROCEDURE_MAPPER['site_game_players']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, "Invalid parameters passed")
