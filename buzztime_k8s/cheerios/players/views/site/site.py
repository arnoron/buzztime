"""
Module which holds the Site APIs
"""
from rest_framework.response import Response

from cheerios.players.views.competition.competition_details import CompetitionDetails
from cheerios.players.views.competition.competition_list import CompetitionList
from cheerios.players.views.game.site_game_list import SiteGameList
from cheerios.players.views.game_instance.site_game_instance import SiteGameInstance
from cheerios.players.views.player.site_game_instance_players import SiteGameInstancePlayers
from cheerios.players.views.player.site_game_players_list import SiteGamePlayersList
from cheerios.players.views.player.site_player_list import SitePlayerList
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.site.site_details import SiteDetails
from cheerios.players.views.site.site_search import SiteSearch
from cheerios.players.views.site.top_trivia_location import TopTriviaLocation
from cheerios.players.decorators import get_store_procedure_api


class SiteViewSet(PlayersViewSet):
    """
    Returns the site response
    """
    @get_store_procedure_api
    def get(self, request, site_id, site_info_subtype, site_info_games_subtype,
            site_info_subtype_id):
        """
        Returns the site response
        :return:
        """

        request_params = request.GET

        if not site_id:
            response = self.handle_search_and_sort(request_params)

        else:
            if site_info_subtype:
                response = self.handle_site_info_subtype(request_params,
                                                         site_id,
                                                         site_info_subtype,
                                                         site_info_subtype_id,
                                                         site_info_games_subtype
                                                        )

            else:
                site_interface = SiteDetails()
                response = site_interface.get_site_details(site_id)

        return response

    def handle_search_and_sort(self, request_params):
        """
        Returns the site response when search or sort params are passed
        :param request_params:
        :return:
        """
        if request_params.get('search'):
            response = self.handle_site_search(request_params)

        elif request_params.get('sort'):
            response = self.handle_site_sort(request_params)

        else:
            return UTIL_FUNCTION.log_error(400, "Invalid query parameter passed")

        return response

    @staticmethod
    def handle_site_search(request_params):
        """
        Returns the site response when search param is passed
        :return:
        """
        site_interface = SiteSearch()
        latitude = request_params.get('latitude')
        longitude = request_params.get('longitude')
        search_string = request_params.get('search')
        miles = request_params.get('miles')
        response = site_interface.get_site_search_response(latitude, longitude, search_string, miles)
        return response

    def handle_site_sort(self, request_params):
        """
        Returns the site response when sort param is passed
        :param request_params:
        :return:
        """
        sort_param = request_params.get('sort')

        if sort_param == 'num-gold-desc':
            response = self.handle_num_gold_sort(request_params)

        else:
            UTIL_FUNCTION.log_error(400, "Invalid Sorting Criteria passed {0}".format(sort_param))
            return Response("Invalid Sorting Criteria passed", status=400)

        return response

    @staticmethod
    def handle_num_gold_sort(request_params):
        """
        Returns the site response when num_gold sort param is passed
        :param request_params:
        :return:
        """
        site_interface = TopTriviaLocation()

        state = request_params.get('state')
        site_id = request_params.get('site', None)
        response = site_interface. \
            get_top_sites_response(state, site_id)
        return response


    def handle_site_info_subtype(self, request_params,
                                 site_id, site_info_subtype,
                                 site_info_subtype_id,
                                 site_info_games_subtype):
        """
        Returns the site response when site info subtype is passed
        :param site_id:
        :param site_info_subtype:
        :param site_info_subtype_id:
        :return:
        """
        if site_info_subtype == 'competitions':
            response = self.handle_site_competitions(site_id, site_info_subtype_id)

        elif site_info_subtype == 'games':
            response = self.handle_site_games(request_params, site_id, site_info_subtype_id,
                                              site_info_games_subtype)

        elif site_info_subtype == 'players':
            response = self.handle_site_players(site_id)

        return response

    @staticmethod
    def handle_site_competitions(site_id, site_info_subtype_id):
        """
        Returns the site response when competitions subtype is passed
        :param request_params:
        :param site_id:
        :param site_info_Subtype_id:
        :return:
        """
        if site_info_subtype_id:
            competition_detail_interface = CompetitionDetails()
            response = competition_detail_interface.get_competition_details(site_info_subtype_id)

        else:
            competition_list_interface = CompetitionList()
            response = competition_list_interface.get_competition_list(site_id)

        return response

    def handle_site_games(self, request_params,
                          site_id, site_info_subtype_id,
                          site_info_games_subtype):
        """
        Returns the site response when games subtype is passed
        :param request_params:
        :param site_id:
        :param site_info_Subtype_id:
        :return:
        """
        if site_info_subtype_id:

            if site_info_games_subtype == 'instances':
                game_instance_interface = SiteGameInstance()
                response = game_instance_interface.\
                    get_site_game_instance(site_id, site_info_subtype_id)

            elif site_info_games_subtype == 'players':

                response = self.handle_games_players(request_params, site_id, site_info_subtype_id)

            else:
                return UTIL_FUNCTION.log_error(400, 'Incorrect games subtype passed')

        else:
            games_interface = SiteGameList()
            response = games_interface.get_games_for_site(site_id)

        return response

    @staticmethod
    def handle_games_players(request_params, site_id, site_info_subtype_id):
        """
        Returns the site response when players subtype is passed
        :return:
        """
        if request_params.get('start-date') and request_params.get('start-time'):
            start_date = request_params.get('start-date')
            start_time = request_params.get('start-time')
            players_interface = SiteGameInstancePlayers()
            response = players_interface.\
                get_site_gameinstance_players(site_id, site_info_subtype_id,
                                              start_date, start_time)
        else:
            player_interface = SiteGamePlayersList()
            response = player_interface.get_site_game_players(site_id, site_info_subtype_id)

        return response

    @staticmethod
    def handle_site_players(site_id):
        """
        Returns the site players
        :param request_params:
        :param site_id:
        :param site_info_Subtype_id:
        :return:
        """
        player_interface = SitePlayerList()
        response = player_interface.get_players_for_site(site_id)
        return response
