"""
Module which holds the GameSites class
"""

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.services.log_classifier import Logger


class GameSites(PlayersViewSet):
    """
    Returns the game site
    """

    def get_game_sites(self, premium_game_id, site, city, state, lower_limit, upper_limit):
        """
        Returns the game sites for the passed parameters
        :return:
        """

        data = dict(LowerLimit=lower_limit,
                    UpperLimit=upper_limit,
                    PremiumGameID=premium_game_id)

        if site: data['SiteName'] = site
        if city: data['City'] = city
        if state: data['State'] = state

        stored_procedure = STORED_PROCEDURE_MAPPER['site_scores']

        result = self.execute_stored_procedure(stored_procedure, data)
        return result
