"""
Module which holds the SiteDetails class
"""
import datetime
from operator import itemgetter

from cheerios.players.constants import STORED_PROCEDURE_MAPPER, GAME_TYPE_ID_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer


class SiteDetails(PlayersViewSet):
    """
    Returns the site details response
    """

    def get_site_details(self, site_id):
        """
        Returns the site details for a site
        :return:
        """
        serializer = CheckIntegerSerializer(data=dict(integer_value=site_id))
        if serializer.is_valid():
            latitude = 0
            longitude = 0
            request_data = dict(SiteID=site_id, Latitude=latitude, Longitude=longitude)
            stored_procedure = STORED_PROCEDURE_MAPPER['site_info']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            if result.data and result.data.get('PowerPage') and \
                    not result.data.get('PowerPage').get('Description'):
                stored_procedure = STORED_PROCEDURE_MAPPER['power_page']
                result_with_powerpages = self.execute_stored_procedure(stored_procedure,
                                                                       dict(SiteID=site_id))
                if result_with_powerpages.data:
                    result.data['PowerPage'] = result_with_powerpages.data.get('PowerPage')
            return result

        else:
            return UTIL_FUNCTION.log_error(400, "Site ID is mandatory")

    def format_response_data(self, result):
        """
        Formats the response data returned by the stored procedure into a JSON readable format
        :param result: un-formatted result data in the form of tuple
        :return: formatted JSON data
        """

        list_data = {}
        site_events_list = []
        if isinstance(result, list):
            if not result:
                return result
            result = result[0]
            if not list_data:
                list_data = dict(Id=result.get('siteid'),
                                 Chain=result.get('Chain'),
                                 Phone=result.get('PriPhone'),
                                 Latitude=result.get('latitude'),
                                 Longitude=result.get('longitude'),
                                 Distance=result.get('distance'),
                                 ReviewCount=result.get('ReviewCount'),
                                 SalesType=result.get('SalesType'),
                                 SiteName=result.get('OrgName'),
                                 Accuracy=result.get('Accuracy'),
                                 IsTabletSite=True if result.get('isTabletSite') else False,
                                 Location=dict(Id=result.get('siteid'),
                                               Name=result.get('OrgName'),
                                               Address=dict(City=dict(Name=result.get('City')),
                                                            State=dict(Name=result.get('State')),
                                                            ZipCode=dict(
                                                                ZipCodeValue=result.get('Zip')),
                                                            StreetAddress1=result.get('Address1')
                                                            )
                                               ),
                                 PowerPage=dict(PowerPageID=result.get('PowerPageID'),
                                                Description=result.get('PowerPageDescription') if \
                                                    result.get('PowerPageDescription') \
                                                    else result.get('Description'),
                                                URL=result.get('PowerPageURL') if \
                                                    result.get('PowerPageURL') \
                                                    else result.get('URL'),
                                                Logo=result.get('PowerPageLogo') if \
                                                    result.get('PowerPageLogo') \
                                                    else result.get('Logo'),
                                                PowerPage_First_Image= \
                                                    result.get('PowerPageImage1') \
                                                        if result.get('PowerPageImage1') \
                                                        else result.get('Image1'),
                                                PowerPage_Second_Image= \
                                                    result.get('PowerPageImage2') \
                                                        if result.get('PowerPageImage2') \
                                                        else result.get('Image2'),
                                                PowerPage_Third_Image= \
                                                    result.get('PowerPageImage3') \
                                                        if result.get('PowerPageImage3') \
                                                        else result.get('Image3'),
                                                URLFacebook=result.get('PowerPageURLFacebook') \
                                                    if result.get('PowerPageURLFacebook') \
                                                    else result.get('URLFacebook'),
                                                URLMyspace=result.get('PowerPageURLMyspace') \
                                                    if result.get('PowerPageURLMyspace') \
                                                    else result.get('URLMyspace'),
                                                URLTwitter=result.get('PowerPageURLTwitter') \
                                                    if result.get('PowerPageURLTwitter') \
                                                    else result.get('URLTwitter'),
                                                Opt21AndOver=result.get("Opt21AndOver"),
                                                OptFreeWifi=result.get("OptFreeWifi"),
                                                OptKidFriendly=result.get("OptKidFriendly"),
                                                OptLiveMusic=result.get("OptLiveMusic"),
                                                OptFullBar=result.get("OptFullBar"),
                                                OptFullMenu=result.get("OptFullMenu")
                                                ),
                                 )
                site_events = dict(EventDayId=result.get('EventDayID'),
                                   EventEndTime=result.get('EventEndTime'),
                                   EventId=result.get('EventID'),
                                   EventStartTime=result.get('EventStartTime'),
                                   EventStatusId=result.get('EventStatusID'),
                                   EventType=self.get_event_type(result.get('EventTypeID')),
                                   EventTypeId=result.get('EventTypeID'),
                                   SiteId=result.get('siteid')
                                   )

                site_events_list.append(site_events)

        list_data['SiteEvents'] = {}
        list_data['SiteEvents']['SiteEvent'] = site_events_list
        return list_data

    @staticmethod
    def get_event_type(event_type_id):
        """
    Function will take event_type_id and return the event type
    if found else return Special Event
    :param event_type_id:
    :return:
    """
        switcher = GAME_TYPE_ID_MAPPER
        return switcher.get(event_type_id, 'Special Event')

    def post_process_data(self, data):
        """
        Process data after getting the response
        :param data: response data
        :return: Processed data
        """
        if not data:
            return data
        now = datetime.datetime.now()
        today_day = int(now.strftime("%w"))
        site_events_temp = data['SiteEvents']['SiteEvent']
        for event in site_events_temp:
            if event['EventDayId'] is not None and event['EventDayId'] < today_day:
                event['EventDayId'] += 7
        site_events_temp = sorted(site_events_temp, key=itemgetter('EventDayId', 'EventStartTime'))
        for event in site_events_temp:
            if event['EventDayId'] is not None and event['EventDayId'] >= 7:
                event['EventDayId'] -= 7
        data['SiteEvents']['SiteEvent'] = site_events_temp
        return data
