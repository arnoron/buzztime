from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.serializers import SiteSearchByLocationSerializer
from cheerios.players.views.players import PlayersViewSet
from cheerios.utils import CheeriosUtils

cheerios_utils = CheeriosUtils()


class Site(PlayersViewSet):
    """

    """

    @get_store_procedure_api
    def get(self, request):
        """

        :param request:
        :return:
        """

        start_row = 1
        number_of_rows = 100

        query_params = request.GET

        serializer = SiteSearchByLocationSerializer(data=query_params)
        cheerios_utils.validate_serializer(serializer)

        stored_procedure = STORED_PROCEDURE_MAPPER['location_search_site_near']

        response = self.execute_stored_procedure(store_procedure=stored_procedure,
                                                 data=dict(Latitude=query_params.get('latitude'),
                                                           Longitude=query_params.get('longitude'),
                                                           StartRowNumber=start_row,
                                                           NumberOfResults=number_of_rows))

        return response

    def format_response_data(self, sites):
        """

        :return:
        """

        miles = 20

        sites_under_miles = list(filter(lambda site: site.get('distance') <= miles, sites))
        return sites_under_miles

    def post_process_data(self, data):
        """

        :return:
        """
        list_data_geosite = []
        if isinstance(data, list):
            for result_obj in data:
                list_data_geosite.append(
                    dict(Id=result_obj.get('siteid'),
                         Chain=result_obj.get('Chain'),
                         Phone=result_obj.get('PriPhone'),
                         Latitude=result_obj.get('latitude'),
                         Longitude=result_obj.get('longitude'),
                         Distance=result_obj.get('distance'),
                         ReviewCount=result_obj.get('ReviewCount'),
                         RowNum=result_obj.get('rownum'),
                         TabletSite=True if result_obj.get('isTabletSite') == 1 else False,
                         Location=dict(Id=result_obj.get('siteid'),
                                       Name=result_obj.get('OrgName'),
                                       Address=dict(City=dict(Name=result_obj.get('City')),
                                                    State=dict(Name=result_obj.get('State')),
                                                    Zipcode=dict(ZipCodeVAlue=result_obj.
                                                                 get('Zip')),
                                                    Country=dict(Name=result_obj.
                                                                 get('Country')),
                                                    StreetAddress1=result_obj.get('Address1'),
                                                    StreetAddress2=result_obj.get('Address2'),
                                                    )
                                       ),
                         PowerPage=dict(Description=result_obj.get('Description'),
                                        Logo=result_obj.get('Logo'),
                                        Opt21AndOver=True if result_obj.get('Opt21AndOver') == 1
                                        else False,
                                        OptFreeWifi=True if result_obj.get('OptFreeWifi') == 1
                                        else False,
                                        OptLiveMusic=True if result_obj.get('OptLiveMusic') == 1
                                        else False,
                                        OptFullMenu=True if result_obj.get('OptFullMenu') == 1
                                        else False,
                                        OptFullBar=True if result_obj.get('OptFullBar') == 1
                                        else False,
                                        OptKidFriendly=True if result_obj
                                        .get('OptKidFriendly') == 1
                                        else False,
                                        URL=result_obj.get('URL'),
                                        PowerPageID=result_obj.get('PowerPageID'),
                                        Image1=result_obj.get('Image1'),
                                        Image2=result_obj.get('Image2'),
                                        Image3=result_obj.get('Image3')
                                        )
                         )
                )

        return list_data_geosite
