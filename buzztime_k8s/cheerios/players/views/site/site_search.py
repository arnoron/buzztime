"""
Module which holds the SiteSearch class
"""
from pymssql import OperationalError

from rest_framework.response import Response
from sqlalchemy import exc

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.players.decorators import close_connection
from cheerios.services.log_classifier import Logger


class SiteSearch(PlayersViewSet):
    """
    Returns the site search response
    """
    def get_site_search_response(self, latitude_data, longitude_data, search_string, miles):
        """
        Returns the site search based on passed latitude, longtiude and search string
        :return:
        """
        database_name = MSSQLDBGAMEPLAY

        start_row_number = '1'
        number_of_rows = '100'

        if not search_string:
            return UTIL_FUNCTION.log_error(400, "search is a mandatory field")

        data = [latitude_data, longitude_data]
        if latitude_data and longitude_data:
            if UTIL_FUNCTION.is_float(data):
                latitude = latitude_data
                longitude = longitude_data
            else:
                error_result = "Bad Request: Latitude and Longitude should be float"
                message = "Error status code 400"
                Logger.log.error(message)
                return UTIL_FUNCTION.log_error(400, error_result)

        if miles is not None:
            if not UTIL_FUNCTION.is_positive_integer(miles):
                error_result = "Bad Request: Miles can only be positive integer."
                message = "Error status code 400"
                Logger.log.error(message)
                return UTIL_FUNCTION.log_error(400, error_result)
        search_string.replace(' ', ',')

        flag_geosite = True
        if not latitude_data or not longitude_data:
            flag_geosite = False

        data_sitename = (
            search_string
        )
        store_procedure_sitename = STORED_PROCEDURE_MAPPER['location_search_site_name']

        databases = dict(sitename=database_name)

        if flag_geosite:
            data_geosite = dict(Latitude=latitude,
                                Longitude=longitude,
                                StartRowNumber=start_row_number,
                                NumberOfResults=number_of_rows
                               )
            store_procedure_geosite = STORED_PROCEDURE_MAPPER['location_search_site_near']
            store_procedures = dict(geosite=store_procedure_geosite,
                                    sitename=store_procedure_sitename)
            request_data = dict(geosite=data_geosite, sitename=data_sitename)
        else:
            store_procedures = dict(sitename=store_procedure_sitename)
            request_data = dict(sitename=data_sitename)
        result = self.get_response(store_procedures, request_data, databases, miles)
        message = "Location Search Results {0}".format(result)
        Logger.log.info(message)
        return result

    @close_connection
    def get_response(self, store_procedure_args, data, databases, miles=''):
        """
        Calls the stored procedures and returns the response
        :param store_procedure_args: Names of the stored procedures to be called
        :param data: The data to be bound to the Stored Procedure call
        :param databases: database to query to
        ":param miles: for filtering out results based on miles passed as arguement
        :return: Tuple Response from the stored procedure
        """
        try:
            store_procedure_sitename = store_procedure_args.get('sitename')
            cursor = self.get_cursor(databases.get('sitename'))
            data_site = dict(PhraseList=data.get('sitename').replace('\'', '\'\''))
            data_string_site = self.get_data_string(data_site)
            self.call_storeprocedure(store_procedure_sitename,
                                     cursor, data_string_site)
            result_sitename = self.fetchall_from_cursor(self.cursor)
            filtered_result_sitename = list(filter(lambda locations: locations.get('distance')<=int(miles), result_sitename)) if miles else [];
            if store_procedure_args.get('geosite'):
                store_procedure_geosite = store_procedure_args.get('geosite')
                data_string_geo = self.get_data_string(data.get('geosite'))
                self.call_storeprocedure(store_procedure_geosite,
                                         cursor, data_string_geo)
                result_geosite = self.fetchall_from_cursor(self.cursor)
                filtered_result_geosite = list(filter(lambda locations: locations.get('distance')<=int(miles), result_geosite)) if miles else[];
                result = dict(geosite=filtered_result_geosite, sitename=filtered_result_sitename) if miles else dict(geosite=result_geosite, sitename=result_sitename)
            else:
                result = dict(sitename=filtered_result_sitename) if miles else dict(sitename=result_sitename)
            list_data = self.format_response_data(result)
            response = Response(list_data, status=200)
            return response
        except ValueError as inst:
            return UTIL_FUNCTION.value_error(inst)
        except OperationalError as inst:
            return UTIL_FUNCTION.operation_error(inst)
        except exc.DBAPIError as excep:
            return UTIL_FUNCTION.dbapi_error(excep)
        except Exception as inst:
            return UTIL_FUNCTION.exception(inst)
        finally:
            self.close_cursor()

    def format_response_data(self, result):
        """
        Formats the response data returned by the stored procedure into a JSON readable format
        :param result: un-formatted result data in the form of tuple
        :return: formatted JSON data
        """
        result_sitename = result.get('sitename')
        list_data_sitename = []
        if isinstance(result_sitename, list):
            for result_obj in result_sitename:
                list_data_sitename.append(
                    dict(Id=result_obj.get('siteid'),
                         Chain=result_obj.get('Chain'),
                         Phone=result_obj.get('PriPhone'),
                         Latitude=result_obj.get('latitude'),
                         Longitude=result_obj.get('longitude'),
                         Distance=result_obj.get('distance'),
                         ReviewCount=result_obj.get('ReviewCount'),
                         RowNum=result_obj.get('rownum'),
                         TabletSite=True if result_obj.get('isTabletSite') == 1 else False,
                         Location=dict(Id=result_obj.get('siteid'),
                                       Name=result_obj.get('OrgName'),
                                       Address=dict(City=dict(Name=result_obj.get('City')),
                                                    State=dict(Name=result_obj.get('State')),
                                                    Zipcode=dict(ZipCodeVAlue=result_obj.
                                                                 get('Zip')),
                                                    Country=dict(Name=result_obj.get('Country')),
                                                    StreetAddress1=result_obj.get('Address1'),
                                                    StreetAddress2=result_obj.get('Address2'),
                                                   )
                                      ),
                         PowerPage=dict(Description=result_obj.get('Description'),
                                        Logo=result_obj.get('Logo'),
                                        Opt21AndOver=True if result_obj.get('Opt21AndOver') == 1
                                        else False,
                                        OptFreeWifi=True if result_obj.get('OptFreeWifi') == 1
                                        else False,
                                        OptLiveMusic=True if result_obj.get('OptLiveMusic') == 1
                                        else False,
                                        OptFullMenu=True if result_obj.get('OptFullMenu') == 1
                                        else False,
                                        OptFullBar=True if result_obj.get('OptFullBar') == 1
                                        else False,
                                        OptKidFriendly=True if result_obj.get('OptKidFriendly') == 1
                                        else False,
                                        URL=result_obj.get('URL'),
                                        PowerPageID=result_obj.get('PowerPageID'),
                                        Image1=result_obj.get('Image1'),
                                        Image2=result_obj.get('Image2'),
                                        Image3=result_obj.get('Image3')
                                       )
                        )
                    )
        list_data_sitename_data = dict(SiteGeoCode=list_data_sitename)
        if result.get('geosite'):
            result_geosite = result.get('geosite')
            list_data_geosite = []
            if isinstance(result_geosite, list):
                for result_obj in result_geosite:
                    list_data_geosite.append(
                        dict(Id=result_obj.get('siteid'),
                             Chain=result_obj.get('Chain'),
                             Phone=result_obj.get('PriPhone'),
                             Latitude=result_obj.get('latitude'),
                             Longitude=result_obj.get('longitude'),
                             Distance=result_obj.get('distance'),
                             ReviewCount=result_obj.get('ReviewCount'),
                             RowNum=result_obj.get('rownum'),
                             TabletSite=True if result_obj.get('isTabletSite') == 1 else False,
                             Location=dict(Id=result_obj.get('siteid'),
                                           Name=result_obj.get('OrgName'),
                                           Address=dict(City=dict(Name=result_obj.get('City')),
                                                        State=dict(Name=result_obj.get('State')),
                                                        Zipcode=dict(ZipCodeVAlue=result_obj.
                                                                     get('Zip')),
                                                        Country=dict(Name=result_obj.
                                                                     get('Country')),
                                                        StreetAddress1=result_obj.get('Address1'),
                                                        StreetAddress2=result_obj.get('Address2'),
                                                       )
                                          ),
                             PowerPage=dict(Description=result_obj.get('Description'),
                                            Logo=result_obj.get('Logo'),
                                            Opt21AndOver=True if result_obj.get('Opt21AndOver') == 1
                                            else False,
                                            OptFreeWifi=True if result_obj.get('OptFreeWifi') == 1
                                            else False,
                                            OptLiveMusic=True if result_obj.get('OptLiveMusic') == 1
                                            else False,
                                            OptFullMenu=True if result_obj.get('OptFullMenu') == 1
                                            else False,
                                            OptFullBar=True if result_obj.get('OptFullBar') == 1
                                            else False,
                                            OptKidFriendly=True if result_obj
                                            .get('OptKidFriendly') == 1
                                            else False,
                                            URL=result_obj.get('URL'),
                                            PowerPageID=result_obj.get('PowerPageID'),
                                            Image1=result_obj.get('Image1'),
                                            Image2=result_obj.get('Image2'),
                                            Image3=result_obj.get('Image3')
                                           )
                            )
                        )
            list_data_sitelist = dict(SiteGeoCode=list_data_geosite)
            list_data = dict(SiteListName=list_data_sitename_data,
                             SiteListNear=list_data_sitelist)
        else:
            list_data = dict(SiteListName=list_data_sitename_data)

        return list_data
