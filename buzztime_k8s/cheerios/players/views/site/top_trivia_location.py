"""
Module which holds the TopTriviaLocation class
"""
from rest_framework.response import Response

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.services.log_classifier import Logger


class TopTriviaLocation(PlayersViewSet):
    """
    Returns the response for the top trivia locations
    """

    def get_top_sites_response(self, state, site_id):
        """
        Returns the top sites for a passed state
        :param state:
        :return:
        """

        result_site = self.get_site_results(state, None).__dict__.get('data')
        result = result_site

        if site_id:
            result = {}
            result_user = self.get_site_results(state, site_id).__dict__.get('data')
            result.update({"MySiteInfo": result_user})
            result.update({"Sites": result_site})

        return Response(result, 200)

    def get_site_results(self, state, site_id):
        """
        Returns top site results
        :return:
        """

        data = dict()

        if state: data['State'] = state
        if site_id: data['SiteID'] = site_id
        stored_procedure = STORED_PROCEDURE_MAPPER['top_trivia_sites']
        result = self.execute_stored_procedure(stored_procedure, data)
        return result
