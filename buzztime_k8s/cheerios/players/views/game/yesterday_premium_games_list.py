"""
Module which holds the YesterdayPremiumGamesList class
"""
from pymssql import DatabaseError

from rest_framework.response import Response

from cheerios.players.constants import STORED_PROCEDURE_MAPPER, PREMIUMGAMEID_QB1
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.services.log_classifier import Logger


class YesterdayPremiumGamesList(PlayersViewSet):
    """
    Returns the yesterday premium games response
    """

    def get_yesterday_premium_games(self):
        """
        Returns the premium games info hosted a day before
        :return:
        """
        try:
            message = "Yesterday Game Details api called"
            Logger.log.info(message)
            yesterday_games_list = self.get_yesterday_games()
            for game in yesterday_games_list:
                game["game_winner"] = self.get_game_winner_data(game.get("GameID"),
                                                                game.get("PremiumGameID")
                                                               )
                game["game_winner_site"] = self.get_game_winner_site_data(game.get("GameID"),
                                                                          game.get("PremiumGameID")
                                                                         )
            return Response(yesterday_games_list, status=200)
        except ValueError as inst:
            raise ValueError(inst)
        except DatabaseError as inst:
            raise ValueError(inst)
        except Exception as inst:
            message = "Error while fetching data for Yesterday games details Error {0}".format(inst)
            Logger.log.error(message)
            return UTIL_FUNCTION.exception(inst)

    def get_yesterday_games(self):
        """
        Calls the stored procedure
        Gets the API data and returns the response
        :return: Http Response Object
        """

        data = dict()
        stored_procedure = STORED_PROCEDURE_MAPPER['yesterday_games']
        result = self.execute_stored_procedure(stored_procedure, data)
        return result.data

    def get_game_winner_data(self, game_id, premium_game_id):
        """
        Calls the stored procedure
        Gets the API data and returns the response
        :param game_id: game id
        :param premium_game_id" premium game id
        :return: Http Response Object
        """

        data = dict(PremiumGameID=premium_game_id,
                    GameID=game_id)

        stored_procedure = STORED_PROCEDURE_MAPPER['game_winners']
        result = self.execute_stored_procedure(stored_procedure, data)
        message = "Yesterday's Games Winners: {0}".format(result)
        Logger.log.info(message)
        return result.data

    def get_game_winner_site_data(self, game_id, premium_game_id):
        """
        Calls the stored procedure
        Gets the API data and returns the response
        :param game_id: game id
        :param premium_game_id" premium game id
        :return: Http Response Object
        """

        if game_id != PREMIUMGAMEID_QB1:
            game_id = 0

        data = dict(PremiumGameID=premium_game_id,
                    GameID=game_id)

        stored_procedure = STORED_PROCEDURE_MAPPER['game_winners_site']
        result = self.execute_stored_procedure(stored_procedure, data)
        return result.data
