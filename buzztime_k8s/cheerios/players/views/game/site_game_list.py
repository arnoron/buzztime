"""
Module which holds the SiteGameList class
"""
from rest_framework.response import Response
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.views.serializer import CheckIntegerSerializer
from cheerios.services.log_classifier import Logger


class SiteGameList(PlayersViewSet):
    """
    Returns the site game response
    """

    def get_games_for_site(self, site_id):
        """
        Returns the games for a site
        :param site_id:
        :return:
        """
        request_data = dict(SiteID=site_id)
        serializer = CheckIntegerSerializer(data=dict(integer_value=site_id))

        if serializer.is_valid():
            stored_procedure = STORED_PROCEDURE_MAPPER['site_games']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            error = {
                "Error": "Empty parameters passed",
                "ErrorCode": 400
            }
            result = "Serializer Error: {0}".format(serializer.errors)
            message = "Error status code 400, {0}".format(result)
            Logger.log.error(message)
            return Response(error, status=400)