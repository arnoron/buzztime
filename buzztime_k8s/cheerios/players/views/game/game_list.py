"""
Module which holds the GameList class
"""

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet

class GameList(PlayersViewSet):
    """
    Returns the game list response
    """

    def get_game_list(self):
        """
        Returns the game list response
        :return:
        """

        data = dict(GameTypeID=1, GameStatusID=1)
        stored_procedure = STORED_PROCEDURE_MAPPER['game_forum']
        result = self.execute_stored_procedure(stored_procedure, data)
        return result
