"""
Module which holds the Games API
"""
from cheerios.players.views.game.game_list import GameList
from cheerios.players.views.game.yesterday_premium_games_list import YesterdayPremiumGamesList
from cheerios.players.views.player.game_players import GamePlayers
from cheerios.players.views.player.game_site_players import GameSitePlayers
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.views.site.game_sites import GameSites
from cheerios.players.views.game import premium_games_list

class GameViewSet(PlayersViewSet):
    """
    Returns the game info
    """
    @get_store_procedure_api
    def get(self, request, game_id, game_info_subtype, game_info_subtype_id):
        """
        Returns the game info
        :param game_id:
        :param game_info_subtype:
        :param game_info_subtype_id:
        :return:
        """
        request_params = request.GET

        if not game_id:
            if request_params.get('type'):
                response = self.handle_game_type(request_params)

            else:
                game_interface = GameList()
                response = game_interface.get_game_list()

        else:
            response = self.handle_game_info_subtype(request_params, game_id, game_info_subtype,
                                                     game_info_subtype_id)

        return response

    @staticmethod
    def handle_game_type(request_params):
        """
        Returns the game info response when game type is passed
        :return:
        """
        if request_params.get('day') and request_params.get('day') == 'yesterday':
            yesterday_game_interface = YesterdayPremiumGamesList()
            response = yesterday_game_interface.get_yesterday_premium_games()

        else:
            premium_game_interface = premium_games_list.PremiumGamesList()
            response = premium_game_interface.get_premium_games()

        return response

    @staticmethod
    def handle_game_info_subtype(request_params, game_id, game_info_subtype,
                                 game_info_subtype_id):
        """
        Returns the game info response when the game info subtype is passed
        :param request_params:
        :param game_id:
        :param game_info_subtype:
        :param game_info_subtype_id:
        :return:
        """

        if game_info_subtype == 'sites':
            if game_info_subtype_id:
                game_site_player_interface = GameSitePlayers()
                response = game_site_player_interface.\
                    get_game_site_players(game_id, game_info_subtype_id)

            else:
                site = request_params.get('site', '') if request_params.get('site', '') else ""
                city = request_params.get('city', '') if request_params.get('city', '') else ""
                state = request_params.get('state', '') \
                    if request_params.get('state', '') else ""
                lower_limit = request_params.get('lower-limit', 1)
                upper_limit = request_params.get('upper-limit', 200)

                site_interface = GameSites()
                response = site_interface.get_game_sites(game_id,
                                                         site,
                                                         city,
                                                         state,
                                                         lower_limit,
                                                         upper_limit
                                                        )

        elif game_info_subtype == 'players':
            handle = request_params.get('handle', '')
            site = request_params.get('site', '')
            city = request_params.get('city', '')
            state = request_params.get('state', '')
            lower_limit = request_params.get('lower-limit', '')
            upper_limit = request_params.get('upper-limit', '')

            game_player_interface = GamePlayers()
            response = game_player_interface.get_game_players(game_id,
                                                              handle,
                                                              site,
                                                              city,
                                                              state,
                                                              lower_limit,
                                                              upper_limit
                                                             )
        else:
            response = UTIL_FUNCTION.log_error(400, "Invalid game info subtype")

        return response
