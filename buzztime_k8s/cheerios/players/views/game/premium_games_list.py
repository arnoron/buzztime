"""
Module which holds the PremiumGamesList class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet


class PremiumGamesList(PlayersViewSet):
    """
    Returns the premium games list response
    """

    def get_premium_games(self):
        """
        Returns the premium games list response
        :return:
        """

        data = dict()
        stored_procedure = STORED_PROCEDURE_MAPPER['trivia_games']
        result = self.execute_stored_procedure(stored_procedure, data)
        return result
