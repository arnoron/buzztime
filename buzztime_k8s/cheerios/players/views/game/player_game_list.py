"""
Module which holds the PlayerGameList class
"""

from rest_framework.response import Response

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.views.serializer import CheckIntegerSerializer
from cheerios.services.log_classifier import Logger


class PlayerGameList(PlayersViewSet):
    """
    Returns the player games response
    """
    def get_game_list(self, player_id):
        """
        Returns the game list for a player
        :param player_id:
        :return:
        """
        serializer = CheckIntegerSerializer(data=dict(integer_value=player_id))
        if serializer.is_valid():
            game_derivative_id = 0

            request_data = dict(PlayerID=player_id,
                                GameSubTypeDerivativeID=game_derivative_id)
            stored_procedure = STORED_PROCEDURE_MAPPER['game_type_averages']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            error = {
                "Error": "Empty parameters passed",
                "ErrorCode": 400
            }
            result = "Serializer Error: {0}".format(serializer.errors)
            message = "Error status code 400, {0}".format(result)
            Logger.log.error(message)
            return Response(error, status=400)