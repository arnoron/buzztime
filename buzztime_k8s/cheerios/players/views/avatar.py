"""
Module for getting and setting avatar
"""
from datetime import datetime

import zlib

import os
import json
from django.http import HttpResponse
from rest_framework.response import Response

from PIL import Image

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.views.players import UTIL_FUNCTION
from cheerios.services.is_authenticated import is_authenticated
from cheerios.settings import MSSQLPLAYERPLUSDB, IMAGE_SERVER_DIR, PNG_IMAGE_SERVER_DIR
from cheerios.players.serializers import AvatarSerializer, DeleteAvatarSerializer
from cheerios.utils import CheeriosUtils

CHERRIOS_UTILS = CheeriosUtils()

IMAGE_SERVER_PATH = IMAGE_SERVER_DIR
PNG_IMAGE_SERVER_PATH = PNG_IMAGE_SERVER_DIR

DEFAULT_COLOR = 230


class AvatarViewSet(MSSQLApiView):
    """
    API viewset for Avatars
    """

    def __init__(self):
        """
        Constructor initializes the global vars
        """
        super().__init__()
        self.hex_color_code_len = 4
        self.image_width = 200
        self.image_height = 200

    @is_authenticated()
    @get_store_procedure_api
    def get(self, request):
        """
        Gets the Avatar Info of a player
        :return:
        """

        parameters = "@PlayerID={0}".format(
            request.GET.get('player_id', request.COOKIES.get('player_id')))

        cursor = self.get_cursor(MSSQLPLAYERPLUSDB)
        store_procedure = STORED_PROCEDURE_MAPPER['get_avatar']

        self.call_storeprocedure(store_procedure, cursor, parameters)
        result = self.fetchall_from_cursor(cursor)

        if not result:
            result = ''
        else:
            result = result[0]['AvatarXML']

        return HttpResponse(result, content_type="application/json")

    @is_authenticated()
    @get_store_procedure_api
    def post(self, request):
        """
        Saves the Avatar Info of a player
        :param request:
        :return:
        """

        request_params = request.POST

        serializer = AvatarSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)

        player_id = request_params.get('player_id')
        avatar_xml = request_params.get('xml')
        image_pixels = request_params.get('pixels')
        backdrop_color = request_params.get('backdrop')

        jpg_avatar_file = "{0}.jpg".format(player_id)
        png_avatar_file = "{0}.png".format(player_id)

        try:
            self.save_avatar_image(image_pixels, jpg_avatar_file, png_avatar_file, backdrop_color)
        except KeyError:
            return UTIL_FUNCTION.log_error(400, "The output format could not be determined from the file name.")
        except Exception as ex:
            return UTIL_FUNCTION.log_error(500, str(ex))

        file_crc_32 = self.get_file_crc_32(jpg_avatar_file)

        if self.save_xml(player_id, jpg_avatar_file, file_crc_32, avatar_xml):
            return Response("Image saved successfully", 200)
        else:
            return UTIL_FUNCTION.log_error(500, "Image not saved successfully")

    def writePixels(self, pixels_code, pixels, backdrop_color=None):
        """
        Creates a PNG or JPG image
        :param type:
        :param pixels_code:
        :param pixels:
        :param backdrop_color:
        :return:
        """
        if backdrop_color is not None:
            backdrop_color = json.loads(backdrop_color)

            r = backdrop_color.get('r', DEFAULT_COLOR)
            g = backdrop_color.get('g', DEFAULT_COLOR)
            b = backdrop_color.get('b', DEFAULT_COLOR)

        code = pixels_code.split(',')

        num_pixels = len(code) / self.hex_color_code_len

        img_x_pixels = 0
        img_y_pixels = 0
        for i in range(0, int(num_pixels)):
            start_pixel = i * self.hex_color_code_len
            rgb_color_code = tuple(
                int(color) for color in code[start_pixel:start_pixel + self.hex_color_code_len])
            if rgb_color_code == (0, 0, 0, 0) and backdrop_color is not None:
                rgb_color_code = (r, g, b, 1)

            pixels[img_x_pixels, img_y_pixels] = rgb_color_code

            img_x_pixels = img_x_pixels + 1

            if img_x_pixels == self.image_width:
                img_x_pixels = 0
                img_y_pixels = img_y_pixels + 1

    def save_avatar_image(self, pixels_code, jpg_avatar_file, png_avatar_file, backdrop_color):
        """
        Save Avatar image to image server
        :param image_pixels:
        :return:
        """
        jpg_img = Image.new('RGB', (self.image_width, \
                                    self.image_height), "black")  # create a new black JPG image

        png_img = Image.new('RGBA', (self.image_width, \
                                     self.image_height), "black")  # create a new black PNG image

        jpg_pixels = jpg_img.load()  # create the pixel map
        png_pixels = png_img.load()

        self.writePixels(pixels_code, png_pixels)  # creates PNG image

        self.writePixels(pixels_code, jpg_pixels, backdrop_color)  # creates JPG image with backdrop color

        jpg_img.save(IMAGE_SERVER_PATH + jpg_avatar_file)
        png_img.save(PNG_IMAGE_SERVER_PATH + png_avatar_file)

    def get_file_crc_32(self, avatar_file):
        """
        Read and convert it ro crc_32
        :return:
        """
        with open(IMAGE_SERVER_PATH + avatar_file, 'rb') as content_file:
            content = content_file.read()
        crc_content = abs(zlib.crc32(content))

        if crc_content & 0x80000000:
            crc_content ^= 0xffffffff
            crc_content += 1

        return crc_content

    def save_xml(self, player_id, avatar_file, file_crc_32, avatar_xml):
        """
        Saves the xml for the avatar to the db
        :return:
        """
        modified_date_time = str(datetime.now()).replace(" ", "T")

        parameters = "@PlayerID={0}, @AvatarFile='{1}', @FileModified='{2}', @FileCRC32='{3}', " \
                     "@AvatarXML='{4}'".format(
            player_id, avatar_file, modified_date_time[:-3], file_crc_32,
            avatar_xml)

        cursor = self.get_cursor(MSSQLPLAYERPLUSDB)
        store_procedure = STORED_PROCEDURE_MAPPER['save_avatar']

        try:
            self.call_storeprocedure(store_procedure, cursor, parameters)
        except Exception as ex:
            return False

        return True

    @is_authenticated()
    @get_store_procedure_api
    def delete(self, request):
        """
        Deletes the Avatar Info of a player
        :param request:
        :return:
        """

        request_params = request.data

        serializer = DeleteAvatarSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)

        player_id = request_params.get('player_id')

        parameters = "@PlayerID={}".format(player_id)
        cursor = self.get_cursor(MSSQLPLAYERPLUSDB, True)
        store_procedure = STORED_PROCEDURE_MAPPER['delete_avatar']

        self.call_storeprocedure(store_procedure, cursor, parameters)

        os.remove(IMAGE_SERVER_PATH + player_id + ".jpg")

        return Response(True, status=200)
