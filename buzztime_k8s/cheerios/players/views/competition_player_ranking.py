from pymssql import OperationalError

from rest_framework import status
from rest_framework.response import Response

from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet
from cheerios.services.log_classifier import Logger
from cheerios.utils import CheeriosUtils

CHEERIOS_UTILS = CheeriosUtils()

class CompetitionPlayerRanking(PlayersViewSet):
    """

    """

    def get(self, request, competition_id):
        """

        :param request:
        :return:
        """
        request_data = dict(CompetitionID=competition_id)

        stored_procedure = STORED_PROCEDURE_MAPPER['competition_player_ranking']

        try:
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        except OperationalError as ex:
            Logger.log.error(ex.args[1].decode("utf-8"))
            return Response("Invalid parameter passed", status=status.HTTP_400_BAD_REQUEST)
