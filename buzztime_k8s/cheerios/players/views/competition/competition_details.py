"""
Module which holds the CompetitionDetails class
"""
from rest_framework.response import Response
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.players.decorators import close_connection
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer

from cheerios.services.log_classifier import Logger


class CompetitionDetails(PlayersViewSet):
    """
    Returns the competition details response
    """
    def __init__(self):
        """
        Defines Class scope constants
        """
        super().__init__()
        self.competition_id = 0

    def get_competition_details(self, competition_id):
        """
        Returns the competition details for a competition
        :return:
        """
        serializer = CheckIntegerSerializer(data=dict(integer_value=competition_id))
        if serializer.is_valid():
            self.competition_id = competition_id
            response_data = dict(participating_locations=self.get_participating_locations(),
                                 competition_leaders=self.get_competition_leaders(),
                                 competition_gameplay=self.get_competition_gameplay())
            return Response(response_data, 200)

        else:
            return UTIL_FUNCTION.log_error(400, 'Invalid Parameters passed')

    @close_connection
    def get_participating_locations(self):
        """
        Returns the participating locations for a competition
        :param site_id:
        :param competition_id:
        :return:
        """
        database_name = MSSQLDBGAMEPLAY
        request_data = dict(CompetitionID=self.competition_id)
        data_string = self.get_data_string(request_data)

        stored_procedure = STORED_PROCEDURE_MAPPER['customer_competitions']
        message = "Stored Procedure : " + stored_procedure + " Parameters {0}". \
            format(self.competition_id)
        Logger.log.info(message)

        cursor = self.get_cursor(database_name)
        self.call_storeprocedure(stored_procedure, cursor, data_string)
        result = self.fetchall_from_cursor(cursor)
        if result[0].get('CompetitionXML'):
            result = UTIL_FUNCTION.convert_xml_to_dict(result[0].get('CompetitionXML'))

        return result

    @close_connection
    def get_competition_leaders(self):
        """
        Returns the competition leaders for a competition
        :param site_id:
        :param competition_id:
        :return:
        """
        database_name = MSSQLDBGAMEPLAY

        request_data = dict(CompetitionID=self.competition_id)
        data_string = self.get_data_string(request_data)
        stored_procedure = STORED_PROCEDURE_MAPPER['site_competitions_ranking']

        cursor = self.get_cursor(database_name)
        self.call_storeprocedure(stored_procedure, cursor, data_string)
        result = self.fetchall_from_cursor(cursor)
        return result

    @close_connection
    def get_competition_gameplay(self):
        """
        Returns the competition gameplay for a competition
        :param site_id:
        :param competition_id:
        :return:
        """
        database_name = MSSQLDBGAMEPLAY

        request_data = dict(CompetitionID=self.competition_id)
        data_string = self.get_data_string(request_data)

        stored_procedure = STORED_PROCEDURE_MAPPER['competition_games']

        cursor = self.get_cursor(database_name)
        self.call_storeprocedure(stored_procedure, cursor, data_string)
        result = self.fetchall_from_cursor(cursor)
        return result
