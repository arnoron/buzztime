"""
Module which holds the Competition class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.serializers import CompetitionSerializer
from cheerios.utils import CheeriosUtils
CHERRIOS_UTILS = CheeriosUtils()


class CompetitionViewSet(PlayersViewSet):
    """
    API View Set for Competition
    """

    @get_store_procedure_api
    def get(self, request):
        """
        Get API for Competition
        :param request:
        :return:
        """
        request_params = request.GET
        promotion_id = request_params.get('promotion_id')

        serializer = CompetitionSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)

        request_data = dict(PromotionID=promotion_id)

        stored_procedure = STORED_PROCEDURE_MAPPER['site_competitions']
        result = self.execute_stored_procedure(stored_procedure, request_data)
        return result

    def post_process_data(self, result):
        """
        Function to create response from result.
        :return:
        """
        list_data = []

        if isinstance(result, list):
            for result_obj in result:
                list_data.append(
                    dict(FileName=result_obj.get('FileName'),
                         StartDate=result_obj.get('StartDate'),
                         CompetitionID=result_obj.get('CompetitionID'),
                         EndDate=result_obj.get('EndDate'),
                         Name=result_obj.get('Name')
                        )
                )

        return list_data
