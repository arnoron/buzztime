"""
Module which holds the CompetitionList class
"""
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer



class CompetitionList(PlayersViewSet):
    """
    Returns the competitions for a site
    """

    def get_competition_list(self, site_id):
        """
        Returns the competitions for a site
        :return:
        """
        serializer = CheckIntegerSerializer(data=dict(integer_value=site_id))
        if serializer.is_valid():
            hide_sequence = 1
            request_data = dict(SiteID=site_id, HideSequence=hide_sequence)
            stored_procedure = STORED_PROCEDURE_MAPPER['site_competitions']
            result = self.execute_stored_procedure(stored_procedure, request_data)
            return result
        else:
            return UTIL_FUNCTION.log_error(400, "Site ID is mandatory")

