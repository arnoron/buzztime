from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class CheckIntegerSerializer(SerializerValidator):

    integer_value = serializers.IntegerField(required=True, allow_null=False)


class CheckIntegerOptionalSerializer(SerializerValidator):

    integer_value = serializers.IntegerField(required=True, allow_null=True)


class CheckCharOptionalSerializer(SerializerValidator):

    char_value = serializers.CharField(required=True, allow_null=True)


class CheckCharSerializer(SerializerValidator):

    char_value = serializers.CharField(required=True, allow_null=False)

class AMOESerializer(SerializerValidator):
    """
    Serializer for AMOE data post
    """
    first_name = serializers.CharField(required=True, max_length=20)
    last_name = serializers.CharField(required=True, max_length=50)
    email_address = serializers.CharField(required=True, max_length=128)
    zip = serializers.CharField(required=True, max_length=10)
    favorite_trivia_game = serializers.CharField(required=True, max_length=24)
