"""
Module for converting avatar xml to json.
"""

import csv
import json
from random import randint
import untangle
from django.http import HttpResponse, JsonResponse

from cheerios.players.constants import AVATAR_JSON_DEFAULT_VALUES, AVATAR_XML_CSV_PATH, DEFAULT_LOWER_LIMIT_FOR_COORDS, \
    DEFAULT_UPPER_LIMIT_FOR_COORDS
from cheerios.players.views.avatar import AvatarViewSet
from cheerios.players.views.players import PlayersViewSet
from cheerios.utils import CheeriosUtils

CHERRIOS_UTILS = CheeriosUtils()


class AvatarConverterViewSet(PlayersViewSet):

      def __init__(self):
        """
        Constructor initializes the global vars
        """
        super().__init__()
        self.json_list = []

      def get(self, request):
    #     """
    #     Converts XML Avatar data to JSON Data.
    #     :return:
    #     """
    #     player_id = "30302649"
    #     avatar_interface = AvatarViewSet()
    #
    #     json_string = '<avatar gender="m"><features><feature selected="1" name="body"/><feature selected="0" name="chest"/><feature selected="1" name="head"/><feature selected="3" name="shirt"/><feature selected="3" name="shirtFront"/><feature selected="3" name="shirtChest"/><feature selected="3" name="hair"/><feature selected="3" name="hairFront"/><feature selected="1" name="hairBack"/><feature selected="1" name="hairMiddle"/><feature selected="0" name="hat"/><feature selected="0" name="faceHair"/><feature selected="1" name="facialHairFront"/><feature selected="0" name="facialHairBack"/><feature selected="1" name="ears"/><feature selected="1" name="earFront"/><feature selected="1" name="earBack"/><feature selected="3" name="eyes"/><feature selected="1" name="eyeBrows"/><feature selected="5" name="mouth"/><feature selected="5" name="nose"/></features><colors><color ypos="12" xpos="72" color="9921348" name="skinColor"/><color ypos="4" xpos="288" color="15132390" name="hairColor"/><color ypos="0" xpos="0" color="0" name="lipColor"/><color ypos="22" xpos="215" color="2687198" name="eyeColor"/><color ypos="24" xpos="190" color="262340" name="shirtColor"/><color ypos="4" xpos="132" color="13434596" name="backdropColor"/></colors></avatar>'
    #
    #     file_crc = '580191011'
    #     avatar_interface.save_xml(player_id, player_id + ".jpg", file_crc, json_string)

        try:
            with open(AVATAR_XML_CSV_PATH) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    obj = untangle.parse(row[1])
                    json_item = {}
                    for features in obj.avatar.features:
                        for feature in features.feature:
                            json_item[feature["name"] + "_type"] = feature["name"] + "_" + feature["selected"]

                    for colors in obj.avatar.colors:
                        for color in colors.color:
                            try:
                                if len(color["color"]) > 6:
                                    rgb_color = tuple(int(format(int(color["color"]), 'x')[i:i + 2], 16) for i in (0, 2, 4))
                                else:
                                    rgb_color = tuple(int(color["color"][i:i + 2], 16) for i in (0, 2, 4))
                                json_item[color["name"]] = {"r": rgb_color[0], "g": rgb_color[1], "b": rgb_color[2]}
                                json_item[color["name"] + "Coords"] = {"x": color["xpos"], "y": color["ypos"]}
                            except ValueError as e:
                                # print('Invalid Hex data.',e)
                                json_item[color["name"]] = {"r": 0, "g": 0, "b": 0}
                                json_item[color["name"] + "Coords"] = {
                                    "x": randint(DEFAULT_LOWER_LIMIT_FOR_COORDS, DEFAULT_UPPER_LIMIT_FOR_COORDS),
                                    "y": randint(DEFAULT_LOWER_LIMIT_FOR_COORDS, DEFAULT_UPPER_LIMIT_FOR_COORDS)}

                    if obj.avatar['gender'] == 'm':
                        json_item['gender'] = 'male'
                    else:
                        json_item['gender'] = 'female'

                    json_item.update(AVATAR_JSON_DEFAULT_VALUES)

                    json_string = json.dumps(json_item)

                    avatar_interface = AvatarViewSet()
                    player_id = row[0]
                    file_crc_32 = row[2]
                    try:
                        avatar_interface.save_xml(player_id, player_id + ".jpg", file_crc_32, json_string)
                    except Exception as ex:
                        print(str(ex))
                        return HttpResponse(str(ex), 500)

                    print("avatar saved for {}".format(player_id))
                    self.json_list.append(json.dumps(json_item))

            return JsonResponse(self.json_list, safe=False)
            # TODO : Should take csv data one by one and convert and call Avatar save_xml API to save and should
            # proceed to next data when saving is complete.
        except FileNotFoundError as e:
            return HttpResponse("File not found in {} location".format(AVATAR_XML_CSV_PATH))
