from rest_framework.response import Response
from rest_framework.views import APIView

from cheerios.players.decorators import get_store_procedure_api
from cheerios.services.is_authenticated import is_authenticated
from cheerios.services.redis import RedisSession
from cheerios.utils import CheeriosUtils

utils_obj = CheeriosUtils()
redis_obj = RedisSession()


class Logout(APIView):
    """
    API View to handle logouts from the website
    """

    @is_authenticated()
    @get_store_procedure_api
    def post(self, request):
        """
        Gets the player id from the request body
        Removes the key for the respective player id from redis
        :param request:
        :return:
        """
        player_id = request.data.get('player_id')

        redis_obj.remove_keys(str(player_id)+utils_obj.get_auth_string(mobile=False))
        return Response("Player has been successfully logged out.", status=200)
