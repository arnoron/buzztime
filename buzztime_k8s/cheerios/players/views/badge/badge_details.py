"""
Module which holds the BadgeDetail class
"""
from rest_framework.response import Response
from cheerios.players.constants import ACHIEVEMENT_PLATFORM, \
    NAME_SPACE_TYPE, STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.settings import MSSQLUSERDB
from cheerios.players.decorators import close_connection
from .badge_serializer import BadgeDetailsSerializer



class BadgeDetail(PlayersViewSet):
    """
    Returns the badge details response
    """
    def get_badge_details(self, player_id, badge_id):
        """
        Returns the badge details for a particular player_id and badge_id
        :param player_id:
        :param badge_id:
        :return:
        """
        serializer = BadgeDetailsSerializer(data=dict(player_id=player_id,badge_id=badge_id))
        if serializer.is_valid():
            response_data = dict(badge_info=self.get_badge_info(player_id, badge_id),
                                 badge_events=self.get_badge_events(player_id, badge_id))
            return Response(response_data, status=200)
        else:
            return UTIL_FUNCTION.log_error(400, 'Invalid parameters passed')

    @close_connection
    def get_badge_info(self, player_id, badge_id):
        """
        Returns the badge info for a player and a badge
        :param player_id:
        :param badge_id:
        :return:
        """

        database_name = MSSQLUSERDB

        achievement_platform = ACHIEVEMENT_PLATFORM['BuzztimeNetwork']
        namespace_type = NAME_SPACE_TYPE['BuzztimeCom']
        parameters = dict(AchievementPlatformID=achievement_platform,
                          NamespaceID=namespace_type,
                          NamespaceUserID=player_id,
                          AchievementID=badge_id)

        data_string = self.get_data_string(parameters)

        store_procedure = STORED_PROCEDURE_MAPPER['achievement_info']

        with self.connection_manager(store_procedure, data_string, database_name) as cursor:
            result = self.fetchall_from_cursor(cursor)
        if result:
            result[0]['ImageFileName'] = "{0}-{1}-{2}-{3}".format(
                'badge', result[0]['ImageFileName'],result[0]['AchievementLevelArtName'],'210')
        self.close_cursor()

        return result

    @close_connection
    def get_badge_events(self, player_id, badge_id):
        """
        Returns the badge events for a player and a badge
        :param player_id:
        :param badge_id:
        :return:
        """
        database_name = MSSQLUSERDB
        first_row = 1
        row_count = 100

        achievement_platform = ACHIEVEMENT_PLATFORM['BuzztimeNetwork']
        namespace_type = NAME_SPACE_TYPE['BuzztimeCom']

        parameters = dict(AchievementPlatformID=achievement_platform,
                          NamespaceID=namespace_type,
                          NamespaceUserID=player_id,
                          AchievementID=badge_id,
                          RowStart=first_row,
                          RowCount=row_count)

        data_string=self.get_data_string(parameters)
        store_procedure = STORED_PROCEDURE_MAPPER['achievement_event_list']

        with self.connection_manager(store_procedure, data_string, database_name) as cursor:
            result = self.fetchall_from_cursor(cursor)

        return result
