"""
Module which holds the BadgeClassification class
"""
from cheerios.players.constants import ACHIEVEMENT_PLATFORM, STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION



class BadgeClassification(PlayersViewSet):
    """
    Return the name-value list of badges based on classification type
    """
    def __init__(self):
        """
        Class constructor.
        Initializes the values of several mappers and other variables used in the method
        """
        super().__init__()

        self.achievement_sp_mapper = dict(category=STORED_PROCEDURE_MAPPER
                                          ['achievement_classification'],
                                          rarity=STORED_PROCEDURE_MAPPER
                                          ['achievement_rarity'],
                                          type=STORED_PROCEDURE_MAPPER
                                          ['achievement_type'],
                                          name=STORED_PROCEDURE_MAPPER
                                          ['achievement_name']
                                         )

        self.achievement_mode_key_mapper = dict(category=dict(id='AchievementClassificationID',
                                                              name='AchievementClassificationName'),
                                                rarity=dict(id='AchievementRarityID',
                                                            name='AchievementRarityName'),
                                                type=dict(id='AchievementTypeID',
                                                          name='AchievementTypeName'),
                                                name=dict(id='AchievementID',
                                                          name='AchievementName')
                                               )

        self.selected_mode = ''

    def get_badge_on_classification(self, classification_type):
        """
        Returns badge on classification
        :param classification_type:
        :return:
        """

        data = dict(AchievementPlatformID=ACHIEVEMENT_PLATFORM['BuzztimeNetwork'])
        self.selected_mode = classification_type

        store_procedure = self.achievement_sp_mapper.get(classification_type)

        if not store_procedure:
            return UTIL_FUNCTION.log_error(400, "Incorrect stored procedure passed")

        result = self.execute_stored_procedure(store_procedure, data)
        return result

    def format_response_data(self, result):
        """
        Formats the response data returned by the stored procedure into a JSON readable format
        :param result: un-formatted result data in the form of tuple
        :return: formatted JSON data
        """
        list_data = []

        if isinstance(result, list):
            for result_obj in result:
                list_data.append(
                    dict(name=result_obj.get(self.achievement_mode_key_mapper
                                             [self.selected_mode]['name']),
                         value=result_obj.get(self.achievement_mode_key_mapper
                                              [self.selected_mode]['id']))
                )

        return list_data
