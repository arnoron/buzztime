"""
Module which holds the BadgeList class
"""
import datetime
from operator import itemgetter
from rest_framework.response import Response

from cheerios.players.constants import ACHIEVEMENT_PLATFORM, \
    NAME_SPACE_TYPE, STORED_PROCEDURE_MAPPER
from cheerios.players.views.players import PlayersViewSet, UTIL_FUNCTION
from cheerios.players.views.serializer import CheckIntegerSerializer

class BadgeList(PlayersViewSet):
    """
    Returns the badge list response
    """

    @staticmethod
    def get_badge_list(player_id, badge_type):
        """
        Returns the badge list for a player and a badge type
        :param player_id:
        :return:
        """
        serializer = CheckIntegerSerializer(data=dict(integer_value=player_id))
        if serializer.is_valid():

            if badge_type == 'all':
                badge_interface = AllBadgeList()
            elif badge_type == 'rare':
                badge_interface = RareBadgeList()
            elif badge_type == 'recent':
                badge_interface = RecentBadgeList()
            else:
                UTIL_FUNCTION.log_error(400, \
                    "Incorrect badge-type parameter passed {0}".format(badge_type))
                return Response("Incorrect or no badge type parameter passed. Badge type "
                                "will be either 'all', 'rare' or 'recent'", status=400)

            return badge_interface.get_badges(player_id)


        return UTIL_FUNCTION.log_error(400, \
                                "Incorrect player_id parameter passed {0}".format(player_id))

class BadgeListBase(PlayersViewSet):
    """
    API which returns the recent achievement list of a player
    """

    FIRST_ROW = 1
    ROW_COUNT = 100

    def get_badges(self, player_id):
        """
        Requests a paginated subset of a player's earned achievement
        events sorted by the date last achieved
        :param request:
        :return:
        """

        achievement_platform = ACHIEVEMENT_PLATFORM['BuzztimeNetwork']
        namespace_type = NAME_SPACE_TYPE['BuzztimeCom']

        request_data = dict(AchievementPlatformID=achievement_platform,
                            NamespaceID=namespace_type,
                            NamespaceUserID=player_id)

        stored_procedure = STORED_PROCEDURE_MAPPER['achievement_list']
        result = self.execute_stored_procedure(stored_procedure, request_data)
        return result

    @staticmethod
    def handle_nonetype_date(val):
        """
        Handles none type date values
        :param val:
        :return:
        """
        if not val:
            return datetime.datetime(2000, 1, 1)
        return val


class RareBadgeList(BadgeListBase):
    """
    API which returns the recent achievement list of a player
    """

    def post_process_data(self, data):
        """

        :param data:
        :return:
        """

        for badge in data:
            if badge['AchievementLevelArtName']:
                badge['ImageFileName'] = "{0}-{1}-70".format(badge['ImageFileName'],
                                                             badge['AchievementLevelArtName'])
            else:
                badge['ImageFileName'] = badge['ImageFileName'] + '-70'
            badge['LastAchievementDate'] = self.handle_nonetype_date(badge['LastAchievementDate'])

        # sort the data in the descending order of the rarity percentile and achievement date
        data = sorted(data, key=itemgetter('AchievementRarityEndPercentile',
                                           'LastAchievementDate'), reverse=True)
        # slice the data based on passed start index and count, if any
        return data[int(self.FIRST_ROW) - 1:int(self.FIRST_ROW) - 1 + int(self.ROW_COUNT)]


class AllBadgeList(BadgeListBase):
    """
    API which returns the recent achievement list of a player
    """

    def post_process_data(self, data):
        """

        :param data:
        :return:
        """

        #Since the image file is saved as imagename-70, we append -70
        #send it to the frontend for the easy access of the image file
        for badge in data:

            if badge['AchievementLevelArtName']:
                badge['ImageFileName'] = badge['ImageFileName'] +\
                 '-' + badge['AchievementLevelArtName'] + '-70'
            else:
                badge['ImageFileName'] = badge['ImageFileName'] + '-70'
            badge['LastAchievementDate'] = self.handle_nonetype_date(\
                badge['LastAchievementDate'])

        # sort the data in the ascending order of the achievement name
        data = sorted(data, key=itemgetter('AchievementName'))
        # slice the data based on passed start index and count, if any
        return data[int(self.FIRST_ROW) - 1:int(self.FIRST_ROW) - 1 + int(self.ROW_COUNT)]


class RecentBadgeList(PlayersViewSet):
    """
    API which returns the recent achievement list of a player
    """

    def get_badges(self, player_id):
        """
        Requests a paginated subset of a player's earned achievement events
        sorted by the date last achieved
        :param request:
        :return:
        """

        first_row = 1
        row_count = 100

        achievement_platform = ACHIEVEMENT_PLATFORM['BuzztimeNetwork']
        namespace_type = NAME_SPACE_TYPE['BuzztimeCom']

        request_data = dict(AchievementPlatformID=achievement_platform,
                            NamespaceID=namespace_type,
                            NamespaceUserID=player_id,
                            RowStart=first_row,
                            RowCount=row_count
                            )
        stored_procedure = STORED_PROCEDURE_MAPPER['achievement_event_history']
        result = self.execute_stored_procedure(stored_procedure, request_data)

        return result

    def post_process_data(self, data):
        """
        Renames the image file name
        :return:
        """
        for badge in data:

            if badge['AchievementLevelArtName']:
                badge['ImageFileName'] = badge['ImageFileName'] + \
                '-' + badge['AchievementLevelArtName'] + '-70'
            else:
                badge['ImageFileName'] = badge['ImageFileName'] + '-70'

            badge['EarnedCount'] = badge['AchieveCount']

        return data
