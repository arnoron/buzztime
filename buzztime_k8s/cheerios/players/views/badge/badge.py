"""
Module which holds the Badge API
"""

from cheerios.players.views.badge.badge_classification import BadgeClassification
from cheerios.players.views.players import PlayersViewSet
from cheerios.players.decorators import get_store_procedure_api
from cheerios.players.serializers import BadgeSerializer
from cheerios.utils import CheeriosUtils
CHERRIOS_UTILS = CheeriosUtils()


class BadgeViewSet(PlayersViewSet):
    """
    Returns the badge information
    """
    @get_store_procedure_api
    def get(self, request):
        """
        Returns the badge information response on type
        :return:
        """
        request_params = request.GET
        classification_type = request_params.get('classification-type')
        request_params._mutable = True
        request_params["classification_type"] =  classification_type
        serializer = BadgeSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)
        badge_classification_interface = BadgeClassification()
        return badge_classification_interface.\
            get_badge_on_classification(classification_type)
