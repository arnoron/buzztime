from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class BadgeDetailsSerializer(SerializerValidator):

    player_id = serializers.IntegerField(required=True, allow_null=False)
    badge_id = serializers.IntegerField(required=True, allow_null=False)


