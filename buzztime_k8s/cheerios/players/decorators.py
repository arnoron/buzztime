"""
Decorators for triviaweb
"""
from functools import wraps

from cheerios.services.log_classifier import Logger
from cheerios.services.validate_captcha import ReCaptchaError
from redis.sentinel import MasterNotFoundError, SlaveNotFoundError

from cheerios.utils import CheeriosUtils
from pymssql import OperationalError, DatabaseError
from sqlalchemy import exc
CHERRIOS_UTILS = CheeriosUtils()


def get_store_procedure_api(get_func):
    """
    Decorator to control exception handling for the GET APIs
    """
    @wraps(get_func)
    def inner_fn(self, request=None, *args, **kwargs):
        """
        Inner function which is going to be called for handling exceptions
        """
        try:
            # Do a return on whichever function is being called and
            # if they get any exception raise it.
            return get_func(self, request, *args, **kwargs)
        except ValueError as inst:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.value_error(inst)
        except OperationalError as inst:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.operation_error(inst)
        except exc.DBAPIError as excep:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.dbapi_error(excep)
        except DatabaseError as ex:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.value_error(ex)
        except ReCaptchaError as ex:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.captcha_error(ex)
        except (MasterNotFoundError,SlaveNotFoundError) as ex:
            return CHERRIOS_UTILS.redis_connection_error(ex)
        except Exception as inst:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.exception(inst)
        finally:
            CHERRIOS_UTILS.execute_function_if_exist(self, "close_cursor")
    return inner_fn


def close_connection(get_func):
    """
    Decorator to control exception handling for the GET APIs
    """
    @wraps(get_func)
    def inner_fn(self, *args, **kwargs):
        """
        Inner function which is going to be called for handling exceptions
        """
        try:
            # Do a return on whichever function is being called and
            # if they get any exception raise it.
            # if request:
            #     return get_func(self, request, *args, **kwargs)
            # else:
            return get_func(self, *args, **kwargs)
        except Exception as inst:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            raise inst
        finally:
            CHERRIOS_UTILS.execute_function_if_exist(self, "close_cursor")
    return inner_fn
