#pylint: skip-file
"""
Settings of database for Testcases
"""
from cheerios.settings import *


SECRET_KEY = 'testsecretkeyshouldntbeusedinproduction'

MONGODB = os.environ.get("MONGODB", 'test_buzztime_test')
MONGOHOST = os.environ.get("MONGOHOST", '127.0.0.1')
MONGOPORT = os.environ.get("MONGOPORT", 27017)
LOGGING.get('handlers')['file'] =\
    {
        'level': 'ERROR',
        'class': 'logging.handlers.RotatingFileHandler',
        'formatter': 'verbose',
        'filename': os.path.join(LOG_DIR, 'buzztime_test_app.log'),
        'maxBytes': 1024 * 5,
        'backupCount': 10
    }
