"""
Common function.
"""
import binascii
import datetime
import hashlib
import random
import re
import string
import time

import pytz
import xmltodict
from json import dumps, loads

from Crypto.Cipher import AES
from rest_framework.response import Response
from cheerios.services.log_classifier import Logger
from cheerios.services.redis import RedisSession
from cheerios.settings import AES_INITIALIZATION_VECTOR


class CheeriosUtils:
    """
    Util function which allow to other function to do specific task.
    This function is common for all classes.
    """

    @staticmethod
    def validate_email(email):
        """
        Check if email id is valid.
        """
        from django.core.validators import validate_email
        from django.core.exceptions import ValidationError
        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    @staticmethod
    def is_not_empty(field):
        """
        Check field is empty or not.
        """
        result = False
        if field != '' and field != "None" and field is not None:
            result = True
        return result

    def operation_error(self, inst):
        """
        # Function accept OperationalError exception and log that exception
        # return status 500 response.
        """
        error = str(inst)
        error = "OperationalError {0}".format(error)
        return self.log_error(500, error)

    def dbapi_error(self, inst):
        """
        # Function accept DBAPIError exception and log that exception
        # return status 500 response.
        """
        if inst.connection_invalidated:
            result = "Connection was invalidated"
        else:
            result = "Unable to execute query in database"
        return self.log_error(500, result)

    def value_error(self, inst):
        """
        # Function accept ValueError exception and log that exception
        # return status 400 response.
        """
        error = str(inst)
        error = "ValueError {0}".format(error)
        return self.log_error(400, error)

    def captcha_error(self, inst):
        """
        Accepts and logs captcha errors
        :param inst:
        :return:
        """
        error = str(inst)
        error = "Captcha Error: {0}".format(error)
        return self.log_error(400, error)

    def redis_connection_error(self, inst):
        """

        :param inst:
        :return:
        """
        error = "Internal Server Error. Please retry after some time."
        return self.log_error(500, error)

    def exception(self, inst):
        """
        # Function accept all exception and log that exception
        # return status 500 response.
        """
        error = str(inst)
        return self.log_error(500, error)

    @staticmethod
    def log_error(status_value, error):
        """
        Log error with status code in file.
        """
        message = "Error status code {0}, Error : {1}". \
            format(status_value, error)
        Logger.log.error(message)
        return Response(error, status=status_value)

    @staticmethod
    def attr_exist(obj, attribute):
        """
        Check If attribute is exits in Object
        """
        return hasattr(obj, attribute)

    def execute_function_if_exist(self, obj, attribute):
        """
        Call function if function is exist in object.
        """
        if self.attr_exist(obj, attribute):
            getattr(obj, attribute)()

    def attr_has_value(self, obj, attribute):
        """
        Check if attribute exist and has value
        """
        return self.attr_exist(obj, attribute) and getattr(obj, attribute)

    @staticmethod
    def response_headers(response):
        """
        To return the headers of a response
        :param response:
        :return:
        """
        return response._headers

    def required_parameters(self, data):
        """
        # Function gets list or tupple of values.
        # Check Each element contains certain value.
        """
        contain_value = True
        if data is None:
            raise ValueError("Invalid Parameter passed")
        for value in data:
            if not self.is_not_empty(value):
                contain_value = False

        if not contain_value:
            raise ValueError("Invalid Parameter passed")

    @staticmethod
    def create_md5_hash(data):
        """
        Create md5 of string.
        """
        bytes_value = str.encode(data)
        hash_object = hashlib.md5(bytes_value)
        hash_code = hash_object.hexdigest()
        return hash_code

    @staticmethod
    def is_positive_integer(data):
        """
        Checks if all the data is the list are positive integers
        :param data: list of values to be checked
        :return: True if all are positive integers False otherwise
        """
        for number in data:
            if not number.isdigit():
                return False
        return True

    @staticmethod
    def convert_str_to_datetime(date_time, str_format="%Y-%m-%dT%H:%M:%S"):
        """
        converting string data to date and time object.
        Format is invalid then give error for invaid format.
        """
        if date_time:
            try:
                return datetime.datetime(*time.strptime(date_time, str_format)[:6])
            except ValueError:
                raise ValueError("Incorrect date format, should be %Y-%m-%dT%H:%M:%S")
        return None

    @staticmethod
    def convert_datetime_to_str(date_time, str_format="%Y-%m-%dT%H:%M:%S"):
        """
        converting datetime object to string.
        Passwed object is not of date time type it will throw an invaid datetime object Error.
        """
        if date_time:
            try:
                return date_time.strftime(str_format)
            except AttributeError:
                raise AttributeError("Incorrect date format, should be datetime object")
        return None

    @staticmethod
    def format_date(date_string):
        """
        Converts the date_string recieved through query param to a python datetime object
        :param date_string: date from query param in string format
        :return: python datetime object for the date string
        """
        try:
            date_delimited = date_string.split("-")
            date = datetime.datetime(int(date_delimited[2]),
                                     int(date_delimited[1]),
                                     int(date_delimited[0])
                                     )
        except:
            raise IndexError

        return date

    @staticmethod
    def time_in_range(start, end, time):
        """Return true if time is in the range [start, end]"""
        return start <= time <= end

    @staticmethod
    def dict_clean(items):
        """
        This takes a dict as a param and converts all the None value
        in the dict to 0
        :param items:
        :return:
        """
        result = {}
        for key, value in items:
            if value is None:
                value = 0
            result[key] = value
        return result

    @staticmethod
    def convert_xml_to_dict(data):
        """
        Converts the xml data to a dictionary
        :param data:
        :return:
        """
        return loads(dumps(xmltodict.parse(data), indent=4))

    @staticmethod
    def format_date_time(date, time):
        """
        formats the date and time to a single date time string
        :param date:
        :param time:
        :return:
        """
        return datetime.datetime.strptime(date + ' ' + time, '%d-%m-%Y %H:%M')

    @staticmethod
    def is_float(data):
        """
        Checks if all the data in the list are float values
        :param data: list of values to be checked
        :return: True if all are float False otherwise
        """
        for number in data:
            try:
                float(number)
            except ValueError:
                return False
        return True

    def validate_serializer(self, serializer):
        """
        This Function validate serializer and throw error if invalid.
        """
        if not serializer.is_valid():
            self.log_error(400, serializer.errors)
            raise ValueError("Invalid Parameter passed")

    @staticmethod
    def create_sp_parameters(parameters):
        """
        This function accepts dictionary contatining stored procedure key, value pairs and
        checks for the quotes in the stored procedure parameters and escapes them,
        :param parameters: The dictionary containing the parameters to call stored procedure with
        :return: Cleaned parameter in string format
        """
        return ",".join(["@{0}=NULL".format(data_key) if data_value in ['None', 'NULL'] else
                         "@{0}='{1}'".format(data_key, data_value.replace("'", "''")
                         if isinstance(data_value, str) else data_value)
                         for data_key, data_value in parameters.items()])

    @staticmethod
    def escape_quotes_sp_params(parameters):
        """
        Checks for the quotes in the stored procedure parameters and escapes them
        :param parameters: The parameters to call stored procedure with
        :return: cleaned paramters
        """
        parameters_dict = dict()

        # if no parameters are being passed, just return without any processing
        if not parameters:
            return parameters

        parameters_list = re.split(r',\s*@', parameters.lstrip())

        # Deconstructing the parameter string to a dictionary format and escaping the quotes in values
        for item in parameters_list:
            key, value = item.split("=")

            if value.startswith("'") and value.endswith("'"):
                value = value[1:-1].replace("'", "''")
            if key.startswith("@"):
                key = key[1:]

            parameters_dict[key] = value

        # reconstruct the string and return cleaned parameters
        return ",".join(["@{0}=NULL".format(data_key) if data_value in ['None', 'NULL'] else
                         "@{0}='{1}'".format(data_key, data_value)
                         for data_key, data_value in parameters_dict.items()])


    @staticmethod
    def is_odd(number):
        """
        Checks if the passed number is odd
        :param number:
        :return:
        """
        if number % 2 == 1:
            return True
        return False

    @staticmethod
    def get_pst_date_time():
        """
        This function will return the PST time
        :return:
        """
        pst_timezone = pytz.timezone("US/Pacific")
        date = datetime.datetime.now(pst_timezone).date()
        date = date.strftime('%Y-%m-%d')
        return date

    def get_auth_token(self, token_length):
        """

        :return:
        """
        letters_and_digits = string.ascii_letters + string.digits
        return ''.join(random.choice(letters_and_digits) for i in range(token_length))

    def get_auth_string(self, mobile):
        """

        :return:
        """
        auth_token_string = "_auth_token"
        if not mobile:
            auth_token_string = auth_token_string + "_site"

        return auth_token_string

    def create_auth_token_for_user(self, user_id, persist, mobile=False):
        """

        :param user_id:
        :param persist:
        :return:
        """
        auth_token = self.get_auth_token(token_length=32)
        redis_obj = RedisSession()

        redis_obj.save_data(key=str(user_id)+self.get_auth_string(mobile), value=auth_token, persist=persist)
        return auth_token

    def does_auth_token_exist_for_user(self, user_id, mobile):
        """

        :param user_id:
        :return:
        """
        redis_obj = RedisSession()

        if not redis_obj.get_value(str(user_id)+self.get_auth_string(mobile)):
            return False
        return True

    def get_auth_token_for_user(self, user_id, mobile):
        """

        :param user_id:
        :return:
        """
        redis_obj = RedisSession()

        return redis_obj.get_value(str(user_id)+self.get_auth_string(mobile))

    @staticmethod
    def encrypt_text_aes(plaintext, sym_key):
        """
        Encrypts a text using AES 16 bytes
        :param plaintext: The text to be encrypted
        :return: Encrypted test
        """

        key = binascii.a2b_hex(sym_key)
        iv = binascii.a2b_hex(AES_INITIALIZATION_VECTOR)

        BLOCK_SIZE = 16

        aes = AES.new(key, AES.MODE_CFB, iv, segment_size=128)

        in_len = len(plaintext)
        pad_size = BLOCK_SIZE - (in_len % BLOCK_SIZE)
        ps = plaintext.ljust(in_len + pad_size, chr(pad_size))

        return binascii.b2a_base64(aes.encrypt(ps.encode("utf-8"))).rstrip()



def escape_quotes(func):
    """
    The decorator to allow/deny access based on client IP
    :param func:
    :return:
    """
    def inner(*args, **kwargs):
        """

        :return:
        """
        args_list = list(args)
        for argument in enumerate(args_list):
            if isinstance(argument[1], str):
                escaped_arg = argument[1].replace("'", "''")
                args_list[argument[0]] = escaped_arg
        return func(*args_list)
    return inner

