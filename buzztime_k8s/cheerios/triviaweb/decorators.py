"""
Decorators for triviaweb
"""
import ast
import logging
import pickle
from functools import wraps
from pymssql import OperationalError

from django.conf import settings
from django.core.exceptions import PermissionDenied
from redis.sentinel import MasterNotFoundError, SlaveNotFoundError, ConnectionError
from rest_framework.response import Response
from sqlalchemy import exc

from cheerios.services.redis import RedisSession
from cheerios.triviaweb.constants import ALL_ENTERPRISES, POST_API, \
    SESSION_EXPIRY, DELETE_API, GET_API, PUT_API
from cheerios.triviaweb.utils.utils import UtilFunction
from cheerios.utils import CheeriosUtils

LOGGER = logging.getLogger("triviaweb")  # This is explicit, decorators are loaded before execution.

UTIL_FUNCTION = UtilFunction()
CHERRIOS_UTILS = CheeriosUtils()


def allow_user(check_roles=None, check_enterprise=None, check_value_in=None):
    """
    Allow user to access the parameters which are passed in the decorator
    """

    def actual_decorator(function):
        """
        Actual decorator used which contains the function.
        Allow user to access the function based on the role provided
        """

        @wraps(function)
        def allow(request, *args):
            """
            Check to allow this function to be passed or not
            """
            # If there are no roles defined then make sure that you are returning the function
            _role_check = check_roles and len(check_roles) != 0
            if not _role_check and not check_enterprise:
                return function(request, *args)
            # Get the session id of the player and fetch the data from the redis
            # object. The parse the object and deduce whether the given group-permissions
            # are present as well as check for the enterprise
            try:
                method = request.request.method
                default_id = request.request.COOKIES.get(settings.SESSION_COOKIE_NAME, None)
            except Exception:
                return function(request, *args)
            user_session_id = request.request.META.get('HTTP_AUTHORIZATION', default_id)

            redis_obj = RedisSession()

            user_perm = redis_obj.get_value(key=user_session_id)

            user_perm = user_perm.decode("utf-8")

            # If the only value fetched is the True string then do not show the response
            if user_perm == str(True):
                raise PermissionDenied
            user_perm = ast.literal_eval(user_perm)
            user_roles = user_perm.get('roles')
            if _role_check and len(set(check_roles).intersection(set(user_roles))) == 0:
                # Call an intersection between the sets and check whether the roles which
                # are being checked are present
                message = "User forbidden to access url as they have incorrect roles.\
                               To access this request user must have {0} roles but the present\
                               user has {1} roles". \
                    format(check_roles, user_roles)
                LOGGER.error(message)
                raise PermissionDenied
            user_enterprise = user_perm.get('enterprise')
            if check_enterprise and user_enterprise != ALL_ENTERPRISES:
                data_set = args[0]
                # If we have check_value_in then we can directly check if
                # the attribute exists in the data-set through hasattr and
                # fetch the value whenever we need it. If value does not exist
                # then use the default call
                if check_value_in and hasattr(data_set, check_value_in):
                    _ent = getattr(data_set, check_value_in).get(check_enterprise)
                else:
                    _ent = data_set.GET.get(check_enterprise) if method not in \
                                                                 [POST_API, DELETE_API,
                                                                  PUT_API] else data_set.data.get(
                        check_enterprise)
                if _ent and str(user_enterprise) != str(_ent):
                    message = "User forbidden to access API as they beloong to a different brand.\
                               To access this request user must have {0} brand id but the present\
                               user has {1} brand id". \
                        format(check_enterprise, user_enterprise)
                    LOGGER.error(message)
                    raise PermissionDenied
            return function(request, *args)

        return allow

    return actual_decorator


def check_serializer_validation(serializer):
    """
    Check whether the data which is being passed is matching your
    serializer or not
    """

    def actual_decorator(function):
        """
        Actual decorator used which contains the function.
        This allows us to access the request variable present inside our calls.
        """

        @wraps(function)
        def allow(viewset_args, *args):
            """
            Check to allow this function to be passed or not
            """
            # The assumption here is that the validations need to occur on:
            # POST, DELETE methods => request.data
            # GET methods => request.GET
            # This assumption becomes valid as our API's our called like that
            check_on = viewset_args.request.data if \
                viewset_args.request.method != GET_API \
                else viewset_args.request.GET
            validator = serializer(data=check_on)
            if validator.is_valid():
                return function(viewset_args, *args)
            else:
                raise ValueError(validator.errors)

        return allow

    return actual_decorator


def get_store_procedure_api(get_func):
    """
    Decorator to control exception handling for the GET APIs
    """

    @wraps(get_func)
    def inner_fn(self, request=None, *args):
        """
        Inner function which is going to be called for handling exceptions
        """
        try:
            # Do a return on whichever function is being called and
            # if they get any exception raise it.
            return get_func(self, request, *args)
        except ValueError as inst:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.value_error(inst)
        except OperationalError as inst:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.operation_error(inst)
        except exc.DBAPIError as excep:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.dbapi_error(excep)
        except PermissionDenied:
            raise PermissionDenied
        except (MasterNotFoundError, SlaveNotFoundError, ConnectionError) as ex:
            return CHERRIOS_UTILS.redis_connection_error(ex)
        except Exception as inst:
            CHERRIOS_UTILS.execute_function_if_exist(self, "connection_rollback")
            return CHERRIOS_UTILS.exception(inst)
        finally:
            CHERRIOS_UTILS.execute_function_if_exist(self, "close_cursor")

    return inner_fn


def cache_api(set_ttl=SESSION_EXPIRY):
    """
    Decorator to pass the cache api variable inputs
    """

    def actual_decorator(get_func):
        """
        Decorator to control the caching of APIs using Redis
        """

        @wraps(get_func)
        def inner_fn(self, request=None, *args):
            """
            Inner function which is going to be called for handling caching
            """

            redis_obj = RedisSession()
            redis_key = self.request.get_full_path()
            if redis_obj.get_value(key=redis_key):
                # Get the value of the key from redis and run it through pickle's parsing
                # to get the result. Then use the result as the response for the API
                result = redis_obj.get_value(key=self.request.get_full_path())
                result = pickle.loads(result)

                return Response(result, status=200)
            _response_func = get_func(self, request, *args)

            if _response_func.status_code == 200:

                try:
                    redis_obj.save_data(key=self.request.get_full_path(),
                                        value=pickle.dumps(_response_func.data), ttl=set_ttl)
                except:
                    CHERRIOS_UTILS.log_error(500, "Redis Master is down unable save data")

            return _response_func

        return inner_fn

    return actual_decorator
