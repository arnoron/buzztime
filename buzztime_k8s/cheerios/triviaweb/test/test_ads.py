"""
Test case for ads.py
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import AD_API
from cheerios.triviaweb.constants import STUMPTRIVIA_TESTADS_CONSTANTS
from cheerios.triviaweb.test.test_data import get_ad_data, \
    get_ad_id


class TestAds(APITestCase):
    """
    Test case for ads
    """

    def setUp(self):
        """
        Setup the test cases
        """
        self.url = "/triviaweb/ads/?brand_id=2"
        super_admin_session = "abcde12345"
        self.auth = super_admin_session
        host_session = "abcde12345"
        self.auth_host = host_session

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses([], [])))
    def test_get_with_brand_id(self):
        """
        Test the get request when brand id is given and user is valid.
        """
        response = self.client.get(self.url, HTTP_AUTHORIZATION=self.auth, \
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses([], [])))
    def test_get_without_brand_id(self):
        """
        Test the get request when brand id is not given and user is valid.
        """
        response = self.client.get(AD_API, HTTP_AUTHORIZATION=self.auth, \
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin']}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses([], [])))
    def test_delete_valid_user(self):
        """
        Test the delete request when user is valid.
        """
        data = get_ad_id(True)
        response = self.client.delete(AD_API, data, HTTP_AUTHORIZATION=self.auth, \
                                      format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin']}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    STUMPTRIVIA_TESTADS_CONSTANTS['StumpTriviaAdID'])))
    def test_post_valid_user(self):
        """
        Test the post request when user is valid and Ad data is valid.
        """
        data = get_ad_data(False, True)
        response = self.client.post(AD_API, data, HTTP_AUTHORIZATION=self.auth, \
                                    format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin']}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses( \
                [STUMPTRIVIA_TESTADS_CONSTANTS['StumpTriviaBrandingID']])))
    def test_put_valid_user(self):
        """
        Test the put request when user is valid and Ad data is valid.
        """
        data = get_ad_data(True, True)
        response = self.client.put(AD_API, data, HTTP_AUTHORIZATION=self.auth, \
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_get_invalid_user(self):
        """
        Test the get request when user is invalid.
        """
        response = self.client.get(self.url, HTTP_AUTHORIZATION=None,
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['admin']}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_delete_invalid_user(self):
        """
        Test the delete request when user is invalid.
        """
        data = get_ad_id(True)
        response = self.client.delete(AD_API, data,
                                      HTTP_AUTHORIZATION=self.auth_host,
                                      format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['admin']}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_post_invalid_user(self):
        """
        Test the post request when user is invalid and Ad data is valid.
        """
        data = get_ad_data(False, True)
        response = self.client.post(AD_API, data,
                                    HTTP_AUTHORIZATION=self.auth_host,
                                    format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['admin']}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_put_invalid_user(self):
        """
        Test the put request when user is invalid and Ad data is valid.
        """
        data = get_ad_data(True, True)
        response = self.client.put(AD_API, data,
                                   HTTP_AUTHORIZATION=self.auth_host,
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    def test_put_invalid_data(self):
        """
        Test the put request when user is valid and Ad data is invalid.
        """
        data = get_ad_data(True, False)
        with self.assertRaises(Exception):
            self.client.put(AD_API, data, HTTP_AUTHORIZATION=self.auth,
                            format='json')

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    def test_delete_invalid_ad_id(self):
        """
        Test the delete request when user is valid.
        """
        data = get_ad_id(False)
        with self.assertRaises(Exception):
            self.client.put(AD_API, data, HTTP_AUTHORIZATION=self.auth,
                            format='json')

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None]))
    def test_get_with_invalid_data(self):
        """
        Test the get request when brand id is not valid data.
        """
        url = "/triviaweb/ads/?brand_id=test"
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, \
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
