import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


class StumpSelenium():
    def __init__(self):
        """
        Constructor: It will call the main functions
        required for testing
        """
        # create a new Firefox session
        self.driver_host = webdriver.Firefox()
        self.driver_host.maximize_window()
        self._room_id = None
        self._team_count = 1
        # set player count here
        self.player_count = 20
        # navigate to the application home page
        self.driver_host.get("http://stg-stumpdocker-01/home")
        self.get_login_page()
        self.get_game_start_page()
        self.get_player_page()
        self.get_team_list()
        self.instruction_page()
        self.game_start()
        self.close_browser()

    @property
    def room_id(self):
        """
        This is 'room_id' property.
        To InvoKe it use self.x
        Or If c is an instance of C, c.x will invoke the getter
        """
        return self._room_id

    @room_id.setter
    def room_id(self, value):
        """
        This is setter property
        To invoke: c.x = value
        Or self.x
        :param value:
        :return:
        """
        self._room_id = value

    @room_id.deleter
    def x(self):
        """
        This is a delete property
        To invoke: del c.x
        Or del self.x
        :return:
        """
        del self._room_id

    @property
    def team_count(self):
        """
        This is 'room_id' property.
        To InvoKe it use self.x
        Or If c is an instance of C, c.x will invoke the getter
        """
        return self._team_count

    @team_count.setter
    def team_count(self, value):
        """
        This is setter property
        To invoke: c.x = value
        Or self.x
        :param value:
        :return:
        """
        self._team_count += value

    def get_login_page(self):
        # get the Login button and move to login page
        login_button = self.driver_host.find_element_by_class_name("login-host")
        login_button.click()
        self.login()

    def login(self):
        # Enter username, password and login
        wait = WebDriverWait(self.driver_host, 10)
        wait.until(lambda s: s.find_element_by_id('username').is_displayed())
        self.driver_host.find_element_by_name("username").send_keys("soneygeorge")
        wait.until(lambda s: s.find_element_by_id('password').is_displayed())
        self.driver_host.find_element_by_id("password").send_keys("Hasher123")
        self.driver_host.find_element_by_class_name("btn-success").click()

    def get_game_start_page(self):
        """
        Host Page to submit validate game token
        and start game
        :return:
        """
        # Wait till button is visible
        WebDriverWait(self.driver_host, 40).until(lambda s: s.find_element_by_class_name('game-next-button').is_displayed())
        time.sleep(10)
        self.driver_host.find_element_by_class_name("game-next-button").click()
        self.room_id = self.get_game_token()

    def get_game_token(self):
        """
        This function will access span in html and get game token
        which will be used by players to register there team
        and be part of same game
        :return:
        """
        WebDriverWait(self.driver_host, 10).until(lambda s: s.find_element_by_id('room-id').is_displayed())
        return self.driver_host.find_element_by_id('room-id').text

    def get_team_list(self):
        """
        Function to be called on Host side.
        This page will show host a list of teams.
        Host will select all team names and move to instruction page.
        :return:
        """
        time.sleep(10)
        TABLE_XPATH = "//table[contains(@class,'table')]/tbody/tr"
        count = len(self.driver_host.find_elements_by_xpath(TABLE_XPATH))
        TD_XPATH = "//table[contains(@class,'table')]/tbody/tr[1]/td[4]"
        while count > 0:
            self.driver_host.find_element_by_xpath(TD_XPATH).click()
            count -= 1
        BUTTON_XPATH = "//div[contains(@class,'margin-button')]/button[contains(@class,'btn-success')]"
        self.driver_host.find_element_by_xpath(BUTTON_XPATH).click()

    def instruction_page(self):
        """
        This function will check for next button on instruction page
        for host. Host will click on next button and redirect to game page.
        :return:
        """
        time.sleep(10)
        INSTRUCTION_XPATH = "//div[contains(@class,'next-button')]//button[contains(@class,'btn-success')]"
        self.driver_host.find_element_by_xpath(INSTRUCTION_XPATH).click()

    def game_start(self):
        """
        Here starts the game. This function will internally call
        separate functions to perform various operations.
        :return:
        """
        round_number = 1
        while round_number <= 6:
            counter = 0
            if round_number < 6:
                # It will come here for all rounds except bonus round
                while counter < 2:
                    self.round_page()
                    question_number = 1
                    while question_number <= 5:
                        self.process_question(question_number)
                        question_number += 1
                    counter += 1
            else:
                self.round_page()
                self.process_question(1)
            round_number += 1
            self.leaderboard()
        self.player_survey()
        self.host_end_game()

    def player_survey(self):
        """
        This page will be visible for players only.
        Each player will submiot there survey answers.
        Once filled all answers click on Submit button.
        :return:
        """
        SUBMIT_BUTTON_XPATH = "//button[contains(@class,'btn-success')]"
        driver_count = 0
        time.sleep(10)
        while driver_count < self.player_count:
            self.driver[driver_count].find_element_by_xpath(SUBMIT_BUTTON_XPATH).click()
            driver_count += 1

    def host_end_game(self):
        """
        This page will be visible to Host only.
        Host will look for End game button and
        click on button to end the game and
        call api to save the game data in DB
        :return:
        """
        END_BUTTON_XPATH = "//button[contains(@class, 'btn-success')]"
        self.driver_host.find_element_by_xpath(END_BUTTON_XPATH).click()

    def process_question(self, question_number):
        """
        Below functions are being used at multiple places for instead of
        calling them multiple times, created a function whihch will call them internally.
        :param question_number:
        :return:
        """
        self.question_page(question_number)
        self.host_validate_question()
        self.host_correct_answer()

    def leaderboard(self):
        """
        The Leaderboard will be displayed to both Host and Players.
        Host will have control of this function.
        This function will look for reeveal and next button.
        Till reveal is enabled click reveal button and
        once reveal is disabled and next button is enabled
        click on Next button and move to next round/category page
        :return:
        """
        # time.sleep(20)
        REVEAL_XPATH = "//button[contains(@class,'pull-left')]"
        NEXT_XPATH = "//button[contains(@class,'pull-right')]"
        reveal_button = self.driver_host.find_element_by_xpath(REVEAL_XPATH)
        next_button = self.driver_host.find_element_by_xpath(NEXT_XPATH)
        call_again = True
        while call_again:
            if reveal_button.is_enabled():
                reveal_button.click()
            elif next_button.is_enabled():
                call_again = False
                next_button.click()

    def round_page(self):
        """
        Button on Category and round button will be clicked
        and redirected to question page.
        :return:
        """
        time.sleep(20)
        ROUND_XPATH = "//div[contains(@id,'category-detail')]/button[contains(@class,'btn-success')]"
        self.driver_host.find_element_by_xpath(ROUND_XPATH).click()

    def question_page(self, question_number):
        """
        This function will execute from player side.
        Enter answer and select a bet score and clisk on submit button
        :param question_number:
        :return:
        """
        ANSWER_XPATH = "//input[contains(@id,'answer')]"
        SCORE_XPATH = "//div[contains(@class, 'btn-group')]/button[" + str(question_number) + "]"
        SUBMIT_BUTTON_XPATH = "//button[contains(@id, 'submitAnswer')]"
        driver_count = 0
        time.sleep(10)
        while driver_count < self.player_count:
            self.driver[driver_count].find_element_by_xpath(ANSWER_XPATH).send_keys("Answer")
            self.driver[driver_count].find_element_by_xpath(SCORE_XPATH).click()
            self.driver[driver_count].find_element_by_xpath(SUBMIT_BUTTON_XPATH).click()
            driver_count += 1

    def host_validate_question(self):
        """
        This function is called on Host side
        to validate answers submitted by players.
        As per this function, host is selecting all teh answers
        given by every team.
        :return:
        """
        time.sleep(10)
        TABLE_XPATH = "//table[contains(@class,'table')]/tbody/tr"
        count = len(self.driver_host.find_elements_by_xpath(TABLE_XPATH))
        TD_XPATH = "//table[contains(@class,'table')]/tbody/tr[1]/td[4]"
        while count > 0:
            self.driver_host.find_element_by_xpath(TD_XPATH).click()
            count -= 1
        BUTTON_XPATH = "//div[contains(@id,'game-detail')]/button[contains(@class,'btn-success')]"
        self.driver_host.find_element_by_xpath(BUTTON_XPATH).click()

    def host_correct_answer(self):
        """
        Page will be displayed to both host and players.
        This function will be used on Host side to click on
        next button and move to next
        page(Either next question or round/category page)
        :return:
        """
        time.sleep(10)
        NEXT_XPATH = "//div[contains(@id,'answer-details')]/button[contains(@class,'btn-success')]"
        self.driver_host.find_element_by_xpath(NEXT_XPATH).click()

    def get_player_page(self):
        """
        Function to register different players browsers
        and navigate to Player team name page
        :return:
        """
        driver_count = 0
        self.driver = []
        while driver_count < self.player_count:
            self.driver.append(webdriver.Firefox())
            self.driver[driver_count].maximize_window()
            # navigate to the application home page
            self.driver[driver_count].get("http://stg-stumpdocker-01/home")
            play_button = self.driver[driver_count].find_element_by_class_name("play-btn")
            play_button.click()
            self.set_player_teamname(driver_count)
            driver_count += 1

    def set_player_teamname(self, driver_count):
        """
        This function will be called for each team
        to register there team name with Game
        :param driver_count:
        :return:
        """
        WebDriverWait(self.driver[driver_count], 10).until(lambda s: s.find_element_by_id('teamname').is_displayed())
        self.driver[driver_count].find_element_by_name("teamname").send_keys("Team" + str(self.team_count))
        WebDriverWait(self.driver[driver_count], 10).until(lambda s: s.find_element_by_id('gameid').is_displayed())
        self.driver[driver_count].find_element_by_name("gameid").send_keys(self.room_id)
        self.team_count = 1
        self.driver[driver_count].find_element_by_id("submitGameName").click()

    def close_browser(self):
        # close the all browser window
        driver_count = 0
        while driver_count < self.player_count:
            self.driver[driver_count].quit()
            driver_count += 1
        self.driver_host.quit()

if __name__ == '__main__':
    STUMPSELENIUM = StumpSelenium()

# get the search textbox
# search_field = driver.find_element_by_id("gs_lc0")
# print(search_field, "***")
# search_field.clear()

# enter search keyword and submit
# search_field.send_keys("Selenium WebDriver Interview questions")
# search_field.submit()

# get the list of elements which are displayed after the search
# currently on result page using find_elements_by_class_name  method
# lists = driver.find_elements_by_class_name("_Rm")
#
# # get the number of elements found
# print("Found " + str(len(lists)) + "searches:")
#
# # iterate through each element and print the text that is
# # name of the search
#
# i = 0
# for listitem in lists:
#     print(listitem)
#     i = i + 1
#     if (i > 10):
#         break

# close the browser window
# driver.quit()