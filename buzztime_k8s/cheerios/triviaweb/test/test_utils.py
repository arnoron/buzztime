"""
Unit Test case for Trivia web API
"""
from rest_framework.test import APITestCase
from pymssql import OperationalError
from sqlalchemy import exc
from cheerios.triviaweb.utils.utils import UtilFunction
from cheerios.triviaweb.test.test_data import data_with_empty_value, \
    data_with_proper_data, data_with_single_data, get_test_token
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


class TestUtilsUnitTestcase(APITestCase):
    """
    Unit Test case for Utils  functions
    """

    def test_empty_parameter(self):
        """
        Testing is_not_empty with Empty parameter.
        """
        result = CHEERIOS_UTILS.is_not_empty(None)
        self.assertEqual(result, False)

    def test_is_not_empty_with_data(self):
        """
        Testing is_not_empty with value.
        """
        result = CHEERIOS_UTILS.is_not_empty("data")
        self.assertEqual(result, True)

    def test_required_paramter_empty(self):
        """
        Testing required_parameters with empty data.
        """
        data = data_with_empty_value()
        result = None
        try:
            CHEERIOS_UTILS.required_parameters(data)
        except ValueError as inst:
            result = str(inst)
        self.assertEqual(result, "Invalid Parameter passed")

    def test_required_paramter_list(self):
        """
        Testing required_parameters with empty list data
        """
        data = data_with_proper_data()
        result = None
        try:
            CHEERIOS_UTILS.required_parameters(data)
        except ValueError as inst:
            result = inst
        self.assertEqual(result, None)

    def test_required_paramter_data(self):
        """
        Testing required_parameters with empty single data
        """
        data = data_with_single_data()
        result = None
        try:
            CHEERIOS_UTILS.required_parameters(data)
        except ValueError as inst:
            result = inst
        self.assertEqual(result, None)

    def test_operaional_error(self):
        """
        Testing operational error.
        """
        data = OperationalError("Operational Error")
        result = CHEERIOS_UTILS.operation_error(data)
        expected = "OperationalError Operational Error"
        self.assertEqual(result.data, expected)

    def test_connection_error(self):
        """
        Testing connection error.
        """
        data = exc.DBAPIError(None, None, None, connection_invalidated=True)
        result = CHEERIOS_UTILS.dbapi_error(data)
        expected = "Connection was invalidated"
        self.assertEqual(result.data, expected)
    def test_dbapi_error(self):
        """
        Testing dbapi_error.
        """
        data = exc.DBAPIError(None, None, None)
        result = CHEERIOS_UTILS.dbapi_error(data)
        expected = "Unable to execute query in database"
        self.assertEqual(result.data, expected)

    def test_value_error(self):
        """
        Testing value_error.
        """
        data = ValueError("Value Error")
        result = CHEERIOS_UTILS.value_error(data)
        expected = "ValueError Value Error"
        self.assertEqual(result.data, expected)

    def test_general_exception(self):
        """
        Testing general exception.
        """
        data = Exception("Exception Raised")
        result = CHEERIOS_UTILS.exception(data)

        expected = "Exception Raised"
        self.assertEqual(result.data, expected)

    def test_log_error(self):
        """
        """
        data = "Invalid Json Format"
        result = CHEERIOS_UTILS.log_error(500, data)
        expected = "Invalid Json Format"
        self.assertEqual(result.data, expected)

    def test_is_number_negative(self):
        """
        Test if is_positive_number function returns
        True for positive integer and
        False for negative integer
        :return:
        """
        data = -1
        result = None
        try:
            UTIL_FUNCTION.is_positive_number(data)
        except ValueError as inst:
            result = str(inst)
        self.assertEqual(result, "Parameter passed should be a positive integer.")

    def test_is_number_positive(self):
        """
        Test if is_positive_number function returns
        True for positive integer and
        False for negative integer
        :return:
        """
        result = UTIL_FUNCTION.is_positive_number(1)
        self.assertEqual(result, True)

    def get_headers_for_api(self):
        """
        Get the headers for API for AUTH as per the new middleware
        """
        return {'HTTP_AUTHORIZATION': get_test_token()['TEST_AUTH_TOKEN']}
