"""
Test cases for Location API
"""
import mock
from rest_framework import status
from rest_framework.response import Response
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status, response_data
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import LOCATION_API, USER_ROLES, API_METHODS
from cheerios.triviaweb.test.access_control import TriviaAccessControl
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import location_complete_data, USER_STATUS
from cheerios.triviaweb.utils.utils import remove_cache

MSSQL_FETCH_ALL_PATH = "cheerios.triviaweb.views.locations.LocationViewSet.post_call"


class MockedLocationViewSet():
    """
    Class to mock the location API post and put calls
    """

    def insert_new_location(self, *_):
        """
        Check if email exists mocked function
        """
        return Response(True, status=200)

    def update_location(self, *_):
        """
        Check if email exists mocked function
        """
        return Response(True, status=200)

    def fetchall(self, *_):
        """
        Overriding Query Execution
        """
        pass

    def post_call(self, *_):
        """
        Overriding post call
        """
        return


class TestLocationAPI(APITestCase):
    """
    Testcase for trivia web locations.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.player_id = "1234"
        self.auth = "test-auth-header"

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None, "{'roles':['super-admin'], 'enterprise':'1'}".encode(
                    "utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_user_get_location(self):
        """
        Test trivia web get API.
        """
        location_url = "{0}?brand_id={1}".format(LOCATION_API, 1)
        response = self.client.get(location_url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        self.assertTrue(len(response_data(response)) > 0)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None, "{'roles':['super-admin'], 'enterprise':'1'}".encode(
                    "utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_get_location_failure(self):
        """
        Test trivia location API without user credentials
        """
        response = self.client.get(LOCATION_API, format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 b'}(NNs.']))
    def test_get_brand_id(self):
        """
        Test trivia location API without user credentials
        """
        location_url = "{0}?brand_id={1}".format(LOCATION_API, 1)
        response = self.client.get(location_url, HTTP_AUTHORIZATION=self.auth, format='json')
        data = response_data(response)
        self.assertTrue(len(data) > 0)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 b'}(NNs.']))
    def test_get_location_id(self):
        location_url = "{0}?location_id={1}".format(LOCATION_API, 1)
        response = self.client.get(location_url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    def test_user_post_failure(self):
        """
        Test trivia location API with incorrect post details
        """
        result = self.client.post(LOCATION_API, HTTP_AUTHORIZATION=self.auth, \
                                  data={}, format='json')
        self.assertEqual(response_status(result), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    def test_user_post_city_failure(self):
        """
        Test trivia location API without city details
        """
        data = location_complete_data()
        # Remove city from the location data set being passed for update
        del data['city']
        result = self.client.post(LOCATION_API, HTTP_AUTHORIZATION=self.auth, \
                                  data=data, format='json')
        self.assertEqual(response_status(result), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    def test_user_post_latitude_failure(self):
        """
        Test trivia location API without latitude details
        """
        data = location_complete_data()
        # Remove city from the location data set being passed for update
        del data['latitude']
        result = self.client.post(LOCATION_API, HTTP_AUTHORIZATION=self.auth, \
                                  data=data, format='json')
        self.assertEqual(response_status(result), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    def test_user_post_success(self):
        """
        Test trivia location API with correct post details
        """
        response = self.client.post(LOCATION_API, HTTP_AUTHORIZATION=self.auth, \
                                    data=location_complete_data(), format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    def test_user_put_success(self):
        """
        Test trivia location API with correct post details
        """
        api = "{0}?location_id={1}".format(LOCATION_API, 1)
        response = self.client.put(api, HTTP_AUTHORIZATION=self.auth, \
                                   data=location_complete_data(), format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['locations'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    def test_user_delete_success(self):
        """
        Test trivia location API with correct post details
        """
        url = "{0}?location_id=1".format(LOCATION_API)
        response = self.client.delete(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(MSSQL_FETCH_ALL_PATH, side_effect=MockedLocationViewSet().post_call)
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['Host'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_forbidden_post(self, *_):
        """
        Test trivia location API with correct post details for Host
        """
        obj = TriviaAccessControl()
        obj.forbidden_request(self, LOCATION_API, API_METHODS["POST"], \
                              [USER_ROLES['HOST']], location_complete_data())

    @mock.patch(MSSQL_FETCH_ALL_PATH, side_effect=MockedLocationViewSet().fetchall)
    @mock.patch(MSSQL_FETCH_ALL_PATH, side_effect=MockedLocationViewSet().post_call)
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['Host'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.ping_master', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_forbidden_delete(self, *_):
        """
        Test delete location API with correct post details for Host
        """
        url = "{0}?location_id=1".format(LOCATION_API)
        obj = TriviaAccessControl()
        obj.forbidden_request(self, url, API_METHODS["DELETE"], \
                              [USER_ROLES['HOST']])

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def tearDown(self):
        """
        This will remove redis cache created by the particular test-case.
        So that it won't be a problem for another test-case.
        :return: None
        """
        remove_cache(key='')
