"""
This file includes test cases to check
trivia web API.
"""
import json
from random import randint
import mock
from rest_framework import status
from rest_framework.test import APITestCase
from cheerios.analytics.test_data import response_status, response_data
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import post_signup_data
from cheerios.triviaweb.constants import SIGNUP_URL

GET_PLAYER_STATUS = 'cheerios.triviaweb.utils.user_util.UserDetailsUtil.get_particular_status'


def mocked_requests_signup(_kwargs):
    """
    Mocking the method which calls the store procedure so that
    we do not keep on inserting new entries in the database
    """
    return {}


class MockedEmailSignUp():
    """
    Class to mock the email service from bronto
    """

    def email_exists(self, *args):
        """
        Check if email exists mocked function
        """
        pass


class TestTriviaWebSignup(APITestCase):
    """
    Testcase for trivia web signup.
    """

    def setUp(self):
        """
        Setup the test cases
        """
        self.data = post_signup_data()
        self.url = SIGNUP_URL

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['email_not_exists'],
                    MOCK_RESPONSE['username_not_exists'],
                    MOCK_RESPONSE['profile_pin'],
                    [], [])))
    @mock.patch(GET_PLAYER_STATUS, mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
        MOCK_RESPONSE['get_paticular_status'])))
    @mock.patch('cheerios.triviaweb.utils.user_util.UserDetailsUtil.get_particular_role', \
                mock.Mock(return_value=1))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_check_post_success(self, mock_bad_word):
        """
        Check whether the signup form works with our correct send_data
        """
        self.data = {k: "{0}{1}".format(v, randint(0, 10000)) for k, v in self.data.items()}
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    def test_serializer_for_signup(self):
        """
        Check whether the serializer for signup throws value error on incorrect params
        """
        response = self.client.post(self.url, {}, format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['bad_words'])))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_for_inappropriate_words(self, mock):
        """
        Check for inapproprate word
        """
        inappropriate_data = self.data
        inappropriate_data['firstName'] = 'SATAN'
        response = self.client.post(self.url, inappropriate_data, format='json')
        self.assertEqual(response_data(response), 'SATAN contains inappropriate text')
        self.assertEqual(response_status(response), 400)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['email_not_exists'],
                                                         MOCK_RESPONSE['username_exists'])))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_for_duplicate_username(self, mock):
        """
        Check for duplicate username
        """
        _data = self.data
        _data['username'] = 'Hasher'
        response = self.client.post(self.url, _data, format='json')
        self.assertEqual(response_data(response), "Username is already taken")
        self.assertEqual(response_status(response), 400)
