"""
This file includes test cases to check change_password_data
"""
import mock
from rest_framework.test import APITestCase

from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import post_change_password_data, \
    post_login_data, get_session_key, USER_STATUS
from cheerios.triviaweb.constants import CHANGE_PASSWORD, LOGIN_URL
from cheerios.analytics.test_data import response_status, response_data



class TestChangePassword(APITestCase):
    """
    Test for change_password_data
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.change_password_data = post_change_password_data()
        self.url = CHANGE_PASSWORD

        self.auth = "test-auth-header"

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses([])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_change_password_invalid(self):
        """
        Test change password for incorrect old password data
        :return:
        """
        self.change_password_data['password'] = 'invalid'
        response = self.client.put(self.url, self.change_password_data, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_data(response), False)
        self.assertEqual(response_status(response), 200)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses([])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_change_password_failure(self):
        """
        Test change password for incorrect username
        :return:
        """
        self.change_password_data['username'] = 'invalid'
        response = self.client.put(self.url, self.change_password_data, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_data(response), False)
        self.assertEqual(response_status(response), 200)
