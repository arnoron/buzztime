"""
Test cases for enterprise
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import ENTERPRISE_URL, USER_ROLES, API_METHODS
from cheerios.triviaweb.test.access_control import TriviaAccessControl
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import delete_brand_data, enterprise_valid_data, \
    delete_brand_invalid_data, \
    enterprise_blank_data, enterprise_invalid_data, USER_STATUS

MOCK_PATH = 'cheerios.triviaweb.views.enterprise.EnterpriseViewSet.call_brand_sp'
TEST_BRAND_ID = 123


def mocked_valid_stored_procedure(param, stored_procedure):
    """
    :return: Mocked response for successful save
    """
    if param and stored_procedure:
        response = [{'StumpTriviaBrandingID': TEST_BRAND_ID}]
        return response


def mocked_invalid_stored_procedure(param, stored_procedure):
    """
    :return: Mocked response for unsuccessful save
    """
    if param and stored_procedure:
        response = False
        return response


class TestEnterprise(APITestCase):
    """
    Testcase for trivia web Enterprise.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.url = ENTERPRISE_URL
        self.auth = "test-auth-header"

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['ent_get_api'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    def test_enterprise_get_api(self):
        """
        Test trivia web get API.
        """
        response = self.client.get(self.url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_enterprise_post_validate(self):
        """
        Test Enterprise post data validation with blank data
        """
        response = self.client.post(self.url, HTTP_AUTHORIZATION=self.auth,
                                    data=enterprise_blank_data(), format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[[{'StumpTriviaBrandingID': 123}], [{'StumpSiteID': 170}]]))
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"logo":"test_logo"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_enterprise_post(self, mocked_stored_procedure=True):
        """
        Mocking: connection_manager
        Test Enterprise post data validation
        #Todo invalid details test-case fails
        """
        if mocked_stored_procedure:
            response = self.client.post(self.url, HTTP_AUTHORIZATION=self.auth,
                                        data=enterprise_valid_data(), format='json')
            self.assertEqual(response_status(response), status.HTTP_200_OK)
            self.assertEqual(response.data[0].get('StumpTriviaBrandingID'), TEST_BRAND_ID)

    @mock.patch('cheerios.triviaweb.views.customize.CustomizeViewSet.save_onboarding_logo',
                mock.Mock(return_value='None'))
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"logo":"test_logo"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_invalid_enterprise_post(self):
        """
        Mocking: connection_manager
        Test Enterprise post data with unsuccesfull brand save
        Mocking on high-level as it is dependent on cache. But made sure the mocked are covered
        in another test cases.
        """
        response = self.client.post(self.url, HTTP_AUTHORIZATION=self.auth,
                                    data=enterprise_valid_data(), format='json')
        self.assertEqual(response_status(response), status.HTTP_500_INTERNAL_SERVER_ERROR)

    @mock.patch(MOCK_PATH, side_effect=mocked_valid_stored_procedure)
    @mock.patch('cheerios.triviaweb.views.customize.CustomizeViewSet.save_onboarding_logo',
                mock.Mock(return_value='None'))
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"logo":"test_logo"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_enterprise_post_validation(self, mocked_stored_procedure=True):
        """
        Mocking: connection_manager
        Test Enterprise post data validation with blank location name
        #Todo Test case fails because of assertion error.
        """
        if mocked_stored_procedure:
            response = self.client.post(self.url, HTTP_AUTHORIZATION=self.auth,
                                        data=enterprise_invalid_data(), format='json')
            self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"logo":"test_logo"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':[], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_forbidden(self):
        """
        Test trivia web get API enterprise for Host
        """
        obj = TriviaAccessControl()
        obj.forbidden_request(self, self.url, API_METHODS["GET"], \
                              [USER_ROLES['HOST']])

    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"logo":"test_logo"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':[], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor',
                mock.Mock(side_effect=[]))
    def test_forbidden_post(self):
        """
        Test trivia web post API enterprise for host
        """
        obj = TriviaAccessControl()
        obj.forbidden_request(self, self.url, API_METHODS["POST"], \
                              [USER_ROLES['HOST']])

        @mock.patch('cheerios.triviaweb.views.enterprise.EnterpriseViewSet.delete_users',
                    mock.Mock(return_value='None'))
        @mock.patch(
            'cheerios.triviaweb.views.enterprise.EnterpriseViewSet.delete_or_activate_brand',
            mock.Mock(return_value='None'))
        @mock.patch('cheerios.services.redis.RedisSession.get_key',
                    mock.Mock(side_effect=[1, b'{"logo":"test_logo"}']))
        @mock.patch('cheerios.services.redis.RedisSession.get_value',
                    mock.Mock(
                        side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                     None]))
        def test_delete_brand(self):
            """
            Tests delete brand
            """
            response = self.client.delete(self.url, HTTP_AUTHORIZATION=self.auth,
                                          data=delete_brand_data(), format='json'
                                          )
            self.assertEqual(response_status(response), status.HTTP_200_OK)

        @mock.patch('cheerios.triviaweb.views.enterprise.EnterpriseViewSet.delete_users',
                    mock.Mock(return_value='None'))
        @mock.patch(
            'cheerios.triviaweb.views.enterprise.EnterpriseViewSet.delete_or_activate_brand',
            mock.Mock(return_value='None'))
        @mock.patch('cheerios.services.redis.RedisSession.get_key',
                    mock.Mock(side_effect=[1, b'{"logo":"test_logo"}']))
        @mock.patch('cheerios.services.redis.RedisSession.get_value',
                    mock.Mock(
                        side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                     None]))
        def test_delete_invalid_brand(self):
            """
            Tests delete brand with invalid brand id
            """
            response = self.client.delete(self.url, HTTP_AUTHORIZATION=self.auth,
                                          data=delete_brand_invalid_data(), format='json'
                                          )
            self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
