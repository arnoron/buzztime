"""
Tests for the send mail viewset
"""
import mock
from rest_framework.test import APITestCase

from cheerios.services.Email.email import Email
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE

EMAIL_SEND = 'cheerios.services.Email.email.Email.send'


def mocked_delivery(_arg):
    """

    :return:
    """
    delivery = {'results': [{'isError': False}]}

    return delivery


def mocked_fail_delivery(_arg):
    """

    :return:
    """
    delivery = {'results': [{'isError': True}]}

    return delivery


class TestSendEmailViewSet(APITestCase):
    """
    Tests for send email viewset
    """

    def setUp(self):
        """
        Function to initialize class scope variables
        """
        self.send_mail_endpoint = '/triviaweb/forgot-password/'

    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses( \
                        MOCK_RESPONSE['mail_recipient'])))
    @mock.patch(EMAIL_SEND, mock.Mock(return_value=True))
    def test_send_email(self):
        """
        Tests the successful email sending
        :return:
        """

        recipient_details = dict(username='test_username',
                                 email='test_email@gmail.com',
                                 display_name='test_display_name', )

        sender_details = dict(name='test_sender_name',
                              email='test_sender_email@gmail.com')

        email_interface = Email(recipient_details, sender_details, message_id='12345')
        self.assertEqual(email_interface.send(), True)

    def get_response_data(self, data):
        """
        Does a post API call and returns the data
        :param credentials:
        :return:
        """
        return self.client.post(self.send_mail_endpoint, data, format='json')

    def test_forgot_pw_mail_empty_data(self):
        """
        Tests forgot password mail with no data
        :return:
        """
        request_data = dict()

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['mail_recipient'], MOCK_RESPONSE['mail_password'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    def test_forgotpw_mail_empty_domain(self):
        """
        Tests forgot password mail with no domain
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", user_email="test-user@test.com",
                                              sender_email="test-sender@test.com"))

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add domain name in the body")

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['mail_recipient'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    def test_mail_empty_user_email(self):
        """
        Tests forgot password mail with no user email
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", domain="test-domain.com",
                                              sender_email="test-sender@test.com"))

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the user email in the body")

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['mail_recipient'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    def test_mail__empty_sender_email(self):
        """
        Tests forgot password mail with no admin email
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", user_email="test-user@test.com",
                                              domain="test-domain.com"))

        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the sender email in the body")

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[None, None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_email_id_not_found(self):
        """
        Tests when the passed email id is not in the db
        :return:
        """
        request_data = dict(data_to_post=dict(username="John", user_email="test-user@test.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com",
                                              user_template_id="12"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Email ID not found in the database")

    @mock.patch(EMAIL_SEND, mock.Mock(return_value=True))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['mail_recipient'], MOCK_RESPONSE['mail_password'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_mail_success(self):
        """
        Tests forgot password mail success
        :return:
        """
        request_data = dict(data_to_post=dict(username="Masroor",
                                              user_email="masroor.h@hashedin.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com",
                                              user_template_id="12345"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 200)
        self.assertEqual(response_data.data, "Email sent successfully")

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['mail_recipient'], MOCK_RESPONSE['mail_password'])))
    @mock.patch(EMAIL_SEND, mock.Mock(return_value=False))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_mail_failed(self):
        """
        Tests forgot password mail unsuccessful
        :return:
        """
        request_data = dict(data_to_post=dict(username="Masroor",
                                              user_email="masroor.h@hashedin.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com",
                                              user_template_id="12345"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 500)
        self.assertEqual(response_data.data, "Email to user not sent successfully")

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['mail_recipient'], MOCK_RESPONSE['mail_password'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_forgotpw_empty_template_id(self):
        """
        Tests forgot password mail with no template id
        :return:
        """
        request_data = dict(data_to_post=dict(username="Masroor",
                                              user_email="masroor.h@hashedin.com",
                                              sender_email="test-sender@test.com",
                                              domain="test-domain.com"))
        response_data = self.get_response_data(request_data)
        self.assertEqual(response_data.status_code, 400)
        self.assertEqual(response_data.data, "Please add the template id in the body")
