"""
Test cases for API to fetch days
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status, response_data
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import GET_DAYS, EXTRA_SET
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import USER_STATUS


class TestGetDays(APITestCase):
    """
    Testcase for trivia web days.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.auth = "test_auth_header"

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    def test_user_get_days(self):
        """
        Test trivia web get API.
        """
        url = "{0}?extra-set={1}".format(GET_DAYS, True)
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    def test_user_get_days_extra(self):
        """
        Test whether the response being returned has the extra set
        """
        url = "{0}?extra-set={1}".format(GET_DAYS, True)
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        check_for_extra_set = len(list(filter \
                                           (lambda x: x['name'] == EXTRA_SET, \
                                            response_data(response)))) > 0
        self.assertTrue(check_for_extra_set)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    def test_user_get_days_wo_extra(self):
        """
        Test whether the response being returned doesn't has the extra set
        """
        response = self.client.get(GET_DAYS, HTTP_AUTHORIZATION=self.auth, format='json')
        check_for_extra_set = len(list(filter \
                                           (lambda x: x['name'] == EXTRA_SET, \
                                            response_data(response)))) > 0
        self.assertFalse(check_for_extra_set)
