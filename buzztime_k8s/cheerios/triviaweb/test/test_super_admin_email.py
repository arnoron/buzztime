"""
Test cases for API to fetch days
"""
import itertools

import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import SUPER_ADMIN_EMAIL
from cheerios.triviaweb.test.test_data import get_sa_email_data, \
    duplicate_objects, USER_STATUS
from cheerios.triviaweb.utils.utils import ListUserObjectsUnique
from .response_data import MOCK_RESPONSE

MOCK_EMAIL_SEND = 'cheerios.services.Email.email.Email.send'


def mocked_fn(_):
    """
    Mock function
    """
    pass


class TestSAEmailViewSet(APITestCase):
    """
    Testcase for trivia web super admin
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.player_id = "12345"
        self.auth = "test-auth-header"

    def test_super_admin_wo_params(self):
        """
        Test trivia web get API with correct credentials
        """
        url = "{0}?{1}".format(SUPER_ADMIN_EMAIL, "")
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['user_cred_email'],
                    MOCK_RESPONSE['email_user'],
                    MOCK_RESPONSE['roles'],
                    MOCK_RESPONSE['all_user'],
                    MOCK_RESPONSE['profile'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(MOCK_EMAIL_SEND, mock.Mock(return_value=True))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_registration_email(self):
        """
        Test super-admin email API with registration
        """
        # Get the params from a dictionary. The below line converts everything
        # inside the dict to a string in the required format
        params = '&'.join(["{0}={1}".format(key, val) for key, val in get_sa_email_data().items()])
        # Add the new_registration flag
        params = "%s&new_registration=true" % params
        url = "{0}?{1}".format(SUPER_ADMIN_EMAIL, params)
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['user_cred_email'],
                    MOCK_RESPONSE['email_user'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(MOCK_EMAIL_SEND, mock.Mock(return_value=True))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_super_admin_email(self):
        """
        Test super-admin email API by sending correct inputs
        """
        # Get the params from a dictionary. The below line converts everything
        # inside the dict to a string in the required format
        params = '&'.join(["{0}={1}".format(key, val) for key, val in get_sa_email_data().items()])
        url = "{0}?{1}".format(SUPER_ADMIN_EMAIL, params)
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    def test_unique_class(self):
        """
        Test the unique class to check data where keys can be removed
        """
        _user_list = duplicate_objects()
        _user_list = list(itertools.chain(_user_list, _user_list))
        self.assertEqual(len(_user_list), 6)
        # Run the class and check if the duplicates are removed
        _user_list = list(set([ListUserObjectsUnique(i, 'UserName') for i in _user_list]))
        self.assertEqual(len(_user_list), 3)
