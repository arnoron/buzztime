"""
Test cases for Draft Question API
"""
import mock
from django.http.cookie import SimpleCookie
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status, response_data
from cheerios.services.redis import RedisSession
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import DRAFT_QUESTION_API, DRAFT_DATA, DRAFT_QUESTION
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import mocked_question_draft, USER_STATUS


class TestDraftAPI(APITestCase):
    """
    Testcase for trivia draft Questions.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.player_id = 12345
        self.auth = "test_auth_header"
        self.draft_brand_id = 0
        self.client.cookies = SimpleCookie({'player_id': self.player_id,
                                            'brandID': self.draft_brand_id})

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None]))
    def test_user_get_draft(self):
        """
        Test trivia draft get API.
        """
        location_url = "{0}?player_id={1}&brandID={2}".format(DRAFT_QUESTION_API, \
                                                              self.player_id, self.draft_brand_id)
        response = self.client.get(location_url, HTTP_AUTHORIZATION=self.auth, format='json')

        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=[None]))
    def test_get_draft_failure(self):
        """
        Test trivia draft API without user credentials
        """
        response = self.client.get(DRAFT_QUESTION_API, format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_post_draft_success(self):
        """
        Test trivia draft post API
        """
        _data = mocked_question_draft()
        _data['player_id'] = self.player_id
        response = self.client.post(DRAFT_QUESTION_API, HTTP_AUTHORIZATION=self.auth, \
                                    data=_data, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.triviaweb.views.question_draft.DraftViewSet.fetch_user_draft',
                mock.Mock(side_effect=DRAFT_DATA))
    def test_put_draft_success(self):
        """
        Test trivia to update the draft in a question set
        """
        url = "%s?draft_id=0" % (DRAFT_QUESTION_API,)
        _data = mocked_question_draft()
        _data['id'] = 2
        _data['player_id'] = self.player_id
        # Call the url and put the new object in it
        response = self.client.put(url, HTTP_AUTHORIZATION=self.auth, \
                                   data=_data, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        # Call the GET API to check if the save occured properly
        draft_url = "{0}?player_id={1}&brandID={2}".format(DRAFT_QUESTION_API, \
                                                           self.player_id, self.draft_brand_id)
        response = self.client.get(draft_url, HTTP_AUTHORIZATION=self.auth, format='json')
        data = response_data(response)
        self.assertTrue(len(data) > 0)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_validation_draft(self):
        """
        Test trivia for validation draft
        """
        response = self.client.post(DRAFT_QUESTION_API, HTTP_AUTHORIZATION=self.auth, \
                                    data={}, format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1, 1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"), [],
                                 [],
                                 "{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 DRAFT_QUESTION]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_delete_draft(self):
        """
        Test trivia to delete a draft
        """
        # First save the length of the request after posting a new draft
        _data = mocked_question_draft()
        _data['player_id'] = self.player_id
        response = self.client.post(DRAFT_QUESTION_API, HTTP_AUTHORIZATION=self.auth, \
                                    data=_data, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        response = self.client.get(DRAFT_QUESTION_API, HTTP_AUTHORIZATION=self.auth, format='json')
        _prev_data_len = len(response_data(response)) if len(response_data(response)) != 0 else 1

        _delete_data = {'StumpQuestionSetID': 0, 'branding_id': "0", "player_id": self.player_id}
        response = self.client.delete(DRAFT_QUESTION_API, HTTP_AUTHORIZATION=self.auth, \
                                      data=_delete_data, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        # Compare the data length
        response = self.client.get(DRAFT_QUESTION_API, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(len(response_data(response)), _prev_data_len - 1)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value', mock.Mock(side_effect=[[]]))
    def test_remove_redis_entry(self):
        """
        Function to tear down the users
        """
        redis_key = "{0}_question_{1}".format(self.player_id, self.draft_brand_id)
        redis_obj = RedisSession()
        redis_obj.remove_keys(key=redis_key)
        draft_url = "{0}?player_id={1}&brandID={2}".format(DRAFT_QUESTION_API, \
                                                           self.player_id, self.draft_brand_id)
        response = self.client.get(draft_url, HTTP_AUTHORIZATION=self.auth, format='json')
        data = response_data(response)
        self.assertTrue(len(data) == 0)
