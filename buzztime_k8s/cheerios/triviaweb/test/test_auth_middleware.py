"""
Classes to test middle ware.
"""
from django.test.testcases import TestCase
from mock import Mock, mock
from cheerios.authenticationmiddleware import SessionManagement
from cheerios.analytics.test_data import response_status
from cheerios.services.redis import RedisSession
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import SUPER_ADMIN

AUTH_TOKEN = '123456789'
QUESTION_URL = '/triviaweb/question/?enterprise_id=1'

class TestAuthMiddleware(TestCase):
    """
    Testing Authentication middle ware.
    """
    def setUp(self):
        """
        Setup test middleware and keep the test variable as an instance of
        sessionManagement so that we can make all calls to the request and response
        """
        self.test = SessionManagement()
        self.request = Mock()
        self.request.session = {}

    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_process_request_empty_auth(self):
        """
        Call request without authentication and catch the redis
        error as there is nothing to store in the sessions
        """

        self.mock_redis_to_save_session()
        response = self.client.get(SUPER_ADMIN, HTTP_AUTHORIZATION="", format='json')
        self.assertEqual(response.status_code, 403)

    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_process_manual_url_call(self):
        """
        Try passing the session variable manually, this could be any session variable
        , the purpose is to check whether redis is handling the sessions properly
        """
        url = QUESTION_URL
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), 403)

    def get_header(self, auth_token=AUTH_TOKEN):
        """
        Set header for the API call so that we can pass the auth credentials
        as required.
        """
        return {'HTTP_AUTHORIZATION': auth_token}

    def call_to_api(self, url):
        """
        Call API with header
        """
        return self.client.get(url, **self.get_header())

    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_manual_headers_wo_redis(self):
        """
        Try passing headers to the api call and check whether it sends the correct value,
        also try flushing out the keys from redis if they were already set.
        """
        RedisSession().remove_keys(key=AUTH_TOKEN)
        url = QUESTION_URL
        response = self.call_to_api(url)
        self.assertAlmostEqual(response_status(response), 403)

    def mock_redis_to_save_session(self):
        """
        Have to mock this method so as to save the session in redis
        and make a call from the host of enterprise stump
        """
        RedisSession().save_data(key=AUTH_TOKEN, value={'roles':['host'], 'enterprise': '1'})

    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses([])))
    def test_with_correct_credentials(self):
        """
        Test login api and get the correct redis parameters by making
        sure that the redis data is stored accordingly.
        """
        self.mock_redis_to_save_session()
        url = QUESTION_URL
        response = self.call_to_api(url)
        self.assertAlmostEqual(response_status(response), 200)
