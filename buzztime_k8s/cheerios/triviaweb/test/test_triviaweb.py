"""
This file includes test cases to check
trivia web API.
"""
from pymssql import OperationalError

import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status, response_data
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import QUESTION_API, PREFLIGHT_SURVEY_URL, \
    USER_ROLES, API_METHODS, QUESTIONS
from cheerios.triviaweb.test.access_control import TriviaAccessControl
from cheerios.triviaweb.test.test_data import get_tuple_data, question_post_without_question, \
    question_post_without_set_name, \
    question_correct, question_put_incorrect, question_delete_status_zero, \
    question_delete_status_one, question_delete_without_status, \
    question_delete_without_set_id, question_put_edit_correct, survey_put_data, \
    question_with_out_brand, question_wrong_brand, question_put_add_correct, \
    question_put_delete_correct, survey_set_put_add, USER_STATUS
from cheerios.triviaweb.test.test_utils import TestUtilsUnitTestcase
from cheerios.triviaweb.views.trivia_question import QuestionViewSet
from .response_data import MOCK_RESPONSE

COMMIT = 'cheerios.interface.msssql_connection.MSSQLApiView.connection_commit'

SAVE_TO_DB = 'cheerios.triviaweb.views.trivia_question.QuestionViewSet.save_into_db'

POST_QUESTION_SET = 'cheerios.triviaweb.views.trivia_question.QuestionViewSet.post_question_set'

SAVE_SURVEY_SET = 'cheerios.triviaweb.views.trivia_question.QuestionViewSet.save_survey_db'

SAVE_QUESTION_SET = 'cheerios.triviaweb.views.trivia_question.QuestionViewSet.save_into_db'


class MockedQueViewSet():
    """
    Class to mock the location API post and put calls
    """

    def post_question_set(self, *_):
        """
        Check if email exists mocked function
        """
        return 1

    def save_into_db(self, *_):
        """
        Check if email exists mocked function
        """
        pass

    def save_survey_db(self, *_):
        """
        Check if email exists mocked function
        """
        return 1


def mocked_requests(_args, cursor):
    """
    Mocking the method which calls the store procedure so that
    we do not keep on inserting new entries in the database
    """
    if _args and cursor:
        return {}


class TestTriviaWeb(APITestCase):
    """
    Testcase for trivia web get API.
    """

    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Call the login api for authentication of question API.
        """
        header = "test_header"
        self.auth = header

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None]))
    def test_get_api(self):
        """git push origin :the_remote_branch
        Test trivia web get API.
        """
        url = QUESTION_API
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    def test_format_question_data(self):
        """
        Test case with valid data for format_question_data function
        :return:
        """
        question_obj = QuestionViewSet()
        raw_question_obj = get_tuple_data()
        list_data, question_instance_id, _list = question_obj.format_question_data(raw_question_obj)
        self.assertEqual(list_data[0]['category'], raw_question_obj[0]['RoundCategory'])
        self.assertEqual(question_instance_id, 1)

    def test_question_invalid_data(self):
        """
        Test case with valid data for format_question_data function
        :return:
        """
        question_obj = QuestionViewSet()
        list_data, question_instance_id, _list = question_obj.format_question_data('')
        self.assertEqual(list_data, [])
        self.assertEqual(question_instance_id, '')

    def test_question_empty_tuple(self):
        """
        Test case with valid data for format_question_data function
        :return:
        """
        question_obj = QuestionViewSet()
        list_data, question_instance_id, _list = question_obj.format_question_data([])
        self.assertEqual(list_data, [])
        self.assertEqual(question_instance_id, '')

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin']}".encode("utf-8"), None, b'100']))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[MOCK_RESPONSE['questions']]))
    def test_question_list_get_api(self):
        """
        Calling question API to get Question list.
        """
        url = "{0}?StumpQuestionSetID=1".format(QUESTION_API)
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
        self.assertTrue('question' in response.data)
        self.assertTrue('stump_question_set_id' in response.data)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[MOCK_RESPONSE['ent_questions']]))
    def test_question_get_api(self):
        """
        Calling question API to get Question list.
        """
        url = "{0}?enterprise_id=1&day=4".format(QUESTION_API)
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_post_without_question(self):
        """
        Calling post question API without question list
        """
        data = question_post_without_question()
        response = self.client.post(QUESTION_API, data, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_post_without_data(self):
        """
        Calling post question API with empty data
        """
        data = {}
        response = self.client.post(QUESTION_API, data, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_post_without_set_name(self):
        """
        Calling post question API without question set name
        """
        data = question_post_without_set_name()
        response = self.client.post(QUESTION_API, data, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[[{"StumpQuestionID": "1"}], None]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_delete_status_id_zero(self):
        """
        Calling delete question API with status id 0.
        """
        data = question_delete_status_zero()
        response = self.client.delete(QUESTION_API, data, \
                                      HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.connection_commit', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[[{"StumpQuestionID": "1"}], None]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_delete_status_id_one(self):
        """
        Calling delete question API with status id 1.
        """
        data = question_delete_status_one()
        response = self.client.delete(QUESTION_API, data, \
                                      HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.connection_commit', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[[{"StumpQuestionID": "1"}], None]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_delete_empty_set_id(self):
        """
        Calling delete question API without set question set.
        """
        data = question_delete_without_set_id()
        response = self.client.delete(QUESTION_API, data, \
                                      HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.connection_commit', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[[{"StumpQuestionID": "1"}], None]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_delete_empty_status_id(self):
        """
        Calling delete question API without status id.
        """
        data = question_delete_without_status()
        response = self.client.delete(QUESTION_API, data, \
                                      HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)


class TestTriviaWebQuestion(APITestCase):
    """
    Testcase for trivia web questions API.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Call the login api for authentication of question API.
        """

        self.auth = "test-auth-header"

    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    OperationalError)))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_incorrect_question_set(self):
        """
        Calling GET question API on a question set which is not correct
        """
        question_url = "{0}?StumpQuestionSetID={1}".format(QUESTION_API, "abcde")
        response = self.client.get(question_url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_500_INTERNAL_SERVER_ERROR)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_question_with_out_brand(self):
        """
        Calling post question API with out brand id.
        """
        data = question_with_out_brand()
        response = self.client.post(QUESTION_API, data, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_question_wrong_brand(self):
        """
        Calling post question API with wrong brand id.
        """
        data = question_wrong_brand()
        response = self.client.post(QUESTION_API, data, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_question_blank(self):
        """
        Calling post question API with blank questions.
        """
        data = question_correct()
        data['Question'] = ''
        response = self.client.post(QUESTION_API, data, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['ent_questions'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_get_question_api(self):
        """
        Calling GET question API with correct details
        """
        response = self.client.get(QUESTION_API, {"enterprise_id": 1}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[MOCK_RESPONSE['ent_questions'], MOCK_RESPONSE['questions']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 "{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_get_questionset(self):
        """
        Calling GET question set API with correct details
        """
        response = self.client.get(QUESTION_API, {"enterprise_id": 1}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        question_set_id = response.data[0].get('StumpQuestionSetID')
        question_url = "{0}?StumpQuestionSetID={1}".format(QUESTION_API, question_set_id)
        response = self.client.get(question_url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    # @mock.patch('cheerios.triviaweb.views.trivia_question.QuestionViewSet.update_days',
    #             side_effect=mocked_requests)
    # @mock.patch(POST_QUESTION_SET, side_effect=[MockedQueViewSet.post_question_set])
    # @mock.patch(SAVE_SURVEY_SET, side_effect=[MockedQueViewSet.save_survey_db])
    # @mock.patch(SAVE_QUESTION_SET, side_effect=[MockedQueViewSet.save_into_db])
    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"),
    #                                    "test",
    #                                    "{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"),
    #                                    "test", "test"]))
    # @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    # @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
    #             mock.Mock(side_effect=[MOCK_RESPONSE['questions']]))
    # def test_update_days(self, *_):
    #     """
    #     Test Question API for updating days
    #     """
    #     data = question_put_add_correct()
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth, \
    #                                data=data, format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(POST_QUESTION_SET, side_effect=MockedQueViewSet.post_question_set)
    # @mock.patch(SAVE_SURVEY_SET, side_effect=MockedQueViewSet.save_survey_db)
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_question_post_success(self, *_):
    #     """
    #     Test Question API inserting QS
    #     """
    #     response = self.client.post(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                 data=question_correct(), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_question_put_edit_success(self, *_):
    #     """
    #     Test Question PUT API, successful
    #     """
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                data=question_put_edit_correct(), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_question_put_add_success(self, *_):
    #     """
    #     Test Question PUT API, successful
    #     """
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                data=question_put_add_correct(), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)
    #
    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_question_put_delete_success(self, *_):
    #     """
    #     Test Question PUT API for delete, successful
    #     """
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                data=question_put_delete_correct(), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    def test_survey_put_add_failure(self, *_):
        """
        Test Question PUT API for adding a survey, failure
        """
        response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
                                   data=survey_put_data("ADD", False), format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch(SAVE_SURVEY_SET, side_effect=MockedQueViewSet.save_survey_db)
    @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    def test_survey_set_put_add_failure(self, *_):
        """
        Test Question PUT API for adding a survey set, failure
        """
        response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
                                   data=survey_set_put_add(False), format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    def test_survey_put_edit_failure(self, *_):
        """
        Test Question PUT API for editing survey, failure
        """
        response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
                                   data=survey_put_data("EDIT", False), format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    def test_survey_put_delete_failure(self, *_):
        """
        Test Question PUT API for deleting survey, failure
        """
        response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
                                   data=survey_put_data("DELETE", False), format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_survey_put_add_success(self, *_):
    #     """
    #     Test Question PUT API for adding survey, successful
    #     """
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                data=survey_put_data("ADD", True), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(SAVE_SURVEY_SET, side_effect=MockedQueViewSet.save_survey_db)
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_survey_set_put_add_success(self, *_):
    #     """
    #     Test Question PUT API for adding survey set, successful
    #     """
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                data=survey_set_put_add(True), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_survey_put_edit_success(self, *_):
    #     """
    #     Test Question PUT API for editing survey, successful
    #     """
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                data=survey_put_data("EDIT", True), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #
    #                              b'100']))
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_survey_put_delete_success(self, *_):
    #     """
    #     Test Question PUT API for deleting survey, successful
    #     """
    #     response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
    #                                data=survey_put_data("DELETE", True), format='json')
    #     self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 b'100']))
    def test_question_put_failure(self, *_):
        """
        Test Question PUT API, unsuccessful
        """
        response = self.client.put(QUESTION_API, HTTP_AUTHORIZATION=self.auth,
                                   data=question_put_incorrect(), format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    # def test_forbidden_get(self):
    #     """
    #     Calling GET question API for SAget
    #     """
    #     obj = TriviaAccessControl()
    #     obj.forbidden_request(self, QUESTION_API, API_METHODS["GET"], \
    #                           [USER_ROLES['SUPER_ADMIN']], {"enterprise_id": 1})

    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_forbidden_put(self, *_):
    #     """
    #     Test Question PUT API for Host
    #     """
    #     obj = TriviaAccessControl()
    #     obj.forbidden_request(self, QUESTION_API, API_METHODS["PUT"], \
    #                           [USER_ROLES['HOST']], question_put_add_correct())
    #
    # @mock.patch(POST_QUESTION_SET, side_effect=MockedQueViewSet.post_question_set)
    # @mock.patch(SAVE_SURVEY_SET, side_effect=MockedQueViewSet.save_survey_db)
    # @mock.patch(SAVE_QUESTION_SET, side_effect=MockedQueViewSet.save_into_db)
    # def test_forbidden_post(self, *_):
    #     """
    #     Test Question API POST for Host
    #     """
    #     obj = TriviaAccessControl()
    #     obj.forbidden_request(self, QUESTION_API, API_METHODS["POST"], \
    #                           [USER_ROLES['HOST']], question_correct())

    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['ent_questions'], MOCK_RESPONSE['questions'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
                                 "{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"),
                                 "{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"),
                                 QUESTIONS]))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[MOCK_RESPONSE['questions']]))
    def test_fun_facts(self):
        """
        Test Question GET to check if fun-facts are returned in the response
        """
        resp = self.client.get(QUESTION_API, {"enterprise_id": 1}, \
                               HTTP_AUTHORIZATION=self.auth, format='json')
        _data = response_data(resp)
        # Empty lists/dicts evaluate to False
        self.assertTrue(_data)

        question_set_id = _data[0].get('StumpQuestionSetID')
        # Call the api to fetch the question's data
        question_url = "{}?StumpQuestionSetID={}".format(QUESTION_API, question_set_id)
        response = self.client.get(question_url, HTTP_AUTHORIZATION=self.auth, format='json')
        # Check if response was successful
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        # Check for fun-fact field
        _ques_data = response_data(response)
        self.assertEqual(1, len(list(filter(lambda x: 'fun_fact' in x, _ques_data))))

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise': 1}".encode("utf-8"), None,
    #                              b'100']))
    # @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    # @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
    #             mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
    #                 [{'StumpQuestionSetID': 158}])))
    # def test_incorrect_fun_fact_save(self):
    #     """
    #     Test case for validating the fun fact limit
    #     """
    #     _data = question_correct()
    #     # Adding an incorrect length for fun-fact
    #     _data['Question'][0]['fun_fact'] = '*' * 151
    #     resp = self.client.post(QUESTION_API, HTTP_AUTHORIZATION=self.auth, data=_data, \
    #                             format='json')
    #     self.assertEqual(response_status(resp), status.HTTP_400_BAD_REQUEST)
    #     # Check for value error as we cannot use context managers due to custom errors
    #     self.assertTrue('ValueError' in response_data(resp))

    # @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    # @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
    #             mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
    #                 [{'StumpQuestionSetID': 158}],
    #                 [{'StumpSurveyID': 121}],
    #                 MOCK_RESPONSE['posted_response'])))
    # @mock.patch(SAVE_TO_DB, mock.Mock(return_value=None))
    # @mock.patch(COMMIT, mock.Mock())
    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(
    #                 side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
    #                              None]))
    # @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    # @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    # def test_correct_fun_fact_save(self):
    #     """
    #     Test case for validating the fun fact limit
    #     """
    #     _data = question_correct()
    #     # Adding an correct length for fun-fact
    #     _data['Question'][0]['fun_fact'] = '*' * 10
    #     resp = self.client.post(QUESTION_API, HTTP_AUTHORIZATION=self.auth, data=_data, \
    #                             format='json')
    #     self.assertEqual(response_status(resp), status.HTTP_200_OK)


class TestTeam(APITestCase):
    """
    Testcase for trivia web get API.
    """

    def test_game_incomplete_data(self):
        """git push origin :the_remote_branch
        Test trivia web get API.
        """
        data = {}
        url = "/triviaweb/team/"
        post_header = TestUtilsUnitTestcase().get_headers_for_api()
        post_header['format'] = 'json'
        self.client.post(url, data, **post_header)


# class TestSurvey(APITestCase):
#     """
#     Testcase for trivia web get API.
#     """
#
#     @mock.patch(mock_test_utils.CONNECTION_MANAGER,
#                 mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
#                     MOCK_RESPONSE['survey_questions'])))
#     def test_survey_get_api(self):
#         """git push origin :the_remote_branch
#         Test trivia web get API.
#         """
#         response = self.client.get(PREFLIGHT_SURVEY_URL)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
#
#     @mock.patch(mock_test_utils.CONNECTION_MANAGER,
#                 mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
#                     MOCK_RESPONSE['survey_questions'])))
#     def test_survey_get_question(self):
#         """
#         Test trivia web get API.
#         """
#         url = "{0}?StumpQuestionSetID=1".format(PREFLIGHT_SURVEY_URL)
#         response = self.client.get(url)
#         self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)
