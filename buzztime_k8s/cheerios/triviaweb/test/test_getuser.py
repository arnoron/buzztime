"""
Test cases for get users
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase
from cheerios.analytics.test_data import response_status, response_data
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import GET_USER, LOGIN_URL
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import post_login_data, USER_STATUS


class TestGetUser(APITestCase):
    """
    Testcase for trivia web Enterprise.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.player_id = "12345"
        self.auth = "test_auth_header"

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['get_user'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    def test_user_get_api(self):
        """git push origin :the_remote_branch
        Test trivia web get API.
        """
        url = "{0}?user_id={1}".format(GET_USER, self.player_id)
        response = self.client.get(url, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
