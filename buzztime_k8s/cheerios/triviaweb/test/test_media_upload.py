"""
Test case for media_questions.py
"""
import mock
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status
from cheerios.triviaweb.test.test_data import get_file_data
from cheerios.triviaweb.views.media_questions import MediaViewSet


class TestMediaUpload(APITestCase):
    """
    Test the media upload.
    """

    def setUp(self):
        """
        Setup the test cases
        """
        self.auth = "test-auth-header"
        self.url = "/triviaweb/media-questions/"

    @mock.patch('django.core.files.storage.Storage.save', mock.Mock(
        return_value='abc/image/ewru90324kncskd/round1/question1/test_file.jpg'))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_post_file(self):
        """
        Test the post request when user is valid and file is provided.
        """
        test_file = SimpleUploadedFile("test_file.jpg", b" ")
        data = get_file_data(test_file)
        response = self.client.post(self.url, data, HTTP_AUTHORIZATION=self.auth)
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_format_filename(self):
        """
        Test the file name with #,% getting replaced.
        """
        obj = MediaViewSet()
        file_name = obj.create_filename('test.%%#image.png', 'round1', 'question1')
        self.assertEqual(file_name, "test.image.png")

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_format_filename_all_sp(self):
        """
        Test the file name only .,%,# get replaced with round and question number.
        """
        obj = MediaViewSet()
        file_name = obj.create_filename('%%#.png', 'round1', 'question1')
        self.assertEqual(file_name, "round1question1.png")
