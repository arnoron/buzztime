"""
MOCKED RESPONSE
"""
import datetime

MOCK_RESPONSE = dict(
    result_username=[
        {
            'PlayerID': 30302646,
            'Username': 'shubhamsingh',
            'EmailAddress': 'shubham.singh@hashedin.com'
        }
    ],
    user_cred=[
        {
            'PlayerID': 30302646,
            'Password': 'be48f351245afc7a426f10e3aedcfa6b',
            'Salt': 't86'
        }
    ],
    check_host=[
        {
            'StumpTriviaUserStatusID': 1,
            'PlayerID': 30302646,
            'FirstName': 'Shubham',
            'LastName': 'Singh',
            'Email': 'shubham.singh@hashedin.com',
            'PhoneNumber': '12345454546',
            'StumpTriviaBrandingID': 2,
            'UserName': 'shubhamsingh'
        }
    ],
    white_labelling=[
        {
            'URLPath': 'MOT',
            'StumpTriviaBrandingName': 'MyOwnTrivia',
            'FooterColor': '#3e3c3e',
            'Logo': 'MOT_4472588011673954053.png',
            'BackgroundImage': '#dedac7',
            'HeaderColor': 'rgba(177, 92, 187, 1)',
            'StumpTriviaBrandingID': 2
        }
    ],
    user_roles=[
        {
            'StumpTriviaRoleDesc': 'Group-Admin',
            'PlayerID': 30302646
        },
        {
            'StumpTriviaRoleDesc': 'Host',
            'PlayerID': 30302646
        },
        {
            'StumpTriviaRoleDesc': 'Super-Admin',
            'PlayerID': 30302646
        }
    ],
    status=[
        {
            'StumpTriviaUserStatusDesc': 'Active',
            'StumpTriviaUserStatusID': 1
        },
        {
            'StumpTriviaUserStatusDesc': 'Deactivated',
            'StumpTriviaUserStatusID': 0
        },
        {
            'StumpTriviaUserStatusDesc': 'Pending',
            'StumpTriviaUserStatusID': 2
        },
        {
            'StumpTriviaUserStatusDesc': 'Wait',
            'StumpTriviaUserStatusID': 3
        }
    ],
    questions=[
        {
            'Days': '7',
            'Question': 'What word, which helps opens pores and get rid of Toxins, is the only'
                        ' commonly used word in the English language that comes from the '
                        'Finnish language?',
            'QuestionSetName': 'Stump Mobile Trivia Alpha Question Set',
            'RoundName': 'Tie',
            'MediaType': None,
            'QuestionNumber': 1,
            'RoundNumber': 12,
            'QuestionSetCreateDate': datetime.datetime(2017, 4, 4, 16, 41, 32, 973000),
            'RoundCategory': 'Language',
            'StumpQuestionSetID': 1,
            'StumpQuestionID': 4471,
            'RoundCategoryDescription': 'Language Description',
            'ExtraQuestionSetIndicator': 0,
            'CorrectAnswer': 'Sauna',
            'MediaPath': None,
            'StumpQuestionType': 'Open',
            'OriginalStumpQuestionSetID': 1,
            'IsParentQuestionSet': True
        }
    ],
    ent_questions=[
        {
            'StatusID': 1,
            'CreateDate': datetime.datetime(2018, 3, 12, 13, 27, 53, 753000),
            'Days': '2',
            'StumpQuestionSetID': 85,
            'StumpGame': 'Stump Mobile',
            'ExtraQuestionSetIndicator': 1,
            'StumpTriviaBrandingID': 1,
            'QuestionCount': 54,
            'QuestionSetName': 'Sample question set'
        }
    ],
    posted_response=[
        {
            'StumpSurveyQuestionType': 'Open',
            'StumpSurveyQuestionTypeDescription': 'Question type which requires any string '
                                                  'containing letters, numbers and spaces to be '
                                                  'provided as an answer ',
            'StumpSurveyQuestionTypeID': 1
        },
        {
            'StumpSurveyQuestionType': 'Rating',
            'StumpSurveyQuestionTypeDescription': 'Question type which requires integer value '
                                                  'within 1-10 range to be provided as an answer.',
            'StumpSurveyQuestionTypeID': 2
        },
        {
            'StumpSurveyQuestionType': 'TrueFalse',
            'StumpSurveyQuestionTypeDescription': 'Question type which requires true or false '
                                                  'to be provided as an answer',
            'StumpSurveyQuestionTypeID': 3
        }
    ],
    survey_questions=[
        {
            'StumpSurveyID': 1,
            'StumpSurveyQuestionType': 'Open',
            'StumpSurveyQuestionID': 5,
            'SurveyName': 'Stump Mobile Trivia Alpha Survey',
            'SurveyQuestionNumber': 5,
            'StumpQuestionSetID': 1,
            'SurveyQuestion': 'What could we do to make a better digital experience?'
        }
    ],
    user_cred_email=[{
        'StumpTriviaUserStatusID': 1,
        'Email': 'testcase@sharklasers.com',
        'PlayerID': 30302647,
        'StumpTriviaBrandingID': 1,
        'UserName': 'testcase@sharklasers.com',
        'PhoneNumber': None,
        'LastName': 'asd',
        'FirstName': 'Testcase'
    }],
    email_user=[
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 15358765,
            'Email': '15358765@ntn.com',
            'LastName': 'Carney',
            'UserName': 'triviabob',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 0,
            'FirstName': 'Bob'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 22631268,
            'Email': '22631268@ntn.com',
            'LastName': '05',
            'UserName': 'Hasher05',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 1,
            'FirstName': 'Hasher'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '1234567890',
            'PlayerID': 23021455,
            'Email': 'priyanka.sah@hashedin.com',
            'LastName': 'Sah.',
            'UserName': 'Priyankasah',
            'StumpTriviaBrandingID': 2,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 0,
            'FirstName': 'Priyanka'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '12345454546',
            'PlayerID': 30302646,
            'Email': 'shubham.singh@hashedin.com',
            'LastName': 'Singh',
            'UserName': 'shubhamsingh',
            'StumpTriviaBrandingID': 2,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 0,
            'FirstName': 'Shubham'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 30302647,
            'Email': 'testcase@sharklasers.com',
            'LastName': 'asd',
            'UserName': 'testcase@sharklasers.com',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 0,
            'FirstName': 'Testcase'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '123456789012345',
            'PlayerID': 30302689,
            'Email': 'sonzgeorge@gmail.com',
            'LastName': 'George',
            'UserName': 'sonzgeorge',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 0,
            'FirstName': 'Soneyy'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 30302700,
            'Email': 'pooja.lodhi@hashedin.com',
            'LastName': 'pooja',
            'UserName': 'pooja',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 0,
            'FirstName': 'pooja'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 30302744,
            'Email': 'ravi.mehta@hashedin.com',
            'LastName': 'mehta',
            'UserName': 'ravi.mehta@hashedin.com',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'AdminNotificationFlag': 0,
            'FirstName': 'Ravi'
        }
    ],
    profile=[
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.92)',
            'StumpTriviaBrandingName': 'Stump Trivia',
            'URLPath': '',
            'HeaderColor': 'rgba(119, 78, 245, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3ee',
            'StumpTriviaBrandingID': 1
        },
        {
            'BackgroundImage': '#dedac7',
            'StumpTriviaBrandingName': 'MyOwnTrivia',
            'URLPath': 'MOT',
            'HeaderColor': 'rgba(177, 92, 187, 1)',
            'Logo': 'MOT_4472588011673954053.png',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 2
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 1)',
            'StumpTriviaBrandingName': 'Stump Holiday Quiz 2017',
            'URLPath': 'stump2017',
            'HeaderColor': 'rgba(24, 179, 76, 1)',
            'Logo': 'stump2017_2320538826972816223.png',
            'FooterColor': 'rgba(213, 28, 28, 1)',
            'StumpTriviaBrandingID': 3
        },
        {
            'BackgroundImage': 'rgba(201, 197, 184, 0)',
            'StumpTriviaBrandingName': 'Hashedin',
            'URLPath': 'HI',
            'HeaderColor': 'rgba(0, 1, 12, 1)',
            'Logo': 'HI_8657616559789286602.png',
            'FooterColor': 'rgba(7, 7, 7, 1)',
            'StumpTriviaBrandingID': 4
        },
        {
            'BackgroundImage': 'rgba(44, 81, 162, 0)',
            'StumpTriviaBrandingName': 'American Multi-Cinema',
            'URLPath': 'AMC',
            'HeaderColor': 'rgba(0, 10, 14, 1)',
            'Logo': 'default_5282993630939039161.png',
            'FooterColor': 'rgba(62, 60, 60, 1)',
            'StumpTriviaBrandingID': 5
        },
        {
            'BackgroundImage': 'rgba(201, 197, 184, 0)',
            'StumpTriviaBrandingName': 'DEMO',
            'URLPath': 'DEMO',
            'HeaderColor': 'rgba(0, 0, 0, 1)',
            'Logo': 'default_2606528248451088564.png',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 6
        },
        {
            'BackgroundImage': 'rgba(201, 197, 184, 0)',
            'StumpTriviaBrandingName': 'Union Cafe',
            'URLPath': 'Unioncafe',
            'HeaderColor': 'rgba(0, 2, 9, 1)',
            'Logo': 'default_4267793689213766783.png',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 7
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'StumpTriviaBrandingName': 'jjj',
            'URLPath': 'test',
            'HeaderColor': 'rgba(102, 48, 254, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 8
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'StumpTriviaBrandingName': 'tesre',
            'URLPath': 'tesre',
            'HeaderColor': 'rgba(196, 206, 134, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 9
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'StumpTriviaBrandingName': 'Test123',
            'URLPath': 'Test123',
            'HeaderColor': 'rgba(102, 48, 254, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 10
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'StumpTriviaBrandingName': 'Cafe Passuci',
            'URLPath': 'CPS',
            'HeaderColor': 'rgba(102, 48, 254, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 11
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'StumpTriviaBrandingName': 'roo',
            'URLPath': 'rooo',
            'HeaderColor': 'rgba(102, 48, 254, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 14
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'StumpTriviaBrandingName': 'kool',
            'URLPath': 'tytt',
            'HeaderColor': 'rgba(102, 48, 254, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 15
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'StumpTriviaBrandingName': 'Testbrandfive',
            'URLPath': 'TestKeyy5',
            'HeaderColor': 'rgba(102, 48, 254, 1)',
            'Logo': 'default_3471621209539921843.png',
            'FooterColor': '#3e3c3e',
            'StumpTriviaBrandingID': 17
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.92)',
            'StumpTriviaBrandingName': 'roar',
            'URLPath': 'roar',
            'HeaderColor': 'rgba(119, 78, 245, 1)',
            'Logo': 'None',
            'FooterColor': '#3e3c3ee',
            'StumpTriviaBrandingID': 18
        },
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.92)',
            'StumpTriviaBrandingName': 'bb',
            'URLPath': 'kkk',
            'HeaderColor': 'rgba(119, 78, 245, 1)',
            'Logo': 'None',
            'FooterColor': 'rgba(40, 38, 38, 1)',
            'StumpTriviaBrandingID': 19
        }],
    all_user=[
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '',
            'PlayerID': 11395326,
            'Email': 'jsanders@ntn.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'jsand',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': '', 'FirstName': ''
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '',
            'PlayerID': 22082582,
            'Email': 'jason.kaul@buzztime.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'BRAD2719',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': '', 'FirstName': ''
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '9538987257',
            'PlayerID': 22378626,
            'Email': 'soney.george@hashedin.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'HASHER_007',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'George',
            'FirstName': 'Soneygeorge'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '',
            'PlayerID': 22378627,
            'Email': 'pansul.bhatt@hashedin.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'AZHAR_AM',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': '', 'FirstName': ''
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 23083127,
            'Email': '23083127@ntn.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'STUMPGA',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'Group Admin',
            'FirstName': 'Stump'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '6178949324',
            'PlayerID': 23223641,
            'Email': '23223641@ntn.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'JonathanKrieger',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'Krieger',
            'FirstName': 'Jonathan'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': '5555555555',
            'PlayerID': 23233209,
            'Email': '23233209@ntn.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'Bingo Tree Limbs',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'Doe',
            'FirstName': 'John'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 30302647,
            'Email': 'testcase@sharklasers.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'testcase@sharklasers.com',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'asd',
            'FirstName': 'Testcase'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 30302700,
            'Email': 'pooja.lodhi@hashedin.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'pooja',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'pooja',
            'FirstName': 'pooja'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 30302744,
            'Email': 'ravi.mehta@hashedin.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'ravi.mehta@hashedin.com',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'mehta',
            'FirstName': 'Ravi'
        },
        {
            'StumpTriviaUserStatusID': 1,
            'PhoneNumber': None,
            'PlayerID': 30302810,
            'Email': 'dwarika.pandey@hashedin.com',
            'StumpTriviaRoleID': 2,
            'UserName': 'dwarika',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaRoleDesc': 'Group-Admin',
            'LastName': 'Pandey',
            'FirstName': 'Dwarika'
        }
    ],
    roles=[
        {
            'StumpTriviaRoleID': 2,
            'StumpTriviaRoleDesc': 'Group-Admin'
        },
        {
            'StumpTriviaRoleID': 1,
            'StumpTriviaRoleDesc': 'Host'
        },
        {
            'StumpTriviaRoleID': 3,
            'StumpTriviaRoleDesc': 'Super-Admin'
        }
    ],
    super_admin=[
        {
            'Email': 'ravi.mehta@hashedin.com',
            'LastName': 'mehta',
            'UserName': 'ravi.mehta@hashedin.com',
            'PlayerID': 30302744,
            'AdminNotificationFlag': 0,
            'PhoneNumber': None,
            'FirstName': 'Ravi',
            'StumpTriviaUserStatusID': 1,
            'StumpTriviaRoleDesc': 'Super-Admin',
            'StumpTriviaBrandingID': 1
        }
    ],
    get_paticular_status=[
        {
            'StumpTriviaUserStatusID': 1,
            'StumpTriviaUserStatusDesc': 'Active'
        },
        {
            'StumpTriviaUserStatusID': 0,
            'StumpTriviaUserStatusDesc': 'Deactivated'
        },
        {
            'StumpTriviaUserStatusID': 2,
            'StumpTriviaUserStatusDesc': 'Pending'
        },
        {
            'StumpTriviaUserStatusID': 3,
            'StumpTriviaUserStatusDesc': 'Wait'
        }
    ],
    bad_words=b"{'dirty_words':['SATAN'], 'exceptions':['test']}",
    username_not_exists=[
        {
            'UserNameExists': 0
        }
    ],
    email_not_exists=[
        {
            'EmailExists': 0
        }
    ],
    profile_pin=[
        {
            'PIN': '999689@',
            'PlayerID': 30302929
        }
    ],
    username_exists=[
        {
            'UserNameExists': 1
        }
    ],
    email_exists=[
        {
            'EmailExists': 0
        }
    ],
    locations=[
        {
            'CreateDate': datetime.datetime(2018, 4, 17, 2, 58, 19, 273000),
            'Days': '567',
            'PostalCode': '560102',
            'StatusID': 1,
            'Address1': '1289/1090E, 1st Floor, 18th Cross Road, Sector 3,',
            'SiteName': 'kirusa',
            'Address2': 'None',
            'State': 'Karnataka',
            'StumpTriviaBrandingID': 1,
            'City': 'Bengaluru',
            'Country': 'US',
            'UpdateDate': datetime.datetime(2018, 4, 17, 2, 58, 57, 597000),
            'StartTime': '15:26',
            'StumpSiteID': 76
        }
    ],
    group_admins=[
        {
            'PlayerID': 11395326,
            'Email': 'jsanders@ntn.com',
            'UserName': 'jsand',
            'StumpTriviaRoleDesc': 'Host',
            'StumpTriviaRoleID': 1,
            'FirstName': '',
            'StumpTriviaUserStatusID': 1,
            'LastName': '',
            'StumpTriviaBrandingID': 1,
            'PhoneNumber': ''
        }
    ],
    get_user=[
        {
            'StumpTriviaBrandingID': 1,
            'FirstName': 'Ntn',
            'LastName': '',
            'PhoneNumber': '',
            'PlayerID': 22627958,
            'StumpTriviaUserStatusID': 1,
            'Email': '22627958@ntn.com',
            'username': 'Hasher16'
        }
    ],
    theme=[
        {
            'BackgroundImage': 'rgba(255, 255, 255, 0.57)',
            'FooterColor': '#3e3c3e',
            'HeaderColor': 'rgba(102, 48, 254, 1)',
            'StumpTriviaBrandingName': 'jjj',
            'StumpTriviaBrandingID': 8,
            'URLPath': 'test',
            'Logo': 'None'
        }
    ],
    data_reporting_empty=[
        {
            'GameCount': 0,
            'StumpTriviaBrandingID': 18,
            'StumpTriviaBrandingName': 'roar'
        },
        {
            'GameCount': 0,
            'StumpTriviaBrandingID': 19,
            'StumpTriviaBrandingName': 'bb'
        }
    ],
    dr_host_data=[
        {
            'PlayerID': 30302954,
            'GameCount': 0,
            'StumpTriviaBrandingName': 'Stump Trivia',
            'FirstName': 'Romit',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaUserStatusDesc': 'Wait',
            'IsGroupAdmin': 0,
            'LastName': 'Test'
        }
    ],
    dr_without_branding_id=[
        {
            'StumpTriviaBrandingID': 18,
            'StumpTriviaBrandingName': 'roar',
            'GameCount': 0
        },
        {
            'StumpTriviaBrandingID': 19,
            'StumpTriviaBrandingName': 'bb',
            'GameCount': 0
        }
    ],
    dr_with_banding_id=[
        {
            'PlayerID': 30302954,
            'GameCount': 0,
            'StumpTriviaBrandingName': 'Stump Trivia',
            'FirstName': 'Romit',
            'StumpTriviaBrandingID': 1,
            'StumpTriviaUserStatusDesc': 'Wait',
            'IsGroupAdmin': 0,
            'LastName': 'Test'
        }
    ],
    ent_get_api=[
        {
            'StumpTriviaBrandingID': 19,
            'HeaderColor': 'rgba(119, 78, 245, 1)',
            'Logo': 'None',
            'StumpTriviaBrandingName': 'bb',
            'FooterColor': 'rgba(40, 38, 38, 1)',
            'URLPath': 'kkk',
            'BackgroundImage': 'rgba(255, 255, 255, 0.92)'
        }
    ],
    mail_recipient=[
        {
            'Username': 'TESTNAME1',
            'PlayerID': 22631264,
            'EmailAddress': 'masroor.h@hashedin.com'
        }
    ],
    mail_password=[
        {
            'Handle': 'TEST N',
            'Username': 'TESTNAME1',
            'Password': '33aabf27747ceba4e885627a7ad02bb3'
        }
    ],
)
