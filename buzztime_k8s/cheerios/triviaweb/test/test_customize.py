"""
This file includes test cases to check
trivia web White labelling api.
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.foodmenu.test.test_data import response_status
from cheerios.services.redis import RedisSession
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import CUSTOMIZE_API, USER_ROLES, API_METHODS
from cheerios.triviaweb.test.access_control import TriviaAccessControl
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import modified_data_value, \
    get_customize_values, USER_STATUS


class TestCustomize(APITestCase):
    """
    Test case for Customization
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Call the login api for authentication of question API.
        """
        self.auth = "test-auth-header"

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['theme'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[b'{"test-key":"test-value"}']))
    def test_get_customize_data_db(self):
        """
        Test GET function after removing data from redis.
        :return:
        """
        domain = "test"
        url = '{0}?domain={1}'.format(CUSTOMIZE_API, domain)
        redis = RedisSession()
        redis.remove_keys(domain)
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['theme'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[b'{"test-key":"test-value"}']))
    def test_get_customize_data(self):
        """
        Test GET function from redis.
        :return:
        """
        url = '{0}?domain=test'.format(CUSTOMIZE_API)
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses([])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['theme'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[b'{"test-key":"test-value"}']))
    def test_get_invalid_enterprise(self):
        """
        Test GET function with invalid enterprise key.
        :return:
        """
        url = '{0}?domain=123'.format(CUSTOMIZE_API)
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['theme'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['theme'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[b'{"test-key":"test-value"}']))
    def test_get_without_parameters(self):
        """
        Test GET function without any parameter.
        :return:
        """
        url = CUSTOMIZE_API
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(MOCK_RESPONSE['theme'])))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[b'{"test-key":"test-value"}']))
    def test_get_without_parameters_db(self):
        """
        Test GET function without any parameter.
        :return:
        """
        url = CUSTOMIZE_API
        redis = RedisSession()
        redis.remove_keys("default")
        response = self.client.get(url)
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['theme'], [], MOCK_RESPONSE['theme']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    def test_post_without_domain(self):
        """
        Test POST api with missing data like domain
        :return:
        """
        data_to_post = modified_data_value("domain", "")
        data_to_post["logo"] = ""
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[None, b'{"test-key":"test-value"}']))
    def test_post_without_auth(self):
        """
        Test POST api without auth header
        :return:
        """
        data_to_post = get_customize_values()
        response = self.client.post(CUSTOMIZE_API, data_to_post, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['theme'], [], MOCK_RESPONSE['theme']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    def test_post_without_id(self):
        """
        Test POST api with out brand id
        :return:
        """
        data_to_post = modified_data_value("id", "")
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['theme'], [], MOCK_RESPONSE['theme']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    def test_post_without_footer(self):
        """
        Test POST api with out footer
        :return:
        """
        data_to_post = modified_data_value("footer", "")
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['theme'], [], MOCK_RESPONSE['theme']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    def test_post_without_header(self):
        """
        Test POST api with out header
        :return:
        """
        data_to_post = modified_data_value("header", "")
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['theme'], [], MOCK_RESPONSE['theme']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    def test_post_without_background(self):
        """
        Test POST api with out background
        :return:
        """
        data_to_post = modified_data_value("background", "")
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('PIL.Image.Image.save', mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[[], MOCK_RESPONSE['theme']]))
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_post_proper_data(self):
        """
        Test POST api with correct data but invalid id.
        Invalid id will not update Db value but it will return 200.
        Needs to be run this on whole. Test data depends on cache created by other cases.
        :return:
        """
        data_to_post = get_customize_values()
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER, mock.Mock(
        side_effect=mock_test_utils.mock_cursor_responses(
            [], MOCK_RESPONSE['theme'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_post_without_logo(self):
        """
        Test POST api with correct data but invalid id.
        Logo is empty.
        Invalid id will not update Db value but it will return 200.
        :return:
        """
        data_to_post = modified_data_value("logo", "")
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['theme'], [], MOCK_RESPONSE['theme']]))
    @mock.patch('cheerios.services.redis.RedisSession.get_key',
                mock.Mock(side_effect=[1, b'{"test-key":"test-value"}']))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'-1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_post_without_logo_db(self):
        """
        Test POST api with correct data but invalid id.
        Logo is empty. Post API called after redis data removed.
        Invalid id will not update Db value but it will return 200.
        :return:
        """
        redis = RedisSession()
        redis.remove_keys("test")
        data_to_post = modified_data_value("logo", "")
        response = self.client.post(CUSTOMIZE_API, data_to_post, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    # def test_forbidden_post(self):
    #     """
    #     Test theme post API for Host
    #     """
    #     obj = TriviaAccessControl()
    #     obj.forbidden_request(self, CUSTOMIZE_API, API_METHODS["POST"], \
    #                           [USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']],
    #                           get_customize_values())
