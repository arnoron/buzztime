"""
This file includes test cases to check
trivia web API.
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import DATA_REPORTING_URL, USER_ROLES, API_METHODS
from cheerios.triviaweb.constants import NO_DATA_FOUND, INVALID_PARAM_ERROR
from cheerios.triviaweb.test.access_control import TriviaAccessControl
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import dr_without_branding_id, dr_with_branding_id, \
    dr_with_host_id, \
    dr_with_host_id_empty_data, \
    dr_with_valid_location_id, dr_with_invalid_location_id, USER_STATUS


class TestTriviaDataReportingGet(APITestCase):
    """
    Testcase for trivia data reporting get API.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.url = DATA_REPORTING_URL
        self.auth = "test-auth-header"

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['data_reporting_empty']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_super_admin_security(self):
        """
        Test trivia web data reporting get api with no data
        """
        response = self.client.get(self.url, {}, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['dr_without_branding_id']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_super_admin_with_date_without_brand_id(self):
        """
        Test trivia web data reporting get api for branding with dates and without brandId
        It returns list of all the the brands
        """
        response = self.client.get(self.url, dr_without_branding_id(),
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[MOCK_RESPONSE['dr_with_banding_id']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_host_with_date_screen(self):
        """
        Test trivia web data reporting get api for host with dates
        """
        response = self.client.get(self.url, dr_with_branding_id(),
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[[]]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_host_details_with_date(self):
        """
        Test trivia web data reporting get api for host-details with dates
        """
        response = self.client.get(self.url, dr_with_host_id(),
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[[]]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_host_details_empty_data(self):
        """
        Test trivia web data reporting get api for host-details with dates
        """
        response = self.client.get(self.url, dr_with_host_id_empty_data(),
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(
        side_effect=[[]]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_host_details_with_valid_location_id(self):
        """
        Test trivia web data reporting get api for host-details with dates
        """
        response = self.client.get(self.url, dr_with_valid_location_id(),
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        if "message" in response.data:
            self.assertEquals(response.data['message'], NO_DATA_FOUND)
        else:
            self.assertEquals(type(response.data[0].LocationID), int)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_host_details_with_invalid_location_id(self):
        """
        Test trivia web data reporting get api for host-details with dates
        """
        response = self.client.get(self.url, dr_with_invalid_location_id(),
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, INVALID_PARAM_ERROR)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['group-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_forbidden_host(self):
        """
        Test trivia web data reporting get api security for Host
        """
        obj = TriviaAccessControl()
        obj.forbidden_request(self, self.url, API_METHODS["GET"], [USER_ROLES['HOST']])
