"""
This file includes test cases to check
trivia web API.
"""
from rest_framework.test import APITestCase
from rest_framework import status
from cheerios.services.redis import RedisSession
from cheerios.triviaweb.test.test_data import mock_test_user
from cheerios.analytics.test_data import response_status


class TriviaAccessControl(APITestCase):
    """
    Testcase for forbidden requests
    """
    def forbidden_request(self, client, url, method, permissions, data={}):
        """
        Common function for testing forbidden access
        """
        mocked_session_id = str(10*mock_test_user())
        mock_test_redis = RedisSession()
        mock_test_redis.save_data(key=mocked_session_id, value={'roles':permissions})
        #API to get host details
        response = getattr(client.client, method.lower())(url, data, \
            HTTP_AUTHORIZATION=mocked_session_id, format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)
