"""
Test cases for API to fetch days
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import SUPER_ADMIN, \
    MSSQL_FETCH_ALL_PATH, API_METHODS, USER_ROLES
from cheerios.triviaweb.test.access_control import TriviaAccessControl
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import USER_STATUS


def mocked_fn(*_):
    """
    Mock function
    """
    pass


class TestSAViewSet(APITestCase):
    """
    Testcase for trivia web super admin
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.player_id = "12345"
        self.auth = "test-auth-header"

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[None, 1]))
    def test_super_admin_forbidden(self):
        """
        Test trivia web get API with incorrect credentials
        """
        response = self.client.get(SUPER_ADMIN, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_403_FORBIDDEN)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['super_admin'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_super_admin_get(self):
        """
        Test trivia web get API with correct credentials
        """
        response = self.client.get(SUPER_ADMIN, HTTP_AUTHORIZATION=self.auth, \
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_sa_put_incorrect(self):
        """
        Test trivia web put API with incorrect data
        """
        result = self.client.put(SUPER_ADMIN, HTTP_AUTHORIZATION=self.auth, data={}, \
                                 format='json')
        self.assertEqual(response_status(result), status.HTTP_400_BAD_REQUEST)

    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    def test_sa_put(self, *_):
        """
        Test trivia web put API with incorrect data
        """
        url = "{0}?user_id={1}".format(SUPER_ADMIN, 1)
        response = self.client.put(url, HTTP_AUTHORIZATION=self.auth, \
                                   data={'send_email': True}, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['group-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_forbidden_get(self):
        """
        Test super admin get API with correct credentials for GA
        """

        obj = TriviaAccessControl()
        obj.forbidden_request(self, SUPER_ADMIN, API_METHODS["GET"], \
                              [USER_ROLES['GROUP_ADMIN'], USER_ROLES['HOST']])

    @mock.patch(MSSQL_FETCH_ALL_PATH, side_effect=mocked_fn)
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['group-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_forbidden_put_ga(self, *_):
        """
        Test super admin put API with correct data for GA
        """
        url = "{0}?user_id={1}".format(SUPER_ADMIN, 1)
        obj = TriviaAccessControl()
        obj.forbidden_request(self, url, API_METHODS["PUT"], \
                              [USER_ROLES['GROUP_ADMIN'], USER_ROLES['HOST']], {'send_email': True})
