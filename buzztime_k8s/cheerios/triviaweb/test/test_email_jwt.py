"""
This file includes test cases to check
trivia web API.
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import post_email_data, post_login_data, \
    get_session_key, USER_STATUS
from cheerios.triviaweb.constants import SIGNUP_URL, ACCOUNT_VERIFICATION, \
    LOGIN_URL
from cheerios.triviaweb.views.account_verification import AccountVerificationViewSet
from cheerios.triviaweb.serializers import EmailSerializer
from cheerios.analytics.test_data import response_status
from cheerios.interface.msssql_connection import MSSQLApiView

CALL_EMAIL = 'cheerios.triviaweb.views.account_verification.AccountVerificationViewSet.call_email'


class MockEmailFailure():
    """
    Mocking the email class for incorrect submissions
    """

    def __init__(self, *_args):
        return

    def send(self):
        """
        Mocking send function in Mock email
        """
        return False

    def __repr__(self):
        """
        Adding repr for debugging and class instantiation
        """
        return "{0}".format(self.__class__.__name__)


def call_mock_fail(*_args):
    """
    Calling email class with failed params
    """
    return MockEmailFailure()


class MockEmailSuccess():
    """
    Mocking the email class for sucess
    """

    def __init__(self, *_args):
        return

    def send(self):
        """
        Send method override to send a True response
        """
        return True

    def __repr__(self):
        return "{0}".format(self.__class__.__name__)


def call_mock_success(*args):
    """
    Calling email calls for successful submissions
    """
    return MockEmailSuccess(args)


class UserViewMocked(MSSQLApiView):
    """
    Mocking user view set
    """

    def __init__(self):
        super().__init__()

    def role_update(self, _data, cursor, game_cursor):
        """
        Overriding role update
        """
        return True


class TestTriviaWebEmail(APITestCase):
    """
    Testcase for trivia web signup.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.data = post_email_data()
        self.auth = "test-auth-header"

    def test_for_validating_serailizer(self):
        """
        Test case for validating serializer for emails
        """
        _data = self.data
        serialized_data = EmailSerializer(data=_data)
        self.assertEqual(serialized_data.is_valid(), True)

    def test_for_invalid_serializer(self):
        """
        Test case for checking invalid data on a serializer
        """
        _data = self.data
        # Popping the item here as this is more atomic and more memory efficient
        popped_tuple = _data.popitem()
        # Check whether the item which is popped is not None
        self.assertNotEqual(popped_tuple, None)

        serialized_data = EmailSerializer(data=_data)
        self.assertEqual(serialized_data.is_valid(), False)

    @mock.patch(CALL_EMAIL, call_mock_fail)
    def test_view_set_with_failure(self):
        """
        Test case for validating the view set with the failed response
        """
        _data = self.data
        serialized_data = EmailSerializer(data=_data)
        self.assertEqual(serialized_data.is_valid(), True)

        if serialized_data.is_valid():
            result = AccountVerificationViewSet().send_mail(serialized_data.validated_data)

        self.assertEqual(result.status_code, 500)

    @mock.patch(CALL_EMAIL, call_mock_success)
    def test_view_set_with_sucess(self):
        """
        Test case for checking correct response
        """
        _data = self.data
        serialized_data = EmailSerializer(data=_data)
        self.assertEqual(serialized_data.is_valid(), True)

        if serialized_data.is_valid():
            result = AccountVerificationViewSet().send_mail(serialized_data.validated_data)

        self.assertEqual(response_status(result), status.HTTP_200_OK)

    @mock.patch('cheerios.triviaweb.utils.user_util.UserDetailsUtil.get_particular_status',
                mock.Mock(return_value=1))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_get_api(self):
        """
        Test case to check if get in the API is working correctly
        """
        user_id = 1
        verify_obj = AccountVerificationViewSet()
        encoded = verify_obj.encode_id(user_id)
        resp = self.client.get(ACCOUNT_VERIFICATION + '?token={0}'.format(encoded), \
                               HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(resp), status.HTTP_200_OK)
