"""
This file includes test cases to check login
"""
import mock
from rest_framework.test import APITestCase

from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import post_login_data, USER_STATUS
from cheerios.triviaweb.constants import LOGIN_URL
from cheerios.analytics.test_data import response_status, response_data


class TestLogin(APITestCase):
    """
    Test for login
    """

    def setUp(self):
        """
        Setup the test cases
        """
        self.login_data = post_login_data()
        self.url = LOGIN_URL

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_login_correct_data(self):
        """
        Test login with valid credentials
        :return:
        """
        response = self.client.post(self.url, self.login_data, format='json')
        self.assertEqual(response_status(response), 200)
        self.assertEqual(response_data(response)["player_id"], 30302646)

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(None)))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_login_invalid(self):
        """
        Test login with invalid email
        :return:
        """
        self.login_data['email'] = 'invalid'
        response = self.client.post(self.url, self.login_data, format='json')
        self.assertEqual(response_data(response), 'Invalid credentials')
        self.assertEqual(response_status(response), 200)
