"""
This file includes test cases to check
trivia web API.
"""
import mock
from rest_framework import status
from rest_framework.test import APITestCase

from cheerios.analytics.test_data import response_status, response_data
from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.constants import USER_URL, GET_USER, \
    USER_ROLES, API_METHODS
from cheerios.triviaweb.test.access_control import TriviaAccessControl
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import post_profile_update_data, post_status_update_data, \
    get_user_data, mock_test_user, USER_STATUS
from cheerios.triviaweb.utils.utils import remove_cache

GET_PARTICULAR_ROLE = 'cheerios.triviaweb.utils.user_util.UserDetailsUtil.get_particular_role'

GET_PARTICULAR_STATUS = 'cheerios.triviaweb.utils.user_util.UserDetailsUtil.get_particular_status'

MOCK_PATH = 'cheerios.triviaweb.views.account_verification.AccountVerificationViewSet.send_mail'
MOCK_EMAIL = 'cheerios.services.Email.email.Email.send'


def mocked_requests(_args, cursor):
    """
    Mocking the method which calls the store procedure so that
    we do not keep on inserting new entries in the database
    """
    return {}


def mocked_send_email(_args, _throw):
    """
    Mocking the method which calls the store procedure so that
    we do not keep on inserting new entries in the database
    """
    return {}


class TestTriviaUserUpdate(APITestCase):
    """
    Testcase for trivia web user update.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.data_status = post_status_update_data()
        self.data_profile = post_profile_update_data()
        self.url = USER_URL
        self.auth = "test_auth_header"

    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    @mock.patch('cheerios.triviaweb.utils.user_util.UserDetailsUtil.get_particular_status',
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(1)))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['super-admin']}".encode("utf-8")]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_success_status_update(self):
        """
        Check whether the user update for status update works with our correct send_data
        """
        response = self.client.post(self.url, self.data_status, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(MOCK_PATH, mock.Mock(return_value=True))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['username_not_exists'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    [], MOCK_RESPONSE['username_not_exists'], [])))
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_correct_profile_update(self, mock):
        """
        Check whether the profile update works with our correct send_data
        """
        response = self.client.post(self.url, self.data_profile, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_serializer_incorrect_data(self):
        """
        Check whether the serializer for user update throws value error on incorrect params
        """
        response = self.client.post(self.url, {}, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses([])))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_for_inappropriate_words(self, mock):
        """
        Test to check inappropriate words
        """
        inappropriate_data = self.data_profile
        inappropriate_data['username'] = 'SATAN'
        response = self.client.post(self.url, inappropriate_data, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_data(response), 'SATAN contains inappropriate text')
        self.assertEqual(response_status(response), 400)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['username_exists'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    [], MOCK_RESPONSE['username_exists'])))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                side_effect=mock_test_utils.mock_fetch_responses(MOCK_RESPONSE['bad_words']))
    def test_for_duplicate_username(self, mock):
        """
        Test to check duplicate username
        """
        inappropriate_data = self.data_profile
        inappropriate_data['username'] = 'Hasher'
        response = self.client.post(self.url, inappropriate_data, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_data(response), 'Username is already taken')
        self.assertEqual(response_status(response), 400)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def tearDown(self):
        """
        This will remove redis cache created by the particular test-case.
        So that it won't be a problem for another test-case.
        :return: None
        """
        remove_cache(key='')


class TestTriviaUserGet(APITestCase):
    """
    Testcase for trivia web get API.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Setup the test cases
        """
        self.data = get_user_data()
        self.url = USER_URL
        self.auth = 'test_auth_header'

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['group_admins'])))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_user_get_api(self):
        """git push origin :the_remote_branch
        Test trivia web get API.
        """
        response = self.client.get(self.url, self.data, HTTP_AUTHORIZATION=self.auth, \
                                   format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['group_admins'])))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_user_get_group_admin_api(self):
        """
        Test trivia web get API with params for fetching all group-admins
        """
        response = self.client.get(self.url, {'roles': '{"group-admin":[]}'}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['group_admins'])))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_user_get_host_api_params(self):
        """
        Test trivia web get API with params for fetching all hosts
        """
        response = self.client.get(self.url, {'roles': '{"host":[]}'}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['group_admins'])))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_user_get_mixed_api_params(self):
        """
        Test trivia web get API with params.
        TODO: Add a filter and check if all the values are correct
        """
        response = self.client.get(self.url, {'roles': '{"host":["Active"], "group-admin":[]}'}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['group_admins'])))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_user_get_super_admin(self):
        """
        Test trivia web get API with params.
        """
        response = self.client.get(self.url, \
                                   {'roles': '{"host":["Active", "Pending"], "group-admin":[]}'}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses(
                    MOCK_RESPONSE['group_admins'])))
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    def test_user_get_super_admin_ent(self):
        """
        Test trivia web get API with enterprise parameter
        """
        response = self.client.get(self.url, {'enterprise_id': 1}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[MOCK_RESPONSE['get_user']]))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_connection_manager()))
    @mock.patch(MOCK_PATH, mocked_send_email)
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_user_profile_update(self):
        """
        Test trivia web get API with enterprise parameter
        """
        response = self.client.get(GET_USER, {'user_id': mock_test_user()}, \
                                   HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)
        user_data = response_data(response)
        user_data['firstName'] = "test"
        user_data.pop('username')
        response = self.client.post(self.url, user_data, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['super-admin'], 'enterprise':'1'}".encode("utf-8"),
                                 None]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    @mock.patch(MOCK_PATH, mocked_send_email)
    @mock.patch(GET_PARTICULAR_STATUS, mock.Mock(return_value=1))
    @mock.patch(mock_test_utils.CONNECTION_MANAGER,
                mock.Mock(side_effect=mock_test_utils.mock_cursor_responses([], Exception)))
    @mock.patch(GET_PARTICULAR_ROLE, mock.Mock(return_value=1))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_user_update_role(self):
        """
        Test trivia web get API with enterprise parameter
        """
        send_data = {
            'user_id': mock_test_user(),
            'status': 'Active',
            'role': ['Host']
        }
        response = self.client.post(self.url, send_data, \
                                    HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch(MOCK_PATH, mocked_send_email)
    @mock.patch('cheerios.services.redis.RedisSession.save_data', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(
                    side_effect=["{'roles':['Host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=[[{'StumpTriviaUserStatusDesc': 'Active',
                                         'StumpTriviaUserStatusID': 1}], '',
                                       [{'StumpTriviaRoleDesc': 'Host',
                                         'StumpTriviaRoleID': 1}]
                                       ]))
    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def test_forbidden_put(self):
        """
        Test user update for host
        """
        send_data = {
            'user_id': mock_test_user(),
            'status': 'Active',
            'role': ['Host']
        }

        obj = TriviaAccessControl()
        obj.forbidden_request(self, self.url, API_METHODS["POST"], \
                              [USER_ROLES['HOST']], send_data)

    @mock.patch('cheerios.services.redis.RedisSession.remove_keys', mock.Mock())
    def tearDown(self):
        """
        This will remove redis cache created by the particular test-case.
        So that it won't be a problem for another test-case.
        :return: None
        """
        remove_cache(key='')
