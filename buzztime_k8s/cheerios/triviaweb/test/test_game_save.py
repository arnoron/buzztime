"""
This file includes test cases to check login
"""
import mock
from rest_framework.test import APITestCase
from rest_framework import status

from cheerios.test_utils import mock_test_utils
from cheerios.triviaweb.test.response_data import MOCK_RESPONSE
from cheerios.triviaweb.test.test_data import invalid_game_key, \
    post_login_data, get_invalid_game_token, get_game_token, \
    get_session_key, change_game_value, firebase_mock_data, game_team_data, \
    invalid_survey, change_team_value, db_transaction, USER_STATUS, game_team_data_with_quotes
from cheerios.triviaweb.constants import SAVE_GAME, LOGIN_URL
from cheerios.analytics.test_data import response_status
from cheerios.triviaweb.views.game_details import GameDetailViewSet
from cheerios.triviaweb.views.game import GameViewSet
from cheerios.triviaweb.views.team import TeamViewSet
from cheerios.settings import MSSQLDBGAMEPLAY
GAME_DETAIL = GameDetailViewSet()
GAME_VIEW_SET = GameViewSet()
TEAM_VIEW_SET = TeamViewSet()


class TestGameSave(APITestCase):
    """
    Testcase for trivia web get API.
    """

    @mock.patch(mock_test_utils.CALL_STORED_PROCEDURE, mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR,
                mock.Mock(side_effect=mock_test_utils.mock_fetch_responses(
                    MOCK_RESPONSE['result_username'],
                    MOCK_RESPONSE['user_cred'],
                    MOCK_RESPONSE['check_host'],
                    MOCK_RESPONSE['white_labelling'],
                    MOCK_RESPONSE['user_roles'],
                )))
    @mock.patch(USER_STATUS, mock.Mock(return_value='Active'))
    def setUp(self):
        """
        Call the login api for authentication of question API.
        """
        # data = post_login_data()
        # header = get_session_key(self, LOGIN_URL, data)
        self.auth = "test_auth_header"

    # @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    # @mock.patch('cheerios.services.redis.RedisSession.get_value',
    #             mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    # @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    # def test_game_incomplete_data(self):
    #     """
    #     Game API will return 200 and works as a background job.
    #     """
    #     data = get_invalid_game_token()
    #     response = self.client.post(SAVE_GAME, data, HTTP_AUTHORIZATION=self.auth, format='json')
    #     self.assertAlmostEqual(response_status(response), status.HTTP_200_OK)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_game_without_data(self):
        """
        Passing empty data in game API it will return 400.
        """
        response = self.client.post(SAVE_GAME, HTTP_AUTHORIZATION=self.auth, format='json')
        self.assertAlmostEqual(response_status(response), status.HTTP_400_BAD_REQUEST)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_save_invalid_data(self):
        """
        The function will check save details function.
        This function will not return any thing if we are passing wrong key.
        """
        data = invalid_game_key()
        GAME_DETAIL.save_details(data)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_remove_entry(self):
        """
        The function will check remove_firebase_entry function.
        This function will not return any thing if we are passing wrong key.
        """
        key = invalid_game_key()
        token = get_game_token()
        GAME_DETAIL.create_cache_connection()
        GAME_DETAIL.remove_firebase_entry(key, token)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_game_without_game(self):
        """
        Check post game without any game object.
        It will return ValueError.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                GAME_VIEW_SET.post_game(token, cursor, '')
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_game_without_page(self):
        """
        Check post game without any page value.
        It will return ValueError.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        game_data = firebase_mock_data()
        game_data = change_game_value(game_data, 'page', "")
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                GAME_VIEW_SET.post_game(token, cursor, game_data)

        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_unfinished_game(self):
        """
        Check post game which is not finished yet.
        It will return ValueError.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        game_data = firebase_mock_data()
        game_data = change_game_value(game_data, 'page', "game-start")
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                GAME_VIEW_SET.post_game(token, cursor, game_data)
        self.assertTrue("Game not yet finished" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_game_without_questions(self):
        """
        Check post game with out question.
        It will works fine.and will return nothing.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        game_data = firebase_mock_data()
        game_data = change_game_value(game_data, 'questions', "")
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            GAME_VIEW_SET.post_game(token, cursor, game_data)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_game_without_host(self):
        """
        Check post game with out host id.
        It will return ValueError.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        game_data = firebase_mock_data()
        game_data = change_game_value(game_data, 'hostId', "")
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                GAME_VIEW_SET.post_game(token, cursor, game_data)
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_game_without_gameid(self):
        """
        Check post game with out game id.
        It will return ValueError.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        game_data = firebase_mock_data()
        game_data = change_game_value(game_data, 'stumpGameId', "")
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                GAME_VIEW_SET.post_game(token, cursor, game_data)
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_game_without_venue(self):
        """
        Check post game with out venue id.
        It will return ValueError.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        game_data = firebase_mock_data()
        game_data = change_game_value(game_data, 'venueId', "")
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                GAME_VIEW_SET.post_game(token, cursor, game_data)
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(side_effect=[[{'StumpGameInstanceID': 1}], 1]))
    def test_game_proper_data(self):
        """
        Check post game with proper data
        It will works fine.and will return nothing.
        """
        token = get_game_token()
        GAME_VIEW_SET.create_cache_connection()
        game_data = firebase_mock_data()
        with db_transaction(GAME_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            result = GAME_VIEW_SET.post_game(token, cursor, game_data)

        self.assertTrue(isinstance(result, int))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_team_empty_key(self):
        """
        Check post team with out game key.
        It will return ValueError.
        """
        team_data = game_team_data()
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                TEAM_VIEW_SET.post_game('', 1, cursor, team_data)
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_team_empty_game_id(self):
        """
        Check post team with out game id.
        It will return ValueError.
        """
        key = invalid_game_key()
        team_data = game_team_data()
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                TEAM_VIEW_SET.post_game(key, '', cursor, team_data)
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_team_negative_game_id(self):
        """
        Check post team with negative game id.
        It will return ValueError.
        """
        key = invalid_game_key()
        team_data = game_team_data()
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                TEAM_VIEW_SET.post_game(key, -1, cursor, team_data)
        self.assertTrue("Parameter passed should be a positive integer." in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_team_empty_score(self):
        """
        Check post team with out score.
        It will return ValueError.
        """
        key = invalid_game_key()
        team_data = game_team_data()
        team_data = change_team_value(team_data, 'score', "")
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                TEAM_VIEW_SET.post_game(key, 1, cursor, team_data)
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_team_empty_name(self):
        """
        Check post team with out name.
        It will return ValueError.
        """
        key = invalid_game_key()
        team_data = game_team_data()
        team_data = change_team_value(team_data, 'name', "")
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                TEAM_VIEW_SET.post_game(key, 1, cursor, team_data)
        self.assertTrue("Invalid Parameter passed" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    def test_team_empty_team(self):
        """
        Check post team with out team data.
        It will return ValueError.
        """
        key = invalid_game_key()
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            with self.assertRaises(Exception) as error:
                TEAM_VIEW_SET.post_game(key, 1, cursor, '')
        self.assertTrue("Game doesn't have teams" in str(error.exception))

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(side_effect=[[{'StumpGameTeamID': 1}], '']))
    def test_team_without_survey(self):
        """
        Check post team with out survey response.
        It will works fine and will return nothing.
        """
        key = invalid_game_key()
        team_data = game_team_data()
        team_data = change_team_value(team_data, 'survey', "")
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            TEAM_VIEW_SET.post_game(key, 7, cursor, team_data)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(side_effect=[[{'StumpGameTeamID': 1}], '']))
    def test_negetive_survey_key(self):
        """
        Check post team with negative key in survey response.
        It will works fine and will return nothing.
        """
        key = invalid_game_key()
        team_data = game_team_data()
        survey = invalid_survey()
        team_data = change_team_value(team_data, 'survey', survey)
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            TEAM_VIEW_SET.post_game(key, 7, cursor, team_data)

    @mock.patch('cheerios.services.redis.RedisSession.get_key', mock.Mock(side_effect=[1, 1]))
    @mock.patch('cheerios.services.redis.RedisSession.get_value',
                mock.Mock(side_effect=["{'roles':['host'], 'enterprise':'1'}".encode("utf-8"), None]))
    @mock.patch('cheerios.interface.msssql_connection.MSSQLApiView.get_cursor', mock.Mock())
    @mock.patch(mock_test_utils.FETCH_ALL_FROM_CURSOR, mock.Mock(side_effect=[[{'StumpGameTeamID': 1}], '']))
    def test_team_proper_data(self):
        """
        Check post team with proper data.
        It will works fine and will return nothing.
        """
        key = invalid_game_key()
        team_data = game_team_data()
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            TEAM_VIEW_SET.post_game(key, 7, cursor, team_data)

    def test_team_proper_data_with_quotes(self):
        """
        Check post team with proper data.
        It will works fine and will return nothing.
        """
        key = invalid_game_key()
        team_data = game_team_data_with_quotes()
        with db_transaction(TEAM_VIEW_SET, MSSQLDBGAMEPLAY, True, True) as cursor:
            TEAM_VIEW_SET.post_game(key, 7, cursor, team_data)
