"""
Function which returns question data.
"""
import base64
import datetime
from contextlib import contextmanager

from cheerios.services.redis import RedisSession
from cheerios.triviaweb.constants import TIME_START, TIME_END, QUESTION_TYPES

START_DATE = datetime.date.today().replace(day=1)
START_DATE = "{0}{1}".format(START_DATE, TIME_START)
END_DATE = datetime.date.today()
END_DATE = "{0}{1}".format(END_DATE, TIME_END)
LOCATION_ID_VALID = 1
LOCATION_ID_INVALID = 'test'
IMAGE_URL = 'https://ntnservices.dev.buzztime.com/stp/stump/Picture/9f5abbde-5e70-54da-600d' \
            '-b433d4fc48e6/round1/question1/KDFC02_web_1.png'
IMAGE_DES = 'Get 70% off on all the burger meals on weekends'
USER_STATUS = 'cheerios.triviaweb.utils.user_util.UserDetailsUtil.get_user_status'


@contextmanager
def db_transaction(obj, database, rollback=False, transaction=False):
    """
    Context manager to create connection for given DB and close connection.
    """
    cursor = obj.get_cursor(database, True) if transaction else obj.get_cursor(database)

    yield cursor
    if rollback:
        obj.connection_rollback()
    obj.close_cursor()


def get_session_key(ref, url, data):
    """
    Getting login data.
    Check if user is logged in.
    if logged in then fetch key from redis.
    Else calling login API and save key in redis.
    """
    redis = RedisSession()
    key = redis.get_key(data["email"])

    if not key:
        response = ref.client.post(url, data, format='json')
        key = response["Session"]
        redis = RedisSession()
        redis.save_data(data["email"], key)
    return key


def game_team_data():
    """
    Team valid data.
    """
    data = {
        '-Km6Mes5BgpnRGXe6wki': {
            'name': 'abcTest1{0}'.format(START_DATE),
            'id': '00c45b9a-69f2-4282-cf5b-d9d4f0672d72',
            'score': 5, 'team-status': 'accepted',
            'answers': {
                'round1-question4': {
                    'betScore': '5',
                    'marked': True,
                    'usedBets': ['5'],
                    'category': 'Sports',
                    'questionId': 4,
                    'round': 1,
                    'answer': 'sss'
                }
            },
            'survey': valid_survey(),
        }}
    return data

def game_team_data_with_quotes():
    data = game_team_data()
    data['-Km6Mes5BgpnRGXe6wki']['name'] = "team_name'with'two_quotes"
    return data


def change_team_value(data, attribute, value):
    """
    Update value of team data
    """
    data['-Km6Mes5BgpnRGXe6wki'][attribute] = value
    return data


def valid_survey():
    """
    Valid survey data
    """
    data = {'1': {'Response': 'true',
                  'SurveyQuestionId': 1}
            }
    return data


def invalid_survey():
    """
    Invalid survey data
    """
    data = {'survey': {'Response': 'true',
                       'SurveyQuestionId': 1
                       }
            }
    return data


def firebase_mock_data():
    """
    Valid game data
    """
    data = {
        'gameid': 'e4cb02fc-cb94-8a59-482c-1ad744e53283',
        'status': 'to_be_saved',
        'teams': game_team_data(),
        'stumpGameId': '1',
        'gameToken': 'F6GVCG',
        'venueId': 1,
        'questionNumber': 1,
        'round': 2,
        'questionAnswered': 0,
        'start_date': 'Mon, 13 Nov 2017 07:27:01 GMT',
        'page': 'game-end',
        'hostId': '30304044',
        'questions': {'question': [{
            'correctAnswer': 'Sri Lanka',
            'roundDescription': 'Sports Related Questions',
            'questionSetName': 'test',
            'questionText': 'testing',
            'category': 'Sports',
            'questionId': 1,
            'round': 1
        }, {
            'correctAnswer': '4',
            'roundDescription': 'Sports Related Questions',
            'questionSetName': 'test',
            'questionText': 'testing',
            'category': 'Sports',
            'questionId': 2,
            'round': 1
        }, {
            'correctAnswer': 'Kamaljit Sandhu',
            'roundDescription': 'Sports Related Questions',
            'questionSetName': 'test',
            'questionText': 'testing',
            'category': 'Sports',
            'questionId': 3,
            'round': 1
        }, {
            'correctAnswer': '1928',
            'roundDescription': 'Sports Related Questions',
            'questionSetName': 'test',
            'questionText': 'test',
            'category': 'Sports',
            'questionId': 4,
            'round': 1
        }, {
            'correctAnswer': 'Brian Lara',
            'roundDescription': 'Sports Related Questions',
            'questionSetName': 'test',
            'questionText': 'test',
            'category': 'Sports',
            'questionId': 5,
            'round': 1
        }, {
            'correctAnswer': 'Insects',
            'roundDescription': 'General Knowledge',
            'questionSetName': 'test',
            'questionText': 'test',
            'category': 'General Knowledge',
            'questionId': 1,
            'round': 2
        }],
            'stump_question_set_id': 1
        }}
    return data


def change_game_value(data, attribute, value):
    """
    Update value of game data.
    """
    data[attribute] = value
    return data


def get_tuple_data():
    """
    This function return set of questions in tuples.
    """
    return [
        {
            'CorrectAnswer': 'William Golding',
            'QuestionSetName': 'Stump Mobile Trivia Alpha Question Set',
            'StumpQuestionID': 1,
            'QuestionNumber': 1,
            'RoundNumber': 1,
            'StumpQuestionSetID': 1,
            'Question': 'Who authored Lord of the Flies (1954)? ',
            'RoundCategory': 'Animal Books',
            'RoundName': '1',
            'StumpQuestionType': 'Open'
        },
        {
            'CorrectAnswer': 'C.S Lewis',
            'QuestionSetName': 'Stump Mobile Trivia Alpha Question Set',
            'StumpQuestionID': 2,
            'QuestionNumber': 2,
            'RoundNumber': 1,
            'StumpQuestionSetID': 1,
            'Question': 'Who authored The Lion the Witch and the Wardrobe (1950)? ',
            'RoundCategory': 'Animal Books',
            'RoundName': '2',
            'StumpQuestionType': 'Open'
        },
        {
            'CorrectAnswer': 'Sara Gruen',
            'QuestionSetName': 'Stump Mobile Trivia Alpha Question Set',
            'StumpQuestionID': 3,
            'QuestionNumber': 3,
            'RoundNumber': 1,
            'StumpQuestionSetID': 1,
            'Question': 'Who authored Water for Elephants (2006)?',
            'RoundCategory': 'Animal Books',
            'RoundName': '3',
            'StumpQuestionType': 'Open'
        }
    ]


def data_with_empty_value():
    """
    Return data with empty value.
    """
    return ["Testing Question", 1, "None"]


def data_with_proper_data():
    """
    Return proper data.
    """
    return ["Testing Question", 1]


def data_with_single_data():
    """
    Return sting.
    """
    return "Test1"


def get_invalid_game_token():
    """
    Return incomplete game token.
    """
    return {
        "gameKey": invalid_game_key()
    }


def invalid_game_key():
    """
    Return incomplete game token.
    """
    return "-KmR0cqumCA7YPYFlQ"


def get_game_token():
    """
    Return incomplete game token.
    """
    return "92X2P8"


def post_login_data():
    """
    Return login data.
    """
    return {
        "email": "pooja.lodhi@hashedin.com",
        "password": "6c6101f1098dcd55fb23d28cd32f7e66"
    }


def post_login_not_super_admin():
    """
    this data is for only host login not assigned any other access like groupadmin or superadmin
    """
    return {
        "email": "host1@sharklasers.com",
        "password": "6c6101f1098dcd55fb23d28cd32f7e66"
    }


def post_login_super_admin():
    """
    Return post login of super-admin
    """
    return {
        "email": "pooja.lodhi@hashedin.com",
        "password": "6c6101f1098dcd55fb23d28cd32f7e66"
    }


def post_normal_user_login():
    """
    Return post login of normal user
    :return:
    """
    return {
        "email": "soney.george@hashedin.com",
        "password": "6c6101f1098dcd55fb23d28cd32f7e66"
    }


def mocked_question_draft():
    """
    Return mocked draft data
    """
    return {"branding_id": "0",
            "days": ["1", "2"],
            "draft_time": "2018-02-22T05:17:07.619Z",
            "name": "Sample",
            "question": []}


def get_sa_email_data():
    """
    Return mocked data for super admin emails
    """
    return {
        "removed_user_id": 30302647,
        "email_id": 'abc',
        "domain": 'some elusive domain'
    }


def duplicate_objects():
    """
    Returns a list of objects where the key is duplicate
    """
    return [{"UserName": "Test%d" % i, "other": "test%d" % i} for i in range(1, 4)]


def get_test_token():
    """
    Return token.
    """
    return {'TEST_AUTH_TOKEN': '123456789'}


def questions():
    """
    Return list of question for question post API.
    """
    return [
        {
            'question_id': "1",
            'question_text': "Test Question",
            'category': 'Test Category',
            'round': '1',
            'correct_answer': 'William Golding',
            'round_description': 'Test Round',
            'type_of_question': QUESTION_TYPES['Open'],
            'fun_fact': 'Testing in bytes',
            'media_url': 'test_url',
            'media_path': 'test_path'
        },
        {
            'question_id': "2",
            'question_text': 'Test "" Question',
            'category': 'Test Category',
            'round': "1",
            'correct_answer': 'William Golding',
            'round_description': 'Test Round',
            'type_of_question': QUESTION_TYPES['Open'],
            'media_url': 'test_url',
            'media_path': 'test_path'
        },
        {
            'question_id': "3",
            'question_text': 'Test " Question',
            'category': 'Test Category',
            'round': "1",
            'correct_answer': 'William Golding',
            'round_description': 'Test Round',
            'type_of_question': QUESTION_TYPES['Open'],
            'media_url': 'test_url',
            'media_path': 'test_path'
        },
        {
            'question_id': "4",
            'question_text': "Test '' Question",
            'category': 'Test Category',
            'round': "1",
            'correct_answer': 'William Golding',
            'round_description': 'Test Round',
            'type_of_question': QUESTION_TYPES['Open'],
            'fun_fact': 'Coca-Cola would be green if coloring wasn’t added to it.',
            'media_url': 'test_url',
            'media_path': 'test_path'
        },
        {
            'question_id': "5",
            'question_text': "Test ' Question",
            'category': 'Test Category',
            'round': "1",
            'correct_answer': 'William Golding',
            'round_description': 'Test Round',
            'type_of_question': QUESTION_TYPES['Open'],
            'media_url': 'test_url',
            'media_path': 'test_path'
        },
        {
            'question_id': "1",
            'question_text': "Test Question",
            'category': 'Test Category',
            'round': "Bonus",
            'round_name': 'Bonus',
            'round_description': 'Bonus',
            'correct_answer': 'William Golding',
            'type_of_question': QUESTION_TYPES['MultipleChoice'],
            'media_url': 'test_url',
            'media_path': 'test_path'
        },
        {
            'question_id': "1",
            'question_text': "Test Question",
            'category': 'Test Category',
            'round': "Tie",
            'round_description': 'Tie',
            'round_name': 'Tie',
            'correct_answer': 'William Golding',
            'type_of_question': QUESTION_TYPES['TrueFalse'],
            'media_url': 'test_url',
            'media_path': 'test_path'
        }
    ]


def question_with_survey():
    """
    Return list of question with survey for question post API.
    """
    date = datetime.datetime.now()
    question = questions()
    question.append({
        "round": "Survey",
        "category": "Test survey{0}".format(str(date)),
        "round_description": "TrueFalse",
        "question_id": "1",
        "question_text": "Is the Stump! 2.0 better than a traditional pen and paper game?"
    })
    question.append({
        "round": "Survey",
        "category": "Test survey{0}".format(str(date)),
        "round_description": "Open",
        "question_id": "2",
        "question_text": "Why not is Stump! 2.0 better?"
    })
    return question


def question_set_two_question():
    """
    Return two question from list of question set for post API.
    """
    question = questions()
    return_value = []
    return_value.append(question[0])
    return_value.append(question[1])
    return return_value


def question_set_one_question():
    """
    Return one question from list of question set for post API.
    """
    question = questions()
    return_value = []
    return_value.append(question[0])
    return return_value


def question_post_without_set_name():
    """
    Return Post API data without question set name.
    """
    question = questions()
    return {
        'Question': question,
        'StumpGameId': 1,
        'BrandingId': 1
    }


def question_post_without_question():
    """
    Return Post API data without question list.
    """
    return {
        'QuestionSetName': 'Test case Question Set',
        'StumpGameId': 1,
        'BrandingId': 1
    }


def question_incomplete_data():
    """
    Return Post API incomplete data.
    """
    return {
        'QuestionSetName': 'Test case Question Set',
        'Question': question_set_two_question(),
        'BrandingId': 1
    }


def location_complete_data():
    """
    Return Post API
    """
    return {
        "brand": "1",
        "days": ["1", "2"],
        "time": "12:30",
        "name": "Test Location 1",
        "pin_code": "123",
        "city": "Test City 1",
        "state": "Test State1",
        "address": "Test Address 1",
        "latitude": 33.1313381,
        "longitude": -117.2809629
    }


def convert_to_data_uri(data):
    """
    convert data to data-uri.
    """
    csv = "Round, Category, Category Description, Question Number, Question, Answer"

    for data_object in data:
        if data_object["RoundName"].lower() == "survey":
            csv = "{0}\n{1}".format(csv, data_object["RoundName"])
            csv = "{0},{1}".format(csv, data_object["SurveyName"])
            csv = "{0},{1}".format(csv, data_object["SurveyType"])
            csv = "{0},{1}".format(csv, data_object["SurveyQuestionId"])
            csv = "{0},{1}".format(csv, data_object["Question"])
            csv = "{0},{1}".format(csv, "''")
        else:
            csv = "{0}\n{1}".format(csv, data_object["RoundName"])
            csv = "{0},{1}".format(csv, data_object["Category"])
            csv = "{0},{1}".format(csv, data_object["RoundDescription"])
            csv = "{0},{1}".format(csv, data_object["QuestionId"])
            csv = "{0},{1}".format(csv, data_object["QuestionText"])
            csv = "{0},{1}".format(csv, data_object["CorrectAnswer"])

    question = base64.b64encode(bytes(csv, "utf-8"))
    question = "data:text/csv;base64," + question.decode('utf-8')
    return question


def question_correct():
    """
    Correct question for testing post call with tie and survey
    """
    question = questions()
    question.append({
        "round": "Survey",
        "category": "Test survey",
        "round_description": "TrueFalse",
        "question_id": "1",
        "question_text": "Is the Stump! 2.0 better than a traditional pen and paper game?"
    })
    return {
        'Question': question,
        'BrandingId': [1, 2],
        'QuestionSetName': 'Test case Question Set',
        'days': ["1", "8"]
    }


def question_edit():
    """
    Surevy edit object with questions
    """
    question = {
        "edit_status": "EDIT",
        "question_id": "1",
        "question_text": "Test Question",
        "category": "Test Category",
        "round": "1",
        "correct_answer": "William Golding",
        "round_description": "Test Round",
        "type_of_question": "Text",
        'media_url': 'test_url',
        'media_path': 'test_path'
    }
    return question


def question_delete():
    """
    Surevy edit object with questions
    """
    question = {
        "edit_status": "DELETE",
        "question_id": "1",
        "question_text": "Test Question",
        "category": "Test Category",
        "round": "1",
        "correct_answer": "William Golding",
        "round_description": "Test Round",
        "type_of_question": "Text",
        "media_path": "test_path",
        "media_url": "test_url"
    }
    return question


def test_survey_data(status, success):
    """
    Surevy edit object with questions
    """
    question = {
        "edit_status": status,
        "question_id": "2" if success else "invalid",
        "question_text": "Test Question",
        "category": "Rating",
        "round": "Survey",
        "round_description": "Rating",
        "round_name": "Tie",
        "correct_answer": "William Golding",
        "media_url": "test_url",
        "media_path": "test_path"
    }
    return question


def tie_add():
    """
    Surevy edit object with questions
    """
    question = {
        "edit_status": "ADD",
        "question_id": "2",
        "question_text": "Test Question",
        "category": "Test Category",
        "round": "Tie",
        "round_description": "Tie",
        "round_name": "Tie",
        "correct_answer": "William Golding",
        "type_of_question": "Text",
        "media_path": "test_path",
        "media_url": "test_url"
    }
    return question


def survey_edit_data(data):
    """
    Surevy edit object with questions
    """
    question = questions()
    question[0] = data
    return question


def question_put_edit_correct():
    """
    Correct question for testing post call with tie and survey
    """
    return {
        'question_id': 13003,
        'question': survey_edit_data(question_edit()),
        'branding_id': 1,
        "survey_add_flag": "EDIT",
        'days': ["1", "8"]
    }


def question_put_add_correct():
    """
    Correct question for testing post call with tie and survey
    """
    return {
        'question_id': 13003,
        'question': survey_edit_data(question_delete()),
        'branding_id': 1,
        "survey_add_flag": "EDIT",
        'days': ["1", "8"]
    }


def question_put_delete_correct():
    """
    Correct question for testing post call with tie and survey
    """
    return {
        'question_id': 13003,
        'question': survey_edit_data(tie_add()),
        'branding_id': 1,
        "survey_add_flag": "EDIT",
        'days': ["1", "8"]
    }


def survey_put_data(status, success):
    """
    Correct question for testing post call with tie and survey
    """
    return {
        'question_id': 13003,
        'question': survey_edit_data(test_survey_data(status, success)),
        'branding_id': 1,
        "survey_add_flag": "EDIT",
        'days': ["1", "8"]
    }


def survey_set_put_add(success):
    """
    Correct question for testing post call with tie and survey
    """
    return {
        'question_id': 13003,
        'question': survey_edit_data(test_survey_data("ADD", success)),
        'branding_id': 1,
        "survey_add_flag": "ADD",
        'days': ["1", "8"]
    }


def question_put_incorrect():
    """
    Correct question for testing post call with tie and survey
    """
    return {
        'question_id': 13003,
        'question': survey_edit_data(question_edit()),
        'branding_id': 'invalid',
        "survey_add_flag": "EDIT",
        'days': ["1", "8"]
    }


def question_put_correct():
    """
    Correct question for testing post call with tie and survey
    """
    return {
        'question_id': 13003,
        'question': survey_edit_data(),
        'branding_id': 1,
        'days': '123'
    }


def invalid_survey_type():
    """
    Data to be returned with invalid survey
    """
    question = questions()
    question.append({
        "round": "Survey",
        "category": "Test survey",
        "round_description": "Multiple",
        "question_id": "1",
        "question_text": "Is the Stump! 2.0 better than a traditional pen and paper game?"
    })
    return {
        'Question': question,
        'BrandingId': 1,
        'QuestionSetName': 'Test case Question Set',
        'days': ["1", "8"]
    }


def days_update():
    """
    Data for updating the days for a QS
    """
    return {
        'question_id': 1,
        'days': ["1", "8"]
    }


def change_attribute_of_data(data, attribute, row=0, value=''):
    """
    Change value of given data object at given row and given value.
    """
    data[row][attribute] = value
    return {
        'QuestionSetName': 'Test case Question Set',
        'Question': data,
        'StumpGameId': 1,
        'BrandingId': 1
    }


def question_without_attribute(attribute, row=0, value=''):
    """
    Change value of question object at given row and given value.
    """
    return change_attribute_of_data(questions(), attribute, row, value)


def survey_without_attribute(attribute, row=0, value=''):
    """
    Change value of question object at given row and given value.
    """
    return change_attribute_of_data(question_with_survey(), attribute, row, value)


def question_two_question():
    """
    Return Post API with two question.
    """
    return {
        'QuestionSetName': 'Test case Question Set',
        'Question': question_set_two_question(),
        'StumpGameId': 1,
        'BrandingId': 1
    }


def question_one_question():
    """
    Return Post API with one question.
    """
    return {
        'QuestionSetName': 'Test case Question Set',
        'Question': question_set_one_question(),
        'StumpGameId': 1,
        'BrandingId': 1
    }


def question_wrong_game_id():
    """
    Return Post API with wrong game id.
    """
    question = questions()
    return {
        'QuestionSetName': 'Test case Question Set',
        'Question': question,
        'StumpGameId': -1,
        'BrandingId': 1
    }


def question_with_out_brand():
    """
    Return Post API with wrong game id.
    """
    question = questions()
    return {
        'QuestionSetName': 'Test case Question Set',
        'Question': question,
        'StumpGameId': 1
    }


def question_wrong_brand():
    """
    Return Post API with wrong game id.
    """
    question = questions()
    return {
        'QuestionSetName': 'Test case Question Set',
        'Question': question,
        'BrandingId': -1
    }


def question_delete_status_zero():
    """
    Return delete API data with status id 0.
    """
    return {
        'StumpQuestionSetID': 0,
        'StatusID': 0
    }


def question_delete_status_one():
    """
    Return delete API data with status id 1.
    """
    return {
        'StumpQuestionSetID': 0,
        'StatusID': 1
    }


def question_delete_without_status():
    """
    Return delete API data without status id.
    """
    return {
        'StumpQuestionSetID': 0
    }


def question_delete_without_set_id():
    """
    Return delete API data without question set id.
    """
    return {
        'StatusID': 1
    }


def post_signup_data():
    """
    Return login data.
    """
    return {
        "username": "Thisissomethingspecial",
        "firstName": "He",
        "lastName": "bytes",
        "email": "test@test.com",
        "role": "host",
        "enterprise_id": 1,
        "password": "6c6101f1098dcd55fb23d28cd32f7e66",
        "whitelabel_id": 1
    }


def post_status_update_data():
    """
    Return post status update data.
    """
    return {
        "user_id": 1,
        "status": "active"
    }


def post_profile_update_data():
    """
    Return post profile update data.
    """
    return {
        "user_id": 1,
        "username": "username",
        "email": "abc@abc.com",
        "firstName": "firstName",
        'lastName': "lastName",
        "role": "host",
        "status": "active",
        "phoneNumber": "1234567890"
    }


def get_user_data():
    """
    Return get user data.
    """
    return {
        "enterprise_id": 1,
        "role": "host"
    }


def post_email_data():
    """
    Returns Email data for sending to the user
    """
    return {
        "user_id": 1,
        "username": "test",
        "email": "test@test.com",
        "domain": "test",
        "activation_id": 1
    }


def get_customize_values():
    """
    This function will return dummy data
    for customization.
    :param domain:
    :return:
    """
    return {
        "footer": "#ffffff",
        "header": "#ffffff",
        "background": "#000000",
        "domain": "test",
        "id": -1,
        "logo": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC\
AAAAA3CAYAAABtqUjXAAACn0lEQVRYhb3ZR2sUUQDA8V9E7OVg77FHReLdiogauyL\
iUVGiiMSC38OKSCKKKIIgmBBij4oKfgvx7MGLXdB4eDMQg9l9k32zc9udB//flJ3Ze\
dOwbs06dVga8QALcQ7t+YrhdYjPx0vMyz5fwwhcgWF1jkMDLqGNcvfAgiw+9z/rGnA\
BH8sCLMArzKkwpgEHyjgECyPi+XI/NWBRgfhZ3E0JWCwc89lVxvXhDM6T7iTM47Mi4\
xfzL1LsgSUF4qf7x6l9DyzFC8yMiJ/C5YEragEUibfJrnypAE1ZfEZE/CSuDjZgKIAm\
4ZhPrzU+FMAyYctj4ieEG0/FpQhgmbDl01LFiwCWC1seEz+u3/0+BWBFFp8aET+Gjth\
4DKBIvBXXi8SrAVaiF1Mi4kdxo2icwS/FsfE/tcQHAzTXK/4/QDOeRcR/Z/GbtcT59xx\
YlcUnR8SP4Fat8f6AVXiOSfWM54DmAvHDuJ0qngPuRcYP4U7KOOEknBgx7hvep47ngFb\
8qjJuPB5hdRmAbhyMQEzAw9SI/DrwAAciEY+wJjUAuiIR+eFIghh4JezC/gjEuAyxNjW\
AcE7UDTHY3bAb+yIQYzPEkKdZKj0Z9WBvAcT61ADCz24PflYZNyYbuyE1gLB1sYgebEwN\
gMcZ4kcEorsIosjTcVHEptQAeJIhvkcgumIQQ5kfyBHfIhGbUwPgqfATjUF0VkLUMkPyFL\
sjEKMzxJbUAMJfuV21IFLMEfViZwRiVIbYmhpAeH7cUQDRkhpAmDvYHoEYKfwBakkNIMyS\
tkQiOrG2jLni1xnia5VxI9BW1vuCHPGlyrh3Zb6weINtFRAdOF/2G5M3wp74POD7dmE6p6\
9sALwV9sSn7PMVYSKrj/q8tMoRjcIz6If+K/4CcAedN5DTL5YAAAAASUVORK5CYII="
    }


def modified_data_value(attribute, value):
    """
    Fectch whitelable current values update key value
    Return updated values.
    """
    theme = get_customize_values()
    theme[attribute] = value
    return theme


def post_change_password_data():
    """
    Return change password correct data.
    """
    return {
        "username": "PRIYANKA",
        "oldpassword": "6c6101f1098dcd55fb23d28cd32f7e66"
    }


def mock_test_user():
    """
    Return sample user id
    """
    return 22627958


def dr_without_branding_id():
    """
    Return companies
    """
    return {
        'start_date': START_DATE,
        'end_date': END_DATE
    }


def dr_with_branding_id():
    """
    Return all hosts
    """
    return {
        'branding': 1,
        'start_date': START_DATE,
        'end_date': END_DATE
    }


def dr_with_host_id():
    """
    Return host details
    """
    return {
        'host': 30303842,
        'start_date': START_DATE,
        'end_date': END_DATE
    }


def dr_with_host_id_empty_data():
    """
    Return host details with no data
    """
    return {
        'host': 30303842,
        'start_date': START_DATE,
        'end_date': END_DATE
    }


def dr_with_valid_location_id():
    """
    Return host details with location_id
    """
    return {
        'host': 30303842,
        'start_date': START_DATE,
        'end_date': END_DATE,
        'location_id': LOCATION_ID_VALID
    }


def dr_with_invalid_location_id():
    """
    Return host details with location_id
    """
    return {
        'host': 30303842,
        'start_date': START_DATE,
        'end_date': END_DATE,
        'location_id': LOCATION_ID_INVALID
    }


def enterprise_valid_data():
    """
    Returns valid mock post data for enterprise API
    """
    return {
        'brand': {'name': "test_case", 'url_path': "test_case"},
        'location': {'name': "q", 'address': "q",
                     'state': "q", 'pin_code': "q",
                     'city': "q", 'days': ["2"],
                     'time': "0:00",
                     "latitude": 33.1313381,
                     "longitude": -117.2809629
                     },
        'theme': {'background': "#dedac7",
                  'footer': "rgba(225, 101, 225, 1)",
                  'header': "rgba(124, 127, 147, 1)",
                  'logo': "default.png"
                  }
    }


def delete_brand_data():
    """
    :return: Valid Mocked Data for Delete brand API
    """
    return {
        'enterpriseId': -1,
        'domain': 'testing delete'
    }


def delete_brand_invalid_data():
    """
    :return: Invalid Mocked Data for Delete brand API
    """
    return {
        'enterpriseId': 'test',
        'domain': 'testing delete'
    }


def enterprise_blank_data():
    """
    Returns blank data for enterprise
    """
    return {
        'brand': {'name': "", 'url_path': ""
                  },
        'location': {'name': "", 'address': "",
                     'state': "", 'pin_code': "",
                     'city': "", 'days': "", 'time': ""
                     },
        'theme': {'background': "", 'footer': "",
                  'header': "", 'logo': ""
                  }
    }


def enterprise_invalid_data():
    """
    Returns invalid mock post data for enterprise API
    """
    return {
        'brand': {'name': "test_case", 'url_path': "test_case"
                  },
        'location': {'name': "", 'address': "q",
                     'state': "q", 'pin_code': "q",
                     'city': "q", 'days': ["2"],
                     'time': "0:00"
                     },
        'theme': {'background': "#dedac7",
                  'footer': "rgba(225, 101, 225, 1)",
                  'header': "rgba(124, 127, 147, 1)",
                  'logo': "default.png"
                  }
    }


def get_ad_id(valid):
    """
    Ad Id
    """
    data = {'adID': 10 if valid else 'test',
            'brandsDissociated': '2',
            }
    return data


def get_ad_data(put, valid):
    """
    Ad data for post request
    """
    data = {'adUrl': IMAGE_URL,
            'adDescription': IMAGE_DES,
            'adType': 'IMAGE',
            'adBrands': '2, 5, 10',
            'brandsDissociated': '2'
            }
    if put and valid:
        data['adID'] = 10
    return data


def get_file_data(myfile):
    """
    file data for post request
    :return:
    """
    data = {'file': myfile,
            'brand': 'abc',
            'media_type': 'image',
            'uniqueSetKey': 'ewru90324kncskd',
            'round': 'round1',
            'question': 'question1'}
    return data
