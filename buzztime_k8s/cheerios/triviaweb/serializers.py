"""
Serializer classes
"""
from rest_framework import serializers
from cheerios.triviaweb.models import Survey, Team, Game
from cheerios.serializers import SerializerValidator


class SurveySerializer(serializers.ModelSerializer):
    """
    Serializer for Survey Model to show field in doc.
    """
    class Meta:
        """
        Meta class for print fields in DRF doc.
        """
        fields = ('SurveyQuestionId', 'GameInstanceId',\
            'GameTeamId', 'Response')
        model = Survey


class TeamSerializer(serializers.ModelSerializer):
    """
    Serializer for Survey Model to show field in doc.
    """
    class Meta:
        """
        Meta class for print fields in DRF doc.
        """
        fields = ['Teams']
        model = Team


class GameSerializer(serializers.ModelSerializer):
    """
    Serializer for Survey Model to show field in doc.
    """
    class Meta:
        """
        Meta class for print fields in DRF doc.
        """
        fields = ('QuestionId', 'HostId', 'stumpGameId',\
            'VenueId', 'GameStartTime', 'GameToken')
        model = Game

class UserSerializer(SerializerValidator):
    """
    Serializer for Account Verification to validate fields.
    """
    user_id = serializers.IntegerField(required=True)

class AccountVerificationSerializer(SerializerValidator):
    """
    Serializer for Account Verification to validate fields.
    """
    token = serializers.CharField(required=True)

class LoginSerializer(SerializerValidator):
    """
    Serializer for Account Verification to validate fields.
    """
    password = serializers.CharField(required=True)
    email = serializers.CharField(required=True)

class GameAPISerializer(SerializerValidator):
    """
    Serializer for game Model to validate fields.
    """
    gameKey = serializers.CharField(required=True)

class TeamAPISerializer(SerializerValidator):
    """
    Serializer for team Model to validate fields.
    """
    score = serializers.CharField(required=True)
    stump_game_instance_id = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True)

class SurveyResponseSerializer(SerializerValidator):
    """
    Serializer for survey response Model to validate fields.
    """
    SurveyQuestionId = serializers.IntegerField(required=True)
    Response = serializers.CharField(required=True, allow_blank=True)


class GameDetailSerializer(SerializerValidator):
    """
    Serializer for game details Model to validate fields.
    """
    page = serializers.CharField(required=True)
    hostId = serializers.IntegerField(required=True)
    stumpGameId = serializers.IntegerField(required=True)
    venueId = serializers.IntegerField(required=True)


class AuthenticationSerializer(SerializerValidator):
    """
    Serializer for authentication class
    """
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)


class AuthorizationSerializer(SerializerValidator):
    """
    Serializer for authentication class
    """
    PlayerID = serializers.CharField(required=True)


class UserIdSerializer(SerializerValidator):
    """
    Serializer for authentication class
    """
    playerId = serializers.IntegerField(required=True)


class RemoveQuestionSerializer(SerializerValidator):
    """
    Meta class for print fields in DRF doc.
    """
    StumpQuestionSetID = serializers.IntegerField(required=True)
    StatusID = serializers.IntegerField(allow_null=True, required=False)


class QuestionListSerializer(SerializerValidator):
    """
    Serializer for question list.
    """
    Question = serializers.CharField(required=True)
    StumpGameId = serializers.IntegerField(allow_null=True, required=False)
    ParentSetId = serializers.IntegerField(allow_null=True, required=False)
    QuestionSetName = serializers.CharField(required=True)
    BrandingId = serializers.CharField(required=True)
    days = serializers.CharField(required=True)
    uniqueSetKey = serializers.CharField(allow_null=True, required=False)



class QuestionListPutSerializer(SerializerValidator):
    """
    Serializer for question list.
    """
    question_id = serializers.IntegerField(required=True)
    question = serializers.CharField(required=True)
    days = serializers.CharField(required=True)
    branding_id = serializers.IntegerField(required=True)
    survey_add_flag = serializers.CharField(required=True)


class QuestionSetSerializer(SerializerValidator):
    """
    Serializer for question set.
    """
    round_name = serializers.CharField(required=True)
    round = serializers.IntegerField(required=True)
    category = serializers.CharField(required=True)
    question_id = serializers.IntegerField(required=True)
    question_text = serializers.CharField(required=True)
    correct_answer = serializers.CharField(required=True)
    round_description = serializers.CharField(required=True)
    type_of_question = serializers.CharField(required=True)
    fun_fact = serializers.CharField(required=False, allow_blank=True, max_length=150)


class SurveyQuestionSerializer(SerializerValidator):
    """
    Serializer for Survey question.
    """
    category = serializers.CharField(required=True)
    question_id = serializers.IntegerField(required=True)
    question_text = serializers.CharField(required=True)

class SurveySetSerializer(SerializerValidator):
    """
    Serializer for  Survey set.
    """
    survey_name = serializers.CharField(required=True)

class DraftQuestionSerializer(SerializerValidator):
    """
    Serializer for validating drafts
    """
    branding_id = serializers.CharField(required=True)
    days = serializers.ListField(required=True)
    draft_time = serializers.CharField(required=True)
    name = serializers.CharField(required=True)
    question = serializers.CharField(required=True)
    player_id = serializers.CharField(required=True)


class SignUpSerializer(SerializerValidator):
    """
    Serializer for the Sign up Form
    """
    username = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    firstName = serializers.CharField(required=True)
    lastName = serializers.CharField(required=True)
    role = serializers.CharField(required=True)
    phoneNumber = serializers.CharField(allow_null=True, required=False)
    whitelabel_id = serializers.CharField(allow_null=True, required=False)

class UserUpdateSerializer(SerializerValidator):
    """
    Serializer for the User role and status update
    """
    user_id = serializers.IntegerField(required=True)
    role = serializers.CharField(allow_null=True, required=False)
    status = serializers.CharField(allow_null=True, required=False)
    delete_role = serializers.CharField(allow_null=True, required=False)

class UserProfileUpdateSerializer(SerializerValidator):
    """
    Serializer for the updating User profile
    """
    username = serializers.CharField(allow_null=True, required=False)
    firstName = serializers.CharField(required=True)
    lastName = serializers.CharField(required=True)
    phoneNumber = serializers.CharField(allow_null=True, required=False)
    user_id = serializers.IntegerField(required=True)

class EnterpriseSerializer(SerializerValidator):
    """
    Serializer for the getting Enterprise details
    """
    enterpeise_id = serializers.IntegerField(required=True)
    domain = serializers.CharField(required=True)
    name = serializers.CharField(required=True)


class UserGetSerializer(SerializerValidator):
    """
    Serializer for getting user details
    """
    user_id = serializers.IntegerField(required=True)
    username = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    firstName = serializers.CharField(required=True)
    lastName = serializers.CharField(required=True)
    role = serializers.CharField(required=True)
    status = serializers.CharField(required=True)
    phoneNumber = serializers.CharField(allow_null=True, required=False)
    enterprise_id = serializers.IntegerField(required=True)

class EmailSerializer(SerializerValidator):
    """
    Serializers for sending emails
    """
    user_id = serializers.CharField(required=True)
    username = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    domain = serializers.CharField(required=True)
    activation_id = serializers.CharField(required=True)
    message = serializers.CharField(required=False, allow_null=True)
    enterprise = serializers.CharField(required=False, allow_null=True)

class SuperAdminEmailSerializer(SerializerValidator):
    """
    Serializers for super admin emails
    """
    domain = serializers.CharField(required=True)
    email_id = serializers.CharField(required=True)
    removed_user_id = serializers.CharField(required=True)

class EnterprisePostSerializer(SerializerValidator):
    """
    Serializers for post brand details
    """
    name = serializers.CharField(required=True)
    url_path = serializers.CharField(required=True)
    logo = serializers.CharField(required=True)
    header = serializers.CharField(required=True)
    footer = serializers.CharField(required=True)
    background = serializers.CharField(required=True)

class LocationSerializer(SerializerValidator):
    """
    Serializers for location
    """
    brand = serializers.CharField(required=True)
    name = serializers.CharField(required=True)
    city = serializers.CharField(required=True, allow_blank=True)
    state = serializers.CharField(required=True, allow_blank=True)
    address = serializers.CharField(required=True)
    pin_code = serializers.CharField(required=True, allow_blank=True)
    days = serializers.ListField(required=True)
    time = serializers.CharField(required=True)
    latitude = serializers.FloatField(required=True)
    longitude = serializers.FloatField(required=True)

class CustomizeSerializer(SerializerValidator):
    """
    Serializer to get white labeling details
    """
    id = serializers.IntegerField(required=True)
    footer = serializers.CharField(required=True)
    header = serializers.CharField(required=True)
    background = serializers.CharField(required=True)

class GameSummary(SerializerValidator):
    """
    Serializer for data reporting
    """
    user_id = serializers.IntegerField(allow_null=True, required=False)
    start_date = serializers.DateTimeField(required=True)
    end_date = serializers.DateTimeField(required=True)
    location_id = serializers.IntegerField(allow_null=True, required=False)


class AdPutSerializer(SerializerValidator):
    """
    Serializer for ad management PUT request
    """
    adID = serializers.IntegerField(required=True)
    adDescription = serializers.CharField(allow_null=True, required=False)
    adURL = serializers.CharField(allow_null=True, required=False)
    adType = serializers.CharField(allow_null=True, required=False)
    adBrands = serializers.CharField(allow_null=True, required=False)


class AdPostSerializer(SerializerValidator):
    """
    Serializer for ad management POST request
    """
    adDescription = serializers.CharField(allow_null=True, required=False)
    adURL = serializers.CharField(allow_null=True, required=False)
    adType = serializers.CharField(allow_null=True, required=False)
    adBrands = serializers.CharField(allow_null=True, required=False)


class DeleteAdSerializer(SerializerValidator):
    """
    Serializer for ad management DELETE request
    """
    adID = serializers.IntegerField(required=True)

class AdGetSerializer(SerializerValidator):
    """
    Serializer for ad management GET request
    """
    brand_id = serializers.IntegerField(allow_null=True, required=False)

class DeleteBrandSerializer(SerializerValidator):
    """
    Serializer for deleting a brand
    """
    enterpriseId = serializers.IntegerField(required=True)
    domain = serializers.CharField(allow_null=True, required=False)

class BrandActivationSerializer(SerializerValidator):
    """
    Serializer for deleting a brand
    """
    brand_name = serializers.CharField(required=True)
    brand_key = serializers.CharField(required=True)
