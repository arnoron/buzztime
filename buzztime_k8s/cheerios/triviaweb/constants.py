"""
Defining all constants for triviaweb in this file
"""
EXCEPTION_CODE = 500
VALUE_ERROR = 400
SUCESS_STATUS = 200
NOT_MODIFY_STATUS = 304
DB_REF_ERROR = "UNIQUE KEY constraint"
DEFAULT_THEME_NAME = "default"
DEFAULT_THEME_BRAND = 1
TRIVIA_WEB_PREFIX = '/triviaweb/'
LOGIN_URL = '/triviaweb/login/'
SURVEY_URL = '/triviaweb/survey'
SIGNUP_URL = '/triviaweb/signup/'
PREFLIGHT_SURVEY_URL = '/triviaweb/survey/'
SESSION_EXPIRY = 9 * 60 * 60
QUESTION_API = '/triviaweb/question/'
CUSTOMIZE_API = '/triviaweb/setting/'
ROUND_QUESTIONS = 5
BONUS_ROUND = 11
ACCOUNT_VERIFICATION = '/triviaweb/account_verify/'
USER_URL = '/triviaweb/users/'
ENTERPRISE_URL = '/triviaweb/enterprises/'
GET_USER = '/triviaweb/get-user/'
CHANGE_PASSWORD = '/triviaweb/change_password/'
DATA_REPORTING_URL = '/triviaweb/game_summary/'
POST_API = "POST"
PUT_API = "PUT"
SAVE_GAME = "/triviaweb/game/"
FORGOT_PASSWORD = "/triviaweb/forgot-password/"
GET_DAYS = "/triviaweb/days/"
SUPER_ADMIN = "/triviaweb/super-admin/"
SUPER_ADMIN_EMAIL = "/triviaweb/super-admin-email/"
LOCATION_API = "/triviaweb/locations/"
ENTERPRISE_API = "/triviaweb/enterprises/"
DRAFT_QUESTION_API = "/triviaweb/draft-questions/"
DELETE_API = "DELETE"
GET_API = "GET"
INVALID_PARAM_ERROR = "ValueError Invalid Parameter passed"
NO_DATA_FOUND = "No data for selected date range"
Survey = "survey"
AD_API = "/triviaweb/ads/"

"""
Use for SALT variables
"""
RANDOM_CHOICE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
"""
Ignore URLS are all those URLs which do not need session management in trivia
"""

IGNORE_URL = [LOGIN_URL, SURVEY_URL, PREFLIGHT_SURVEY_URL, SIGNUP_URL, \
              ACCOUNT_VERIFICATION, CUSTOMIZE_API, FORGOT_PASSWORD, SUPER_ADMIN_EMAIL]

STORE_PROC = {"post_game_instances": "dbo.usp_ins_StumpGameInstance",
              "save_single_team": "dbo.usp_ins_StumpGameTeam",
              "get_player_auth": "dbo.usp_sel_PlayerAuthentication",
              "get_trivia_host": "dbo.usp_sel_StumpTriviaUser",  # "dbo.usp_sel_StumpTriviaHost",
              "get_username_from_email": "dbo.usp_sel_PlayerUsernameOnEmail",
              "registration": "UserDB.dbo.usp_ins_Registration",
              "registration_user": "UserDB.dbo.usp_ins_StumpTriviaUser",
              "update_status": "UserDB.dbo.usp_upd_StumpTriviaUserStatus",
              "get_users": "UserDB.dbo.usp_sel_StumpTriviaUserByBrandingID",
              "get_survey": "dbo.usp_sel_StumpSurvey",
              "save_survey_response": "dbo.usp_ins_StumpSurveyQuestionResponse",
              "get_question_set": "dbo.usp_sel_StumpQuestionSet",
              "get_question_set_list": "dbo.usp_sel_StumpQuestionSetList",
              "save_survey_set": "dbo.usp_ins_StumpSurvey",
              "save_question_set": "dbo.usp_ins_StumpQuestionSet",
              "save_question": "dbo.usp_ins_StumpQuestion",
              "save_survey": "dbo.usp_ins_StumpSurveyQuestion",
              "delete_question_set": "dbo.usp_upd_StumpQuestionSetStatus",
              "survey_question_type": "dbo.usp_sel_StumpSurveyQuestionTypeList",
              "get_all_roles": "UserDB.dbo.usp_sel_StumpTriviaRoleList",
              "get_all_status": "UserDB.dbo.usp_sel_StumpTriviaUserStatusList",
              "get_white_labeling_by_id": "dbo.usp_sel_StumpTriviaBrandingByID",
              "get_user_roles": "UserDB.dbo.usp_sel_StumpTriviaUserRole",
              "get_theme": "dbo.usp_sel_StumpTriviaBranding",
              "update_theme": "dbo.usp_upd_StumpTriviaBrandingGA",
              "insert_user_role": "UserDB.dbo.usp_ins_StumpTriviaUserRole",
              "get_host_by_branding": "dbo.usp_sel_StumpGamesbyBrandingID",
              "get_all_brands": "dbo.usp_sel_StumpGamesAllBranding",
              "get_host_details": "dbo.usp_sel_StumpGamesbyHostPlayerID",
              "get_users_from_db": "UserDB.dbo.usp_sel_StumpTriviaUserSearch",
              "update_user_profile_details": "UserDB.dbo.usp_upd_StumpTriviaUser",
              "get_theme_by_id": "dbo.usp_sel_StumpTriviaBrandingByID",
              "delete_user_role": "UserDB.dbo.usp_del_StumpTriviaUserRole",
              "get_enterprise_list": "dbGamePlay.dbo.usp_sel_StumpTriviaBrandingList",
              "get_enterprise": "dbo.usp_sel_StumpTriviaBrandingList",
              "get_locations_by_site_id": "dbo.usp_sel_StumpSitebyID",
              "get_locations_by_brand": "usp_sel_StumpSiteListbyBrandingID",
              "update_existing_location": "dbo.usp_upd_StumpSite",
              "insert_new_location": "dbo.usp_ins_StumpSite",
              "soft_delete_location": "usp_upd_StumpSiteStatus",
              "get_super_admin": "UserDB.dbo.usp_sel_StumpTriviaAdmin",
              "update_super_admin_email": "UserDB.dbo.usp_upd_StumpTriviaUserAdminNotification",
              "update_question_days": "dbo.usp_upd_StumpQuestionSetDays",
              "update_question": "dbo.usp_upd_StumpQuestion",
              "save_brand_with_theme": "dbo.usp_ins_StumpTriviaBranding",
              "get_all_ads": "dbo.usp_sel_StumpTriviaAd",
              "get_ad_by_brand": "dbo.usp_sel_StumpTriviaAdByBranding",
              "get_brands_by_ad": "dbo.usp_sel_StumpTriviaBrandingByAd",
              "upload_ad": "dbo.usp_ins_StumpTriviaAd",
              "associate_ad": "dbo.usp_ins_StumpTriviaBrandingAd",
              "update_ad": "dbo.usp_upd_StumpTriviaAd",
              "disassociate_ad": "dbo.usp_del_StumpTriviaBrandingAd",
              "delete_ad": "dbo.usp_del_StumpTriviaAd",
              "delete_question": "dbo.usp_del_StumpQuestion",
              "delete_survey": "dbo.usp_del_StumpSurveyQuestion",
              "soft_delete_brand": "dbo.usp_upd_StumpTriviaBrandingStatus",
              "update_user_brand": "dbo.usp_upd_StumpTriviaUserBrand"
              }

JWT_AUTH_TIME_INTERVAL = 60 * 60 * 24 * 7

POST_AUTH_URL = [CUSTOMIZE_API]

USER_ROLES = {'GROUP_ADMIN': 'group-admin',
              'HOST': 'host',
              'SUPER_ADMIN': 'super-admin'}

STUMPTRIVIA_TESTADS_CONSTANTS = {
    'StumpTriviaAdID': [{'StumpTriviaAdID': 127}],
    'StumpTriviaBrandingID': {'StumpTriviaBrandingID': 123}
}

ALL_ENTERPRISES = "All Enterprises"

SHORT_API_CACHE_TIME = 60 * 5

TIME_START = ' 00:00:00'
TIME_END = ' 23:59:59'

EMAIL_POOL_SIZE = 5

EXTRA_SET = 'Extra Set'

EXTRA_SET_NUMBER = '8'

MSSQL_FETCH_ALL_PATH = "cheerios.interface.msssql_connection.MSSQLApiView.fetchall"

BONUS = 'bonus'
TIE = 'tie'
ROUND_VALIDATE = 2

EXTRA_ROUNDS = ['Bonus', 'Tie']

API_METHODS = {'GET': 'GET', 'PUT': 'PUT', 'POST': 'POST', 'DELETE': 'DELETE'}

QUE_EDIT_MODE = {'ADD': 'ADD', 'EDIT': 'EDIT', 'DELETE': 'DELETE'}

CHOICE_ROW_DEFAULT = {x: {'url': "Option {0}".format(x), 'score': "", 'type': "Text", \
                          'answer': False} for x in range(1, 5)}

QUESTION_TYPES = {'MultipleChoice': 'MCQ', 'Open': 'Text', 'TrueFalse': 'True/False'}

QUESTION_TYPES_MAP = {'TEXT': 1, 'MCQ': 2, 'True/False': 3}

DRAFT_DATA = [[{'days': ['1', '2'], 'question': [], 'name': 'Sample', 'player_id': 12345,
                'id': 2, 'branding_id': '0', 'draft_time': '2018-02-22T05:17:07.619Z'}],
              [{'days': ['1', '2'], 'question': [], 'name': 'Sample', 'player_id': 12345,
                'id': 0, 'branding_id': '0', 'draft_time': '2018-02-22T05:17:07.619Z'}]]
import pickle

DRAFT_QUESTION = pickle.dumps([{'days': ['1', '2'], 'question': [], 'name': 'Sample',
                                'player_id': 12345, 'id': 2, 'branding_id': '0',
                                'draft_time': '2018-02-22T05:17:07.619Z'}])

QUESTIONS = pickle.dumps(
    {
        'question_id': "1",
        'question_text': "Test Question",
        'category': 'Test Category',
        'round': '1',
        'correct_answer': 'William Golding',
        'round_description': 'Test Round',
        'fun_fact': 'Testing in bytes',
        'media_url': 'test_url',
        'media_path': 'test_path'

    }
)
