"""
API view For Document like post, get method for any survey.
"""
from rest_framework.response import Response
from rest_framework import status
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.utils import CheeriosUtils
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.triviaweb.constants import USER_ROLES, STORE_PROC
from cheerios.triviaweb.views.enterprise import EnterpriseViewSet
from cheerios.triviaweb.serializers import BrandActivationSerializer

CHEERIOS_UTILS = CheeriosUtils()

class EnterpriseActivationViewSet(MSSQLApiView):
    """
    API To get activate an enterprise on the basis of key/name
    """
    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def post(self, _request):
        """
        Post API to add a new enterprise to the database along with location and theme
        """

        serializer = BrandActivationSerializer(data=_request.data)
        CHEERIOS_UTILS.validate_serializer(serializer)

        brand_name = _request.data.get('brand_name')
        brand_key = _request.data.get('brand_key')
        store_procedure = STORE_PROC['get_enterprise_list']

        with self.connection_manager(store_procedure, "", MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)

        result = filter(lambda x: x['StumpTriviaBrandingName'].lower() == brand_name.lower() or x['URLPath'].lower() == brand_key.lower(), result)
        result = list(result)
        enterprise_id = result[0]['StumpTriviaBrandingID']

        store_procedure = STORE_PROC['soft_delete_brand']
        store_proc_params = "@StumpTriviaBrandingID={0}, @StumpTriviaBrandingStatusID={1}".format(enterprise_id, 1)

        # Activating the brand
        enterprise_viewset = EnterpriseViewSet()
        enterprise_viewset.delete_or_activate_brand(store_proc_params, store_procedure,  brand_key)

        return Response(enterprise_id, status=status.HTTP_200_OK)