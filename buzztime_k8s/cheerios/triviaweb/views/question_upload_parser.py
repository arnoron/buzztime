import logging
import csv
import collections
import io
from rest_framework.views import APIView
from rest_framework.response import Response
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.triviaweb.constants import USER_ROLES
LOGGER = logging.getLogger("buzztime")


class QuestionParserViewSet(APIView):
    """
    API To get and parse the question set which has been uploaded
    """
    def __init__(self):
        super().__init__()
        # Question parameters which would be used in our named tuples
        self.ques_param_new = 'round category round_description question_id type_of_question question_text' \
                              ' question_image option1 option2 option3 option4 ' \
                              'correct_answer score fun_fact'
        self.ques_param_old = 'round category round_description question_id question_text correct_answer fun_fact'
        # Named tuples used for easier access of our variables
        self.QuestionSet_new = collections.namedtuple('QuestionSet', self.ques_param_new)
        self.QuestionSet_old = collections.namedtuple('QuestionSet', self.ques_param_old)
        self.is_decoder_type_latin = False

    @get_store_procedure_api
    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    def post(self, _request):
        """
        Question Parse API
        :return:
        """
        # Fetch the file from the request object and decode to string to make sense of it
        _file = _request.FILES['file'].read()
        try:
            _file = _file.decode('UTF-8')
        except UnicodeDecodeError:
            _file = _file.decode('latin-1')
            self.is_decoder_type_latin = True
        # The parse fn function is going to return a generator.
        # This is a performance enhancement but it would be required to be converted into
        # a dictionary.
        result = [el._asdict() for el in self.parse_fn(_file) if el]
        return Response(result, status=200)

    def parse_fn(self, _file):
        """
        Parser function which will take the file name and the correct format for parsing
        """
        _file = io.StringIO(_file)
        # Perform csv reader parsing with the required delimiters
        lines = csv.reader(_file, delimiter=',', quotechar='"')
        # Skip the first line, it is just the headings

        q_len = len(self.ques_param_new.split(' '))
        next(lines)
        for row in lines:
            if len(row) < 10:
                q_len = len(self.ques_param_old.split(' '))
            # We need only a specfic number of records so we maintain their length and split the
            # string with nulls accordingly
            req_records = row[:q_len]
            # THis is to append blank spaces if no value is given in the row
            # Because fun-fact is option user might not have that coloumn
            if (q_len - len(req_records) > 0):
                # Appending blank values
                req_records.append(""*(q_len - len(req_records)))
            if self.is_decoder_type_latin:
                # If the decoder type in latin then make sure that we are performing the other
                # parsing as well
                for index, _record in enumerate(req_records):
                    # Update all records to get the quotes properly
                    try:
                        encoded_req = req_records[index].encode('latin-1')
                        req_records[index] = encoded_req.decode('windows-1252')
                    except UnicodeDecodeError:
                        req_records[index] = encoded_req.decode('UTF-8')
            if len(row) < 10:
                question_record = self.QuestionSet_old(*req_records)
            else:
                question_record = self.QuestionSet_new(*req_records)
            yield question_record