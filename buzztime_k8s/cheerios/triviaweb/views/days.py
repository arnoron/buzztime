"""
API view For Document like post, get method for any survey.
"""
import json
from rest_framework.response import Response
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.constants import EXTRA_SET


class DaysViewSet(MSSQLApiView):
    """
    API To get List of days
    """
    def get(self, _request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of days
        Returns list of days.
        """
        with open('cheerios/triviaweb/views/days.json') as result:
            result = json.load(result)
        if not _request.GET.get('extra-set'):
            result = filter(lambda x: x['name'] != EXTRA_SET, result)
        if _request.GET.get('location-page'):
            result = [obj if obj['name'] != 'Extra Set' \
                      else {'id': obj['id'], 'name': 'All Days'} for obj in result]
        return Response(result, status=200)
