"""
APIs for user profile updation in stump
"""
from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api
from cheerios.settings import MSSQLUSERDB
from cheerios.triviaweb.constants import STORE_PROC
from cheerios.triviaweb.utils.utils import CustomConverter
from cheerios.triviaweb.serializers import UserSerializer
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()


class UserGetViewSet(MSSQLApiView):
    """
    API viewset for user profile updation
    """
    @get_store_procedure_api
    def get(self, request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of users
        Returns list of users.
        """
        request_params = request.GET
        serializer = UserSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)

        user_id = request_params.get("user_id")
        store_procedure = STORE_PROC['get_trivia_host']
        with self.connection_manager(store_procedure, "@PlayerID={0}".format(user_id),\
                                     MSSQLUSERDB) as cursor:
            result = self.fetchall_from_cursor(cursor)
        if not result:
            return Response("", status=200)
        converter = [('UserName', 'username'), ('LastName', 'lastName'),\
                     ('FirstName', 'firstName'), ('PlayerID', 'user_id'),\
                     ('Email', 'email'), ('StumpTriviaRoleDesc', 'roles'),\
                     ('PhoneNumber', 'phoneNumber'), ('StumpTriviaBrandingID', 'enterprise_id')]
        result = CustomConverter(result).convert(converter)
        return Response(result[0], status=200)
