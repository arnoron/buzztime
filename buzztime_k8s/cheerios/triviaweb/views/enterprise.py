"""
API view For Document like post, get method for any survey.
"""
from rest_framework.response import Response
from rest_framework import status
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.settings import MSSQLUSERDB
from cheerios.utils import CheeriosUtils
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.serializers import DeleteBrandSerializer
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.triviaweb.utils.utils import CustomConverter
from cheerios.triviaweb.constants import USER_ROLES, STORE_PROC
from cheerios.triviaweb.views.locations import LocationViewSet
from cheerios.triviaweb.views.customize import CustomizeViewSet
from cheerios.triviaweb.serializers import EnterprisePostSerializer, \
    LocationSerializer
from cheerios.triviaweb.views.user import UserViewSet
from cheerios.triviaweb.utils.utils import remove_cache

CHEERIOS_UTILS = CheeriosUtils()

class EnterpriseViewSet(MSSQLApiView):
    """
    API To get List of enterprises
    """
    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def get(self, _request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of enterprise
        Returns list of enterprise.
        """
        # store procedure to get enterprise(s) data
        # serializer call to check if fetched data is valid or not
        store_procedure = STORE_PROC['get_enterprise_list']
        with self.connection_manager(store_procedure, "", MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
            converter = [('StumpTriviaBrandingName', 'name'),\
                         ('StumpTriviaBrandingID', "enterprise_id")]
            result = CustomConverter(result).convert(converter)
        result = filter(lambda x: x['FooterColor'] and x['StumpTriviaBrandingStatusID'] and
                        any(s in x['FooterColor'] for s in ['#', 'rgb']),\
                        result)
        return Response(result, status=200)

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def post(self, _request):
        """
        Post API to add a new enterprise to the database along with location and theme
        """
        brand = _request.data.get('brand')
        location = _request.data.get('location')
        theme = _request.data.get('theme')
        data = brand.copy()
        data.update(theme)

        # serializer call to check if brand data is valid or not
        validator = EnterprisePostSerializer(data=data)
        if not validator.is_valid():
            raise ValueError(validator.errors)
        url_path = brand.get('url_path')
        name = brand.get('name')

        customize_view_set = CustomizeViewSet()
        logo_name = customize_view_set.save_onboarding_logo(theme)

        params = "@StumpTriviaBrandingName='{0}', @URLPath='{1}', @Logo='{2}', @HeaderColor='{3}',\
                  @FooterColor='{4}', @BackgroundImage='{5}'".\
                  format(name, url_path, logo_name, theme.get('header'),\
                         theme.get('footer'), theme.get('background'))
        store_procedure = STORE_PROC['save_brand_with_theme']
        # making the stored procedure call to save brand and theme
        result = self.call_brand_sp(params, store_procedure)
        if isinstance(result, list) and 'StumpTriviaBrandingID' in result[0]:
            location['brand'] = result[0].get('StumpTriviaBrandingID')
            location_response = self.post_location(location)
            if location_response.status_code == status.HTTP_200_OK:
                return Response(result, status=200)
            else:
                return location_response
        else:
            return Response(result, status=500)

    @staticmethod
    def post_location(location):
        """
        function to store location against the brand created
        """
        location_view_set = LocationViewSet()
        # serializer call to check if location data is valid or not
        validator = LocationSerializer(data=location)
        if validator.is_valid():
            location_response = location_view_set.insert_new_location(location)
            return location_response
        else:
            raise ValueError(validator.errors)

    def call_brand_sp(self, params, store_procedure):
        """
        function to call brand store procedure
        """

        with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
            return result

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def delete(self, _request):
        """
        Delete API to delete the brand with the given location ID
        """
        serializer = DeleteBrandSerializer(data=_request.data)
        CHEERIOS_UTILS.validate_serializer(serializer)

        enterprise_id = _request.data.get('enterpriseId')
        domain = _request.data.get('domain')
        params = "@StumpTriviaBrandingID={0}".format(enterprise_id)
        store_procedure = STORE_PROC["get_users_from_db"]

        #Deleting hosts
        self.delete_users(params, store_procedure)

        store_procedure = STORE_PROC['soft_delete_brand']
        store_proc_params = "@StumpTriviaBrandingID={0}, @StumpTriviaBrandingStatusID={1}".format(enterprise_id, 0)

        #Deleting Brand
        self.delete_or_activate_brand(store_proc_params, store_procedure, domain)

        return Response(True, status=status.HTTP_200_OK)

    def delete_users(self, params, store_procedure):
        """
        Delete all the users from a brand
        :param params: params for SP
        :param store_procedure: SP name
        :return: none
        """
        # Fetching all the users
        with self.connection_manager(store_procedure, params, MSSQLUSERDB, False) as cursor:
            result = self.fetchall_from_cursor(cursor)

        # Setting Status to de "Deleted" for hosts
        user_viewset = UserViewSet()
        for user in result:
            user_id = user.get("PlayerID")
            if user_id:
                user_viewset.delete_user(user_id)

    def delete_or_activate_brand(self, store_proc_params, store_procedure, domain):
        """
        Soft deletes a brand and removes the corresponding cache
        :param store_proc_params: Params for SP
        :param store_procedure: SP name
        :param domain: brand id
        :return: none
        """
        with self.connection_manager(store_procedure, store_proc_params, MSSQLDBGAMEPLAY, True) as cursor:
            self.fetchall_from_cursor(cursor)

        if domain:
            remove_cache(key=domain)
        else:
            remove_cache(key="default")