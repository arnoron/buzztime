"""
API view for game operation.
"""
from datetime import datetime

from cheerios.triviaweb.utils.firebase import FirebaseService
from cheerios.triviaweb.utils.utils import GameShouldNotDeleteException
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.serializers import GameDetailSerializer
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.triviaweb.constants import STORE_PROC
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

CHEERIOS_UTILS = CheeriosUtils()


class GameViewSet(MSSQLApiView, FirebaseService):
    """
    API to save game detail
    # It will create connection with MS SQL server
    # using mypool object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure for saving game details.
    # API should contain StumpQuestionSetID, HostPlayerID,
    StumpGameID, VenueID, GameStartTime, GameToken.
    # API will save game instance in MSSQL database.
    """
    def post_game(self, game_token, cursor, game_obj):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure store game instance and return id of it.
        """
        try:
            Logger.log.info("---in post game---")
            serializer = GameDetailSerializer(data=game_obj)
            CHEERIOS_UTILS.validate_serializer(serializer)
            page_value = game_obj.get('page')
            if page_value == "game-end":
                question_obj = game_obj.get('questions')
                if question_obj:
                    stump_question_set_id = question_obj.get('stump_question_set_id')
                    host_player_id = game_obj.get('hostId')
                    stump_game_id = game_obj.get('stumpGameId')
                    venue_id = game_obj.get('venueId')
                    game_start_time = game_obj.get('start_date')
                    game_start_time = datetime.strptime(game_start_time, '%a, %d %b %Y %H:%M:%S %Z')
                    game_start_time = datetime.strftime(game_start_time, '%Y-%m-%d %H:%M:%S')
                    data = (stump_question_set_id, host_player_id, stump_game_id,
                            venue_id, game_start_time, game_token)
                    CHEERIOS_UTILS.required_parameters(data)
                    store_procedure = STORE_PROC["post_game_instances"]
                    params = "@StumpQuestionSetID={0}, @HostPlayerID={1}, \
                              @StumpGameID={2}, @StumpSiteID={3}, \
                              @GameStartTime='{4}', @GameToken='{5}'".\
                            format(stump_question_set_id, host_player_id, stump_game_id,
                                   venue_id, game_start_time, game_token)
                    with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY, \
                                                 False) as cursor:
                        result = self.fetchall_from_cursor(cursor)
                    return result[0].get('StumpGameInstanceID')
                else:
                    return ""
            else:
                Logger.log.info("---post game else---")
                message = "Game not yet finished"
                Logger.log.error(message)
                raise GameShouldNotDeleteException(message)
        except Exception:
            raise
