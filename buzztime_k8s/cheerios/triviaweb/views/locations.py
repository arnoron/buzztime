"""
API view For Document like post, get method for any survey.
"""
from rest_framework import status
from rest_framework.response import Response
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user, cache_api
from cheerios.triviaweb.utils.utils import CustomConverter, remove_cache, ping_redis_master
from cheerios.triviaweb.constants import USER_ROLES, STORE_PROC, LOCATION_API
from cheerios.triviaweb.serializers import LocationSerializer

class LocationViewSet(MSSQLApiView):
    """
    API To get List of Locations
    """
    @cache_api()
    # Not adding a security check as we need the locations for all loggedin users
    @allow_user(check_enterprise='brand_id', check_value_in='GET')
    @get_store_procedure_api
    def get(self, _request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of days
        Returns list of days.
        """
        # Using get for two use-cases:
        # 1. If the brand id is not there
        # 2. If the brand id is not present and we need to fetch location_id

        brand_id = _request.GET.get("brand_id")
        location_id = _request.GET.get("location_id")

        day = _request.GET.get("day")
        # Also handle the edge case if day is present then append it as a parameter
        store_proc_params = "@StumpSiteID={0}".format(location_id) if location_id else\
                            "@StumpTriviaBrandingID={0}".format(brand_id) if not day\
                            else "@StumpTriviaBrandingID={0}, @DaysPattern='%{1}%'".\
                            format(brand_id, day)
        # Check which store procedure needs to be called
        store_procedure = STORE_PROC["get_locations_by_site_id"] if location_id else\
                                STORE_PROC["get_locations_by_brand"]

        with self.connection_manager(store_procedure, store_proc_params, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
            converter = [('StumpTriviaBrandingID', 'Brand'), ('StumpSiteID', 'id'), \
                         ('Days', 'days'), ('StartTime', 'time'), ('SiteName', 'Name'), \
                         ('PostalCode', 'Pincode'), ('Address1', 'Address')]
            result = CustomConverter(result).convert(converter)

        if location_id:
            # If we have a location id then just fetch the location id
            result = result[0]
        return Response(result, status=status.HTTP_200_OK)

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']],\
                check_enterprise='brand')
    @get_store_procedure_api
    def post(self, _request):
        """
        Location insertion API
        :return:
        """
        # checks if Redis master is available
        ping_redis_master()

        location_data = LocationSerializer(data=_request.data)
        if location_data.is_valid():
            result = self.insert_new_location(location_data.validated_data)
        else:
            raise ValueError("Parameter value missing.")

        if result.status_code == status.HTTP_200_OK:
            # Remove cache of Locations from Redis
            remove_cache(key=LOCATION_API)
        return result

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']],\
                check_enterprise='brand')
    @get_store_procedure_api
    def put(self, _request):
        """
        Location updation API
        :return:
        """
        location_id = _request.data.get('location_id')
        location_data = LocationSerializer(data=_request.data)
        if location_data.is_valid():
            result = self.update_existing_location(location_data.validated_data, location_id)
        else:
            raise ValueError("Parameter value missing.")
        if result.status_code == status.HTTP_200_OK:
            # Remove cache of Locations from Redis
            remove_cache(key=LOCATION_API)
        return result

    @get_store_procedure_api
    def insert_new_location(self, data):
        """
        Function to insert into the store procedure
        """
        store_procedure = STORE_PROC['insert_new_location']
        days = "".join(data['days'])
        store_proc_params = "@SiteName='{0}', @Address1='{1}', @Address2={2}, @City='{3}',\
                            @State='{4}', @PostalCode='{5}', @Country='{6}', @StartTime='{7}',\
                            @Days='{8}', @StumpTriviaBrandingID={9}, @Longitude={10}, " \
                            "@Latitude={11}".format(data['name'], data['address'], None, \
                                                    data['city'], data['state'], data['pin_code'], \
                                                    'US', data['time'], days, int(data['brand']), \
                                                    data['longitude'], data['latitude'])
        result = self.post_call(store_procedure, store_proc_params, MSSQLDBGAMEPLAY)
        return Response(result, status=status.HTTP_200_OK)

    def post_call(self, store_procedure, store_proc_params, ms_db):
        """
        Post call with connection manager
        """
        with self.connection_manager(store_procedure, store_proc_params, ms_db, True) as cursor:
            return self.fetchall_from_cursor(cursor)
    @get_store_procedure_api
    def update_existing_location(self, data, location_id):
        """
        Function to update location into the store procedure
        """
        store_procedure = STORE_PROC['update_existing_location']
        days = "".join(data['days'])
        store_proc_params = "@StumpSiteID={0}, @SiteName='{1}', @Address1='{2}', @Address2={3},\
                             @City='{4}', @State='{5}', @PostalCode='{6}', @Country='{7}',\
                             @StartTime='{8}', @Days='{9}' , @Longitude={10}, @Latitude={11}".\
                             format(location_id, data['name'], data['address'], None, data['city'],\
                                    data['state'], data['pin_code'], 'US', data['time'], days, \
                                    data['longitude'], data['latitude'])
        result = self.post_call(store_procedure, store_proc_params, MSSQLDBGAMEPLAY)
        return Response(result, status=status.HTTP_200_OK)

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def delete(self, _request):
        """
        Delete API to delete the location with the given location ID
        """

        store_procedure = STORE_PROC['soft_delete_location']
        store_proc_params = "@StumpSiteID={0}, @StatusID=0".format(_request.GET.get('location_id'))
        result = self.post_call(store_procedure, store_proc_params, MSSQLDBGAMEPLAY)
        remove_cache(key=LOCATION_API)
        return Response(result, status=status.HTTP_200_OK)
