"""
API for Teams.
"""
from cheerios.triviaweb.utils.firebase import FirebaseService
from cheerios.triviaweb.utils.utils import UtilFunction,\
    GameShouldNotDeleteException
from cheerios.triviaweb.serializers import TeamAPISerializer
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.views.survey import SurveyViewSet
from cheerios.triviaweb.constants import STORE_PROC
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

from cheerios.services.log_classifier import Logger

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()
SURVEY_VIEW_SET = SurveyViewSet()


class TeamViewSet(MSSQLApiView, FirebaseService):
    """
    API to store teams objects
    # API which allow list of team objects.
    # Each Object contains stumpGameInstanceId, score,
    teamName, captainPlayerId, teamPlayerCount
    # captainPlayerId, teamPlayerCount can be null.
    # API will connect MSSQL and save each team object.
    # It will run in background.
    """

    def post_game(self, game_key, stump_game_instance_id, cursor, result):
        """
        Post API which allow to save list of team object.
        It will run on background.
        It will create thread. Thread will work in background.
        API will return 200 status without completion of thread.
        """
        message = "Team API called"
        Logger.log.info(message)

        required_values = [game_key, stump_game_instance_id]
        CHEERIOS_UTILS.required_parameters(required_values)
        UTIL_FUNCTION.is_positive_number(stump_game_instance_id)
        team_list = []
        answers = []
        if result:
            for key, value in result.items():
                if value["team-status"] == "accepted":
                    value['stump_game_instance_id'] = stump_game_instance_id
                    serializer = TeamAPISerializer(data=value)
                    CHEERIOS_UTILS.validate_serializer(serializer)
                    if value.get('answers'):
                        for answer_key in value['answers']:
                            answers.append(value['answers'][answer_key])
                    value['answers'] = answers
                    team_list.append(value)
            self.save_team_list(team_list, cursor)
        else:
            message = "Game doesn't have teams"
            Logger.log.error(message)
            raise GameShouldNotDeleteException(message)
        return team_list

    def save_team_list(self, teams, cursor):
        """
        Function accept list of teams.
        It will separate each team object.
        Each team object stores in MSSQL.
        """
        for team in teams:
            self.save_single_team(team, cursor)
        message = "Teams data is saved."
        Logger.log.info(message)

    def save_single_team(self, data, cursor):
        """
        Function accept one team object.
        Function call store procedure to save each team.
        """
        stump_game_instance_id = data.get('stump_game_instance_id')
        score = data.get('score')
        team_name = data.get('name')

        # captain_player_id = #data.get('captainPlayerId')
        # team_player_count = #data.get('teamPlayerCount')

        store_procedure = STORE_PROC["save_single_team"]
        params = dict(StumpGameInstanceID=stump_game_instance_id, Score=score, TeamName=team_name)
        with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY, True, cursor=cursor):
            result = self.fetchall_from_cursor(cursor)
        SURVEY_VIEW_SET.post_survey(data, stump_game_instance_id,\
            result[0].get('StumpGameTeamID'), cursor)
        message = "Data saved successfully Data:- {0}".format(data)
        Logger.log.info(message)
