"""
API view For Document like post, get method for any survey.
"""
import pickle
from rest_framework.response import Response
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user,\
    check_serializer_validation
from cheerios.triviaweb.constants import USER_ROLES
from cheerios.services.redis import RedisSession
from cheerios.triviaweb.serializers import DraftQuestionSerializer


class DraftViewSet(MSSQLApiView):
    """
    API for Question Drafts
    """
    @get_store_procedure_api
    def get(self, _request):
        """
        Get API to fetch all the drafts with the loggedd in user.
        """

        redis_key = "{0}_question_{1}".format(_request.GET.get('player_id'),\
                                              _request.GET.get('brandID'))
        redis_obj = RedisSession()

        user_drafts = self.fetch_user_draft(redis_key, redis_obj)
        draft_id = _request.GET.get('draft_id')
        # If the draft id is present then fetch that value
        if draft_id:
            user_drafts = user_drafts[int(draft_id)]

        return Response(user_drafts, status=200)

    @get_store_procedure_api
    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    @check_serializer_validation(DraftQuestionSerializer)
    def post(self, _request):
        """
        Draft insertion API
        :return:
        """
        # We need to now fetch the key parameters which we would use for our redis.
        # The session_id and the brandID would be fetched directly by the cookies which
        # have been set
        redis_key = "{0}_question_{1}".format(_request.data.get('player_id'),\
                                              _request.data.get('branding_id'))
        redis_obj = RedisSession()
        user_drafts = self.fetch_user_draft(redis_key, redis_obj)
        data = _request.data
        # Data manipulation for saving date and so on
        data['id'] = len(user_drafts)+1
        user_drafts.append(data)
        redis_obj.save_data(key=redis_key, value=pickle.dumps(user_drafts), persist=True)

        return Response(True, status=200)

    def fetch_user_draft(self, key, redis_obj):
        """
        Function to fetch the user drafts
        """
        # Fetch the value from Redis if there is already some value present
        # else map user_drafts as a single list and append the new draft

        user_drafts = redis_obj.get_value(key=key)

        return pickle.loads(user_drafts) if user_drafts else []


    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    @check_serializer_validation(DraftQuestionSerializer)
    @get_store_procedure_api
    def put(self, _request):
        """
        Draft updation API
        :return:
        """
        draft_id = int(_request.GET.get('draft_id'))
        # Fetching the correct Redis object which needs to be updated
        redis_key = "{0}_question_{1}".format(_request.data.get('player_id'),\
                                              _request.data.get('branding_id'))

        redis_obj = RedisSession()
        user_drafts = self.fetch_user_draft(redis_key, redis_obj)

        data = _request.data
        # Data manipulation for saving id of draft
        data['id'] = draft_id
        user_drafts[draft_id] = data
        redis_obj.save_data(key=redis_key, value=pickle.dumps(user_drafts), persist=True)
        return Response(True, status=200)

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def delete(self, _request):
        """
        Delete API to delete the draft
        """
        draft_id = int(_request.data.get('StumpQuestionSetID'))
        # Fetching the correct Redis object which needs to be updated
        redis_key = "{0}_question_{1}".format(_request.data.get('player_id'),\
                                              _request.data.get('branding_id'))
        redis_obj = RedisSession()

        user_drafts = self.fetch_user_draft(redis_key, redis_obj)
        user_drafts.pop(draft_id)
        redis_obj.save_data(key=redis_key, value=pickle.dumps(user_drafts), persist=True)
        return Response(True, status=200)
