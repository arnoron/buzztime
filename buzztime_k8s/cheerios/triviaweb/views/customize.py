"""
API for returning instruction for a game.
"""
import ast
import base64
import os
import random
import sys
from pathlib import Path
from io import BytesIO
from PIL import Image
from rest_framework.response import Response
from django.template.defaultfilters import lower
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.settings import MSSQLDBGAMEPLAY, IMAGE_SERVER_STP
from cheerios.triviaweb.serializers import CustomizeSerializer
from cheerios.services.redis import RedisSession
from cheerios.triviaweb.utils.utils import UtilFunction
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.triviaweb.constants import STORE_PROC, DEFAULT_THEME_BRAND, DEFAULT_THEME_NAME,\
    USER_ROLES
from cheerios.utils import CheeriosUtils
from cheerios.players.decorators import close_connection

CHERRIOS_UTILS = CheeriosUtils()

UTIL_FUNCTION = UtilFunction()


class CustomizeViewSet(MSSQLApiView):

    """
    API to get and POST Customization changes for the Website
    This function checks the request object 'Domain' passed,
    If Domain is passed call the api and get the specifications for that
    particular domain.
    If Domain is not passed return error saying Domain is mandatory
    """

    @get_store_procedure_api
    def get(self, request):
        """
        This is a default get function.
        This function will call get_theme_details function
        to get data
        :param self:
        :param request:
        :return:
        """
        request_params = request.GET
        domain = request_params.get('domain')
        domain = domain if domain else DEFAULT_THEME_NAME
        theme = self.get_theme_details(domain)

        return Response(theme, status=200)

    def get_theme_details(self, domain):
        """
        This function will get data from Redis for passed domain
        :param domain:
        :return:
        """

        cursor = self.get_cursor(MSSQLDBGAMEPLAY)
        response = self.fetch_theme(domain, cursor)
        return response

    def fetch_theme(self, domain, cursor):
        """
        fetching theme from redis or db.
        """
        redis = RedisSession()
        theme = redis.get_key(domain)

        if theme:
            response = ast.literal_eval(theme.decode("utf-8"))
        else:
            response = self.get_theme(domain, cursor)
            if response:
                try:
                    redis.save_data(domain, response)
                except:
                    CHERRIOS_UTILS.log_error(500, "Redis Master is down unable save data")
        return response

    def get_theme(self, domain, cursor):
        """
        Function to connect to backend and get data for passed
        Domain from DB and return it.
        :return:
        """
        if domain == "default":
            domain = DEFAULT_THEME_BRAND
            store_procedure = STORE_PROC["get_theme_by_id"]
            param = "@StumpTriviaBrandingID={0}".format(domain)
        else:
            store_procedure = STORE_PROC["get_theme"]
            param = "@URLPath={0}".format(domain)
        with self.connection_manager(store_procedure, param, MSSQLDBGAMEPLAY, \
                                     True) as cursor:
            result = self.fetchall_from_cursor(cursor)
        if result:
            return self.convert_theme_response(result)

    @staticmethod
    def convert_theme_response(result):
        """
        Converting result to theme response/ dict.
        """
        result_obj = result[0]
        if result_obj.get('StumpTriviaBrandingStatusID'):
            return_value = dict(id=result_obj.get('StumpTriviaBrandingID'),
                                footer=result_obj.get('FooterColor'),
                                background=result_obj.get('BackgroundImage'),
                                logo=result_obj.get('Logo'),
                                status=result_obj.get('StumpTriviaBrandingStatusID'),
                                header=result_obj.get('HeaderColor'))
            return return_value

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN'], USER_ROLES['GROUP_ADMIN']],
                check_enterprise='id')
    @get_store_procedure_api
    def post(self, request):
        """
        Default function to post data for enterprise theme in db
        and cache in Redis
        :param request:
        :return:
        """
        data_to_post = request.data
        serializer = CustomizeSerializer(data=data_to_post)
        CHERRIOS_UTILS.validate_serializer(serializer)
        self.save_theme_details(data_to_post)
        return Response(status=200)

    def save_theme_details(self, data_to_post):
        """
        Function to save theme data in db and Redis
        :param data_to_post:
        :return:
        """
        # Saving image in path Defined in Env variable IMAGE_SERVER_PATH
        logo = data_to_post.get('logo')
        domain = data_to_post.get('domain')
        domain = domain if domain else "default"

        cursor = self.get_cursor(MSSQLDBGAMEPLAY, True)
        theme = self.fetch_theme(domain, cursor)
        exists_image = theme.get("logo")
        data_to_post['logo'] = self.save_logo_to_nfs(logo, domain, exists_image)

        self.save_theme_db(data_to_post, cursor)
        result = self.get_theme(domain, cursor)
        if result:
            redis = RedisSession()
            redis.save_data(domain, result)
        if logo and exists_image:
            path = "{0}{1}".format(IMAGE_SERVER_STP, exists_image)
            my_file = Path(exists_image)
            if my_file.is_file():
                os.remove(path)
        if result:
            return Response(status=200)

    def save_theme_db(self, data_to_post, cursor):
        """
        Function to save the DB for theme
        :param data_to_post:
        :return:
        """
        store_procedure = STORE_PROC["update_theme"]
        footer = data_to_post.get("footer")
        background = data_to_post.get("background")
        branding_id = data_to_post.get("id")
        logo = data_to_post.get("logo")
        header = data_to_post.get("header")
        parameters = "@StumpTriviaBrandingID={0}, @Logo='{1}',\
                      @HeaderColor='{2}', @FooterColor='{3}', @BackgroundImage='{4}'"\
                      .format(branding_id, logo, header, footer, background)
        with self.connection_manager(store_procedure, parameters, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
        return result

    @close_connection
    def save_onboarding_logo(self, data_to_post):
        """
        Function to save logo while onboarding
        :param data_to_post:
        :return:
        """
        # Saving image in path Defined in Env variable IMAGE_SERVER_PATH
        logo = data_to_post.get('logo')
        domain = data_to_post.get('domain')
        domain = domain if domain else "default"

        cursor = self.get_cursor(MSSQLDBGAMEPLAY, True)
        theme = self.fetch_theme(domain, cursor)
        exists_image = theme.get("logo")
        logo_name = self.save_logo_to_nfs(logo, domain, exists_image)

        if logo and exists_image:
            path = "{0}{1}".format(IMAGE_SERVER_STP, exists_image)
            my_file = Path(exists_image)
            if my_file.is_file():
                os.remove(path)

        return logo_name

    def save_logo_to_nfs(self, logo, domain, exists_image):
        """
        Function to save logo to nfs
        """
        # Saving image in path Defined in Env variable IMAGE_SERVER_PATH
        if logo and "data:image" in logo:
            logo_image = base64.b64decode(logo.split(",")[1])
            img = Image.open(BytesIO(logo_image))
            postname = random.randint(0, sys.maxsize)
            img_name = "{0}_{1}.png".format(domain, postname)
            # Making sure that new name is not same as previous one
            while lower(exists_image) == lower(img_name):
                postname = random.randint(0, sys.maxsize)
                img_name = "{0}_{1}.png".format(domain, postname)

            img_path = "{0}{1}".format(IMAGE_SERVER_STP, img_name)
            img.save(img_path)
            return img_name
        else:
            return exists_image
