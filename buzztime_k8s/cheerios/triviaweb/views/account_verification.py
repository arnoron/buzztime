"""
APIs for user registration in stump
"""
from datetime import datetime

import jwt
from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import BUZZTIME_TRIVIAWEB_NAME
from cheerios.services.Email.email import Email
from cheerios.settings import MSSQLUSERDB
from cheerios.triviaweb.constants import JWT_AUTH_TIME_INTERVAL, USER_URL
from cheerios.triviaweb.utils.utils import remove_cache
from cheerios.triviaweb.serializers import AccountVerificationSerializer
from cheerios.triviaweb.decorators import get_store_procedure_api
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()


class AccountVerificationViewSet(MSSQLApiView):
    """
    API viewset for account verification
    """
    @get_store_procedure_api
    def get(self, request):
        """
        API to get the token and do the check for whether that token exists
        :return:
        """
        request_params = request.GET

        serializer = AccountVerificationSerializer(data=request_params)
        CHERRIOS_UTILS.validate_serializer(serializer)
        #Run jwt decode on the token sent as a parameter
        token = request_params.get('token')

        jwt_decrypted_data = jwt.decode(token, 'secret', algorithms=['HS256'])

        if jwt_decrypted_data.get('time'):
            #Check for whether the token has expired or not
            datetime_object = (datetime.utcnow() - CHERRIOS_UTILS.convert_str_to_datetime(\
                jwt_decrypted_data['time'])).total_seconds()
            if datetime_object < -60 or datetime_object > JWT_AUTH_TIME_INTERVAL:
                result = "Invalid JWT token. JWT token expired"
                message = "Error status code 400,  {0}".format(result)
                Logger.log.error(message)
                raise jwt.exceptions.InvalidTokenError(result)

        #Done to avoid cyclic dependency
        from cheerios.triviaweb.views.user import UserViewSet
        from cheerios.triviaweb.serializers import UserUpdateSerializer

        #Make a call to your role API class for updating the status
        #but first serialize the data

        data = UserUpdateSerializer(data={'user_id': jwt_decrypted_data['token'],
                                          'status': 'Active'})
        cursor = self.get_cursor(MSSQLUSERDB, True)
        game_cursor = self.get_cursor(MSSQLUSERDB, True)
        self.conn_flag = 1
        _result = UserViewSet().role_update(data, cursor, game_cursor) if data.is_valid() else None

        #Remove cache of users from Redis
        remove_cache(key=USER_URL)
        return Response(_result, status=200)

    def send_mail(self, form_data=None):
        """
        This function would be used to send the mail to the person who is requesting for it
        :params:
            form_data = {'name': display_name,
                         'email': email,
                         'username': username
                         'domain': 'domain'<this needs to be fetched from env> }
        """
        sender_details = {}
        if form_data:
            #This would need to be fetched from the db
            sender_details['email'] = 'PlayTrivia@buzztime.com'
            sender_details['name'] = BUZZTIME_TRIVIAWEB_NAME
            user_recipient_details = dict(email=form_data.get('email', None),
                                          username=form_data.get('username', 'User'))
            domain_name = form_data.get('domain', None)
            message = form_data.get('message', None)
            user_id = form_data.get('user_id', None)
            #Check whether the domain name and email is present in the form_data
            if not domain_name:
                Logger.log.error("Please add domain name in the body")
                raise ValueError("Invalid Parameter passed")
            if not user_recipient_details['email']:
                Logger.log.error("Please add the email in the body")
                raise ValueError("Invalid Parameter passed")
            # Check if all values are given to the form
            _check_list = list(filter(lambda x: not x, list(user_recipient_details.values())))
            if len(_check_list) > 0:
                Logger.log.error("Please add all the required fields to the form")
                raise ValueError("Invalid Parameter passed")
            user_recipient_details.update({'display_name': form_data.get('username', 'User')})
            template_id = form_data['activation_id']
            sent_email = self.call_email(user_recipient_details, sender_details, template_id, \
                                         domain_name, user_id, message)
            user_email_status = sent_email.send()
            if user_email_status:
                return Response("Email sent successfully", status=200)
            return Response("Email to user not sent successfully", status=500)

    def encode_id(self, user_id):
        """
        Get the user_id and pass it to the jwt for encoding using the secret keys
        """
        encoded = jwt.encode({"token": user_id,
                              "time": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')},
                             'secret', algorithm='HS256')

        return str(encoded, 'utf-8')

    def call_email(self, user_recipient_details, sender_details, template_id,\
                    domain_name, user_id, message):
        """
        Call email is a call to your email object for sending mails
        """
        return Email(user_recipient_details, sender_details,
                     template_id, domain=domain_name,
                     jwt=self.encode_id(user_id), message=message)
