"""
API view for super admin
"""
import threading
from rest_framework.response import Response
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api,\
    check_serializer_validation
from cheerios.triviaweb.views.account_verification import AccountVerificationViewSet
from cheerios.settings import MSSQLUSERDB, MSSQLDBGAMEPLAY
from cheerios.triviaweb.constants import STORE_PROC, USER_ROLES
from cheerios.triviaweb.serializers import SuperAdminEmailSerializer
from cheerios.triviaweb.utils.utils import build_html_table_message,\
    ListUserObjectsUnique
from cheerios.triviaweb.views.user import USER_UTIL



class SuperAdminEmailViewSet(MSSQLApiView):
    """
    API To get List of super-admin
    """
    @get_store_procedure_api
    @check_serializer_validation(SuperAdminEmailSerializer)
    def get(self, request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of super admin
        """
        email_data = request.GET

        cursor = self.get_cursor(MSSQLUSERDB)
        store_procedure = STORE_PROC['get_trivia_host']
        params = "@PlayerID=%s" % email_data.get('removed_user_id')
        self.call_storeprocedure(store_procedure, cursor, params)
        _user = self.fetchall_from_cursor(cursor)[0]

        # Fetch all those users which we need to send a mail to
        users_to_send = self.fetch_recieving_users(email_data.get('new_registration'), _user)

        # Get the message as it is separate for different types of conditions
        message = _user.get('UserName') if not email_data.get('new_registration') else\
                    self.get_registration_message(_user)

        for user in users_to_send:
            send_data = {'domain': email_data.get('domain'),
                         'message': message,
                         'activation_id': email_data.get('email_id'),
                         'email': user.get('Email'),
                         'username': user.get('Email')}
            acc_obj = AccountVerificationViewSet()
            # Adding parallel Thread Pools for sending emails
            # Multi threading the email calls
            thread = threading.Thread(target=acc_obj.send_mail, args=(send_data,))
            thread.start()

        return Response(users_to_send, status=200)

    def fetch_recieving_users(self, group_admin_required, _user):
        """
        This function would be used to fetch all those users which are present as
        super-admins or group-admins belonging to that enterprise (this would only
        be required if group-admin's are required)
        """
        _user_list = self.fetch_all_super_admins()
        # If the group admin flag is raised from the API call then fetch all
        # group-admins of that enterprise where the user has registered
        if group_admin_required:
            _user_list.extend(self.fetch_all_group_admins(_user.get('StumpTriviaBrandingID')))
            # Get the unique elements from the set
            _user_list = list(set([ListUserObjectsUnique(i, 'UserName') for i in _user_list]))
            # We need to update the list so that its compatible with everything
            _user_list = list(map(lambda x: x.value, _user_list))

        return _user_list

    def fetch_all_group_admins(self, enterprise_id):
        """
        This function would fetch all group-admins belonging to an enterprise
        """
        store_procedure = STORE_PROC["get_users_from_db"]
        _role = USER_UTIL.get_particular_role(USER_ROLES['GROUP_ADMIN'])
        parameters = "@StumpTriviaBrandingID={0}, @StumpTriviaRoleID={1}".format(enterprise_id,\
                                                                               _role)
        with self.connection_manager(store_procedure, parameters, MSSQLUSERDB) as cursor:
            return self.fetchall_from_cursor(cursor)


    def fetch_all_super_admins(self):
        """
        This function would fetch all super-admins whose notifications are enabled
        """
        store_procedure = STORE_PROC['get_super_admin']
        # Fetch all the super admins present in the database
        with self.connection_manager(store_procedure, "", MSSQLDBGAMEPLAY) as cursor:
            # Only provide those super-admin(s) which have a send_email as True, thus
            # the filter
            return list(filter(lambda x: x['AdminNotificationFlag'] == 1,\
                                 self.fetchall_from_cursor(cursor)))


    def get_registration_message(self, _user):
        """
        This function would be used to register the users
        """
        # Call the enterprise list stored procedure to get the name of the enterprise
        get_enterprise_list = STORE_PROC['get_enterprise_list']
        with self.connection_manager(get_enterprise_list, "", MSSQLDBGAMEPLAY) as cursor:
            enterprises = self.fetchall_from_cursor(cursor)
            enterprises = list(filter(lambda x: x['StumpTriviaBrandingID'] ==\
                                      _user['StumpTriviaBrandingID'], enterprises))[0]

        # Build user format list which would be sent to the utility function
        # for converting it into an html
        format_user = [('Name', _user.get('UserName')), ('Email', _user.get('Email')),
                       ('Brand', enterprises.get('StumpTriviaBrandingName'))]

        return build_html_table_message(format_user)
