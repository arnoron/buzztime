"""
API view For Document like post, get method for any document.
"""
import copy
from collections import namedtuple, Counter
from rest_framework.response import Response
from pytz import timezone
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.triviaweb.utils.utils import remove_cache, CustomConverter
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.serializers import RemoveQuestionSerializer, \
  QuestionSetSerializer, QuestionListSerializer, SurveyQuestionSerializer, \
  SurveySetSerializer, QuestionListPutSerializer
from cheerios.triviaweb.constants import STORE_PROC, Survey,\
    USER_ROLES, QUESTION_API, PREFLIGHT_SURVEY_URL, BONUS, TIE,\
    EXTRA_SET_NUMBER, QUE_EDIT_MODE, EXTRA_ROUNDS, CHOICE_ROW_DEFAULT,\
    QUESTION_TYPES, QUESTION_TYPES_MAP
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user,\
    cache_api
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger
from cheerios.services.redis import RedisSession
from django.utils.crypto import ( get_random_string )

CHEERIOS_UTILS = CheeriosUtils()
redis_obj = RedisSession()

class QuestionViewSet(MSSQLApiView):
    """
    API To get Question
    # It will create connection with MS SQL server
    # using mypool_game_play object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure
    # Stored procedure will return list of question/question set from DB which will be passed
      to UI and displayed
    API To post Question
    # It will create connection with MS SQL server
    # using mypool_game_play object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure
    # Stored procedure will save questions data into DB.
    API To delete Question
    # It will create connection with MS SQL server
    # using mypool_game_play object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure
    # Stored procedure will inactive/active questions set into DB.
    """
    @allow_user(check_enterprise="enterprise_id")
    @cache_api()
    @get_store_procedure_api
    def get(self, request_values):
        """
        Get API to connect mssql.
        It will call store procedure.
        If stump question id found in get parameter then it will send question list.
        If any parameter is not found then it will return active question set list.
        If status id found in parameter it will return value of set list according to status id.
        """
        stump_question_set_id = request_values.GET.get('StumpQuestionSetID')
        if stump_question_set_id:
            store_procedure = STORE_PROC["get_question_set"]
            parameters = "@StumpQuestionSetID={0}".format(stump_question_set_id)
            with self.connection_manager(store_procedure, parameters, MSSQLDBGAMEPLAY) as cursor:
                result = self.fetchall_from_cursor(cursor)
            set_info = result[0]
            question_list, stump_question_set_id, question_count_list = \
                self.format_question_data(result)
            date = set_info['QuestionSetCreateDate']
            localtz = timezone('US/Pacific')
            date = localtz.localize(date)
            date = date.astimezone(timezone('UTC'))
            days = set_info['Days']
            extra_indicator = set_info['ExtraQuestionSetIndicator']
            parent_id = set_info['OriginalStumpQuestionSetID']
            is_parent = set_info['IsParentQuestionSet']
            unique_set_key = redis_obj.get_value(key=stump_question_set_id)
            list_data = {
                'question': question_list,
                'stump_question_set_id': stump_question_set_id,
                'created_date': date,
                'ExtraQuestionSetIndicator': extra_indicator,
                'Days': days,
                'question_count_list': question_count_list,
                'OriginalStumpQuestionSetID': parent_id,
                'IsParentQuestionSet': is_parent,
                'UniqueSetKey': unique_set_key.decode("utf") if unique_set_key else unique_set_key
            }

            # Appending the extra day according to the ExtraQuestionSetIndicator flag
            list_data = self.update_days_in_question_set(list_data)
        else:
            store_procedure = STORE_PROC["get_question_set_list"]
            stump_status_id = request_values.GET.get('StatusID')
            branding_id = request_values.GET.get('enterprise_id')
            day = request_values.GET.get('day')
            # checking if parameter has status id or branding id.
            # and creating query according to that.
            parameter = "@StatusID='{0}'".format(stump_status_id) if stump_status_id else ""
            parameter = "{0},".format(parameter) if parameter and branding_id else parameter
            parameter = "{0} @StumpTriviaBrandingID='{1}'".format(parameter, branding_id)
            parameter = "{0}, @Days='%{1}%'".format(parameter, day) if day else parameter\
                if branding_id else parameter

            with self.connection_manager(store_procedure, parameter, MSSQLDBGAMEPLAY) as cursor:
                result = self.fetchall_from_cursor(cursor)
            # For adding the correct understandable days in questions
            list_data = list(map(self.update_days_in_question_set, result))
        response = Response(list_data, status=200)
        return response

    @staticmethod
    def update_days_in_question_set(question):
        """
        Add the index 8 for our UI to understand
        """
        if question['ExtraQuestionSetIndicator'] != 0:
            question['Days'] = ''.join((question['Days'], EXTRA_SET_NUMBER))
        # Removing redundant spaces
        question['Days'] = question['Days'].replace(' ', '')
        return question

    def format_question_data(self, result):
        """
        This function will create required format of data
        retrieved from MSSQL Stored procedure
        :param result:
        :return: list_data
        """
        list_data = []
        question_instance_id = ''
        round_count = 0
        previous_round = ""
        question_count_list = []
        #Filtering rounds, Bonus Round and Tie Rounds separately and
        #filtering each according round and question numbers
        only_numeric = list(filter(
            lambda result_obj: result_obj.get('RoundName').lower() != BONUS and \
                               result_obj.get('RoundName').lower() != TIE, result))
        only_numeric = sorted(only_numeric, \
                              key=lambda k: (int(k['RoundName']), k['QuestionNumber']))
        bonus_round = list(filter( \
            lambda result_obj: result_obj.get('RoundName').lower() == BONUS, result))
        bonus_round = sorted(bonus_round, key=lambda k: (k['QuestionNumber']))
        tie_round = list(filter( \
            lambda result_obj: result_obj.get('RoundName').lower() == TIE, result))
        tie_round = sorted(tie_round, key=lambda k: (k['QuestionNumber']))
        # result = sorted(result, key=lambda k: (k['RoundName'], k['QuestionNumber']))
        # Appending all rounds into one
        result = only_numeric + bonus_round + tie_round
        if isinstance(result, list):
            for result_obj in result:
                question_instance_id = result_obj.get('StumpQuestionSetID')
                if result_obj.get('RoundName').lower() != BONUS and \
                        result_obj.get('RoundName').lower() != TIE:
                    round_name = int(result_obj.get('RoundName'), 10)
                else:
                    round_name = result_obj.get('RoundName')
                # Tracking the round_count to sort it at the end
                current_round = round_name
                if previous_round != current_round:
                    previous_round = current_round
                    round_count += 1
                type_of_question = QUESTION_TYPES[result_obj.get('StumpQuestionType')]
                score = result_obj.get('Score')
                answers = result_obj.get('Answers')
                correct_answer = result_obj.get('CorrectAnswer')
                list_data.append(
                    dict(questionDbId=result_obj.get('StumpQuestionID'),
                         questionText=result_obj.get('Question'),
                         category=result_obj.get('RoundCategory'),
                         correctAnswer=correct_answer,
                         questionId=result_obj.get('QuestionNumber'),
                         round=round_name,
                         questionSetName=result_obj.get('QuestionSetName'),
                         roundDescription=result_obj.get('RoundCategoryDescription'),
                         questionMediaType=result_obj.get('MediaType'),
                         mediaUrl=result_obj.get('MediaPath'),
                         round_no=round_count,
                         funFact=result_obj.get('FunFact', ""),
                         choiceRowsList=self.format_choices(type_of_question, \
                                                            score, answers, correct_answer),
                         typeOfQuestion=type_of_question)
                )
        # To keep track of no of question in each round
        counter_obj = Counter([d['round'] for d in list_data if d['round'] not in EXTRA_ROUNDS])
        question_count_list = list(counter_obj.values())
        return list_data, question_instance_id, question_count_list

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def post(self, request_values):
        """
        Post API to save question list in mssql.
        Check question set has at least 6 question and
        question list should has 5 question for each round.
        Question list round question must have bonus question.
        Post API will save question set name.
        After saving question set name it will save question list one by one.
        """
        serializer = QuestionListSerializer(data=request_values.data)
        CHEERIOS_UTILS.validate_serializer(serializer)
        question_parameter = request_values.data.get('Question')
        question_set_name = request_values.data.get('QuestionSetName')
        stump_game_id = request_values.data.get('StumpGameId')
        brand_id = request_values.data.get('BrandingId')
        parent_id = request_values.data.get('ParentSetId')
        question_days = request_values.data.get('days')
        unique_set_key = request_values.data.get('uniqueSetKey')
        survey = self.generate_survey_question_list(question_parameter)
        cursor = self.get_cursor(MSSQLDBGAMEPLAY, True)
        if not unique_set_key:
            unique_set_key = get_random_string(36)

        for company_id in brand_id:
            que_days = copy.copy(question_days)
            if len(brand_id) > 1 and not parent_id:
                parent_id = 0
            stump_question_set_id = self.post_question_set(stump_game_id, \
                                    question_set_name, company_id, que_days, parent_id, cursor)

            #saving the guid-question_Set mapping
            redis_obj.save_data(key=stump_question_set_id, value=unique_set_key, persist=True)

            self.post_stump_question(survey.questions, stump_question_set_id, cursor)
            if survey.survey_questions:
                stump_survey_id = self.post_survey_set(survey.survey_name, stump_question_set_id, cursor)
                self.post_stump_survey(survey.survey_questions, stump_survey_id, cursor, False)

        #Remove cache of questions from Redis
        remove_cache(key=QUESTION_API)
        self.connection_commit()
        return Response(status=200)

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']],\
                check_enterprise='BrandingId')
    @get_store_procedure_api
    def put(self, request_values):
        """
        PUT API integration for updating days in a question set
        """
        serializer = QuestionListPutSerializer(data=request_values.data)
        CHEERIOS_UTILS.validate_serializer(serializer)
        question_id = request_values.data.get('question_id')
        survey_id = request_values.data.get('survey_id') if request_values.data.get('survey_id') \
            else False
        survey_add_flag = request_values.data.get('survey_add_flag')
        question_days = request_values.data.get('days')
        cursor = self.get_cursor(MSSQLDBGAMEPLAY, True)
        if question_days and question_id:
            store_procedure = STORE_PROC['update_question_days']
            # Fetch question days and set indicator
            question_days, extra_set_indicator = self.get_correct_days_params(question_days)
            store_proc_params = "@StumpQuestionSetID='{0}', @Days='{1}',\
                                 @ExtraQuestionSetIndicator={2}"\
                                .format(question_id, question_days, extra_set_indicator)
            self.call_storeprocedure(store_procedure, cursor, store_proc_params)
            self.fetchall_from_cursor(cursor)
        if request_values.data.get('question'):
            question_parameter = request_values.data.get('question')

            survey = self.generate_survey_question_list(question_parameter)
            self.post_stump_question(survey.questions, question_id, cursor, True)

            if survey.survey_questions:
                if survey_id is False and survey_add_flag == QUE_EDIT_MODE['ADD']:
                    survey_id = self.post_survey_set(survey.survey_name, question_id, cursor)
                self.post_stump_survey(survey.survey_questions, survey_id, cursor, True)

        # Remove the soft-delete flag as well
        store_procedure = STORE_PROC["delete_question_set"]
        request_values = "@StumpQuestionSetID='{0}', @StatusID=1".\
                            format(question_id)
        self.call_storeprocedure(store_procedure, cursor, request_values)

        # Removing the cache of questions from Redis
        remove_cache(key=QUESTION_API)
        remove_cache(key='/triviaweb/survey/')
        self.connection_commit()
        return Response(status=200)

    def format_choices(self, type_of_question, score, answers, correct_answer):
        """
        this function formats the choices of the mcq question
        :param type_of_question:
        :param score:
        :param answers:
        :param correct_answer:
        :return:
        """
        choice = copy.deepcopy(CHOICE_ROW_DEFAULT)
        if type_of_question == 'MCQ':
            for key in choice:
                choice[key]['url'] = answers.split('~~')[key - 1].split('##')[1]
                choice[key]['type'] = answers.split('~~')[key - 1].split('##')[0]
            choice[int(correct_answer[-1])]['score'] = score
            choice[int(correct_answer[-1])]['answer'] = True
        return choice

    @staticmethod
    def get_correct_days_params(question_days):
        """
        Function to get the correct days
        """
        extra_set_indicator = 1 if EXTRA_SET_NUMBER in question_days else 0
        # Removing the extra set number if it is present
        if EXTRA_SET_NUMBER in question_days:
            question_days.pop(question_days.index(EXTRA_SET_NUMBER))
        return "".join(question_days), extra_set_indicator

    @get_store_procedure_api
    def update_days(self, store_procedure, store_proc_params):
        """
        Function to update days
        """
        with self.connection_manager(store_procedure, store_proc_params, \
                                     MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
        return Response(result, status=200)

    def post_survey_set(self, survey_name, stump_question_set_id, cursor):
        """
        This function calls stored procedure and and save survey question set into DB.
        It return survey set id.
        """
        value = dict(survey_name=survey_name)
        serializer = SurveySetSerializer(data=value)
        CHEERIOS_UTILS.validate_serializer(serializer)
        store_procedure = STORE_PROC["save_survey_set"]
        data = "@SurveyName='{0}', @StumpQuestionSetID='{1}'". \
            format(survey_name, stump_question_set_id)
        survey_id = self.save_survey_db(store_procedure, data, cursor)
        return survey_id

    def save_survey_db(self, store_procedure, data, cursor):
        """
        Calling store procedure for saving the survey set in DB
        """
        self.call_storeprocedure(store_procedure, cursor, data)
        result = self.fetchall_from_cursor(cursor)
        stump_survey_id = result[0].get('StumpSurveyID')
        return stump_survey_id

    def post_question_set(self, stump_game_id, question_set_name, company_id, \
            question_days, parent_id, cursor):
        """
        This function calls stored procedure and and save question set into DB.
        It return question set id.
        """
        store_procedure = STORE_PROC["save_question_set"]
        if stump_game_id:
            data = "@StumpGameID={0}, @QuestionSetName={1}, @StumpTriviaBrandingID={2}".format(
                    stump_game_id, question_set_name.replace("'", "''"), company_id)
            days, extra_set_indicator = self.get_correct_days_params(question_days)
            data = "{0}, @Days={1}".format(data, days)\
                      if question_days else "{0}, @Days=''".format(days)
            if parent_id or parent_id == 0:
                data = "{0}, @OriginalStumpQuestionSetID={1}".format(data, parent_id)
            self.call_storeprocedure(store_procedure, cursor, data)
            result = self.fetchall_from_cursor(cursor)
        else:
            # Fetch question days and set indicator
            question_days, extra_set_indicator = self.get_correct_days_params(question_days)

            question_set_params = "@QuestionSetName='{0}', @StumpTriviaBrandingID={1}, \
                @ExtraQuestionSetIndicator={2}".format(question_set_name.replace("'", "''"), company_id, \
                extra_set_indicator)
            # Update question set accordingly
            question_set_params = "{0}, @Days={1}".format(question_set_params, question_days)\
                                  if question_days else "{0}, @Days=''".format(question_set_params)
            if parent_id or parent_id == 0:
                question_set_params = "{0}, @OriginalStumpQuestionSetID={1}".format(question_set_params, parent_id)
            self.call_storeprocedure(store_procedure, cursor, question_set_params)
            result = self.fetchall_from_cursor(cursor)
        stump_question_set_id = result[0].get('StumpQuestionSetID')
        return stump_question_set_id

    def generate_survey_question_list(self, question_parameter):
        """
        Create list of question and survey question to save list in Db.
        It is return survey and question list and survey name.
        """
        questions = list(filter(lambda x: str(x['round']).lower() != Survey,
                                question_parameter))
        survey_questions = list(filter(lambda x: str(x['round']).lower() == Survey,
                                       question_parameter))
        survey_questions = CustomConverter(survey_questions).\
                                    convert([('round_description', 'category')])
        survey_questions = sorted(survey_questions, key=lambda k: int(k['question_id']))
        # Fetch the survey name from the last category if it exists
        survey_name = survey_questions[len(survey_questions)-1]['category'] if\
                        len(survey_questions) > 0 else ""
        # Formatting the questions and assigning rounds to TIE and BONUS.
        # Previous round is set to check whether round has changed or not
        # And if its changed then we need to change the round count
        previous_round = ""
        round_count = 0
        for question_row in questions:
            current_round = question_row['round']
            if str(previous_round) != str(current_round) and \
                    question_row.get('edit_status') != QUE_EDIT_MODE['DELETE']:
                previous_round = current_round
                round_count += 1
            question_row.update(dict(
                round_name=current_round,
                round=round_count,
            ))
        survey = namedtuple("survey", ["questions", "survey_questions", "survey_name", \
            "round_count"])
        return survey(questions, survey_questions, survey_name, round_count)

    def post_stump_survey(self, survey, stump_survey_id, cursor, update):
        """
        Get list of survey question and call store procedure to save each question in database.
        """
        store_procedure = STORE_PROC["survey_question_type"]
        self.call_storeprocedure(store_procedure, cursor, "")
        result = self.fetchall_from_cursor(cursor)
#        creating dict of question type and question type id.
#        so we can easily get id of question type.
        question_type = {}
        for value in result:
            survey_type = value["StumpSurveyQuestionType"]
            type_id = value["StumpSurveyQuestionTypeID"]
            question_type[survey_type] = type_id

        for value in survey:
            serializer = SurveyQuestionSerializer(data=value)
            CHEERIOS_UTILS.validate_serializer(serializer)
            category = value.get('category')
            question_number = value.get('question_id')
            question = value.get('question_text')
            edit_status = value.get('edit_status')
            survey_question_id = value.get('survey_question_id')
            if category in question_type:
                question_type_id = question_type[category]
            else:
                raise ValueError("Survey question type is invalid")
            #Block of code below is executed only while editing(Put request)
            if update:
                if edit_status == QUE_EDIT_MODE['ADD']:
                    required_values = "@StumpSurveyID={0}, @SurveyQuestionNumber='{1}'," \
                                      "@SurveyQuestion='{2}', @StumpSurveyQuestionTypeID={3}" \
                        .format(stump_survey_id, question_number, question, question_type_id)
                    store_procedure = STORE_PROC["save_survey"]
                    self.save_into_db(store_procedure, required_values, cursor)
                elif edit_status == QUE_EDIT_MODE['EDIT']:
                    required_values = "@StumpSurveyQuestionID={0}".format(survey_question_id)
                    store_procedure = STORE_PROC["delete_survey"]
                    self.save_into_db(store_procedure, required_values, cursor)

                    required_values = "@StumpSurveyID={0}, @SurveyQuestionNumber='{1}'," \
                                      "@SurveyQuestion='{2}', @StumpSurveyQuestionTypeID={3}" \
                        .format(stump_survey_id, question_number, question, question_type_id)
                    store_procedure = STORE_PROC["save_survey"]
                    self.save_into_db(store_procedure, required_values, cursor)
                elif edit_status == QUE_EDIT_MODE['DELETE']:
                    required_values = "@StumpSurveyQuestionID={0}".format(survey_question_id)
                    store_procedure = STORE_PROC["delete_survey"]
                    self.save_into_db(store_procedure, required_values, cursor)
                else:
                    continue
            else:
                required_values = "@StumpSurveyID={0}, @SurveyQuestionNumber='{1}'," \
                                  "@SurveyQuestion='{2}', @StumpSurveyQuestionTypeID={3}" \
                    .format(stump_survey_id, question_number, question, question_type_id)
                store_procedure = STORE_PROC["save_survey"]
                self.save_into_db(store_procedure, required_values, cursor)
                message = "Data saved successfully Data:- {0}".format(value)
                Logger.log.info(message)

    def save_into_db(self, store_procedure, required_values, cursor=False):
        """
        Calling store procedure for saving the survey and question in DB
        """
        # If cursor is already coming then use that otherwise make a new cursor.
        if cursor:
            self.call_storeprocedure(store_procedure, cursor, required_values)
            self.fetchall_from_cursor(cursor)
        else:
            with self.connection_manager(store_procedure, required_values, \
                                         MSSQLDBGAMEPLAY) as cursor:
                self.fetchall_from_cursor(cursor)

    @staticmethod
    def ques_apostrophe():
        """
        Function which returns apostrophe in english
        """
        return  b'\xe2\x80\x99'.decode('utf-8')

    def check_new_round(self, value, questions):
        """
        Function to check if a new round was added
        """
        question_number = value.get('question_id')
        if question_number == '1':
            round_que = list(filter( \
                lambda x: str(x.get('round_name')) == str(value.get('round_name')), questions))
            for value in round_que:
                if value.get('edit_status') != QUE_EDIT_MODE['ADD']:
                    return False
            return True
        else:
            return False

    def delete_bonus_tie(self, question, cursor):
        """
        Function to delete bonus and tie questions
        """
        tie_bonus_que = list(filter( \
            lambda x: x.get('round_name') == 'Tie' or x.get('round_name') == 'Bonus', question))

        for value in tie_bonus_que:
            status = value.get('edit_status')
            if status != QUE_EDIT_MODE['ADD']:
                store_procedure = STORE_PROC["delete_question"]
                stump_question_id = value.get('question_db_id')
                required_values = "@StumpQuestionID={0}".format(stump_question_id)
                self.save_into_db(store_procedure, required_values, cursor)

    def add_bonus_tie(self, question, edit_set_id, cursor):
        """
        Function to add back bonus and tie questions
        """

        tie_bonus_que = list(filter(lambda x: x.get('round_name') == 'Tie' or x.get('round_name') == 'Bonus', question))
        round_category_description = question[0].get('round_description')
        round_category = question[0].get('category')
        last_round = ""

        for value in tie_bonus_que:
            status = value.get('edit_status')
            round_name = value.get('round_name')
            if last_round != round_name:
                last_round = round_name
                round_category_description = value.get('round_description'). \
                    replace("'", self.ques_apostrophe())
                round_category = value.get('category').replace("'", self.ques_apostrophe())
            if status != QUE_EDIT_MODE['DELETE'] and status != QUE_EDIT_MODE['ADD']:
                store_procedure = STORE_PROC["save_question"]
                stump_question_id = edit_set_id
                round_number = value.get('round')
                question_number = value.get('question_id')
                question = value.get('question_text').replace("'", self.ques_apostrophe())
                correct_answer = value.get('correct_answer').replace("'", self.ques_apostrophe())
                media_path = value.get('media_url') if value.get('media_url') != "None" else ""
                question_media_type = value.get('question_media_type') if media_path else ""
                # Fetch the fun fact of the question
                fun_fact = value.get('fun_fact', '').replace("'", self.ques_apostrophe())
                type_of_question = value.get('type_of_question')
                param = "@StumpQuestionSetID='{0}'".format(stump_question_id)
                required_values = "{0}, @RoundNumber='{1}', @RoundName='{2}', \
                                @QuestionNumber='{3}', @RoundCategory='{4}', @Question='{5}', \
                                @CorrectAnswer='{6}', @RoundCategoryDescription='{7}', \
                                @MediaType='{8}', @MediaPath='{9}', @FunFact='{10}'". \
                    format(param, round_number, round_name, \
                           question_number, round_category, question, correct_answer, \
                           round_category_description, question_media_type, media_path, \
                           fun_fact)
                choice_rows_list = value.get('choice_rows_list') \
                    if type_of_question == QUESTION_TYPES['MultipleChoice'] else ""
                if (type_of_question == 'MCQ' and choice_rows_list):
                    required_values = self.format_choices_row(choice_rows_list, \
                                                              required_values, \
                                                              type_of_question)

                elif type_of_question == 'True/False':
                    score = 1 if correct_answer == "True" else 0
                    type_id = QUESTION_TYPES_MAP[type_of_question]
                    required_values = "{0}, @Score='{1}', @StumpQuestionTypeID='{2}', " \
                                      "@Answers='{3}'".format(required_values, score, \
                                                              type_id, '')
                else:
                    required_values = "{0}, @Score='{1}', @StumpQuestionTypeID='1', " \
                                      "@Answers='{2}'".format(required_values, '', '')

                self.save_into_db(store_procedure, required_values, cursor)


    def post_stump_question(self, questions, stump_question_id, cursor, update=False):
        """
        Get list of question and call store procedure to save each question in database.
        Function check if each round has 5 question and all attribute must present in Question.
        """
        question_count = 0
        edit_set_id = stump_question_id
        round_category_description = questions[0].get('round_description')
        round_category = questions[0].get('category')
        last_round = ""
        round_added_flag = False
        for value in questions:
            round_name = value.get('round_name')
            if last_round == round_name:
                question_count += 1
            else:
                last_round = round_name
                question_count = 1
                round_category_description = value.get('round_description').\
                                                replace("'", self.ques_apostrophe())
                round_category = value.get('category').replace("'", self.ques_apostrophe())
            last_round = value.get('round_name')
            value["round_description"] = round_category_description
            value["category"] = round_category
            value["fun_fact"] = value.get("fun_fact") if value.get("fun_fact") != None else ""
            # MediaType needs to be checked
            serializer = QuestionSetSerializer(data=value)
            CHEERIOS_UTILS.validate_serializer(serializer)
            round_number = value.get('round')
            question_number = value.get('question_id')
            question = value.get('question_text').replace("'", self.ques_apostrophe())
            correct_answer = value.get('correct_answer').replace("'", self.ques_apostrophe())
            media_path = value.get('media_url') if value.get('media_url') != "None" else ""
            media_path = media_path.replace("'", "''")
            question_media_type = value.get('question_media_type') if media_path else ""
            # Fetch the fun fact of the question
            fun_fact = value.get('fun_fact', '').replace("'", self.ques_apostrophe())
            type_of_question = value.get('type_of_question')

            choice_rows_list = value.get('choice_rows_list') if type_of_question == \
                QUESTION_TYPES['MultipleChoice'] else ""

            round_added = self.check_new_round(value, questions)

            if round_added:
                round_added_flag = True

            if update:
                # If put is called then only this block will be called
                status = value.get('edit_status')
                if status == QUE_EDIT_MODE['EDIT']:
                    # If edit flag is set then update SP is called
                    store_procedure = STORE_PROC["update_question"]
                    stump_question_id = value.get('question_db_id')
                    param = "@StumpQuestionID='{0}'".format(stump_question_id)
                elif status == QUE_EDIT_MODE['ADD']:
                    # If add flag is set then add SP is called
                    #if a new round was added then we need to delete bonus and tie
                    #before adding the new round
                    if round_added:
                        self.delete_bonus_tie(questions, cursor)
                    store_procedure = STORE_PROC["save_question"]
                    stump_question_id = edit_set_id
                    param = "@StumpQuestionSetID='{0}'".format(stump_question_id)
                elif status == QUE_EDIT_MODE['DELETE']:
                    store_procedure = STORE_PROC["delete_question"]
                    stump_question_id = value.get('question_db_id')
                    required_values = "@StumpQuestionID={0}".format(stump_question_id)
                    self.save_into_db(store_procedure, required_values, cursor)
                    continue
                else:
                    continue

            else:
                store_procedure = STORE_PROC["save_question"]
                param = "@StumpQuestionSetID='{0}'".format(stump_question_id)

            required_values = "{0}, @RoundNumber='{1}', @RoundName='{2}', \
                @QuestionNumber='{3}', @RoundCategory='{4}', @Question='{5}', \
                @CorrectAnswer='{6}', @RoundCategoryDescription='{7}', \
                @MediaType='{8}', @MediaPath='{9}', @FunFact='{10}'".\
                format(param, round_number, round_name, \
                question_number, round_category, question, correct_answer, \
                round_category_description, question_media_type, media_path, \
                fun_fact)

            if (type_of_question == 'MCQ' and choice_rows_list):
                required_values = self.format_choices_row(choice_rows_list, \
                                                          required_values, \
                                                          type_of_question)

            elif type_of_question == 'True/False':
                score = 1 if correct_answer == "True" else 0
                type_id = QUESTION_TYPES_MAP[type_of_question]
                required_values = "{0}, @Score='{1}', @StumpQuestionTypeID='{2}', @Answers='{3}'".\
                format(required_values, score, type_id, '')
            else:
                required_values = "{0}, @Score='{1}', @StumpQuestionTypeID='1', @Answers='{2}'".\
                format(required_values, '', '')

            self.save_into_db(store_procedure, required_values, cursor)

        if round_added_flag and update:
            #adding back the bonus and tie questions
            self.add_bonus_tie(questions, edit_set_id, cursor)

        message = "Data saved successfully Data:- {0}".format(value)
        Logger.log.info(message)


    def format_choices_row(self, choice_rows_list, required_values, type_of_question):
        """
        This function is for saving and formatting the choices for MCQ
        """
        score = [x['score'] for x in choice_rows_list.values() if x['score']][0]
        type_id = QUESTION_TYPES_MAP[type_of_question]
        answers = ""
        for key in range(1, 5):
            answers = answers + choice_rows_list[str(key)]['type'] + "##" + \
                      choice_rows_list[str(key)]['url'].replace("'", self.ques_apostrophe()) + '~~'
        required_values = "{0}, @Score='{1}', @Answers='{2}', @StumpQuestionTypeID='{3}'".\
            format(required_values, score, answers, type_id)
        return required_values

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']],\
                check_enterprise='enterprise_id')
    @get_store_procedure_api
    def delete(self, request_values):
        """
        Delete API to delete question set from mssql.
        API call store procedure and update value of stump question set according to status id.
        If status id is 0 or not present then delete.
        """
        serializer = RemoveQuestionSerializer(data=request_values.data)
        CHEERIOS_UTILS.validate_serializer(serializer)
        cursor = self.get_cursor(MSSQLDBGAMEPLAY)
        stump_question_set_id = request_values.data.get('StumpQuestionSetID')
        company_id = request_values.data.get('enterprise_id')
        status_id = request_values.data.get('StatusID')
        status_id = status_id if status_id else 0
        store_procedure = STORE_PROC["delete_question_set"]
        request_values = "@StumpQuestionSetID='{0}', @StatusID='{1}'".\
                            format(stump_question_set_id, status_id)
        self.call_storeprocedure(store_procedure, cursor, request_values)
        self.connection_commit()

        #Fetching questions of the set
        store_procedure = STORE_PROC["get_question_set"]
        parameters = "@StumpQuestionSetID={0}".format(stump_question_set_id)
        with self.connection_manager(store_procedure, parameters, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)

        #Deleting Questions from the set
        for question in result:
            stump_question_id = question['StumpQuestionID']
            store_procedure = STORE_PROC["delete_question"]
            required_values = "@StumpQuestionID={0}".format(stump_question_id)
            with self.connection_manager(store_procedure, required_values, MSSQLDBGAMEPLAY) as cursor:
                result = self.fetchall_from_cursor(cursor)

        #Remove cache of questions from Redis and the cache of the
        #deleted set (survey and questions)
        remove_cache(key=QUESTION_API, param="enterprise_id={0}".format(company_id))
        remove_cache(key=QUESTION_API, param="StumpQuestionSetID={0}".format(stump_question_set_id))
        remove_cache(key=PREFLIGHT_SURVEY_URL, \
                     param="StumpQuestionSetID={0}".format(stump_question_set_id))
        return Response(status=200)
