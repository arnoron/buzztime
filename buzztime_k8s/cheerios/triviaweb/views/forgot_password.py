
"""
Module to send forgot password mail
"""
from rest_framework.response import Response
from cheerios.settings import MSSQLUSERDB
from cheerios.triviaweb.constants import STORE_PROC
from cheerios.players.views.send_email import EmailViewSet
from cheerios.triviaweb.decorators import get_store_procedure_api
from cheerios.players.views.player.edit_user_info import EditUserViewSet
from cheerios.services.log_classifier import Logger


class ForgotPasswordViewSet(EmailViewSet):
    """
    Email API.
    Forms the email data and initiates the send email method
    """

    @get_store_procedure_api
    def post(self, request):
        """
        POST data call whenever someone sends a request to get the
        forgot password email
        :return:
        """
        request_data = request.data
        form_data = request_data.get('data_to_post', None)
        if not form_data:
            return Response("Incorrect response data passed. Please check.", status=400)
        form_data['username'] = self.get_username_from_email(form_data.get('user_email', None))
        return self.send_forgot_password_mail(form_data)

    def get_username_from_email(self, email):
        """
        Returns the username belonging to the email
        """
        store_proc = STORE_PROC["get_username_from_email"]
        parameters = "@EmailAddress='{0}', @NamespaceID=1".format(email)
        with self.connection_manager(store_proc, parameters, MSSQLUSERDB) as cursor:
            result = self.fetchall_from_cursor(cursor)
        return result[0]['Username'] if result else None

    @get_store_procedure_api
    def put(self, request):
        """
        PUT method to change user's password
        """
        edit_obj = EditUserViewSet()
        result = edit_obj.post(request)
        if not result:
            return Response(False, status=200)
        return Response(True, status=200)
        
