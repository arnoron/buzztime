"""
API view For Document like post, get method for any survey.
"""
from rest_framework.response import Response
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.utils.utils import UtilFunction
from cheerios.triviaweb.serializers import SurveyResponseSerializer

from cheerios.triviaweb.decorators import get_store_procedure_api, cache_api

from cheerios.triviaweb.constants import STORE_PROC
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


class SurveyViewSet(MSSQLApiView):
    """
    API To get survey Question
    # It will create connection with MS SQL server
    # using mypool_game_play object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure
    # API will returns all survey questions.
    API To post survey Question response
    # It will create connection with MS SQL server
    # using mypool_game_play object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure
    # API accept stumpSurveyQuestionId, stumpGameInstanceId
    stumpGameTeamId and response and save in to MSSQL
    """
    @cache_api()
    @get_store_procedure_api
    def get(self, request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of survey question
        Creating store procedure response to proper structure.
        Returns list of survey question.
        """
        store_procedure = STORE_PROC["get_survey"]
        question_set_id = request.GET.get('StumpQuestionSetID')
        parameter = "@StumpQuestionSetID={0}".format(question_set_id) if question_set_id else ""
        with self.connection_manager(store_procedure, parameter, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
        list_data = self.format_question_data(result)
        return Response(list_data, status=200)

    @staticmethod
    def format_question_data(result):
        """
        This function will create required format of data
        retrieved from MSSQL Stored procedure
        :param result:
        :return: list_data
        """
        list_data = []
        if isinstance(result, list):
            for result_obj in result:
                list_data.append(
                    dict(StumpSurveyId=result_obj.get('StumpSurveyID'),
                         SurveyName=result_obj.get('SurveyName'),
                         StumpSurveyQuestionId=result_obj.get('StumpSurveyQuestionID'),
                         SurveyQuestionNumber=result_obj.get('SurveyQuestionNumber'),
                         SurveyQuestion=result_obj.get('SurveyQuestion'),
                         StumpSurveyQuestionType=result_obj.get('StumpSurveyQuestionType'))
                )
        return list_data

    def post_survey(self, team, stump_game_instance_id, stump_game_team_id, cursor):
        """
        Post API to connect mssql.
        It will call store procedure.
        API accept stumpSurveyQuestionId, stumpGameInstanceId
        stumpGameTeamId and response and save in to MSSQL
        """
        survey_questions = team.get('survey')
        teams_survey_response = []
        if survey_questions:
            required_values = [stump_game_team_id, stump_game_instance_id]
            CHEERIOS_UTILS.required_parameters(required_values)

            for key, value in survey_questions.items():
                if UTIL_FUNCTION.represents_int(key):
                    teams_survey_response.append(value)
            if teams_survey_response:
                for survey_question in teams_survey_response:
                    if survey_question:
                        self.save_single_survey(survey_question, \
                                                stump_game_instance_id, stump_game_team_id, cursor)
                message = "Teams Survey data is saved."
            else:
                message = "No survey question answered"
        else:
            message = "No survey question answered"
        Logger.log.info(message)

    def save_single_survey(self, survey_question, game_id, stump_game_team_id, cursor):
        """

        :param survey_question:
        :param game_id:
        :param stump_game_team_id:
        :param cursor:
        :return:
        """
        serializer = SurveyResponseSerializer(data=survey_question)
        CHEERIOS_UTILS.validate_serializer(serializer)

        stump_survey_question_id = survey_question.get('SurveyQuestionId')
        stump_game_instance_id = game_id
        stump_game_team_id = stump_game_team_id
        response = survey_question.get('Response')
        data = "@StumpSurveyQuestionID={0}, @StumpGameInstanceID={1},\
                @StumpGameTeamID={2}, @Response='{3}'"\
                .format(stump_survey_question_id, stump_game_instance_id, \
                        stump_game_team_id, response)
        store_procedure = STORE_PROC["save_survey_response"]
        with self.connection_manager(store_procedure, data, \
                                     MSSQLDBGAMEPLAY, cursor=cursor) as cursor:
            self.fetchall_from_cursor(cursor)
        message = "Survey Data saved successfully Data:- {0}".format(data)
        Logger.log.info(message)
