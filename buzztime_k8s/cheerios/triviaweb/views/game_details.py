"""
API view for game operation.
"""
from threading import Thread
from rest_framework.response import Response

from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.triviaweb.utils.firebase import FirebaseService
from cheerios.triviaweb.utils.utils  import GameDetailsException,\
    GameShouldNotDeleteException
from cheerios.triviaweb.serializers import GameAPISerializer
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.views.game import GameViewSet
from cheerios.triviaweb.views.survey import SurveyViewSet
from cheerios.triviaweb.views.team import TeamViewSet
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.triviaweb.constants import USER_ROLES
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger
CHEERIOS_UTILS = CheeriosUtils()

GAME_VIEW_SET = GameViewSet()
TEAM_VIEW_SET = TeamViewSet()
SURVEY_VIEW_SET = SurveyViewSet()


class GameDetailViewSet(MSSQLApiView, FirebaseService):
    """
    API to save game detail
    # It will create connection with MS SQL server
    # using mypool object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure for saving game details.
    # Parameter passed to API is GameToken.
    # Rest of details will be obtained from Firebase
    # and API will save game instance, Team details
    # and survey details in MSSQL database.
    """

    @allow_user(check_roles=[USER_ROLES['HOST']])
    @get_store_procedure_api
    def post(self, request):
        """
        Post API to connect mssql.
        It will call store procedure.
        Store procedure store game instance and return id of it.
        """
        Logger.log.info("---entering post---")
        serializer = GameAPISerializer(data=request.data)
        CHEERIOS_UTILS.validate_serializer(serializer)
        game_key = request.data.get("gameKey")
        thread = Thread(target=self.save_details, args=(game_key, ))
        thread.start()
        return Response(status=200)

    def remove_firebase_entry(self, game_key, game_token):
        """
        Removes firebase entry
        :param game_key:
        :param game_token:
        :return:
        """
        self.remove_cache_data('stump/games/', game_key)
        game_id_list = self.get_cache_data('/stump/games/gameId')
        if game_token in game_id_list:
            game_id_list.remove(game_token)
        self.update_cache_data('/stump/games/', 'gameId', game_id_list)

    def save_details(self, game_key):
        """
        This function will internally call rest of
        the dependent functions to save game details data in DB
        :return:
        """
        try:
            url = 'stump/games/' + game_key
            self.create_cache_connection()
            Logger.log.info("----create_cache----")
            game_obj = self.get_cache_data('/stump/games/{0}'.format(game_key))
            game_token = game_obj.get('gameToken')
            cursor = self.get_cursor(MSSQLDBGAMEPLAY, False)
            stump_game_instance_id = GAME_VIEW_SET.post_game(game_token, \
                cursor, game_obj)

            if stump_game_instance_id:
                TEAM_VIEW_SET.post_game(game_key, stump_game_instance_id, \
                    cursor, game_obj["teams"])
#            Removing game when all data is stored in DB.
#            Also Removing game when questions are not present because game is not started.
#            Game is just generated in firebase and ended game before starting the game.
            self.remove_firebase_entry(game_key, game_token)

        except GameDetailsException as inst:
            self.remove_firebase_entry(game_key, game_token)
            return CHEERIOS_UTILS.value_error(inst)

        except GameShouldNotDeleteException as inst:
            self.update_cache_data(url, 'status', 'to_be_saved')
            message = "Data Save failed (GameShouldNotDeleteException) for gamekey: {0}, " \
                      "Please post data manually using /triviaweb/game/ " \
                      "with data  gameKey : <gamekey value>" \
                      "Error occured is defined below:".format(game_key)
            Logger.log.info(message)
            self.connection_rollback()
            return CHEERIOS_UTILS.value_error(inst)

        except Exception as inst:
            self.update_cache_data(url, 'status', 'to_be_saved')
            message = "Data Save failed for gamekey: {0}, " \
                      "Please post data manually using /triviaweb/game/ " \
                      "with data  gameKey : <gamekey value>" \
                      "Error occured is defined below:".format(game_key)
            Logger.log.error(message)
            self.connection_rollback()
            return CHEERIOS_UTILS.exception(inst)
        finally:
            self.close_cursor()
