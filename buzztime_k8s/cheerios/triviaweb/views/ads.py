"""
API view For Document like post, get method for any survey.
"""
import logging
from rest_framework.response import Response
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.triviaweb.utils.utils import remove_cache
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user,\
    check_serializer_validation, cache_api
from cheerios.triviaweb.constants import USER_ROLES, STORE_PROC, AD_API
from cheerios.triviaweb.serializers import AdPutSerializer, AdPostSerializer, \
    DeleteAdSerializer, AdGetSerializer
LOGGER = logging.getLogger("buzztime")


class AdViewSet(MSSQLApiView):
    """
    API for fetching Brands
    """
    allowed_roles = []

    @get_store_procedure_api
    @cache_api()
    @check_serializer_validation(AdGetSerializer)
    def get(self, _request):
        """
        Get API to fetch advertisements
        """
        if 'brand_id' in _request.GET:
            self.allowed_roles = [USER_ROLES['SUPER_ADMIN'], USER_ROLES['GROUP_ADMIN'], \
                                  USER_ROLES['HOST']]
        else:
            self.allowed_roles = [USER_ROLES['SUPER_ADMIN']]
        data = self.get_data(_request.GET.get('brand_id'))
        return Response(data, status=200)

    @allow_user(check_roles=allowed_roles)
    def get_data(self, brand_id):
        """
        Function to fetch ads depending on brandId
        """
        store_proc_params = "@StumpTriviaBrandingID = {0}".format(brand_id) if brand_id else ""
        store_procedure = STORE_PROC["get_ad_by_brand"] if brand_id else STORE_PROC["get_all_ads"]
        with self.connection_manager(store_procedure, store_proc_params, \
                                     MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
        if brand_id:
            return result[0] if len(result) > 0 else result
        else:
            store_procedure = STORE_PROC["get_brands_by_ad"]
            params = "@StumpTriviaAdID={0}".format(-1)
            with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
                associations = self.fetchall_from_cursor(cursor)
            for advertisement in result:
                brands = list(filter( \
                    lambda x: int(x['StumpTriviaAdID']) == advertisement['StumpTriviaAdID'], \
                    associations))
                advertisement['Brands'] = brands
            return result

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @check_serializer_validation(AdPostSerializer)
    @get_store_procedure_api
    def post(self, _request):
        """
        Ad insertion API
        :return: True with status code 200 on success
        """
        request_params = _request.data
        ad_description = request_params.get('adDescription')
        ad_type = request_params.get('adType')
        ad_url = request_params.get('adUrl')
        ad_brands = map(int, request_params.get('adBrands').split(","))
        store_procedure = STORE_PROC['upload_ad']
        params = "@MediaPath='{0}', @MediaType='{1}', @AdText='{2}'".format( \
            ad_url, ad_type, ad_description)
        with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
        ad_id = result[0]['StumpTriviaAdID']
        store_procedure = STORE_PROC['associate_ad']
        # Associating selected brands with the advertisement
        for brand in ad_brands:
            params = "@StumpTriviaBrandingID={0}, @StumpTriviaAdID={1}".format(brand, ad_id)
            with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
                self.fetchall_from_cursor(cursor)
        remove_cache(key=AD_API)
        return Response(result, status=200)

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @check_serializer_validation(AdPutSerializer)
    @get_store_procedure_api
    def put(self, _request):
        """
        Ad updation API
        :return: True with status code 200 on success
        """
        request_params = _request.data
        ad_id = request_params.get('adID')
        ad_description = request_params.get('adDescription')
        ad_type = request_params.get('adType')
        ad_url = request_params.get('adUrl')
        brands_to_be_associated = request_params.get('adBrands').split(",")
        brands_to_be_dissociated = request_params.get('brandsDissociated').split(",")
        store_procedure = STORE_PROC['update_ad']
        params = "@MediaPath='{0}', @MediaType='{1}', @AdText='{2}', " \
                 "@StumpTriviaAdID={3}".format(ad_url, ad_type, ad_description, ad_id)
        with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
            self.fetchall_from_cursor(cursor)
        store_procedure_associate = STORE_PROC['associate_ad']
        # Associating selected brands with the advertisement
        for brand in brands_to_be_associated:
            if brand:
                params = "@StumpTriviaBrandingID={0}, @StumpTriviaAdID={1}".format(brand, ad_id)
                with self.connection_manager(store_procedure_associate, params, \
                                             MSSQLDBGAMEPLAY) as cursor:
                    self.fetchall_from_cursor(cursor)
        store_procedure_dissociate = STORE_PROC['disassociate_ad']
        # Dissociating brands with the advertisement that were unchecked
        for brand in brands_to_be_dissociated:
            if brand:
                params = "@StumpTriviaBrandingID={0}, @StumpTriviaAdID={1}".format(brand, ad_id)
                with self.connection_manager(store_procedure_dissociate, params, \
                                             MSSQLDBGAMEPLAY) as cursor:
                    self.fetchall_from_cursor(cursor)
        remove_cache(key=AD_API)
        return Response(True, status=200)

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @check_serializer_validation(DeleteAdSerializer)
    @get_store_procedure_api
    def delete(self, _request):
        """
        Ad deletion API
        """
        ad_id = _request.data.get('adID')
        brands_to_be_dissociated = _request.data.get('brandsDissociated').split(",")
        store_procedure_dissociate = STORE_PROC['disassociate_ad']
        # Dissociating brands with the ad that has to be deleted
        for brand in brands_to_be_dissociated:
            if brand:
                params = "@StumpTriviaBrandingID={0}, @StumpTriviaAdID={1}".format(brand, ad_id)
                with self.connection_manager(store_procedure_dissociate, params, \
                                             MSSQLDBGAMEPLAY) as cursor:
                    self.fetchall_from_cursor(cursor)
        # Deleting ad
        store_procedure = STORE_PROC['delete_ad']
        params = "@StumpTriviaAdID={0}".format(ad_id)
        with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)

        remove_cache(key=AD_API)
        return Response(result, status=200)
