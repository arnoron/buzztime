"""
APIs for user profile updation in stump
"""
import ast

from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.views.check_email_availability import EmailAvailabilityViewSet
from cheerios.players.views.check_username_availability import UsernameAvailabilityViewSet
from cheerios.services.check_inappropriate_word import InAppropriateWordViewSet
from cheerios.settings import MSSQLUSERDB, MSSQLDBGAMEPLAY
from cheerios.triviaweb.constants import USER_ROLES, STORE_PROC, USER_URL
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user, \
    cache_api
from cheerios.triviaweb.serializers import UserProfileUpdateSerializer, UserUpdateSerializer, \
    EmailSerializer, UserIdSerializer
from cheerios.triviaweb.utils.utils import UtilFunction, CustomConverter, \
    remove_cache
from cheerios.triviaweb.utils.user_util import UserDetailsUtil
from cheerios.triviaweb.views.account_verification import AccountVerificationViewSet
from cheerios.services.log_classifier import Logger
from cheerios.utils import CheeriosUtils

UTIL_FUNCTION = UtilFunction()
USER_UTIL = UserDetailsUtil()
CHEERIOS_UTILS = CheeriosUtils()


class UserViewSet(MSSQLApiView):
    """
    API viewset for user profile updation
    """
    def __init__(self):
        """
        Defines constants
        """
        super().__init__()
        self.inappropriate_word_check = InAppropriateWordViewSet()
        self.duplicate_username_check = UsernameAvailabilityViewSet()
        self.duplicate_email_check = EmailAvailabilityViewSet()

    @get_store_procedure_api
    def post(self, request):
        """
        User profile updation API
        :return:
        """
        request_params = request.data
        profile_data = UserProfileUpdateSerializer(data=request_params)
        role_data = UserUpdateSerializer(data=request_params)
        email_data = EmailSerializer(data=request_params)

        cursor = self.get_cursor(MSSQLUSERDB, True)
        game_cursor = self.get_cursor(MSSQLDBGAMEPLAY, True)
        if profile_data.is_valid():
            #Sending email for profile update
            user_data = self.get_user_details(request.data.get('user_id'))
            send_data = {'domain': request.data.get('domain'),
                         'message': self.build_email_msg(profile_data.data, user_data),
                         'activation_id': request.data.get('email_id'),
                         'email': request.data.get('email'),
                         'username': user_data.get('username')}
            #Calling the post method before email is triggered
            _result = self.profile_update(profile_data, cursor)
            if not isinstance(_result, dict) and _result.status_code == 400:
                return _result
            acc_obj = AccountVerificationViewSet()
            acc_obj.send_mail(send_data)

        elif role_data.is_valid():
            _result = self.role_update(role_data, cursor, game_cursor)
            if not isinstance(_result, dict) and _result.status_code == 400:
                return _result

            if email_data.is_valid():
                user_data = email_data.validated_data
                acc_obj = AccountVerificationViewSet()
                send_data = {'user_id': user_data.get('user_id'),
                             'name': user_data.get('username'),
                             'email': user_data.get('email'),
                             'username': user_data.get('username'),
                             'domain': user_data.get('domain'),
                             'activation_id': user_data.get('activation_id')}
                if 'message' in user_data:
                    send_data['message'] = user_data.get('message')
                acc_obj.send_mail(send_data)

        else:
            raise ValueError("Invalid Parameter passed")

        #Remove cache of users from Redis
        remove_cache(key=USER_URL)

        return Response(_result, status=200)

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']], \
                check_enterprise='enterprise_id')
    @get_store_procedure_api
    def delete(self, request):
        """
        User deletion API
        :param request:
        :return:
        """
        user_id = request.data.get('playerId')
        auth_serializer = UserIdSerializer(data=request.data)
        CHEERIOS_UTILS.validate_serializer(auth_serializer)
        self.delete_user(user_id)
        return Response(True, status=200)

    def delete_user(self, user_id):
        """
        Change the status of user to deleted and
        remove all the roles related with the user.
        :param user_id:
        :return:
        """
        # SP to know the status id for Delete
        status_id = USER_UTIL.get_particular_status('Deleted')
        store_procedure = STORE_PROC["update_status"]
        # Store procedure to update the status
        params = "@PlayerID={0}, @StumpTriviaUserStatusID={1}".format(user_id, status_id)
        with self.connection_manager(store_procedure, params, MSSQLUSERDB, False) as cursor:
            result = self.fetchall_from_cursor(cursor)
        # Store procedure to delete a role from a user
        store_procedure = STORE_PROC["delete_user_role"]
        roles = USER_UTIL.get_user_roles(user_id)
        for role in roles['roles']:
            role_id = USER_UTIL.get_particular_role(role)
            params = "@PlayerID={0}, @StumpTriviaRoleID={1}".format(user_id, role_id)
            with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY, False) as cursor:
                result = self.fetchall_from_cursor(cursor)

        # Remove cache of users from Redis
        remove_cache(key=USER_URL)

        return result

    def build_email_msg(self, updated_dict, prev_dict):
        """
        This function is to build the email messages for profile update email
        """
        _listed_items = {"<span style='text-transform:uppercase;'>{0}</span> : {1}".\
                        format(x, updated_dict[x]) for x in prev_dict\
                        if x in updated_dict and str(updated_dict[x]) != str(prev_dict[x])}
        return "<br>".join(_listed_items)

    def get_user_details(self, user_id):
        """
        Get user details with the required user_id
        """
        store_procedure = STORE_PROC['get_trivia_host']
        parameters = "@PlayerID=%d" % user_id
        with self.connection_manager(store_procedure, parameters, MSSQLUSERDB) as cursor:
            result = self.fetchall_from_cursor(cursor)
        converter = [('UserName', 'username'), ('LastName', 'lastName'),\
                     ('FirstName', 'firstName'), ('PlayerID', 'user_id'),\
                     ('Email', 'email'), ('StumpTriviaRoleDesc', 'roles'),\
                     ('PhoneNumber', 'phoneNumber'), ('StumpTriviaBrandingID', 'enterprise_id')]
        result = CustomConverter(result).convert(converter)
        if not result:
            return {}
        return result[0]

    def profile_update(self, profile_data, cursor):
        """
        create the user data to be updated
        :return:user_data
        """
        user_obj = profile_data
        request_params = user_obj.validated_data
        user_data = {'user_id': request_params.get('user_id'),
                     'first_name': request_params.get('firstName'),
                     'last_name': request_params.get('lastName'),
                     'phone_no': request_params.get('phoneNumber')
                    }
        if request_params.get('username'):
            user_data['username'] = request_params.get('username')

        # Fetching all the bad words from the database and then doing a filter to get all the
        # inappropriate words
        bad_words = self.inappropriate_word_check.load_inappropriate_words()

        # Get all the variables which have a name associated with them then ,
        # map all the parameters to strings and parse them to filter which would check whehter
        # these values are correct
        list_values = {key: value for (key, value) in user_data.items() if 'name' in key}
        request_str_params = list(map(str, list(list_values.values())))
        inappropriate_words = list(filter(lambda x: x in bad_words.get('dirty_words'),\
                                          request_str_params))

        if len(inappropriate_words) > 0:
            return Response(",".join(inappropriate_words) +\
                            " contains inappropriate text", status=400)

        if 'username' in user_data and \
            self.duplicate_username_check.username_exists(user_data.get('username')):
            return Response("Username is already taken", status=400)

        _result = self.update_user_profile(user_data, cursor)

        return _result

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    def role_update(self, role_data, cursor, game_cursor):
        """
        create the user data to be updated
        :return:user_data
        """
        user_obj = role_data
        request_params = user_obj.validated_data
        user_data = {'user_id': request_params.get('user_id'),
                     'status': request_params.get('status'),
                     'role': request_params.get('role'),
                     'delete_role': request_params.get('delete_role')
                    }
        _result = self.update_user_role(user_data, cursor, game_cursor)
        return _result

    def update_user_role(self, user_data, cursor, game_cursor):
        """
        update the user in the db
        :return:
        """
        store_procedure = STORE_PROC["update_status"]
        result = {}
        # Store procedure to update the status
        if user_data.get('status'):
            status = USER_UTIL.get_particular_status(user_data.get('status'), game_cursor)
            params = "@PlayerID={0}, @StumpTriviaUserStatusID={1}".\
                            format(user_data.get('user_id'), status)
            with self.connection_manager(store_procedure, params, MSSQLUSERDB) as cursor:
                result = self.fetchall_from_cursor(cursor)

        # Store procedure to update the role
        store_procedure = STORE_PROC["insert_user_role"]
        if user_data.get('role'):
            for role in ast.literal_eval(user_data.get('role')):
                _role = USER_UTIL.get_particular_role(role, game_cursor)
                try:
                    params = "@PlayerID={0}, @StumpTriviaRoleID={1}". \
                      format(user_data.get('user_id'), _role)
                    with self.connection_manager(store_procedure, params,\
                             MSSQLUSERDB) as cursor:
                        result = self.fetchall_from_cursor(cursor)
                except Exception:
                    continue

        #Store procedure to delete a role from a user
        store_procedure = STORE_PROC["delete_user_role"]
        if user_data.get('delete_role'):
            _role = USER_UTIL.get_particular_role(user_data.get('delete_role'), game_cursor)
            params = "@PlayerID={0}, @StumpTriviaRoleID={1}". \
                      format(user_data.get('user_id'), _role)
            with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
                result = self.fetchall_from_cursor(cursor)
        if result:
            result = {'user': user_data.get('user_id')}
        else:
            message = "User updation failed. Database returned no user"
            Logger.log.error(message)

        return user_data

    def update_user_profile(self, user_data, cursor):
        """
        update the user in the db
        :return:
        """
        parameters = "@PlayerID={0}, @StumpTriviaUserStatusID=1,\
                      @FirstName='{1}',\
                      @LastName='{2}'".format(user_data.get('user_id'), user_data.get('first_name'),
                                              user_data.get('last_name'))
        phone_no = "'%s'" % user_data.get('phone_no') if user_data.get('phone_no') else "NULL"
        parameters = "{0}, @PhoneNumber={1}".format(parameters, phone_no)
        store_procedure = STORE_PROC['update_user_profile_details']

        with self.connection_manager(store_procedure, parameters, MSSQLUSERDB) as cursor:
            result = self.fetchall_from_cursor(cursor)
        if result:
            result = {'user': user_data.get('user_id')}
        else:
            message = "User updation failed. Database returned no user"
            Logger.log.error(message)
        return user_data

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']],\
                check_enterprise='enterprise_id')
    @cache_api()
    @get_store_procedure_api
    def get(self, request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of users
        Returns list of users.
        """
        enterprise_id = request.GET.get("enterprise_id", None)
        roles = ast.literal_eval(request.GET.get("roles", "[]"))
        # store procedure to get user data according to enterprise and role
        # serializer call to check if fetched data is valid or not
        params = "@StumpTriviaBrandingID={0}". \
            format(enterprise_id) if enterprise_id else ""
        result = self.call_store_proc(params)
        if not result:
            return Response({}, status=200)
        if roles:
            roles_res = []
            list_calls = (self.get_group_admin, self.get_host)
            if USER_ROLES['GROUP_ADMIN'] not in roles:
                list_calls = (self.get_host,)
            for fn_calls in list_calls:
                fn_result = fn_calls(roles, result)
                if fn_result:
                    roles_res.extend(fn_result)
            else:
                result = roles_res
        converter = [('UserName', 'username'), ('LastName', 'lastName'),\
                     ('FirstName', 'firstName'), ('PlayerID', 'user_id'),\
                     ('Email', 'email'), ('StumpTriviaRoleDesc', 'roles'),\
                     ('PhoneNumber', 'phoneNumber'), ('StumpTriviaBrandingID', 'enterprise_id')]
        # Getting all the role details of the rows associated to the user
        result = CustomConverter(result).convert(converter)
        result = self.handle_duplicates(result)
        # Sorting based on user_id
        result = sorted(result, key=lambda x: x.get('user_id'))
        result = filter(lambda user: 'StumpTriviaUserStatusID' in user and \
                                     user['StumpTriviaUserStatusID'] != -1, result)
        return Response(result, status=200)

    def handle_duplicates(self, result):
        """
        Function to handle the way the data is being sent
        """
        recieved_val = {}
        for res in result:
            if res['username'] in recieved_val:
                recieved_val[res['username']]['roles'].extend(res['roles'])
            else:
                recieved_val[res['username']] = res
        return recieved_val.values()

    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    def get_host(self, roles, list_of_users):
        """
        Store procedure to get lists of hosts
        """
        if USER_ROLES['HOST'] not in roles:
            return

        roles_params = roles[USER_ROLES['HOST']]
        res = list(filter(lambda x: 'StumpTriviaRoleDesc' in x and \
                           x['StumpTriviaRoleDesc'].lower() == USER_ROLES['HOST'],\
                           list_of_users))
        if len(roles_params) > 0:
            res = list(filter(lambda x: x['status'] in roles_params, res))
        return res

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    def get_group_admin(self, roles, list_of_users):
        """
        Function call to get list of group-admins
        """
        if USER_ROLES['GROUP_ADMIN'] not in roles:
            return
        #This is going to contain the store procedure for host role
        roles_params = roles[USER_ROLES['GROUP_ADMIN']]
        res = list(filter(lambda x: 'StumpTriviaRoleDesc' in x and \
                           x['StumpTriviaRoleDesc'].lower() == USER_ROLES['GROUP_ADMIN'],\
                           list_of_users))
        if len(roles_params) > 0:
            res = list(filter(lambda x: x['StumpTriviaUserStatusID'] in roles_params, res))

        return res

    def add_status(self, obj):
        """
        Function to get the status of the user_obj that is passed
        """
        obj['status'] = USER_UTIL.get_status_by_id(obj['StumpTriviaUserStatusID'])
        return  obj

    def call_store_proc(self, parameters):
        """
        Procedure for calling store procedures for fetching the values of all the users
        """
        store_procedure = STORE_PROC["get_users_from_db"]
        with self.connection_manager(store_procedure, parameters, MSSQLUSERDB) as cursor:
            result = self.fetchall_from_cursor(cursor)
        res = list(map(self.add_status, result if result else []))
        return res
