"""
API for Stump authentication.
"""
from rest_framework.response import Response
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.services.Login.authenticate_view import AuthenticateViewSet
from cheerios.players.views.player.edit_user_info import EditUserViewSet
from cheerios.triviaweb.decorators import get_store_procedure_api
from cheerios.triviaweb.serializers import EmailSerializer
from cheerios.triviaweb.views.account_verification import AccountVerificationViewSet


class ChangePasswordViewSet(MSSQLApiView):
    """
    GET API to verify entered password is correct or not:-
    # API which allow username and password.
    # username and password should encrypted  using base64.
    # This API checks if entered password is correct or not.
    # It will return true if password matches.
    """
    @get_store_procedure_api
    def put(self, request):
        """
        PUT method to authenticate and chnage user's password
        """
        username = request.POST.get('username', None)
        password = request.POST.get('oldpassword', None)
        parameters = (username, 1)
        auth_obj = AuthenticateViewSet(parameters)
        result = auth_obj.authenticate_api()
        if not result:
            return Response(False, status=200)
        result = result[0]
        # If old password doesn't match then return false
        if auth_obj.match_password(password, result.get('Password'), result.get('Salt')):
            # If password is matching then change the password
            edit_obj = EditUserViewSet()
            result = edit_obj.post(request)
            if not result:
                return Response(False, status=200)
            # Send mail after password being changed
            # Not rolling back because if password is changed and mail is not sent,
            # then we dont want to block ther user.
            self.send_success_email(request.POST)
            return Response(True, status=200)
        return Response("Old password not matching", status=400)

    def send_success_email(self, data):
        """
        Method to trigger mail to the user
        """
        email_data = EmailSerializer(data=data)
        if email_data.is_valid():
            user_data = email_data.validated_data
            acc_obj = AccountVerificationViewSet()
            send_data = {'user_id': user_data['user_id'],
                         'name': user_data['username'],
                         'email': user_data['email'],
                         'username': user_data['username'],
                         'domain': user_data['domain'],
                         'activation_id': user_data['activation_id']}
            acc_obj.send_mail(send_data)
