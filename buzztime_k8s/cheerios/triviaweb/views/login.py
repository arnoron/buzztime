"""
API for Stump authentication.
"""

from collections import ChainMap
from rest_framework.response import Response
from cheerios.services.Login.authorization_view import AuthorizationViewSet
from cheerios.services.Login.login import LoginViewInterface
from cheerios.settings import MSSQLUSERDB, MSSQLDBGAMEPLAY
from cheerios.triviaweb.constants import STORE_PROC
from cheerios.triviaweb.decorators import get_store_procedure_api
from cheerios.triviaweb.serializers import AuthorizationSerializer, LoginSerializer
from cheerios.triviaweb.utils.utils import UtilFunction
from cheerios.triviaweb.utils.user_util import UserDetailsUtil
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger
UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()
USER_UTIL = UserDetailsUtil()


class LoginViewSet(LoginViewInterface):
    """
    GET API to verify host login:-
    # API which allow email and password.
    # email and password should encrypted  using base64.
    # This API authenticate host.
    # It will return true if host is authenticated else false.
    """

    def post(self, request):
        """
        API which allow email and password.
        email and password should encrypted  using base64.
        This API authenticate host.
        It will return true if host is authenticated else false.
        """
        return self.login_user(request)

    @get_store_procedure_api
    def login_user(self, request):
        """
        Gets the login params
        Calls the authentication and authrization viewset
        :param request:
        :return:
        """
        message = "Login API called"
        Logger.log.info(message)

        request_params = request.data
        serializer = LoginSerializer(data=request_params)
        CHEERIOS_UTILS.validate_serializer(serializer)
        cursor = self.get_cursor(MSSQLUSERDB)

        password = request_params.get('password')
        email = request_params.get('email')

        # To get username from email
        sp_get_username_from_email = STORE_PROC["get_username_from_email"]

        parameters = "@EmailAddress='{0}', @NamespaceID={1}".format(email, 1)
        self.call_storeprocedure(sp_get_username_from_email, cursor, parameters)
        result_username = self.fetchall_from_cursor(cursor)
        if not result_username:
            return Response('Invalid credentials', status=200)
        authenticated_player = self.authenticate_user(result_username[0]['Username'], password, 1)
        if authenticated_player:
            authorisation_result = self.authorize_user(authenticated_player)
        else:
            return Response('Invalid credentials', status=200)
        if authorisation_result:
            response = self.get_response(authorisation_result[0], email)
            if not response.data:
                return Response('Invalid credentials', status=200)
            else:
                return response
        else:
            return Response('Invalid credentials', status=200)



    def authorize_user(self, result):

        """
        Authorizes the user
        :return:
        """
        auth_serializer = AuthorizationSerializer(data=result)
        CHEERIOS_UTILS.validate_serializer(auth_serializer)

        authorized = AuthorizationViewSet(result.get('PlayerID'))
        result = authorized.check_host()
        if result:
            return result
        return False

    def get_response(self, result, username):
        """
        Gets the response to return
        :return:
        """

        player = {'player_id': result.get('PlayerID'),
                  'status': result.get('StumpTriviaUserStatusID'),
                  'brandID': result.get('StumpTriviaBrandingID')}
        player = dict(ChainMap(player, self.get_whitelabeling_details(result),\
                               USER_UTIL.get_user_roles(result.get('PlayerID'))))
        # Validate whether the user is Active
        player['status'] = USER_UTIL.get_user_status(player['status'])
        if player['status'] != 'Active':
            return Response(False, status=200)

        response = Response(player, status=200)
        response['Access-Control-Expose-Headers'] = 'Session'
        response["Session"] = self.get_session_key(username)
        return response

    def get_whitelabeling_details(self, result):
        """
        Get the whitelabeling details of the user who is logged in
        """
        cursor = self.get_cursor(MSSQLDBGAMEPLAY)
        store_procedure = STORE_PROC["get_white_labeling_by_id"]
        data = "@StumpTriviaBrandingID={}".format(result['StumpTriviaBrandingID'])
        self.call_storeprocedure(store_procedure, cursor, data)
        result = self.fetchall_from_cursor(cursor)
        return result[0]
