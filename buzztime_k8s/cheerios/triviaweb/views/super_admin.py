"""
API view for super admin
"""
from rest_framework.response import Response
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.triviaweb.utils.utils import CustomConverter
from cheerios.triviaweb.constants import USER_ROLES, STORE_PROC

class SuperAdminViewSet(MSSQLApiView):
    """
    API To get List of super-admin
    """
    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def get(self, _request):
        """
        Get API to connect mssql.
        It will call store procedure.
        Store procedure return results list of super admin
        """
        store_procedure = STORE_PROC['get_super_admin']
        with self.connection_manager(store_procedure, "", MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
            converter = [('AdminNotificationFlag', 'send_email'), ('UserName', 'name'),\
                         ('Email', 'email'), ('PlayerID', 'id')]
            result = CustomConverter(result).convert(converter)
        return Response(result, status=200)

    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    @get_store_procedure_api
    def put(self, request):
        """
        Super Admin updation API
        :return:
        """
        user_id = request.GET.get('user_id')
        data = request.data
        if data and user_id:
            self.update_super_admin_notification(data, user_id)
        else:
            raise ValueError("Kindly pass the correct values")
        return Response(True, status=200)

    @get_store_procedure_api
    def update_super_admin_notification(self, data, user_id):
        """
        Function to update super admin into the store procedure
        """
        store_procedure = STORE_PROC['update_super_admin_email']
        store_proc_params = "@PlayerID={0}, @AdminNotificationFlag={1}".\
                            format(int(user_id), int(data.get('send_email')))
        with self.connection_manager(store_procedure, store_proc_params, MSSQLDBGAMEPLAY) as cursor:
            self.fetchall_from_cursor(cursor)
