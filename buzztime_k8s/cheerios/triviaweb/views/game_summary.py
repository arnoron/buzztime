"""
API view for game operation.
"""
import datetime
from rest_framework.response import Response
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.utils  import CheeriosUtils
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.triviaweb.constants import STORE_PROC, USER_ROLES, TIME_START, TIME_END
from cheerios.triviaweb.serializers import GameSummary
from cheerios.services.log_classifier import Logger

CHEERIOS_UTILS = CheeriosUtils()



class GameSummaryViewSet(MSSQLApiView):
    """
    API to get game summary
    # It will create connection with MS SQL server
    # using mypool object which is sqlalchemy object used for
    # connection pooling
    # Calls stored procedure for fetching game details.
    # Parameter will be game start date and game end date.
    # Stored procedure are depends upon parameter.
    # If super admin calls this API it will get all group admin.
    # If group admin then all host and host will gets all games.
    # It will return all game details contacted by user on given period.
    """
    @get_store_procedure_api
    def get(self, request):
        """
        get API to get game summary.
        It will call store procedure.
        Store procedure will return game details.
        """
        branding = request.GET.get('branding')
        host = request.GET.get('host')
        start_date = request.GET.get('start_date')
        end_date = request.GET.get('end_date')
        location_id = request.GET.get('location_id')
        # If no date is given then by default taking 1st and current date of the month
        first_day = datetime.date.today().replace(day=1)
        # start date starts from 12am and end end time needs to end at 11.59pm
        end_date = end_date if end_date else "{0}{1}".format(datetime.date.today(), TIME_END)
        start_date = start_date if start_date else "{0}{1}".format(first_day, TIME_START)

        if branding:
            store_procedure, user_id = self.all_host(branding)

        elif host:
            store_procedure, user_id = self.host_details(host)

        else:
            store_procedure, user_id = self.all_companies()

        # to get desired parameters for procedure and creating serializer object
        serializer = GameSummary(data=dict(user_id=user_id,
                                           start_date=start_date,
                                           end_date=end_date,
                                           location_id=location_id
                                          )
                                )

        CHEERIOS_UTILS.validate_serializer(serializer)
        parameters = "@StartDate='{0}', @EndDate='{1}'".format(start_date, end_date)
        if user_id and branding:
            parameters = "@StumpTriviaBrandingID='{0}', {1}".format(user_id, parameters)
        elif user_id and host:
            parameters = "@HostPlayerID={0}, {1}".format(user_id, parameters)
        if location_id:
            parameters = "{0}, @StumpSiteID={1}".format(parameters, location_id)
        CHEERIOS_UTILS.validate_serializer(serializer)
        with self.connection_manager(store_procedure, parameters, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
        if not result:
            result = {"message": "No data for selected date range"}
            response = Response(result, status=200)
            return response

        # If page is host details, then we need to sort the list by game token in asceding order
        # In case of other pages game counts, need to sort descending to show 0 counts at
        # the bottom

        result.sort(key=lambda x: x['GameStartTime' if host else 'GameCount'],
                    reverse=True)
        response = Response(result, status=200)
        return response

    # only for SA AND GA
    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN'], USER_ROLES['GROUP_ADMIN']])
    def all_host(self, branding):
        """
        This function is to get all host and group admin of a brand
        """
        store_procedure = STORE_PROC["get_host_by_branding"]
        user_id = branding
        return store_procedure, user_id

    # only for SA AND GA
    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN'], USER_ROLES['GROUP_ADMIN']])
    def host_details(self, host):
        """
        This function is to game details of a host
        """
        store_procedure = STORE_PROC["get_host_details"]
        user_id = host
        return store_procedure, user_id

    # only for SA
    @allow_user(check_roles=[USER_ROLES['SUPER_ADMIN']])
    def all_companies(self):
        """
        This function is to get all brands game count
        """
        store_procedure = STORE_PROC["get_all_brands"]
        user_id = None
        return store_procedure, user_id
