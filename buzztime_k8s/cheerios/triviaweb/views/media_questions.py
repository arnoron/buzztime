"""
API view For Document like post, get method for any survey.
"""
import shutil
import urllib.parse
from django.core.files.storage import FileSystemStorage
from rest_framework.views import APIView
from rest_framework.response import Response
from cheerios.triviaweb.decorators import get_store_procedure_api, allow_user
from cheerios.settings import IMAGE_SERVER_STP
from cheerios.triviaweb.constants import USER_ROLES


class MediaViewSet(APIView):
    """
    API To get and upload media
    """
    def __init__(self):
        super().__init__()
        self.url_path = IMAGE_SERVER_STP

    @get_store_procedure_api
    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    def post(self, _request):
        """
        Media insertion API
        :return:
        """
        _file = _request.FILES['file']
        file_name = self.create_filename(_file,
                                        _request.POST.get('round'),
                                        _request.POST.get('question'))

        media_name = "{0}{1}/{2}/{3}/{4}/{5}/{6}".format("",\
                                                         _request.POST.get('brand'),
                                                         _request.POST.get('media_type'),
                                                         _request.POST.get('uniqueSetKey'),
                                                         _request.POST.get('round'),
                                                         _request.POST.get('question'),
                                                         file_name)
        # Using file system storage to save the file within the system
        file_storage = FileSystemStorage(location=self.url_path)
        filename = file_storage.save(media_name, _file)
        # converting the endoded url because file storage returns encoded path
        uploaded_file_url = file_storage.url(filename)
        uploaded_file_url = urllib.parse.unquote(urllib.parse.unquote(uploaded_file_url))
        return Response(uploaded_file_url, status=200)

    def create_filename(self, _file, _round, _question):
        file_name = str(_file).replace("%", "").replace("#", "")
        name = file_name.split(".")
        if name[-2] == "":
            file_name = "{0}{1}{2}".format(_round,
                                           _question,
                                           file_name)
        return file_name

    @get_store_procedure_api
    @allow_user(check_roles=[USER_ROLES['GROUP_ADMIN'], USER_ROLES['SUPER_ADMIN']])
    def delete(self, request):
        """
        Media deletion API
        """
        if not request.data.get('delete_folder'):
            self.delete_file(request)
        else:
            self.delete_folder(request)
        return Response(True, status=200)

    def delete_file(self, request):
        """
        Deletes the given file path
        """
        file_path = request.data.get('file_path')
        # Using file system storage to delete the file
        file_storage = FileSystemStorage(location=self.url_path)
        # If file path does not exist then raise this error
        if not file_storage.exists(file_path):
            raise ValueError("Incorrect file path.")
        file_storage.delete(file_path)

    def delete_folder(self, request):
        """
        Deletes the given folder path
        """
        folder_path = request.data.get('file_path')
        # Using file system storage to delete the file
        file_storage = FileSystemStorage(location=self.url_path)
        # If file path does not exist then raise this error
        if not file_storage.exists(folder_path):
            raise ValueError("Incorrect folder path.")
        # using shutil to remove file
        shutil.rmtree(folder_path)
