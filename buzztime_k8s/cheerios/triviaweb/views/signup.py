"""
APIs for user registration in stump
"""

import random
from rest_framework.response import Response

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.views.check_email_availability import EmailAvailabilityViewSet
from cheerios.players.views.check_username_availability import UsernameAvailabilityViewSet
from cheerios.players.views.player.player_details import PlayerDetails
from cheerios.players.views.player.edit_user_info import EditUserViewSet
from cheerios.services.Login.authorization_view import AuthorizationViewSet
from cheerios.services.Login.login import LoginViewInterface
from cheerios.services.check_inappropriate_word import InAppropriateWordViewSet
from cheerios.settings import MSSQLUSERDB, MSSQLDBGAMEPLAY
from cheerios.triviaweb.constants import RANDOM_CHOICE, STORE_PROC, USER_URL
from cheerios.triviaweb.decorators import get_store_procedure_api
from cheerios.triviaweb.serializers import SignUpSerializer
from cheerios.triviaweb.utils.utils import remove_cache
from cheerios.triviaweb.utils.user_util import UserDetailsUtil
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()
USER_UTIL = UserDetailsUtil()


class UserRegistrationViewSet(MSSQLApiView):
    """
    API viewset for user registration
    """
    def __init__(self):
        """
        Defines constants
        """
        super().__init__()
        self.inappropriate_word_check = InAppropriateWordViewSet()
        self.duplicate_username_check = UsernameAvailabilityViewSet()
        self.duplicate_email_check = EmailAvailabilityViewSet()
        self.authentication = LoginViewInterface()
        self.edit_obj = EditUserViewSet()
        self.player_user_registration = False
        self.existing_deleted_user = 0
        self.initial_player_user = False
        self.user_info = None

    @get_store_procedure_api
    def post(self, request):
        """
        User registration API
        :return:
        """
        sign_up_obj = SignUpSerializer(data=request.data)
        CHERRIOS_UTILS.validate_serializer(sign_up_obj)
        request_params = sign_up_obj.validated_data
        user_data = {'first_name': request_params.get('firstName'),
                     'username': request_params.get('username'),
                     'password': request_params.get('password'),
                     'last_name': request_params.get('lastName'),
                     'email': request_params.get('email'),
                     'status': request_params.get('status', 'pending'),
                     'role': request_params.get('role'),
                     'phone_no': request_params.get('phoneNumber'),
                     'whitelabel_id': request_params.get('whitelabel_id'),
                     'whitelabel_name': 'Stump Trivia'
                    }
        # Fetching all the bad words from the database and then doing a filter to get all the
        # inappropriate words
        _words = self.inappropriate_word_check.load_inappropriate_words()
        # Get all the variables which have a name associated with them then,
        # map all the parameters to strings and parse them to filter which would check whether
        # these values are correct
        list_values = {key:value for (key, value) in user_data.items() if 'name' in key}
        request_str_params = list(map(str, list(list_values.values())))
        inappropriate_words = list(filter(lambda x: x in _words['dirty_words'],\
                                         request_str_params))

        if len(inappropriate_words) > 0:
            return Response(",".join(inappropriate_words) + " contains inappropriate text",\
                             status=400)
        if self.duplicate_email_check.email_exists(user_data['email']):
            # check if the user is a stump trivia user
            if self.is_stump_registered_user(user_data['email']):
                return Response("Email is already taken", status=400)
            else:
                if self.initial_player_user \
                        and not self.players_password_match(user_data['email'],
                                                            user_data['password']):
                    return Response("Password is not matching with Players account. "
                                    "Please change your password on Buzztime.com or "
                                    "enter the same password", status=400)
                self.player_user_registration = True
        # Checks if the user is new user or user who directly registered from stump
        # then checks for duplicate username.
        if (not self.player_user_registration or not self.initial_player_user) and \
                self.duplicate_username_check.username_exists(user_data['username']):
            if not self.player_user_registration:
                return Response("Username is already taken", status=400)
            if not self.initial_player_user and \
                    self.user_info.get('UserName').upper() \
                    != user_data.get('username').upper():
                return Response("Username is already taken", status=400)


        _result = self.send_registration_req(user_data)
        #Remove cache of users from Redis
        remove_cache(key=USER_URL,\
                     param="enterprise_id={0}".format(request_params.get('whitelabel_id')))
        remove_cache(key=USER_URL)
        return Response(_result, status=200)

    def send_registration_req(self, user_data):
        """
        Registers the user in the db
        :return:
        """
        profile = {}
        salt = self.get_random_salt_value()

        hashed_password = CHERRIOS_UTILS.create_md5_hash(user_data.get('password') + salt)
        if not self.player_user_registration:

            parameters = "@Username='{0}', @Password='{1}', @Salt='{2}', @Handle='{3}',\
                          @PostalCode='{4}', @Email='{5}', @ForumUserID=0,\
                          @RegistrationSourceName='{6}'".format(\
                          user_data['username'], hashed_password, salt, user_data['first_name'],
                          user_data.get('postal_code', 1), user_data['email'],\
                          user_data['whitelabel_name'])
            store_procedure = STORE_PROC["registration"]
            with self.connection_manager(store_procedure, parameters, MSSQLUSERDB, True) as cursor:
                result = self.fetchall_from_cursor(cursor)[0]
        else:
            result = self.registered_user
        if result:
            profile = {'username': user_data.get('username'), 'player_id': result['PlayerID']}
            if not result['PlayerID']:
                message = "Sign up Registration failed. Database returned no host"
                Logger.log.error(message)
            else:
                game_cursor = self.get_cursor(MSSQLDBGAMEPLAY, True)

                if not self.existing_deleted_user:
                    store_procedure = STORE_PROC["registration_user"]

                    parameters = "@PlayerID={0}, @StumpTriviaUserStatusID={1},\
                                  @StumpTriviaBrandingID={2}, @FirstName='{3}',\
                                  @LastName='{4}'".format(result['PlayerID'], \
                                  USER_UTIL.get_particular_status('Pending', game_cursor), \
                                  int(user_data['whitelabel_id']),\
                                  user_data['first_name'], user_data['last_name'])
                    phone_no = "'%s'" % user_data.get('phone_no') if user_data.get('phone_no') \
                        else "NULL"
                    parameters = "{0}, @PhoneNumber={1}".format(parameters, phone_no)
                    with self.connection_manager(store_procedure, parameters,
                                                 MSSQLUSERDB, True) as cursor:
                        self.fetchall_from_cursor(cursor)

                else:
                    # Update the profile
                    self.update_profile(user_data, result['PlayerID'])

                    if not self.initial_player_user:
                        # Update Username
                        self.update_username(result['PlayerID'], user_data.get('username'))
                        # Change Password
                        self.update_password(result['PlayerID'], user_data.get('password'))

                    # Update the status to pending
                    self.update_user_status(result['PlayerID'])
                    # Change the brand for the user
                    self.update_brand(result['PlayerID'], int(user_data['whitelabel_id']))


                store_procedure = STORE_PROC["insert_user_role"]
                _role = USER_UTIL.get_particular_role('Host', game_cursor)
                parameters = "@PlayerID={0}, @StumpTriviaRoleID={1}".\
                    format(result['PlayerID'], _role)
                with self.connection_manager(store_procedure, parameters,
                                             MSSQLUSERDB, True) as cursor:
                    self.fetchall_from_cursor(cursor)

        else:
            message = "Sign up registration failed. Store procedure didn't return any data"
            Logger.log.error(message)
        return profile

    @staticmethod
    def get_random_salt_value():
        """
        Gets random salt value
        :return:
        """
        return ''.join(random.choice
                       (RANDOM_CHOICE)
                       for i in range(3))

    def is_stump_registered_user(self, email):
        """
        Checks if the passed username is a stump registered user
        :return:
        """
        player_details = PlayerDetails()
        self.registered_user = player_details.get_member_info(player_id=None, email=email)
        if self.registered_user:
            self.initial_player_user = self.registered_user['BirthDate'] is not None
        auth_obj = AuthorizationViewSet(self.registered_user['PlayerID'])
        result = auth_obj.check_host()
        if result:
            self.user_info = result[0]
            status_id = result[0]['StumpTriviaUserStatusID']
            status = USER_UTIL.get_user_status(status_id)
            if status == 'Deleted':
                self.existing_deleted_user = 1
            if status != 'Deleted':
                return result
        return False

    def players_password_match(self, email, password):
        """
        Checks if the password entered matches the password in the players registration
        """
        cursor = self.get_cursor(MSSQLUSERDB)

        # To get username from email
        sp_get_username_from_email = STORE_PROC["get_username_from_email"]

        parameters = "@EmailAddress='{0}', @NamespaceID={1}".format(email, 1)
        self.call_storeprocedure(sp_get_username_from_email, cursor, parameters)
        result_username = self.fetchall_from_cursor(cursor)

        result = self.authentication.authenticate_user(result_username[0]['Username'],
                                                       password, namespace_id=1)

        return result

    def update_profile(self, user_data, user_id):
        """
        Updates the user profile if user has directly registered form stump
        :param user_data:
        :param user_id:
        :return:
        """
        if self.user_info.get('LastName') != user_data.get('last_name') or \
            self.user_info.get('FirstName') != user_data.get('first_name') or \
                user_data.get('phone_no') != self.user_info.get('PhoneNumber'):
            parameters = "@PlayerID={0}, @StumpTriviaUserStatusID=1,\
                                  @FirstName='{1}',\
                                  @LastName='{2}'".format(user_id,
                                                          user_data.get('first_name'),
                                                          user_data.get('last_name'))
            phone_no = "'%s'" % user_data.get('phone_no') if user_data.get('phone_no') \
                else "NULL"
            parameters = "{0}, @PhoneNumber={1}".format(parameters, phone_no)
            store_procedure = STORE_PROC['update_user_profile_details']

            with self.connection_manager(store_procedure, parameters, MSSQLUSERDB) as cursor:
                result = self.fetchall_from_cursor(cursor)
            return result

    def update_brand(self, user_id, brand_id):
        """
        Updates user brand
        :param user_id:
        :param brand_id:
        :return:
        """
        store_procedure = STORE_PROC["update_user_brand"]
        params = "@PlayerID={0} ,@StumpTriviaBrandingID={1}". \
            format(user_id, brand_id)
        with self.connection_manager(store_procedure, params, MSSQLUSERDB) as cursor:
            self.fetchall_from_cursor(cursor)

    def update_user_status(self, user_id):
        """
        Updates user status to pending.
        :param user_id:
        :return:
        """
        game_cursor = self.get_cursor(MSSQLDBGAMEPLAY, True)
        status_id = USER_UTIL.get_particular_status('Pending', game_cursor)
        store_procedure = STORE_PROC["update_status"]
        # Update the status to pending
        params = "@PlayerID={0}, @StumpTriviaUserStatusID={1}".format(user_id, status_id)
        with self.connection_manager(store_procedure, params, MSSQLUSERDB) as cursor:
            self.fetchall_from_cursor(cursor)

    def update_username(self, player_id, new_username):
        """
        Updates username if user has registered from stump
        :param player_id:
        :param new_username:
        :return:
        """
        user_cursor = self.edit_obj.get_cursor(MSSQLUSERDB, True)
        player_info = self.edit_obj.login_viewset_interface.get_player_profile(
            player_id, user_cursor)
        data = {'player_id': player_id,
                'username': new_username}
        if player_info.get('username').upper() != new_username.upper():
            self.edit_obj.update_entity_namespace(data, player_info, user_cursor)

    def update_password(self, player_id, password):
        """
        Updates user password.
        :param player_id:
        :param password:
        :return:
        """
        user_cursor = self.edit_obj.get_cursor(MSSQLUSERDB, True)
        player_info = \
            self.edit_obj.login_viewset_interface.get_player_profile(player_id, user_cursor)
        self.edit_obj.password_and_salt = \
            self.edit_obj.get_password_and_salt(player_info.get('username'), user_cursor)
        data = {'player_id': player_id,
                'password': password}
        salt = self.edit_obj.password_and_salt['Salt']
        hashed_password = CHERRIOS_UTILS.create_md5_hash(password + salt)
        if self.edit_obj.password_and_salt['Password'] != hashed_password:
            self.edit_obj.update_namespace_password(data, user_cursor)