"""
classes for print fields in DRF doc.
"""
from __future__ import unicode_literals
from django.db import models


class Survey(models.Model):
    """
    Survey class for print fields in DRF doc.
    """
    SurveyQuestionId = models.IntegerField()
    GameInstanceId = models.IntegerField()
    GameTeamId = models.IntegerField()
    Response = models.TextField()


class Team(models.Model):
    """
    Team class for print fields in DRF doc.
    """
    Teams = models.TextField()


class Game(models.Model):
    """
    Game class for print fields in DRF doc.
    """
    QuestionId = models.IntegerField()
    HostId = models.BigIntegerField()
    GameId = models.IntegerField()
    VenueId = models.IntegerField()
    GameStartTime = models.DateTimeField()
    GameToken = models.TextField()


class GameDetail(models.Model):
    """
    GameDetail class for printing fields in DRF doc.
    """
    GameToken = models.TextField()
