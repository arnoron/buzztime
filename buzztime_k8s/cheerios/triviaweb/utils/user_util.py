"""
Util file for user details
"""
from django.utils.lru_cache import lru_cache
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.settings import MSSQLDBGAMEPLAY
from cheerios.triviaweb.constants import STORE_PROC


class UserDetailsUtil(MSSQLApiView):
    """
    Util class for user details
    """
    from cheerios.triviaweb.decorators import get_store_procedure_api
    @lru_cache(maxsize=32)
    @get_store_procedure_api
    def get_all_statuses(self, cursor=None):
        """
        Method to fetch all host statuses in the database
        """
        if not cursor:
            cursor = self.get_cursor(MSSQLDBGAMEPLAY)
        store_procedure = STORE_PROC['get_all_status']
        self.call_storeprocedure(store_procedure, cursor, "")
        result = self.fetchall_from_cursor(cursor)
        return result

    @lru_cache(maxsize=32)
    @get_store_procedure_api
    def get_all_roles(self, cursor=None):
        """
        Method to fetch all host roles in the database
        """
        if not cursor:
            cursor = self.get_cursor(MSSQLDBGAMEPLAY)
        store_procedure = STORE_PROC['get_all_roles']
        self.call_storeprocedure(store_procedure, cursor, "")
        result = self.fetchall_from_cursor(cursor)
        return result

    @get_store_procedure_api
    def get_user_roles(self, user_id):
        """
        Method to fetch user roles
        """
        store_procedure = STORE_PROC["get_user_roles"]
        params = "@PlayerID={0}".format(user_id)
        with self.connection_manager(store_procedure, params, MSSQLDBGAMEPLAY) as cursor:
            result = self.fetchall_from_cursor(cursor)
        roles = [role['StumpTriviaRoleDesc'].lower() for role in result]
        return {'roles': roles}

    @lru_cache(maxsize=32)
    def get_user_status(self, status_id):
        """
        Method to get the user status
        """
        status = [stat['StumpTriviaUserStatusDesc'] for stat in self.get_all_statuses()\
                   if stat['StumpTriviaUserStatusID'] == status_id]
        return status[0]

    @lru_cache(maxsize=32)
    def get_particular_role(self, role_name, cursor=None):
        """
        Method to fetch get a particular role
        """
        _roles = self.get_all_roles(cursor)
        return [role['StumpTriviaRoleID'] for role in _roles\
                if role['StumpTriviaRoleDesc'].lower() == role_name.lower()][0]

    @lru_cache(maxsize=32)
    def get_particular_status(self, check, cursor=None):
        """
        Method to fetch get a particular status
        """
        status = self.get_all_statuses(cursor)
        return [stat['StumpTriviaUserStatusID'] for stat in status if\
                 stat['StumpTriviaUserStatusDesc'].lower() == check.lower()][0]

    @lru_cache(maxsize=32)
    def get_status_by_id(self, check):
        """
        Method to fetch get a particular status by id
        """
        status = self.get_all_statuses()
        return [stat['StumpTriviaUserStatusDesc'] for stat in status if\
                 stat['StumpTriviaUserStatusID'] == check][0]
