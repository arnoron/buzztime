"""
Common functions for triviaweb.
"""
import ast
import collections
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from cheerios.services.redis import RedisSession



class UtilFunction():
    """
    Util function which allow to other function to do specific task.
    This function is common for all classes.
    """

    # to be removed from here
    @staticmethod
    def is_positive_number(data):
        """
        Check if numbr passed is a positive number or not
        :param data:
        :return: True/False
        """
        result = False
        if data > 0:
            result = True
        if not result:
            raise ValueError("Parameter passed should be a positive integer.")
        else:
            return result

    @staticmethod
    def represents_int(value):
        """
        Convert value into int.
        """
        try:
            int(value)
            return True
        except ValueError:
            return False


    @staticmethod
    def validate_email(email):
        """
        Validate email id.
        """
        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    @staticmethod
    def string_to_dict(data):
        """
        Method to convert string representation of dictionary
        into dictionary
        :param data:
        :return:
        """
        if data:
            return ast.literal_eval(data)


def remove_cache(key=None, param=None):
    """
    Function to remove cache matching the key and parameters passed as values
    """

    redis_obj = RedisSession()
    redis_obj.remove_keys(key+"*")

    return


def ping_redis_master():
    """
    Pings redis master and lets us know if it is available
    :return:
    """
    redis_obj = RedisSession()
    redis_obj.ping_master()


def build_html_table_message(user_data):
    """
    The following method would be used to build an html message.
    This is used for Registration.
    :param user_data : list containing of head and body of the element
    """
    UserTable = collections.namedtuple('UserTable', 'head body')
    user_data = list(map(lambda x: UserTable(*x), user_data))
    _listed_items = ["<div class='message-data'>\
                        <span class='msg-head-1'>{0} :</span>\
                        <span style='font-weight:bold;padding-left:15px;'>{1}</span>\
                      </div>".format(user.head, user.body) for user in user_data]
    return "".join(_listed_items)

class ListUserObjectsUnique:
    """
    List of unique user objects
    """
    def __init__(self, value, key):
        """
        Initialize the values to understand how we can set unique objects
        """
        self.value = value
        self.key = key

    def __eq__(self, other):
        """
        Customizing eq function to check and compare the required functions
        """
        return self.value.get(self.key) == other.value.get(other.key)

    def __hash__(self):
        """
        To return the hash function so that we can run set and get the required values
        """
        return hash(('user', self.value.get(self.key)))

    def __repr__(self):
        """
        Just for displaying purposes
        """
        return self.value.get(self.key)


class GameDetailsException(Exception):
    """
    Game details exception class.
    """
    pass


class GameShouldNotDeleteException(Exception):
    """
    Game Should Not Delete Exception.
    """
    pass

class CustomConverter():
    """
    Converter to convert the fields from store procedure
    """
    def __init__(self, data):
        self.data = data
    def convert(self, args):
        """
        Convert the arguments to required fields
        """
        if not self.data:
            return []
        _data = list(map(lambda x: self.convert_into_req_fields(x, args), self.data))
        return _data

    @staticmethod
    def convert_into_req_fields(_element, args):
        """
        Function to add or push the required key to the desired result value
        """
        #Expecting args to contain a list of tuples
        for key1, key2 in args:
            if key1 not in _element:
                continue
            _element[key2] = _element.pop(key1)
            if key2 == 'roles':
                _element[key2] = [_element[key2]]
        return _element
