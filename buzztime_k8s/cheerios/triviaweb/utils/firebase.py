"""
interface for third party Caching service.
"""
import pyrebase
from cheerios.interface.caching_service import CacheDataView
from cheerios.services.log_classifier import Logger
from cheerios.settings import FIREBASE_KEY, FIREBASE_EMAIL, FIREBASE_PWD, FIREBASE_STORAGEBUCKET, \
    FIREBASE_AUTHDOMAIN, FIREBASE_API_KEY


class FirebaseService(CacheDataView):
    """
    Class to serve all firebase services.
    """

    def create_cache_connection(self):
        """
        This function will be implemented as per
        the external service used for caching data.
        This function will contain code to create connection
        with specific url/node in caching service
        :return:
        """
        config = {
            "apiKey": FIREBASE_API_KEY,
            "authDomain": FIREBASE_AUTHDOMAIN,
            "databaseURL": FIREBASE_KEY,
            "storageBucket": FIREBASE_STORAGEBUCKET
        }

        self.firebase_obj = pyrebase.initialize_app(config)

        # Creating the firebase auth object for authorizing the user
        auth = self.firebase_obj.auth()


        # Log the user in with the firebase user credentials
        self.user = auth.sign_in_with_email_and_password(FIREBASE_EMAIL, FIREBASE_PWD)

        # Get a reference to the database service
        self.firebase_obj = self.firebase_obj.database()

    def get_cache_data(self, url):
        """
        Get node value from passed url
        from firebase cache
        :param url:
        :param node:
        :return:
        """
        # getting the data from url passed
        Logger.log.info("---in get cache---")
        firebase_data = self.firebase_obj.child(url).get(self.user['idToken'])
        Logger.log.info(firebase_data.val())
        return firebase_data.val()

    def remove_cache_data(self, url, node):
        """
        Remove passed node from given url
        from firebase cache
        :param url:
        :param node:
        :return:
        """
        # If node is present then it will go to node child and remove the values
        # If node is not present then it will remove the whole object for the mentioned URL
        obj = self.firebase_obj.child(url).child(node) if node else self.firebase_obj.child(url)
        obj.remove(self.user['idToken'])

    def open_cache_connection(self):
        """
        Opening cache connection.
        """
        pass

    def update_cache_data(self, url, key, data):
        """
        Update data for passed node in firebase
        :param url:
        :param node:
        :return:
        """
        self.firebase_obj.child(url).child(key).set(data, self.user['idToken'])
