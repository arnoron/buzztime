"""ucars URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from cheerios.triviaweb.views.customize import CustomizeViewSet
from cheerios.triviaweb.views.trivia_question import QuestionViewSet
from cheerios.triviaweb.views.login import LoginViewSet
from cheerios.triviaweb.views.game_details import GameDetailViewSet
from cheerios.triviaweb.views.game_summary import GameSummaryViewSet
from cheerios.triviaweb.views.signup import UserRegistrationViewSet
from cheerios.triviaweb.views.survey import SurveyViewSet
from cheerios.triviaweb.views.account_verification import AccountVerificationViewSet
from cheerios.triviaweb.views.enterprise import EnterpriseViewSet
from cheerios.triviaweb.views.user import UserViewSet
from cheerios.triviaweb.views.user_get import UserGetViewSet
from cheerios.triviaweb.views.change_password import ChangePasswordViewSet
from cheerios.triviaweb.views.forgot_password import ForgotPasswordViewSet
from cheerios.triviaweb.views.days import DaysViewSet
from cheerios.triviaweb.views.locations import LocationViewSet
from cheerios.triviaweb.views.super_admin import SuperAdminViewSet
from cheerios.triviaweb.views.super_admin_email import SuperAdminEmailViewSet
from cheerios.triviaweb.views.media_questions import MediaViewSet
from cheerios.triviaweb.views.question_draft import DraftViewSet
from cheerios.triviaweb.views.question_upload_parser import QuestionParserViewSet
from cheerios.triviaweb.views.ads import AdViewSet
from cheerios.triviaweb.views.enterprise_activate import EnterpriseActivationViewSet

urlpatterns = [
    url(r'^question/$', QuestionViewSet.as_view()),
    url(r'^login/$', LoginViewSet.as_view()),
    url(r'^game/$', GameDetailViewSet.as_view()),
    # url(r'^team/$', TeamViewSet.as_view()),
    url(r'^survey/$', SurveyViewSet.as_view()),
    url(r'^signup/$', UserRegistrationViewSet.as_view()),
    url(r'^account_verify/$', AccountVerificationViewSet.as_view(), name='account_verify'),
    url(r'^users/$', UserViewSet.as_view()),
    url(r'^enterprises/$', EnterpriseViewSet.as_view()),
    url(r'^get-user/$', UserGetViewSet.as_view()),
    url(r'^setting/$', CustomizeViewSet.as_view()),
    url(r'^change_password/$', ChangePasswordViewSet.as_view()),
    url(r'^game_summary/$', GameSummaryViewSet.as_view()),
    url(r'^forgot-password/$', ForgotPasswordViewSet.as_view()),
    url(r'^days/$', DaysViewSet.as_view()),
    url(r'^locations/$', LocationViewSet.as_view()),
    url(r'^super-admin/$', SuperAdminViewSet.as_view()),
    url(r'^super-admin-email/$', SuperAdminEmailViewSet.as_view()),
    url(r'^media-questions/$', MediaViewSet.as_view()),
    url(r'^draft-questions/$', DraftViewSet.as_view()),
    url(r'^ads/$', AdViewSet.as_view()),
    url(r'^question-parser/$', QuestionParserViewSet.as_view()),
    url(r'^activate-enterprise/$', EnterpriseActivationViewSet.as_view())
]
