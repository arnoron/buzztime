"""ucars URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from cheerios.foodmenu.views.ncr_menu_with_promise import PingView, FoodMenuView
#from cheerios.foodmenu.views.ncr_menu import PingView, FoodMenuView

urlpatterns = [
    url(r'^ping/$', PingView.as_view()),
    url(r'^ncr/$', FoodMenuView.as_view()),
]
