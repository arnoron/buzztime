# """
# Test cases for Food menu live interface
# """
# import hashlib
#
# from rest_framework import status
# from rest_framework.test import APITestCase
#
# from cheerios.foodmenu.test.test_data import response_status, get_ncr_menu_data,\
#     get_ncr_new_menu_data # , get_ncr_formated_external_data, get_formatted_menu_data
#
# URL = dict(ping='/foodmenu/ping/',
#            gomenu='/foodmenu/ncr/?siteid=15&menuid=1001',
#            datemenu='/foodmenu/ncr/?siteid=15&menuid=1001&promisetime=2016-06-16')
#
#
# class TestPing(APITestCase):
#     """
#     Testcase for get operation. To check ping api
#     """
#
#     def test_health_check(self):
#         """
#         Test If connection to db is established or not
#         Created connection host and expected host are different
#         """
#         response = self.client.get(URL['ping'])
#         self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#
# class TestFoodMenuView(APITestCase):
#     """
#     Testcases for get operation.
#     """
#     def test_without_headers(self):
#         """
#         Call api without any headers.
#         """
#         url = URL['gomenu']
#         response = self.client.get(url)
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     # def test_with_authorization_headers(self):
#     #     """
#     #     Call api with all headers.
#     #     """
#     #     url = URL['gomenu']
#     #     response = self.client.get(url, format='json',
#     #                                HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#     #                                HTTP_X_API_COMPANYCODE='BWWI001')
#     #     self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_with_auth_headers(self):
#         """
#         Call api with only authorization header.
#         """
#         url = URL['gomenu']
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_company_code_headers(self):
#         """
#         Call api with Company Code headers.
#         """
#         url = URL['gomenu']
#         response = self.client.get(url, format='json',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_etag_null_headers(self):
#         """
#         Test Etag functionality for foodmenu api
#         """
#         url = URL['gomenu']
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001',
#                                    HTTP_IF_NONE_MATCH=None)
#         self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_with_etag_headers(self):
#         """
#         Test Etag functionality for foodmenu api
#         """
#         url = URL['gomenu']
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001',
#                                    HTTP_IF_NONE_MATCH=None)
#         self.assertEqual(response_status(response), status.HTTP_200_OK)
#         etag = response['Etag']
#
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001',
#                                    HTTP_IF_NONE_MATCH=etag)
#         self.assertEqual(response_status(response), status.HTTP_304_NOT_MODIFIED)
#
#     def test_with_diff_etag_headers(self):
#         """
#         Test Etag functionality for foodmenu api
#         """
#         url = URL['gomenu']
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001',
#                                    HTTP_IF_NONE_MATCH='284fe02cf7f275b000c05f8de203f2f3n')
#         self.assertEqual(response_status(response), status.HTTP_200_OK)
#         etag = response['Etag']
#
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001',
#                                    HTTP_IF_NONE_MATCH=etag)
#         self.assertEqual(response_status(response), status.HTTP_304_NOT_MODIFIED)
#     #
#     # def test_to_verify_md5(self):
#     #     """
#     #     Test to verify if data has changed format
#     #     md5 will be still same.
#     #     :return:
#     #     """
#     #     normal_data = get_ncr_menu_data()
#     #     hash_normal_data = hashlib.md5(str.encode(str(normal_data))).hexdigest()
#     #     formatted_data = get_ncr_formated_external_data()
#     #     hash_formatted_data = hashlib.md5(str.encode(str(formatted_data))).hexdigest()
#     #     self.assertEqual(hash_normal_data, hash_formatted_data)
#     #     # Changed sequence of data in sub-menu key
#     #     formatted_internal_data = get_formatted_menu_data()
#     #     hash_formatted_internal_data = hashlib.md5(
#     #         str.encode(str(formatted_internal_data))).hexdigest()
#     #     self.assertNotEqual(hash_normal_data, hash_formatted_internal_data)
#     #
#     def test_to_verify_md5_change(self):
#         """
#         Test to verify if data has changed
#         md5 will create new key.
#         :return:
#         """
#         menu_data = get_ncr_menu_data()
#         hash_menu_data = hashlib.md5(str.encode(str(menu_data))).hexdigest()
#         new_menu_data = get_ncr_new_menu_data()
#         hash_new_menu_data = hashlib.md5(str.encode(str(new_menu_data))).hexdigest()
#         self.assertNotEqual(hash_menu_data, hash_new_menu_data)
#
#     def test_with_invalid_params(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = "/foodmenu/ncr/?siteid=%&menuid=1001"
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_promise_time(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = URL['datemenu']
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_200_OK)
#
#     def test_with_invalid_promise_time(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=1435&menuid=1001&promisetime=2016-06-16T10:35:00'
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_empty_promise_time(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=1435&menuid=1001&promisetime='
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_special_char_promise(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=1435&menuid=1001&promisetime=%'
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_empty_siteid(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=""&menuid=1001'
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_empty_menuid(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=1435&menuid='
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_invalid_menuid(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=1435&menuid=%'
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_string_menuid(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=1435&menuid=testMenu'
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_string_siteid(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=testMenu&menuid=1001'
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
#
#     def test_with_string_promise_date(self):
#         """
#         Call api with invalid siteid.
#         """
#         url = '/foodmenu/ncr/?siteid=1435&menuid=1001&promisetime=testPromiseDate'
#         response = self.client.get(url, format='json',
#                                    HTTP_AUTHORIZATION='YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=',
#                                    HTTP_X_API_COMPANYCODE='BWWI001')
#         self.assertEqual(response_status(response), status.HTTP_400_BAD_REQUEST)
