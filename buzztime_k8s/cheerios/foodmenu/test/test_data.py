"""
Test data for Food Menu live api
"""

def response_status(response):
    """
    Common function which send response status code from response.
    """
    return response.status_code


def get_ncr_menu_data():
    """
    Return Response for NCR api call.
    :return:
    """
    return {
        "$id": "1",
        "Menus": [
            {
                "$id": "2",
                "MenuId": 1001,
                "Name": "Buzztime - Buffalo Wild Wings",
                "Description": None,
                "MenuType": 0,
                "ExternalId": "",
                "SupportedOrderModes": 1,
                "IsVisible": None,
                "MenuAttribute": 2,
                "DisplayName": "Buzztime - Buffalo Wild Wings",
                "SubMenus": [
                    173,
                    153,
                    160,
                    161,
                    197,
                    198,
                    164,
                    167,
                    165,
                    166,
                    158,
                    195,
                    145,
                    169,
                    171,
                    154
                ]
            }
        ],
        "SubMenus": [
            {
                "$id": "3",
                "SubMenuId": 145,
                "Name": "ADD-ONS",
                "HeaderImageName": "submenu_145.png",
                "Description": "",
                "Subtitle": None,
                "IsSelectable": True,
                "IsVisible": True,
                "Restrictions": None,
                "DisplayName": "ADD-ONS",
                "MenuItems": [
                    1356,
                    1360,
                    1359,
                    1357,
                    1321,
                    1330,
                    2095,
                    1326,
                    1358,
                    1322,
                    1323,
                    1324
                ],
                "ExternalId": None
            }
        ]
    }


def get_ncr_formated_external_data():
    """
    Return same data as above function.
    But it has different format.
    :return:
    """
    return {
        "$id": "1",
        "SubMenus": [
            {
                "$id": "3",
                "SubMenuId": 145,
                "Name": "ADD-ONS",
                "HeaderImageName": "submenu_145.png",
                "Description": "",
                "Subtitle": None,
                "IsSelectable": True,
                "IsVisible": True,
                "Restrictions": None,
                "DisplayName": "ADD-ONS",
                "MenuItems": [
                    1356,
                    1360,
                    1359,
                    1357,
                    1321,
                    1330,
                    2095,
                    1326,
                    1358,
                    1322,
                    1323,
                    1324
                ],
                "ExternalId": None
            }
        ],
        "Menus": [
            {
                "$id": "2",
                "MenuId": 1001,
                "Name": "Buzztime - Buffalo Wild Wings",
                "Description": None,
                "MenuType": 0,
                "ExternalId": "",
                "SupportedOrderModes": 1,
                "IsVisible": None,
                "MenuAttribute": 2,
                "DisplayName": "Buzztime - Buffalo Wild Wings",
                "SubMenus": [
                    173,
                    153,
                    160,
                    161,
                    197,
                    198,
                    164,
                    167,
                    165,
                    166,
                    158,
                    195,
                    145,
                    169,
                    171,
                    154
                ]
            }
        ]
    }


def get_formatted_menu_data():
    """
    Return Response for NCR api call.
    :return:
    """
    return {
        "$id": "1",
        "Menus": [
            {
                "ExternalId": "",
                "SupportedOrderModes": 1,
                "IsVisible": None,
                "MenuAttribute": 2,
                "DisplayName": "Buzztime - Buffalo Wild Wings",
                "$id": "2",
                "MenuId": 1001,
                "Name": "Buzztime - Buffalo Wild Wings",
                "Description": None,
                "MenuType": 0,
                "SubMenus": [
                    173,
                    153,
                    160,
                    161,
                    197,
                    198,
                    164,
                    167,
                    165,
                    166,
                    158,
                    195,
                    145,
                    169,
                    171,
                    154
                ]
            }
        ],
        "SubMenus": [
            {
                "$id": "3",
                "SubMenuId": 145,
                "Name": "ADD-ONS",
                "HeaderImageName": "submenu_145.png",
                "Description": "",
                "Subtitle": None,
                "IsSelectable": True,
                "IsVisible": True,
                "Restrictions": None,
                "DisplayName": "ADD-ONS",
                "MenuItems": [
                    1356,
                    1360,
                    1359,
                    1357,
                    1321,
                    1330,
                    2095,
                    1326,
                    1358,
                    1322,
                    1323,
                    1324
                ],
                "ExternalId": None
            }
        ]
    }


def get_ncr_new_menu_data():
    """
    Return Response for NCR api call.
    :return:
    """
    return {
        "$id": "1",
        "Menus": [
            {
                "$id": "2",
                "MenuId": 1001,
                "Name": "Buzztime - Buffalo Spicy Wild Wings",
                "Description": None,
                "MenuType": 0,
                "ExternalId": "",
                "SupportedOrderModes": 1,
                "IsVisible": None,
                "MenuAttribute": 2,
                "DisplayName": "Buzztime - Buffalo Spicy Wild Wings",
                "SubMenus": [
                    173,
                    153,
                    160,
                    161,
                    197,
                    198,
                    164,
                    167,
                    165,
                    166,
                    158,
                    195,
                    145,
                    169,
                    171,
                    154
                ]
            }
        ],
        "SubMenus": [
            {
                "$id": "3",
                "SubMenuId": 145,
                "Name": "Main CourseS",
                "HeaderImageName": "submenu_145.png",
                "Description": "",
                "Subtitle": None,
                "IsSelectable": True,
                "IsVisible": True,
                "Restrictions": None,
                "DisplayName": "Main Course",
                "MenuItems": [
                    1356,
                    1360,
                    1359,
                    1357,
                    1321,
                    1330,
                    2095,
                    1326,
                    1358,
                    1322,
                    1323,
                    1324
                ],
                "ExternalId": None
            }
        ]
    }
