"""
This file contains Constants and
classes required for Food Menu live api
"""
import hashlib
import requests
import ast
from rest_framework.response import Response
from rest_framework.views import APIView

from cheerios.constant.constant import NCR_ALLOWED_STATUS
from cheerios.services.redis import RedisSession
from cheerios.interface.baseclass import CachedApiView
from cheerios.settings import NCRTTL
from cheerios.services.log_classifier import Logger
from cheerios.utils import CheeriosUtils

NCR_URL = 'https://api.ncr.com/tto/v1/aoo/Menus/'
CHERRIOS_UTILS = CheeriosUtils()

API_TIMEOUT = 10


class FoodMenuView(CachedApiView):# pragma: no cover
    """
     API for Food Menu view.
     1. GET Latest Menu from GoMenu.
         It call GoMenu API and get latest menu form GoMenu.
         Set TTL as 30 minutes and return
         Response object with cache expiry to nginx
     """

    def __init__(self):
        self.redis_data = None

    def get_data_from_request(self, request):
        """
        Overriding Interface CachedApiView function.
        This function accepts request as input
        It will process request object and return data as per our requirement.
        :param request:
        :return:
        """
        return dict(company_code=request.META.get('HTTP_X_API_COMPANYCODE'),
                    authorization=request.META.get('HTTP_AUTHORIZATION'),
                    menu_id=request.GET.get('menuid'),
                    site_id=request.GET.get('siteid'),
                    etag=request.META.get('HTTP_IF_NONE_MATCH'))

    def get_from_downstream(self, request_object, *args):
        """
        Overriding Interface CachedApiView function.
        It accepts an object and list of arguments.
        It will process request_object which was created in
        get_data_from_request function.
        This function will set site_id and menu_id and create url.
        Call NCR api and returns data.
        :param request_object:
        :param args:
        :return:
        """
        message = "Food Menu API called"
        Logger.log.info(message)
        site_id = request_object.get('site_id')
        menu_id = request_object.get('menu_id')
        company_code = request_object.get('company_code')
        authorization = request_object.get('authorization')

        try:
            redis_data = self.get_from_redis(request_object)
            if redis_data:
                self.redis_data = ast.literal_eval(redis_data.decode("utf-8"))
        except:
            CHERRIOS_UTILS.log_error(500, "Redis is down unable to fetch NCR data")
            self.redis_data = None

        data = self.redis_data
        if data:
            etag = request_object.get('etag')     
            if etag == data['ETag']:
                message = "No change in Data"
                Logger.log.info(message)
                return Response('', status=304)
            else:
                response = self.create_etag(Response(data['response'], status=200), data['ETag'])
                return response
        else:
            if site_id and site_id.isdigit() \
                    and menu_id and menu_id.isdigit() \
                    and company_code and authorization:
                message = "Data passed: Site_id: {0}, Menu_id: {1}, Company_Code: {2}".format(
                    site_id, menu_id, company_code)
                Logger.log.info(message)
                food_menu_url = self.create_foodmenu_url(site_id, menu_id)
                Logger.log.info("Url: %s", food_menu_url)
                headers = {
                    'X-Api-CompanyCode': company_code,
                    'Authorization': 'Basic ' + authorization
                }
                data = requests.get(food_menu_url, headers=headers, timeout=API_TIMEOUT)
                json_data = data.json()
                data_etag = hashlib.md5(str.encode(str(json_data))).hexdigest()
                try:
                    self.save_to_redis(request_object, json_data, data_etag, data.status_code)
                except:
                    CHERRIOS_UTILS.log_error(500, "Redis is down unable to save NCR data")

                if request_object.get('etag') == data_etag:
                    message = "No change in Data"
                    Logger.log.info(message)
                    return Response('', status=304)
                return self.create_etag(Response(json_data, data.status_code), data_etag)
            else:
                error = {
                    "Error": "Empty parameters passed",
                    "ErrorCode": 400
                }
                Logger.log.error(error)
                return Response(error, status=400)

    @staticmethod
    def create_etag(response_obj, data_etag):
        """
        Function to set ETag with Response object passed.
        :param response_obj:
        :param data_etag:
        :return:
        """
        response_obj['ETag'] = data_etag
        return response_obj

    def get_ttl(self, data, request_object=None):
        """
        Overriding Interface CachedApiView function.
        This function will calculate TTL for every request.
        For Erros we have set the cache s 5 mins,
        for other api's it is 30 mins.
        :param data:
        :return:
        """
        if data.status_code in NCR_ALLOWED_STATUS:
            redis_data = self.redis_data
            ttl = NCRTTL
            if redis_data:
                redis = RedisSession()
                key = self.create_key(request_object)
                ttl = redis.get_ttl(key)
            message = "Data TTL set to {0}".format(ttl)
            Logger.log.info(message)
        else:
            message = "Error Details: {0}, Error Status {1}" .\
                format(data, data.status_code)
            Logger.log.error(message)
            ttl = 60 * 5
        return ttl

    @staticmethod
    def create_foodmenu_url(site_id, menu_id):
        """
        Function to cerate food menu api
        :param site_id:
        :param menu_id:
        :return:
        """
        food_menu_url = NCR_URL + site_id + '/' + menu_id + '?includeInvisible=true'
        return food_menu_url

    def get_from_redis(self, request_object):
        """
        Get NCR data from redis if NCR data is saved in redis.
        """
        redis = RedisSession()
        key = self.create_key(request_object)
        data = redis.get_key(key)
        return data

    def save_to_redis(self, request_object, response, etag, status):
        """
        Save NCR data to redis.
        Etag and response both are saved in redis.
        """
        data = {}
        data['response'] = response
        data['ETag'] = etag
        redis = RedisSession()
        key = self.create_key(request_object)
        ttl = 60*5
        if status in NCR_ALLOWED_STATUS:
            ttl = NCRTTL
        redis.save_data(key, data, ttl=ttl)

    @staticmethod
    def create_key(request_object):
        """
        Create key for NCR to save response in redis.
        """
        site_id = request_object.get('site_id')
        menu_id = request_object.get('menu_id')
        company_code = request_object.get('company_code')
        authorization = request_object.get('authorization')
        decode_key = "{0}{1}{2}{3}".format(site_id, menu_id, company_code, authorization)
        key = hashlib.md5(str.encode(decode_key)).hexdigest()
        return key


class PingView(APIView):# pragma: no cover
    """
    Get api created to test response time
    This is a test api.
    To test the response time.
    It will just return status 200
    """
    @staticmethod
    def get(request):
        """
        Get API created for Response time check.
        :param request:
        :return: 200 status
        """
        return Response("", status=200)
