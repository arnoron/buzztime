import pyrebase

from cheerios.settings import MM_FIREBASE_EMAIL, MM_FIREBASE_PWD


class Firebase:
    """
    Class to serve all firebase services.
    """

    def __init__(self, api_key, auth_domain, firebase_key, firebase_storage_bucket):
        """
        Initializes connection parameters
        """
        self.config = {
                        "apiKey": api_key,
                        "authDomain": auth_domain,
                        "databaseURL": firebase_key,
                        "storageBucket": firebase_storage_bucket
                       }
        self.user = None
        self.firebase_obj = None

    def create_connection(self):
        """
        his function will contain code to create connection
        with specific url/node in caching service
        :return:
        """
        firebase_obj = pyrebase.initialize_app(self.config)
        auth = firebase_obj.auth()
        self.user = auth.sign_in_with_email_and_password(MM_FIREBASE_EMAIL, MM_FIREBASE_PWD)
        self.firebase_obj = firebase_obj.database()

    def update(self, url, key, value):
        """
        Update data for passed node in firebase
        :return:
        """
        self.firebase_obj.child(url).child(key).set(value, self.user['idToken'])

    def push(self, url, key, data):
        """
        Push data for passed node in firebase
        :param url:
        :param key:
        :param data:
        :return:
        """
        self.firebase_obj.child(url).child(key).push(data, self.user['idToken'])
