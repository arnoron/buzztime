"""
Checks if a word is inappropriate or not
"""
import re
from ast import literal_eval

from rest_framework.response import Response

from cheerios.constant.constant import ONE_WEEK
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.constants import STORED_PROCEDURE_MAPPER
from cheerios.players.decorators import close_connection
from cheerios.services.redis import RedisSession
from cheerios.settings import MSSQLPLAYERPLUSDB

OBSCENITY_LEVEL = 0

class InAppropriateWordViewSet(MSSQLApiView):
    """
    Checks if the word query param contains any inappropriate word
    """

    def contains_inappropriate_word(self, word_to_validate, dirty_words_list):
        """

        :param word_to_validate:
        :param dirty_words_list:
        :return:
        """
        for exception_word in dirty_words_list['exceptions']:
            regexp = re.compile(exception_word, re.IGNORECASE)
            if regexp.search(word_to_validate):
                word_to_validate.replace(exception_word, '')

        for dirty_word in dirty_words_list['dirty_words']:
            regexp = re.compile(dirty_word, re.IGNORECASE)
            if regexp.search(word_to_validate):
                return True

        return False

    def load_inappropriate_words(self):
        """
        Loads all the dirty words from the cache if available
        else load from DB and store in cache
        before returning back
        :return:
        """
        redis_obj = RedisSession()

        try:
            dirty_words = redis_obj.get_value(key="dirty_words")
        except Exception:
            return Response("Internal Server Error. Please try after some time", 500)

        if dirty_words:
            try:
                dirty_words = literal_eval(dirty_words.decode("utf-8"))
            except Exception:
                return Response("Internal Server Error. Please try after some time", 500)
        else:
            dirty_words = self.get_dirty_words_from_db()
            redis_obj.save_data("dirty_words", dirty_words, ttl=ONE_WEEK)

        return dirty_words

    @close_connection
    def get_dirty_words_from_db(self):
        """

        :return:
        """
        parameters = (OBSCENITY_LEVEL)

        store_procedure = STORED_PROCEDURE_MAPPER['inappropriate_word_status']

        cursor = self.get_cursor(MSSQLPLAYERPLUSDB)

        self.call_storeprocedure(store_procedure, cursor, parameters)

        result = self.fetchall_from_cursor(cursor)
        unallowed_word_dict = {'dirty_words': [], 'exceptions': []}
        for unallowed_word in result:
            if unallowed_word.get('Type') == 1:
                unallowed_word_dict['dirty_words'].append(unallowed_word.get('DirtyWord'))
            elif unallowed_word.get('Type') == 99:
                unallowed_word_dict['exceptions'].append(unallowed_word.get('DirtyWord'))

        return unallowed_word_dict
