"""
A service to send mails
"""

import datetime
import traceback

from suds.client import Client

from cheerios.players.constants import BRONTO_WSDL
from cheerios.settings import BRONTO_API_KEY, BRONTO_FIELD_REF, BUZZTIME_SUBSCRIBERS_LIST_ID
from cheerios.services.log_classifier import Logger
from cheerios.utils import CheeriosUtils

CHERRIOS_UTILS = CheeriosUtils()


class Email():
    """
    Email Class to send mails
    """

    def __init__(self, recipient_details=None, sender_details=None, message_id=None,
                 email_fields=None, domain=None, jwt=None, message=None):
        """
        Initializes all the email parameters
        """
        if recipient_details:
            self.recipient_username = recipient_details.get('username')
            self.recipient_email = recipient_details.get('email')
            self.recipient_display_name = recipient_details.get('display_name')
            self.recipient_password = recipient_details.get('password')
        if sender_details:
            self.sender_name = sender_details.get('name')
            self.sender_email = sender_details.get('email')

        self.message_id = message_id
        self.api_token = BRONTO_API_KEY
        self.bronto_wsdl = BRONTO_WSDL
        self.email_fields = email_fields
        self.domain = domain
        self.jwt = jwt
        self.message = message
        self.b_api = {}
        self.b_api = Client(self.bronto_wsdl)
        session_id = self.b_api.service.login(self.api_token)
        session_header = self.b_api.factory.create("sessionHeader")
        session_header.sessionId = session_id
        self.b_api.set_options(soapheaders=session_header)
        self.mapper = dict(displayNameFieldId='display_name',
                           userNameFieldId='username',
                           playerIdFieldId='player_id',
                           customSourceFieldId='reg_source',
                           registeredSiteIdFieldId='Reg_site_id',
                           homeSiteIdFieldId='Reg_site_id',
                           registeredSiteNameFieldId='Reg_site_name',
                           homeSiteNameFieldId='Reg_site_name'
                       )

    def create_field_object(self, type, field_id, data):
        object = self.b_api.factory.create(type)
        object.fieldId = field_id
        object.content = data
        return object

    def register_user_to_bronto(self, user_data):
        """
        Function to add user details with Bront for automated emailing service
        :param user_data:
        :return:
        """
        fields = []
        for key, value in BRONTO_FIELD_REF.items():
            if key == 'birthdateFieldId':
                fields.append(self.create_field_object('contactField',
                                                       value,
                                                       self.format_date_for_bronto(user_data.get('dob'))))
            elif key == 'regDateFieldId':
                fields.append(self.create_field_object('contactField',
                                                       value,
                                                       CHERRIOS_UTILS.get_pst_date_time()))
            else:
                fields.append(self.create_field_object('contactField',
                                                       value,
                                                       user_data.get(self.mapper[key])))

        array_buzztime_subscriber = BUZZTIME_SUBSCRIBERS_LIST_ID
        contact = self.b_api.factory.create('contactObject')
        contact.email = user_data.get('email')
        contact.listIds = array_buzztime_subscriber

        contact.fields = fields
        try:
            self.b_api.service.addOrUpdateContacts([contact])
        except Exception as ex:
            Logger.log.error("Error While Adding User details in Bronto for User %s", user_data.get('email'))

    def format_date_for_bronto(self, date):
        """
        Bronto Expects the date to be in YYYY-MM-DD
        Across the application the format being used is DD-MM-YYYY
        This function will format the date as per the Bronto's requirement
        :param date: string
        :return: date: string
        """
        date = datetime.datetime.strptime(date, '%d-%m-%Y')
        return date.strftime('%Y-%m-%d')

    def send(self):
        """
        The main implementation to send mail
        :return:
        """
        contact = self.b_api.factory.create('contactObject')
        contact.email = self.recipient_email

        contact_result = self.b_api.service.addOrUpdateContacts(contact)

        add_delivery = self.b_api.factory.create('deliveryObject')
        add_delivery.start = datetime.datetime.now()
        add_delivery.messageId = self.message_id
        add_delivery.fromName = self.sender_name
        add_delivery.fromEmail = self.sender_email
        field_object = []

        # For each fieldName and fieldPrice, create
        # a dictionary containing a name, type, and content, and
        # then append that dictionary to the field_object list.

        display_name_field = {}
        recipient_email_field = {}
        username_field = {}
        password_field = {}
        form_name_field = {}
        form_data_field = {}
        domain_field = {}
        jwt_field = {}
        message_field = {}

        display_name_field['name'] = 'displayName'
        display_name_field['content'] = self.recipient_display_name
        display_name_field['type'] = 'html'
        field_object.append(display_name_field)

        recipient_email_field['name'] = 'contact_email'
        recipient_email_field['content'] = self.recipient_email
        recipient_email_field['type'] = 'html'
        field_object.append(recipient_email_field)

        username_field['name'] = 'userName'
        username_field['content'] = self.recipient_username
        username_field['type'] = 'html'
        field_object.append(username_field)

        password_field['name'] = 'forgotPasswordGuid'
        password_field['content'] = self.recipient_password
        password_field['type'] = 'html'
        field_object.append(password_field)

        jwt_field['name'] = 'jwtField'
        jwt_field['content'] = self.jwt
        jwt_field['type'] = 'html'
        field_object.append(jwt_field)

        message_field['name'] = 'messageField'
        message_field['content'] = self.message
        message_field['type'] = 'html'
        field_object.append(message_field)

        domain_field['name'] = 'domainName'
        domain_field['content'] = self.domain
        domain_field['type'] = 'html'
        field_object.append(domain_field)

        if self.email_fields:
            form_name_field['name'] = 'form_name'
            form_name_field['content'] = self.email_fields['form_name']
            form_name_field['type'] = 'html'
            field_object.append(form_name_field)

            form_data_field['name'] = 'entry_data'
            form_data_field['content'] = self.email_fields['form_data']
            form_data_field['type'] = 'html'
            field_object.append(form_data_field)

        add_delivery.fields = field_object

        delivery_recipient_object = self.b_api.factory.create('deliveryRecipientObject')
        delivery_recipient_object.type = 'contact'
        if contact_result.results[0] and hasattr(contact_result.results[0], 'id'):
            delivery_recipient_object.id = contact_result.results[0].id
        add_delivery.recipients = delivery_recipient_object

        delivery = self.add_deliveries(add_delivery)

        if delivery['results'][0]['isError']:
            Logger.log.error(str(delivery['results'][0]['errorString']))
            return False

        return True

    def add_deliveries(self, delivery):
        """
        Adds the delivery and schedules the mail send
        :param delivery:
        :return:
        """
        return self.b_api.service.addDeliveries(delivery)
