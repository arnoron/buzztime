"""
Decorator to verify client IP
"""
from rest_framework.response import Response

from cheerios.settings import DEFAULT_WHITELISTED_IPS


def allow_ip(whitelisted_ip=[]):
    """
    The decorator to allow/deny access based on client IP
    :param func:
    :return:
    """
    def allow_ip_inner(func):
        """

        :param func:
        :return:
        """

        def wrapper(self, request, *args, **kwargs):
            """
            Inner function for the decorator
            :return:
            """

            default_allowed_hosts_string = DEFAULT_WHITELISTED_IPS
            default_allowed_hosts = default_allowed_hosts_string.split(',')

            allowed_hosts = default_allowed_hosts + whitelisted_ip

            forwarded_ips = request.META.get('HTTP_X_FORWARDED_FOR')

            if not forwarded_ips:
                real_client_ip = request.META.get('REMOTE_ADDR')
            else:
                real_client_ip = forwarded_ips.split(',')[0]

            if real_client_ip not in allowed_hosts:
                return Response(status=403)
            return func(self, request, *args, **kwargs)
        return wrapper
    return allow_ip_inner
