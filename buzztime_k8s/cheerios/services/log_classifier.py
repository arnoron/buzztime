"""
This Class will decide the log_file.
"""
import logging


class Logger:
    log = logging.getLogger("buzztime")
    prefix_dict = {
        'triviaweb': 'triviaweb',
        'players': 'players',
        'foodmenu': 'foodmenu',
        'feeds': 'tablet',
        'analytics': 'tablet',
        'manifest': 'tablet',
        'mysterymachine':'mysterymachine',
    }

    @staticmethod
    def assign_logfile(path):
        log_file = path.split('/')[1]
        Logger.log = logging.getLogger(
            Logger.prefix_dict.get(log_file, 'buzztime'))
