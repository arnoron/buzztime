"""
Decorator to validate captcha
"""
import requests

from functools import wraps

from cheerios.constant.constant import TRUE_VALUES
from cheerios.settings import CAPTCHA_SECRET_KEY, RECAPTCHA_VERIFY_URL


def validate_captcha():
    """
    The decorator to validate the captcha token
    sent from the client
    :param func:
    :return:
    """
    def captcha_inner(func):
        """
        Inner function for the decorator
        :return:
        """
        @wraps(func)
        def func_wrapper(self, request,*args):
            """

            :param request:
            :return:
            """
            request_body = request.data
            mobile = request_body.get('mobile')

            if mobile in TRUE_VALUES:
                return func(self, request, *args)

            captcha_token = request_body.get('captcha-token')

            data = dict(secret=CAPTCHA_SECRET_KEY,
                        response=captcha_token)

            response = requests.post(RECAPTCHA_VERIFY_URL, data)

            response_content = response.json()

            if not response_content.get('success'):
                error_message = response_content.get('error-codes')[0]
                raise ReCaptchaError(error_message)
            return func(self, request, *args)

        return func_wrapper

    return captcha_inner


class ReCaptchaError(Exception):
    """

    """
    def __init__(self, message):
        """

        :param args:
        """
        super(Exception, self).__init__(message)
