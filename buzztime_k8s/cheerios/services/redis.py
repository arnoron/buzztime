"""
Key storing and retrieval from Redis
"""
# Python uses a reference counter mechanism to deal with objects, so at the end of the blocks,
# the redis object will be automatically destroyed and the connection closed

from django.conf import settings
from cheerios.interface.singleton import Singleton
from cheerios.local_settings import REDISDB
from cheerios.settings import sentinel, settings_file
from cheerios.triviaweb.constants import SESSION_EXPIRY


class RedisSession(metaclass=Singleton):
    """
    Redis session back-end for Django
    """

    def __init__(self):
        super(RedisSession, self)
        if(settings_file == 'cheerios.settings'):
            self.redis_master = sentinel.master_for('mymaster', socket_timeout=2, retry_on_timeout=True)
            self.redis_slave = sentinel.slave_for('mymaster', socket_timeout=2, retry_on_timeout=True)
        else:
            self.redis_master = self.redis_slave = REDISDB

    def save_data(self, key, value=True, ttl=SESSION_EXPIRY, persist=False):
        """
        Save the session to the redis server, we need to use
        everything from within the object. We already have the redis setup
        and we would use that object to connect to our redis.
        """
        self.redis_master.set(key, value)
        self.redis_master.expire(key, ttl)
        if persist:
            self.redis_master.persist(key)
        return

    def get_key(self, key):
        """
        We need to get the session id and if the key doesnt exist in the
        sesssion then the object would return a None
        """
        return self.redis_slave.get(key)

    def get_value(self, key=None):
        """
        Get value of the key from the redis object
        """
        return self.redis_slave.get(key)

    def remove_keys(self, key):
        """

        :return:
        """
        keys = self.redis_master.keys(key + "*")
        for key in keys:

            self.redis_master.delete(key)

    def get_ttl(self, key):
        return self.redis_slave.ttl(key)

    def ping_master(self):
        return self.redis_master.ping()

    def set_object(self, name, key, value):
        """
        Sets a nested dictionary in redis
        :param name:
        :param key:
        :param value:
        :return:
        """
        return self.redis_master.hset(name, key, value)
