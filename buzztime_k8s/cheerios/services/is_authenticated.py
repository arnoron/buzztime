from functools import wraps

from redis.sentinel import SlaveNotFoundError, MasterNotFoundError

from rest_framework.response import Response

from cheerios.serializers import AuthorizationSerializer
from cheerios.constant.constant import PLAYMAKER_AUTH_APIS, TRIVIAWEB_APIS
from cheerios.services.redis import RedisSession
from cheerios.utils import CheeriosUtils

CHERRIOS_UTILS = CheeriosUtils()

from cheerios.services.log_classifier import Logger


def is_triviaweb_authenticated(redis_obj, auth_token, request_method):
    """

    :return:
    """
    if not redis_obj.get_key(key=auth_token) and request_method != 'OPTIONS':
        return False
    else:
        return True

def is_authenticated():
    """
    The decorator to authenticate the API
    :param func:
    :return:
    """
    def is_auth_inner(func):
        """
        Inner function for the decorator
        :return:
        """
        @wraps(func)
        def func_wrapper(self, request, *args):
            """

            :param request:
            :return:
            """
            request_data = None
            mobile = False
            redis_obj = RedisSession()

            if request.path in TRIVIAWEB_APIS:
                if is_triviaweb_authenticated(redis_obj, request.META.get('HTTP_AUTHORIZATION'), request.method):
                    return func(self, request, *args)
                else:
                    return Response("Auth Token Error", 403)

            if request.path in PLAYMAKER_AUTH_APIS:
                mobile = True

            if not request.method:
                return Response("Incorrect Method Passed")

            if request.method in ['POST', 'PUT']:
                request_data = request.POST
                if not request_data:
                    request_data = request.data
            elif request.method == 'GET':
                request_data = request.GET
            elif request.method == 'DELETE':
                request_data = request.data

            player_id = request_data.get('player_id')

            serializer = AuthorizationSerializer(data=dict(player_id=player_id))

            if not serializer.is_valid():
                Logger.log.error(str(serializer.errors))
                return Response(serializer.errors, 400)

            try:
                if CHERRIOS_UTILS.does_auth_token_exist_for_user(player_id, mobile):
                    token_saved = CHERRIOS_UTILS.get_auth_token_for_user(player_id, mobile)
                    token_passed = request.META.get('HTTP_AUTHORIZATION')
                    if token_saved.decode("utf-8") != token_passed:
                        Logger.log.error("Auth Token Error")
                        return Response("Auth Token Error", 403)
                else:
                    Logger.log.error("Auth Token Error")
                    return Response("Auth Token Error", 403)
            except (SlaveNotFoundError, MasterNotFoundError, Exception) as ex:
                Logger.log.error(str(ex))
                return Response("Internal Server Error. Please try after some time", 500)

            return func(self, request, *args)

        return func_wrapper

    return is_auth_inner
