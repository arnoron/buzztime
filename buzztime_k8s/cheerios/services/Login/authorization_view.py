"""
Module to perform authorization for login
"""
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.decorators import close_connection
from cheerios.settings import MSSQLUSERDB
from cheerios.triviaweb.utils.utils  import UtilFunction
from cheerios.triviaweb.constants import STORE_PROC
from cheerios.services.log_classifier import Logger
UTIL_FUNCTION = UtilFunction()


class AuthorizationViewSet(MSSQLApiView):
    """
    Defining the authorization view set as a class instead of a method
    to handle various operations
    """
    def __init__(self, playerID):
        super().__init__()
        self.player_id = playerID

    @close_connection
    def check_host(self):
        """
        This function call store procedure.
        This function accepts playerID value.
        Store procedure return playerID , statusID and note value.
        """
        message = "Trivia host API called"
        Logger.log.info(message)
        data = "@PlayerID={}".format(self.player_id)
        cursor = self.get_cursor(MSSQLUSERDB)
        store_procedure = STORE_PROC["get_trivia_host"]
        self.call_storeprocedure(store_procedure, cursor, data)
        result = self.fetchall_from_cursor(cursor)
        return result
