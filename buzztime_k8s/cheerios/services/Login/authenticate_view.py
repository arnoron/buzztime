"""
Module to authenticate the login of the player
"""
from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.players.decorators import close_connection
from cheerios.settings import MSSQLUSERDB
from cheerios.triviaweb.utils.utils  import UtilFunction
from cheerios.triviaweb.constants import STORE_PROC
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger
UTIL_FUNCTION = UtilFunction()
CHEERIOS_UTILS = CheeriosUtils()


class AuthenticateViewSet(MSSQLApiView):
    """
    Defining the authenticate view set as a class instead of a method
    to handle various operations
    """
    def __init__(self, params):
        super().__init__()
        self.params = params

    @close_connection
    def authenticate_api(self):
        """
        This function call store procedure.
        This function accepts username and namespaceId value.
        username and namespaceId should store in parameters.
        Store procedure return playerID , hash password and salt value.
        """
        parameters = "@Username='{0}', @NamespaceID={1}".format(self.params[0],
                                                              self.params[1])
        message = "Authentication API called"
        Logger.log.info(message)
        cursor = self.get_cursor(MSSQLUSERDB)
        store_procedure = STORE_PROC["get_player_auth"]
        self.call_storeprocedure(store_procedure, cursor, parameters)
        result = self.fetchall_from_cursor(cursor)
        return result

    @staticmethod
    def match_password(password, hashpassword, salt):
        """
        Convert password to hash password.
        Compare generated password and hash password.
        return true if both password match else return false.
        """
        result_pass = password + salt if salt else password
        result_pass = CHEERIOS_UTILS.create_md5_hash(result_pass)

        if result_pass == hashpassword:

            return True
        return False
