"""
Serializer for authentication and common functions.
"""
from rest_framework import serializers

from cheerios.serializers import SerializerValidator


class AuthenticationSerializer(SerializerValidator):
    """
    Serializer for authentication class
    """
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=True)
    player_id = serializers.CharField(required=False)


class AuthorizationSerializer(SerializerValidator):
    """
    Serializer for authentication class
    """
    PlayerID = serializers.CharField(required=True)
