"""
Common class of login.
"""
from pymssql import OperationalError

from django.contrib.sessions.backends.db import SessionStore
from rest_framework.response import Response
from sqlalchemy import exc

from cheerios.interface.msssql_connection import MSSQLApiView
from cheerios.services.Login.authenticate_view import AuthenticateViewSet
from cheerios.services.Login.serializers import AuthenticationSerializer
from cheerios.utils import CheeriosUtils
from cheerios.services.log_classifier import Logger

CHERRIOS_UTILS = CheeriosUtils()

class LoginViewInterface(MSSQLApiView):
    """
    Login Interface.
    Contains the common code for the login mechanism
    """

    def login_user(self, request):
        """
        Gets the login params
        Calls the authentication and authrization viewset
        :param request:
        :return:
        """
        message = "Login API called"
        Logger.log.info(message)
        try:
            password = request.data.get('password')
            username = request.data.get('username')
            # If the value sent is an email then put it as 6 else as 1
            namespace_id = 6 if CHERRIOS_UTILS.validate_email(username) else 1
            authenticated_player = self.authenticate_user\
                (username, password, namespace_id)


            if authenticated_player:
                authorisation_result = self.authorize_user(authenticated_player)
            else:
                return Response(False, status=200)

            if authorisation_result or authorisation_result is 'not required':
                response = self.get_response(authenticated_player, username)
            else:
                return Response(False, status=200)

            return response

        except OperationalError as inst:
            return CHERRIOS_UTILS.operation_error(inst)
        except exc.DBAPIError as excep:
            return CHERRIOS_UTILS.dbapi_error(excep)
        except ValueError as inst:
            return CHERRIOS_UTILS.value_error(inst)
        except Exception as inst:
            return CHERRIOS_UTILS.exception(inst)
        finally:
            self.close_cursor()

    def authenticate_user(self, username, password, namespace_id, version="1.0", mobile_auth=False):

        """
                Authenticates the user
                :return:
                """

        data = {"username": username, "password": password}

        auth_serializer = AuthenticationSerializer(data=data)

        CHERRIOS_UTILS.validate_serializer(auth_serializer)
        parameters = [username, namespace_id]
        auth_obj = AuthenticateViewSet(parameters)
        result = auth_obj.authenticate_api()

        if not result:
            return False
        result = result[0]

        if mobile_auth and version == "1.1":
            password = CHERRIOS_UTILS.create_md5_hash("{0}".format(password))

        if auth_obj.match_password(password, result.get('Password'), result.get('Salt')):
            return result
        else:
            return False

    def authorize_user(self, result):
        """
        Authorizes the user
        :return:
        """
        # Check serializer to check whether parameters are defined
        return 'not required'

    def get_response(self, result, username):
        """
        Gets the response to return
        :return:
        """
        player = {'player_id': result.get('PlayerID')}

        response = Response(player, status=200)
        response['Access-Control-Expose-Headers'] = 'Session'
        response["Session"] = self.get_session_key(username)
        return response

    def get_session_key(self, username):
        """
        Get the random session key to return as header
        :return:
        """
        session = SessionStore()
        session['user'] = username
        session.create()
        session.set_expiry(60 * 60)
        key = session.session_key
        return key
