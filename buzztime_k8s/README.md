Buzztime
=============

Buzztime - A project to create ultimate bar and restaurant entertainment experience.

##Steps to Setup Local Environment.

###To clone the repository 
1. git clone git@proddevelopment.git.beanstalkapp.com:/proddevelopment/py-platform-services.git

###To setup local environment dependencies
1. Install python3.
   sudo apt-get install python3
2. See if it works and show version as 3.x.x
   python3
3. Install python3-dev
   sudo apt-get install python3-dev
4. Install python-virtualenv
   sudo apt-get install python-virtualenv
6. Create virtual environment.
   virtualenv pyvenv
7. Set its path
   virtualenv -p /usr/bin/python3.4 pyvenv
8. Activate it.
   source pyvenv/bin/activate
9. Create env variable file give name as .env specified all env values. run below command to export it.
   export $(cat .env)
10. Install dependencies:
   pip3 install -r docker-build/configs/requirements.txt

###To setup for Test cases
1. To run test cases we need to install fabric
   sudo apt-get install fabric or pip3 install fabric3
2. Command to test pylint errors on terminal
   fab lint_py
3. Also we can use below pylint command to create pylint report
   pylint --rcfile=pylint.cfg $(find . -maxdepth 1 -name "*.py" -print) cheerios/ > out/pylint.log

###To setup Mongodb Databases
1. Add dump mongodb to ur local machine.
   mongorestore -db <path of downloaded file DevMongoDump_160603/>
2. Log in to mongo and check if database restored.
   mongo 127.0.0.1:27017
   show dbs 
   use <database name>
   show collections

###To setup MS SQL Server
1. Install pymssql
   apt-get --assume-yes update
   apt-get --assume-yes install freetds-dev freetds-bin
   apt-get --assume-yes install python-dev python-pip
   pip3 install pymssql

   For reference Mac: 
   http://stackoverflow.com/questions/37771434/mac-pip-install-pymssql-error
   For Ubuntu:
   https://msdn.microsoft.com/library/mt694094.aspx#Ubuntu-Linux

###To run the server
1. python3 manage.py runserver

###To run test cases
1. Running all test cases
   fab test
2. Run a particular test file
   fab test:<folder.module>
   or
   python manage.py test <folder.module.filename.Classname.specific function name>

###For code Coverage
1. To generate coverage report
   fab coverage test
It will generate coverage folder in /out

###For Performance test
1. Install apache2-utils in virtual environment
   sudo apt-get install apache2-utils // For Ubuntu
   yum install httpd-tools // For Centos
2. Vaidate if data_to_post for that specific post event is present
3. Run following command
   ab -n2000 -c10 -pdocker-build/configs/data_to_post.txt -Tapplication/json http://127.0.0.1:8000/analytics/event/

4. Test performance using Jmeter. Install Jmeter
   sudo apt-get install jmeter  // For Ubuntu
   sudo yum install jmeter  // For centOS
   yum install jmeter // For Mac
   Jmeter Setup for Mac
   https://loadfocus.com/blog/2014/06/24/run-apache-jmeter-on-mac/

###Gunicorn setup for local
1. Install Gunicorn:
   pip install gunicorn
2. Run command for gunicorn
   gunicorn cheerios.wsgi:application --bind 127.0.0.1:8000 --workers=6 --threads=4 --log-file /var/log/gunicorn.log --log-level error
   or
   gunicorn cheerios.wsgi:application --bind 127.0.0.1:8000

###Run logentries
1. sh path/to/logentries_install.sh ${LOG_ENTRIES_FLAG} ${LOGENTRIES_LICENSE_KEY} ${APPNAME}
   example 'sudo ./logentries_install.sh true <Key> Cheerios'

   Required environment variables we need to declare:
   LOGENTRIES_LICENSE_KEY = <KEY>
   LOG_ENTRIES_FLAG = <True/False> Depending on environment its value will varry.
                      Its value will decide whether to intall logentries or not.
   APPNAME = <Application name e.g., 'Buzztime'>

###Install Redis
1. Install Redis in Mac
   brew install redis

   For Linux:
   sudo apt-get install redis-server

###Docker Setup for local machine
NOTE : The below steps will work only if docker setup is done on machine.

1. Pull latest code from repo
2. Build docker
   docker-compose -f local-compose.yml build
3. This steps will start docker and our local will start on ip 0.0.0.0
   docker-compose -f local-compose.yml up -d
4. It will list all containers being used on local.
   docker ps
5. To select any container which were displayed using step 4 use this step.
   docker exec -it <ContainerID> /bin/bash
6. For scaling:
    docker-compose -f local-compose.yml scale wsgi=<number of instances we want e.g,3>
    To test hit feeds/hostname/ It will return all instance ip address one by one.
7. Stop Docker
   docker-compose -f local-compose.yml down

To check mongo db
1. docker exec -it <MongoContainerID> /bin/bash
2. mongo
3. show dbs
4. use <dbname>
5. show tables
6. db.<tablename>.find.pretty()
