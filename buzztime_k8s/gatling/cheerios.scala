package cheerios

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.TimeZone
import java.util.UUID
import java.util.Base64
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLConnection;

object JWT {
  def getJWT() = {
    val time = FTime.getFtime()
    // Expected header val: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9
    val jwtHeader = """{"alg":"HS256","typ":"JWT"}"""
    val encodedJWTHeader = Base64.getUrlEncoder()
      .withoutPadding()
      .encodeToString(jwtHeader.getBytes())

    val payload = s"""{"REQUEST_METHOD":"POST","PATH_INFO":"/analytics/event/","time":"$time"}"""

    val encodedPayload = Base64.getUrlEncoder()
      .withoutPadding()
      .encodeToString(payload.getBytes())

    val hMACSHA256 = Mac.getInstance("HmacSHA256")
    val secret = new SecretKeySpec("secret".getBytes("UTF-8"), "HmacSHA256")
    hMACSHA256.init(secret)

    val signature = s"$encodedJWTHeader.$encodedPayload"

    // May need to .replace("+", "-")
    val encodedSignature = Base64.getUrlEncoder()
      .withoutPadding()
      .encodeToString(hMACSHA256.doFinal(signature.getBytes("ASCII")))

    val jwt = s"$encodedJWTHeader.$encodedPayload.$encodedSignature"
    jwt
  }
  def getCustomeJWT(para: String, url: String, secretKey: String) = {
  val time = FTime.getFtime()
    // Expected header val: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9
    val jwtHeader = """{"alg":"HS256","typ":"JWT"}"""
    val encodedJWTHeader = Base64.getUrlEncoder()
      .withoutPadding()
      .encodeToString(jwtHeader.getBytes())

    val payload = s"""{"REQUEST_METHOD": "$para","PATH_INFO": "$url","time": "$time"}"""

    val encodedPayload = Base64.getUrlEncoder()
      .withoutPadding()
      .encodeToString(payload.getBytes())

    val hMACSHA256 = Mac.getInstance("HmacSHA256")
    val secret = new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA256")
    hMACSHA256.init(secret)

    val signature = s"$encodedJWTHeader.$encodedPayload"

    // May need to .replace("+", "-")
    val encodedSignature = Base64.getUrlEncoder()
      .withoutPadding()
      .encodeToString(hMACSHA256.doFinal(signature.getBytes("ASCII")))

    val jwt = s"$encodedJWTHeader.$encodedPayload.$encodedSignature"
    jwt
  }
}

object FTime {
  def getFtime() = {
    val time = Calendar.getInstance.getTime

    val formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
    val ftime = formatter.format(time)
    ftime
  }
}

/*
For the 7 day period 7/4-7/10:
Most records per second: 529
Most records per minute: 11051
Most records per hour: 485535
*/
class CheeriosSimulation extends Simulation {
  // val baseURL = System.getenv("baseUrl")
  val baseURL = "https://ntnservices.stg.buzztime.com/"
  //val baseURL = "http://localhost:8000"

  val httpProtocol = http
    .baseURL(baseURL)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
    .acceptLanguageHeader("en-US,en;q=0.8")
    .acceptEncodingHeader("gzip, deflate")
    .shareConnections

  // XXX values before sportradar rN = 5, rS = 20, rE = rS * 5
  val repeatNum = 5
  val rampStart = 100
  val rampEnd = 100
  val duration = 1 // ramp duration, minutes
  val rampuser = 6000
  val crispuserwithetag = 5
  val playersusers = 4000
  val crispuser = 10
  def serial_feeder() = csv("serial_nums.csv").circular
  val eventuser = 1000
//  (menuid, siteid)
  var SiteMenuIDs = Array.ofDim[Int](560, 2)
  SiteMenuIDs = Array(Array(1000, 1458), Array(1001, 1453))
/*
  Array(1001, 791),Array(1001, 960),
  Array(1001, 874), Array(1001, 904), Array(1001, 5), Array(1001, 6), Array(1001, 792),
  Array(1001, 8), Array(1001, 176), Array(1001, 10), Array(1001, 7), Array(1001, 9),
  Array(1001, 772), Array(1001, 793), Array(1001, 12), Array(1001, 15), Array(1001, 14),
  Array(1001, 221), Array(1001, 22), Array(1001, 860), Array(1001, 561), Array(1001, 25),
  Array(1001, 24), Array(1001, 786), Array(1001, 28), Array(1001, 776), Array(1001, 229),
  Array(1001, 178), Array(1001, 773), Array(1001, 32), Array(1001, 323), Array(1001, 31),
  Array(1001, 760), Array(1001, 795), Array(1001, 35), Array(1000, 332), Array(1001, 1327),
  Array(1001, 1295), Array(1001, 41), Array(1001, 1167), Array(1001, 36), Array(1001, 39),
  Array(1001, 40), Array(1001, 337), Array(1001, 350), Array(1001, 906), Array(1001, 831),
  Array(1001, 44), Array(1001, 1334), Array(1001, 796), Array(1001, 46), Array(1001, 48),
  Array(1001, 834), Array(1001, 50), Array(1001, 53), Array(1001, 52), Array(1001, 1037),
  Array(1001, 55), Array(1001, 766), Array(1001, 876), Array(1001, 61), Array(1001, 94),
  Array(1001, 855), Array(1001, 54), Array(1001, 51), Array(1001, 58), Array(1001, 63),
  Array(1001, 49), Array(1001, 1331), Array(1001, 56), Array(1001, 60), Array(1001, 797),
  Array(1001, 69), Array(1000, 68), Array(1001, 67), Array(1001, 1348), Array(1001, 384),
  Array(1001, 395), Array(1001, 71), Array(1001, 74), Array(1001, 82), Array(1001, 75),
  Array(1001, 76), Array(1001, 79), Array(1001, 83), Array(1001, 1287), Array(1001, 403),
  Array(1001, 84), Array(1001, 1172), Array(1001, 907), Array(1001, 1184),Array(1001, 86),
  Array(1001, 73), Array(1001, 798), Array(1001, 431),Array(1001, 90),Array(1001, 89),
  Array(1001, 1292), Array(1001, 1296), Array(1001, 101), Array(1001, 95), Array(1001, 443),
  Array(1001, 1293), Array(1001, 105), Array(1001, 100), Array(1001, 103), Array(1001, 1185),
  Array(1001, 93), Array(1001, 97), Array(1001, 91), Array(1001, 107), Array(1001, 478),
  Array(1001, 110), Array(1001, 96), Array(1001, 111), Array(1001, 1312), Array(1001, 1299),
  Array(1001, 492), Array(1001, 877), Array(1001, 1297), Array(1001, 115), Array(1001, 116),
  Array(1001, 112), Array(1001, 121), Array(1001, 114), Array(1001, 118), Array(1001, 1150),
  Array(1001, 113), Array(1001, 520), Array(1001, 799), Array(1001, 500), Array(1001, 120),
  Array(1001, 123), Array(1001, 126), Array(1001, 124), Array(1001, 127), Array(1001, 125),
  Array(1001, 128), Array(1001, 129), Array(1001, 1148), Array(1001, 1189), Array(1001, 1279),
  Array(1001, 1280), Array(1001, 134), Array(1001, 136), Array(1001, 878), Array(1001, 553),
  Array(1001, 1035), Array(1001, 131), Array(1001, 138), Array(1001, 122), Array(1001, 157),
  Array(1001, 155), Array(1001, 142), Array(1001, 133), Array(1001, 140), Array(1001, 145),
  Array(1001, 1174), Array(1001, 725), Array(1001, 146), Array(1001, 147), Array(1001, 139),
  Array(1001, 159), Array(1001, 137), Array(1001, 165), Array(1001, 148), Array(1001, 144),
  Array(1001, 567), Array(1001, 595), Array(1001, 588), Array(1001, 1186), Array(1001, 163),
  Array(1001, 149), Array(1001, 1281), Array(1001, 1191), Array(1001, 161), Array(1001, 162),
  Array(1001, 800), Array(1001, 160), Array(1001, 612), Array(1001, 895), Array(1001, 167),
  Array(1001, 182), Array(1001, 175), Array(1001, 628), Array(1001, 184), Array(1001, 1340),
  Array(1001, 1303), Array(1001, 174), Array(1001, 183), Array(1001, 166), Array(1001, 171),
  Array(1001, 629), Array(1001, 185), Array(1001, 193), Array(1001, 181), Array(1001, 191),
  Array(1001, 1173), Array(1001, 152), Array(1001, 151), Array(1001, 369), Array(1001, 180),
  Array(1001, 186), Array(1001, 646), Array(1001, 194), Array(1001, 198), Array(1001, 173),
  Array(1001, 196), Array(1001, 207), Array(1001, 879), Array(1001, 1329), Array(1001, 202),
  Array(1001, 199), Array(1001, 1170), Array(1001, 192), Array(1001, 190), Array(1001, 169),
  Array(1001, 204), Array(1001, 187), Array(1001, 179), Array(1001, 197), Array(1001, 200),
  Array(1001, 189), Array(1001, 201), Array(1001, 213), Array(1001, 215), Array(1001, 1337),
  Array(1001, 647), Array(1001, 211), Array(1001, 652), Array(1001, 210), Array(1001, 1328),
  Array(1001, 1193), Array(1001, 1313), Array(1001, 639), Array(1001, 212), Array(1001, 223),
  Array(1001, 205), Array(1001, 483), Array(1001, 555), Array(1001, 495), Array(1001, 648),
  Array(1001, 1305), Array(1001, 1325), Array(1001, 188), Array(1001, 908), Array(1001, 208),
  Array(1001, 209), Array(1001, 218), Array(1001, 214), Array(1001, 224), Array(1001, 217),
  Array(1001, 225), Array(1001, 226), Array(1001, 227), Array(1001, 801), Array(1001, 216),
  Array(1001, 231), Array(1001, 230), Array(1001, 1339), Array(1001, 1298), Array(1001, 667),
  Array(1001, 228), Array(1001, 909), Array(1001, 802), Array(1001, 1294), Array(1001, 1304),
  Array(1001, 236), Array(1001, 232), Array(1001, 676), Array(1001, 1307), Array(1001, 711),
  Array(1001, 1190), Array(1001, 238), Array(1001, 234), Array(1001, 233), Array(1001, 1453),
  Array(1001, 1179), Array(1001, 1183), Array(1001, 708), Array(1001, 243), Array(1001, 245),
  Array(1001, 251), Array(1001, 235), Array(1001, 244), Array(1001, 246), Array(1001, 248),
  Array(1001, 914), Array(1001, 803), Array(1001, 247), Array(1001, 242), Array(1001, 252),
  Array(1001, 915), Array(1001, 240), Array(1001, 253), Array(1001, 256), Array(1001, 255),
  Array(1001, 250), Array(1001, 739), Array(1001, 261), Array(1001, 1434), Array(1001, 260),
  Array(1001, 257), Array(1001, 258), Array(1001, 916), Array(1001, 748),
  Array(1001, 759), Array(1001, 750), Array(1001, 1073), Array(1001, 1242), Array(1001, 810),
  Array(1001, 264), Array(1001, 784), Array(1001, 779), Array(1001, 262), Array(1001, 1326),
  Array(1001, 993), Array(1001, 775), Array(1001, 790), Array(1001, 758), Array(1001, 761),
  Array(1001, 1158), Array(1001, 807), Array(1001, 778), Array(1001, 780), Array(1001, 812),
  Array(1001, 804), Array(1001, 1322), Array(1001, 809), Array(1001, 754),
    Array(1001, 805), Array(1001, 808), Array(1001, 782), Array(1001, 764), Array(1001, 762),
    Array(1001, 787), Array(1001, 1195), Array(1001, 781), Array(1001, 947), Array(1001, 785),
    Array(1001, 763), Array(1001, 765), Array(1001, 789), Array(1001, 788), Array(1001, 768),
    Array(1001, 1097), Array(1001, 817), Array(1001, 1316), Array(1001, 816), Array(1001, 822),
    Array(1001, 818), Array(1001, 828), Array(1001, 819), Array(1000, 823), Array(1001, 830),
    Array(1001, 1188), Array(1001, 821), Array(1001, 827), Array(1001, 824), Array(1001, 882),
    Array(1001, 835), Array(1001, 1101), Array(1001, 1243), Array(1001, 848), Array(1001, 845),
    Array(1001, 847), Array(1001, 851), Array(1001, 846), Array(1001, 843), Array(1001, 850),
    Array(1001, 857), Array(1001, 849), Array(1001, 856), Array(1001, 853), Array(1001, 861),
    Array(1001, 1007),
Array(1001, 864),
Array(1001, 858),
Array(1001, 862),
Array(1001, 852),
Array(1001, 870),
Array(1001, 866),
Array(1001, 964),
Array(1001, 888),
Array(1001, 871),
Array(1001, 868),
Array(1001, 883),
Array(1001, 970),
Array(1001, 265),
Array(1001, 959),
Array(1001, 1192),
Array(1001, 872),
Array(1001, 885),
Array(1001, 867),
Array(1001, 891),
Array(1001, 884),
Array(1001, 889),
Array(1001, 887),
Array(1001, 886),
Array(1001, 869),
Array(1001, 873),
Array(1001, 898),
Array(1001, 1187),
Array(1001, 892),
Array(1001, 890),
Array(1001, 894),
Array(1001, 899),
Array(1001, 901),
Array(1001, 900),
Array(1001, 893),
Array(1001, 903),
Array(1001, 1064),
Array(1001, 1006),
Array(1001, 911),
Array(1001, 1351),
Array(1001, 1349),
Array(1001, 1074),
Array(1001, 1244),
Array(1001, 902),
Array(1001, 910),
Array(1001, 912),
Array(1001, 841),
Array(1001, 1346),
Array(1001, 1336),
Array(1001, 1333),
Array(1001, 1330),
Array(1001, 917),
Array(1001, 918),
Array(1001, 1246),
Array(1001, 1332),
Array(1001, 1194),
Array(1001, 922),
Array(1001, 1108),
Array(1001, 263),
Array(1001, 927),
Array(1001, 815),
Array(1001, 925),
Array(1001, 924),
Array(1001, 1003),
Array(1001, 1109),
Array(1001, 1014),
Array(1001, 926),
Array(1001, 1239),
Array(1001, 1045),
Array(1001, 1052),
Array(1001, 1106),
Array(1001, 1082),
Array(1001, 1067),
Array(1001, 1065),
Array(1001, 1021),
Array(1001, 998),
Array(1001, 988),
Array(1001, 1056),
Array(1001, 1017),
Array(1001, 1059),
Array(1001, 1113),
Array(1001, 1115),
Array(1001, 1117),
Array(1001, 1004),
Array(1001, 1110),
Array(1001, 1126),
Array(1001, 1044),
Array(1001, 1128),
Array(1001, 1245),
Array(1001, 1127),
Array(1001, 1338),
Array(1001, 1138),
Array(1001, 1136),
Array(1001, 1130),
Array(1001, 1132),
Array(1001, 1135),
Array(1001, 1133),
Array(1001, 1134),
Array(1001, 1197),
Array(1001, 1165),
Array(1001, 1139),
Array(1001, 1198),
Array(1001, 1166),
Array(1001, 1159),
Array(1001, 1168),
Array(1001, 955),
Array(1001, 990),
Array(1001, 943),
Array(1001, 1144),
Array(1001, 1180),
Array(1001, 1162),
Array(1001, 1169),
Array(1001, 1306),
Array(1001, 1335),
Array(1001, 1182),
Array(1001, 1161),
Array(1001, 1177),
Array(1001, 1176),
Array(1001, 1181),
Array(1001, 1178),
Array(1001, 1175),
Array(1001, 1205),
Array(1001, 1350),
Array(1001, 1216),
Array(1001, 1311),
Array(1001, 1227),
Array(1001, 1029),
Array(1001, 1235),
Array(1001, 1234),
Array(1001, 1231),
Array(1001, 1233),
Array(1001, 1229),
Array(1001, 1230),
Array(1001, 1264),
Array(1001, 1262),
Array(1001, 1274),
Array(1001, 1273),
Array(1001, 1263),
Array(1001, 1295),
Array(1001, 1222),
Array(1001, 1310),
Array(1001, 1309),
Array(1001, 1399),
Array(1001, 1352),
Array(1001, 1366),
Array(1001, 1353),
Array(1001, 1452),
Array(1001, 1402),
Array(1001, 1375),
Array(1001, 1423),
Array(1001, 1388),
Array(1001, 1401),
Array(1001, 1397),
Array(1001, 1436),
Array(1001, 1410),
Array(1001, 1416),
Array(1001, 1403),
Array(1001, 1419),
Array(1001, 1421),
Array(1001, 864),
Array(1001, 1430),
Array(1001, 1449),
Array(1001, 1448),
Array(1001, 1441),
Array(1001, 1437),
Array(1001, 1444))
*/
  //var ncr_with_Etag = exec()
  var ncr_with_Etag_with_promise = exec()
  var ncr = exec()
  var schedule = exec()
  for( SiteMenuID <- SiteMenuIDs ){
      var url = baseURL +"/foodmenu/ncr/?menuid=" + SiteMenuID(0) +"&siteid=" + SiteMenuID(1) +"&promisetime=2016-06-16T10:35:00"
      val etag = ""
      /*var obj = new URL(url)
      var conn = obj.openConnection()
      conn.setRequestProperty("authorization", "YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=")
      conn.setRequestProperty("X-Api-CompanyCode", "BWWI001")
      var header = conn.getHeaderFields()
      var etagList = header.get("ETag")
      var etag = etagList.get(0);
      ncr_with_Etag = ncr_with_Etag.exec(
      http("foodmenu.NCRWithEtag")
      .get("/foodmenu/ncr/")
      .queryParam("menuid", SiteMenuID(0))
      .queryParam("siteid", SiteMenuID(1))
      .header("If-None-Match", etag)
      .header("authorization", "YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=")
      .header("X-Api-CompanyCode", "BWWI001")
      .check(status.is(304)))
*/
      ncr_with_Etag_with_promise = ncr_with_Etag_with_promise.exec(
      http("foodmenu.NCRWithEtagWithPromise")
      .get("/foodmenu/ncr/")
      .queryParam("menuid", SiteMenuID(0))
      .queryParam("siteid", SiteMenuID(1))
      .queryParam("promisetime", "2016-06-16T10:35:00")
      .header("If-None-Match", etag)
      .header("authorization", "YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=")
      .header("X-Api-CompanyCode", "BWWI001")
      .check(status.is(304)))
      Thread sleep 2000
   }
  //var ncrwithEtagScn = scenario("CheeriosSimulation.foodmenu.NCRWithEtag").exec(ncr_with_Etag)
  var ncrwithEtagScnWithPromise = scenario("CheeriosSimulation.foodmenu.NCRWithEtagWithPromise").exec(ncr_with_Etag_with_promise)

  schedule = schedule.exec(
    http("foodmenu.Schedule")
    .get("/players/schedule/weekly")
    .check(status.is(200)))
    Thread sleep 2000

  var schedule_run = scenario("CheeriosSimulation.players.schedule").exec(schedule)

  for( SiteMenuID <- SiteMenuIDs ){
      ncr = ncr.exec(
      http("NCR")
      .get("/foodmenu/ncr/")
      .queryParam("menuid", SiteMenuID(0))
      .queryParam("siteid", SiteMenuID(1))
      .header("authorization", "YnV6enRpbWVTZXJ2aWNlVXNlcjppP3IvKUokVDE=")
      .header("X-Api-CompanyCode", "BWWI001")
      .check(status.is(200)))
   }

    val event = exec(http("event")
      .post("/analytics/event/")
      .body(StringBody(session =>
          s"""{ "name": "event",
          "source": "Tablet",
          "time": "${FTime.getFtime()}",
          "keyValuePairs": {
            "ac": "Appearance of view",
            "sid": "37216",
            "sc": "Premium Arcade",
            "sn": "${serial_feeder()}",
            "s": "${java.util.UUID.randomUUID()}",
            "et": "Navigation",
            "md": "gamelobby.activities.GameBrowseActivity.appPanel_onStart" },
          "jwt": "${JWT.getJWT()}"}""")).asJSON
            .check(status.is(200))
            )
  //val eventScn = scenario("CheeriosSimulation.event").exec(event)

  val ping = repeat(repeatNum) {
    exec(http("ping")
      .get("/analytics/ping/")
      .check(status.is(200)))
  }

  val crisp = exec(http("crisp")
      .post("/feeds/crisp/")
      .body(StringBody(
    """{
          "ContentId": "4CDAE812-DB34-4B47-B02B-953200965640",
          "Action": "OK",
          "ContentType": "text",
          "ApiKey": "24A501F37C53AD1CED5AABFE94942C664BC28CE678D6494D8E29A3515275A588"
          }""")).asJSON
            .check(status.is(200))
          )


var session = "hong2pfw0wz64liydxlovv4timg0hem8"
  val survey =exec(http("survey")
      .get("/triviaweb/survey/?StumpQuestionSetID=1")
      .header("Authorization", session)
      .check(status.is(200)))

  var surveyScn = scenario("CheeriosSimulation.survey").exec(survey)

  val question1 =exec(http("question1")
      .get("/triviaweb/question/?StumpQuestionSetID=1")
      .header("Authorization", session)
      .check(status.is(200)))

  var question1Scn = scenario("CheeriosSimulation.question1").exec(question1)


  val question2 =exec(http("question2")
      .get("/triviaweb/question/?enterprise_id=1")
      .header("Authorization", session)
      .check(status.is(200)))

  var question2Scn = scenario("CheeriosSimulation.question2").exec(question2)

  val draft1 =exec(http("draft1")
      .get("/triviaweb/draft-questions/")
      .queryParam("brandID",1).queryParam("player_id",30302584)
      .header("Authorization", session)
      .check(status.is(200)))

  var draft1Scn = scenario("CheeriosSimulation.draft1").exec(draft1)

  val draft2 =exec(http("draft2")
      .get("/triviaweb/draft-questions/")
      .queryParam("brandID",1).queryParam("player_id",30302584).queryParam("draft_id",0)
      .header("Authorization", session)
      .check(status.is(200)))

  var draft2Scn = scenario("CheeriosSimulation.draft2").exec(draft2)

  val users =exec(http("users")
      .get("/triviaweb/users/?roles=%7B%27group-admin%27:+[]%7D")
      .header("Authorization", session)
      .check(status.is(200)))

  var usersScn = scenario("CheeriosSimulation.users").exec(users)

  val enterprises =exec(http("enterprises")
      .get("/triviaweb/enterprises/")
      .header("Authorization", session)
      .check(status.is(200)))

   var enterprisesScn = scenario("CheeriosSimulation.enterprises").exec(enterprises)

  val game1 =exec(http("game1")
      .get("/triviaweb/game_summary/")
      .header("Authorization", session)
      .check(status.is(200)))

  var game1Scn = scenario("CheeriosSimulation.game1").exec(game1)

  val game2 =exec(http("game2")
      .get("/triviaweb/game_summary/?branding=1")
      .header("Authorization", session)
      .check(status.is(200)))

  var game2Scn = scenario("CheeriosSimulation.game2").exec(game2)

  val game3 =exec(http("game3")
      .get("/triviaweb/game_summary/?host=30302585")
      .header("Authorization", session)
      .check(status.is(200)))

  var game3Scn = scenario("CheeriosSimulation.game3").exec(game3)

  val userget =exec(http("userget")
      .get("/triviaweb/get-user/?user_id=30302585")
      .header("Authorization", session)
      .check(status.is(200)))

  var usergetScn = scenario("CheeriosSimulation.userget").exec(userget)

  val domain =exec(http("domain")
      .get("/triviaweb/setting/?domain=kfc")
      .header("Authorization", session)
      .check(status.is(200)))

  var domainScn = scenario("CheeriosSimulation.domain").exec(domain)

  val location1 =exec(http("location1")
     .get("/triviaweb/locations/")
     .queryParam("brand_id",1)
     .header("Authorization",session)
     .check(status.is(200)))

  var location1Scn = scenario("CheeriosSimulation.location1").exec(location1)

   val location2 =exec(http("location2")
     .get("/triviaweb/locations/")
     .queryParam("location_id",1)
     .header("Authorization",session)
     .check(status.is(200)))

  var location2Scn = scenario("CheeriosSimulation.location2").exec(location2)

  val superadmin =exec(http("superadmin")
     .get("/triviaweb/super-admin/")
     .header("Authorization",session)
     .check(status.is(200)))

  var superadminscn = scenario("CheeriosSimulation.superadmin").exec(superadmin)

  val admAll =exec(http("admAll")
     .get("/triviaweb/ads/")
     .header("Authorization",session)
     .check(status.is(200)))

  var admAllScn = scenario("CheeriosSimulation.admAll").exec(admAll)

  val admByBrand =exec(http("admByBrand")
     .get("/triviaweb/ads/")
     .queryParam("brand_id",1)
     .header("Authorization",session)
     .check(status.is(200)))

  var admByBrandScn = scenario("CheeriosSimulation.admByBrand").exec(admByBrand)

  val imageFromNfs =exec(http("imageFromNfs")
     .get("https://ntnservices.int.buzztime.com/stp/stump/Picture/d8643f6b-3421-9717-0bcb-98d1f8050b8e/round1/question1/a.png")
     .header("Authorization",session)
     .check(status.is(200)))

  var imageFromNfsScn = scenario("CheeriosSimulation.imageFromNfs").exec(imageFromNfs)

  val handshake = exec(
      http("handshake")
      .get("/manifest/handshake/")
      .queryParam("serialnumber", "906")
      .header("authorization", JWT.getCustomeJWT("GET", "/manifest/handshake/", "ps9BrTGB55GJqZ9qIviqZyyvKyMGzrWslf4ljDaR"))
      .check(status.is(200)))
  var handshakeScn = scenario("CheeriosSimulation.handshake").exec(handshake)

   val certificates = exec(
      http("certificates")
      .get("/manifest/certificates/")
      .queryParam("sha256", "35a7bcd104d7f1749234011780767ebde63baa0a70739d12e7e6761acb883de3")
      .header("authorization", JWT.getCustomeJWT("GET", "/manifest/certificates/", "secret"))
      .check(status.is(200)))
   var certificatesScn = scenario("CheeriosSimulation.certificates").exec(certificates)

   val confidentials = exec(
      http("confidentials")
      .get("/manifest/confidentials/")
      .queryParam("sha256", "8ad41d126ceee215bcd5f83df78539cd").queryParam("name", "SiteHub.mqttbroker.password")
      .header("authorization", JWT.getCustomeJWT("GET", "/manifest/confidentials/", "ps9BrTGB55GJqZ9qIviqZyyvKyMGzrWslf4ljDaR"))
      .check(status.is(200)))
   var confidentialsScn = scenario("CheeriosSimulation.confidentials").exec(confidentials)

   val feeds = exec(
    http("feeds")
    .get("/feeds/hostname")
    .check(status.is(200)))
    var feedsScn = scenario("CheeriosSimulation.feeds").exec(feeds)

   val getRMQCreds = exec(
    http("getRMQCreds")
    .get("analytics/rmq_creds/")
    .queryParam("player_id", "30302635")
    .header("authorization", "ESYG6k0Sqy5Jo5ChwboLnnmJxsTBeZhd")
    .check(status.is(200)))
    var getRMQCredsScn = scenario("CheeriosSimulation.getRMQCreds").exec(getRMQCreds)

   val siteSearch = exec(
    http("siteSearch")
    .get("players/game_sites/nearby/")
    .queryParam("latitude", "36.699662").queryParam("longitude", "-95.935095")
    .check(status.is(200)))
    var siteSearchScn = scenario("CheeriosSimulation.siteSearch").exec(siteSearch)


  setUp(
     //siteSearchScn.inject(constantUsersPerSec(15) during (60 seconds))
     getRMQCredsScn.inject(constantUsersPerSec(20) during (60 seconds))
     //feedsScn.inject(constantUsersPerSec(15) during (60 seconds))
     //handshakeScn.inject(rampUsers(38750) over (duration minutes)).throttle(reachRps(150) in (1 minute),holdFor(4 minutes))
     //certificatesScn.inject(rampUsers(65000) over (duration minutes)).throttle(reachRps(200) in (1 minute),holdFor(4 minutes))
     //confidentialsScn.inject(rampUsers(51200) over (duration minutes)).throttle(reachRps(190) in (1 minute),holdFor(4 minutes))
     //surveyScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(3 minutes)),
     //question1Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //question2Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //draft1Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //draft2Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //usersScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //enterprisesScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(3 minutes)),
     //game1Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //game2Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //game3Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //usergetScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(30 minutes)),
     //domainScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (10 minute),holdFor(10 minutes)),
     //eventScn.inject(rampUsers(eventuser) over (duration minutes)).throttle(reachRps(500) in (1 minute),holdFor(3 minutes)),
     //ncrwithEtagScn.inject(rampUsers(crispuserwithetag) over (duration minutes)).throttle(reachRps(500) in (1 minute),holdFor(3 minutes)),
     //ncrwithEtagScnWithPromise.inject(rampUsers(crispuserwithetag) over (duration minutes)).throttle(reachRps(500) in (1 minute),holdFor(3 minutes)),
    //schedule_run.inject(rampUsers(playersusers) over (duration minutes)).throttle(reachRps(500) in (1 minute),holdFor(3 minutes)),
    //location1Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
    //location2Scn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
    //superadminscn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //admAllScn.inject(rampUsers(1000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //admByBrandScn.inject(rampUsers(1000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes))
     //admAllScn.inject(rampUsers(5000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //admByBrandScn.inject(rampUsers(5000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes))
     //admAllScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes)),
     //admByBrandScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (1 minute),holdFor(10 minutes))
     //admAllScn.inject(rampUsers(30000) over (duration minutes)).throttle(reachRps(200) in (1 minute),holdFor(10 minutes))
     //admByBrandScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(100) in (1 minute),holdFor(10 minutes))
    //imageFromNfsScn.inject(rampUsers(10000) over (duration minutes)).throttle(reachRps(50) in (10 minute),holdFor(10 minutes))
 ).protocols(httpProtocol)
}
