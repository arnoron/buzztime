package swarm

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class SwarmSimulation extends Simulation {
  val baseURL = System.getenv("baseUrl")

  val httpProtocol = http
    .baseURL(baseURL)

  val SwarmEndPointScenario = scenario("Swarm_Perf_Simulation")
    .exec(http("Get Homepage").get("/"))
  setUp(SwarmEndPointScenario.inject(rampUsers(11000) over (110 seconds))).protocols(httpProtocol)
}

