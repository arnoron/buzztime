#!/bin/bash
if [ $UID != 0 ]
 then
  exit 0
 else
  echo You are root.
fi
if service --status-all | grep -Fq 'docker'
 then 
docker_version=$(docker --version | awk {'print $3'} | awk -F "." {'print $1'})
echo $docker_version
 if [ $docker_version != 18 ]
   then
     yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine -y
      yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2 -y
      yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
    yum install docker-ce.x86_64 0:18.03.0.ce-1.el7.centos -y
    curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    systemctl start docker
    systemctl enable docker
echo ********************** Docker updated with running version ************************
    fi
else
      yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2 -y
      yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
    yum install yum install docker-ce.x86_64 0:18.03.0.ce-1.el7.centos -y
    curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    systemctl start docker
    systemctl enable docker
fi 


