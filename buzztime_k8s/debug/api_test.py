# Create Virtual Environment
# Run file with "python -c 'from debug import api_test; api_test.test_all_api()'" in command line

import requests

def single_api_test(baseurl):
#     url = "/feeds/sportradar/?url={0}".format(baseurl)
    dev_url = "https://ntnservices.dev.buzztime.com/{0}".format(baseurl)
    local_url = "http://localhost:8000/{0}".format(baseurl)
    local_resonse = requests.get(local_url)
    dev_response = requests.get(dev_url)
#     return "{0}".format(baseurl)
    if dev_response.status_code == 200 and local_resonse.status_code == 200:
        local_data = local_resonse.json()
        dev_data = dev_response.json()
        if dev_data == local_data:
            return "{0} Responses are Same.".format(baseurl)
        else:
            return "{0} Responses are Different.".format(baseurl)
    else:
        return "Responses with error Local:- {2} Dev:- {0}.:- {1}".format(\
            dev_response.status_code, baseurl, local_resonse.status_code)


def test_all_api():
    # connection closed two times.
    print(single_api_test("players/events/sitefinder/?latitude=33.15&longitude=-117.35"))
    print(single_api_test("players/events/sitefinder/?count=20&start_index=11"))
    print(single_api_test("players/players/206276/badges/187"))
    print(single_api_test("players/check_username_availability/?username=john"))
         
       
    print(single_api_test("players/schedule/game/"))
    print(single_api_test("players/schedule/today/"))
    print(single_api_test("players/schedule/day/"))
    print(single_api_test("players/schedule/weekly/"))
  
    print(single_api_test("players/players/206276/gameinstances/63/?mode=local-wins"))
    print(single_api_test("players/players/206276/gameinstances/?type=recent"))
    print(single_api_test("players/players/206276/badges/?type=recent"))
    print(single_api_test("players/sites/25/players"))
    
    print(single_api_test("players/sites/25/games/"))
    print(single_api_test("players/sites/12/games/8981/players"))
    print(single_api_test("players/sites/25/games/50/instances"))
    print(single_api_test("players/sites/25/games/50/players/?start_date=25-03-2014&start_time=18:30"))
    print(single_api_test("players/badges/?classification-type=rarity"))
    
    print(single_api_test("players/sites/15/competitions/"))
    print(single_api_test("players/sites/15/"))
    print(single_api_test("players/sites/?search=mexico&latitude=25.56&longitude=45.67"))
    print(single_api_test("players/games/12/sites/24/players"))
   
    print(single_api_test("players/games/23/sites/"))
    print(single_api_test("players/games/45/players"))
    print(single_api_test("players/players/?sort=trivia-average-desc&state=AR"))
    print(single_api_test("players/sites/25/competitions/50"))
    print(single_api_test("players/players/?sort=trivia-average-desc&handle=HENRY"))
       
    print(single_api_test("players/players/14049509/"))
    print(single_api_test("players/players/14049509/badges/?type=recent"))
    print(single_api_test("players/games/?type=premium"))
    print(single_api_test("players/games/"))
    print(single_api_test("players/players/?sort=num-badges-desc&badge-classifier=name&badge-type=179"))
    print(single_api_test("players/players/?sort=playerplus-points-desc&handle=HENRY"))
    print(single_api_test("players/players/?sort=playerplus-points-desc&state=AR"))
    print(single_api_test("players/players/?search=john"))
    print(single_api_test("players/players/?search=%john%"))
    print(single_api_test("players/games/?type=premium&day=yesterday"))
    print(single_api_test("players/sites/?sort=num-gold-desc&state=FL"))
    print(single_api_test("players/smartestbar/?competition_id=10275"))

#    
#     Error
#     print(single_api_test("players/players/?sort=trivia-average-desc&game-id=87"))
    
#     print(single_api_test("/avatar/?player_id=12"))
    
