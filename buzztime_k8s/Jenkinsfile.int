#!groovy

try {
     node ('dc-dockerenv-int') {
        docker.withRegistry('https://nexus.buzztime.com/', 'hideploy') {
        stage('Deploy and Restart Services') {
            checkout scm
            sh 'docker ps && docker-compose -f int-compose.yml build --pull'
            sh 'docker-compose -f int-compose.yml up -d'
            sh 'docker-compose -f int-compose.yml scale wsgi=2'
            sh 'echo ${NODE_NAME}'
            sh 'docker push nexus.buzztime.com/cheerios/dev-wsgi:latest'
        }

         stage('Redis cache clear') {
            withEnv(['REDIS_CACHE=0']) {
                sh 'if [ $REDIS_CACHE -eq 0 ] ; then echo "Redis & Nginx cache clearing Disabled" ; else echo "Clearing Redis & Nginx cache" && docker-compose -f release-compose.yml exec -T nginx-proxy  bash -c "rm -rvf /tmp/nginx-cache/*" && docker-compose -f release-compose.yml exec -T redis redis-cli FLUSHALL ; fi'
            }
          }
        }
      }

     node ('dc-dockerhost') {
        docker.withRegistry('https://nexus.buzztime.com/', 'hideploy') {
        stage('Build Test Containers') {
            checkout scm
            sh 'docker-compose -f perf-compose.yml pull'
            sh 'docker-compose -f perf-compose.yml build --pull'
        }


         stage('Pylint') {
            sh 'docker-compose -f perf-compose.yml up pylint | awk \'/<.*>/{$1=$2=""; print $0 }\' \
                > out/pylint/pylint.html'
         } 

/*        stage('Fabric3') {
            timeout(5) { sh 'docker-compose -f perf-compose.yml run fabric' }
         } 
*/
       stage('Gatling & AB tests') {

         sh 'docker-compose -f perf-compose.yml run --rm -e baseUrl=https://ntnservices.int.buzztime.com/ gatling'
         //sh 'docker-compose -f perf-compose.yml run --rm abench ab -r -n 20000 -c 400 https://ntnservices.int.buzztime.com/'

         } 
       }
     }


 }

 catch (err) {
    currentBuild.result = "FAILED"
        mail (to: 'buzztime-cheerios@hashedin.com',
             subject: "Job '${env.JOB_NAME}'- (${env.BUILD_NUMBER}) has FAILED",
             body: "Please go to ${env.BUILD_URL} for more details. ");

        throw err
  }


finally {

    node ('dc-dockerhost') {
        stage('Publish Test Data') {
            timeout(5) {
                publishHTML(target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: true,
                        keepAll: true,
                        reportDir: 'out/pylint/',
                        reportFiles: 'pylint.html',
                        reportName: 'Pylint Results'])

/*
                publishHTML(target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: true,
                        keepAll: true,
                        reportDir: 'out/coverage/',
                        reportFiles: 'index.html',
                        reportName: 'Fabric3 Coverage'])
*/
                dir('out/gatling/') {
                    gatlingArchive()
                     }
                 }
              }

        stage('Cleanup testing host') {
            sh 'docker-compose -f perf-compose.yml down -v || true'
            sh 'docker ps --no-trunc --all --quiet --filter="status=exited" | xargs --no-run-if-empty docker rm -f  || true'
            sh 'docker images --no-trunc --all --quiet --filter="dangling=true" | xargs --no-run-if-empty docker rmi -f || true'
            sh 'docker ps'
         }
      }



    node ('dc-dockerenv-int') {
        stage('Cleanup') {
            sh 'docker ps --no-trunc --all --quiet --filter="status=exited" | xargs --no-run-if-empty docker rm -f  || true'
            sh 'docker images --no-trunc --all --quiet --filter="dangling=true" | xargs --no-run-if-empty docker rmi -f || true'
            sh 'docker ps'
        }
    }
}
