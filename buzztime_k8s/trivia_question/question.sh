#!/bin/bash

: '
  This code will all the parameter value which passed in scipt.
  Parameter should be passed using -(hyphen)
  While loop check all -(hyphen) value and assign next value according to -(hyphen) value.
  If -h found in parameter then script will print description of script and then exit.
  u: this means script accept value of -u and next value must needed.
  ho: means scipt -h and -o in parameter if -h not required any value but we must need to pass value of o if scipt has -o in parameter.
'
while getopts "u:p:e:ho:f:q:n:s:g:" opt; do
  case $opt in
    u) username="$OPTARG"
    ;;
    p) passwod="$OPTARG"
    ;;
    e) environment="$OPTARG"
    ;;
    o) operation="$OPTARG"
    ;;
    f) file_path="$OPTARG"
    ;;
    q) question_set_id="$OPTARG"
    ;;
    n) question_set_name="$OPTARG"
    ;;
    s) status_id="$OPTARG"
    ;;
    g) game_id="$OPTARG"
    ;;
    h) printf "usage:  Remove question set from database \n\tbash question.sh -u <username> -p <password> -e <environment> -o delete -q <question_set_id> [-s <status_id>]\n\n"
      printf "usage:  Add question into database.\n\tbash question.sh -u <username> -p <password> -e <environment> -o insert -n <question_set_name> -f <file_path of csv> [-g <game_id>]\n\n"
      printf "This Sciprt has two use.\n\t1.Remove question set from database.\n\t2.Add question into database.\n\n"
      printf "\nList of parameters are mention bellow\n\t-u\tusername for authentication.\n\t-p\tpassword for authentication.\n\t-e\tenvironment of the database.This value can be prod/production, dev/development, stg/staging, int/integration, local/localhost\n\t-o\tadd/insert to insert question in database OR remove/delete to remove question set from database.\n\t[-h]\tFor help.It will give information of script and exit."
      printf "\n\n1.Remove question set from Database\n\t-q\tQuestion set Id.Question set Id.Question set id is available in triviaweb/question/ API. Also available in stump UI game-list page.\n\t[-s]\tStatus id should be 0 for delete and 1 for activate question set."
      printf "\n\n2.Add question into database.\n\t-f\tfile path which contains question list.\n\t-n\tQuestion set name of qyestion list.\n\t[-g]\tGame id of question list default value is 1.\n"
      exit 1
    ;;
  esac
done

: '
  Check Username, password and environment values are specify or not if not then accept value of it using command line.
'
if [ -z "$username" ];
then
  printf "Username:-"
  read username
fi

if [ -z "$passwod" ];
then
  printf "Password:-"
  read -s passwod
fi

if [ -z "$environment" ];
then
  printf "\nEnvironment ( production: prod/production , development: dev/development, integration: int/integration, staging: stg/staging, local: local | localhost) :-"
  read environment
fi

: '
  Convert environment value in lower case and assign value of url according to that value.
'
environment=${environment,,}
case "$environment" in
  "prod" | "production") url="http://ntnservices.buzztime.com/"
  ;;
  "dev" | "development") url="http://ntnservices.dev.buzztime.com/"
  ;;
  "int" | "integration") url="http://ntnservices.int.buzztime.com/"
  ;;
  "stg" | "staging") url="http://ntnservices.stg.buzztime.com/"
  ;;
  "local" | "localhost") url="http://localhost:8000/" 
  ;;
  *) echo "Invalid environment value."
     exit 1
  ;;
esac

: '
  Specify url of question API and login API.
'
question_url="${url}triviaweb/question/"
login_url="${url}triviaweb/login/"

: '
  Check If username contain @ then namespace value should be 6 otherwise 1.
'
if [[ $username =~ .*@.* ]]; then name_space_id=6; else name_space_id=1; fi


: '
  Converting password to md5.
  Create jason for login API parameter value.
'
passwod=$(echo -n $passwod | md5sum | awk '{print $1}')
passwod="\"password\":\"${passwod}\""
username="\"username\":\"${username}\""
name_space_id="\"namespaceId\":\"${name_space_id}\""
parameter='{'$username','$passwod','$name_space_id'}'

: '
  Calling Login API with parameter.
'
result=$(curl -s -X POST $login_url -i -H "Content-type: application/json" -d "$parameter")

: '
  Check if API called proper. If not then throwing an error.
'
if [ -z "$result" ]
then
  echo "API connection issue."
  exit
fi

: '
  Extracting session value from login API response. and assign it to session_id.
'
unauthorize=true
IFS='
'
set -f
for line in $result
do
  if [[ ${line,,} == "session:"* ]]
  then
    unauthorize=false
    break;
  fi
done
set +f
unset IFS
: '
  Check if login resonse has session attribute if not then throw Authentication error. 
'

if [[ $unauthorize == "true" ]]
then
  echo "Authentication fail."
  exit
fi
session_id=$(echo "$line" | cut -d ':' -f 2 | sed -e 's/\s$//g')

: '
  Check operation value is specify or not if not then accept value of it using command line.
'
if [ -z "$operation" ];
then
  printf "Operation\n\t1.Remove question set from database: remove/delete \n\t2.Add question into database: add/insert\n :-"
  read operation
fi

: '
  Create session header using session_id.
'
session_header="authorization:$session_id"

: '
  Convert operation to lower case and perform opertaion acording to that value.
'
operation=${operation,,}
case "$operation" in
  "add" | "insert")
    : '
      Check file_path, question_set_name, game_id values are specify or not if not then accept value of it using command line.
    '
    if [ -z "$file_path" ];
    then
      printf "Enter CSV file path:-"
      read file_path
    fi
    if [ -z "$question_set_name" ];
    then
      printf "Question Set Name:-"
      read question_set_name
    fi
    if [ -z "$game_id" ];
    then
      printf "Game ID (Enter for null/empty):-"
    read game_id
    fi

    : '
      Read CSV file content and create question json object according to content.
    '

    filename=$file_path
    count=0
    result="["
    while read -r line
    do
      count=$((count + 1))
      : '
        Check if first line then it should be header so skip that line.
      '
      if [ $count == 1 ];
      then
      	continue
      fi
      : '
        If is not second line then we need to add ,(comma) in json object.
      '
      if [ $count != 2 ];
      then
        result="$result,"
      fi
	
      : '
        Fecting column value using cut command.
      '
	
      round=$(echo "$line" | cut -d ',' -f 1)
      : '
        If round is bonus then changing round value to 11.
      '
      if [ ${round,,} == "bonus" ];
      then
        round=11
      fi
      round_name=$(echo "$line" | cut -d ',' -f 1)
      category=$(echo "$line" | cut -d ',' -f 2)
      question_id=$(echo "$line" | cut -d ',' -f 4)
      question_text=$(echo "$line" | cut -d ',' -f 5)
      answer=$(echo "$line" | cut -d ',' -f 6)
      categort_dec=$(echo "$line" | cut -d ',' -f 3)
	    
      : '
        Create Json object for question parameter.
      '
      round="\"Round\":\"$round\""
      round_name="\"RoundName\":\"$round_name\""
      category="\"Category\":\"$category\""
      question_id="\"QuestionId\":\"$question_id\""
      question_text="\"QuestionText\":\"$question_text\""
      answer="\"CorrectAnswer\":\"$answer\""
      categort_dec="\"RoundDescription\":\"$categort_dec\""
      object='{'$round','$round_name','$category','$question_id','$question_text','$answer','$categort_dec'}'
      result="${result}${object}"
    done < "$filename"
    result="${result}]"
    : '
      Create Json object question API parameter.
    '
    question="\"Question\":${result}"
    question_set_name="\"QuestionSetName\":\"${question_set_name}\""
    if [[ -z "$game_id" ]]; then game_id=1; fi
    game_id="\"StumpGameID\":\"${game_id}\""
    parameter='{'$question','$question_set_name','$game_id'}'

    : '
      Calling Question API and printing response of it.
    '
    post_result=$(curl -s -i -X POST --data "$parameter" -H "$session_header" -H "Content-type: application/json" "$question_url")
    echo "$post_result" 
  ;;
  "delete" | "remove") 
    : '
      Check question_set_id, status_id values are specify or not if not then accept value of it using command line.
    '
    if [ -z "$question_set_id" ];
    then
      printf "Question Set Id:-"
      read question_set_id
    fi

    if [ -z "$status_id" ];
    then
      printf "status Id 0 for delete/incative and 1 for active(Enter for null/empty):-"
      read status_id
    fi
    if [[ -z "$status_id" ]]; then status_id=0; fi
    : '
      Create Json object question API parameter.
    '
    question_set_id="\"StumpQuestionSetID\":${question_set_id}"
    status_id="\"StatusID\":\"${status_id}\""
    parameter='{'$question_set_id','$status_id'}'

    : '
      Calling Question API and printing response of it.
    '
    post_result=$(curl -s -i -X DELETE --data "$parameter" -H "$session_header" -H "Content-type: application/json" "$question_url")
    echo "$post_result" 
  ;;
  *) echo "Invalid operation value."
     exit 1
  ;;
esac

