"""
Standalone for RabbitMQ Listener
"""
import logging
from urllib.parse import unquote

import pika
import requests

from cheerios.services.jwt_utils import get_jwt
from cheerios.settings import MM_RMQ_USERNAME, MM_RMQ_PASSWORD, MM_RMQ_HOST, LISTENER_QUEUE, \
    MM_VIRTUAL_HOST, LISTENER_PREFIX

LOGGER = logging.getLogger('listener')


def callback(ch, method, properties, body):
    """
    Checks topic in redis for url, posts back the body to the url and Acknowledges accordingly.
    :param ch:
    :param method:
    :param properties:
    :param body:
    :return:
    """
    topic = method.routing_key
    encoded_url = topic.split('.', 1)[1]
    url = unquote(encoded_url)

    payload = body.decode()

    data = dict(
        payload=payload,
    )
    headers = dict(
        Authorization=get_jwt()
    )
    try:
        response = requests.post(url=url, json=data, headers=headers)
        if not response.ok:
            raise Exception("Response not Ok")
        log = "Published {0} ".format(url)
        LOGGER.info(log)
        ch.basic_ack(delivery_tag=method.delivery_tag)
    except Exception as err:
        error = 'Call back Error URL {0} \n{1}'.format(url, str(err))
        LOGGER.error(error)
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)


def start_listener():
    """
    Setups the client and listens to the response
    :return:
    """
    try:
        routing_key = '.'.join([LISTENER_PREFIX, '#'])

        credentials = pika.PlainCredentials(username=MM_RMQ_USERNAME, password=MM_RMQ_PASSWORD)
        connection = pika.BlockingConnection(
            # Default Port is 5672 For AMQP. This is not MQTT Protocol
            pika.ConnectionParameters(
                host=MM_RMQ_HOST,
                credentials=credentials,
                virtual_host=MM_VIRTUAL_HOST
            )
        )

        channel = connection.channel()

        channel.queue_declare(queue=LISTENER_QUEUE, auto_delete=True, passive=True)
        channel.queue_bind(
            exchange='amq.topic',
            queue=LISTENER_QUEUE,
            routing_key=routing_key
        )

        channel.basic_qos(prefetch_count=10)
        channel.basic_consume(callback, queue=LISTENER_QUEUE)
        channel.start_consuming()
    except Exception as err:
        error = 'Client Error {0}'.format(str(err))
        LOGGER.error(error)


def run(*args):
    """
    Starts the listener
    :param args:
    :return:
    """
    start_listener()
